<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		//return view('includes/header');
		return view('pages/home',[
			'title' => 'IT Industrial Training, Java, .Net, Php, Oracle, Sas, Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]
		);
		//view('home');
		//return view('home');best-java-training-in-noida
	}

	public function check_page_exist($pag=""){

		

	}

	public function commingsoonbatches()
	{
		$this->check_page_exist('pages/comming-soon-batch/comming-soon-batches-'.$_GET['center']);
		return view('pages/comming-soon-batch/comming-soon-batches-'.$_GET['center']);
	}
	public function DUC3dmaxtraining()
	{
		return view('pages/3dmaxtraining', [
			'title' => '3D MAX Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);	
	}
	public function DUC6monthsindustrialtraining()
	{
		return view('pages/6-months-industrial-training', [
			'title' => '6 months project based industrial training In Delhi, Noida, Ghaziabad ,Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUC6weekssummertraining()
	{
		return view('pages/6weekssummertraining', [
			'title' => '6 weeks summer training In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUC436clustertraining()
	{
		return view('pages/436clustertraining', [
			'title' => '436 CLUSTER Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUC8051microcontrollertraining()
	{
		return view('pages/8051-microcontroller-training', [
			'title' => '8051 Microcontroller Training Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCaboutus()
	{
		return view('pages/aboutus', [
			'title' => 'Welcome to Ducat' 
		]);
	}
	public function DUCAdministrationessentials()
	{
		return view('pages/Administrationessentials', [
			'title' => 'Administration Essentials for New Admins Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCadvancedigitalmarketingcourse()
	{
		return view('pages/advance-digital-marketing-course', [
			'title' => 'Advance Digital Marketing Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur' 
		]);
	}
	public function DUCadvancedmicrosoftexceltraining()
	{
		return view('pages/advancedmicrosoftexceltraining', [
			'title' => 'Advanced Microsoft Excel Training In Noida Delhi NCR' 
		]);
	}
	public function DUCadvancejavatraining()
	{
		return view('pages/advancejavatraining', [
			'title' => 'Advance Java 6 week Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCadvancephptraining()
	{
		return view('pages/advancephptraining', [
			'title' => 'Advance Php Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur' 
		]);
	}
	public function DUCamazon6weeks()
	{
		return view('pages/amazon6weeks', [
			'title' => 'Amazon 6 weeks TRAINING Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCamazoncloudtraining()
	{
		return view('pages/amazoncloudtraining', [
			'title' => 'Amazon Cloud Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCandroidsummertraining()
	{
		return view('pages/android-summer-training', [
			'title' => '6 Weeks Android Summer Training In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCandroidtraining()
	{
		return view('pages/androidtraining', [
			'title' => 'Android Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCangular2training()
	{
		return view('pages/angular2training', [
			'title' => 'ANGULAR 2 Training in Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCansystraining()
	{
		return view('pages/ansystraining', [
			'title' => 'Ansys Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCapitestingtrainingnoida()
	{
		return view('pages/apitestingtrainingnoida', [
			'title' => 'Api Testing Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCappiumtraining()
	{
		return view('pages/appiumtraining', [
			'title' => 'Appium Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCarduinotraining()
	{
		return view('pages/arduino-training', [
			'title' => 'Arduino Training In Noida Delhi NCR.' 
		]);
	}
	public function DUCarmtraining()
	{
		return view('pages/armtraining', [
			'title' => 'Embedded ARM Training Institutes In delhi ,Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCasafirewalltraining()
	{
		return view('pages/asafirewalltraining', [
			'title' => 'ASA Firewall Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCautocadtraining()
	{
		return view('pages/autocadtraining', [
			'title' => 'AUTOCAD Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCautodeskcertification()
	{
		return view('pages/autodeskcertification', [
			'title' => 'Autodesk Authorized Training & Certification Center In Delhi, Noida, Ghaziabad.' 
		]);
	}
	public function DUCautomationanywheretraining()
	{
		return view('pages/automationanywheretraining', [
			'title' => 'Automation Anywhere Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCavrmicrocontrollertraining()
	{
		return view('pages/avr-microcontroller-training', [
			'title' => 'AVR Microcontroller Training Noida Delhi NCR.' 
		]);
	}
	public function DUCawstraining()
	{
		return view('pages/awstraining', [
			'title' => 'AWS Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCazuretraining()
	{
		return view('pages/azuretraining', [
			'title' => 'AZURE Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCbecomeaninstructor()
	{
		return view('pages/become-an-instructor', [
			'title' => 'Welcome to Ducat' 
		]);
	}
	public function DUCbestjavatraininginnoida()
	{
		return view('pages/best-java-training-in-noida', [
			'title' => 'Best Java Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCBestsaperptraining()
	{
		return view('pages/Best-sap-erp-training', [
			'title' => 'SAP ERP Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCbesttallyerp9Training()
	{
		return view('pages/besttallyerp9Training', [
			'title' => 'TALLY ERP 9 GST Training Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida.' 
		]);
	}
	public function DUCbicognostraining()
	{
		return view('pages/bicognostraining', [
			'title' => 'BI CONGOS Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida,Jaipur.' 
		]);
	}
	public function DUCbigcommercetraining()
	{
		return view('pages/big-commerce-training', [
			'title' => 'BIG COMMERCE TRAINING, E-Commerce Website Development Training In Delhi NCR.' 
		]);
	}
	public function DUCbigdatahadoopdevtraining()
	{
		return view('pages/bigdatahadoopdevtraining', [
			'title' => 'Big data hadoop training Certification|Hadoop Training Center|Big Data hadoop Training Institute in delhi NCR' 
		]);
	}
	public function DUCbigdatatraining()
	{
		return view('pages/bigdatatraining', [
			'title' => 'Big Data training in noida, Ghaziabad, Greater noida, Gurgaon, Faridabad' 
		]);
	}
	public function DUCBlockchaintraining()
	{
		return view('pages/Blockchaintraining', [
			'title' => 'Blockchain Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCctraining()
	{
		return view('pages/c-training', [
			'title' => 'C and Data Structure Training Institutes In delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCcadciviltraining()
	{
		return view('pages/cadciviltraining', [
			'title' => 'CAD Civil Training Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCcadcustomizationtraining()
	{
		return view('pages/cadcustomizationtraining', [
			'title' => 'CAD Customization Training and Certification Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida,Jaipur.' 
		]);
	}
	public function DUCcadmechanicaltraining()
	{
		return view('pages/cadmechanicaltraining', [
			'title' => 'Cad mechanical Training Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCcampustraining()
	{
		return view('pages/campus-training', [
			'title' => 'Campus Training by Ducat' 
		]);
	}
	public function DUCcatiatraining()
	{
		return view('pages/catiatraining', [
			'title' => 'CATIA Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCccnalinux()
	{
		return view('pages/ccna-linux', [
			'title' => 'CCNA and Linux training In Noida Gurgaon Ghaziabad Greater Noida Faridabad.' 
		]);
	}
	public function DUCccnasecuritytraining()
	{
		return view('pages/ccnasecuritytraining', [
			'title' => 'CCNA Security Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCccnatraining()
	{
		return view('pages/ccnatraining', [
			'title' => 'CCNA Training & certification Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCccnptraining()
	{
		return view('pages/ccnptraining', [
			'title' => 'CCNP Training & certification Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCcertificationrequest()
	{
		return view('pages/certificationrequest', [
			'title' => 'Welcome to Ducat Certificate Request Form ' 
		]);
	}
	public function DUCcheckpointfirewalltraining()
	{
		return view('pages/checkpointfirewalltraining', [
			'title' => 'Checkpoint Firewall Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCclassroomtraining()
	{
		return view('pages/classroom-training', [
			'title' => 'Classroom Training At Ducat' 
		]);
	}
	public function DUCcloudcomputingtraining()
	{
		return view('pages/cloudcomputingtraining', [
			'title' => 'CLOUD COMPUTING Training Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCcloudinfrastructuretraining()
	{
		return view('pages/cloudinfrastructuretraining', [
			'title' => 'Cloud Infrastructure Training Institute In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCcloudtraining()
	{
		return view('pages/cloudtraining', [
			'title' => 'CLOUD COMPUTING Training Classes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur' 
		]);
	}
	public function DUCcmosbasedtraining()
	{
		return view('pages/cmosbasedtraining', [
			'title' => 'CMOS BASE Training Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCcncprogrammingtraining()
	{
		return view('pages/cncprogrammingtraining', [
			'title' => 'CNC Programming Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCcontactus()
	{
		return view('pages/contact-us', [
			'title' => 'Welcome to Ducat' 
		]);
	}
	public function DUCcoredotnettraining()
	{
		return view('pages/coredotnettraining', [
			'title' => 'Core dotnet Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCcorejavaandroidandkotlintraining()
	{
		return view('pages/corejavaandroidandkotlintraining', [
			'title' => 'Corejava with Android And Kotlin Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCcoreldrawtraining()
	{
		return view('pages/coreldrawtraining', [
			'title' => 'Coreldraw Training Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCcorepythontraining()
	{
		return view('pages/corepythontraining', [
			'title' => 'Core Python Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCcorporatetraining()
	{
		return view('pages/corporate-training', [
			'title' => 'Ducat Corporate Training' 
		]);
	}
	public function DUCcpptraining()
	{
		return view('pages/cpptraining', [
			'title' => 'C++ and Data Structure Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCdatascienceMLusingrprogramming()
	{
		return view('pages/data-science-ML-using-r-programming', [
			'title' => 'DATA SCIENCE & ML USING R PROGRAMMING Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCdatascienceusingrprogramming()
	{
		return view('pages/data-science-using-r-programming', [
			'title' => 'DATA SCIENCE USING R PROGRAMMING Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCdatasciencemlusingpython()
	{
		return view('pages/datasciencemlusingpython', [
			'title' => 'DATA SCIENCE & ML USING Python Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCdatasciencerprogramming()
	{
		return view('pages/datasciencerprogramming', [
			'title' => 'Data Science Using R Programming 6 weeks Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCdatascienceusingpython()
	{
		return view('pages/datascienceusingpython', [
			'title' => 'Data science using Python TRAINING Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCdatawarehousingtraining()
	{
		return view('pages/datawarehousingtraining', [
			'title' => 'DATA WAREHOUSING Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCDeclarativedevelopment()
	{
		return view('pages/Declarativedevelopment', [
			'title' => 'Declarative Development for Platform App Builders Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCdeeplearninginpythontraining()
	{
		return view('pages/deeplearninginpythontraining', [
			'title' => 'Deep learning in Python Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCdeeplearningtraining()
	{
		return view('pages/deeplearningtraining', [
			'title' => 'Deep learning Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCdevicedriverstraining()
	{
		return view('pages/device-drivers-training', [
			'title' => 'Device Drivers Training In Noida, Delhi, gurgaon, ghaziabad, greater Noida,Faidabad.' 
		]);
	}
	public function DUCdevopstraining()
	{
		return view('pages/devopstraining', [
			'title' => 'DevOps Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida,.' 
		]);
	}
	public function DUCdigitalmarketing()
	{
		return view('pages/digital-marketing', [
			'title' => 'Digital Marketing Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCdigitalmarketing6weeks()
	{
		return view('pages/digitalmarketing6weeks', [
			'title' => 'Digital Marketing Six Weeks Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur' 
		]);
	}
	public function DUCdigitalsystemtraining()
	{
		return view('pages/digitalsystemtraining', [
			'title' => 'DIGITAL SYSTEM Training Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida, Jaipur.' 
		]);
	}
	public function DUCdiplomainhardwarenetworking()
	{
		return view('pages/diplomainhardwarenetworking', [
			'title' => 'DIPLOMA IN HARDWARE NETWORKING Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida,Jaipur.' 
		]);
	}
	public function DUCdjangotraining()
	{
		return view('pages/djangotraining', [
			'title' => 'Django Training Institutes In Noida,  Ghaziabad,  Gurgaon,  Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCdo407ansiblettraining()
	{
		return view('pages/do407ansiblettraining', [
			'title' => 'DO407 Ansible Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCdotnetcoretraining()
	{
		return view('pages/dotnetcoretraining', [
			'title' => 'Dotnet core 4.6 Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCdotnetraining()
	{
		return view('pages/dotnetraining', [
			'title' => '.NET Training  Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCdotnetsixweekstraining()
	{
		return view('pages/dotnetsixweekstraining', [
			'title' => 'Dot net six weeks Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur' 
		]);
	}
	public function DUCdotnettrainingnoida()
	{
		return view('pages/dotnettrainingnoida', [
			'title' => 'Advanced .net Training Certification Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCducatgallery()
	{
		return view('pages/ducat-gallery', [
			'title' => 'Welcome to Ducat' 
		]);
	}
	public function DUCDucatIntegratedIndustrialProfessionalProgram()
	{
		return view('pages/DucatIntegratedIndustrialProfessionalProgram', [
			'title' => 'Ducat Integrated Industrial Professional Program - DIIPP' 
		]);
	}
	public function DUCembeddedlinuxinternalstraining()
	{
		return view('pages/embedded-linux-internals-training', [
			'title' => 'Embedded Linux Training in Noida, Delhi, Gurgaon, Ghaziabad, greater Noida, faridabad, Jaipur.' 
		]);
	}
	public function DUCembeddedandroidtraining()
	{
		return view('pages/embeddedandroidtraining', [
			'title' => 'Embedded with Android Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCembeddedsystemtraining()
	{
		return view('pages/embeddedsystemtraining', [
			'title' => 'Embedded system training in noida, Ghaziabad, Greater noida, Gurgaon, Faridabad.' 
		]);
	}
	public function DUCembeddedtraining()
	{
		return view('pages/embeddedtraining', [
			'title' => 'Embedded Training Institutes In Delhi,Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCerpabap()
	{
		return view('pages/erp-abap', [
			'title' => 'Best ERP ABAP Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCerpbasis()
	{
		return view('pages/erp-basis', [
			'title' => 'ERP BASIS Training In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCerpcrm()
	{
		return view('pages/erp-crm', [
			'title' => 'ERP CRM Training In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCerpfico()
	{
		return view('pages/erp-fico', [
			'title' => 'ERP FICO Training In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCerphr()
	{
		return view('pages/erp-hr', [
			'title' => 'ERP HR(Human resource) Training In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCerpmm()
	{
		return view('pages/erp-mm', [
			'title' => 'ERP MM(Materials Management) Training In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCerppp()
	{
		return view('pages/erp-pp', [
			'title' => 'ERP PP(Production Planning) Training In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCerpsd()
	{
		return view('pages/erp-sd', [
			'title' => 'ERP SD(Sales and Distribution) Training In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCerpbibwtraining()
	{
		return view('pages/erpbibwtraining', [
			'title' => 'ERP BI & BW Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCerpscmtraining()
	{
		return view('pages/erpscmtraining', [
			'title' => 'ERP SCM training in noida, Ghaziabad, Greater noida, Gurgaon, Faridabad' 
		]);
	}
	public function DUCethicalhackingtraining()
	{
		return view('pages/ethicalhackingtraining', [
			'title' => 'Ethical Hacking Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCgdandttraining()
	{
		return view('pages/gdandttraining', [
			'title' => 'GD&T Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCglobalalliances()
	{
		return view('pages/global-alliances', [
			'title' => 'Ducat Global Alliances' 
		]);
	}
	public function DUChadoop6weeks()
	{
		return view('pages/hadoop6weeks', [
			'title' => 'Hadoop 6 weeks Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUChadooptraining()
	{
		return view('pages/hadooptraining', [
			'title' => 'Hadoop training in noida, Ghaziabad, Greater noida, Gurgaon, Faridabad' 
		]);
	}
	public function DUChardwareandelectronicstraining()
	{
		return view('pages/hardware-and-electronics-training', [
			'title' => 'Hardware and Electronics Training In Noida Delhi NCR.' 
		]);
	}
	public function DUChrgeneralisttraining()
	{
		return view('pages/hrgeneralisttraining', [
			'title' => 'HR Generalist Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUChtml5training()
	{
		return view('pages/html5training', [
			'title' => 'Html5 Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad,Greater Noida, Jaipur' 
		]);
	}
	public function DUCibmmainframetraining()
	{
		return view('pages/ibmmainframetraining', [
			'title' => 'IBM MAINFRAME Training In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida.' 
		]);
	}
	public function DUCindustrialtraining()
	{
		return view('pages/industrial-training', [
			'title' => 'Industrial Training Java .net Php Embedded By Ducat' 
		]);
	}
	public function DUCiottraining()
	{
		return view('pages/iottraining', [
			'title' => 'IOT Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida,Jaipur.' 
		]);
	}
	public function DUCiphonetraining()
	{
		return view('pages/iphonetraining', [
			'title' => 'Iphone Training Institutes In Noida, Delhi, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCjavaforbeginners()
	{
		return view('pages/java-for-beginners', [
			'title' => 'Java For Beginners Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCjavaanduitraining()
	{
		return view('pages/javaanduitraining', [
			'title' => 'Java and UI Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCJavafullstackdeveloper()
	{
		return view('pages/Javafullstackdeveloper', [
			'title' => 'Java Full Stack Developer Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCjavahadooptraining()
	{
		return view('pages/javahadooptraining', [
			'title' => 'Java Hadoop 6 weeks Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCjavaseleniumtraining()
	{
		return view('pages/javaseleniumtraining', [
			'title' => 'Java selenium Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur' 
		]);
	}
	public function DUCjavasixweekstraining()
	{
		return view('pages/javasixweekstraining', [
			'title' => 'CORE JAVA Training & Certification Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur' 
		]);
	}
	public function DUCjavawithandroidtraining()
	{
		return view('pages/javawithandroidtraining', [
			'title' => 'Java with Android Training In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCjavawithangular4training()
	{
		return view('pages/javawithangular4training', [
			'title' => 'Java with Angular TRAINING Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCjavawithsqltraining()
	{
		return view('pages/javawithsqltraining', [
			'title' => 'Java with SQL TRAINING Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCjmetertraining()
	{
		return view('pages/jmetertraining', [
			'title' => 'Best jmeter Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCjobfairregistration()
	{
		return view('pages/jobfair-registration', [
			'title' => 'Welcome to Ducat' 
		]);
	}
	public function DUClaraveltraining()
	{
		return view('pages/laraveltraining', [
			'title' => 'Laravel Training In Noida Delhi NCR.' 
		]);
	}
	public function DUClearnandearn()
	{
		return view('pages/learnandearn', [
			'title' => 'Learn and Earn Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida,.' 
		]);
	}
	public function DUCLIGHTNINGCOMPONENTS()
	{
		return view('pages/LIGHTNINGCOMPONENTS', [
			'title' => 'PROGRAMMING LIGHTNING COMPONENTS Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCLoadrunnertraining()
	{
		return view('pages/Loadrunnertraining', [
			'title' => 'Best Load Runner Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCmachinelearningusingrprogramming()
	{
		return view('pages/machine-learning-using-r-programming', [
			'title' => 'MACHINE LEARNING USING R PROGRAMMING Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCmachinelearninginpython()
	{
		return view('pages/machinelearninginpython', [
			'title' => 'Machine Learning in Python 6 weeks Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCmachinelearningtraining()
	{
		return view('pages/machinelearningtraining', [
			'title' => 'MACHINE LEARNING With Python Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCmagentotraining()
	{
		return view('pages/magentotraining', [
			'title' => 'Magento Training Noida Delhi NCR.' 
		]);
	}
	public function DUCmanulselenum6weeks()
	{
		return view('pages/manulselenum6weeks', [
			'title' => 'Manual Testing & Selenium 6 weeks Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCmanul_selenium()
	{
		return view('pages/manul_selenium', [
			'title' => 'Manual Testing & Selenium Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur' 
		]);
	}
	public function DUCmatlabtraining()
	{
		return view('pages/matlabtraining', [
			'title' => 'Matlab Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida,Jaipur.' 
		]);
	}
	public function DUCmcitptraining()
	{
		return view('pages/mcitptraining', [
			'title' => 'MCITP Training & certification Institutes Noida Delhi NCR.' 
		]);
	}
	public function DUCmcsaserver2016training()
	{
		return view('pages/mcsaserver2016training', [
			'title' => 'Mcsa server 2016 Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCmcsatraining()
	{
		return view('pages/mcsatraining', [
			'title' => 'MCSA Training & certification Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCmcse2012exchangetraining()
	{
		return view('pages/mcse2012exchangetraining', [
			'title' => 'MCSE 2012 EXCHANGE SERVER Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCmcsetraining()
	{
		return view('pages/mcsetraining', [
			'title' => 'MCSE Training & certification Institutes In Noida, Ghaziabad, Gurgaon, Faridabad,Greater Noida, Jaipur' 
		]);
	}
	public function DUCmeantraining()
	{
		return view('pages/meantraining', [
			'title' => 'Mean Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCmicrosoftsqlservertraining()
	{
		return view('pages/microsoft-sql-server-training', [
			'title' => 'Microsoft sql server Training In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCmistraining()
	{
		return view('pages/mistraining', [
			'title' => 'MIS Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCmsbitraininginnoida()
	{
		return view('pages/msbi-training-in-noida', [
			'title' => 'Best MSBI Training In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCmultimediaanimation()
	{
		return view('pages/multimedia-animation', [
			'title' => 'Multimedia & Animation Training In Noida Delhi NCR.' 
		]);
	}
	public function DUCnetworking3monthstraining()
	{
		return view('pages/networking3monthstraining', [
			'title' => 'Networking 3 months Training Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCnetworkingtraining()
	{
		return view('pages/networkingtraining', [
			'title' => 'Networking Training Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCnetworkingtraining6weeks()
	{
		return view('pages/networkingtraining6weeks', [
			'title' => 'Networking 6 weeks Training Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUConlineregistration()
	{
		return view('pages/online-registration', [
			'title' => 'Welcome to Ducat' 
		]);
	}
	public function DUCopenstackadmintraining()
	{
		return view('pages/openstackadmintraining', [
			'title' => 'OpenStack Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur,' 
		]);
	}
	public function DUCoracle11gdataguardtraining()
	{
		return view('pages/oracle11gdataguardtraining', [
			'title' => 'ORACLE 11G Data Guard Training & certification Institutes In Noida, Ghaziabad,Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCoracle11gdbatraining()
	{
		return view('pages/oracle11gdbatraining', [
			'title' => 'ORACLE 11G DBA 6 Months Training & certification Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCoracle11gdevtrainingindelhincr()
	{
		return view('pages/oracle11gdevtrainingindelhincr', [
			'title' => 'Oracle 11g Development Training In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCoracle11gractraininginnoida()
	{
		return view('pages/oracle11gractraininginnoida', [
			'title' => 'ORACLE 11G RAC Training & certification Institutes In Noida, Ghaziabad,Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCoracle12cdbatraininginnoida()
	{
		return view('pages/oracle12cdbatraininginnoida', [
			'title' => 'ORACLE 12C DBA Training & certification Institutes In Noida, Ghaziabad,Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCoracleappsdba()
	{
		return view('pages/oracleappsdba', [
			'title' => 'ORACLE App\'s DBA Training & certification Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCoracleappsscm()
	{
		return view('pages/oracleappsscm', [
			'title' => 'Oracle app\'s SCM Training Noida Ghaziabad Gurgaon Faridabad Greater Noida.' 
		]);
	}
	public function DUCoracleappstraining()
	{
		return view('pages/oracleappstraining', [
			'title' => 'Oracle Apps R12 Financials Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad,Greater Noida.' 
		]);
	}
	public function DUCoracletechniacltraining()
	{
		return view('pages/oracletechniacltraining', [
			'title' => 'ORACLE APPS R12 Technical Training & certification Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCpaloaltotraining()
	{
		return view('pages/paloaltotraining', [
			'title' => 'PALO ALTO Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCpcbdesigntraining()
	{
		return view('pages/pcb-design-training', [
			'title' => 'PCB Design Training Institutes In Noida.' 
		]);
	}
	public function DUCpegatraininginnoida()
	{
		return view('pages/pegatraininginnoida', [
			'title' => 'PEGA Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCperformancetuningtraining()
	{
		return view('pages/performancetuningtraining', [
			'title' => 'Performance tuning Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCperlscriptingtraining()
	{
		return view('pages/perlscriptingtraining', [
			'title' => 'Perl scripting Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCphpsixmonthstraining()
	{
		return view('pages/phpsixmonthstraining', [
			'title' => 'php training in noida, Ghaziabad, Greater noida, Gurgaon, Faridabad, Jaipur.' 
		]);
	}
	public function DUCphpsixweekstraining()
	{
		return view('pages/phpsixweekstraining', [
			'title' => 'Php six weeks Training Institutes In Delhi,Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCphptraining()
	{
		return view('pages/phptraining', [
			'title' => 'Core php Training Institutes In Delhi Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCpicmicrocontrollertraining()
	{
		return view('pages/pic-microcontroller-training', [
			'title' => 'PIC Microcontroller Training In Noida, Ghaziabad, Gurgaon, Greater Noida, Faridabad.' 
		]);
	}
	public function DUCplcscadasixweekstraining()
	{
		return view('pages/plcscadasixweekstraining', [
			'title' => 'PLC SCADA 6 weeks training In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur' 
		]);
	}
	public function DUCplcscadatraining()
	{
		return view('pages/plcscadatraining', [
			'title' => 'PLC SCADA Training In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCplsqltraining()
	{
		return view('pages/plsqltraining', [
			'title' => 'PL/SQL Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCpowerbitraininginnoida()
	{
		return view('pages/powerbitraininginnoida', [
			'title' => 'Power BI Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur' 
		]);
	}
	public function DUCprimavertraining()
	{
		return view('pages/primavertraining', [
			'title' => 'Primavera Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur' 
		]);
	}
	public function DUCproetraining()
	{
		return view('pages/proetraining', [
			'title' => 'PROE Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCprogrammaticdevelopers()
	{
		return view('pages/programmaticdevelopers', [
			'title' => 'PROGRAMMATIC DEVELOPMENT USING APEX AND VISUALFORCE Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCpython6weekstraining()
	{
		return view('pages/python6weekstraining', [
			'title' => 'Python 6 weeks TRAINING Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCpythonanddjangotraining()
	{
		return view('pages/pythonanddjangotraining', [
			'title' => 'Python and Django Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCpythontraining()
	{
		return view('pages/pythontraining', [
			'title' => 'Python Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCpythonwithmachinelearning()
	{
		return view('pages/pythonwithmachinelearning', [
			'title' => 'Python with Machine learning TRAINING Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCqtpufttraining()
	{
		return view('pages/qtpufttraining', [
			'title' => 'Best QTP/UFT Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCRanddataanalyticstraining()
	{
		return view('pages/Randdataanalyticstraining', [
			'title' => 'R AND DATA ANALYTICS Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCraspberrypitraining()
	{
		return view('pages/raspberry-pi-training', [
			'title' => 'Raspberry PI Training In Noida, Ghaziabad, Gurgaon, Greater Noida.' 
		]);
	}
	public function DUCreactjstraining()
	{
		return view('pages/reactjstraining', [
			'title' => 'Best React js Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCredhatserverhardeningrh413()
	{
		return view('pages/red-hat-server-hardening-rh413', [
			'title' => 'RED HAT SERVER HARDENING (RH413) training Noida Delhi NCR.' 
		]);
	}
	public function DUCredhattraining()
	{
		return view('pages/redhattraining', [
			'title' => 'Red Hat Linux Training & certification Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCrevittraining()
	{
		return view('pages/revittraining', [
			'title' => 'Revit Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCrh236glustertraining()
	{
		return view('pages/rh236glustertraining', [
			'title' => 'RH236 Gluster Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCrh310openstacktraining()
	{
		return view('pages/rh310openstacktraining', [
			'title' => 'RH310 openstack Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCrhcvatraining()
	{
		return view('pages/rhcvatraining', [
			'title' => 'RHCVA Training & certification Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur,' 
		]);
	}
	public function DUCrhtraining()
	{
		return view('pages/rhtraining', [
			'title' => 'RH342 Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCroboticsprocessautomationcourses()
	{
		return view('pages/roboticsprocessautomationcourses', [
			'title' => 'Best Robotic Process Automation(RPA) Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCroboticstraining()
	{
		return view('pages/roboticstraining', [
			'title' => 'Robotics Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida ,Jaipur.' 
		]);
	}
	public function DUCrpauipathtraining()
	{
		return view('pages/rpauipathtraining', [
			'title' => 'Best Robotics Process Automation (RPA) UI Path Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCrpaworkfusiontraining()
	{
		return view('pages/rpaworkfusiontraining', [
			'title' => 'Best Robotics Process Automation (RPA) Work Fusion Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCrprogrammingtraining()
	{
		return view('pages/rprogrammingtraining', [
			'title' => 'R PROGRAMMING Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCrtostraining()
	{
		return view('pages/rtos-training', [
			'title' => 'RTOS TRAINING Institute in Noida, Gurgaon, Ghaziabad, greater Noida, Faridabad.' 
		]);
	}
	public function DUCsalesforceadmintraining()
	{
		return view('pages/salesforceadmintraining', [
			'title' => 'Salesforce Administration Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCsalesforcedevtraining()
	{
		return view('pages/salesforcedevtraining', [
			'title' => 'Salesforce Development Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCsasbitraining()
	{
		return view('pages/sasbitraining', [
			'title' => 'SAS BI Training & certification Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCsastraining()
	{
		return view('pages/sastraining', [
			'title' => 'SAS Training & certification Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCscheduleafreedemo()
	{
		return view('pages/schedule-a-free-demo', [
			'title' => 'Welcome to Ducat' 
		]);
	}
	public function DUCseleniumwithpythontraining()
	{
		return view('pages/seleniumwithpythontraining', [
			'title' => 'Selenium with Python Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCseotraining()
	{
		return view('pages/seotraining', [
			'title' => 'SEO Training & certification Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCsepdtraining()
	{
		return view('pages/sepdtraining', [
			'title' => 'PERSONALITY DEVELOPMENT & SOFT SKILL Training Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCsharepointadmin2010training()
	{
		return view('pages/sharepointadmin2010training', [
			'title' => 'SharePoint Admin 2010 Training certification Institutes In Noida,Ghaziabad, Gurgaon,Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCsharepointadmin2013training()
	{
		return view('pages/sharepointadmin2013training', [
			'title' => 'SharePoint Admin 2013 Training certification Institutes In Noida,Ghaziabad, Gurgaon,Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCsharepointdev2013training()
	{
		return view('pages/sharepointdev2013training', [
			'title' => 'Sharepoint Development 2013 Training Institutes In Noida, Ghaziabad, Gurgaon,Faridabad, Greater Noida, Jaipur' 
		]);
	}
	public function DUCsharepointtraining()
	{
		return view('pages/sharepointtraining', [
			'title' => 'Sharepoint Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCsolidworkstraining()
	{
		return view('pages/solidworkstraining', [
			'title' => 'Solidworks Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, ' 
		]);
	}
	public function DUCspringboottraining()
	{
		return view('pages/springboottraining', [
			'title' => 'Spring Boot & Microservices SECURITY WITH
HIBERNATE & JPA Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCspringhibernatetraining()
	{
		return view('pages/springhibernatetraining', [
			'title' => 'SPRING HIBERNATE Training & Certification Institutes In delhi Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCsqlserverdbatrainingnoida()
	{
		return view('pages/sql-server-dba-training-noida', [
			'title' => 'SQL Server DBA Training Courses in Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida.' 
		]);
	}
	public function DUCsqtsixmonthstraining()
	{
		return view('pages/sqtsixmonthstraining', [
			'title' => 'SOFTWARE TESTING Training(SQT) Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCsqttraining()
	{
		return view('pages/sqttraining', [
			'title' => 'SOFTWARE QUALITY TESTING Training Institutes In Noida, Ghaziabad, Gurgaon,Faridabad, Greater Noida' 
		]);
	}
	public function DUCstaadprotraining()
	{
		return view('pages/staadprotraining', [
			'title' => 'STAAD PRO Training Institutes In Noida,Ghaziabad,Gurgaon,Faridabad,Greater Noida,Jaipur.' 
		]);
	}
	public function DUCstudentdetails()
	{
		return view('pages/student-details', [
			'title' => 'Student Details Form For job & Placement.' 
		]);
	}
	public function DUCsuccessmantra()
	{
		return view('pages/successmantra', [
			'title' => 'Success Mantra Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida' 
		]);
	}
	public function DUCtableautraininginnoida()
	{
		return view('pages/tableautraininginnoida', [
			'title' => 'Tableau Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur' 
		]);
	}
	public function DUCuiandangulartraining()
	{
		return view('pages/uiandangulartraining', [
			'title' => 'UI AND ANGULAR JS Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCuitraining()
	{
		return view('pages/uitraining', [
			'title' => 'UI Specialization Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCunigraphicstraining()
	{
		return view('pages/unigraphicstraining', [
			'title' => 'Unigraphics Training In Noida, Ghaziabad, Gurgaon, Greater Noida.' 
		]);
	}
	public function DUCunixshellscripttraining()
	{
		return view('pages/unixshellscripttraining', [
			'title' => 'UNIX SHELL SCRIPTING Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCupcomingbatches()
	{
		return view('pages/upcoming-batches', [
			'title' => 'Ducat Upcoming Batches for Noida, Gurgaon, Ghaziabad, Greater Noida, Faridabad, Jaipur.' 
		]);
	}
	public function DUCvlsidesignflowtraining()
	{
		return view('pages/vlsidesignflowtraining', [
			'title' => 'VLSI Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur' 
		]);
	}
	public function DUCvlsiverilogtraining()
	{
		return view('pages/vlsiverilogtraining', [
			'title' => 'VLSI VERILOG Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCvlsiverilogwithtcltraining()
	{
		return view('pages/vlsiverilogwithtcltraining', [
			'title' => 'VLSI VERILOG WITH TCL Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCvlsivhdltraining()
	{
		return view('pages/vlsivhdltraining', [
			'title' => 'Vlsi Vhdl Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCvmwaretraining()
	{
		return view('pages/vmwaretraining', [
			'title' => 'Vmware Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCwebdesigntraining()
	{
		return view('pages/webdesigntraining', [
			'title' => 'Web Designing Training Institutes In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida, Jaipur.' 
		]);
	}
	public function DUCworkshopatcolleges()
	{
		return view('pages/workshop-at-colleges', [
			'title' => 'Arduino Training In Noida Delhi NCR.' 
		]);
	}
	public function DUCxamarintraining()
	{
		return view('pages/xamarintraining', [
			'title' => 'Xamarin Training Institutes In Delhi, Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida,.' 
		]);
	}
}