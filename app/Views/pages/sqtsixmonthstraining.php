<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     SQT SIX MONTHS
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     SQT SIX MONTHS
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      BE A PROFESSION WITH TECHNICAL TRAINING COURSES AT DUCAT
     </h4>
     <p>
      Be a Profession with Technical Training Courses at DUCAT DUCAT is one of the most preferred places where any layman can learn SQT in 6 months and also be an expert in IT. Our course offers an in-depth within reach solution to people. Anyone who prefers this training can learn both advantages and disadvantages of the program and can provide readymade solution of any issue arising from different situation. This course - SQT provides an in depth knowledge going into details starting from system design, problems related to definition, analysis related requirements and others details of testing. Interesting feature of this is that, it includes mock tests and technical test which provides confidence to students to clear technical interview and pass the certification exam with ISTQB. Software testing training will increase portability, maintainability, compatibility, capability, reliability, efficiency and usability of the candidate. This training program helps businessman and job applicants to know a prominent way to progress productivity and cost diminution. DUCAT has trainers who are specialist in this field. They convey their proficiency in a way that the trainee can relate very easily with the subject and excel in the field. The training imparted makes them very much capable of taking proper situation based decision and to solve issues, making them more desired candidates than rest in the job market.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction to Software Application Testing
      </h2>
      <ul>
       <li>
        Overview of Software Application Testing
       </li>
       <li>
        Overview of SDLC [Software Development Life Cycle]
       </li>
       <li>
        Overview of Project Execution Lifecycle
       </li>
       <li>
        Overview of Project Lifecycle
       </li>
       <li>
        Terms used in software testing [Defect, Error, Bug, Failure etc..]
       </li>
      </ul>
      <h2>
       Models of Software Engineering
      </h2>
      <ul>
       <li>
        Waterfall Model
       </li>
       <li>
        Prototype model
       </li>
       <li>
        Spiral Model
       </li>
       <li>
        Incremental Model
       </li>
       <li>
        RAD (Rapid Application Development)
       </li>
       <li>
        V Model
       </li>
      </ul>
      <h2>
       Quality Management System
      </h2>
      <ul>
       <li>
        What is Quality Management System and It's need
       </li>
       <li>
        What is Quality Assurance
       </li>
       <li>
        What is Quality Control
       </li>
       <li>
        Review process and Types of Review Process
       </li>
       <li>
        What is Quality Improvement
       </li>
       <li>
        Quality Management Life cycle
       </li>
       <li>
        Principals of Quality Management
       </li>
       <li>
        Policies and Processes
       </li>
       <li>
        Elements of Processes
       </li>
       <li>
        Overview of Quality and Operational Excellence
       </li>
       <li>
        Quality Standard
       </li>
      </ul>
      <h2>
       Software Testing Methodology
      </h2>
      <ul>
       <li>
        White Box
       </li>
       <li>
        Black Box
       </li>
       <li>
        Grey Box
       </li>
      </ul>
      <h2>
       Levels of Testing
      </h2>
      <ul>
       <li>
        Unit Testing
       </li>
       <li>
        Integration Testing
       </li>
       <li>
        System testing
       </li>
       <li>
        UAT
       </li>
      </ul>
      <h2>
       Testing Types
      </h2>
      <ul>
       <li>
        Smoke Testing/ Sanity Testing
       </li>
       <li>
        Functional Testing
       </li>
       <li>
        Non Functional Testing
       </li>
       <li>
        Security Testing
       </li>
       <li>
        SOA testing
       </li>
       <li>
        Mobile Testing
       </li>
       <li>
        Adhoc and Exploratory testing
       </li>
       <li>
        Web Application Testing
       </li>
       <li>
        Accessibility Testing
       </li>
      </ul>
      <h2>
       Test Plan, Test Reports
      </h2>
      <ul>
       <li>
        Overview of Traceability Matrix and creation
       </li>
       <li>
        Overview of Test Metrics and its importance
       </li>
       <li>
        Test case writing
       </li>
       <li>
        Test case exhibition
       </li>
       <li>
        Bug Reporting
       </li>
      </ul>
      <h2>
       Agile Methodology
      </h2>
      <ul>
       <li>
        Introduction to Agile Methodology
       </li>
       <li>
        Scrum Process
       </li>
       <li>
        Meetings &amp; Artifacts (Sprint, Sprint Burn down chart,Sprint Review etc.)
       </li>
      </ul>
      <h2>
       Practical Implementation of Agile Introduction to Agile Tool
      </h2>
      <ul>
       <li>
        How to create User Story
       </li>
       <li>
        How to Add Task
       </li>
       <li>
        How to Track Tasks
       </li>
       <li>
        Acceptance criteria of Task Order
       </li>
       <li>
        Kanban Board
       </li>
      </ul>
      <h2>
       Software Testing Lifecycle (STLC) Practical Introduction to Project (Lab Sessions)
      </h2>
      <ul>
       <li>
        Creation of Use Cases
       </li>
       <li>
        Creation Of Test Plan
       </li>
       <li>
        Creation Of Test Cases
       </li>
       <li>
        Implementation of testing optimization Techniques (ECP, BVA, Decision Table...)
       </li>
       <li>
        Review Of Test Cases
       </li>
       <li>
        Execution Of Test Case
       </li>
       <li>
        Agile Tracking, Scrum Meeting and Daily Stand-ups
       </li>
       <li>
        Defect Reporting
       </li>
      </ul>
      <h2>
       OVERVIEW OF DOMAINS
      </h2>
      <ul>
       <li>
        Insurance
       </li>
       <li>
        Health Care
       </li>
       <li>
        General Banking Overview
       </li>
       <li>
        Basic understanding of Insurance Business
       </li>
       <li>
        Know Terminology used in Insurance
       </li>
       <li>
        Type of insurance
       </li>
       <li>
        Non Life &amp; Life &amp; General Insurance
       </li>
       <li>
        Introduction of Health care Domain
       </li>
       <li>
        Health care Terminology
       </li>
       <li>
        Health care work Flow
       </li>
       <li>
        Role of Bank
       </li>
       <li>
        Types of Banks
       </li>
       <li>
        Banking structure in India
       </li>
       <li>
        Service Offered by Bank
       </li>
       <li>
        Type of Cards
       </li>
       <li>
        Mobile Banking
       </li>
      </ul>
      <h2>
       Introduction
      </h2>
      <ul>
       <li>
        What is Database Application ?
       </li>
       <li>
        Understanding data storage
       </li>
       <li>
        Back end &amp; front end Testing
       </li>
       <li>
        General Database Basics
       </li>
       <li>
        Relational Database Concepts
       </li>
      </ul>
      <h2>
       Basic Sql
      </h2>
      <ul>
       <li>
        Overview of DML , DDL, TCL
       </li>
       <li>
        DML : Insert , update , Delete , marge
       </li>
       <li>
        DDL : Create, Drop, Alter, Rename, Modify, Truncate
       </li>
       <li>
        TCL : Commit, RoleBack, SavePoint
       </li>
      </ul>
      <h2>
       SQL Functions
      </h2>
      <ul>
       <li>
        Single Row Function / Multiple Row Function
       </li>
       <li>
        String Function , Number Function , Date and Time Function, General Function
       </li>
      </ul>
      <h2>
       Joining Tables
      </h2>
      <ul>
       <li>
        Obtaining data from multiple tables
       </li>
       <li>
        Types of joins : Inner joins , Non eque Join, Cross Join, Natural Join, Left Join ,Right join, Full Join
       </li>
      </ul>
      <h2>
       Operators : (Data Using Group Function)
      </h2>
      <ul>
       <li>
        Arithmatic Operator
       </li>
       <li>
        Relational/Logical
       </li>
       <li>
        Like Operator
       </li>
      </ul>
      <h2>
       Constraints &amp; View
      </h2>
      <ul>
       <li>
        Not Null
       </li>
       <li>
        Unique
       </li>
       <li>
        Primary key
       </li>
       <li>
        Foreign key
       </li>
       <li>
        Check
       </li>
      </ul>
      <h2>
       QUICK TEST PROFESSIONAL (QTP)
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Object Identification
       </li>
       <li>
        Object Spy
       </li>
       <li>
        Batch Execution
       </li>
       <li>
        Run Mode in QTP
       </li>
      </ul>
      <h2>
       LOAD RUNNER
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Load runner testing process
       </li>
       <li>
        Vuser Scripts
       </li>
       <li>
        Working with Vuser
       </li>
       <li>
        Transactions
       </li>
       <li>
        Parametrization
       </li>
       <li>
        Check Points
       </li>
       <li>
        Rendezvous Points
       </li>
       <li>
        LR controller
       </li>
      </ul>
      <h2>
       SELENIUM IDE
      </h2>
      <ul>
       <li>
        What is Selenium / Testing tool selenium
       </li>
       <li>
        Introduction to Selenium IDE,RC and WebDriver
       </li>
       <li>
        Configuring IDE
       </li>
       <li>
        Recording Script
       </li>
       <li>
        Running, Pausing and debugging Script
       </li>
       <li>
        Running a Script line by line
       </li>
       <li>
        Inserting commands in between script,
       </li>
       <li>
        Commands commonly used in the IDE
       </li>
       <li>
        About XPATHS, How to Get XPATH
       </li>
       <li>
        Verification Mode in IDE
       </li>
       <li>
        User-extension JS in Selenium IDE
       </li>
       <li>
        Using IF statements and loops in IDE
       </li>
       <li>
        Echo,StoreEval and StoredVars
       </li>
       <li>
        Randomization of Test Data
       </li>
      </ul>
      <h2>
       QUALITY CENTER (QC)
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Site Administration
       </li>
       <li>
        Test Planning
       </li>
       <li>
        Test Execution
       </li>
       <li>
        Filter Technique
       </li>
       <li>
        Sorting Technique
       </li>
       <li>
        Attachments Technique
       </li>
       <li>
        Defect Tracking
       </li>
       <li>
        Writing Test case using QC
       </li>
       <li>
        Execution of test cases in QC
       </li>
       <li>
        Overview of defect life cycle
       </li>
       <li>
        Logging Defect in QC
       </li>
      </ul>
      <h2>
       USP
      </h2>
      <ul>
       <li>
        Certification level training of ISTQB.
       </li>
       <li>
        Detailed Training on each topic-(From basic till advance level).
       </li>
       <li>
        Practical Exposure- Enables Professional to work in the Industry immediately after the course.
       </li>
       <li>
        Chance to work on Live Projects.
       </li>
       <li>
        Updated and well equipped Lab.
       </li>
       <li>
        12 Hours lab facility with lab coordinator.
       </li>
       <li>
        Training on the latest versions.
       </li>
       <li>
        100% Job Assistance.
       </li>
      </ul>
      <h2>
       QTP Fundamentals
      </h2>
      <ul>
       <li>
        Why QTP ?
       </li>
       <li>
        When do we use QTP ?
       </li>
       <li>
        Which application will we test? Will be doing live applications testing
       </li>
       <li>
        Downloading and installing trial version
       </li>
       <li>
        Installing addins
       </li>
       <li>
        Installing script debugger
       </li>
       <li>
        QTP vs other automation tools
       </li>
       <li>
        Advantages/ Disadvantages of QTP
       </li>
       <li>
        How much VB scripting is required in QTP?
       </li>
       <li>
        Simple Record and Run
       </li>
      </ul>
      <h2>
       Basics of QTP
      </h2>
      <ul>
       <li>
        Creating a new Test
       </li>
       <li>
        Record/Run
       </li>
       <li>
        Record and Run settings
       </li>
       <li>
        Rules for recording web based applications
       </li>
       <li>
        Rules for recording windows based applications
       </li>
       <li>
        Run from step and Run to step
       </li>
       <li>
        Commenting and uncommenting code
       </li>
       <li>
        Changing font size in QTP
       </li>
       <li>
        Expert view and Keyword view - Which is better?
       </li>
       <li>
        Option Steps
       </li>
       <li>
        Slowing down the script execution speed
       </li>
       <li>
        Saving the test
       </li>
       <li>
        Batch Runner
       </li>
      </ul>
      <h2>
       Recording types, Virtual objects
      </h2>
      <ul>
       <li>
        What If QTP fails to recognize objects?
       </li>
       <li>
        Analog Recording Mode
       </li>
       <li>
        Low Level Recording Mode
       </li>
       <li>
        Virtual objects and their usage
       </li>
      </ul>
      <h2>
       Insert
      </h2>
      <ul>
       <li>
        Dimension [Inferred , Horizontal , vertialcal , Parallel , Radius , Radius to center , Folded Radious Concentic Circle , Arc Length , Horizontal chain , Vertiacal chain , Horizontal
       </li>
      </ul>
      <h2>
       Objects, Object Repository
      </h2>
      <ul>
       <li>
        What is an object?
       </li>
       <li>
        Object Spy
       </li>
       <li>
        Features in Object Spy
       </li>
       <li>
        Classification of objects
       </li>
       <li>
        Object Repository
       </li>
       <li>
        Mapping OR with code and application
       </li>
       <li>
        Object hierarchy in Object repository
       </li>
       <li>
        Dragging objects
       </li>
       <li>
        Object Synchronization and wait
       </li>
       <li>
        Adding objects manually in OR
       </li>
       <li>
        Adding all objects of page in OR
       </li>
       <li>
        Adding selected objects of page
       </li>
       <li>
        Finding Object in OR from app
       </li>
       <li>
        Finding Object in app from OR
       </li>
       <li>
        Storing OR on hard disk
       </li>
       <li>
        Local and shared object repository
       </li>
       <li>
        Implementing QTP in your project and importance of shared repository
       </li>
       <li>
        Object repository manager
       </li>
       <li>
        Merging object repositories
       </li>
      </ul>
      <h2>
       Object Identification
      </h2>
      <ul>
       <li>
        Native and Identification Properties
       </li>
       <li>
        GetRoProperty - Read Identification properties
       </li>
       <li>
        Script to find browser version
       </li>
       <li>
        Script to print browser title
       </li>
       <li>
        Find number of search results on live applications
       </li>
       <li>
        Links
       </li>
       <li>
        List box / Multi Select List Box
       </li>
       <li>
        Input Box
       </li>
       <li>
        Button
       </li>
       <li>
        Radio Buttons
       </li>
       <li>
        CheckBoxes
       </li>
       <li>
        Identifying/reading Text from application
       </li>
       <li>
        Spy Operations
       </li>
       <li>
        Capture screenshot of page
       </li>
       <li>
        Capture screenshot of an object like Link, button etc
       </li>
      </ul>
      <h2>
       Checkpoints
      </h2>
      <ul>
       <li>
        What are checkpoints?
       </li>
       <li>
        Active screen
       </li>
       <li>
        Concept of step generator
       </li>
       <li>
        Standard Checkpoint
       </li>
       <li>
        Text checkpoint
       </li>
       <li>
        Text area checkpoint
       </li>
       <li>
        Bitmap checkpoint
       </li>
       <li>
        Database checkpoint
       </li>
       <li>
        Accessibility checkpoint
       </li>
       <li>
        XML Checkpoint
       </li>
       <li>
        Practicality of checkpoints
       </li>
       <li>
        Checkpoints in Webtables
       </li>
      </ul>
      <h2>
       Output Values
      </h2>
      <ul>
       <li>
        What is output value?
       </li>
       <li>
        Standard Output value
       </li>
       <li>
        Text Output value
       </li>
       <li>
        Text area Output value
       </li>
       <li>
        Database Output value
       </li>
       <li>
        Checkpoints and output value with Webtables
       </li>
       <li>
        Parameterizing output values
       </li>
       <li>
        Practicality of checkpoints and output value
       </li>
      </ul>
      <h2>
       Parameterization
      </h2>
      <ul>
       <li>
        What is Parameterization
       </li>
       <li>
        Dynamic Parameterization
       </li>
       <li>
        Data Table Parameterization
       </li>
       <li>
        Random Parameterization
       </li>
       <li>
        Environment Parameterization
       </li>
      </ul>
      <h2>
       Recovery Scenarios
      </h2>
      <ul>
       <li>
        What is recovery scenario manager?
       </li>
       <li>
        When do we use Recover scenario in QTP?
       </li>
       <li>
        Various phases of recovery scenario
       </li>
       <li>
        Handling popups with recovery scenario
       </li>
       <li>
        Handling errors with Recovery scenario
       </li>
       <li>
        Parameters of the recovery Function
       </li>
       <li>
        Enabling conditions of recovery scenario
       </li>
      </ul>
      <h2>
       VB Scripting Fundamentals
      </h2>
      <ul>
       <li>
        What is VB? Why VB in QTP ?
       </li>
       <li>
        variables and constants
       </li>
       <li>
        Msgbox, Inputbox, vbCRLF
       </li>
       <li>
        Running Script
       </li>
       <li>
        Concatination operator
       </li>
       <li>
        Managing long Strings
       </li>
       <li>
        Run from Step and Run to Step
       </li>
       <li>
        Optionexplicit
       </li>
       <li>
        Statements
       </li>
       <li>
        While loop, for loop
       </li>
       <li>
        Exiting loops
       </li>
       <li>
        Debugging Script
       </li>
       <li>
        Practical usage of loops in QTP
       </li>
       <li>
        Creating/Executing VB scripts in QTP
       </li>
       <li>
        Creating/Executing VB scripts in notepad
       </li>
      </ul>
      <h2>
       Actions, Function Libraries
      </h2>
      <ul>
       <li>
        What Is action?
       </li>
       <li>
        Exit Action
       </li>
       <li>
        Multiple Actions in Test
       </li>
       <li>
        Shared OR of the Actions
       </li>
       <li>
        What are reusable actions
       </li>
       <li>
        Creating and calling a reusable action
       </li>
       <li>
        Local Functions
       </li>
       <li>
        Library Functions
       </li>
       <li>
        Practical usage of function libraries
       </li>
       <li>
        When to use reusable actions and when to use function libraries
       </li>
       <li>
        Split Action
       </li>
      </ul>
      <h2>
       Working without OR, Handling Webtables
      </h2>
      <ul>
       <li>
        Working without Object repository
       </li>
       <li>
        Why/When to work without OR
       </li>
       <li>
        Counting total open browsers
       </li>
       <li>
        Closing all open Browsers
       </li>
       <li>
        Identifying object with single/multiple properties without using OR
       </li>
       <li>
        What if 2 objects have same Properties
       </li>
       <li>
        Working with Links opening in new tab
       </li>
       <li>
        Type of Descriptive Programming
       </li>
       <li>
        Dynamic and Static Programming
       </li>
       <li>
        Description .Create
       </li>
      </ul>
      <h2>
       Functions and Objects in VB
      </h2>
      <ul>
       <li>
        Arrays,Dim
       </li>
       <li>
        Redim and arrays
       </li>
       <li>
        Sub, Option explicit
       </li>
       <li>
        Sat Statement
       </li>
       <li>
        Using Inbuilt VB Functions
       </li>
       <li>
        User Define Function
       </li>
       <li>
        Type Conversion functions
       </li>
       <li>
        Date and Time functions
       </li>
       <li>
        Comparing dates
       </li>
       <li>
        Formatting dates
       </li>
       <li>
        String manipulation functions
       </li>
       <li>
        Practical usage of functions in QTP
       </li>
       <li>
        On error resume next
       </li>
       <li>
        Methods and Properties of Err Object
       </li>
      </ul>
      <h2>
       Descriptive programming using VB
      </h2>
      <ul>
       <li>
        Counting all the objects in a page
       </li>
       <li>
        How does QTP extracts objects
       </li>
       <li>
        Extracting all the objects and printing properties of objects
       </li>
       <li>
        What is Micclass
       </li>
       <li>
        Extracting all links of page and printing their names
       </li>
       <li>
        When do we use descriptive programming and bulk object extraction
       </li>
       <li>
        Unique properties and similar objects
       </li>
       <li>
        Extracting specific objects
       </li>
       <li>
        Descriptive programming and various web components
       </li>
       <li>
        Finding all result links after searching on Live Applications
       </li>
       <li>
        Exercise on online applications(Google,Gmail,Yahoo and Facebook)
       </li>
       <li>
        Navigating through Pagination links
       </li>
      </ul>
      <h2>
       Descriptive Programming/VB continued
      </h2>
      <ul>
       <li>
        Objects inside objects
       </li>
       <li>
        Count Child Object
       </li>
       <li>
        Finding Objects which do not have Unique properties
       </li>
       <li>
        Login Text Fields in any live applications
       </li>
       <li>
        Closing all open Browsers
       </li>
       <li>
        Using regular expressions in descriptions
       </li>
       <li>
        Using Regex to Handle dynamically changing object
       </li>
      </ul>
      <h2>
       Descriptive programming/VB - Testing Links
      </h2>
      <ul>
       <li>
        Extracting links and clicking them one by one
       </li>
       <li>
        How to find if link is broken
       </li>
       <li>
        Testing Pagination Links
       </li>
      </ul>
      <h2>
       Reports , ToProperty and ROProperty, Object validation
      </h2>
      <ul>
       <li>
        Various Test Status-Pass, Fail, Done, Warning
       </li>
       <li>
        Reporter Object
       </li>
       <li>
        Reporter .report event
       </li>
       <li>
        Properties and Methods of reporter object
       </li>
       <li>
        Marking test case fail/pass
       </li>
       <li>
        Difference between TO Property and RO property
       </li>
       <li>
        Comparing RO and TO properties
       </li>
       <li>
        Reporting RO and TO properties validation in reports using a dictionary object
       </li>
      </ul>
      <h2>
       Datatable/XL file handling
      </h2>
      <ul>
       <li>
        Why data table
       </li>
       <li>
        What is data table object
       </li>
       <li>
        Local and global sheets
       </li>
       <li>
        Global Sheet - Reading/writing data
       </li>
       <li>
        Local Sheet - Reading/writing data
       </li>
       <li>
        Adding custom sheets in data table
       </li>
       <li>
        Run time Data table
       </li>
       <li>
        Adding columns through script
       </li>
       <li>
        Importing xls sheet data into data table sheet
       </li>
       <li>
        Importing complete xls file in data table
       </li>
       <li>
        Exporting sheet from xls file
       </li>
       <li>
        Exporting complete data table to xls file
       </li>
       <li>
        Counting Rows and Column in Datatable
       </li>
      </ul>
      <h2>
       Handling XLS files with VB script
      </h2>
      <ul>
       <li>
        Why do we need Xls file handling?
       </li>
       <li>
        VB script to create xls file
       </li>
       <li>
        Opening existing xls file
       </li>
       <li>
        FileSystemObject object methods and properties
       </li>
       <li>
        VB script to find if XLS file is existing
       </li>
       <li>
        Overriding an existing file
       </li>
       <li>
        Adding sheet / Deleting sheets
       </li>
       <li>
        Counting number of rows/column in a sheet
       </li>
       <li>
        Reading from XLS File
       </li>
       <li>
        Writing in XLS File
       </li>
       <li>
        Determining if sheet/column is existing
       </li>
       <li>
        Copying contents of one sheet to another
       </li>
       <li>
        Comparing contents of 2 sheets
       </li>
       <li>
        Concept of function library
       </li>
       <li>
        Creating custom Xls Functions and storing them in VBS File
       </li>
       <li>
        Preparing custom functions to read, write, delete and add sheets/cells/columns
       </li>
      </ul>
      <h2>
       Dictionary Object, File Handling(FSO) in VB
      </h2>
      <ul>
       <li>
        What is Dictionary Object in VB
       </li>
       <li>
        Methods and Properties of Dictionary Object
       </li>
       <li>
        What is FileSystemObject
       </li>
       <li>
        FileSystemObject object methods and properties
       </li>
       <li>
        Reading, Writing, Creating, Deleting files
       </li>
       <li>
        Creating files Using FSO
       </li>
       <li>
        Writing files Using FSO
       </li>
       <li>
        Reading Files Using FSO
       </li>
       <li>
        Files Collection - Handling multiple files
       </li>
       <li>
        Folders Collection - Handling multiple folders
       </li>
       <li>
        Random Number generation
       </li>
      </ul>
      <h2>
       QC connection with QTP
      </h2>
      <ul>
       <li>
        What is QC
       </li>
       <li>
        QC Addin
       </li>
       <li>
        QC Test Plan
       </li>
       <li>
        QC Test Resources
       </li>
       <li>
        Saving a Test in QC
       </li>
       <li>
        Adding Test to QC Test Set
       </li>
       <li>
        Executing QTP test through QC
       </li>
       <li>
        Batch execution of tests
       </li>
       <li>
        Viewing QTP test run reports through QC
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="sqtsixmonthstraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="5 + 96 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="101">
       <input name="url" type="hidden" value="/sqtsixmonthstraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>