<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     About us
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     About us
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-12 detailsStyle">
    <h3>
     WELCOME TO DUCAT CREATIVE
    </h3>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
    <div class="clearfix">
    </div>
    <p>
     Getting a job is as difficult as beating the crowd because being in the corporate world demands a lot from the applicant because of which the applicants are putting their best, which results in the increment of difficulty level. You can see each and every thing is connected but the solution of this problem is either spending years to reach to a desired position or come to Ducat. At Ducat we provide the entire necessary computer training which helps the newbies and also the experienced workers so that they can achieve better recognition in this competitive world.
    </p>
    <h4>
     How Ducat is helpful?
    </h4>
    <p>
     Like other educational and training industry at Ducat you will be offered varieties programs but the instructors makes the difference and make Ducat stand out from others. We have a variety of skilled and trained trainers whose approach is different which you can see anywhere. Ducat contributes a lot to the knowledge of its trainees and we try our level hard to contribute the best to increase our trainee�s ability so that they stand out from others and whatever they contribute to the corporate world automatically becomes productive. Not only the fresher but also the corporates who are not able to deal with the rising technology and software are also helped here. We try our level best to deliver our services to every corner of the world by the help of customized education. Our motto is to deliver the best services to you and that is why we have taken the customized approach because we do not want you to compromise with your education.
    </p>
    <div class="row">
     <div class="col-md-6">
      <p>
       It is not necessary that you have to leave your job in order to make-up with us. You can contact our experts and can get the best result. To serve you we are always at your service, you can contact us as and when you get time and clear your queries.
      </p>
      <h4>
       WHAT TYPES OF SERVICES ARE OFFERED BY DUCAT?
      </h4>
      <p>
       Ducat provides the best available programs which helps in enhancing the technical skills which seems to be beneficial for all the applicants.
      </p>
      <ul>
       <li>
        <strong>
         Software Development:
        </strong>
        We provide the best and latest IT software training which helps all the fresher and the corporates to understand well and give them the knowledge to go hand in hand with the latest technologies. This does not only helps the companies but also increases the self-level to deal with all the necessary software.
       </li>
       <li>
        <strong>
         Instructor led campus:
        </strong>
        Ducat helps all the new instructors to get the best exposure to show their talent in right way.
       </li>
       <li>
        <strong>
         Workshops and Placement Service:
        </strong>
        At Ducat, workshops are held to increase the understanding level because theoretical values are always not enough and workshops helps in getting the practical knowledge which results in better understanding. As everything leads to the placement because if the institute does not provide placement services then it is ultimately bad for the applicants but we provide the best placement services and for that we give our best to give you the best.
       </li>
      </ul>
     </div>
     <!-- End Of Col MD 6 -->
     <div class="col-md-6">
      <img alt="Ducat" class="img-responsive" src="../images/about-us.png"/>
     </div>
     <!-- End Of Col MD 6 -->
    </div>
    <!-- End Of Row -->
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
 <div class="aboutBottom">
  <div class="container">
   <div class="row">
    <div class="col-md-12 text-center">
     <h3>
      WELCOME TO DUCAT CREATIVE
     </h3>
     <div class="separator short">
      <div class="separator_line">
      </div>
     </div>
     <br/>
     <p>
      Find more about our capabilities and skills. We work for you.
     </p>
     <br/>
     <img alt="About" class="img-responsive" src="../images/about.jpg"/>
    </div>
    <!-- End Of Col MD 12 -->
   </div>
   <!-- End Of Row -->
  </div>
  <!-- End OF Container -->
 </div>
 <!-- End Of About Bottom -->
 <section id="b2brelation">
  <div class="container">
   <div class="row">
    <div class="col-md-12 text-center">
     <h2>
      TRUSTED BY LEADING COMPANIES AND INSTITUTIONS
     </h2>
     <div class="separator short">
      <div class="separator_line">
      </div>
     </div>
    </div>
    <!-- End Of Col MD 12 -->
   </div>
   <!-- End Of Row -->
   <div class="row">
    <div class="col-md-12 text-center">
     <img alt="Ducat" class="img-responsive" src="../images/companies.jpg"/>
    </div>
    <!-- End Of Col MD 12 -->
   </div>
   <!-- End Of Row -->
  </div>
  <!-- End OF Container -->
 </section>
</section>
<section id="b2brelation">
 <div class="container">
  <div class="row">
   <div class="col-md-12 text-center">
    <h2>
     TRUSTED BY LEADING COMPANIES AND INSTITUTIONS
    </h2>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
  <div class="row">
   <div class="col-md-12 text-center">
    <img alt="Ducat" class="img-responsive" src="../images/companies.jpg"/>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section class="text-center" id="offices">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <h5>
     CORPORATE OFFICE NOIDA:
     <span>
      0120 - 4646464
     </span>
    </h5>
    <p>
     GR.NOIDA:
     <span>
      0120-4345190
     </span>
     GHAZIABAD:
     <span>
      0120-4835400
     </span>
     FARIDABAD:
     <span>
      0129-4150605
     </span>
     GURGAON:
     <span>
      0124-4219095
     </span>
     JAIPUR:
     <span>
      0141-2550077
     </span>
    </p>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>