<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Classroom Training
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Classroom Training
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <h3>
     Classroom Training At Ducat
    </h3>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
    <div class="coursesArea">
     <br/>
     <p>
      <img alt="corporate-training" class="img-responsive" src="../images/ducatclass-new.jpg"/>
     </p>
     <p>
      Ducat Classroom Training gives you hands-on, interactive learning with expert instructors. Solidify your understanding through practice exercises. Classroom training has been the foundation of employee and client education for years. Many participants learn best and have the greatest opportunity for retention when they learn from a live instructor in a classroom setting. Classroom training affords the opportunity to incorporate various learning principles, including lectures, demonstrations, hands-on workshops, and retention quizzes.
     </p>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/cc1.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        Solution Focused Approach
       </h3>
       <p class="paraText">
        Solution-focused is a future-focused, goal-directed approach to therapy that highlights the importance of searching for solutions. Trainer allows Candidate to adopt different perspectives to look at a specific situation and therefore deepen their understanding.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/cc2.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        Hands-On Demonstrations
       </h3>
       <p class="paraText">
        Reinforce new concepts with hands-on exercises that put your learning in action.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/cc3.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        Real Time Environment
       </h3>
       <p class="paraText">
        READY is the Buzz word for Industry, The learning system followed is replica to real time applications in Industry, making the candidates professional &amp; can immediately be Developed on the Projects.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/cc4.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        Interact with Top Instructors
       </h3>
       <p class="paraText">
        Meet face-to-face with expert instructors; participate in question &amp; answer sessions throughout the course. An interactive session full of examples, exercises, project based learning builds a great peer group that facilitates quicker problem solving.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <form class="searchForm">
     <input placeholder="Search" type="text"/>
    </form>
    <div class="widgetArea">
     <h5>
      CONTACT INFO
     </h5>
     <address>
      <span class="address">
       A - 43 &amp; A - 52 Sector - 16,
       <br/>
       Noida (U.P) (Near McDonalds)
      </span>
      <br>
       <span class="phone">
        <strong>
         Phone:
        </strong>
        0120-4646464, +91- 9871055180
       </span>
       <br/>
       <span class="email">
        <strong>
         E-Mail:
        </strong>
        <a href="mailto:info@ducatindia.com">
         info@ducatindia.com
        </a>
       </span>
       <br/>
       <span class="web">
        <strong>
         Web:
        </strong>
        <a href="http://www.ducatindia.com/">
         http://www.ducatindia.com/
        </a>
       </span>
      </br>
     </address>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section class="text-center" id="offices">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <h5>
     CORPORATE OFFICE NOIDA:
     <span>
      0120 - 4646464
     </span>
    </h5>
    <p>
     GR.NOIDA:
     <span>
      0120-4345190
     </span>
     GHAZIABAD:
     <span>
      0120-4835400
     </span>
     FARIDABAD:
     <span>
      0129-4150605
     </span>
     GURGAON:
     <span>
      0124-4219095
     </span>
     JAIPUR:
     <span>
      0141-2550077
     </span>
    </p>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>