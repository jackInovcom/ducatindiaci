<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     BOOST YOUR CONFIDENCE WITH PERFECT PERSONALITY DEVELOPMENT TRAINING
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     BOOST YOUR CONFIDENCE WITH PERFECT PERSONALITY DEVELOPMENT TRAINING
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Soft skill and Personality development helps a person in many extents in corporate world.DUCAT can be easily considered as the best place to learn the nitty gritty when it comes to developing personality through SE-PD(Personality Development) .There are various courses which are being offered by DUCAT that can facilitate students/professional with perfect skills which are essential for the developing market demands and IT services. DUCAT introduced a unique methodology in its courses that helps individual to develop skills according to once individual need. They develop customer interaction, English speaking, grammar, pronunciation and more for one individual. One will feel at par with the industry and will have confidence to face any upcoming challenges. The training starts by improving a person for interview then proceeds towards further personality development to a extent that one can face any challenge that comes up to them at the time of interview. Those persons undergoing the training courses will not only survive in the corporate world but will grow with it at ease. The modules and training programs are designed in such a manner by DUCAT that it helps people in every aspect. The best faculties are chosen for the training so that amazing solutions can be provided to people in minimum cost.
     </p>
     <div class="contentAcc">
      <h2>
       SE-PD English Communication - Basic (8 Weeks) Program Fundamental of English the Kick Start
      </h2>
      <ul>
       <li>
        Practical Grammar
       </li>
       <li>
        Essence of Communication
       </li>
       <li>
        General Communication Concepts
       </li>
       <li>
        Stress of noun, verb and Adjectives
       </li>
       <li>
        Punctuation and it's usage in sentence formation
       </li>
       <li>
        Vocabulary building to encourage and communicate Effectively and Diplomatically
       </li>
      </ul>
      <h2>
       Essence of Communication the Stepping Stone
      </h2>
      <ul>
       <li>
        Self Introduction and others
       </li>
       <li>
        Public Discourse and presentation
       </li>
       <li>
        Public speaking and Role-plays
       </li>
       <li>
        Dealing with Fear and Hesitation
       </li>
       <li>
        Extempore &amp; Story Coining
       </li>
       <li>
        Knowing your audience
       </li>
       <li>
        Practice Class
       </li>
      </ul>
      <h2>
       Probing &amp; Refining Techniques the Finishing
      </h2>
      <ul>
       <li>
        Fundamentals of Interview
       </li>
       <li>
        Mock Interviews
       </li>
       <li>
        One-o-One Sessions
       </li>
       <li>
        Personalized Feedback
       </li>
      </ul>
      <h2>
       Corporate Communication &amp; Understanding Smart Communicator
      </h2>
      <ul>
       <li>
        Frequent errors in business writing
       </li>
       <li>
        Introduction to Business communication
       </li>
       <li>
        Different Stages of Communication Cycle
       </li>
       <li>
        Types of Communication( Verbal and Non Verbal)
       </li>
       <li>
        Business Letters, Email-Etiquette
       </li>
      </ul>
      <h2>
       Personality Enhancement the Leader
      </h2>
      <ul>
       <li>
        Leadership Development
       </li>
       <li>
        Team Building and Intern Personal Skills
       </li>
       <li>
        Corporate Etiquette (as in a corporate)
       </li>
       <li>
        Stress management
       </li>
       <li>
        Time Management-1 and Goal Setting
       </li>
      </ul>
      <h2>
       Group Discussions
      </h2>
      <ul>
       <li>
        Fundamentals of Group Discussion
       </li>
       <li>
        Tips to Crack G.D
       </li>
       <li>
        G.D Sessions
       </li>
       <li>
        Comprehension of G.D Graph
       </li>
      </ul>
      <h2>
       English Communication - Advance (10 Weeks) Program What Makes a Winning Personality Winning Edge?
      </h2>
      <ul>
       <li>
        Introduction to Business Writing
       </li>
       <li>
        Verbal Communication and It's Importance
       </li>
       <li>
        Role Plays and Group Dynamics
       </li>
       <li>
        Role of Communication
       </li>
       <li>
        Listening Skills
       </li>
       <li>
        Barriers and gateways of communication
       </li>
       <li>
        7'c of Communication
       </li>
       <li>
        ASK C(2)Concept `
       </li>
      </ul>
      <h2>
       Communication Enhancement and Beyond
      </h2>
      <ul>
       <li>
        Listening in public speaking
       </li>
       <li>
        Speaking on Various Topics
       </li>
       <li>
        The skill of making presentation
       </li>
       <li>
        Planning and practice of public speaking
       </li>
       <li>
        Techniques of Effective speech
       </li>
      </ul>
      <h2>
       Mantra for Success the Perfectionist
      </h2>
      <ul>
       <li>
        Negotiation skills
       </li>
       <li>
        Mock Interviews
       </li>
       <li>
        Role-plays e
       </li>
       <li>
        Self Confidence
       </li>
       <li>
        SWOT Analysis
       </li>
       <li>
        Resume writing
       </li>
      </ul>
      <h2>
       Personality Development Corporate Orientation
      </h2>
      <ul>
       <li>
        Conflict Management
       </li>
       <li>
        Cross Cultural Communication
       </li>
       <li>
        Client Handling/Sensitivity
       </li>
       <li>
        Professional etiquette as an individual
       </li>
       <li>
        Internal Communication
       </li>
       <li>
        Change Management
       </li>
       <li>
        Organizational Skills
       </li>
      </ul>
      <h2>
       PHP Syntax
      </h2>
      <ul>
       <li>
        Syntax
       </li>
       <li>
        variable
       </li>
       <li>
        Datatype
       </li>
       <li>
        Syntax
       </li>
       <li>
        variable
       </li>
       <li>
        Datatype
       </li>
      </ul>
      <h2>
       Correcting MTI's
      </h2>
      <ul>
       <li>
        What is Accent
       </li>
       <li>
        Staircase Intonations
       </li>
       <li>
        Accent Correction
       </li>
       <li>
        Phonetics
       </li>
       <li>
        Voice Modulation
       </li>
       <li>
        Accent Neutralization
       </li>
      </ul>
      <h2>
       Body Language
      </h2>
      <ul>
       <li>
        Cross Cultural
       </li>
       <li>
        Inter-Personal Body Language
       </li>
       <li>
        Know your Body Language
       </li>
       <li>
        Handling Perceptions
       </li>
       <li>
        Rapport Building
       </li>
       <li>
        Aptitude &amp; Reasoning preparation
       </li>
       <li>
        Aptitude preparation
       </li>
       <li>
        Reasoning preparation
       </li>
       <li>
        Logical reasoningc
       </li>
       <li>
        Verbal reasoning
       </li>
       <li>
        Non verbal reasoning
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="sepdtraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="6 + 86 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="92">
       <input name="url" type="hidden" value="/sepdtraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>