<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     IBM MAINFRAME
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     IBM MAINFRAME
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      SHAPE YOUR FUTURE WITH IBM MAINFRAME AT DUCAT
     </h4>
     <p>
      Shape your future with IBM MAINFRAME at DUCAT Mainframes systems are used by all large companies around the globe as it can handle vast amounts of data with exceptional computational ability. IBM was the pioneer in mainframe systems and still the global leader. The demand for qualified mainframes professionals is continuously on the rise and the supply is limited. IBM MAINFRAME Course can be taken up by anybody with or without prior knowledge or experience in IT. Knowledge of computers is the only prerequisite. This course is chosen by people of varied profiles like those who are beginning their career, changing career path, wanting to learn new technologies and stay up to date or move up the career ladder. IBM MAINFRAME course at DUCAT is comprehensive and training intensive course that helps the students gain sound knowledge on the fundamentals, principles and techniques. They would be exposed to real time work environment in the form of live projects as well. The IBM Training courseware is designed by industry veterans and taught by our talented consultants. Apart from this the students get to interact with the technology experts who would give them insights on the actual application of the concepts in the developing applications. The well-structured DUCAT's IBM MAINFRAME Training course covers most popular IBM products/languages like MVS COBOL II, JCL, VSAM, CICS, DB2, FILE-AID etc. On successful completion of this exhaustive course students would placement assistance as well.
     </p>
     <div class="contentAcc">
      <h2>
       INTRODUCTION TO Z/OS &amp; TSO /ISPF
      </h2>
      <ul>
       <li>
        Empty section. Edit page to add content here.
       </li>
      </ul>
      <h2>
       JCL (JOB CONTROL LANGUAG )
      </h2>
      <ul>
       <li>
        Basic concept of JCL
       </li>
       <li>
        JCL Statement
       </li>
       <li>
        JOB, EXEC &amp; DD with their Parameters
       </li>
       <li>
        MODAL Commands
       </li>
       <li>
        JCL UTILITIES
       </li>
       <li>
        JCL PROCEDURES
       </li>
       <li>
        GDG (Generation Data Group )
       </li>
      </ul>
      <h2>
       COBOL (APPLICATION PROGRAMMING )
      </h2>
      <ul>
       <li>
        Introduction to COBOL
       </li>
       <li>
        DIVISIONS of COBOL
       </li>
       <li>
        Identification ,Environment , Data , Procedure Division
       </li>
       <li>
        Group /Elements ITEMS
       </li>
       <li>
        Level Numbers
       </li>
       <li>
        Picture Class
       </li>
       <li>
        Editing Characters
       </li>
       <li>
        VERBS
       </li>
       <li>
        MOVE ,ADD ,SUBSCTACT , MULIPLY ,DIVIDE, COMPUTE ,MOVE CORRESPONDING , ADD CORRESPONDING , SUBSTRACT CORRESPONDING
       </li>
       <li>
        Clauses (VALUE , VALUE ALL , REDEFINES , RENAMES)
       </li>
       <li>
        STATEMENTS (ACCEPT , DISPLAY , EVALUATE , INITIALIZE , GO TO , GO TO DEPENDING ON GOBACK , EXIT PROGRAM , NEXT SENTENCE , CONTINUE , STOP RUN , PERFORM ,EXIT , CONDITIONAL statement (If �THEN ELSE ),CONDITION NAME, CONDITIONS )
       </li>
       <li>
        TABLE HANDLING(OCCURS CLAUSE , SEARCH &amp; SEARCH ALL statement )
       </li>
       <li>
        FILE HANDLING(SEQUENTIAL FILES , SORT &amp; MEREGE)
       </li>
       <li>
        REPORT GENERATION
       </li>
       <li>
        SUBROUTINES (STATIC CALL , DYNAMIC CALL )
       </li>
       <li>
        CHARACTER HANDLING(INSERT , STRING , UNSTRING , REFERENCE MODIFICATION )
       </li>
       <li>
        INTRINSIC FUNCTIONS
       </li>
      </ul>
      <h2>
       VSAM (FILE SYSTEM )
      </h2>
      <ul>
       <li>
        VSAM Fundamentals
       </li>
       <li>
        VSAM Clusters
       </li>
       <li>
        KSDS ,ESDS, RRDS
       </li>
       <li>
        Advanced Fundamental of VSAM
       </li>
       <li>
        Cl SPLIT, CA SPLIT, SPANNED RECORDS
       </li>
       <li>
        AMS (Access Methods Services )
       </li>
       <li>
        COBOL + VSAM
       </li>
      </ul>
      <h2>
       DB2 (DATABASE )
      </h2>
      <ul>
       <li>
        DB2 Fundamentals
       </li>
       <li>
        Tools
       </li>
       <li>
        SPUFI, QMF , DCLGEN
       </li>
       <li>
        SQL Queries
       </li>
       <li>
        DDL , DML ,DCL ,TCL ,AGGREGATE FUNCTIONS , NESTED QUERIES , JOINS ,UNION
       </li>
       <li>
        UNION ALL
       </li>
       <li>
        EMBEDDED SQL
       </li>
       <li>
        Basics (Pre � compilation Process ,BIND RUN ) , CURSORS ,STATIC SQL , DYNAMIC SQL
       </li>
       <li>
        ERROR HANDLING
       </li>
       <li>
        NULL INDICATOR HANDLING
       </li>
      </ul>
      <h2>
       CICS (ON LINE TRANSACTION PROCESSING SYSTEM )
      </h2>
      <ul>
       <li>
        CICS Introduction
       </li>
       <li>
        BMS (Basic Mapping Support )
       </li>
       <li>
        CICS COBOL
       </li>
       <li>
        SKIPPER /STOPPER TECHNIQUES
       </li>
       <li>
        CICS COBOL + VSAM
       </li>
       <li>
        CICS COBOL + DB2
       </li>
       <li>
        Pseudo Conversational Techniques
       </li>
       <li>
        T1,T2,T3(With Examples )
       </li>
       <li>
        Error Handling
       </li>
       <li>
        Link &amp; XCTL Commands
       </li>
       <li>
        TSQ &amp; TDQ
       </li>
       <li>
        Interval Control Commands
       </li>
       <li>
        ASKTIME , FORMATTIME, START , DELAY
       </li>
       <li>
        Function key Programing
       </li>
       <li>
       </li>
       <li>
        Hands-On Exercise
       </li>
      </ul>
      <h2>
       THE REXX
      </h2>
      <ul>
       <li>
        Introduction to REXX &amp; Why REXX
       </li>
       <li>
        Features of REXX
       </li>
       <li>
        Defining Variables
       </li>
       <li>
        SAY &amp; PULL Instruction
       </li>
       <li>
        Concatenation &amp; Abuttal
       </li>
       <li>
        Assignment &amp; Arithematic Instructions
       </li>
       <li>
        Complex Expressions
       </li>
       <li>
        Logical Comparisons
       </li>
       <li>
        Conditional Processing
       </li>
       <li>
        Instruction Looping (DO Loops (with FOR ,BY and TO )
       </li>
       <li>
        Leave , Iterate &amp; Forver Instruction
       </li>
       <li>
        The SELECT , WHEN , OTHERWISE and NOP instructions
       </li>
       <li>
        Others bits : INTERPRET and Functions
       </li>
       <li>
        FUNCTIONS (Sub � routines )
       </li>
       <li>
        Built in Functions
       </li>
       <li>
        All String Functions like CENTER , COMPARE , ABBREV OVERLAY , INSERT etc ,
       </li>
       <li>
        All Numeric &amp; Data Functions
       </li>
       <li>
        BIT Functions
       </li>
       <li>
        The SIGNAL instructions . CONDITION Function
       </li>
       <li>
        Exception Handling
       </li>
       <li>
        Parsing Tutorial
       </li>
       <li>
        Using Compound Variable and Stems
       </li>
       <li>
        Using EXECIO to Process Information to and from Data Sets
       </li>
       <li>
        Using in the Data Stack
       </li>
       <li>
        Issuing Subcommands of TSO/E Commands
       </li>
       <li>
        Datatype
       </li>
      </ul>
      <h2>
       TOOLS
      </h2>
      <ul>
       <li>
        FILE AID
       </li>
       <li>
        XPEDITOR
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="ibmmainframetraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="61 + 65 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="126">
       <input name="url" type="hidden" value="/ibmmainframetraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>