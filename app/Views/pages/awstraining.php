<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     AWS TRAINING
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     AWS
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      AWS TRAINING
     </h4>
     <p>
      In this module of AWS Architect Certification Training, you will learn about the
different projects and services of AWS. You will also understand the Global Infrastructure of AWS. The
AWS training module will also cover different types of EC2 instances and instance purchasing options.Learning objectives: In this module of AWS Architect Certification Training, you will get introduced to
the compute service offered from AWS, called EC2. You will also learn about different Amazon Machine
Image (AMI) and gain knowledge on Elastic IP vs Public IP. It also includes a demo on launching an
AWS EC2 instance, connecting to it and hosting a website on the AWS EC2 instance.
     </p>
     <div class="contentAcc">
      <h2>
       Getting Started with AWS
      </h2>
      <h2>
       Topics
      </h2>
      <ul>
       <li>
        What is Cloud Computing?
       </li>
       <li>
        AWS Certification
       </li>
       <li>
        AWS Product and Services
       </li>
       <li>
        AWS Global Infrastructure and its benefits
       </li>
       <li>
        AWS Regions
       </li>
       <li>
        EC2 and EC2 Instances
       </li>
       <li>
        EC2 Purchasing Options
       </li>
      </ul>
      <h2>
       Hands On/Demo
      </h2>
      <ul>
       <li>
        Signing up for a free tier account with AWS
       </li>
      </ul>
      <h2>
       Amazon EC2
      </h2>
      <h2>
       Topics
      </h2>
      <ul>
       <li>
        EC2 Instance
       </li>
       <li>
        Security Group
       </li>
       <li>
        AMI
       </li>
       <li>
        Create Security Group
       </li>
       <li>
        Ubuntu and Windows Instance
       </li>
      </ul>
      <h2>
       Hands On/Demo
      </h2>
      <ul>
       <li>
        Creating an Elastic IP
       </li>
      </ul>
      <h2>
       Storage Services and AWS CLI
      </h2>
      <h2>
       Topics
      </h2>
      <ul>
       <li>
        Traditional Storage Tiers
       </li>
       <li>
        Cloud Storage
       </li>
       <li>
        Type of AWS Storage &amp; Storage options
       </li>
       <li>
        AWS Storage Services
       </li>
       <li>
        EBS, EBS Volume and EBS Storage
       </li>
       <li>
        S3 (Simple Storage Service)
       </li>
       <li>
        AWS Glacier Service
       </li>
       <li>
        AWS Command Line Interface (CLI)
       </li>
      </ul>
      <h2>
       Hands On/Demo
      </h2>
      <ul>
       <li>
        Restoring an Amazon EBS Volume from a Snapshot
       </li>
       <li>
        Hosting a Website on Amazon S3
       </li>
      </ul>
      <h2>
       Virtual Private Cloud &amp; Direct Connect
      </h2>
      <h2>
       Topics
      </h2>
      <ul>
       <li>
        VPC &amp; Components of VPC
       </li>
       <li>
        Direct Connect
       </li>
       <li>
        CIDR Notations
       </li>
       <li>
        VPC Components
       </li>
       <li>
        Benefits of VPC
       </li>
      </ul>
      <h2>
       Hands On/Demo
      </h2>
      <ul>
       <li>
        Build the Non- Default VPC
       </li>
      </ul>
      <h2>
       Database Services
      </h2>
      <h2>
       Topics
      </h2>
      <ul>
       <li>
        Benefits of RDS
       </li>
       <li>
        Types of databases instances
       </li>
       <li>
        MYSQL
       </li>
       <li>
        Amazon DynamoDB
       </li>
       <li>
        ElastiCache
       </li>
       <li>
       </li>
      </ul>
      <h2>
       Hands On/Demo
      </h2>
      <ul>
       <li>
        Creating a MySQL DB Instance via Relational Database Service (RDS)
       </li>
       <li>
        To Create Memcached Cluster
       </li>
      </ul>
      <h2>
       Elastic Load Balancing &amp; Auto Scaling
      </h2>
      <h2>
       Topics
      </h2>
      <ul>
       <li>
        Load Balancer
       </li>
       <li>
        Auto Scaling
       </li>
      </ul>
      <h2>
       Hands On/Demo
      </h2>
      <ul>
       <li>
        Load Balancer
       </li>
       <li>
        Auto Scaling
       </li>
      </ul>
      <h2>
       Route 53 &amp; Management Tools
      </h2>
      <h2>
       Topics
      </h2>
      <ul>
       <li>
        Domain Name &amp; DNS Server
       </li>
       <li>
        Auto Scaling
       </li>
       <li>
        Route 53
       </li>
       <li>
        Management tools in AWS
       </li>
       <li>
        CloudTrail
       </li>
       <li>
        CloudWatch
       </li>
       <li>
        Setting up a Billing Alert
       </li>
      </ul>
      <h2>
       Hands On/Demo
      </h2>
      <ul>
       <li>
        Pay as you go
       </li>
       <li>
        Auto Scaling
       </li>
       <li>
        CloudWatch Dashboard
       </li>
      </ul>
      <h2>
       Application Services
      </h2>
      <ul>
       <li>
        Amazon Simple Email Service (SES)
       </li>
       <li>
        Amazon Simple Notification Service (SNS)
       </li>
       <li>
        Amazon Simple Queue Service (SQS)
       </li>
      </ul>
      <h2>
       Hands On/Demo
      </h2>
      <ul>
       <li>
        SNS
       </li>
       <li>
        SQS
       </li>
       <li>
        Create a Lambda Function for saving an Object in Amazon S3
       </li>
       <li>
        Launch an Application using Elastic Beanstalk
       </li>
      </ul>
      <h2>
       OpsWorks, Security &amp; Identity Services
      </h2>
      <h2>
       Topics
      </h2>
      <ul>
       <li>
        Security &amp; Identify Services
       </li>
       <li>
        Key Management Service
       </li>
      </ul>
      <h2>
       Hands On/Demo
      </h2>
      <ul>
       <li>
        Creating user and assign him different policies using IAM
       </li>
       <li>
        Enabling MFA
       </li>
      </ul>
      <h2>
       Project Discussion &amp; Mock Test
      </h2>
      <h2>
       Topics
      </h2>
      <ul>
       <li>
        Hands on workshop
       </li>
       <li>
        Q&amp;A
       </li>
       <li>
        An overview of the AWS certifications
       </li>
       <li>
        Access Control
       </li>
       <li>
        Objects
       </li>
       <li>
        Deleting Objects
       </li>
       <li>
        Restoring Objects
       </li>
       <li>
        Managing Access
       </li>
       <li>
        Protecting Data
       </li>
       <li>
        Hosting a Static Website
       </li>
       <li>
        Notification
       </li>
      </ul>
      <h2>
       Amazon Glacier
      </h2>
      <ul>
       <li>
        What Is Amazon Glacier?
       </li>
       <li>
        Getting Started
       </li>
       <li>
        Working with Archives
       </li>
       <li>
        Access Control
       </li>
      </ul>
      <h2>
       Amazon Auto Scaling
      </h2>
      <ul>
       <li>
        What is Auto Scaling?
       </li>
       <li>
        Auto Scaling Benefits
       </li>
       <li>
        How Auto Scaling Works
       </li>
       <li>
        Setting Up
       </li>
       <li>
        Planning your Auto Scaling Group
       </li>
       <li>
        Controlling Access to Your Auto Scalin Resources
       </li>
       <li>
        Creating Launch Configurations
       </li>
       <li>
        Creating Your Auto Scaling Groups
       </li>
       <li>
        Controlling instances
       </li>
       <li>
        Tagging instances
       </li>
      </ul>
      <h2>
       Amazon Elastic Load Balancing (ELB)
      </h2>
      <ul>
       <li>
        What Is Elastic Load Balancing?
       </li>
       <li>
        How Elastic Load Balancing Works
       </li>
       <li>
        Concepts
       </li>
       <li>
        Create a Basic Load Balancer
       </li>
       <li>
        Listener Configurations
       </li>
      </ul>
      <h2>
       Amazon EBS
      </h2>
      <ul>
       <li>
        Amazon EBS
       </li>
       <li>
        Features of Amazon EBS
       </li>
       <li>
        EBS Volumes
       </li>
       <li>
        EBS Snapshot
       </li>
       <li>
        EBS Encryption
       </li>
       <li>
        EBS Performance
       </li>
       <li>
        Instance Store
       </li>
       <li>
        Instance Store Usage
       </li>
       <li>
        Adding Instance Store Volumes to an AMI
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="awstraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="29 + 42 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="71">
       <input name="url" type="hidden" value="/awstraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>