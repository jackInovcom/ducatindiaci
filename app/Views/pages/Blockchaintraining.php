<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     Blockchain training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Blockchain
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      The course is designed to  introduce you to the concept of Blockchain and explain the fundamentals of  blockchain and bitcoin.The course will provide an overview of the structure and  mechanism of blockchain. As a beginner, you will be learning theimportance of  consensus in transactions, how transactions are stored on blockchain, history  of bitcoin and how to use bitcoin. Furthermore, you will be taught about the  Ethereum platform and its programming language. You will setup your own private  blockchain environment using Ethereum. Also, you will develop a smart contract  on private Ethereum blockchain and will bedeploying the contract from web and  console. The course is fed with various use-cases and examples, which makes the  learning more interesting.
     </p>
     <div class="contentAcc">
      <h2>
       Module 1
      </h2>
      <p>
       Introduction to Cryptocurrency and Networking Concepts.
      </p>
      <h2>
       Goal
      </h2>
      <p>
       In this module, you will learn the concept of cryptocurrencies and networking structure.
      </p>
      <h2>
       Objectives
      </h2>
      <p>
       At the end of this module, you should be able to:
      </p>
      <ul>
       <li>
        Explain cryptography and cryptocurrency
       </li>
       <li>
        Infer hash functions
       </li>
       <li>
        Distinguish the various network structures
       </li>
       <li>
        Explain why decentralized systems are efficient
       </li>
      </ul>
      <h2>
       Topics
      </h2>
      <ul>
       <li>
        Transformation in trading units
       </li>
       <li>
        Cryptography and Crypto-currency
       </li>
       <li>
        Anonymity and Pseudonymity in cryptocurrencies
       </li>
       <li>
        Digital Signatures
       </li>
       <li>
        Cryptocurrency Hash codes
       </li>
       <li>
        Peer to peer networks (structured and unstructured)
       </li>
      </ul>
      <h2>
       Hands On:
      </h2>
      <ul>
       <li>
        Demonstrating hash codes
       </li>
       <li>
        Create your own cryptocurrency
       </li>
      </ul>
      <h2>
       Module 2
      </h2>
      <p>
       Overview of Blockchain
      </p>
      <h2>
       Goal
      </h2>
      <p>
       In this module, you will learn blockchain technology and its architecture.
      </p>
      <h2>
       Objectives
      </h2>
      <p>
       At the end of this module, you should be able to:
      </p>
      <ul>
       <li>
        Explain blockchain and its uses
       </li>
       <li>
        Understand the structure and mechanisms of a blockchain.
       </li>
      </ul>
      <h2>
       Topics
      </h2>
      <ul>
       <li>
        Introduction to Blockchain.
       </li>
       <li>
        Why Blockchain is crucial?
       </li>
       <li>
        Key vocabulary while discussing Blockchain
       </li>
       <li>
        Distinction between databases and blockchain
       </li>
       <li>
        Explaining Distributed Ledger
       </li>
       <li>
        Blockchain ecosystem
       </li>
       <li>
        Blockchain structure
       </li>
       <li>
        Working of blockchain technology
       </li>
       <li>
        Permissioned and permission-less blockchain
       </li>
      </ul>
      <h2>
       Hands On:
      </h2>
      <ul>
       <li>
        Demonstrating valid and invalid transaction
       </li>
       <li>
        Demonstrating Blockchain
       </li>
      </ul>
      <h2>
       Module 3
      </h2>
      <ul>
       <li>
        Bitcoin and Blockchain
       </li>
      </ul>
      <h2>
       Goal
      </h2>
      <p>
       In this module, you will learn about bitcoins. You will understand why transactions with bitcoins is secure and efficient. Also, you will learn how bitcoin network works.
      </p>
      <h2>
       Objectives
      </h2>
      <p>
       At the end of this module, you should be able to:
      </p>
      <ul>
       <li>
        Explain bitcoin and its uses
       </li>
       <li>
        Setup your own bitcoin wallet
       </li>
       <li>
        Explain the working of bitcoin transaction system.
       </li>
       <li>
        Perceive the scripting language of bitcoin
       </li>
       <li>
        Deduce nodes and network of bitcoin
       </li>
       <li>
        Comprehend various roles a person can play in Bitcoin ecosystem
       </li>
      </ul>
      <h2>
       Topics
      </h2>
      <ul>
       <li>
        Bitcoin and its History
       </li>
       <li>
        Why use Bitcoins?
       </li>
       <li>
        Where and how to buy bitcoins
       </li>
       <li>
        How to store bitcoins?
       </li>
       <li>
        How and where to spend bitcoins?
       </li>
       <li>
        Selling bitcoins
       </li>
       <li>
        Bitcoin  transactions
       </li>
       <ul>
        <li>
         How bitcoin transactions work
        </li>
        <li>
         What happens in case of invalid transactions
        </li>
        <li>
         Parameters that invalidate the transactions
        </li>
       </ul>
       <li>
        Scripting language in bitcoin
       </li>
       <li>
        Applications of bitcoin script
       </li>
       <li>
        Nodes and network of bitcoin
       </li>
       <li>
        Various roles you can play in Bitcoin Ecosystem
       </li>
      </ul>
      <h2>
       Hands On:
      </h2>
      <ul>
       <li>
        Setting up bitcoin wallet
       </li>
       <li>
        Creating a paper wallet
       </li>
       <li>
        Transaction tracking of bitcoin
       </li>
      </ul>
      <h2>
       Module 4
      </h2>
      <ul>
       Bitcoin mining
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>