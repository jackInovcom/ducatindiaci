<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     BIG COMMERCE TRAINING
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     BIG COMMERCE TRAINING
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      E-Commerce Website Development in PHP &amp; MySQL with Payment Gateway Integration.
     </h4>
     <p>
      This course will enable you to build real-world, dynamic web sites. If you've built website using plain HTML, you realize the limitation of this approach. Static content from a pure HTML website is just that- static. It says the same unless you physically update it. Your users can't interact with the site in any meaningful fashion.Using PHP language and database MySQL allows you to make our sites dynamic: to have them be customization and contain real-time information. In this course We describe how to approach realworld projects and take you through design, planning and building e-commerce based website from scratch with payment gateway integration and marketing.
     </p>
     <div class="contentAcc">
      <h2>
       FRONT END DESIGNING
      </h2>
      <ul>
       <li>
        PHOTOSHOP CC
       </li>
       <li>
        HTML 5 and CSS 3
       </li>
       <li>
        RESPONSIVE WEB DESIGN
       </li>
      </ul>
      <h2>
       Back End (Development in PHP)
      </h2>
      <ul>
       <li>
        Introduction with PHP
       </li>
       <li>
        Basic Syntax and programming with PHP
       </li>
       <li>
        Control Statement or Selection
       </li>
       <li>
        Implementing Iteration or looping
       </li>
       <li>
        Introducing Array
       </li>
       <li>
        Embedded HTML with PHP
       </li>
       <li>
        Custom Functions
       </li>
       <li>
        PHP Built-in Functions
       </li>
      </ul>
      <h2>
       JavaScript, Ajax and jQuery
      </h2>
      <ul>
       <li>
        JavaScript Implementation
       </li>
       <li>
        Ajax Implementation
       </li>
       <li>
        jQuery Implementation
       </li>
       <li>
        Introducing with File system and Server
       </li>
       <li>
        State Management Concepts
       </li>
       <li>
        Object Oriented PHP
       </li>
      </ul>
      <h2>
       MySQL Database
      </h2>
      <ul>
       <li>
        Relational Database Concept and technology
       </li>
       <li>
        Web Database Design and Architecture.
       </li>
       <li>
        Creating a MySQL Database
       </li>
       <li>
        Setting up Users and Privileges
       </li>
       <li>
        Creating Database T ables
       </li>
       <li>
        Column Data Types and constraints
       </li>
       <li>
        Implementing Insert/Detate/Updae and select
       </li>
       <li>
        Query
       </li>
       <li>
        Joining T able
       </li>
       <li>
        Dropping T ables and Databases
       </li>
       <li>
        Advance My SQL Programming
       </li>
       <li>
        Working in PhpMyAdmin
       </li>
       <li>
        Database Connectivity
       </li>
       <li>
        Setting up connection with PHP and MySQL
       </li>
       <li>
        Mysqli and PDO Connectivity
       </li>
      </ul>
      <h2>
       XML , JSON and Web Services
      </h2>
      <ul>
       <li>
        The Extensible Markup Language (XML)
       </li>
       <li>
        Creating an XML Document
       </li>
       <li>
        Parsing XML Documents
       </li>
       <li>
        Loading and Saving XML Documents
       </li>
       <li>
        Interfacing with Simple XML
       </li>
       <li>
        Implementing JSON
       </li>
       <li>
        Working with json_encode and json_decode
       </li>
       <li>
        Creating web services with the help of XML or JSON.
       </li>
      </ul>
      <h2>
       Developing an E-Commerce W ebsite
      </h2>
      <ul>
       <li>
        Creating the Admin Panel
       </li>
       <li>
        Creating main layout and adding Content
       </li>
       <li>
        Creating the Shopping Cart
       </li>
       <li>
        Customer registration &amp; Login � Logout
       </li>
      </ul>
      <h2>
       Online Payment Gateway Integration in website
      </h2>
      <ul>
       <li>
        Integration with Paypal
        <br/>
       </li>
       <li>
        Integration with CCAvenue
       </li>
      </ul>
      <h2>
       Website Hosting &amp; Security
      </h2>
      <ul>
       <li>
        Upload our website online
        <br/>
       </li>
       <li>
        Give us you the Domain and web space
       </li>
       <li>
        Upload our files in a server with the help of ftp or cpanel.
       </li>
       <li>
        Working on mail accounts and sub-domains
       </li>
       <li>
        Implementing our local database to online server
       </li>
      </ul>
      <h2>
       Some Advance Php Techniques
      </h2>
      <ul>
       <li>
        PDF generation and Pagination
        <br/>
       </li>
       <li>
        Implementing SQL Injection
       </li>
      </ul>
      <h2>
       SEARCH ENGINE OPTIMIZA TION (SEO)
      </h2>
      <ul>
       <li>
        Introduction to SEO and what it involves
       </li>
       <li>
        Types of SEO
       </li>
       <li>
        White Hat SEO
       </li>
       <li>
        Black Hat SEO
       </li>
       <li>
        What are search Engines?
       </li>
       <li>
        Types of Search Engines
       </li>
      </ul>
      <h2>
       Wordpress (Content Management System)
      </h2>
      <ul>
       <li>
        Installation of W ordpress
       </li>
       <li>
        Basics of the WordPress User Interface
       </li>
       <li>
        Finding and Using W ordPress Plugins
       </li>
       <li>
        Working in widgets
       </li>
       <li>
        Working with WordPress Themes
       </li>
       <li>
        WordPress Content Management
       </li>
      </ul>
      <h2>
       CodeIgniter Framework
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        General T opics
       </li>
       <li>
        Class Reference
       </li>
       <li>
        Helper Reference
       </li>
       <li>
        Creating a website in Codeigniter
       </li>
      </ul>
      <h2>
       CakePHP Framework
      </h2>
      <ul>
       <li>
        Introduction to CakePHP
       </li>
       <li>
        Basic Principles of CakePHP
       </li>
       <li>
        CakePHP Controllers
       </li>
       <li>
        CakePHP Models
       </li>
       <li>
        CakePHP Views
       </li>
       <li>
        CakePHP Authentication
       </li>
       <li>
        CakePHP Authentication
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>