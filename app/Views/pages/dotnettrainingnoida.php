<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     ADVANCED .NET MVC
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     ADVANCED .NET
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h2>
      MICROSOFT ADVANCE .NET TRAINING
     </h2>
     <h4>
      ADVANCED .NET MVC TRAINING
     </h4>
     <p>
      DOT NET is the one of high in demand course today. There are many professionals already in the market quipped with this knowledge. But you can stand apart and above all of them by doing the course from an experienced and reputed Advanced .net training institute like DUCAT. DUCAT provides a little more , little differently to give that extra impetus to your CV. DUCAT has a training course named MICROSOFT Advanced .net Training IN 3 MONTHS. This course would make you technically, practically and fundamentally strong in this technology along with live project experience in 3months time. Learning at DUCAT is a very pleasant experience as the whole course is students get practical exposure to all the concepts, contents are well-structured to meet the industry requirements covering all the verticals and parallels in the technology, live project experience under the guidance of experts from the industries. The experienced faculties understand the varying understanding capacities of individuals and provide one to one attention to every student. DUCAT wants to ensure every student benefits out of the course. Last but not the least , a certification from DUCAT can give you a glittering edge over others.
     </p>
     <div class="contentAcc">
      <h2>
       Language Integrated Query (LINQ)
      </h2>
      <ul>
       <li>
        Introduction to LINQ
       </li>
       <ul>
        <li>
         What is LINQ
        </li>
        <li>
         Need of LINQ
        </li>
        <li>
         How LINQ Works
        </li>
        <li>
         Using Linq via Method based and Query based syntax
        </li>
       </ul>
       <li>
        Standard Linq Operators
       </li>
       <ul>
        <li>
         Sorting Data
        </li>
        <li>
         Filtering Data
        </li>
        <li>
         Joining Data
        </li>
        <li>
         Partitioning Data
        </li>
       </ul>
       <li>
        Linq Provider
       </li>
       <ul>
        <li>
         LINQ to Object
        </li>
        <li>
         LINQ to XML
        </li>
        <li>
         LINQ to SQL
        </li>
       </ul>
       <li>
        Understanding Lazy Loading and Eager Loading
       </li>
       <li>
        Difference B/w Lazy and Eager Loading
       </li>
       <li>
        Using Stored Procedure in LINQ
       </li>
       <li>
        Working with Single table Inheritance
       </li>
       <li>
        Using of SQLMetal
       </li>
      </ul>
      <h2>
       Entity Framework
      </h2>
      <ul>
       <li>
        Overview of the Entity Framework
       </li>
       <li>
        Difference b/w Linq to Sql and Entity Framework
       </li>
       <li>
        Using Schema First Approach
       </li>
       <li>
        Using Model First Aproach
       </li>
       <li>
        Using Code First Aproach
       </li>
       <li>
        Using Stored Procedure in Code First Approach
       </li>
       <li>
        Using Sql Profiler with Stored Procedure
       </li>
       <li>
        Creating/Managing one to many and many to one relations
       </li>
       <li>
        Using Table, Column and Key attributes
       </li>
       <li>
        Associations &amp; Foreign Keys
       </li>
      </ul>
      <h2>
       Software Engineering Models and Concept:
      </h2>
      <ul>
       <li>
        What is Software Development Life Cycle?
       </li>
       <li>
        Phases of a SDLC
       </li>
       <li>
        Project initiation and planning
       </li>
       <li>
        Feasibility study
       </li>
       <li>
        System design
       </li>
       <li>
        Coding
       </li>
       <li>
        Testing
       </li>
       <li>
        Implementation
       </li>
       <li>
        Maintenance
       </li>
      </ul>
      <h2>
       Business Requirement Specification (Project Based)
      </h2>
      <ul>
       <li>
        Objective of Project
       </li>
       <li>
        DFD
       </li>
       <li>
        UML Diagram
       </li>
       <li>
        Class Diagram
       </li>
       <li>
        ER Diagram
       </li>
      </ul>
      <h2>
       System Requirement Database Structure (Project Based)
      </h2>
      <ul>
       <li>
        Database Table Structure
       </li>
       <li>
        Database Design
       </li>
       <li>
        DDL
       </li>
       <li>
        DML
       </li>
       <li>
        Stored Procedure
       </li>
       <li>
        Database Security
       </li>
      </ul>
      <h2>
       WindowsCommunicationFoundation (WCF)
      </h2>
      <ul>
       <li>
        IntroductionToWCF
       </li>
       <li>
        What is Distributed Application
       </li>
       <li>
        What is WCF
       </li>
       <li>
        Evaluation of WCF
       </li>
       <li>
        Understanding WCF Architecture
       </li>
       <li>
        WCF comparison with WebServices
       </li>
       <li>
        WCF comparison with Remoting
       </li>
       <li>
        Benefits of WCF Service
       </li>
       <li>
        Understanding WCF Templates
       </li>
       <ul>
        <li>
         WCF Service Library
        </li>
        <li>
         WCF Service Application
        </li>
        <li>
         WCF Workflow Service Application
        </li>
       </ul>
      </ul>
      <h2>
       Endpoints in WCF
      </h2>
      <ul>
       <li>
        What is EndPoint in WCF
       </li>
       <li>
        End points seetings in app. Config &amp; web.config file
       </li>
       <li>
        ServiceMetaData HTTP_Get enabled
       </li>
       <li>
        IMetaDataExchange Contract/Endpoint for Http and Tcp
       </li>
       <li>
        What is Address-Binding-Contract (ABC)
       </li>
       <li>
        <strong>
         Address
        </strong>
       </li>
       <ul>
        <li>
         What is Address
        </li>
        <li>
         Understanding improtance of address
        </li>
        <li>
         Difference between HTTP &amp; HTTPS address
        </li>
        <li>
         Use of TCP and MSMQ address
        </li>
        <li>
         What is Named Pipe address
        </li>
        <li>
         Understanding IIS address
        </li>
       </ul>
       <li>
        <strong>
         Binding
        </strong>
       </li>
       <ul>
        <li>
         Understanding WCF Bindings
        </li>
        <li>
         Types of bindings
        </li>
        <li>
         Basic Binding
        </li>
        <li>
         Web Binding
        </li>
        <li>
         WS Binding
        </li>
        <li>
         WS Dual Binding
        </li>
        <li>
         TCP Binding
        </li>
        <li>
         IPC Binding
        </li>
        <li>
         MSMQ Binding
        </li>
       </ul>
       <li>
        <strong>
         Contract
         <strong>
         </strong>
        </strong>
       </li>
       <ul>
        <li>
         What is the of contract
        </li>
        <li>
         Types of Contract
        </li>
        <li>
         Service Contract
        </li>
        <li>
         Operation Contract
        </li>
        <li>
         Data Contract
        </li>
        <li>
         Fault Contract
        </li>
        <li>
         Message Contract
        </li>
       </ul>
      </ul>
      <h2>
       Developing WCF Service Application
      </h2>
      <ul>
       <li>
        Hosting WCF Service in llS/ASP.NET Development Server
       </li>
       <li>
        Hosting WCF Service in Managed Application(Self Hosting)
       </li>
       <li>
        Hosting WCF Service in Window Service
       </li>
       <li>
        WCF service durable service
       </li>
       <li>
        Use of WCF service Request-Replay
       </li>
       <li>
        Use of WCF service One-Way
       </li>
       <li>
        Use of WCF Callback service
       </li>
      </ul>
      <h2>
       Understand WCF Service instancing behavior
      </h2>
      <ul>
       <li>
        Single(Singleton)
       </li>
       <li>
        PerCall
       </li>
       <li>
        PerSession
       </li>
      </ul>
      <h2>
       Understand Concurrency in WCF Service
      </h2>
      <ul>
       <li>
        Single
       </li>
       <li>
        Multiple
       </li>
       <li>
        Reentrant
       </li>
      </ul>
      <h2>
       Restful WCF Service
      </h2>
      <ul>
       <li>
        Understand Restful service in WCF
       </li>
       <li>
        Use of Restful service
       </li>
       <li>
        How to make Restful service in WCF
       </li>
       <li>
        Use of XML/JSON in Restful service
       </li>
      </ul>
      <h2>
       Implementing Security in WCF Service
      </h2>
      <ul>
       <li>
        Understanding security terms
       </li>
       <li>
        Authentication
       </li>
       <li>
        Authorization
       </li>
       <li>
        Confidentiality
       </li>
       <li>
        Integrity
       </li>
      </ul>
      <h2>
       Security
      </h2>
      <ul>
       <li>
        Message security
       </li>
       <li>
        Transport security
       </li>
       <li>
        TransportWithMessage security
       </li>
      </ul>
      <h2>
       ASP.NET MVC
      </h2>
      <ul>
       <li>
        Introduction to ASP.NET MVC
       </li>
       <li>
        Brief history of ASP.NET MVC
       </li>
       <li>
        Understanding ASP.NET MVC Design Pattern
       </li>
       <li>
        Setup &amp; Installation process
       </li>
       <li>
        Compare ASP.Net Web Forms with ASP.NET MVC
       </li>
       <li>
        Life cycle of ASP.NET MVC
       </li>
       <li>
        Key Benefits of ASP.NET MVC
       </li>
       <li>
        Understanding ASP.NET MVC project templates
       </li>
      </ul>
      <h2>
       View Engine
      </h2>
      <ul>
       <li>
        What is View Engine
       </li>
       <li>
        Role of View Engine
       </li>
       <li>
        Types of View Engine
       </li>
       <li>
        Compare ASP View &amp; Razor View Engine
       </li>
       <li>
        Understanding Razor syntax
       </li>
       <li>
        Accessing model data in Razor views
       </li>
       <li>
        Working with Razor Layouts
       </li>
       <li>
        Understanding Razor Expressions
       </li>
      </ul>
      <h2>
       Models
      </h2>
      <ul>
       <li>
        What is MVC
       </li>
       <li>
        Understanding scaffolding template
       </li>
       <li>
        Scaffolding template with Entity Framework
       </li>
       <li>
        Defining a data model
       </li>
       <li>
        Understanding Model Binding
       </li>
       <li>
        Working with explicit model binding
       </li>
       <li>
        Responding to the edit post request
       </li>
      </ul>
      <h2>
       Views
      </h2>
      <ul>
       <li>
        Purpose of Views
       </li>
       <li>
        Understanding View Conventions
       </li>
       <li>
        Types of View
       </li>
       <li>
        Understanding ViewBag, TempData,ViewData and ViewDataDictionary
       </li>
       <li>
        Creating strongly-typed views
       </li>
       <li>
        Adding a View
       </li>
       <li>
        Using Razor syntax in View
       </li>
       <li>
        Code Block in View
       </li>
       <li>
        Working with Partial View
       </li>
       <li>
        Introduction of Layout/Master page
       </li>
       <li>
        Working with Layout
       </li>
      </ul>
      <h2>
       Controllers and Actions
      </h2>
      <ul>
       <li>
        What is the Controller
       </li>
       <li>
        Use of Controller
       </li>
       <li>
        What is Action and Action Results
       </li>
       <li>
        Action Filters
       </li>
       <li>
        Authorize Filters
       </li>
       <li>
        Result Filters
       </li>
       <li>
        Built In Filters
       </li>
       <li>
        Processing Input and Output Action
       </li>
      </ul>
      <h2>
       Membership, Authorization And Security
      </h2>
      <ul>
       <li>
        Session and application state
       </li>
       <li>
        Using the Authorize Attribute to Login
       </li>
       <li>
        Securing Controller Action
       </li>
       <li>
        Authorize Attribute with Forms Authentication
       </li>
       <li>
        Windows Authentication
       </li>
       <li>
        Using Role wise Login
       </li>
       <li>
        Managing users and roles
       </li>
       <li>
        Configuring OAuth And OpenID Providers
       </li>
       <li>
        Login via OAuth and OpenID
       </li>
       <li>
        Security Implications of External Logins
       </li>
      </ul>
      <h2>
       URL Routing
      </h2>
      <ul>
       <li>
        Introducing URL Routing
       </li>
       <li>
        Routing Approaches
       </li>
       <li>
        Defining Attribute and Traditional Routes
       </li>
       <li>
        Understanding Route Parameters
       </li>
       <li>
        A Details look at URL Generation
       </li>
       <li>
        Creating and Registering a Simple Route
       </li>
       <li>
        Ambient Route Values
       </li>
       <li>
        Using Static URL Segments
       </li>
       <li>
        Constraining Routes
       </li>
       <li>
        Bypassing the Routing System
       </li>
      </ul>
      <h2>
       Forms And Helper Methods
      </h2>
      <ul>
       <li>
        Using Form Helper Methods
       </li>
       <li>
        The Action and the Methods
       </li>
       <li>
        Using GET or POST Method
       </li>
       <li>
        Using Template Helper Methods
       </li>
       <li>
        Using Model Metadata
       </li>
       <li>
        Making Helpers Do your Bidding Inside HTML Helpers
       </li>
       <li>
        Setting up the Edit Forms
       </li>
       <li>
        Using Strongly Typed Helpers
       </li>
       <li>
        Helpers and Model Metadata
       </li>
       <li>
        Helpers and Model State
       </li>
       <li>
        Html.Hidden
       </li>
       <li>
        Html.Password
       </li>
       <li>
        Html. RadioButton
       </li>
       <li>
        Html.CheckBox
       </li>
       <li>
        Html.DropDownList
       </li>
       <li>
        Html.Partial and Html.RenderPartial
       </li>
       <li>
        Html.Action and Html.RenderAction
       </li>
      </ul>
      <h2>
       Data Annotations And Validation
      </h2>
      <ul>
       <li>
        Using Validation Annotations
       </li>
       <li>
        Custom Error Messages and Localization
       </li>
       <li>
        Using Range validator Attribute
       </li>
       <li>
        Regular Expression validator Attribute
       </li>
       <li>
        Required validator Attribute
       </li>
       <li>
        String Length validator Attribute
       </li>
       <li>
        Custom Validation and Annotation
       </li>
       <li>
        Using display format
       </li>
      </ul>
      <h2>
       Additional Techniques
      </h2>
      <ul>
       <li>
        View scaffold templates
       </li>
       <li>
        Model Validation
       </li>
       <li>
        Global filters
       </li>
       <li>
        Model binding
       </li>
       <li>
        Bundling and Display Modes
       </li>
      </ul>
      <h2>
       Using Ajax, jQuery with ASP.NET MVC
      </h2>
      <ul>
       <li>
        Overview of JQuery
       </li>
       <li>
        Unobtrusive Ajax and the 'this' context
       </li>
       <li>
        Using Ajax action links
       </li>
       <li>
        Using Ajax Helpers
       </li>
       <li>
        Autocomplete with jQuery UI
       </li>
       <li>
        JSON &amp; Client-Side Template
       </li>
       <li>
        Bootstrap Plug-in
       </li>
      </ul>
      <h2>
       Using the ASP.NET Web API
      </h2>
      <ul>
       <li>
        Overview of the ASP.NET Web API
       </li>
       <li>
        Building servers and clients
       </li>
       <li>
        Adding routes to your web API
       </li>
       <li>
        Incoming Action Parameters
       </li>
       <li>
        Validation
       </li>
       <li>
        Understanding IHttpController
       </li>
       <li>
        Enabling Dependency Injection
       </li>
       <li>
        Session Management in Web API
       </li>
       <li>
        Securing a Web API
       </li>
       <li>
        Single and Multiple Registered Services in API
       </li>
      </ul>
      <h2>
       Angular
      </h2>
      <ul>
       <li>
        Introduction to Angular
       </li>
       <li>
        Angular 4 Architecture
       </li>
       <li>
        Setup installation
       </li>
       <li>
        Building Blocks of Angular2 Application
       </li>
       <li>
        Modules
       </li>
       <li>
        Components input and output properties
       </li>
       <li>
        Templates
       </li>
       <li>
        Views
       </li>
       <li>
        Directives
       </li>
       <li>
        Dependency Injection
       </li>
       <li>
        Implementing Web API service in Angular
       </li>
       <li>
        Validation of Angular Form
       </li>
       <li>
        Single Page Application and Routing
       </li>
       <li>
        Asynchronous Calls
       </li>
       <li>
        Angular router navigate method
       </li>
       <li>
        Internationalization
       </li>
       <li>
        Angular Event Binding
       </li>
       <li>
        Angular component lifecycle hooks
       </li>
      </ul>
      <h2>
       Technical Design &amp; Development (Project Based)
      </h2>
      <ul>
       <li>
        Working with Project
       </li>
       <li>
        Programming Language: C# (MVC Razor)
       </li>
       <li>
        Designing Tools
       </li>
       <li>
        CSHTML
       </li>
       <li>
        Using CSS
       </li>
       <li>
        Using Ajax
       </li>
       <li>
        Using JavaScript
       </li>
      </ul>
      <h2>
       WPF
      </h2>
      <ul>
       <li>
        Introduction to WPF
       </li>
       <li>
        Understanding WPF Controls
       </li>
       <li>
        Understanding XAML
       </li>
       <li>
        Creating Custom Controls
       </li>
       <li>
        Using Panels/Layout
       </li>
       <ul>
        <li>
         Introduction to WPF Layout
        </li>
        <li>
         StackPanel
        </li>
        <li>
         Grid Panel
        </li>
        <li>
         Dock Panel
        </li>
        <li>
         Canvas Panel
        </li>
        <li>
         Border
        </li>
        <li>
         Wrap Panel
        </li>
       </ul>
       <li>
        Data Binding
       </li>
       <ul>
        <li>
         Types of Data Binding
        </li>
        <li>
         Debug DataBinding Issues
        </li>
        <li>
         Data Validation Rules
        </li>
        <li>
         Value Converters
        </li>
        <li>
         Data Viewing, Sorting and Filtering
        </li>
       </ul>
       <li>
        Using Triggers
       </li>
       <li>
        Styling
       </li>
       <ul>
        <li>
         Themes
        </li>
        <li>
         Styles
        </li>
       </ul>
       <li>
        2D &amp; 3D Graphics
       </li>
       <ul>
        <li>
         Draw exactly on physical device pixels
        </li>
        <li>
         Geometry Transformer
        </li>
        <li>
         How to get a Bitmap from a Visual
        </li>
        <li>
         DrawRoundedRectangle
        </li>
       </ul>
       <li>
        Multithreading
       </li>
       <li>
        Working with Slides &amp; Animations
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="advancedotnetmvc.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="79 + 39 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="118">
       <input name="url" type="hidden" value="/dotnettrainingnoida/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>