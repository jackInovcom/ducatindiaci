<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     Tally ERP 9 GST Training In Noida
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     TALLY ERP 9 GST
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Ducat is one of the best training institute in Delhi Ncr for Tally ERP 9 with GST. We not only provide training on Tally ERP 9, we also provide practical training on Goods &amp; Services Tax (GST), All Income Tax Returns (ITR 1 to ITR 7), Tax Audit, MIS Reporting, TDS Returns, Firm Registrations, ROC Returns, Balance Sheet Finalization, Adv MS Excel by Experienced Chartered Accountants &amp; Industry Experts.
     </p>
     <div class="contentAcc">
      <h2>
       Day1
      </h2>
      <ul>
       <li>
        Revision of Basic Accounting Concepts and Rules of Accounting
       </li>
       <li>
        Practice of Journal Entry by an example
       </li>
       <li>
        Start Tally
       </li>
       <li>
        Company Info Menu
       </li>
       <li>
        How to select company?
       </li>
       <li>
        How to Shut Company ?
       </li>
       <li>
        How to Create Company?
       </li>
       <li>
        How to alter company and its details?
       </li>
       <li>
        How to give Tally password and alter it.
       </li>
       <li>
        Types of Voucher Entry in Tally and Its Function keys
       </li>
       <li>
        Use of Masters-How to Create Ledgers ?
       </li>
       <li>
        Groups of ledgers
       </li>
       <li>
        Account info- How to enter Voucher entries
       </li>
      </ul>
      <h2>
       Day2
      </h2>
      <ul>
       <li>
        Accounting Voucher - Purchase voucher entry , Receipt voucher entry, Journal voucher entry
Contra voucher entry, Sale Voucher entry, Payment Voucher entry
       </li>
       <li>
        Display of Reports �
       </li>
       <ul>
        <li>
         Accounting reports-
        </li>
        <li>
         Trial Balance
        </li>
        <li>
         Profit &amp; Loss
        </li>
        <li>
         Balance Sheet
        </li>
        <li>
         Day book
        </li>
       </ul>
       <li>
        How to activate Functions in accounts info Menu ?
       </li>
       <li>
        How to Create Group Company ?
       </li>
       <li>
        How to apply Security Control ?
       </li>
       <li>
        How to Split company data ?
       </li>
       <li>
        How to take Backup ?
       </li>
       <li>
        How to restore Backup ?
       </li>
       <li>
        How to use Tally Audit Feature?
       </li>
      </ul>
      <h2>
       Day3
      </h2>
      <ul>
       <li>
        Create Masters for Inventory to keep stock details
       </li>
       <li>
        How to set up stock Groups ?
       </li>
       <li>
        How to create Stock Items ?
       </li>
       <li>
        How to create Units of measurement ?
       </li>
       <li>
        Types of Inventory Entry- Invoice and voucher
       </li>
       <li>
        Create Purchase and Sale entry with Inventory details
       </li>
       <li>
        Inventory Reports-
       </li>
       <ul>
        <li>
         Stock Summary
        </li>
        <li>
         Sales Register
        </li>
        <li>
         Purchase Register
        </li>
        <li>
         Stock query report
        </li>
       </ul>
      </ul>
      <h2>
       Day 4
      </h2>
      <ul>
       <li>
        F-11 Features
       </li>
       <li>
        How to use Accounting features
       </li>
       <li>
        How to use Inventory features
       </li>
       <li>
        Start with Inventory features first-
       </li>
       <ul>
        <li>
         Stock category-Category wise report
        </li>
        <li>
         Use of multiple godowns
        </li>
        <li>
         Stock transfer entry between godowns
        </li>
        <li>
         Physical stock entry
        </li>
        <li>
         Separate Actual and Billed quantity
        </li>
        <li>
         Allow zero valued entry
        </li>
       </ul>
      </ul>
      <h2>
       Day5
      </h2>
      <ul>
       <li>
        Integrate Accounts and Inventory
       </li>
       <li>
        Batch-wise Details
       </li>
       <li>
        Manufacturing and expiry date
       </li>
       <li>
        Aging analysis Report to see expired batch
       </li>
       <li>
        Types of Discount- Trade and Cash
       </li>
      </ul>
      <h2>
       Day6
      </h2>
      <ul>
       <li>
        Debit note voucher entry
       </li>
       <li>
        Credit note voucher entry
       </li>
       <li>
        Order Voucher- Purchase order entry and Sales order entry
       </li>
       <li>
        Sale/Purchase order outstanding
       </li>
       <li>
        Reorder Status
       </li>
       <li>
        Invoice
       </li>
      </ul>
      <h2>
       Day7
      </h2>
      <ul>
       <li>
        Purchase Management-
       </li>
       <li>
        Track Additional Cost of Purchase
       </li>
       <li>
        Movement Analysis Report
       </li>
       <li>
        Sales Management-
       </li>
       <li>
        Multiple price level
       </li>
      </ul>
      <h2>
       Day8
      </h2>
      <ul>
       <li>
        Use of Tracking Number
       </li>
       <li>
        Receipt note entry
       </li>
       <li>
        Delivery note entry
       </li>
       <li>
        Rejection out entry
       </li>
       <li>
        Rejection in entry
       </li>
       <li>
        Purchase / Sales Bill pending
       </li>
      </ul>
      <h2>
       Day9
      </h2>
      <ul>
       <li>
        Manufacturing Journal entry
       </li>
       <li>
        BOM- Bill of Material
       </li>
       <li>
        POS- Point of Sale entry
       </li>
      </ul>
      <h2>
       Day10
      </h2>
      <ul>
       <li>
        Accounting Features
       </li>
       <li>
        Outstanding Management
       </li>
       <li>
        Bill wise details- use of Invoice number
       </li>
       <li>
        Bills Receivable and Payable Report
       </li>
       <li>
        How to set credit limits
       </li>
       <li>
        Interest calculation
       </li>
      </ul>
      <h2>
       Day11
      </h2>
      <ul>
       <li>
        Use of Multiple currency
       </li>
       <li>
        Forex Gain/loss Adjustment
       </li>
       <li>
        How to create cost categories &amp; Cost centres
       </li>
      </ul>
      <h2>
       Day12
      </h2>
      <ul>
       <li>
        How to configure check printing
       </li>
       <li>
        How to do Bank reconciliation
       </li>
       <li>
        Maintain Budget and controls
       </li>
      </ul>
      <h2>
       Day13
      </h2>
      <ul>
       <li>
        Maintain reversing journal and optional vouchers
       </li>
       <li>
        Memorandum and postdated vouchers
       </li>
       <li>
        Maintain multiple mailing details and company logo etc.
       </li>
      </ul>
      <h2>
       Day14
      </h2>
      <ul>
       <li>
        How to Enabling payroll in Tally
       </li>
       <li>
        How to generate pay slip
       </li>
       <li>
        How to create Payroll Masters
       </li>
       <li>
        How to Pass Payroll Voucher Entry
       </li>
       <li>
        How to view Statements of Payroll
       </li>
       <li>
        How to view Attendance Reports
       </li>
       <li>
        How to view Payroll Statutory Reports
       </li>
       <li>
        Payroll Statutory Computation
       </li>
      </ul>
      <h2>
       Day15
      </h2>
      <ul>
       <li>
        Employees Provident Fund(EPF)
       </li>
       <li>
        Reports Employee State Insurance(ESI)
       </li>
       <li>
        Overtime
       </li>
       <li>
        Gratuity
       </li>
       <li>
        Reimbursement to employees
       </li>
       <li>
        Loan and advance etc.
       </li>
       <li>
        Payroll Reports- Pay Slips, Salary Register ,PF Challan, ESI Challan etc.
       </li>
      </ul>
      <h2>
       Day16
      </h2>
      <ul>
       <li>
        F12- Configuration Menu
       </li>
       <li>
        How to fill up country detail
       </li>
       <li>
        How to change styles of dates
       </li>
       <li>
        How to use other options
       </li>
       <li>
        How to set financial Year
       </li>
       <li>
        How to use general configuration and Numeric symbols
       </li>
       <li>
        Printing configuration
       </li>
       <li>
        Email configuration
       </li>
       <li>
        Data configuration
       </li>
       <li>
        How to copy or paste Tally file
       </li>
      </ul>
      <h2>
       Day17
      </h2>
      <ul>
       <li>
        F11-Taxation features
       </li>
       <li>
        What is TDS?
       </li>
       <li>
        How to prepare and issue of TDS certificate
       </li>
       <li>
        How to do filling E-TDS return
       </li>
       <li>
        What is nature of TDS related payments?
       </li>
       <li>
        Deductee Types
       </li>
       <li>
        How to create TDS masters
       </li>
       <li>
        How to Pass TDS voucher entries and Transactions
       </li>
       <li>
        How to do pass TDS on expenses (General entries)
       </li>
       <li>
        How to do accounting multiple expenses &amp; deducting letter
       </li>
       <li>
        How to do TDS on advance payments
       </li>
       <li>
        How to do adjusting advances against the bill
       </li>
       <li>
        How to do changes in TDS percentage computation
       </li>
       <li>
        TDS Challan Reconciliation
       </li>
       <li>
        TDS outstanding
       </li>
       <li>
        Form 26, Annexure to 26 ,Form 27
       </li>
      </ul>
      <h2>
       Day18
      </h2>
      <ul>
       <li>
        What is GST
       </li>
       <li>
        SGST/ CGST/ IGST
       </li>
       <li>
        How to register for GST
       </li>
       <li>
        When to pay Tax
       </li>
       <li>
        ITC
       </li>
       <li>
        E-way bill
       </li>
       <li>
        Who needs to File GST Return ?
       </li>
      </ul>
      <h2>
       Day19
      </h2>
      <ul>
       <li>
        GST Account Creation
       </li>
       <li>
        Voucher Entry of GST with different rates
       </li>
       <li>
        HSN Code classifications
       </li>
       <li>
        GST Tax Classification
       </li>
       <li>
        Various classifications of Goods &amp; services
       </li>
       <li>
        Payments of GST
       </li>
       <li>
        Filling of Challan GST
       </li>
       <li>
        GST Return Like GST R1 , GST R2 , GST R3
       </li>
      </ul>
      <h2>
       Day20
      </h2>
      <ul>
       <li>
        What is TCS
       </li>
       <li>
        Form 27EQ
       </li>
       <li>
        TCS outstanding
       </li>
       <li>
        Challan reconciliation
       </li>
       <li>
        Excise
       </li>
      </ul>
      <h2>
       Day21
      </h2>
      <ul>
       <li>
        Utilities
       </li>
       <li>
        Import of data
       </li>
       <li>
        Banking
       </li>
       <li>
        Side buttons when see reports
       </li>
       <li>
        Revision of all syllabus
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="TALLYERP9GSTtraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="0 + 83 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="83">
       <input name="url" type="hidden" value="/besttallyerp9Training/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>