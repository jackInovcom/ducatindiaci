<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     Selenium with Python Training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Selenium with Python
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h2>
      Selenium with Python Training
     </h2>
     <h4>
      About the Course
     </h4>
     <p>
      The purpose of the Selenium with Python training program is to help quality specialists who are saddled with the responsibility of doing tasks that ought to be automated. Tasks such as searching a web page or assessing the quality of a product can be easily automated using the powerful
      <strong>
       Selenium in conjunction with the Python language.
      </strong>
     </p>
     <div class="contentAcc">
      <h2>
       Setup and Configuration
      </h2>
      <ul>
       <li>
        Python Installation in Windows
       </li>
       <li>
        Configuration of python in window
       </li>
       <li>
        Package Management using PIP
       </li>
       <li>
        IDE Option for Python Development
       </li>
       <li>
        Installing ipython
       </li>
      </ul>
      <h2>
       Understanding Variable and Data Types
      </h2>
      <ul>
       <li>
        python Terminal Walkthrough
       </li>
       <li>
        Understanding Objects and References
       </li>
       <li>
        Variables Rules
       </li>
       <li>
        Number Datatypes and Math Operation
       </li>
       <li>
        Number - Exponentiations and Modulo
       </li>
       <li>
        Arithmetic order of precedence
       </li>
       <li>
        Boolean Data Types
       </li>
       <li>
        Working with Strings
       </li>
       <li>
        String Method Part - 1
       </li>
       <li>
        String Method Part - 2
       </li>
       <li>
        More String Slicing and Indexing
       </li>
       <li>
        String Formatting
       </li>
      </ul>
      <h2>
       Advanced Datatypes
      </h2>
      <ul>
       <li>
        List and Accessing the Element
       </li>
       <li>
        List Methods
       </li>
       <li>
        Working With Dictionary
       </li>
       <li>
        Nested Dictionary
       </li>
       <li>
        Dictionary Methods
       </li>
       <li>
        Working with tuple
       </li>
      </ul>
      <h2>
       Comparison and Boolean Operators
      </h2>
      <ul>
       <li>
        Working with comparators
       </li>
       <li>
        Understanding Boolean Operators
       </li>
       <li>
        Boolean Operator and Order of Precedence
       </li>
      </ul>
      <h2>
       Program Control Flow
      </h2>
      <ul>
       <li>
        Conditional Logic - If Else Condition
       </li>
       <li>
        While Loop Demo
       </li>
       <li>
        Break Continue and While / Else
       </li>
       <li>
        For Loop Demo
       </li>
       <li>
        Iterating Multiple List - Using the zip function
       </li>
       <li>
        Using Range function in for loop
       </li>
      </ul>
      <h2>
       Function/Method-Working with reusable code
      </h2>
      <ul>
       <li>
        Understanding Method
       </li>
       <li>
        Working with return value
       </li>
       <li>
        Working with positional / optional parameter
       </li>
       <li>
        Understanding Variable Scope
       </li>
       <li>
        More Built in Function
       </li>
      </ul>
      <h2>
       Classes Object Oriented Programming
      </h2>
      <ul>
       <li>
        Understanding Object and Class
       </li>
       <li>
        Create your Own Object
       </li>
       <li>
        Create your own Methods
       </li>
       <li>
        Inheritance
       </li>
       <li>
        Method Overriding
       </li>
      </ul>
      <h2>
       Exception Handling
      </h2>
      <ul>
       <li>
        Exception Handling Demo
       </li>
       <li>
        finally, and Else Block
       </li>
      </ul>
      <h2>
       Modules
      </h2>
      <ul>
       <li>
        Built in Modules
       </li>
       <li>
        Create Own Module
       </li>
      </ul>
      <h2>
       Working with files
      </h2>
      <ul>
       <li>
        How to write data into a file
       </li>
       <li>
        How to read a File
       </li>
       <li>
        File Handling "With" and "As" Keyword
       </li>
      </ul>
      <h2>
       How to Inspect Elements Different Browsers - Add-Ons
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        How To Inspect Elements Using Firefox DevTools
       </li>
       <li>
        Firefox Add-On - Try XPath
       </li>
       <li>
        How To Inspect Elements Using Chrome DevTools
       </li>
       <li>
        Chrome Extension - Part 1
       </li>
       <li>
        Chrome Extension - Part 2
       </li>
       <li>
        Tricks To Generate XPath
       </li>
       <li>
        Fire Path Fans -&gt; If you want to use Fire Path
       </li>
      </ul>
      <h2>
       Selenium WebDriver -&gt; Setup and Installation
      </h2>
      <ul>
       <li>
        Selenium WebDriver Installation - Windows
       </li>
       <li>
        Selenium 3.x Update
       </li>
       <li>
        Selenium WebDriver 3.x Gecko Driver Setup - Windows
       </li>
      </ul>
      <h2>
       Selenium WebDriver -&gt; Running Tests on Various Browsers
      </h2>
      <ul>
       <li>
        Running Tests on Firefox
       </li>
       <li>
        Running Tests on Google Chrome - Windows
       </li>
       <li>
        Requirements to Run Tests On IE
       </li>
       <li>
        Running Tests on Internet Explorer
       </li>
       <li>
        Requirements to Run Tests On Safari
       </li>
       <li>
        Running Tests on Safari
       </li>
      </ul>
      <h2>
       Selenium WebDriver -&gt; Finding Elements
      </h2>
      <ul>
       <li>
        Understanding Elements And DOM
       </li>
       <li>
        Find Element by Id and Name
       </li>
       <li>
        Understanding Dynamic Ids and Selenium Exception
       </li>
       <li>
        Find Element by XPath And CSS Selectors
       </li>
       <li>
        Find Element by Link Text
       </li>
       <li>
        Find Element by Class Name and Tag Name
       </li>
       <li>
        Understanding "By" Class
       </li>
       <li>
        How to Find List of Elements
       </li>
      </ul>
      <h2>
       CSS Selectors - Advanced Locators
      </h2>
      <ul>
       <li>
        Using Ids With CSS Selectors To Find Elements
       </li>
       <li>
        How To Use Multiple CSS Classes To Find Elements
       </li>
       <li>
        Using Wildcards With CSS Selectors
       </li>
       <li>
        How To Find Child Nodes Using CSS Selectors
       </li>
      </ul>
      <h2>
       Xpath - Advanced Locators
      </h2>
      <ul>
       <li>
        Difference Between Absolute And Relative Xpath
       </li>
       <li>
        How to Build An Effective Xpath
       </li>
       <li>
        Using Text To Build An Effective Xpath
       </li>
       <li>
        Build Xpath Using Contains Keyword
       </li>
       <li>
        Build Xpath Using Starts-With Keyword
       </li>
       <li>
        How To Find Parent and Sibling Nodes
       </li>
      </ul>
      <h2>
       Selenium WebDriver -&gt; Working with Web Elements
      </h2>
      <ul>
       <li>
        Browser Interactions Introduction
       </li>
       <li>
        Browser Interaction
       </li>
       <li>
        How To Click And Type On A Web Element
       </li>
       <li>
        How To Find The State Of A Web Element (Disabled And Enabled Elements)
       </li>
       <li>
        Radio Buttons And Checkboxes
       </li>
       <li>
        Working With Elements List
       </li>
       <li>
        Understanding Dropdown Elements
       </li>
       <li>
        Working With A Dropdown Element
       </li>
       <li>
        How To Work With Hidden Elements
       </li>
       <li>
        Working With Hidden Elements
       </li>
      </ul>
      <h2>
       Selenium WebDriver -&gt; Useful Methods and Properties
      </h2>
      <ul>
       <li>
        How To Get The Text On Element
       </li>
       <li>
        How To Get Value Of Element Attribute
       </li>
       <li>
        Generic Method To Find Elements
       </li>
       <li>
        How To Check If Element Is Present
       </li>
       <li>
        How To Build Dynamic XPath
       </li>
      </ul>
      <h2>
       Selenium WebDriver -&gt; Wait Types
      </h2>
      <ul>
       <li>
        Implicit Wait Vs Explicit Wait
       </li>
       <li>
        Implicit Wait
       </li>
       <li>
        Explicit Wait
       </li>
      </ul>
      <h2>
       Selenium WebDriver -&gt; Advanced
      </h2>
      <ul>
       <li>
        Calendar Selection Introduction
       </li>
       <li>
        Calendar Selection
       </li>
       <li>
        AutoComplete Introduction
       </li>
       <li>
        AutoComplete
       </li>
       <li>
        How To Take Screenshots
       </li>
       <li>
        Generic Method To Take Screenshots
       </li>
       <li>
        Executing JavaScript Commands
       </li>
       <li>
        How To Find Size Of The Window
       </li>
       <li>
        How To Scroll Element Into View
       </li>
      </ul>
      <h2>
       Selenium WebDriver -&gt; Switch Window and Iframes
      </h2>
      <ul>
       <li>
        How To Switch Window Focus
       </li>
       <li>
        Switch To Window
       </li>
       <li>
        How To Work With IFrames
       </li>
       <li>
        Switch ToIFrame
       </li>
       <li>
        Handling JavaScript Popup
       </li>
      </ul>
      <h2>
       Selenium WebDriver -&gt; Working with Actions Class
      </h2>
      <ul>
       <li>
        Mouse Hover Actions
       </li>
       <li>
        How to Drag And Drop Element On A Web Page
       </li>
       <li>
        Working with Sliders Actions
       </li>
      </ul>
      <h2>
       Logging Infrastructure
      </h2>
      <ul>
       <li>
        Introduction To Logging Infrastructure
       </li>
       <li>
        Changing The Format Of Logs
       </li>
       <li>
        Logger - Console Example
       </li>
       <li>
        Logger - Configuration File Example
       </li>
       <li>
        How to Write A Generic Custom Logger Utility
       </li>
      </ul>
      <h2>
       Unit test Infrastructure
      </h2>
      <ul>
       <li>
        Unittest Introduction
       </li>
       <li>
        Writing First Test Case
       </li>
       <li>
        How to Implement Class Level SetupandTeardown Methods
       </li>
       <li>
        How to Assert A Test Method
       </li>
       <li>
        How to Run Code from Terminal
       </li>
       <li>
        How to Create A Test Suite
       </li>
      </ul>
      <h2>
       Pytest -&gt; Advanced Testing Framework
      </h2>
      <ul>
       <li>
        Pytest Installation and First Script
       </li>
       <li>
        Pytest Naming Conventions
       </li>
       <li>
        How to Work withPyTest Fixtures
       </li>
       <li>
        Pytest Fixture Update
       </li>
       <li>
        Multiple Ways to Run Test Cases
       </li>
       <li>
        Important Note for Next Lecture
       </li>
       <li>
        Conftest -&gt; Common Fixtures to Multiple Modules
       </li>
       <li>
        How to Maintain Run Order of Tests
       </li>
       <li>
        Running Tests Based on Command Line Arguments
       </li>
       <li>
        Structure Tests in A Test Class
       </li>
       <li>
        How to Return A Value from Fixtures
       </li>
       <li>
        Install PyTest HTML Plugin
       </li>
       <li>
        How to Generate HTML Test Report
       </li>
      </ul>
      <h2>
       Automation Framework - Part 1
      </h2>
      <ul>
       <li>
        Automation Framework Introduction
       </li>
       <li>
        Understanding Framework Structure
       </li>
       <li>
        Test Scenario Without Framework
       </li>
       <li>
        Important Note for Next Lectures - Windows
       </li>
       <li>
        Convert Test Case to Page Object Model Framework
       </li>
       <li>
        Refactor Your Page Object Class - Part 1
       </li>
       <li>
        Build Your Custom Selenium Driver Class
       </li>
       <li>
        Refactor Your Page Object Class - Part 2
       </li>
      </ul>
      <h2>
       Automation Framework - Part 2
      </h2>
      <ul>
       <li>
        Add Logging to Automation Framework
       </li>
       <li>
        How to Verify Test Case Result
       </li>
       <li>
        Complete Login Page Test Cases
       </li>
       <li>
        Create Conftest To Implement Common Setup Methods
       </li>
       <li>
        Refactor Conftest *** Make Framework More Readable ***
       </li>
      </ul>
      <h2>
       Automation Framework - Part 3
      </h2>
      <ul>
       <li>
        How To Assert Without Stopping Test Execution
       </li>
       <li>
        How To Assert Without Stopping Test Execution Part - 1
       </li>
       <li>
        How To Assert Without Stopping Test Execution Part - 2
       </li>
       <li>
        Implement Screenshots In A Framework
       </li>
       <li>
        Taking Screenshots On Test Failure
       </li>
       <li>
        BasePage And Util Concept Introduction
       </li>
       <li>
        Inheriting BasePage Class
       </li>
      </ul>
      <h2>
       Data Driven Testing
      </h2>
      <ul>
       <li>
        Setup And Configuration
       </li>
       <li>
        Data Driven Testing
       </li>
       <li>
        Utility To Read CSV Data
       </li>
       <li>
        Multiple Data Sets Test Case
       </li>
       <li>
        Running Complete Test Suite
       </li>
      </ul>
      <h2>
       Running Complete Test Suite
      </h2>
      <ul>
       <li>
        How To Manage Navigation In Framework
       </li>
       <li>
        Refactor Login Tests
       </li>
       <li>
        How To Run A Test Suite
       </li>
       <li>
        Running Test Suite On Chrome
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="seleniumwithpython.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="28 + 44 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="72">
       <input name="url" type="hidden" value="/seleniumwithpythontraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>