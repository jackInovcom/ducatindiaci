<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     Best Load Runner Training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     RPA
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      This LoadRunner Course Is made to train People in a Training format how to plan and load test an enterprise web application. Our Goal of this Load Runner Training is to make you understand the complete load testing life cycle. Live projects and Lab Facilities are available for the students. You will learn to develop a workable strategy for load testing of an enterprise learning management system.

Get Enroll in Load Runner Training Class &amp; Upgrade your Skills &amp; productive for your career.
     </p>
     <div class="contentAcc">
      <h2>
       Load Runner Installation
      </h2>
      <ul>
       <li>
        Load Runner architecture
       </li>
       <li>
        Where to install Load Runner components
       </li>
       <li>
        Identify hardware and software needed for installation
       </li>
       <li>
        Installing Load runner samples.
       </li>
      </ul>
      <h2>
       VUGEN
      </h2>
      <h2>
       Introduction to VUSER Concept:
      </h2>
      <ul>
       <li>
        Definition of Vuser
       </li>
       <li>
        Why VUGEN is Used
       </li>
       <li>
        Features of VUGEN
       </li>
       <li>
        VUSER TYPES
       </li>
      </ul>
      <h2>
       Streamlining Recording:
      </h2>
      <ul>
       <li>
        Settings to be done before recording
       </li>
       <li>
        Modes of recording
       </li>
       <li>
        Choosing the right protocol
       </li>
       <li>
        Types of protocol load runner supports
       </li>
       <li>
        Single and Multiple protocols
       </li>
       <li>
        When and how to use more than one Action
       </li>
       <li>
        Deleting Action
       </li>
      </ul>
      <h2>
       First touch to VUGEN:
      </h2>
      <ul>
       <li>
        Starting VUGEN
       </li>
       <li>
        Understanding VUGEN Environment Options
       </li>
       <li>
        Choosing the right protocol
       </li>
      </ul>
      <h2>
       Recording Script using VUGEN:
      </h2>
      <ul>
       <li>
        VUSER Script sections (Init, Action and end)
       </li>
       <li>
        Creating New Virtual VUSER script
       </li>
       <li>
        Adding and removing protocols
       </li>
       <li>
        Choosing New Virtual user category
       </li>
       <li>
        Begin Recording on your application
       </li>
       <li>
        Ending and Saving a recording session
       </li>
       <li>
        Running the Created Script.
       </li>
      </ul>
      <h2>
       Introduction to Output window in VUGEN:
      </h2>
      <ul>
       <li>
        Replay Log
       </li>
       <li>
        Recording Log
       </li>
       <li>
        Generation Log
       </li>
       <li>
        Correlation Results
       </li>
      </ul>
      <h2>
       Understanding the VUSER script:
      </h2>
      <ul>
       <li>
        Viewing and modifying VUSER scripts
       </li>
       <li>
        Understanding the functions generated in the code
       </li>
       <li>
        Getting Help on functions
       </li>
       <li>
        Workflow Wizard
       </li>
       <li>
        Walkthrough on few Load runner functions
       </li>
       <li>
        Recap of Steps for creating Vuser Scripts
       </li>
      </ul>
      <h2>
       Actions in depth:
      </h2>
      <ul>
       <li>
        When and how to use more than one Action
       </li>
       <li>
        Deleting Actions
       </li>
       <li>
        Rename actions
       </li>
       <li>
        Import Action into Vuser
       </li>
      </ul>
      <h2>
       Introduction to VUGEN parameters:
      </h2>
      <ul>
       <li>
        Definition of parameter
       </li>
       <li>
        Why parameterization is required
       </li>
       <li>
        Parameters Limitations
       </li>
       <li>
        Creating Parameters
       </li>
       <li>
        Types of parameters
       </li>
       <li>
        Using Existing Parameters
       </li>
       <li>
        Using Parameter List
       </li>
       <li>
        Parameterization options
       </li>
      </ul>
      <h2>
       File and table type parameters:
      </h2>
      <ul>
       <li>
        Creating data files
       </li>
       <li>
        Properties of file type parameters
       </li>
       <li>
        properties of table type parameters
       </li>
      </ul>
      <h2>
       Setting Parameters properties:
      </h2>
      <ul>
       <li>
        Introduction to setting parameter properties
       </li>
       <li>
        Setting properties for internal data parameter types
       </li>
       <li>
        Setting properties for user defined functions
       </li>
       <li>
        Choosing parameter formats
       </li>
      </ul>
      <h2>
       Data Assignment methods:
      </h2>
      <ul>
       <li>
        Sequential
       </li>
       <li>
        Random
       </li>
       <li>
        Unique
       </li>
       <li>
        Data Update Modes:
       </li>
       <li>
        Each Occurrence
       </li>
       <li>
        Each Iteration
       </li>
       <li>
        Once
       </li>
      </ul>
      <h2>
       Configuring Run time settings:
      </h2>
      <ul>
       <li>
        About Run time settings
       </li>
       <li>
        Configuring run time settings
       </li>
       <li>
        pacing run time settings
       </li>
       <li>
        Configuring Log run time settings
       </li>
       <li>
        Configuring Think time settings.
       </li>
      </ul>
      <h2>
       Correlations
      </h2>
      <h2>
       Introduction to correlations.
      </h2>
      <ul>
       <li>
        Auto Correlation
       </li>
       <li>
        Manual Correlation
       </li>
      </ul>
      <h2>
       Introduction to Transactions:
      </h2>
      <ul>
       <li>
        What are transactions
       </li>
       <li>
        Why we need transactions
       </li>
       <li>
        How to insert transactions in the script
       </li>
      </ul>
      <h2>
       Introduction to content check:
      </h2>
      <ul>
       <li>
        Why we need Content checks
       </li>
       <li>
        How to use the inbuilt content checks
       </li>
       <li>
        How to build your own content checks
       </li>
      </ul>
      <h2>
       Controller
      </h2>
      <h2>
       Introduction to controller:
      </h2>
      <ul>
       <li>
        Opening controller
       </li>
       <li>
        Why we need controller
       </li>
       <li>
        Significance of controller
       </li>
       <li>
        Features on controller.
       </li>
      </ul>
      <h2>
       Load runner Testing Process:
      </h2>
      <ul>
       <li>
        Planning the test
       </li>
       <li>
        Creating Vuser Script
       </li>
       <li>
        Creating the scenario
       </li>
       <li>
        Running the scenario
       </li>
       <li>
        Monitoring the scenario
       </li>
       <li>
        Analyzing the scenario
       </li>
      </ul>
      <h2>
       Designing a scenario:
      </h2>
      <ul>
       <li>
        Creating a scenario
       </li>
       <li>
        Run scenario with sample Vuser load.
       </li>
      </ul>
      <h2>
       Scenario types:
      </h2>
      <ul>
       <li>
        Manual Scenario
       </li>
       <li>
        Goal Oriented scenario
       </li>
      </ul>
      <h2>
       Introduction to Load generators:
      </h2>
      <ul>
       <li>
        What is load generator
       </li>
       <li>
        Why we need load generators
       </li>
      </ul>
      <h2>
       Kinds of Scheduling Scenarios:
      </h2>
      <ul>
       <li>
        Schedule by scenario
       </li>
       <li>
        Schedule by group
       </li>
      </ul>
      <h2>
       Scheduling scenarios:
      </h2>
      <ul>
       <li>
        About Scheduling scenarios
       </li>
       <li>
        Delaying the start of scenarios
       </li>
       <li>
        Scheduling a scenario
       </li>
       <li>
        Scheduling Vuser groups.
       </li>
       <li>
        Adding Vuser to Scheduled scenarios.
       </li>
      </ul>
      <h2>
       Creating and running a manual scenario:
      </h2>
      <ul>
       <li>
        Creating Vuser groups
       </li>
       <li>
        Configuring Vusers in Vuser group.
       </li>
       <li>
        Configuring Vuser run time settings
       </li>
       <li>
        Configuring scripts
       </li>
       <li>
        Setting time out intervals
       </li>
       <li>
        Configuring Scenario run time settings
       </li>
       <li>
        Setting the run time file location
       </li>
       <li>
        Specifying results location
       </li>
       <li>
        Collating results
       </li>
      </ul>
      <h2>
       Running scenarios and controlling Vusers:
      </h2>
      <ul>
       <li>
        Running entire scenario
       </li>
       <li>
        Controlling Vuser groups
       </li>
       <li>
        Controlling individual Vusers
       </li>
       <li>
        Manually adding Vuser to running scenario
       </li>
       <li>
        Manually adding Vuser to Rendezvous
       </li>
      </ul>
      <h2>
       Viewing Vusers during executions:
      </h2>
      <ul>
       <li>
        About viewing vusers during executions
       </li>
       <li>
        Monitoring Vuser status
       </li>
       <li>
        Viewing the output window
       </li>
       <li>
        Viewing Vuser script log
       </li>
       <li>
        Logging execution notes
       </li>
      </ul>
      <h2>
       Creating Goal Oriented scenario:
      </h2>
      <ul>
       <li>
        About Planning goal oriented scenario
       </li>
       <li>
        Understanding goal oriented scenario design tab
       </li>
       <li>
        Defining scenario goals
       </li>
       <li>
        Assigning properties to scripts
       </li>
       <li>
        Configuring scripts
       </li>
      </ul>
      <h2>
       Monitoring and Analysis
      </h2>
      <h2>
       Online Monitoring:
      </h2>
      <ul>
       <li>
        About Online Monitoring
       </li>
       <li>
        Setting up the Monitoring environment
       </li>
       <li>
        Monitor types
       </li>
       <li>
        Choosing Monitors and measurements in the controller
       </li>
       <li>
        Starting the monitors in the controller
       </li>
       <li>
        Opening online monitor graphs in the controller
       </li>
       <li>
        Setting monitor options
       </li>
      </ul>
      <h2>
       Analysis
      </h2>
      <h2>
       Introduction to Analysis:
      </h2>
      <ul>
       <li>
        Over view on analysis
       </li>
      </ul>
      <h2>
       Interview Preparation :
      </h2>
      <ul>
       <li>
        Technical Interview Preparation
       </li>
       <li>
        Mock interview Preparation
       </li>
       <li>
        HR Session
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="loadrunner.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="89 + 59 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="148">
       <input name="url" type="hidden" value="/Loadrunnertraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>