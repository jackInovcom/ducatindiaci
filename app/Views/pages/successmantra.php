<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     Success Mantra training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Success Mantra
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Ducat Provides Best Success Mantra Training in Noida. DUCAT introduced a unique methodology in its courses that helps individual to develop skills according to once individual need.The training starts by improving a person for interview then proceeds towards further personality development to a extent that one can face any challenge that comes up to them at the time of interview. Those persons undergoing the training courses will not only survive in the corporate world but will grow with it at ease. The modules and training programs are designed in such a manner by DUCAT that it helps people in every aspect. The best faculties are chosen for the training so that amazing solutions can be provided to people in minimum cost.
     </p>
     <div class="contentAcc">
      <h2>
       PROGRAMMING &amp; PROBLEM SOLVING
      </h2>
      <h2>
       Introduction
      </h2>
      <ul>
       <li>
        Declaration &amp; Initialization
       </li>
       <li>
        Floating Point Issues, Control Instructions,
       </li>
       <li>
        Structure, Union Enum, Functions,
       </li>
       <li>
        C Pre Processors, Pointers, Arrays, Strings,
       </li>
       <li>
        Expressions, Input/Output, Extern,
       </li>
       <li>
        Bitwise Operators, Typedef, Const, Static,
       </li>
       <li>
        Memory Allocation, Command Line Augments
       </li>
       <li>
        Variable No. of Arguments, Complicated Declarations
       </li>
       <li>
        C PROGRAMMING
       </li>
       <li>
        Introduction, main() function
       </li>
       <li>
        if statement, if and else
       </li>
       <li>
        if, else if and else, switch statments
       </li>
      </ul>
      <h2>
       Loops
      </h2>
      <ul>
       <li>
        while loop
       </li>
       <li>
        do-while loop
       </li>
       <li>
        for loop
       </li>
       <li>
        break
       </li>
       <li>
        continue
       </li>
      </ul>
      <h2>
       Strings
      </h2>
      <ul>
       <li>
        String Basics
       </li>
       <li>
        Program to find the Length of the String using builtin function
       </li>
       <li>
        Program to find the length of the String with out using built in function
       </li>
       <li>
        Program to find the length of the String using while loop
       </li>
       <li>
        Program to find the length of the String using do-while loop
       </li>
       <li>
        Program to find the length of the String using for loop
       </li>
       <li>
        Program to copy one String into another string
       </li>
       <li>
        Program to reverse a String using global variables
       </li>
       <li>
        Program to reverse a String using local variables
       </li>
       <li>
        Program to compare two String without using builtin functions
       </li>
       <li>
        Program to check whether string is palindrome or not
       </li>
       <li>
        Program to concatenate two strings
       </li>
       <li>
        Program to convert a string from lower case to upper case
       </li>
       <li>
        Program to print the ASCII equivalent of alphabets in a string
       </li>
       <li>
        Program to count the number of Vowels,Consonants and special symbols in a string
       </li>
       <li>
        Program to replace a Vowel with a special symbol
       </li>
      </ul>
      <h2>
       Pointers
      </h2>
      <ul>
       <li>
        Pointer basics, Reading different types of pointer
       </li>
       <li>
        Pointer creation, Pointer usage, Pointer to Strings
       </li>
       <li>
        Program to copy one string to another using pointer
       </li>
       <li>
        Parameter Passing Techniques in C
       </li>
       <li>
        Pass by value, Pass by reference
       </li>
       <li>
        Memory allocation techniques
       </li>
       <li>
        Static memory allocation
       </li>
       <li>
        Dynamic memory allocation
       </li>
      </ul>
      <h2>
       TECHNICAL APTITUDE
      </h2>
      <h2>
       DATA STRUCTURES
      </h2>
      <h2>
       INTRODUCTION
      </h2>
      <ul>
       <li>
        Array Basics, Stack Basics
       </li>
       <li>
        Difference between Arrays and Stacks
       </li>
       <li>
        Program to Push,Pop and Display operation on Stacks
       </li>
       <li>
        Queue basics
       </li>
       <li>
        Program to Insert an element at rear of Queue
       </li>
       <li>
        Program to Delete an element at the beginning of Queue
       </li>
       <li>
        Program to display elements in Queue
       </li>
       <li>
        Circular Queue
       </li>
       <li>
        Program to Insert an element in a circular queue
       </li>
       <li>
        Program to delete an element in a circular queue
       </li>
       <li>
        Program to display elements in a circular queue
       </li>
       <li>
        Program to insert, delete and display elements in a circular queue using pointers
       </li>
       <li>
        Structures
       </li>
       <li>
        Double Ended Queue
       </li>
       <li>
        Polish Notation
       </li>
      </ul>
      <h2>
       Singly Linked Lists
      </h2>
      <ul>
       <li>
        Singly LinkedLists
       </li>
       <li>
        Basics of singly LinkedList
       </li>
       <li>
        Program to insert an element at front in singly linked list
       </li>
       <li>
        Program to insert an element at rear in singly linked list
       </li>
       <li>
        Program to delete an element from front in singly linked list
       </li>
       <li>
        Program to delete an element from rear in singly linked list
       </li>
       <li>
        Program to display elements in a singly linked list
       </li>
       <li>
        Program to insert an element at use is specific position
       </li>
       <li>
        Program to delete an element from user's specific position
       </li>
       <li>
        Sort linked list
       </li>
       <li>
        Reverse link list
       </li>
      </ul>
      <h2>
       Doubly Linked Lists
      </h2>
      <ul>
       <li>
        Basics of Doubly LinkedList
       </li>
       <li>
        Program to insert an element at front in doubly linked list
       </li>
       <li>
        Program to insert an element at rear in doubly linked list
       </li>
       <li>
        Program to delete an element from front in doubly linked list
       </li>
       <li>
        Program to delete an element from rear in doubly linked list
       </li>
       <li>
        Program to display elements in a doubly linked list
       </li>
       <li>
        Program to insert an element at use is specific position
       </li>
       <li>
        Program to delete an element from user's specific position
       </li>
      </ul>
      <h2>
       Circular Linked Lists
      </h2>
      <ul>
       <li>
        Circular Linked Lists, Basics of Circular LinkedList
       </li>
       <li>
        Program to insert an element at front in Circular linked list
       </li>
       <li>
        Program to insert an element at rear in Circular linked list
       </li>
       <li>
        Program to delete an element from front in Circular linked list
       </li>
       <li>
        Program to delete an element from rear in Circular linked list
       </li>
       <li>
        Program to display elements in a Circular linked list
       </li>
       <li>
        Program to insert an element at use is specific position
       </li>
       <li>
        Program to delete an element from user's specific position
       </li>
      </ul>
      <h2>
       Stacks and queues
      </h2>
      <ul>
       <li>
        Program to concatenate two linked lists
       </li>
       <li>
        Program to count the number of nodes in the linked list
       </li>
       <li>
        Program to add the data nodes of in the linked list
       </li>
       <li>
        Program to reverse a linked list
       </li>
       <li>
        Program to search an element in a linked list
       </li>
       <li>
        Program to delete a node in the linked list
       </li>
       <li>
        Program to insert an element towards the left of data node in linked list
       </li>
       <li>
        Program to insert an element towards the right of data node in linked list
       </li>
       <li>
        Program to find minimum element in the linked list
       </li>
       <li>
        Program to find maximum element in the linked list
       </li>
       <li>
        Program to count the number of even and odd number of elements in a linked list
       </li>
      </ul>
      <h2>
       Static and Dynamic memory Location
      </h2>
      <ul>
       <li>
        Program to compare two linked lists
       </li>
       <li>
        Program to delete duplicate element in a linked list
       </li>
      </ul>
      <h2>
       Tree Basics
      </h2>
      <ul>
       <li>
        Pre order traversal, Post order traversal, In order traversal
       </li>
       <li>
        Program to create a Binary Search Tree
       </li>
       <li>
        Program to find a minimum element in a Binary Search Tree
       </li>
       <li>
        Program to find a maximum element in a Binary Search Tree
       </li>
       <li>
        Binary Search Tree
       </li>
       <li>
        Threaded Binary Search Tree
       </li>
       <li>
        Heep Tree
       </li>
      </ul>
      <h2>
       SQL
      </h2>
      <h2>
       Introduction to SQL
      </h2>
      <ul>
       <li>
        Categorize the different types of SQL statements
       </li>
       <li>
        Software Installation
       </li>
       <li>
        Database Creation
       </li>
       <li>
        Log on to the database.
       </li>
      </ul>
      <h2>
       Retrieve Data using the SQL SELECT Statement
      </h2>
      <ul>
       <li>
        Select Table Data With All Columns
       </li>
       <li>
        Select Table Data With Specific Columns
       </li>
       <li>
        Use Arithmetic Operators
       </li>
       <li>
        Use Concatenation Operators
       </li>
       <li>
        Use Column Alias
       </li>
       <li>
        DESCRIBE command to display the table structure
       </li>
       <li>
        Use of Distinct
       </li>
      </ul>
      <h2>
       Learn to Restrict and Sort Data
      </h2>
      <ul>
       <li>
        Use WHERE clause to limit the output retrieved
       </li>
       <li>
        Between , In() , Is Null and Like
       </li>
       <li>
        ORDER BY clause to sort Data
       </li>
      </ul>
      <h2>
       Use of DDL Statements to Create and Manage Tables
      </h2>
      <ul>
       <li>
        Create a simple table
       </li>
       <li>
        Alter and Truncate Table
       </li>
       <li>
        Drop Table and Concept of Recyclebin
       </li>
      </ul>
      <h2>
       Data Manipulation Statements
      </h2>
      <ul>
       <li>
        Describe each DML statement
       </li>
       <li>
        Insert rows into a table
       </li>
       <li>
        Change rows in a table by the UPDATE statement
       </li>
       <li>
        Delete rows from a table with the DELETE statement
       </li>
       <li>
        Save and discard changes with the COMMIT and ROLLBACK statements
       </li>
      </ul>
      <h2>
       Constraints
      </h2>
      <ul>
       <li>
        Stop Entry of Invalid Data Through CONSTRAINTS
       </li>
       <li>
        Add Primary Key , Foreign Key
       </li>
       <li>
        Not Null , Unique and Check Constraints
       </li>
      </ul>
      <h2>
       Usage of Functions to Customize Output
      </h2>
      <ul>
       <li>
        Differences between single row and multiple row functions
       </li>
       <li>
        Manipulate strings with character function in the SELECT and WHERE clauses
       </li>
       <li>
        Manipulate numbers with the ROUND, TRUNC, and MOD functions
       </li>
       <li>
        Manipulate dates with the DATE functions
       </li>
       <li>
        ROWNUM and ROWID
       </li>
      </ul>
      <h2>
       Invoke Conversion Functions and Conditional Expressions
      </h2>
      <ul>
       <li>
        Describe implicit and explicit data type conversion
       </li>
       <li>
        Use the TO_CHAR, TO_NUMBER, and TO_DATE conversion functions
       </li>
       <li>
        Nest multiple functions
       </li>
       <li>
        Use of Case Expression
       </li>
      </ul>
      <h2>
       Aggregate Data Using the Group Function
      </h2>
      <ul>
       <li>
        Use the aggregation functions to produce meaningful reports
       </li>
       <li>
        Divide the retrieved data in groups by using the GROUP BY clause
       </li>
       <li>
        Exclude groups of data by using the HAVING clause
       </li>
      </ul>
      <h2>
       Display Data From Multiple Tables Using Joins
      </h2>
      <ul>
       <li>
        Write SELECT statements to access data from more than one table
       </li>
       <li>
        View data that does not meet join condition by using outer joins
       </li>
       <li>
        Join a table to itself by using a self-join
       </li>
      </ul>
      <h2>
       Use Sub-queries to Solve Queries
      </h2>
      <ul>
       <li>
        Describe the types of problem that sub-queries can solve
       </li>
       <li>
        Define sub-queries
       </li>
       <li>
        Write single-row and multiple-row sub-queries
       </li>
       <li>
        Use of Exists
       </li>
      </ul>
      <h2>
       Other Schema Objects
      </h2>
      <ul>
       <li>
        Create a View
       </li>
       <li>
        Retrieve data from views
       </li>
       <li>
        Create, maintain, and use sequences
       </li>
       <li>
        Create and maintain indexes
       </li>
       <li>
        Create synonyms
       </li>
      </ul>
      <h2>
       Control User Access
      </h2>
      <ul>
       <li>
        Create Users
       </li>
       <li>
        System Privileges &amp; Object Privileges
       </li>
       <li>
        Granting Privileges
       </li>
       <li>
        Manage Privileges Through Role
       </li>
       <li>
        Revoke Object Privileges
       </li>
      </ul>
      <h2>
       REASONING &amp; QUANTITATIVE APTITUDE
      </h2>
      <h2>
       Quantitative Ability Test
      </h2>
      <ul>
       <li>
        Basic Mathematics
       </li>
       <li>
        Divisibility
       </li>
       <li>
        HCF and LCM
       </li>
       <li>
        Numbers, decimal fractions and power
       </li>
      </ul>
      <h2>
       Applied Mathematics
      </h2>
      <ul>
       <li>
        Profit and Loss
       </li>
       <li>
        Simple and Compound Interest
       </li>
       <li>
        Time, Speed and Distance
       </li>
       <li>
        Inverse
       </li>
      </ul>
      <h2>
       Engineering Mathematics
      </h2>
      <ul>
       <li>
        Logarithms
       </li>
       <li>
        Permutation and Combinations
       </li>
       <li>
        Probability
       </li>
      </ul>
      <h2>
       Logical Ability Test
      </h2>
      <ul>
       <li>
        Deductive Reasoning
       </li>
       <li>
        Coding deductive logic
       </li>
       <li>
        Data Sufficiency, Directional Sense,Logical Word Sequence
       </li>
       <li>
        Objective Reasoning
       </li>
       <li>
        Selection decision tables
       </li>
       <li>
        Puzzles
       </li>
      </ul>
      <h2>
       Inductive reasoning
      </h2>
      <ul>
       <li>
        Coding pattern and Number series pattern recognition
       </li>
       <li>
        Analogy and Classification pattern recognition
       </li>
      </ul>
      <h2>
       Abductive Reasoning
      </h2>
      <ul>
       <li>
        Logical word sequence
       </li>
       <li>
        Data sufficiency
       </li>
      </ul>
      <h2>
       SOFT SKILLS
      </h2>
      <ul>
       <li>
        Group discussion
       </li>
       <li>
        Resume Writing
       </li>
       <li>
        HR Interview
       </li>
       <li>
        Vocabulary
       </li>
       <li>
        Synonyms
       </li>
       <li>
        Antonyms
       </li>
       <li>
        Contextual Vocabulary
       </li>
       <li>
        Grammar
       </li>
       <li>
        Error Identification
       </li>
       <li>
        Sentence Improvement and Construction
       </li>
       <li>
        Comprehension
       </li>
       <li>
        Reading Comprehension
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="successmantra.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="37 + 20 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="57">
       <input name="url" type="hidden" value="/successmantra/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>