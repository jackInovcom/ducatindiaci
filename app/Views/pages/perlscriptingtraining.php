<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Perl Training
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Perl Training
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      Perl training BY DUCAT
     </h4>
     <p>
      Ducat offers a Live Perl training course that focuses on fundamental concepts of the Perl programming. Perl scripting training is intended for individuals who want to understand how to fully use the features of the Perl programming language in Web application development, database interaction and system administration. These specialized Web language skills such as writing CGI scripts can help you build a career in Web development and technologies. Our perl scripting training centers are equipped with lab facilities and excellent infrastructure. We also provide perl scripting certification training path for our students in delhi NCR.
     </p>
     <div class="contentAcc">
      <h2>
       Perl Training Overview
      </h2>
      <ul>
       <li>
        Perl training teaches Perl developers the skills they need to fully utilize Perl's object-oriented
         features, interact with databases, use Perl/Tk, extend Perl with C/C++, and write their own Perl
       modules.
       </li>
      </ul>
      <h2>
       Perl Training Objectives
      </h2>
      <ul>
       <li>
        Debug Perl applications using a variety of tools
       </li>
       <li>
        Manipulate lists
       </li>
       <li>
        Create packages
       </li>
       <li>
        Utilize Perl's object-oriented features and write their own classes
       </li>
       <li>
        Install and use Perl modules
       </li>
       <li>
        Integrate Perl scripts with databases using the DBI module
       </li>
       <li>
        Explore the fundamentals of Perl/Tk programming
       </li>
       <li>
        Extend Perl with C/C++
       </li>
       <li>
        Develop their own Perl modules
       </li>
      </ul>
      <h2>
       Perl Training Content Debugging
      </h2>
      <ul>
       <li>
        Warnings
       </li>
       <li>
        Diagnostic Messages
       </li>
       <li>
        Carping, Confessing, and Croaking
       </li>
       <li>
        Strict Checks
       </li>
       <li>
        Compiler Pragmas
       </li>
       <li>
        Debugging Flags
       </li>
       <li>
        Your Perl Configuration
       </li>
       <li>
        The Devel::Peek Module
       </li>
       <li>
        The Data::Dumper Module
       </li>
      </ul>
      <h2>
       Expert List Manipulation
      </h2>
      <ul>
       <li>
        The grep Operator
       </li>
       <li>
        Lists, Arrays, and List Operators
       </li>
       <li>
        Context
       </li>
       <li>
        Context and Subroutines
       </li>
       <li>
        Initializing Arrays and Hashes
       </li>
       <li>
        Reference Syntax
       </li>
       <li>
        Auto-vivification
       </li>
       <li>
        Defined Values
       </li>
       <li>
        Other List Operators
       </li>
       <li>
        Usage of map, grep, and foreach
       </li>
       <li>
       </li>
      </ul>
      <h2>
       Blocks and Code References
      </h2>
      <ul>
       <li>
        Blocks
       </li>
       <li>
        Subroutines
       </li>
       <li>
        Subroutine Prototypes
       </li>
       <li>
        Code Refs and Anonymous Subroutines
       </li>
       <li>
        Typeglobbing for the Non-Squeamish
       </li>
       <li>
        Local (Dynamic) Variables
       </li>
       <li>
        Lexical Variables
       </li>
       <li>
        Persistent Private Subroutine Variables
       </li>
       <li>
        Closures
       </li>
       <li>
        The eval Operator
       </li>
       <li>
        The Block Form of eval
       </li>
       <li>
        The String Form of eval
       </li>
       <li>
        Block Form of eval for Exception Handling
       </li>
      </ul>
      <h2>
       Packages
      </h2>
      <ul>
       <li>
        Review of Packages
       </li>
       <li>
        BEGIN and END Blocks
       </li>
       <li>
        Symbol Tables
       </li>
       <li>
        Package Variables
       </li>
       <li>
        Calling Package Subroutines
       </li>
       <li>
        Importing Package Symbols
       </li>
       <li>
        Exporting Package Symbols
       </li>
       <li>
        Using the Exporter Package
       </li>
       <li>
        The use Function
       </li>
       <li>
        AUTOLOAD and @ISA
       </li>
       <li>
        AutoLoader and SelfLoader
       </li>
      </ul>
      <h2>
       Objects and Classes
      </h2>
      <ul>
       <li>
        Object-Oriented Stuff
       </li>
       <li>
        Making Perl Object-Oriented
       </li>
       <li>
        References
       </li>
       <li>
        The bless Function
       </li>
       <li>
        So, What's a Blessed Thing Good For?
       </li>
       <li>
        Calling Class and Object Methods
       </li>
       <li>
        Object Methods
       </li>
       <li>
        Writing Classes
       </li>
       <li>
        Constructors
       </li>
       <li>
        Inheritance
       </li>
       <li>
        What Perl Doesn't Do
       </li>
       <li>
       </li>
      </ul>
      <h2>
       Tied Variables
      </h2>
      <ul>
       <li>
        Why Use tie?
       </li>
       <li>
        Tying a Scalar
       </li>
       <li>
        Inside Tied Variables
       </li>
       <li>
        untie
       </li>
       <li>
        Tying an Array
       </li>
       <li>
        Tying Hashes
       </li>
       <li>
        Tie::Hash and Tie::Array
       </li>
       <li>
        Tying Filehandles
       </li>
       <li>
        What Are DBM, NDBM, GDBM, SDBM, etc?
       </li>
       <li>
        Using the DBM Modules
       </li>
       <li>
        Installing and Using Perl Modules
       </li>
       <li>
        Laziness, Impatience, and Hubris
       </li>
       <li>
        CPAN
       </li>
       <li>
        Using Modules
       </li>
      </ul>
      <h2>
       Installing a Perl Module
      </h2>
      <ul>
       <li>
        Unpacking the Module Source
       </li>
       <li>
        The Configuration Step
       </li>
       <li>
        The Build Step
       </li>
       <li>
        The Test Step
       </li>
       <li>
        The Install Step
       </li>
       <li>
        Using CPAN.pm
       </li>
       <li>
        Using Module Documentation
       </li>
      </ul>
      <h2>
       Introduction to DBI/DBD
      </h2>
      <ul>
       <li>
        The Old Way - DBPerls
       </li>
       <li>
        A Better Way - DBI/DBD
       </li>
       <li>
        Database Programming
       </li>
       <li>
        Handles
       </li>
       <li>
        Connecting to the Database
       </li>
       <li>
        Creating a SQL Query
       </li>
       <li>
        Getting the Results
       </li>
       <li>
        Updating Database Data
       </li>
       <li>
        Transaction Management
       </li>
       <li>
        Finishing Up
       </li>
      </ul>
      <h2>
       DBI/DBD SQL Programming
      </h2>
      <ul>
       <li>
        Error Checking in DBI
       </li>
       <li>
        Getting Connected
       </li>
       <li>
        Drivers
       </li>
       <li>
        Using Parameterized Statements
       </li>
       <li>
        Statement Handle Attributes
       </li>
       <li>
        Other Handle Attributes
       </li>
       <li>
        Column Binding
       </li>
       <li>
        The do Method
       </li>
       <li>
        BLOBs and LONGs and Such
       </li>
       <li>
        Installing DBI Drivers
       </li>
      </ul>
      <h2>
       Introduction to Perl/Tk
      </h2>
      <ul>
       <li>
        Tcl, Tk, Tcl/Tk, Tkperl, Perl/Tk, etc.
       </li>
       <li>
        Perl/Tk
       </li>
       <li>
        Creating a Perl/Tk Application
       </li>
       <li>
        GUI Programming Overview
       </li>
       <li>
        Adding Widgets
       </li>
       <li>
        Scrolled Widgets
       </li>
       <li>
        Configuring Widgets
       </li>
       <li>
        Menus
       </li>
       <li>
        More Fun with Menus
       </li>
       <li>
        Using FileSelect
       </li>
       <li>
       </li>
      </ul>
      <h2>
       Perl/Tk Programming
      </h2>
      <ul>
       <li>
        Tk::Error and Tk::ErrorDialog
       </li>
       <li>
        Configuring Widgets
       </li>
       <li>
        Geometry Management
       </li>
       <li>
        Geometry Management with grid()
       </li>
       <li>
        The Frame Widget
       </li>
       <li>
        Defining Widget Callbacks
       </li>
       <li>
        Bindings
       </li>
       <li>
        Nonblocking I/O with fileevent()
       </li>
       <li>
        Tags
       </li>
       <li>
        Other Widgets
       </li>
       <li>
        Other Tk Commands
       </li>
       <li>
        Getting Tk
       </li>
       <li>
        Extending Perl with C/C++
       </li>
      </ul>
      <h2>
       Perl/Tk Programming
      </h2>
      <ul>
       <li>
        Overview of Perl5 XSUBs
       </li>
       <li>
        Get Started with h2xs
       </li>
       <li>
        Set up the Perl Wrapper Class
       </li>
       <li>
        Write the XS Code
       </li>
       <li>
        The XS File
       </li>
       <li>
        Write Some Test Code
       </li>
       <li>
        What Do You Want?
       </li>
       <li>
        Returning Values on the Stack
       </li>
       <li>
        A Walk Through an XSUB
       </li>
       <li>
        Arguments to XSUBs
       </li>
       <li>
        Other h2xs Options
       </li>
       <li>
        Embedding the Perl Interpreter
       </li>
       <li>
        Why Embed Perl?
       </li>
      </ul>
      <h2>
       Extending the Perl Interpreter
      </h2>
      <ul>
       <li>
        Compiling the Program
       </li>
       <li>
        perlmain.c
       </li>
       <li>
        Perl Data Types
       </li>
       <li>
        Macros and Functions
       </li>
       <li>
        Manipulating Scalars
       </li>
       <li>
        Memory Management
       </li>
       <li>
        Script Space
       </li>
       <li>
        Evaluating Perl Expressions
       </li>
       <li>
        Dynamic Loading
       </li>
       <li>
        Multiple Perl Interpreters
       </li>
      </ul>
      <h2>
       Embedding Perl in a C Program
      </h2>
      <ul>
       <li>
        Distributing Modules
       </li>
       <li>
        Get Started with h2xs
       </li>
       <li>
        Files Created by h2xs
       </li>
       <li>
        The Build Library (blib) ulectory
       </li>
       <li>
        Unit Testing and test.pl
       </li>
       <li>
        Versions
       </li>
       <li>
        Using blib
       </li>
       <li>
        POD
       </li>
       <li>
        POD Translators
       </li>
       <li>
        Cutting a Distribution
       </li>
       <li>
        Other Niceties
       </li>
       <li>
        Makefile.PL
       </li>
      </ul>
      <h2>
       Embedding Perl in a C Program
      </h2>
      <ul>
       <li>
        Think First
       </li>
       <li>
        Object-Oriented Design
       </li>
       <li>
        Object-Oriented Development
       </li>
       <li>
        Library Modules
       </li>
       <li>
        Utility Programs
       </li>
       <li>
        Filters
       </li>
       <li>
        Performance
       </li>
       <li>
        Timing with Benchmark
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="Perlscriptingtraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="49 + 21 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="70">
       <input name="url" type="hidden" value="/perlscriptingtraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>