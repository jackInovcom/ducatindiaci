<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     SALESFORCE DEVELOPMENT
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     SALESFORCE DEVELOPMENT
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      LEARN LATEST SALESFORCE DEVELOPMENT TRAINING
     </h4>
     <p>
      This course provides those who are a new to programming with an introduction to object oriented programming using Salesforce Javalike programming language called Apex. In this class, you will learn the basics of creating classes and triggers in Force.com code as well as best practices and development methodology concepts that will help you be successful as you start to build your first Force.com applications. This course also covers using Apex to build and customize Visualforce pages and along with you will learn.Integrating with Force.com covers the key elements of how to design and implement all types of integrations with the Force.com cloud platform. The course introduces the major integration mechanisms available on the Force.com platform, such as the APIs and the builtin declarative functionalities. In subsequent modules, participants explore the technologies that play specific roles in integrating with the platform, including Force.com Web service APIs, Apex, and outbound messaging. Note: To get the most out of this course, you should be comfortable writing object-oriented code. This comprehensive hands-on course is a must for new application developers who want to create custom Force.com applications or customize existing Salesforce applications. Participants will learn how to use the declarative clicks not code capabilities of the Force.com cloud platform to create newapplications and Force.com pages (Visualforce) and components to create custom interfaces.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction to Force.com Code
      </h2>
      <ul>
       <li>
        Overview, including Force.com code in the salesforce.com stack, environment requirements, usage scenarios, the development lifecycle, execution methods, and salesforce.com objects and fields
       </li>
       <li>
        An introduction to object-oriented programming, classes, and objects Installation and development using the Force.com IDE
       </li>
      </ul>
      <h2>
       Force.com Building Blocks
      </h2>
      <ul>
       <li>
        Classes, variables and methods, and data objects
       </li>
       <li>
        Data objects and relationships between objects
       </li>
       <li>
        Force.com code variables, data types, and expressions
       </li>
       <li>
        Best practices for formatting, commenting, and naming
       </li>
       <li>
        Conventions
       </li>
      </ul>
      <h2>
       Force.com Code Class Construction
      </h2>
      <ul>
       <li>
        Classes and objects
       </li>
       <li>
        Including passing and returning variables
       </li>
       <li>
        Encapsulation
       </li>
       <li>
        Conditions and logic flow control
       </li>
       <li>
        Loops and exception handling
       </li>
      </ul>
      <h2>
       Force.com Triggers and Data
      </h2>
      <ul>
       <li>
        Introduction to Force.com triggers, including when to use a trigger, order of execution, creation and development considerations, and governors and limitations
       </li>
       <li>
        An introduction to querying data with SOQL and SOSL
       </li>
       <li>
        Data Manipulation Language (DML)
       </li>
      </ul>
      <h2>
       Testing, Debugging, and Deployment
      </h2>
      <ul>
       <li>
        Debugging scenarios, methods, and tools
       </li>
       <li>
        Test classes, unit testing, testing requirements and strategies, and considerations
       </li>
       <li>
        IDE testing and an overview of Salesforce CRM UI testing
       </li>
       <li>
        Deployment checklist and preparation steps
       </li>
       <li>
        Force.com IDE deployment
       </li>
      </ul>
      <h2>
       Introduction to Visualforce &amp; Controllers
      </h2>
      <ul>
       <li>
        Understand the Visualforce framework, including its advantages and capabilities.
       </li>
       <li>
        Use expressions to bind data and actions on a page to a controller.
       </li>
       <li>
        Understand the concepts behind controllers, including their functionality and capabilities.
       </li>
       <li>
        Create custom controllers and standard controller extensions toincorporate new data and actions into a page
       </li>
       <li>
        Understand the security implications of using custom vs. standard controllers.
       </li>
       <li>
        Implement wizards using custom controllers to handle the state and operations.
       </li>
       <li>
        Create custom components that use custom controllers.
       </li>
       <li>
        Test, debug, and deploy controllers.
       </li>
      </ul>
      <h2>
       Integrations Overview
      </h2>
      <ul>
       <li>
        Key Characteristics of an Integration
       </li>
       <li>
        Salesforce Integration Mechanisms
       </li>
      </ul>
      <h2>
       Working with Salesforce Data
      </h2>
      <ul>
       <li>
        Retrieving Data: sObjects, SOQL and SOSL
       </li>
      </ul>
      <h2>
       SOAP API
      </h2>
      <ul>
       <li>
        Working with SOAP API
       </li>
       <li>
        Working with the Partner WSDL
       </li>
       <li>
        Debugging and Testing
       </li>
       <li>
        Advanced API Techniques
       </li>
      </ul>
      <h2>
       Force.com REST APIs
      </h2>
      <ul>
       <li>
        The Basics of REST
       </li>
       <li>
        The REST API
       </li>
       <li>
        The Chatter API
       </li>
      </ul>
      <h2>
       Force.com Bulk API
      </h2>
      <ul>
       <li>
        An Overview of the Bulk API
       </li>
       <li>
        Using the Bulk API to Modify Data
       </li>
       <li>
        Querying with the Bulk API
       </li>
      </ul>
      <h2>
       Force.com Streaming API
      </h2>
      <ul>
       <li>
        The Architecture
       </li>
       <li>
        Working with the Streaming API
       </li>
       <li>
        Key Streaming API Facts
       </li>
      </ul>
      <h2>
       Data Loader in Architecture
      </h2>
      <ul>
       <li>
        Data Loader Overview
       </li>
       <li>
        Including Data Loader in Architecture
       </li>
      </ul>
      <h2>
       Apex, Data Loader and Integrations
      </h2>
      <ul>
       <li>
        Custom Apex Web Services
       </li>
       <li>
        Apex Callouts
       </li>
       <li>
        Email Services
       </li>
      </ul>
      <h2>
       Declarative Integration Features
      </h2>
      <ul>
       <li>
        Outbound Messaging
       </li>
       <li>
        Salesforce to Salesforce
       </li>
      </ul>
      <h2>
       Visualforce, Portals, Sites, Site.com, and Ideas Integration
      </h2>
      <ul>
       <li>
        Mashups and Visualforce
       </li>
       <li>
        Portals Integration
       </li>
       <li>
        Sites
       </li>
       <li>
        Site.com Overview
       </li>
       <li>
        Ideas Integration
       </li>
      </ul>
      <h2>
       Design Workshop
      </h2>
      <ul>
       <li>
        Review
       </li>
       <li>
        Architecting a Solution
       </li>
      </ul>
      <h2>
       Building Applications with Force.com
      </h2>
      <h2>
       Implementing Business Processes
      </h2>
      <ul>
       <li>
        Use the VLOOKUP, REGEX, ISCHANGED, ISNEW, and PRIORVALUE functions to build business processes.
       </li>
       <li>
        Use validation rules to enforce conditional required behaviour.
       </li>
       <li>
        Use functions to enforce data format and data consistency.
       </li>
       <li>
        Implement multi-step approval workflows and escalations to automate business processes.
       </li>
       <li>
        Create parallel approval processes and approval processes with dynamic approval routing.
       </li>
       <li>
        Use outbound messages as part of an approval process.
       </li>
       <li>
        Establish approval process criteria with cross-object formulas.
       </li>
       <li>
        Set up field history tracking to audit processes.
       </li>
       <li>
        Learn techniques to prevent or record data changes.
       </li>
      </ul>
      <h2>
       Managing Data
      </h2>
      <ul>
       <li>
        Learn when and how to use upsert.
       </li>
       <li>
        Use data management tools and the capabilities of API-based tools.
       </li>
       <li>
        Use the Force.com data loader to create mapping files and to upsert data.
       </li>
      </ul>
      <h2>
       Force.com Pages
      </h2>
      <ul>
       <li>
        Learn about the capabilities of Force.com pages.
       </li>
       <li>
        Incorporate Force.com pages into Salesforce CRM.
       </li>
       <li>
        Construct expression bindings and incorporate Salesforce CRM into
       </li>
       <li>
        Force.com pages with components.
       </li>
       <li>
        Use Force.com pages components to create page layouts, input forms, output tables, custom components, and more.
       </li>
       <li>
        Create partial page refreshes on JavaScript events.
       </li>
       <li>
        Learn about the functionality that comes with Force.com pages standard controllers.
       </li>
       <li>
        Find out when Force.com code is required for creating custom controllers or extensions.
       </li>
      </ul>
      <h2>
       Industry Interface Program
      </h2>
      <h2>
       Projects
      </h2>
      <ul>
       <li>
        Modular Assignments
       </li>
       <li>
        Mini Projects
       </li>
      </ul>
      <h2>
       Domains / Industry
      </h2>
      <ul>
       <li>
        Retail Industry
       </li>
       <li>
        Banking &amp; Finance
       </li>
       <li>
        Service
       </li>
       <li>
        E-Commerce
       </li>
       <li>
        Manufacturing &amp; Production
       </li>
       <li>
        Web Application Development
       </li>
       <li>
        Research &amp; Analytics
       </li>
       <li>
        HR &amp; Consultancy
       </li>
       <li>
        FMCG
       </li>
       <li>
        Consumer Electronics
       </li>
       <li>
        Event Management Industry
       </li>
       <li>
        Telecom
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="salesforcedevtraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="20 + 14 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="34">
       <input name="url" type="hidden" value="/salesforcedevtraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>