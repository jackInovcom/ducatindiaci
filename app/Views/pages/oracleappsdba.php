<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     ORACLE APP'S DBA
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     ORACLE APP'S DBA
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      This is a modest initiative for providing help to people who really want to boost their skills in Oracle Apps DBA, with special stress to the Administration area. The main reason behind this initiative has been the problems which we had faced when we ventured out to try our hands at Oracle Apps DB Administration.
     </p>
     <p>
      Though there was lots of information available out there in the web, we didn't find most of them handy as they didn't give importance to the fact that many people will look for detailed step by step description of each activity. So, we had to go through many channels to finally get the hold on the subject and reach the level we are today.
     </p>
     <p>
      This was a tough and tedious process, and especially due to this reason, we wanted to come up with a simple and straight forward set of programs which will serve as a kind of guide for all those who want to venture out to the galaxy of Oracle Apps DBA.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction to Oracle E-Business Suite
      </h2>
      <ul>
       <li>
        Oracle E-Business suite (ERP, SCM, CRM...)
       </li>
       <li>
        Introduction to Oracle family suites
       </li>
       <li>
        Oracle Application &amp; Oracle Applications technology stack
       </li>
       <li>
        Introduction form based and java based applications
       </li>
      </ul>
      <h2>
       Introduction to Oracle E-Business Suite   Architecture
      </h2>
      <ul>
       <li>
        Concepts and Architecture ICA Two Tier/Three Tier &amp; fusion Architecture single node/multi node
       </li>
       <li>
        Introduction to oracle Apps Tier components (Apache IAS server, form server, concurrent processing server....)
       </li>
       <li>
        DB Tier component database server
       </li>
      </ul>
      <h2>
       Introduction to E-Business Suite 11i/R12 File System &amp;   Directory
      </h2>
      <ul>
       <li>
        Business Specific Module &amp; Technology Specific Module
       </li>
      </ul>
      <h2>
       Installation of Oracle E-Business Suite Application   11i/R12
      </h2>
      <ul>
       <li>
        Pre &amp; Post Steps/Checklist of installation, Single/Multi node Installation. Creation of Oracle E-Business Suite Stage
       </li>
      </ul>
      <h2>
       EBS Backup &amp; Recovery
      </h2>
      <ul>
       <li>
        Planning, Testing and Implementing EBS Application and database backups and restores/recovery.
       </li>
      </ul>
      <h2>
       SYSADMIN MODULE
      </h2>
      <ul>
       <li>
        Creating of Users
       </li>
       <li>
        Assinging Responsibilities to users
       </li>
       <li>
        Creation of Custom Responsibilities
       </li>
       <li>
        Creation of request group
       </li>
       <li>
        Creation of data group
       </li>
       <li>
        Creation of Custom Application
       </li>
       <li>
        Managing Concurren Programs &amp; Reports
       </li>
       <li>
        Administrating Concurrent Managers
       </li>
       <li>
        Creation of Custom Concurrent Manager
       </li>
       <li>
        Managing Profiles Options
       </li>
       <li>
        Structure of Profiles
       </li>
       <li>
        Seeded Profiles
       </li>
       <li>
        Managing Flex Fields (KFF,DFF)
       </li>
       <li>
        Managing Printers
       </li>
       <li>
        Managing the Workflows activites ( Notification/Mailer)
       </li>
      </ul>
      <h2>
       Oracle EBS UTILITIES
      </h2>
      <ul>
       <li>
        AD Administration (Adadmin)
       </li>
       <li>
        Auto Patch (Ad patch)
       </li>
       <li>
        AD Merge Patch (Admrgpch)
       </li>
       <li>
        AD Controller (adctrl)
       </li>
       <li>
        AD File Identification (adident)
       </li>
       <li>
        AD Rilink (adrelink.sh)
       </li>
       <li>
        AD Splice
       </li>
       <li>
        FNDCPASS
       </li>
       <li>
        FNDLOAD
       </li>
      </ul>
      <h2>
       Oracle Application Manager (OAM)
      </h2>
      <ul>
       <li>
        Oracle Application Manager Features
       </li>
       <li>
        Managing/Monitoring EBS system through OAM console
       </li>
      </ul>
      <h2>
       Introductions to Multi-Organization   Concepts
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Structure of a Patch file &amp; Patch drivers Searching
       </li>
       <li>
        Downloading &amp; Applying patches
       </li>
       <li>
        Pre &amp; Post Steps Checks
       </li>
       <li>
        Patch History
       </li>
       <li>
        Monitoring &amp; Troubleshooting Patch session
       </li>
       <li>
        Reducing Patching Time :- (Merging Patches, Patching Non-Interactively,Patching on
       </li>
       <li>
        Shared Apps Tier Using Distributed AD features, Patcching on Stage instance
       </li>
       <li>
        Troubleshooting Scenarios of Failed Patches and Hanging Patch Session
       </li>
      </ul>
      <h2>
       Cloning
      </h2>
      <ul>
       <li>
        Cloning the existing EBS System Types &amp; Methods of Clonig :- (Adclone, Rapidclone, OAMclone)
       </li>
       <li>
        Cloning Single Node to Single with Refresh/Partial Cloning
       </li>
       <li>
        Single Node to Multiple Node
       </li>
       <li>
        Multiple Node to Single Node (With/Without Shared Apps Tier)
       </li>
       <li>
        Online Cloning/ Hot Cloning and Troubleshooting Cloning Issues.
       </li>
      </ul>
      <h2>
       Performance Tuning
      </h2>
      <ul>
       <li>
        Proactive Performance Management:-
       </li>
       <li>
        Oracle EBS Health Management
       </li>
       <li>
        Effective EBS Configuration
       </li>
       <li>
        Routine/Daily tasks
       </li>
       <li>
        Proactive Monitoring Troubleshooting Performance Issues
       </li>
       <li>
        Tracing forms and concurrent program session
       </li>
      </ul>
      <h2>
       Up gradatin of Oracle E-Business Suite &amp;   Database
      </h2>
      <ul>
       <li>
        Upgrading EBS 9i datqabase to 10g re12 and Upgrading EBS 11.5.10.2 to R12
       </li>
       <li>
        Upgrading Pre and Post steps, Troubleshooting Upgrade Issues
       </li>
      </ul>
      <h2>
       Troubleshooting
      </h2>
      <ul>
       <li>
        Resolving issues like Login &amp; Connectivity Issues
       </li>
       <li>
        Files Corruption/Missing
       </li>
       <li>
        Performance AOL/J diagnostinc test
       </li>
       <li>
        Gathering Diagnostic data using RDA too.
       </li>
       <li>
        Working with Oracle Support Service for rainsing SRs (Service Request)
       </li>
      </ul>
      <h2>
       Conclusion
      </h2>
      <ul>
       <li>
        Resume Preparation
       </li>
       <li>
        Real Time Scenarios
       </li>
       <li>
        Documents
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="oracleappsdba.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="31 + 97 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="128">
       <input name="url" type="hidden" value="/oracleappsdba/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>