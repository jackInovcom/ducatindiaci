<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     ANGULAR 2 TRAINING
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     ANGULAR 2 TRAINING
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Angular 2 is an open source framework formed and maintained by the developers. It is used by the
developers to develop the web applications. Angular js is a programming which contains the logic components, data and presentation. AngularJs is the complete java script based open-source front end of web application framework. The angularjs framework works by reading the HTML page, Which has embedded into it additional custom tag attributes.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction
      </h2>
      <ul>
       <li>
        Course Objectives
       </li>
       <li>
        Course Outline
       </li>
       <li>
        What is Angular
       </li>
       <li>
        Why use Angular
       </li>
      </ul>
      <h2>
       Environment Setup
      </h2>
      <ul>
       <li>
        Node / NPM
       </li>
       <li>
        TypeScript
       </li>
       <li>
        Application File Structure
       </li>
       <li>
        Angular CLI
       </li>
       <li>
        Code Editors
       </li>
      </ul>
      <h2>
       Introduction to TypeScript
      </h2>
      <ul>
       <li>
        Why Use TypeScript
       </li>
       <li>
        Basic Types
       </li>
       <li>
        Classes and Interfaces
       </li>
       <li>
        Type Definitions
       </li>
       <li>
        Compiling TypeScript
       </li>
      </ul>
      <h2>
       Getting Started
      </h2>
      <ul>
       <li>
        Our First Component
       </li>
      </ul>
      <h2>
       Modules
      </h2>
      <ul>
       <li>
        Why use Modules
       </li>
       <li>
        NgModule
       </li>
       <li>
        Declarations
       </li>
       <li>
        Providers
       </li>
       <li>
        Imports
       </li>
       <li>
        Bootstrapping
       </li>
       <li>
        The Core Module
       </li>
       <li>
        Shared Modules
       </li>
      </ul>
      <h2>
       Components
      </h2>
      <ul>
       <li>
        Introduction to Components
       </li>
       <li>
        Component Architecture Patterns
       </li>
       <li>
        Decorator Metadata
       </li>
       <li>
        State &amp; Behaviour
       </li>
       <li>
        Inputs and Outputs
       </li>
      </ul>
      <h2>
       Templates
      </h2>
      <ul>
       <li>
        Inline vs External
       </li>
       <li>
        Template Expressions
       </li>
       <li>
        Data Bindings
       </li>
       <li>
        *ngIf else and *ngFor
       </li>
       <li>
        Built-in Structural Directives
       </li>
       <li>
        Built-in Attribute Directives
       </li>
      </ul>
      <h2>
       Custom Directives
      </h2>
      <ul>
       <li>
        Types of Directive
       </li>
       <li>
        Create your own Structural Directive
       </li>
       <li>
        Create your own Attribute Directive
       </li>
      </ul>
      <h2>
       Pipes
      </h2>
      <ul>
       <li>
        Built-in Pipes
       </li>
       <li>
        Custom Pipes
       </li>
      </ul>
      <h2>
       Services
      </h2>
      <ul>
       <li>
        Introduction to Services
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>