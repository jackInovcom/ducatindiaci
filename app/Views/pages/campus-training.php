<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Campus Training
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Campus Training
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <h3>
     CAMPUS TRAINING
    </h3>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
    <div class="coursesArea">
     <br/>
     <p>
      <img alt="campus-training-at-ducat" class="img-responsive" src="../images/campus-training-at-ducat-new.jpg"/>
     </p>
     <p>
      The level of competition is rising daily and because of which the pressure as well. The feeling of competition is so high that even in the market place where you have to buy or sell goods; you have to face lots to competition. When it comes to education, children are getting pressurized on daily basis because they find it difficult to meet the demands of school and after schooling they face lots of competition in their college life. Whole life goes in competing with each other but the point where the competition goes to its peak is the placement time.
     </p>
     <p>
      Every student has different caliber but the placement process has few concept based on which they are chosen. When a student gets placed he has dual feeling one of happiness and other of fear; happy because from now onwards he is a professional and he is capable enough to handle his own expenditures and fearful because he is not aware of the environment he will get there and how he should react towards them.
     </p>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/ct1.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        Why Ducat for campus training
       </h3>
       <p class="paraText">
        Campus training, as the word suggests in itself the training, which is given in the campus, but this training is not ordinary as it contains all the important aspects, which are necessary for you to know before entering into the professional world where practical knowledge is given more preference than the theoretical knowledge.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/ct2.jpg"/>
      </div>
      <div class="serviceContent">
       <h3>
        Training by Professional Trainers
       </h3>
       <p class="paraText">
        At Ducat  the professional teachers, latest methodologies of learning and practical knowledge about the subject are provided. We are aware that these factors make a vital difference in shaping up the career of a student.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/ct3.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        Practical Exposure
       </h3>
       <p class="paraText">
        Although theoretical knowledge is important because if you do not know the basics, how can you go to the level of practising? The value of practical knowledge is much higher than this because in professional world if you do not know how to do things practically then it will become difficult for you to survive there.
                                most importantly an expert trainer.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/ct4.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        Specific training
       </h3>
       <p class="paraText">
        At Ducat the training programs are specially designed by the experts according to the demand of industries so that they can prepare you according to the current need in order to increase your placement chances.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/ct5.jpg"/>
      </div>
      <div class="serviceContent">
       <h3>
        Customized Training Program
       </h3>
       <p class="paraText">
        At Ducat experts and professionals come to help you to know what exact kind of environment you will get there and how to work accordingly.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/ct6.jpg"/>
      </div>
      <div class="serviceContent">
       <h3>
        Placements
       </h3>
       <p class="paraText">
        Not only the practical knowledge but he or she also gets the facility of placement, which is the major concern of every student. There could not be anything better than a place where campus training.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <form class="searchForm">
     <input placeholder="Search" type="text"/>
    </form>
    <div class="widgetArea">
     <h5>
      CONTACT INFO
     </h5>
     <address>
      <span class="address">
       A - 43 &amp; A - 52 Sector - 16,
       <br/>
       Noida (U.P) (Near McDonalds)
      </span>
      <br>
       <span class="phone">
        <strong>
         Phone:
        </strong>
        0120-4646464, +91- 9871055180
       </span>
       <br/>
       <span class="email">
        <strong>
         E-Mail:
        </strong>
        <a href="mailto:info@ducatindia.com">
         info@ducatindia.com
        </a>
       </span>
       <br/>
       <span class="web">
        <strong>
         Web:
        </strong>
        <a href="http://www.ducatindia.com/">
         http://www.ducatindia.com/
        </a>
       </span>
      </br>
     </address>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section class="text-center" id="offices">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <h5>
     CORPORATE OFFICE NOIDA:
     <span>
      0120 - 4646464
     </span>
    </h5>
    <p>
     GR.NOIDA:
     <span>
      0120-4345190
     </span>
     GHAZIABAD:
     <span>
      0120-4835400
     </span>
     FARIDABAD:
     <span>
      0129-4150605
     </span>
     GURGAON:
     <span>
      0124-4219095
     </span>
     JAIPUR:
     <span>
      0141-2550077
     </span>
    </p>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>