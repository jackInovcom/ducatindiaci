<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     UI AND ANGULAR JS Training
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     UI AND ANGULAR JS Training
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      UI AND ANGULAR JS
     </h4>
     <p>
      UI AND ANGULAR JS course enable you to design and development excellent looking browser-based user interfaces and UI web solutions from the requirements stage to deployment onto the production web farm.In information technology, the user interface (UI) is everything designed into an information device with which a human being may interact. AngularJS is a web based Javascript framework introduced by Google. It is an Open Source which is based on HTML, CSS and JavaScript. AngularJS is latest framework to develop web based application.
     </p>
     <div class="contentAcc">
      <h2>
       HTML
      </h2>
      <ul>
       <li>
        <p>
         What is HTML?
        </p>
       </li>
       <li>
        What is a Web Browser?
       </li>
       <li>
        What are Versions of HTML?
       </li>
       <li>
        What can You Do with HTML?
       </li>
       <li>
        HTML Development Environments
       </li>
       <li>
        Using a WYSIWYG Editor
       </li>
       <li>
        Using an HTML Editor
       </li>
       <li>
        Writing Code with a Text Editor
       </li>
       <li>
        Publishing Documents
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>