<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     Java Full Stack Developer training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Java Full Stack Developer
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Ducat provides Best Java Full Stack Developer Training in noida based on industry standards that helps attendees to secure placements in their dream jobs at MNCs. Ducat Provides Best Java Full Stack Developer Training in Noida. Ducat is one of the most credible Java Full Stack Developer Training institutes in Noida offering hands on practical knowledge and full job assistance with basic as well as advanced level Java Full Stack Developer Training courses.
     </p>
     <div class="contentAcc">
      <h2>
       Intoduction tO Java
      </h2>
      <ul>
       <li>
        Evolution of Java
       </li>
       <li>
        Object Oriented Programming Structure
       </li>
       <li>
        Client side Programming &amp; its requirements
       </li>
       <li>
        Platform Independency &amp; Portability
       </li>
       <li>
        Security
       </li>
       <li>
        Relation b/w JVM, JRE and JDK
       </li>
       <li>
        Description of a Simple Java Program
       </li>
       <li>
        Introduction to JAR format
       </li>
       <li>
        Naming Conventions
       </li>
       <li>
        Data types &amp; Type casting
       </li>
      </ul>
      <h2>
       OOPS Implementation
      </h2>
      <h2>
       Classes and Objects
      </h2>
      <ul>
       <li>
        Defining attributes and methods
       </li>
       <li>
        Implementing data encapsulation
       </li>
       <li>
        Relation b/w objects and reference variables
       </li>
       <li>
        Constructors and Anonymous block
       </li>
       <li>
        Method Overloading
       </li>
       <li>
        Static Data members, Block &amp; methods
       </li>
      </ul>
      <h2>
       Understanding Memory Structure
      </h2>
      <ul>
       <li>
        Stack
       </li>
       <li>
        Heap
       </li>
       <li>
        Class &amp; Method area
       </li>
      </ul>
      <h2>
       Understanding Class loading &amp; Execution flow
      </h2>
      <ul>
       <li>
        Static vs Dynamic Class loading
       </li>
       <li>
        Implicit vs Explicit class loading
       </li>
       <li>
        Sequence of operations performed at the time of class loading
       </li>
      </ul>
      <h2>
       Argument Passing Mechanism
      </h2>
      <ul>
       <li>
        Passing primitive arguments
       </li>
       <li>
        Passing objects
       </li>
       <li>
        Wrapper Classes &amp; their use
       </li>
      </ul>
      <h2>
       Usage of this keyword
      </h2>
      <ul>
       <li>
        Referencing instance members
       </li>
       <li>
        Intra class constructor chaining
       </li>
       <li>
        Method chaining
       </li>
      </ul>
      <h2>
       Inheritance &amp; code reusability
      </h2>
      <ul>
       <li>
        Extending classes for code reusability
       </li>
       <li>
        Usage of super keyword
       </li>
       <li>
        Method Overriding
       </li>
       <li>
        Object class and its roles
       </li>
      </ul>
      <h2>
       Inheritance &amp; Runtime Polymorphism
      </h2>
      <ul>
       <li>
        Static &amp; Dynamic binding
       </li>
       <li>
        Inheritance and Is-A relation
       </li>
       <li>
        Runtime Polymorphism and Generalization
       </li>
       <li>
        Abstract classes &amp; methods
       </li>
       <li>
        Final Keyword
       </li>
      </ul>
      <h2>
       Interfaces and Role based Inheritance
      </h2>
      <ul>
       <li>
        Difference b/w Feature &amp; Role based Inheritance
       </li>
       <li>
        Difference b/w Static &amp; Dynamic classing Environment
       </li>
       <li>
        Difference b/w classes &amp; interfaces
       </li>
       <li>
        Use fo interfaces in real scenarios
       </li>
      </ul>
      <h2>
       Implementing Has-A relation
      </h2>
      <ul>
       <li>
        Difference b/w Aggregation &amp; Composition
       </li>
       <li>
        Nested classes and their role
       </li>
       <li>
        Inner classes
       </li>
       <li>
        Anonymous Inner classes
       </li>
      </ul>
      <h2>
       Package &amp; Scopes
      </h2>
      <ul>
       <li>
        Need of Packages
       </li>
       <li>
        Associating classes to Packages
       </li>
       <li>
        Understanding Classpath environment variable
       </li>
       <li>
        Import Keyword and Feature of static import
       </li>
       <li>
        Public, protected, private &amp; default scope
       </li>
       <li>
        Private Inheritance and its use
       </li>
       <li>
        Examples of private inheritance
       </li>
      </ul>
      <h2>
       Exception Handling
      </h2>
      <ul>
       <li>
        Difference b/w exception and error
       </li>
       <li>
        Exception Handling &amp; Robustness
       </li>
       <li>
        Common Exceptions and Errors
       </li>
       <li>
        Try and catch block
       </li>
       <li>
        Exception handlers &amp; importance of their order
       </li>
       <li>
        Throw keyword and its usages
       </li>
       <li>
        Checked and Unchecked Exceptions
       </li>
       <li>
        Role of finally
       </li>
       <li>
        Creating User defined Exceptions
       </li>
      </ul>
      <h2>
       Collection Framework
      </h2>
      <ul>
       <li>
        Role and Importance of Collection Framework
       </li>
       <li>
        Use of List &amp; Set based collection
       </li>
       <li>
        Use of Iterator &amp; ListIteractor
       </li>
       <li>
        Use of Maps
       </li>
       <li>
        Searching elements in List, Hash and Tree based collections
       </li>
       <li>
        Role of equals and hashCode() methods
       </li>
       <li>
        Role of Comparable and Comparator Interfaces
       </li>
       <li>
        Thread Safety and Vector
       </li>
       <li>
        Difference b/w Enumeration and Iterator
       </li>
       <li>
        Type safety and Generics
       </li>
       <li>
        Common algorithms and Collections class
       </li>
       <li>
        Using Properties class for managing properties files
       </li>
      </ul>
      <h2>
       Database Connectivity Using JDBC 4.x
      </h2>
      <ul>
       <li>
        Overview of native and ODBC Drives
       </li>
       <li>
        Introduction to JDBC
       </li>
       <li>
        Type of JDBC drivers
       </li>
       <li>
        Using different type of drivers
       </li>
       <li>
        Defining properties based Connection Factory
       </li>
      </ul>
      <h2>
       Performing basic database operations
      </h2>
      <ul>
       <li>
        Insert
       </li>
       <li>
        Delete
       </li>
       <li>
        Update
       </li>
       <li>
        Select
       </li>
      </ul>
      <h2>
       Prepared Statement
      </h2>
      <ul>
       <li>
        Difference b/w Statement &amp; Prepared Statement
       </li>
       <li>
        Setting Query parameters
       </li>
       <li>
        Executing Queries
       </li>
      </ul>
      <h2>
       Callabe Statement
      </h2>
      <ul>
       <li>
        Creating PL/SQL Stored procedures and functions
       </li>
       <li>
        Creating Callable statements
       </li>
       <li>
        Executing procedures &amp; functions
       </li>
      </ul>
      <h2>
       Misc
      </h2>
      <ul>
       <li>
        Batch Updation
       </li>
       <li>
        Transacting Queries
       </li>
       <li>
        Programmatic initialization of database
       </li>
       <li>
        ResultSetMetaData
       </li>
       <li>
        DatabaseMetaData
       </li>
      </ul>
      <h2>
       API DEVELOPEMENT
      </h2>
      <ul>
       <li>
        Introduction to SpringBoot
       </li>
       <li>
        IOC &amp; DI
       </li>
       <li>
        Web MVC
       </li>
       <li>
        Spring Data
       </li>
       <li>
        Creating REST API using SpringBoot
       </li>
      </ul>
      <h2>
       UI DEVELOPEMENT
      </h2>
      <h2>
       Introduction
      </h2>
      <ul>
       <li>
        Course Objectives
       </li>
       <li>
        Course Outline
       </li>
       <li>
        What is Angular
       </li>
       <li>
        Why use Angular
       </li>
      </ul>
      <h2>
       Environment Setup
      </h2>
      <ul>
       <li>
        Node / NPM
       </li>
       <li>
        TypeScript
       </li>
       <li>
        Application File Structure
       </li>
       <li>
        Angular CLI
       </li>
       <li>
        Code Editors
       </li>
      </ul>
      <h2>
       Introduction to TypeScript
      </h2>
      <ul>
       <li>
        Why Use TypeScript
       </li>
       <li>
        Basic Types
       </li>
       <li>
        Classes and Interfaces
       </li>
       <li>
        Type Definitions
       </li>
       <li>
        Compiling TypeScript
       </li>
      </ul>
      <h2>
       Getting Started
      </h2>
      <ul>
       <li>
        Our First Component
       </li>
      </ul>
      <h2>
       Modules
      </h2>
      <ul>
       <li>
        Why use Modules
       </li>
       <li>
        NgModule
       </li>
       <li>
        Declarations
       </li>
       <li>
        Providers
       </li>
       <li>
        Imports
       </li>
       <li>
        Bootstrapping
       </li>
       <li>
        The Core Module
       </li>
       <li>
        Shared Modules
       </li>
      </ul>
      <h2>
       Components
      </h2>
      <ul>
       <li>
        Introduction to Components
       </li>
       <li>
        Component Architecture Patterns
       </li>
       <li>
        Decorator Metadata
       </li>
       <li>
        State &amp; Behaviour
       </li>
       <li>
        Inputs and Outputs
       </li>
      </ul>
      <h2>
       Templates
      </h2>
      <ul>
       <li>
        Inline vs External
       </li>
       <li>
        Template Expressions
       </li>
       <li>
        Data Bindings
       </li>
       <li>
        *ngIf else and *ngFor
       </li>
       <li>
        Built-in Structural Directives
       </li>
       <li>
        Built-in Attribute Directives
       </li>
      </ul>
      <h2>
       Custom Directives
      </h2>
      <ul>
       <li>
        Types of Directive
       </li>
       <li>
        Create your own Structural Directive
       </li>
       <li>
        Create your own Attribute Directive
       </li>
      </ul>
      <h2>
       Pipes
      </h2>
      <ul>
       <li>
        Built-in Pipes
       </li>
       <li>
        Custom Pipes
       </li>
      </ul>
      <h2>
       Services
      </h2>
      <ul>
       <li>
        Introduction to Services
       </li>
       <li>
        Building a Service
       </li>
      </ul>
      <h2>
       Dependency Injection
      </h2>
      <ul>
       <li>
        Introduction to Dependency Injection
       </li>
       <li>
        Injectors &amp; Providers
       </li>
       <li>
        Registering Providers
       </li>
      </ul>
      <h2>
       Lifecycle Hooks
      </h2>
      <ul>
       <li>
        Component LifeCycle
       </li>
       <li>
        Using ngOnInit
       </li>
       <li>
        All lifecycle Hooks
       </li>
      </ul>
      <h2>
       Routing
      </h2>
      <ul>
       <li>
        The Component Router
       </li>
       <li>
        Defining Routes
       </li>
       <li>
        Navigation
       </li>
       <li>
        Route Params
       </li>
       <li>
        Child Routes
       </li>
      </ul>
      <h2>
       Advanced Routing
      </h2>
      <ul>
       <li>
        Route Guards
       </li>
      </ul>
      <h2>
       Template-driven Forms
      </h2>
      <ul>
       <li>
        Introduction to forms
       </li>
       <li>
        Template-driven forms
       </li>
       <li>
        Validation
       </li>
      </ul>
      <h2>
       Asynchronous Operations
      </h2>
      <ul>
       <li>
        Introduction to Async
       </li>
       <li>
        Promises
       </li>
       <li>
        Observables
       </li>
       <li>
        HTTP Request / Response
       </li>
      </ul>
      <h2>
       Third �Party NPM Package
      </h2>
      <ul>
       <li>
        Social Login Authentication
       </li>
       <li>
        Cookies and Local Storage
       </li>
       <li>
        Angular File Upload
       </li>
       <li>
        Angular Pagination
       </li>
       <li>
        Angular Shopping Cart
       </li>
      </ul>
      <h2>
       Building &amp; Deployment
      </h2>
      <ul>
       <li>
        Environments
       </li>
       <li>
        Builds
       </li>
       <li>
        Deployment on a Server
       </li>
      </ul>
      <h2>
       PROJECT
      </h2>
      <ul>
       <li>
        Covering All The Concepts
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="Javafullstackdeveloper.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="85 + 5 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="90">
       <input name="url" type="hidden" value="/Javafullstackdeveloper/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>