<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Register for Job Fair
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Register for  Job Fair
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9 text-center">
    <h3>
     FILL ALL THE INFORMATION CORRECTLY
    </h3>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
    <form action="preview_reg.php" class="onlineRegistration text-left" method="post">
     <div class="row">
      <div class="col-md-8">
       <input name="name" placeholder="Enter Full Name" required="" tabindex="1" type="text"/>
       <input name="email" placeholder="Email Address" required="" tabindex="2" type="text"/>
       <input name="mobile" placeholder="Enter Mobile" required="" tabindex="3" type="text"/>
       <input name="ten" placeholder="% of 10th" required="" tabindex="4" type="text"/>
       <input name="twelve" placeholder="% of 12th" required="" tabindex="5" type="text"/>
       <input name="pger" placeholder="% of Graduation" tabindex="6" type="text"/>
       <select name="qualification" tabindex="7">
        <option value="B.Tech">
         B.Tech
        </option>
        <option value="M.Tech">
         M.Tech
        </option>
        <option value="MCA">
         MCA
        </option>
        <option value="BCA">
         BCA
        </option>
        <option value="Diploma">
         Diploma
        </option>
        <option value="Graduation">
         Graduation
        </option>
        <option value="Post Graducation">
         Post Graducation
        </option>
        <option value="Others">
         Others
        </option>
       </select>
       <input name="pyear" placeholder="Passout Year" required="" tabindex="8" type="text"/>
       <select name="fexp" required="" tabindex="9">
        <option value="fresher">
         Fresher
        </option>
        <option value="experiance">
         Experiance
        </option>
       </select>
       <select name="technology" required="" tabindex="10">
        <option value="Android">
         Android
        </option>
        <option value="Angular">
         Angular
        </option>
        <option value="Autocad">
         Autocad
        </option>
        <option value="Automation Testing">
         Automation Testing
        </option>
        <option value="Amazon(aws)">
         Amazon (AWS)
        </option>
        <option value="CCNA">
         CCNA
        </option>
        <option value=".Data Science">
         Data Science
        </option>
        <option value="Digital Marketing">
         Digital Marketing
        </option>
        <option value="Dot NET">
         .NET
        </option>
        <option value="Embedded">
         Embedded
        </option>
        <option value="Graphic Designing">
         Graphic Designing
        </option>
        <option value="IOS">
         IOS
        </option>
        <option value="IOT">
         IOT
        </option>
        <option value="JAVA">
         JAVA
        </option>
        <option value="LINUX">
         LINUX
        </option>
        <option value="Machine learning">
         Machine learning
        </option>
        <option value="manual testing">
         Manual Testing
        </option>
        <option value="Mean Stack">
         Mean Stack
        </option>
        <option value="Node js">
         Node js
        </option>
        <option value="Oracle DBA">
         Oracle DBA
        </option>
        <option value="Oracle Developer">
         Oracle Developer
        </option>
        <option value="PHP">
         PHP
        </option>
        <option value="Python">
         Python
        </option>
        <option value="React js">
         React js
        </option>
        <option value="RPA BluePrism">
         RPA Blue Prism
        </option>
        <option value="RPA uipath">
         RPA UIPath
        </option>
        <option value="Salesforce">
         Salesforce
        </option>
        <option value="SOLIDWORKS">
         SOLIDWORKS
        </option>
        <option value="sql server">
         SQL Server
        </option>
        <option value="UI">
         UI
        </option>
        <option value="Web Designing">
         Web Designing
        </option>
       </select>
       <input name="ducatid" placeholder="If you are ducat student plz fill your Id " tabindex="11" type="text"/>
      </div>
      <!-- End Of Col MD 6 -->
      <div class="col-md-8">
       <input name="submitReg" type="submit" value="Submit"/>
      </div>
      <!-- End Of Col MD 6 -->
     </div>
     <!-- End Of Row -->
    </form>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <form class="searchForm">
     <input placeholder="Search" type="text"/>
    </form>
    <div class="widgetArea">
     <h5>
      CONTACT INFO
     </h5>
     <address>
      <span class="address">
       A - 43 &amp; A - 52 Sector - 16,
       <br/>
       Noida (U.P) (Near McDonalds)
      </span>
      <br>
       <span class="phone">
        <strong>
         Phone:
        </strong>
        0120-4646464, +91- 9871055180
       </span>
       <br/>
       <span class="email">
        <strong>
         E-Mail:
        </strong>
        <a href="mailto:info@ducatindia.com">
         info@ducatindia.com
        </a>
       </span>
       <br/>
       <span class="web">
        <strong>
         Web:
        </strong>
        <a href="http://www.ducatindia.com/">
         http://www.ducatindia.com/
        </a>
       </span>
      </br>
     </address>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section class="text-center" id="offices">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <h5>
     CORPORATE OFFICE NOIDA:
     <span>
      0120 - 4646464
     </span>
    </h5>
    <p>
     GR.NOIDA:
     <span>
      0120-4345190
     </span>
     GHAZIABAD:
     <span>
      0120-4835400
     </span>
     FARIDABAD:
     <span>
      0129-4150605
     </span>
     GURGAON:
     <span>
      0124-4219095
     </span>
    </p>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>