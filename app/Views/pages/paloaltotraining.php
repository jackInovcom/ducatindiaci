<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     PALO ALTO
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     PALO ALTO
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Palo Alto Networks training provides the next-generation firewall knowledge you need to secure your network and safely enable applications.

In This courses that feature lecture and hands-on labs, you will learn to install, configure, manage and troubleshoot Palo Alto Networks firewalls, gaining the skills and expertise needed to protect your organization from the most advanced cybersecurity attacks.
     </p>
     <div class="contentAcc">
      <h2>
       Platforms and Architecture
      </h2>
      <ul>
       <li>
        Single Pass Architecture
       </li>
       <li>
        Flow Logic
       </li>
       <li>
        Designing your PAN in the Network
       </li>
      </ul>
      <h2>
       Initial Configuration
      </h2>
      <ul>
       <li>
        Initial Access to the PAN and Account Administration
       </li>
       <li>
        Overview of Dashboard and Configuration Management
       </li>
       <li>
        Licensing and Software Updates
       </li>
      </ul>
      <h2>
       Interface Configuration
      </h2>
      <li>
       Security Zones
      </li>
      <li>
       Layer2, Layer3, Virtual Wire and Tap
      </li>
      <li>
       Sub Interfaces and virtual routers
      </li>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>