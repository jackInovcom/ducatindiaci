<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     SAS
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     SAS
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      DUCAT is the only training organization giving industrial guidance with a difference. The organization not only provides guidance but takes appropriate care that the training is consequential. DUCAT provides training on SAS in the most practical way. SAS is statistical analysis system which is the collection of software products that is grouped. The collection of the software allows the end users to perform a wide range of task that covers almost every aspect of business administration and function. DUCAT provides an education which teaches the new skill in a focused manner. The reason of the guidance is that it teaches you the new knowledge in a way that you can put it into your work field quickly. You learn directly from the starting place so the information gained is ideal. DUCAT training staffs are the most skilled in this SAS technology. They give education, shows and clarify realistic examples, create work surroundings where this skill can be used. So, after taking the training one becomes a specialist of this expertise which is in demand all over in every industry. The training helps you to gain insight at a high speed to take decision regarding this high value technology. The training will make you an expert in this field and will help to achieve your goal in IT industry.
     </p>
     <div class="contentAcc">
      <h2>
       SAS DATA WAREHOUSING CONCEPTS
      </h2>
      <ul>
       <li>
        What is a Data Warehouse ?
       </li>
       <li>
        What is a Data Mart ?
       </li>
       <li>
        What is the difference between Relational Databases and the Data in Data Warehouse (OLTP versus OLAP)
       </li>
       <li>
        Why do we need Data Warehouses when the Relational Source Database exists
       </li>
       <li>
        Multi Dimensional Analysis and Decision Support Reporting from Data Warehouse
       </li>
       <li>
        Data Warehouse Architecture (ETL Design)
       </li>
       <li>
        Normalized Relational Database Design (Entity Relationship Model)
       </li>
       <li>
        Dimensional Data Modeling
       </li>
       <li>
        Star Schema Design
       </li>
       <li>
        Snowflake Schema Design
       </li>
       <li>
        Slowly Changing Dimensions Why SAS BI ?Capabilities of SAS B12 Advantages of SAS BI Over Base &amp; Advance SAS
       </li>
       <li>
        B1 Architecture
       </li>
       <li>
        SAS BI Tools
       </li>
      </ul>
      <h2>
       BASE SAS INTRODUCTION
      </h2>
      <ul>
       <li>
        An Overview of the SAS System
       </li>
       <li>
        SAS Tasks
       </li>
       <li>
        Output produced by the SAS System
       </li>
       <li>
        SAS Tools (SAS Program - Data step and Proc step)
       </li>
       <li>
        A sample SAS program
       </li>
       <li>
        Exploring SAS Windowing Environment Navigation
       </li>
      </ul>
      <h2>
       DATA ACCESS &amp; DATA MANAGEMENT
      </h2>
      <ul>
       <li>
        SAS Data Libraries
       </li>
       <li>
        Rules for Writing SAS Programs / Statements, Dataset Variable Name Getting familiar with SAS Dataset
       </li>
       <li>
        Data portion of the SAS Dataset
       </li>
       <li>
        Rules for writing Dataset names / Variable names
       </li>
       <li>
        Attributes of a Variable (Numeric / Character)
       </li>
       <li>
        Options
       </li>
       <li>
        System Options (nodate, linesize, pagesize, pageno etc)
       </li>
       <li>
        Dataset Options (Drop, Keep, Rename, Where, Firstobs= Obs=)
       </li>
       <li>
        How SAS works (Flow of Data Step Processing - Compilation &amp; Execution phase)
       </li>
       <li>
        Input Buffer
       </li>
       <li>
        Program data vector (PDV)
       </li>
       <li>
        Descriptor Information of a SAS Dataset
       </li>
      </ul>
      <h2>
       DATALINES OR CARDS DATA TRANSFORMATIONS
      </h2>
      <ul>
       <li>
        SAS Date Values
       </li>
       <li>
        Length Statement
       </li>
       <li>
        Creating multiple output SAS datasets from singe input SAS dataset
       </li>
       <li>
        Conditionally writing observations to one or more SAS datasets
       </li>
       <li>
        Outputting Multiple Observations (Implicit Output)
       </li>
       <li>
        Selecting Variables and observations (DROP or KEEP statement and DROP= or KEEP = dataset options)
       </li>
       <li>
        Controlling which Observations are read (OBS= FIRSTOBS = Options)
       </li>
       <li>
        The Data Statement_Null_
       </li>
       <li>
        The_N_Automatic Variable
       </li>
       <li>
        Creating Subset of observations
       </li>
       <li>
        Conditional Processing using IF-THEN and ELSE statement, IF----THEN DO ; ----END;ELSE DO;----END;
       </li>
       <li>
        DO WHILE Statement
       </li>
       <li>
        DO UNTIL Statement
       </li>
       <li>
        Iterative DO loop Processing
       </li>
       <li>
        Where Statement OR Where Condition (dataset)
       </li>
       <li>
        Deciding whether to use a Where statement or Subsetting IF statement
       </li>
       <li>
        Accumulating Totals for a Group of Data (BY- Group Processing (First &amp; Last)
       </li>
       <li>
        Multiple BY variables
       </li>
       <li>
        DATASETS Procedure ( To modify the Variable name/lable/format/informat)
       </li>
       <li>
        Reading SAS datasets and Creating Variables
       </li>
       <li>
        Creating an Accumulating Variable (The RETAIN Statement)
       </li>
       <li>
        The DELETE Statement
       </li>
       <li>
        The SUM Statement
       </li>
       <li>
        The RENAME = Data Set option
       </li>
       <li>
        Combining SAS Datasets
       </li>
       <li>
        1. Concatenating SAS Data Sets Using SET statement in DATA Step
       </li>
       <li>
        Inter Leaving SAS Data Sets
       </li>
       <li>
        Merging SAS Data Sets
       </li>
       <li>
        Match-Merge
       </li>
       <li>
        Using Merge Statement
       </li>
       <li>
        THE IN = Data Set option
       </li>
       <li>
        Additional Features of merging SAS Datasets
       </li>
       <li>
        One-to-Many Merging
       </li>
       <li>
        Many-to-Many Merging
       </li>
      </ul>
      <h2>
       READING RAW DATA FROM EXTERNAL FILE ( INFILE &amp; INPUT STATEMENT )
      </h2>
      <ul>
       <li>
        Introduction to Raw Data
       </li>
       <li>
        Factors considered to examine the raw data
       </li>
       <li>
        Reading Unaligned Data (List Input)
       </li>
       <li>
        Reading Data Aligned to Columns (Column Input)
       </li>
       <li>
        Reading Data that requires Special Instructions (Formatted Input)
       </li>
       <li>
        Controlling the position of the Pointer in Formatted Input
       </li>
       <li>
        Absolute - Column pointer control (@)
       </li>
       <li>
        Relative- Column pointer control (+)
       </li>
       <li>
        Mixed Style Input ( Mixing List, Input. Formatted Input styles in one INPUT Staement)
       </li>
       <li>
        Using colon (:) modifier to specify an informat in the INPUT Statement )
       </li>
       <li>
        Recognize delimiter in the raw data file (Using DLM= option in INFILE Statement
       </li>
       <li>
        Missing data at the end of row (Using MISSOVER option in INFILE statement )
       </li>
       <li>
        Missing values without placeholders (DSD option in INFILE statement)
       </li>
       <li>
        Reading a raw data file with multiple records per observation(Column pointer controls)
       </li>
       <li>
        Method1: Using Multiple INPUT statement
       </li>
       <li>
        Method2: Using Line Pointer Control (/)
       </li>
       <li>
        Reading Variables from multiple records in any order (#n)
       </li>
       <li>
        Line Hold Specifies in INPUT statement
       </li>
       <li>
        The Single Trailing @
       </li>
       <li>
        The Double Trailing @@ ( Multiple Observations per Record)
       </li>
       <li>
        Methods of Control in INFILE statement
       </li>
       <li>
        FLOWOVER
       </li>
       <li>
        STOPOVER
       </li>
       <li>
        MISSOVER
       </li>
       <li>
        TRUNCOVER
       </li>
       <li>
        Writing to an External File (FILE &amp; PUT Statement )
       </li>
       <li>
        Reading Excel Spreadsheets (IMPORT Wizard / Import Procedure)
       </li>
      </ul>
      <h2>
       SAS FUNCTIONS
      </h2>
      <ul>
       <li>
        Manipulating Character Values (SUBSTRING / RIGHT / LEFT / SCAN/ CONCATENATION TRIM / FIND / INDEX / UPCASE / LOWCASE / COMPRESS / LENGTH )
       </li>
       <li>
        Manipulation Numeric Values ( ROUND / CEIL / FLOOR / INT / SUM / MEAN /MIN/MAX)
       </li>
       <li>
        Manipulating Numeric Values based on DATES ( MDY / TODAY / INTCK / YRDIF)
       </li>
       <li>
        Converting Variable Type
       </li>
       <li>
        INPUT ( character-to-numeric)
       </li>
       <li>
        PUT (numeric-to-character)
       </li>
       <li>
        Debugging SAS program (DEBUG Option)
       </li>
       <li>
        SAS VARIABLE Lists
       </li>
       <li>
        SAS Arrays
       </li>
       <li>
        Enhancing Report Output
       </li>
       <li>
        Defining Titles &amp; Footnotes
       </li>
       <li>
        Formatting Data values ( Date, Character &amp; Numeric values )
       </li>
       <li>
        Creating User-Defined Formats (Proc Format)
       </li>
       <li>
        Formats &amp; Informats
       </li>
      </ul>
      <h2>
       ANALYSIS &amp; PRESENTATION
      </h2>
      <ul>
       <li>
        Descriptor portion of the SAS Data Set ( Proc Contents)
       </li>
       <li>
        Producing List Reports (Proc Print)
       </li>
       <li>
        Sequencing and Grouping Observations (Proc Sort)
       </li>
       <li>
        Producing Summary Reports
       </li>
       <li>
        PROC FREQ -(One Way &amp; Two-Way Frequencies)
       </li>
       <li>
        PROC MEANS
       </li>
       <li>
        PROC REPORT
       </li>
       <li>
        PROC TABULATE
       </li>
       <li>
        PROC SUMMARY
       </li>
       <li>
        PROC PRINTO
       </li>
       <li>
        PROC APPEND
       </li>
       <li>
        PROC TRANSPOSE
       </li>
       <li>
        PROC COPY
       </li>
       <li>
        PROC COMPARE
       </li>
       <li>
        PROC DATASETS
       </li>
       <li>
        Regression Procedure
       </li>
       <li>
        Univariate / Multivariate Procedures
       </li>
       <li>
        Ranking Procedure
       </li>
       <li>
        Producing Bard and Pie Charts
       </li>
       <li>
        Producing Plots
       </li>
       <li>
        The Output Delivery System (SAS/ODS)
       </li>
       <li>
        Creating HTML Reports
       </li>
       <li>
        Creating Text Reports
       </li>
       <li>
        Creating PDF Reports
       </li>
       <li>
        Creating CSV Files
       </li>
      </ul>
      <h2>
       SAS MACRO LANGUAGE INTRODUCTION TO THE MACRO FACILITY PURPOSE OF THE MACRO FACILITY
      </h2>
      <ul>
       <li>
        Generate SAS code using Macros (%Macro &amp; %Mend)
       </li>
       <li>
        Tips on Writing Macro-Based Programs
       </li>
       <li>
        Replacing Text Strings using Macros Variables (%Let)
       </li>
      </ul>
      <h2>
       MACRO PROGRAMS
      </h2>
      <ul>
       <li>
        MACRO PROGRAMS
       </li>
       <li>
        Defining a Macro (%Macro &amp; %Mend )
       </li>
       <li>
        Macro Compilation
       </li>
       <li>
        Monitoring Macro Compilation (MCOMPILENOTE OPTION)
       </li>
       <li>
        Calling a Macro (%Macro-Name)
       </li>
       <li>
        Macro Execution
       </li>
       <li>
        Monitoring Macro Execution (MLOGIC OPTION)
       </li>
       <li>
        Viewing the generate SAS Code in the Log from Macro Program (MPRINT OPTION)
       </li>
       <li>
        Macro Storage
       </li>
       <li>
        Macro Parameters
       </li>
       <li>
        Macro Parameters Lists
       </li>
       <li>
        Macros with Positional Parameters
       </li>
       <li>
        Macros with Keyword Parameters
       </li>
       <li>
        Arithmetic and logical Operations
       </li>
       <li>
        Conditional Processing
       </li>
       <li>
        % IF expression % THEN text ; %ELSE %TEXT;
       </li>
       <li>
        % IF expression % THEN %DO; %END; %ELSE; %DO;
       </li>
       <li>
        Stored Compiled Macros
       </li>
       <li>
        %INCLUDE Statement
       </li>
      </ul>
      <h2>
       MACRO PROCESSING
      </h2>
      <ul>
       <li>
        Tokens
       </li>
       <li>
        Macro Triggers
       </li>
       <li>
        How the Macroprocessor works
       </li>
      </ul>
      <h2>
       MACRO VARIABLES CONCEPTS
      </h2>
      <ul>
       <li>
        Referencing a Macro Variable
       </li>
       <li>
        Displaying Macro Variable Value in the SAS log (SYMBOLGEN OPTION)
       </li>
       <li>
        Automatic Macro Variables
       </li>
       <li>
        System-Defined Macro Variables (_AUTOMATIC_)
       </li>
       <li>
        User-Defined Macro Variable (_USER_)
       </li>
       <li>
        Datatype
       </li>
       <li>
        %LET Statement
       </li>
       <li>
        Global Macro variables
       </li>
       <li>
        Local Macro Variables
       </li>
       <li>
        Deleting User-Defined Macro Variable (%SYMDEL)
       </li>
       <li>
        Macro Functions
       </li>
       <li>
        Character Strings
       </li>
       <li>
        Other SAS Functions
       </li>
       <li>
        %SYSFUNC
       </li>
       <li>
        %STR
       </li>
       <li>
        Combining Macro Variable References with Text
       </li>
       <li>
        Macro Variable Name Delimiter
       </li>
       <li>
        Quoting
       </li>
       <li>
        Creating Macro Variables in the Data Step (CALL SYMPUT ROUTINE) Obtaining Variable value during Macro Execution (SYMGET FUNCTION)Creating Macro Variables during PROC SQL Execution (INTO Clause)
       </li>
       <li>
        creating a delimited list of Values.
       </li>
      </ul>
      <h2>
       SAS SQL PROCESSING INTRODUCTION TO THE SQL PROCEDURE
      </h2>
      <ul>
       <li>
        Terminology
       </li>
       <li>
        Features of PROC SQL
       </li>
       <li>
        PROC SQL Syntax (SELECT, FROM, WHERE, GROUP BY, HAVING, ORDER BY)
       </li>
       <li>
        VALIDATE Keyword
       </li>
       <li>
        NOEXEC Option
       </li>
       <li>
        Added PROC SQL Statements (ALTER, CREATE, DELETE, DESCRIBE, DROP)
       </li>
       <li>
        FEEDBACK OPTION
       </li>
       <li>
        PROC SQL and DATA Step Comparisons
       </li>
       <li>
        Queries
       </li>
       <li>
        Retrieving Data from a table
       </li>
       <li>
        Identify All Rows in a Table
       </li>
       <li>
        Remove Duplicate Rows
       </li>
       <li>
        Sub setting using WHERE clause
       </li>
       <li>
        Sub setting with Calculated Values
       </li>
       <li>
        Sub setting with Calculated Values
       </li>
       <li>
        Enhancing Query Output (LABEL, FORMAT)
       </li>
       <li>
        Grouping Data (Group By)
       </li>
       <li>
        Analyzing Groups of data (COUNT)
       </li>
       <li>
        Updating Data values (Update Statement)
       </li>
       <li>
        Using Table Alias
       </li>
       <li>
        Creating Views
       </li>
       <li>
        Creating Dropping Indexes
       </li>
       <li>
        Sub Queries
       </li>
       <li>
        Non-Correlated Sub Query
       </li>
       <li>
        Correlated Sub Query
       </li>
       <li>
        Combining Tables
       </li>
       <li>
        Joins
       </li>
       <li>
        Inner Joins
       </li>
       <li>
        Outer Joins
       </li>
       <li>
        Left Join
       </li>
       <li>
        Right Join
       </li>
       <li>
        Full Join
       </li>
      </ul>
      <h2>
       SET OPERATORS
      </h2>
      <ul>
       <li>
        EXCEPT
       </li>
       <li>
        INTERSECT
       </li>
       <li>
        UNION
       </li>
       <li>
        Choosing between Data Step Merges and SQL Joins
       </li>
      </ul>
      <h2>
       UNIX
      </h2>
      <ul>
       <li>
        Introduction to Unix
       </li>
       <li>
        Introduction to UNIX Architecture
       </li>
       <li>
        Understanding UNIX Commands
       </li>
       <li>
        Understanding ID/Groups/Permissions
       </li>
       <li>
        Introduction to Shell Scripting
       </li>
       <li>
        Writing UNIX Programs
       </li>
       <li>
        Understanding VI Editor
       </li>
       <li>
        Introduction to LSF
       </li>
       <li>
        Scheduling SAS Codes through UNIX
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="sastraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="3 + 7 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="10">
       <input name="url" type="hidden" value="/sastraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>