<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     DATA SCIENCE &amp; ML USING R PROGRAMMING
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     DATA SCIENCE &amp; ML USING R PROGRAMMING
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      DATA SCIENCE &amp; ML USING R PROGRAMMING Training
     </h4>
     <p>
      This course is designed for both complete beginners with no programming experience or experienced developers looking to make the jump to Data Science!.R is a free programming language and software environment for statistical computing and graphics. The R language is widely used among statisticians and data miners for developing statistical software and data analysis.
R is an implementation of the S programming language combined with lexical scoping semantics inspired by Scheme.S was created by John Chambers while at Bell Labs. There are some important differences, but much of the code written for S runs unaltered.
R was created by Ross Ihaka and Robert Gentleman at the University of Auckland, New Zealand, and is currently developed by the R Development Core Team, of which Chambers is a member. R is named partly after the first names of the first two R authors and partly as a play on the name of S. The project was conceived in 1992, with an initial version released in 1994 and a stable beta version in 2000
     </p>
     <div class="contentAcc">
      <h2>
       FUNDAMENTAL OF STATISTICS.
      </h2>
      <ul>
       <li>
        Population and sample
       </li>
       <li>
        Descriptive and Inferential Statistics
       </li>
       <li>
        Statistical data analysis
       </li>
       <li>
        Variables
       </li>
       <li>
        Sample and Population Distributions
       </li>
       <li>
        Interquartile range
       </li>
       <li>
        Central Tendency
       </li>
       <li>
        Normal Distribution
       </li>
       <li>
        Skewness.
       </li>
       <li>
        Boxplot
       </li>
       <li>
        Five Number Summary
       </li>
       <li>
        Standard deviation
       </li>
       <li>
        Standard Error
       </li>
       <li>
        Emperical Formula
       </li>
       <li>
        central limit theorem
       </li>
       <li>
        Estimation
       </li>
       <li>
        Confidence interval
       </li>
       <li>
        Hypothesis testing
       </li>
       <li>
        p-value
       </li>
       <li>
        Scatterplot and correlation coefficient
       </li>
       <li>
        Standard Error
       </li>
       <li>
        Scales of Measurements and Data Types
       </li>
       <li>
        Data Summarization
       </li>
       <li>
        Visual Summarization
       </li>
       <li>
        Numerical Summarization
       </li>
       <li>
        Outliers &amp; Summary
       </li>
      </ul>
      <h2>
       Module 1- Introduction to Data Analytics
      </h2>
      <ul>
       <li>
        Objectives:
       </li>
       <li>
        This module introduces you to some of the important keywords in R like Business Intelligence, Business
       </li>
       <li>
        Analytics, Data and Information. You can also learn how R can play an important role in solving complex analytical problems.
       </li>
       <li>
        This module tells you what is R and how it is used by the giants like Google, Facebook, etc.
       </li>
       <li>
        Also, you will learn use of 'R' in the industry, this module also helps you compare R with other software
       </li>
       <li>
        in analytics, install R and its packages.
       </li>
       <li>
        <strong>
         Topics:
        </strong>
       </li>
       <li>
        <strong>
         Business Analytics, Data, Information
        </strong>
       </li>
       <li>
        Understanding Business Analytics and R
       </li>
       <li>
        Compare R with other software in analytics
       </li>
       <li>
        Install R
       </li>
       <li>
        Perform basic operations in R using command line
       </li>
      </ul>
      <h2>
       Module 2- Introduction to R programming
      </h2>
      <ul>
       <li>
        Starting and quitting R
       </li>
       <li>
        Recording your work
       </li>
       <li>
        Basic features of R.
       </li>
       <li>
        Calculating with R
       </li>
       <li>
        Named storage
       </li>
       <li>
        Functions
       </li>
       <li>
        R is case-sensitive
       </li>
       <li>
        Listing the objects in the workspace
       </li>
       <li>
        Vectors
       </li>
       <li>
        Extracting elements from vectors
       </li>
       <li>
        Vector arithmetic
       </li>
       <li>
        Simple patterned vectors
       </li>
       <li>
        Missing values and other special values
       </li>
       <li>
        Character vectors Factors
       </li>
       <li>
        More on extracting elements from vectors
       </li>
       <li>
        Matrices and arrays
       </li>
       <li>
        Data frames
       </li>
       <li>
        Dates and times
       </li>
      </ul>
      <h2>
       Import and Export data in R
      </h2>
      <ul>
       <li>
        Importing data in to R
       </li>
       <li>
        CSV File
       </li>
       <li>
        Excel File
       </li>
       <li>
        Import data from text table
       </li>
       <li>
        DATA SCIENCE USING
       </li>
       <li>
        R-PROGRAMMING
       </li>
       <li>
        Topics
       </li>
       <li>
        Variables in R
       </li>
       <li>
        Scalars
       </li>
       <li>
        Vectors
       </li>
       <li>
        R Matrices
       </li>
       <li>
        List
       </li>
       <li>
        R � Data Frames
       </li>
       <li>
        Using c, Cbind, Rbind, attach and detach functions in R
       </li>
       <li>
        R � Factors
       </li>
       <li>
        R � CSV Files
       </li>
       <li>
        R � Excel File
       </li>
       <li>
        NOTE-:
       </li>
       <li>
        Assignments
       </li>
       <li>
        Business Scenerio/Group Discussion.
       </li>
       <li>
        R Nuts and Bolts-:
       </li>
       <li>
        Entering Input. � Evaluation- R Objects- Numbers- Attributes- Creating Vectors- Mixing Objects-
       </li>
       <li>
        Explicit Coercion- Summary- Names- Data Frames.
       </li>
      </ul>
      <h2>
       Module 3- Managing Data Frames with the dplyr package
      </h2>
      <ul>
       <li>
        The dplyr Package
       </li>
       <li>
        Installing the dplyr package
       </li>
       <li>
        select()
       </li>
       <li>
        filter()
       </li>
       <li>
        arrange()
       </li>
       <li>
        rename()
       </li>
       <li>
        mutate()
       </li>
       <li>
        group_by()
       </li>
       <li>
        %&gt;%
       </li>
       <li>
        NOTE-:
       </li>
       <li>
        Assignments
       </li>
       <li>
        Business Scenerio/Group Discussion.
       </li>
      </ul>
      <h2>
       Module 4- Loop Functions
      </h2>
      <ul>
       <li>
        Looping on the Command Line
       </li>
       <li>
        lapply()
       </li>
       <li>
        sapply()
       </li>
       <li>
        tapply()
       </li>
       <li>
        apply()
       </li>
       <li>
        NOTE-:
       </li>
       <li>
        Assignments
       </li>
       <li>
        Business Scenerio/Group Discussion.
       </li>
      </ul>
      <h2>
       Module 5- Data Manipulation in R Objectives:
      </h2>
      <ul>
       <li>
        In this module, we start with a sample of a dirty data set and perform Data Cleaning on it, resulting
       </li>
       <li>
        in a data set, which is ready for any analysis.
       </li>
       <li>
        Thus using and exploring the popular functions required to clean data in R.
       </li>
       <li>
        Topics
       </li>
       <li>
        Data sorting
       </li>
       <li>
        Find and remove duplicates record
       </li>
       <li>
        Cleaning data
       </li>
       <li>
        Merging data
       </li>
       <li>
        Statistical Plotting-:
       </li>
       <li>
        Bar charts and dot charts
       </li>
       <li>
        Pie charts
       </li>
       <li>
        Histograms
       </li>
       <li>
        Box plots
       </li>
       <li>
        Scatterplots
       </li>
       <li>
        QQ plots
       </li>
      </ul>
      <h2>
       Objectives:
      </h2>
      <ul>
       <li>
        Control Structure Programming with R
       </li>
       <li>
        The for() loop
       </li>
       <li>
        The if() statement
       </li>
       <li>
        The while() loop
       </li>
       <li>
        The repeat loop, and the break and next statements
       </li>
       <li>
        Apply
       </li>
       <li>
        Sapply
       </li>
       <li>
        Lapply
       </li>
      </ul>
      <h2>
       Factors
      </h2>
      <ul>
       <li>
        Using Factors
       </li>
       <li>
        Manipulating Factors
       </li>
       <li>
        Numeric Factors
       </li>
       <li>
        Creating Factors from Continuous Variables
       </li>
       <li>
        Convert the variables in factors or in others.
       </li>
      </ul>
      <h2>
       Reshaping
      </h2>
      <ul>
       <li>
        Data Modifying
       </li>
       <li>
        Data Frame Variables
       </li>
       <li>
        Recoding Variables
       </li>
       <li>
        The recode Function
       </li>
       <li>
        Reshaping Data Frames
       </li>
       <li>
        The reshape Package
       </li>
      </ul>
      <h2>
       Module 6- Statistical Learning-:
      </h2>
      <ul>
       <li>
        What Is Statistical Learning?
       </li>
       <li>
        Why Estimate f?
       </li>
       <li>
        How Do We Estimate f?
       </li>
       <li>
        The Trade-Off Between Prediction Accuracy and Model Interpretability
       </li>
       <li>
        Supervised Versus Unsupervised Learning
       </li>
       <li>
        Regression Versus Classification Problems
       </li>
       <li>
        Assessing Model Accuracy
       </li>
      </ul>
      <h2>
       Module 7- Basics of Statistics &amp; Linear &amp; Multiple Regression
      </h2>
      <ul>
       <li>
        This module touches the base of Descriptive and Inferential Statistics and Probabilities &amp;
       </li>
       <li>
        'Regression Techniques'.
       </li>
       <li>
        Linear and logistic regression is explained from the basics with the examples and it is
       </li>
       <li>
        implemented in R using two case studies dedicated to each type of Regression discussed.
       </li>
       <li>
        Assessing the Accuracy of the Coefficient Estimates.
       </li>
       <li>
        Assessing the Accuracy of the Model.
       </li>
       <li>
        Estimating the Regression Coefficients.
       </li>
       <li>
        Some Important Questions
       </li>
       <li>
        Lab: Linear Regression.
       </li>
       <li>
        Libraries .
       </li>
       <li>
        Simple Linear Regression
       </li>
       <li>
        Multiple Linear Regression
       </li>
       <li>
        Interaction Terms
       </li>
       <li>
        Qualitative Predictors
       </li>
       <li>
        Writing Functions
       </li>
       <li>
        NOTE-:
       </li>
       <li>
        Assignments with Different Datasets.
       </li>
       <li>
        Business Scenerio/Group Discussion
       </li>
      </ul>
      <h2>
       Module 8- Classification-:
      </h2>
      <ul>
       <li>
        An Overview of Classification.
       </li>
       <li>
        Why Not Linear Regression?
       </li>
       <li>
        Logistic Regression
       </li>
       <li>
        The Logistic Model
       </li>
       <li>
        Estimating the Regression Coefficients
       </li>
       <li>
        Making Predictions
       </li>
       <li>
        Logistic Regression for &gt;2 Response Classes
       </li>
       <li>
        Lab: Logistic Regression.
       </li>
       <li>
        The Stock Market Data
       </li>
       <li>
        Logistic Regression
       </li>
       <li>
        NOTE-:
       </li>
       <li>
        Assignments with Different Datasets.
       </li>
       <li>
        Business Scenerio/Group Discussion.
       </li>
      </ul>
      <h2>
       Module 9- Variance Inflation Factor-:
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Multicolinearity.
       </li>
       <li>
        How we can detect the multicolinearity.
       </li>
       <li>
        Effects of multicolinearity
       </li>
       <li>
        Lab: VIF
       </li>
       <li>
        Applications.
       </li>
       <li>
        Reduce the features.
       </li>
       <li>
        NOTE-:
       </li>
       <li>
        Assignments with Different Datasets.
       </li>
       <li>
        Business Scenerio/Group Discussion.
       </li>
       <li>
        Correlation
       </li>
       <li>
        Types of Correlation
       </li>
       <li>
        Properties of Correlation
       </li>
       <li>
        Methods of Calculating Correlation
       </li>
      </ul>
      <h2>
       Module 10- Best Model Selection-:
      </h2>
      <ul>
       <li>
        Subset Selection
       </li>
       <li>
        Best Subset Selection
       </li>
       <li>
        Stepwise Selection
       </li>
       <li>
        Choosing the Optimal Model
       </li>
       <li>
        Lab 1: Subset Selection Methods
       </li>
       <li>
        Best Subset Selection
       </li>
       <li>
        Forward and Backward Stepwise Selection
       </li>
       <li>
        Choosing Among Models Using the Validation Set Approach and Cross-Validation
       </li>
       <li>
        NOTE-:
       </li>
       <li>
        Assignments with Different Datasets.
       </li>
       <li>
        Business Scenerio/Group Discussion.
       </li>
      </ul>
      <h2>
       Explore many algorithms and models:
      </h2>
      <ul>
       <li>
        Popular algorithms: Classification, Regression, Clustering, and Dimensional Reduction.
       </li>
       <li>
        Popular models: Train/Test Split, Root Mean Squared Error, and Random Forests. Get ready to do more learning than your machine!
       </li>
      </ul>
      <h2>
       Module 11 - Machine Learning vs Statistical Modeling  Supervised vs Unsupervised Learning
      </h2>
      <ul>
       <li>
        Machine Learning Languages, Types, and Examples
       </li>
       <li>
        Machine Learning vs Statistical Modelling
       </li>
       <li>
        Supervised vs Unsupervised Learning
       </li>
       <li>
        Supervised Learning Classification
       </li>
       <li>
        Unsupervised Learning
       </li>
      </ul>
      <h2>
       Module 12 - Supervised Learning I
      </h2>
      <ul>
       <li>
        K-Nearest Neighbors
       </li>
       <li>
        Decision Trees
       </li>
       <li>
        Random Forests
       </li>
       <li>
        Reliability of Random Forests
       </li>
       <li>
        Advantages  Disadvantages of Decision Trees
       </li>
      </ul>
      <h2>
       Module 13 - Supervised Learning II
      </h2>
      <ul>
       <li>
        Regression Algorithms
       </li>
       <li>
        Model Evaluation
       </li>
       <li>
        Model Evaluation: Overfitting  Underfitting
       </li>
       <li>
        Understanding Different Evaluation Models
       </li>
      </ul>
      <h2>
       Module 14 - Unsupervised Learning
      </h2>
      <ul>
       <li>
        K-Means Clustering plus Advantages  Disadvantages
       </li>
       <li>
        Hierarchical Clustering plus Advantages  Disadvantages
       </li>
       <li>
        Measuring the Distances Between Clusters - Single Linkage Clustering
       </li>
       <li>
        Measuring the Distances Between Clusters - Algorithms for Hierarchy Clustering
       </li>
       <li>
        Density-Based Clustering
       </li>
      </ul>
      <h2>
       Module 15 - Dimensionality Reduction  Collaborative Filtering
      </h2>
      <ul>
       <li>
        Dimensionality Reduction: Feature Extraction  Selection
       </li>
       <li>
        Collaborative Filtering  Its Challenges
       </li>
      </ul>
      <h2>
       Module 16 - Tree-Based Methods-:
      </h2>
      <ul>
       <li>
        The Basics of Decision Trees
       </li>
       <li>
        Regression Trees
       </li>
       <li>
        Classification Trees
       </li>
       <li>
        Trees Versus Linear Models
       </li>
       <li>
        Advantages and Disadvantages of Trees
       </li>
       <li>
        Bagging, Random Forests, Boosting
       </li>
       <li>
        Bagging
       </li>
       <li>
        Random Forests
       </li>
       <li>
        Lab: Decision Trees
       </li>
       <li>
        Fitting Classification Trees
       </li>
       <li>
        Fitting Regression Trees
       </li>
       <li>
        <strong>
         NOTE-:
        </strong>
       </li>
       <li>
        Assignments with Different Datasets.
       </li>
       <li>
        Business Scenerio/Group Discussion.
       </li>
      </ul>
      <h2>
       Module 17 - Time Series Forcasting-:
      </h2>
      <ul>
       <li>
        Time series
       </li>
       <li>
        Estimating and Eliminating the Deterministic Components if they are present in the Model.
       </li>
       <li>
        Estimating and Eliminating Seasonality if it is present in the Model
       </li>
       <li>
        Modeling the Remainder using Auto Regressive Moving Average (ARMA) Models
       </li>
       <li>
        Identify 'order' of the ARMA model
       </li>
       <li>
        'Forecast' or Predict for Future Values
       </li>
       <li>
        Practise on R
       </li>
       <li>
        <strong>
         NOTE-:
        </strong>
       </li>
       <li>
        Assignments with Different Datasets.
       </li>
       <li>
        Business Scenerio/Group Discussion.
       </li>
      </ul>
      <h2>
       Module 18 - Support Vector Machines � Outline
      </h2>
      <ul>
       <li>
        Understand when the Support Vector family of methods are an appropriate method of analysis.
       </li>
       <li>
        Understand what a hyperplane is and how they are used with the Support Vector methods.
       </li>
       <li>
        Identify the differences between Maximal Margin Classifiers, Support Vector Classifiers, and Support Vector Machines.
       </li>
       <li>
        Know how each of the algorithms determines the best separating hyperplane.
       </li>
       <li>
        Distinguish between hard and soft margins and when each is to be used.
       </li>
       <li>
        Know how to extend the method for nonlinear cases.
       </li>
       <li>
        <strong>
         NOTE-:
        </strong>
       </li>
       <li>
        Assignments with Different Datasets.
       </li>
       <li>
        Business Scenerio/Group Discussion.
       </li>
      </ul>
      <h2>
       Module 19 - Principal Component Analysis �Outline
      </h2>
      <ul>
       <li>
        Understand what principal components are and when principal component analysis is appropriate.
       </li>
       <li>
        Describe eigenvalues and eigenvectors and how they are used to calculate principal components.
       </li>
       <li>
        Understand loading and loading vectors.
       </li>
       <li>
        Know how to decide how many principal components to use in the analysis.
       </li>
       <li>
        Be able to use principal component analysis for regression.
       </li>
       <li>
        <strong>
         NOTE-:
        </strong>
       </li>
       <li>
        Assignments with Different Datasets.
       </li>
       <li>
        Business Scenerio/Group Discussion.
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="DATASCIENCEMACHINELEARNING.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="97 + 75 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="172">
       <input name="url" type="hidden" value="/data-science-ML-using-r-programming/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>