<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     GD&amp;T Training
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     GD&amp;T Training
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      GD&amp;T Training BY DUCAT
     </h4>
     <p>
      Geometric dimensioning and tolerancing (GD&amp;T) is used as a symbolic way of showing specific tolerances on drawings. GD&amp;T is a valuable tool that effectively communicates the design intent to manufacturing and inspection. It is governed by the technical standard ASME Y14.5M-2009. This course introduces participants to the GD&amp;T system, providing a working knowledge of the correct interpretation and application of each symbol, general rules, the datum system, and 'bonus' tolerance and highlighting some of the changes in the updated Y14.5 standard. The material is reinforced with many practice exercises.
     </p>
     <div class="contentAcc">
      <h2>
       Engineering Drawing
      </h2>
      <ul>
       <li>
        <p>
         Anatomy Of drawing
        </p>
       </li>
       <li>
        Orthographic Projection
       </li>
       <li>
        Angle Projections
       </li>
       <li>
        Dimensioning
       </li>
       <li>
        Spacing and Readability
       </li>
       <li>
        Dimension placement
       </li>
       <li>
        View Placements
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>