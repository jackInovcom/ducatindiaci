<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     PROGRAMMATIC DEVELOPMENT USING APEX AND VISUALFORCE TRAINING
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     PROGRAMMATIC DEVELOPMENT USING APEX AND VISUALFORCE
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      PROGRAMMATIC DEVELOPMENT USING APEX AND VISUALFORCE TRAINING
     </h4>
     <p>
      This course is designed for programmatic developers who are new to the Force.com platform, who
need to be able to write programmatic customizations to both the business logic and user interface
layers using Apex and Visualforce.
     </p>
     <div class="contentAcc">
      <h2>
       Who should take this course?
      </h2>
      <h2>
       Prerequisites
      </h2>
      <ul>
       <li>
        1 year programming in Java (or another object-oriented programming language)
       </li>
       <li>
        Basic data modeling for relational databases
       </li>
       <li>
        Basic SQL
       </li>
       <li>
        Basic HTML
       </li>
       <li>
        Basic JavaScript
       </li>
      </ul>
      <h2>
       What you will learn
      </h2>
      <ul>
       <li>
        Create and modify objects using the declarative interface
       </li>
       <li>
        Write business logic customizations using Apex triggers and classes. Those customizations will use SOQL and DML.
       </li>
       <li>
        Design programmatic solutions that take advantage of declarative customizations
       </li>
       <li>
        Describe how your trigger code works within the basics oft he Save Orderof Execution
       </li>
       <li>
        Describe some of the fundamental aspects of designing programs on a multi-tenant platform
       </li>
       <li>
        Write Visualforce markup and code to customize the user interface
       </li>
       <li>
        Use the built-in testing framework to test Apex and Visualforce
       </li>
      </ul>
      <h2>
       MODULES &amp; TOPICS
      </h2>
      <h2>
       Programming with Apex
      </h2>
      <ul>
       <li>
        Describe key aspects of Apex that differentiate it from other languages, such as Java and C#
       </li>
       <li>
        Describe why Apex transactions and governor limits must be considered when writing Apex
       </li>
       <li>
        Execute simple Apex
       </li>
       <li>
        Use the sObject data type, the primitive data types, and basic control statements in Apex
       </li>
      </ul>
      <h2>
       Introduction to OOPS
      </h2>
      <ul>
       <li>
        Describe how Apex classes are used
       </li>
       <li>
        Define an Apex class
       </li>
       <li>
        Determine what data an Apex class can access
       </li>
       <li>
        Conditions and logic flows
       </li>
       <li>
        Collections
       </li>
       <li>
        List
       </li>
       <li>
        Set
       </li>
       <li>
        Map
       </li>
      </ul>
      <h2>
       Use SOQL to Query Your Org's Data
      </h2>
      <ul>
       <li>
        Write a basic query using Salesforce's query language, SOQL
       </li>
       <li>
        Process the result of a query in Apex
       </li>
       <li>
        Create a query dynamically at run-time
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>