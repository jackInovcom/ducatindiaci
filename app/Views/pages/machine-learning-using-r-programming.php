<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     MACHINE LEARNING USING R PROGRAMMING
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     MACHINE LEARNING USING R PROGRAMMING
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      MACHINE LEARNING USING R PROGRAMMING Training
     </h4>
     <p>
      R is a free programming language and software environment for statistical computing and graphics. The R language is widely used among statisticians and data miners for developing statistical software and data analysis.
R is an implementation of the S programming language combined with lexical scoping semantics inspired by Scheme.S was created by John Chambers while at Bell Labs. There are some important differences, but much of the code written for S runs unaltered.
R was created by Ross Ihaka and Robert Gentleman at the University of Auckland, New Zealand, and is currently developed by the R Development Core Team, of which Chambers is a member. R is named partly after the first names of the first two R authors and partly as a play on the name of S. The project was conceived in 1992, with an initial version released in 1994 and a stable beta version in 2000
     </p>
     <div class="contentAcc">
      <h2>
       FUNDAMENTAL OF STATISTICS.
      </h2>
      <ul>
       <li>
        Population and sample
       </li>
       <li>
        Descriptive and Inferential Statistics
       </li>
       <li>
        Statistical data analysis
       </li>
       <li>
        Variables
       </li>
       <li>
        Sample and Population Distributions
       </li>
       <li>
        Interquartile range
       </li>
       <li>
        Central Tendency
       </li>
       <li>
        Normal Distribution
       </li>
       <li>
        Skewness.
       </li>
       <li>
        Boxplot
       </li>
       <li>
        Five Number Summary
       </li>
       <li>
        Standard deviation
       </li>
       <li>
        Standard Error
       </li>
       <li>
        Emperical Formula
       </li>
       <li>
        central limit theorem
       </li>
       <li>
        Estimation
       </li>
       <li>
        Confidence interval
       </li>
       <li>
        Hypothesis testing
       </li>
       <li>
        p-value
       </li>
       <li>
        Scatterplot and correlation coefficient
       </li>
       <li>
        Standard Error
       </li>
       <li>
        Scales of Measurements and Data Types
       </li>
       <li>
        Data Summarization
       </li>
       <li>
        Visual Summarization
       </li>
       <li>
        Numerical Summarization
       </li>
       <li>
        Outliers &amp; Summary
       </li>
      </ul>
      <h2>
       Module 1- Introduction to Data Analytics
      </h2>
      <ul>
       <li>
        Objectives:
       </li>
       <li>
        This module introduces you to some of the important keywords in R like Business
       </li>
       <li>
        Intelligence, Business
       </li>
       <li>
        Analytics, Data and Information. You can also learn how R can play an important role in solving complex analytical problems.
       </li>
       <li>
        This module tells you what is R and how it is used by the giants like Google, Facebook, etc.
       </li>
       <li>
        Also, you will learn use of 'R' in the industry, this module also helps you compare R with other software
       </li>
       <li>
        in analytics, install R and its packages.
       </li>
       <li>
        Topics:
       </li>
       <li>
        Business Analytics, Data, Information
       </li>
       <li>
        Understanding Business Analytics and R
       </li>
       <li>
        Compare R with other software in analytics
       </li>
       <li>
        Install R
       </li>
       <li>
        Perform basic operations in R using command line
       </li>
      </ul>
      <h2>
       Module 2- Introduction to R programming
      </h2>
      <ul>
       <li>
        Starting and quitting R
       </li>
       <li>
        Recording your work
       </li>
       <li>
        Basic features of R.
       </li>
       <li>
        Calculating with R
       </li>
       <li>
        Named storage
       </li>
       <li>
        Functions
       </li>
       <li>
        R is case-sensitive
       </li>
       <li>
        Listing the objects in the workspace
       </li>
       <li>
        Vectors
       </li>
       <li>
        Extracting elements from vectors
       </li>
       <li>
        Vector arithmetic
       </li>
       <li>
        Simple patterned vectors
       </li>
       <li>
        Missing values and other special values
       </li>
       <li>
        Character vectors Factors
       </li>
       <li>
        More on extracting elements from vectors
       </li>
       <li>
        Matrices and arrays
       </li>
       <li>
        Data frames
       </li>
       <li>
        Dates and times
       </li>
       <li>
        NOTE:-
       </li>
       <li>
        Assignments with Datasets
       </li>
      </ul>
      <h2>
       Import and Export data in R
      </h2>
      <ul>
       <li>
        Importing data in to R
       </li>
       <li>
        CSV File
       </li>
       <li>
        Excel File
       </li>
       <li>
        Import data from text table
       </li>
       <li>
        Topics
       </li>
       <li>
        Variables in R
       </li>
       <li>
        Scalars
       </li>
       <li>
        Vectors
       </li>
       <li>
        R Matrices
       </li>
       <li>
        List
       </li>
       <li>
        R � Data Frames
       </li>
       <li>
        Using c, Cbind, Rbind, attach and detach functions in R
       </li>
       <li>
        R � Factors
       </li>
       <li>
        R � CSV Files
       </li>
       <li>
        R � Excel File
       </li>
       <li>
        NOTE-:
       </li>
       <li>
        Assignments
       </li>
       <li>
        Business Scenerio/Group Discussion.
       </li>
       <li>
        R Nuts and Bolts-:
       </li>
      </ul>
      <h2>
       Module 3- Managing Data Frames with the dplyr package
      </h2>
      <ul>
       <li>
        The dplyr Package
       </li>
       <li>
        Installing the dplyr package
       </li>
       <li>
        select()
       </li>
       <li>
        filter()
       </li>
       <li>
        arrange()
       </li>
       <li>
        rename()
       </li>
       <li>
        mutate()
       </li>
       <li>
        group_by()
       </li>
       <li>
        %&gt;%
       </li>
       <li>
        NOTE:-
       </li>
       <li>
        Assignments
       </li>
       <li>
        Business Scenerio/Group Discussion.
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>