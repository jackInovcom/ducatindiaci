<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     IOT TRAINING PROGRAM
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     IOT
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      The explosive growth of the �Internet of Things� is changing our world and the quick drop in price for
typical IoT components is allowing people to make new designs and products at home. In this you will
learn the importance of IoT in society, the current mechanism of typical IoT devices and trends for the
future. IoT design considerations, constraints and interfacing between the physical world and your
device will also be covered
     </p>
     <div class="contentAcc">
      <h2>
       Internet OF Things
      </h2>
      <h2>
       IoT Development
      </h2>
      <h2>
       Introduction of IoT (2 Hours)
      </h2>
      <ul>
       <li>
        What is IoT?
       </li>
       <li>
        How IoT is applied in different domains?
       </li>
       <li>
        Use cases ranging from Smart Cities to IIoT
       </li>
       <li>
        How large is the IoT Market in different domains?
       </li>
       <li>
        IoT Technology stack
       </li>
       <li>
        Sensors &amp; Actuators
       </li>
       <li>
        Hardware Platforms
       </li>
       <li>
        IoT Operating System
       </li>
       <li>
        Wireless Communication Protocols
       </li>
       <li>
        Network communication Protocols
       </li>
       <li>
        Cloud, its components and IoT
       </li>
       <li>
        Data Streaming in IoT
       </li>
       <li>
        Data Store and IoT
       </li>
       <li>
        Analytics &amp; Visualization for IoT
       </li>
      </ul>
      <h2>
       IoT Device Design &amp; Management
      </h2>
      <ul>
       <li>
        Top IoT hardware platform
       </li>
       <li>
        Architecture, layout and comparison of different microcontroller
       </li>
       <li>
        Sensor, Actuator, Microcontroller
       </li>
      </ul>
      <h2>
       Wireless Networking Technology
      </h2>
      <ul>
       <li>
        NFC
       </li>
       <li>
        RFID
       </li>
       <li>
        WIFI
       </li>
       <li>
        Z WAVE
       </li>
       <li>
        LoRaWAN
       </li>
       <li>
        Zigbee
       </li>
       <li>
        Cellular
       </li>
       <li>
        Bluetooth
       </li>
       <li>
        SigFox
       </li>
       <li>
        NB-IOT
       </li>
      </ul>
      <h2>
       Wired Communication Protocol
      </h2>
      <ul>
       <li>
        SPI
       </li>
       <li>
        UART
       </li>
       <li>
        I2C
       </li>
      </ul>
      <h2>
       Interfacing of different Sensor, Actuator, Wireless Networking Technology with different microcontroller
      </h2>
      <h2>
       Introduction of Arduino IDE
      </h2>
      <h2>
       Arduino Mega (10 hours)
      </h2>
      <ul>
       <li>
        Introduction of Arduino Mega
       </li>
       <li>
        Industry application
       </li>
       <li>
        Pin configuration or description of board
       </li>
       <li>
        Exploring Linux file system,Hardware, I/O
       </li>
       <li>
        Interfacing a push button
       </li>
       <li>
        Making led on/off by push button
       </li>
       <li>
        Interfacing an RGB led
       </li>
       <li>
        Theory of PWM Pulse width modulation
       </li>
       <li>
        Control light intensity using PWM
       </li>
       <li>
        Interfacing an LDR.
       </li>
       <li>
        Light dependent resistor as sensor to measure brightness
       </li>
       <li>
        Interfacing temperature sensor
       </li>
       <li>
        Motor Interfacing(DC)
       </li>
       <li>
        Motor Driver Introduction
       </li>
       <li>
        Servo motor theory
       </li>
       <li>
        controlling servo motor with PWM
       </li>
       <li>
        Interfacing IR Sensor
       </li>
       <li>
        Interface analog Sensor
       </li>
       <li>
        SPI/I2C and UART protocol
       </li>
      </ul>
      <h2>
       Introduction of Python
      </h2>
      <h2>
       Raspberry Pi (10 hours)
      </h2>
      <ul>
       <li>
        Raspberry-pi Architecture
       </li>
       <li>
        Working with Raspberry Pi 3 Model
       </li>
       <li>
        Industry Use case of Raspberry Pi
       </li>
       <li>
        Installing OS and Designing Systems using Raspberry pi
       </li>
       <li>
        Configuring Raspberry Pi for VNC Connection
       </li>
       <li>
        Getting introduced to Linux OS
       </li>
       <li>
        Basic Linux commands and uses
       </li>
       <li>
        Interface sensor and Actuator with Raspberry-pi
       </li>
       <li>
        Interface relay with raspberry pi
       </li>
       <li>
        Interface different sensor like IR sensor/gas sensor
       </li>
       <li>
        Interface different sensor like LDR/Soil sensor
       </li>
       <li>
        Interface different sensor like Soil sensor with raspberry pi
       </li>
       <li>
        Interface different sensor like DHT11 with raspberry pi
       </li>
       <li>
        Interface PI-Camera with Raspberry
       </li>
       <li>
        Program Raspberry pi for click image
       </li>
       <li>
        Program Raspberry pi for create video
       </li>
       <li>
        Serial communication with raspberry pi and arduino
       </li>
       <li>
        Sending data to arduino to raspberry pi or raspberry pi
       </li>
       <li>
        Interfacing DC motor with raspberry pi
       </li>
       <li>
        Servo motor Concept
       </li>
       <li>
        Interfacing Servo motor with Raspberry pi
       </li>
       <li>
        Interfacing Steeper motor
       </li>
      </ul>
      <h2>
       Working with different wireless networking technology (10 hours)
      </h2>
      <ul>
       <li>
        NFC
       </li>
       <li>
        Bluetooth
       </li>
       <li>
        Zigbee
       </li>
       <li>
        RFID
       </li>
      </ul>
      <h2>
       Networking and Communication Protocol (1 hours)
      </h2>
      <ul>
       <li>
        IoT Network
       </li>
       <li>
        OSI Model
       </li>
       <li>
        TCP and UDP
       </li>
       <li>
        IP4 and IP6
       </li>
      </ul>
      <h2>
       IoT Transport layer protocol (1 hour)
      </h2>
      <ul>
       <li>
        Introduction of TCP &amp; UDP
       </li>
       <li>
        Difference between TCP/UDP Transport layer protocol.
       </li>
       <li>
        Practically testing the TCP v/s UDP by python socket programming.
       </li>
      </ul>
      <h2>
       HTTP IOT Protocol  (4 hours)
      </h2>
      <ul>
       <li>
        Introduction and structure of HTTP protocol
       </li>
       <li>
        Application
       </li>
       <li>
        Start with HTTP protocol GET/POST Method
       </li>
       <li>
        Work on python Flask library design web page
       </li>
       <li>
        Control thing from webpage using HTTP protocol
       </li>
       <li>
        Publish sensor data over webserver
       </li>
      </ul>
      <h2>
       CoAP IoT Protocol (5 hours)
      </h2>
      <ul>
       <li>
        CoAP Architecture
       </li>
       <li>
        Application
       </li>
       <li>
        Difference between HTTP and CoAP Protocol
       </li>
       <li>
        Design client and server using Python library and implement it.
       </li>
       <li>
        Interface using Aneska android app
       </li>
      </ul>
      <h2>
       MQTT IOT Protocol (10 hours)
      </h2>
      <ul>
       <li>
        Introduction to MQTT
       </li>
       <li>
        MQTT Subscribe/Publish
       </li>
       <li>
        MQTT Broker,QoS,Security
       </li>
       <li>
        Application
       </li>
       <li>
        MQTT with Raspberry Pi
       </li>
       <li>
        Installation of Mosquito MQTT broker
       </li>
       <li>
        Publish and Subscriber test on local server broker
       </li>
       <li>
        Test with multiple client.
       </li>
       <li>
        Getting started with MQTT on Raspberry Pi
       </li>
       <li>
        Installing Mosquitto on Raspberry pi
       </li>
       <li>
        Making pi a local MQTT broker
       </li>
       <li>
        Testing Publish and subscribe model on RPi
       </li>
       <li>
        Publishing data from PC
       </li>
       <li>
        Android to RPi over a local network
       </li>
       <li>
        Controlling Pi GPIOs using iot.eclipse.org MQTT broker
       </li>
       <li>
        Publishing live sensor data to io.adafruit.com
       </li>
       <li>
        Controlling devices from cloud platform
       </li>
       <li>
        Designing the IoT Gateway system
       </li>
       <li>
        Gathering data from multiple publishers
       </li>
       <li>
        Making Raspberry Pi as a IoT Gateway
       </li>
       <li>
        Analyzing sensor data in smartphone over internet
       </li>
       <li>
        Analyzing MQTT data packet using Wireshark software.
       </li>
      </ul>
      <h2>
       AMQP Protocol (5 hours)
      </h2>
      <ul>
       <li>
        Architecture of AMQP Protocol
       </li>
       <li>
        Application
       </li>
       <li>
        Producer, Consumer, Broker
       </li>
       <li>
        Architecture of RabbitMQ
       </li>
       <li>
        Message Exchange type
       </li>
       <li>
        Installation of message broker
       </li>
       <li>
        Asynchronous message communication between publisher and RabbitMQ
       </li>
       <li>
        Using pika implement
       </li>
      </ul>
      <h2>
       WebSocket (3 hours)
      </h2>
      <ul>
       <li>
        Understand Architecture of WebSocket
       </li>
       <li>
        Application
       </li>
       <li>
        Implement using python library TORNADO or pywebsocket
       </li>
       <li>
        MQTT over webSockets
       </li>
      </ul>
      <h2>
       OPC and UA (5 hours)
      </h2>
      <ul>
       <li>
        Understanding the OPC UA Specification
       </li>
       <li>
        Servers (using a free server simulator)
       </li>
       <li>
        Clients (using a free client)
       </li>
       <li>
        Information Modeling Fundamentals
       </li>
       <li>
        Security Implementations
       </li>
       <li>
        Server Implementation
       </li>
       <li>
        Implementing the Secure Channel
       </li>
       <li>
        Methods
       </li>
       <li>
        Alarms and Conditions
       </li>
       <li>
        Historical Access
       </li>
       <li>
        Client Implementation
       </li>
      </ul>
      <h2>
       Database (3 hours)
      </h2>
      <ul>
       <li>
        Introduction SQLite database
       </li>
       <li>
        Create table
       </li>
       <li>
        Syntax, query, operators
       </li>
       <li>
        SQLite -container
       </li>
       <li>
        Advance SQLite
       </li>
       <li>
        SQLite Interface
       </li>
       <li>
        Store sensor data/device information into database
       </li>
      </ul>
      <h2>
       IOT Applications with Data Logging and Reporting (15 hours)
      </h2>
      <ul>
       <li>
        IoT Platform-Connect, Monitor, Notify
       </li>
       <li>
        AWS
       </li>
       <li>
        IoT Core
       </li>
       <li>
        IoT Analytics
       </li>
       <li>
        SNS
       </li>
       <li>
        Dynamo db
       </li>
       <li>
        IBM Bluemix
       </li>
       <li>
        Node red
       </li>
       <li>
        Adafruit
       </li>
       <li>
        Ubidot
       </li>
       <li>
        IFTTT
       </li>
       <li>
        MyDevice
       </li>
       <li>
        Azure IoT
       </li>
       <li>
        IOT Hub
       </li>
       <li>
        Stream Analytics
       </li>
       <li>
        Storage
       </li>
       <li>
        Power BI
       </li>
       <li>
        Notification
       </li>
      </ul>
      <h2>
       Notify By Twitter,Mail,SMS (2 hours)
      </h2>
      <ul>
       <li>
        Get Notification by sms using Twilio Platform.
       </li>
       <li>
        Create twitter app and notify by tweet.
       </li>
       <li>
        Notify by mail using SMTP with Python
       </li>
      </ul>
      <h2>
       IoT Product &amp; Project Development (2 hours)
      </h2>
      <ul>
       <li>
        Agile Project Development
       </li>
       <li>
        Do�s &amp; Don�t for IoT Project Development
       </li>
       <li>
        Indian IoT Products
       </li>
       <li>
        Product Development Lifecycle
       </li>
      </ul>
      <h2>
       IoT Analytics (1 hours)
      </h2>
      <ul>
       <li>
        Introduction of IoT Analytics
       </li>
       <li>
        Machine Learning Technique
       </li>
       <li>
        Need of IoT Analytics
       </li>
       <li>
        Case Study
       </li>
      </ul>
      <h2>
       Sensor Analytics (6 hours)
      </h2>
      <ul>
       <li>
        Handling of sensor data,
       </li>
       <li>
        data pre-processing, and integration of different data sources,
       </li>
       <li>
        Heterogeneity and distributed nature,
       </li>
       <li>
        Selection of sensor to capture right set of data,
       </li>
       <li>
        Analog to digital conversion,
       </li>
       <li>
        Time and frequency domain analysis,
       </li>
       <li>
        Sampling theorem, Aliasing, Selection and cleaning
       </li>
       <li>
        Edge analytics
       </li>
      </ul>
      <h2>
       Statistical Analytics (6 hours)
      </h2>
      <ul>
       <li>
        Extracting meaning from data,
       </li>
       <li>
        Techniques for visualizing relationships in data
       </li>
       <li>
        Systematic techniques for understanding the relationships,
       </li>
       <li>
        Exploring data � Visualization, Correlation, and Regression, Probability distributions
       </li>
      </ul>
      <h2>
       Machine Learning (10 hours)
      </h2>
      <ul>
       <li>
        Concept of machine learning,
       </li>
       <li>
        Introduction to Python programming(numpy,pandas,matplotlib,sklearn)
       </li>
       <li>
        Regression � Linear and non-linear, Algorithms
       </li>
       <li>
        Logistics and non-linear regression,
       </li>
       <li>
        Predictive Analytics
       </li>
       <li>
        Classification, Algorithms � SVM, Decision trees, boosted decision trees, Na�ve Bayes,
       </li>
       <li>
        Feature selection methods for classification methods- Information value based, filter based and wrapper based,
       </li>
       <li>
        Algorithms and techniques for marketing analytics � Conjoint analysis, Hidden Markov models
       </li>
       <li>
        Time Series Analysis
       </li>
       <li>
        Clustering
       </li>
      </ul>
      <h2>
       Cloud IoT Analytics Platform (4 hours)
      </h2>
      <ul>
       <li>
        AWS � IoT Analytics
       </li>
       <li>
        Azure - Machine Learning Platform
       </li>
      </ul>
      <h2>
       IoT Security (1 hours)
      </h2>
      <ul>
       <li>
        Need of IoT Security
       </li>
       <li>
        Requirements and Basic Properties
       </li>
       <li>
        Main Challenges
       </li>
       <li>
        Confidentiality
       </li>
       <li>
        Integrity
       </li>
       <li>
        Availability
       </li>
       <li>
        Non-Repudiation
       </li>
      </ul>
      <h2>
       Cryptology  (3 hours)
      </h2>
      <ul>
       <li>
        Cipher
       </li>
       <li>
        Symmetric Key Algorithms (AES and DNS)
       </li>
       <li>
        Attacks
       </li>
       <li>
        Dictionary and Brute Force
       </li>
       <li>
        Lookup Tables
       </li>
       <li>
        Reverse Look Tables
       </li>
       <li>
        Rainbow Tables
       </li>
       <li>
        Hashing
       </li>
       <li>
        MD5, SHA256, SHA512, RipeMD, WI
       </li>
       <li>
        Objectives of Data Mining
       </li>
       <li>
        Key aspects of Data Mining
       </li>
       <li>
        Attack Surface and Threat Assessment
       </li>
       <li>
        Embedded Devices
       </li>
       <li>
        UART, SPI, I2C, JTAG
       </li>
      </ul>
      <h2>
       Network Attacks (1 hour)
      </h2>
      <ul>
       <li>
        Active/Passive Attacks
       </li>
       <li>
        Eavesdropping
       </li>
       <li>
        Identity Spoofing
       </li>
       <li>
        Man-In-The-Middle (MITM)
       </li>
      </ul>
      <h2>
       IOT Protocol Built-In Security Features
      </h2>
      <h2>
       On Transport Layer
      </h2>
      <ul>
       <li>
        SSL / TLS and DTLS
       </li>
      </ul>
      <h2>
       On Application Layer (1 hours)
      </h2>
      <ul>
       <li>
        MQTT
       </li>
       <li>
        CoAP
       </li>
       <li>
        XMPP
       </li>
       <li>
        AMQP
       </li>
       <li>
        Security Management
       </li>
       <li>
        Identity and Access Management
       </li>
       <li>
        Case Studies and Discussion
       </li>
      </ul>
      <h2>
       PLC and IoT (1 hour)
      </h2>
      <h2>
       Augmented Reality with IoT (5 hour)
      </h2>
      <h2>
       Block chain with IoT (6 hour)
      </h2>
      <h2>
       Use Cases (IoT Development, IoT Security, IoT Analytics) 5 hours
      </h2>
      <ul>
       <li>
        Health Care Sector
       </li>
       <li>
        Smart City (Smart parking, lighting, dustbin, trees, using LPWAN Technology)
       </li>
       <li>
        Telecommunication
       </li>
       <li>
        Energy or Power distribution
       </li>
       <li>
        Automobile
       </li>
       <li>
        Smart wearable device (Using NFC)
       </li>
       <li>
        Agriculture (Smart planting)
       </li>
       <li>
        Manufacturing - Industry 4.0(using PLC,SCADA)
       </li>
      </ul>
      <h2>
       Special Advance topic (15 hours)
      </h2>
      <ul>
       <li>
        Introduction to LPWAN
       </li>
       <li>
        Communication technology: Wired and wireless
       </li>
       <li>
        Internet of Things: Different wireless technologies
       </li>
       <li>
        Low Power Wide Area Network (LPWAN)
       </li>
       <li>
        Market players
       </li>
      </ul>
      <h2>
       LoRa (Radio Modulation)
      </h2>
      <ul>
       <li>
        LoRa characteristics
       </li>
       <li>
        Radio propagation
       </li>
       <li>
        LoRa modulation
       </li>
       <li>
        Frequency bands
       </li>
      </ul>
      <h2>
       LoRaWAN Architecture
      </h2>
      <ul>
       <li>
        Overview
       </li>
       <li>
        LoRaWAN network server
       </li>
       <li>
        Device classes
       </li>
       <li>
        Scalability
       </li>
       <li>
        Uplink and downlink messages
       </li>
      </ul>
      <h2>
       LoRaWAN Hardware
      </h2>
      <ul>
       <li>
        Gateways
       </li>
       <li>
        Nodes
       </li>
       <li>
        Prototyping and what hardware to choose
       </li>
       <li>
        Production
       </li>
       <li>
        Power consumption
       </li>
       <li>
        Antennas
       </li>
      </ul>
      <h2>
       LoRaWAN Solutions
      </h2>
      <ul>
       <li>
        Case studies and examples
       </li>
       <li>
        Sketch on the node libraries
       </li>
       <li>
        Payload functions
       </li>
       <li>
        Setting up end-to-end application
       </li>
       <li>
        Protocols: MQTT, HTTP integration
       </li>
      </ul>
      <h2>
       Data modeling and processing
      </h2>
      <ul>
       <li>
        Data storage and visualization
       </li>
       <li>
        Cloud platforms and integrations
       </li>
       <li>
        Tools: Grafana, InfluxDB, NodeRed
       </li>
      </ul>
      <h2>
       LoRaWAN Security
      </h2>
      <ul>
       <li>
        OTAA/ABP Frame counters
       </li>
       <li>
        Secure elements
       </li>
       <li>
        Encryption and decryption
       </li>
      </ul>
      <h2>
       LoRaWAN Deployments
      </h2>
      <ul>
       <li>
        Placing gateways and site surveys
       </li>
       <li>
        Enabling ADR (scalability)
       </li>
       <li>
        NOC, alerting, updates
       </li>
       <li>
        Mass commissioning
       </li>
       <li>
        OTA updates
       </li>
       <li>
        Security: setting up own handler
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="iottraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="6 + 23 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="29">
       <input name="url" type="hidden" value="/iottraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>