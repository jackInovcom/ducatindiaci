<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     C++ And Data Structure Training
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     C++ PROGRAMMING
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Developments in software technology have been always dynamic. New tools and new techniques are announced everyday; as a result, engineers and software industries need to track the new changes to continue the development. However, there are many programming languages, but the Object-Oriented Programming Language is the most preferred one in the software industry. It offers the user a new approach to build the real-time application along with a way to cope the complexity of other programs. Among the OOPs languages available, C++ is the most widely used one. Moreover, it is the most versatile language used for handling very large programs and suitable for programming task including development of compilers, editors and any real-time complications The ability to map the real life problems, makes C++ the most popular and the most important course for the software industry. It would be indeed to say that there are many colleges and institutes, which give training of this course to students. Among the institutes available, DUCAT is one among them offering this course for a term of 2 months. DUCAT has extensive experience trainers to guide the students for real-time projects. Apart from the course, it also assures the enrolled students for 100% placement opportunity. Most of the times, the seats are booked in advance. Therefore, rush to the nearest centre and be above the developer.
     </p>
     <div class="contentAcc">
      <h2>
       C++ Language Contents:
      </h2>
      <ul>
       <li>
        Introduction to C++ Language
       </li>
       <li>
        Difference and Similarities between C and C++
       </li>
       <li>
        Role Of Compilers and Assemblers
       </li>
       <li>
        Introduction to C++ Basic
       </li>
       <li>
        Flow Control Statements
       </li>
       <ul>
        <li>
         Jump statements
        </li>
        <li>
         Conditional Statements
        </li>
        <li>
         Iteration statements
        </li>
       </ul>
      </ul>
      <h2>
       Arrays:
      </h2>
      <ul>
       <li>
        Introduction to Arrays
       </li>
       <li>
        Several examples of Arrays like insert, delete, counter occurrence of items etc.
       </li>
       <li>
        Searching
       </li>
       <li>
        Sorting
       </li>
       <li>
        2 D Array
       </li>
       <li>
        Several Examples of 2 D arrays
       </li>
       <li>
        Multidimensional Array s
       </li>
      </ul>
      <h2>
       Strings:
      </h2>
      <ul>
       <li>
        Introduction to Strings
       </li>
       <li>
        String handling built in functions
       </li>
       <li>
        Several examples of Strings
       </li>
       <li>
        Array of Strings
       </li>
       <li>
        Searching in Array of Strings
       </li>
       <li>
        Sorting in Array of Strings
       </li>
      </ul>
      <h2>
       POINTERS
      </h2>
      <ul>
       <li>
        Introduction to pointers
       </li>
       <li>
        Pointer expressions
       </li>
       <li>
        Types of Pointers
       </li>
       <ul>
        <li>
         Void Pointer
        </li>
        <li>
         NULL Pointer
        </li>
        <li>
         Wild Pointer
        </li>
        <li>
         Dangling Pointer
        </li>
       </ul>
       <li>
        Various examples of pointers
       </li>
       <li>
        Pointer Arithmetic's
       </li>
       <li>
        Array using pointers
       </li>
       <li>
        Array of ponter
       </li>
       <li>
        chaining pointer
       </li>
       <li>
        String using pointers
       </li>
       <li>
        Pointers in Functions
       </li>
      </ul>
      <h2>
       FUNCTIONS
      </h2>
      <ul>
       <li>
        Introduction to functions
       </li>
       <li>
        Types of functions
       </li>
       <li>
        Nesting of functions
       </li>
       <li>
        Various examples of functions
       </li>
       <li>
        Strings passing is functions
       </li>
       <li>
        Array Passing in Functions
       </li>
       <li>
        Pointer passing is functions
       </li>
       <li>
        Function Returning Address
       </li>
       <li>
        Function returning address
       </li>
       <li>
        Recursion
       </li>
       <li>
        Various Examples and Interview Questions on Recursion and Function
       </li>
       <li>
        Storage classes
       </li>
      </ul>
      <h2>
       STRUCTURE
      </h2>
      <ul>
       <li>
        Introduction to structure
       </li>
       <li>
        Advantages of structure
       </li>
       <li>
        Array of structure
       </li>
       <li>
        Structure using pointer
       </li>
       <li>
        Structure with functions
       </li>
       <li>
        Applications of structure
       </li>
      </ul>
      <h2>
       DYNAMIC MEMORY ALLOCATION
      </h2>
      <ul>
       <li>
        Introduction to dynamic memory allocation
       </li>
       <li>
        Advantage of dynamic memory allocation
       </li>
       <li>
        new and delete operators
       </li>
       <li>
        Array implementation using dynamic memory allocation
       </li>
      </ul>
      <h2>
       Oops Introduction
      </h2>
      <ul>
       <li>
        Oops vs. Procedural Programming Approach
       </li>
       <li>
        Oops Implementation
       </li>
       <ul>
        <li>
         Accessing class members
        </li>
        <li>
         Array of objects
        </li>
        <li>
         Types of members of class
        </li>
        <ul>
         <li>
          Instance members
         </li>
         <li>
          Static members
         </li>
        </ul>
       </ul>
       <li>
        Scope resolution Operator (::)
       </li>
       <li>
        Oops Approaches
       </li>
       <ul>
        <li>
         Encapsulation
        </li>
        <ul>
         <li>
          Nesting of Class(i.e. Outer Class, Inner Class, Local Class)
         </li>
        </ul>
        <li>
         Polymorphism
        </li>
        <ul>
         <li>
          Function Overloading
         </li>
         <li>
          Constructor &amp; Destructor
         </li>
         <ul>
          <li>
           Deep Copy
          </li>
          <li>
           Shallow Copy
          </li>
         </ul>
         <li>
          Operator Overloading
         </li>
         <li>
          Function Overriding
         </li>
        </ul>
       </ul>
       <li>
        Reference variable
       </li>
       <li>
        Pointers
       </li>
       <ul>
        <li>
         Class object using pointer
        </li>
        <li>
         Array of objects using pointer
        </li>
        <li>
         This pointer Etc.
        </li>
       </ul>
       <li>
        Inheritance
       </li>
       <ul>
        <li>
         Single Inheritance
        </li>
        <li>
         Multiple Inheritance
        </li>
        <li>
         Multilevel Inheritance
        </li>
        <li>
         Hierarchical inheritance
        </li>
        <li>
         Hybrid Inheritance
        </li>
        <li>
         Need of Virtual
        </li>
       </ul>
       <li>
        Friend Function and Friend Class
       </li>
       <li>
        Function Overriding
       </li>
       <ul>
        <li>
         Binding Types
        </li>
        <li>
         Static Binding and Dynamic Binding
        </li>
        <li>
         Up casting and Down casting
        </li>
        <li>
         Virtual Function
        </li>
       </ul>
       <li>
        Abstraction
       </li>
       <ul>
        <li>
         Data Abstraction
        </li>
        <li>
         Abstract Class
        </li>
        <li>
         Pure Virtual Function
        </li>
       </ul>
       <li>
        Inline Functions
       </li>
       <li>
        Composition and Aggregation
       </li>
      </ul>
      <h2>
       Exception Handling
      </h2>
      <ul>
       <li>
        Introduction to Exception Handling
       </li>
       <li>
        Need of Exception Handling
       </li>
       <li>
        try, throw, catch
       </li>
       <li>
        Multiple catch blocks
       </li>
      </ul>
      <h2>
       FILE MANAGEMENT
      </h2>
      <ul>
       <li>
        Introduction to file management
       </li>
       <li>
        File opening modes
       </li>
       <li>
        Opening and closing a file
       </li>
       <li>
        Input output operations on file
       </li>
       <li>
        Error handling
       </li>
       <li>
        Applications of file management
       </li>
      </ul>
      <h2>
       Multithreading:
      </h2>
      <ul>
       <li>
        Thread introduction
       </li>
       <li>
        Thread Synchronization
       </li>
       <li>
        Life cycle of thread
       </li>
       <li>
        Deal Lock situation
       </li>
      </ul>
      <h2>
       Templates (Generic Programming):
      </h2>
      <ul>
       <li>
        Introduction to Templates and Generic Programming
       </li>
       <li>
        Advantages of template
       </li>
       <li>
        Template function and Template class
       </li>
      </ul>
      <h2>
       Standard Templates Library
      </h2>
      <ul>
       <li>
        Container
       </li>
       <li>
        Class
       </li>
       <li>
        Functions
       </li>
       <li>
        Iterators
       </li>
       <li>
        List class
       </li>
       <li>
        Stack class
       </li>
       <li>
        Queue Class
       </li>
       <li>
        De Queue Etc.
       </li>
      </ul>
      <h2>
       DATA STRUCTURE AND ALGORITHMS CONTENT (using C++)
      </h2>
      <h2>
       INTRODUCTION TO DATA STRUCTURE AND ALGORITHMS
      </h2>
      <ul>
       <li>
        What is data structure
       </li>
       <li>
        Benefits of data structure
       </li>
       <li>
        Types of data structure
       </li>
       <li>
        Introduction to algorithms
       </li>
       <li>
        Types of Algorithms
       </li>
       <li>
        Time and Space Complexity
       </li>
       <li>
        Interview Questions
       </li>
      </ul>
      <h2>
       LINEAR DATA STRUCTURE
      </h2>
      <ul>
       <li>
        Array
       </li>
       <li>
        String
       </li>
       <li>
        Link list
       </li>
       <ul>
        <li>
         Introduction to link list
        </li>
        <li>
         Array vs. link lists
        </li>
        <li>
         Types of link lists
        </li>
        <li>
         Implementation of link list
        </li>
        <li>
         Singly link list
        </li>
        <li>
         Insertion(at first position, last position and at used specific position) , deletion(at first position, last
position and at used specific position) , traversing operations in Singly linked list
        </li>
        <li>
         Doubly link list
        </li>
        <li>
         Insertion (at first position, last position and at used specific position), deletion(at first position, last
position and at used specific position) , traversing operations in Doubly linked list
        </li>
        <li>
         Circular link list
        </li>
        <li>
         Insertion(at first position, last position and at used specific position) , deletion(at first position, last
position and at used specific position) , traversing operations in Circular linked list
        </li>
        <li>
         Application of link list
        </li>
        <li>
         Interview Questions
        </li>
       </ul>
       <li>
        Stack
       </li>
       <ul>
        <li>
         Introduction to stack
        </li>
        <li>
         Stack using array
        </li>
        <li>
         Stack using linked list
        </li>
        <li>
         Applications of stack
        </li>
        <li>
         Reverse Polish Notations(Infix to Postfix and Infix to Prefix)
        </li>
        <li>
         Interview Questions
        </li>
       </ul>
       <li>
        Queue
       </li>
       <ul>
        <li>
         Introduction to queue
        </li>
        <li>
         Queue using array
        </li>
        <li>
         Queue using linked list
        </li>
        <li>
         Applications of queue
        </li>
        <li>
         Introduction to circular queue
        </li>
        <li>
         Application of Circular queue
        </li>
        <li>
         Introduction to DeQueue(Double Ended Queue)
        </li>
        <li>
         Application of Dequeue
        </li>
        <li>
         Priority Queue
        </li>
        <li>
         Interview Questions
        </li>
       </ul>
       <h2>
        Non-linear data structure
       </h2>
       <ul>
        <li>
         Tree
        </li>
        <ul>
         <li>
          Introduction to trees
         </li>
         <li>
          Types of trees
         </li>
         <li>
          Implementation of tress
         </li>
         <li>
          Binary tree
         </li>
         <li>
          Binary search tree
         </li>
         <li>
          AVL tree
         </li>
         <li>
          Threaded binary tree
         </li>
         <li>
          M way tree
         </li>
         <li>
          M way search tree
         </li>
         <li>
          B tree
         </li>
         <li>
          Heap
         </li>
         <li>
          Various operations on trees
         </li>
         <li>
          Application of tress
         </li>
         <li>
          Interview Questions
         </li>
        </ul>
       </ul>
       <h2>
        Searching and sorting
       </h2>
       <ul>
        <li>
         Searching in arrays
        </li>
        <li>
         Searching in strings
        </li>
        <ul>
         <li>
          Linear Search
         </li>
         <li>
          Binary Search
         </li>
        </ul>
        <li>
         Sorting
        </li>
        <li>
         Various sorting techniques
        </li>
        <ul>
         <li>
          Selection sort
         </li>
         <li>
          Bubble sort
         </li>
         <li>
          Insertion sort
         </li>
         <li>
          Quick sort
         </li>
         <li>
          Heap sort
         </li>
         <li>
          Merge sort
         </li>
         <li>
          Radix Sort
         </li>
        </ul>
       </ul>
       <h2>
        Graph
       </h2>
       <ul>
        <li>
         Introduction of graph
        </li>
        <li>
         Types of graphs
        </li>
        <li>
         Implementation of graph using Adj. Matrix and Adj. list
        </li>
        <li>
         Various operations on graphs
        </li>
        <li>
         Shortest path search in graph
        </li>
        <ul>
         <li>
          Floyd Warshall Algorithm
         </li>
         <li>
          Dijkstra Algorithm
         </li>
        </ul>
        <li>
         Minimum spanning tree
        </li>
        <ul>
         <li>
          Kruskal's Algorithm
         </li>
         <li>
          Prims Algorithm
         </li>
        </ul>
        <li>
         Applications of graphs
        </li>
        <li>
         Interview Questions
        </li>
       </ul>
       <h2>
        Hashing
       </h2>
       <ul>
        <li>
         Introduction of hashing
        </li>
        <li>
         Hash table
        </li>
        <li>
         Applications of hashing
        </li>
       </ul>
       <h2>
        Interview Questions
       </h2>
       <h2>
        Project
       </h2>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="cpptraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="31 + 94 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="125">
       <input name="url" type="hidden" value="/cpptraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>