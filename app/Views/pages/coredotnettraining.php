<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     .NET CORE 2.0 Training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Core dotnet
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h2>
      MICROSOFT .NET CORE 2.0 TRAINING
     </h2>
     <h4>
      About the Course
     </h4>
     <p>
      ASP.Net MVC Core is the new web framework from Microsoft. ASP.NET Core is the framework you want to use for web development with .NET. It has been redesigned from the ground upwards to be fast, flexible, modern, and work across different programs. At the end this course, you will receive everything you need to embark on using ASP.NET Core and write an application that can create, edit, and view data from a database.
     </p>
     <h4>
      Audience:
     </h4>
     <p>
      This tutorial is designed for software programmers who would like to learn the basics of ASP.NET Core from scratch. The .NET Core Development course is primarily designed for .NET Beginners/Professionals who want to learn how to develop modern apps using .NET Core. In this program, you will learn about .NET Core CLI, Asp .Net Core, Middleware, MVC Core Framework, Request Pipeline for Asp .Net Core, Html Helpers, Tag Helpers, Custom Helpers, Validation and Custom Validation, Querying Database using Entity Framework Core, ASP.NET Core Authentication, and Authorization, Front End Frameworks and tools &amp; finally publish it on IIS.
     </p>
     <div class="contentAcc">
      <h2>
       BUILDING CONSOLE APPS WITH .NET CORE
      </h2>
      <ul>
       <li>
        Module Intro
       </li>
       <li>
        What to Install
       </li>
       <li>
        dotnet new
       </li>
       <li>
        dotnet restore, build, and run
       </li>
       <li>
        Create a .NET Core Console App in Visual Studio
       </li>
       <li>
        Visual Studio Calls the dotnet CLI
       </li>
       <li>
        How Code Executes with the .NET Framework
       </li>
       <li>
        How Code Executes with .NET Core
       </li>
       <li>
        Cross Platform and Open Source
       </li>
       <li>
        Modularity
       </li>
       <li>
        Project Uses All Files on Disk
       </li>
       <li>
        Default Package References from Microsoft.NETCore.App
       </li>
       <li>
        Adding Package References
       </li>
      </ul>
      <h2>
       Software Engineering Models and Concept:
      </h2>
      <ul>
       <li>
        What is Software Development Life Cycle?
       </li>
       <li>
        Phases of a SDLC
       </li>
       <li>
        Project initiation and planning
       </li>
       <li>
        Feasibility study
       </li>
       <li>
        System design
       </li>
       <li>
        Coding
       </li>
       <li>
        Testing
       </li>
       <li>
        Implementation
       </li>
       <li>
        Maintenance
       </li>
      </ul>
      <h2>
       Business Requirement Specification (Project Based)
      </h2>
      <ul>
       <li>
        Objective of Project
       </li>
       <li>
        DFD
       </li>
       <li>
        UML Diagram
       </li>
       <li>
        Class Diagram
       </li>
       <li>
        ER Diagram
       </li>
      </ul>
      <h2>
       System Requirement Database Structure (Project Based)
      </h2>
      <ul>
       <li>
        Database Table Structure
       </li>
       <li>
        Database Design
       </li>
       <li>
        DDL
       </li>
       <li>
        DML
       </li>
       <li>
        Stored Procedure
       </li>
       <li>
        Database Security
       </li>
      </ul>
      <h2>
       ASP.NET CORE INTRODUCTION
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Setting up npm
       </li>
       <li>
        Serving File from node_modules
       </li>
       <li>
        Styling with Bootstrap
       </li>
       <li>
        ASP.NET Core Environment Setup
       </li>
       <li>
        What is ASP.NET Core?
       </li>
       <li>
        ASP.NET Core Features and its advantages.
       </li>
       <li>
        ASP.NET Core Framework Architecture
       </li>
       <li>
        Understanding MVC Design Pattern
       </li>
       <li>
        Understanding ASP.Net Core MVC Design Pattern
       </li>
       <li>
        ASP.NET Core vs. ASP.NET MVC vs. ASP.NET Web Forms
       </li>
       <li>
        Choose between .Net Core and .Net Framework Runtime.
       </li>
       <li>
        Choose between ASP.NET Core and ASP.NET.
       </li>
      </ul>
      <h2>
       BUILDING YOUR FIRST ASP.NET CORE APPLICATION
      </h2>
      <ul>
       <li>
        Environment Setup and Configuration options for ASP.NET Core Application.
       </li>
       <li>
        Understanding of ASP.NET Core Project Layout.
       </li>
       <li>
        Introduction to .NET Command Line Interface (CLI).
       </li>
       <li>
        Understanding of .NET CLI Commands.
       </li>
       <li>
        Creating and Running Project using .NET CLI.
       </li>
       <li>
        Understanding of .NET Core development using Visual Studio Code Editor.
       </li>
       <li>
        Understanding steps involved in Request Life Cycle of ASP.NET Core.
       </li>
       <li>
        Working with Multiple Environments.
       </li>
      </ul>
      <h2>
       STARTUP AND MIDDLEWARE
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        How Middleware Works
       </li>
       <li>
        Using IApplicationBuilder
       </li>
       <li>
        Showing Exception Details
       </li>
       <li>
        Middleware to Match the Environment
       </li>
       <li>
        Serving Files
       </li>
       <li>
        Setting up ASP.NET MVC Middleware
       </li>
       <li>
        Summary
       </li>
      </ul>
      <h2>
       CONTROLLERS IN THE MVC FRAMEWORK
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        The Model View Controller Design Pattern
       </li>
       <li>
        Routing
       </li>
       <li>
        Conventional Routes
       </li>
       <li>
        Attribute Routes
       </li>
       <li>
        Action Results
       </li>
       <li>
        Rendering Views
       </li>
       <li>
        Summary
       </li>
      </ul>
      <h2>
       MODELS IN THE MVC FRAMEWORK
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Models and View Models
       </li>
       <li>
        Understanding ASP.NET Core MVC Validation
       </li>
       <li>
        Need of Server Side and Client-Side Validation
       </li>
       <li>
        Accepting Form Input
       </li>
       <li>
        POST Redirect GET Pattern
       </li>
       <li>
        Model Validation with Data Annotations
       </li>
       <li>
        Summary
       </li>
      </ul>
      <h2>
       RAZOR VIEWS
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Understanding of Razor View Engine.
       </li>
       <li>
        Difference between Razor View Engine and Web Form Engine.
       </li>
       <li>
        Understanding of Razor Syntax.
       </li>
       <li>
        Understanding of passing data from View to Controller.
       </li>
       <li>
        Introduction to Razor Pages in ASP.NET Core
       </li>
       <li>
        Understanding and Creating View Component.
       </li>
       <li>
        Layout Views
       </li>
       <li>
        _ViewStart
       </li>
       <li>
        _ViewImports
       </li>
       <li>
        Razor Pages
       </li>
       <li>
        An Edit Form
       </li>
       <li>
        Partial Views
       </li>
       <li>
        Summary
       </li>
      </ul>
      <h2>
       HELPERS
      </h2>
      <ul>
       <li>
        Understanding Html Helpers
       </li>
       <li>
        Types of Html Helpers
       </li>
       <li>
        Built-In Html Helpers
       </li>
       <li>
        Tag Helpers
       </li>
       <li>
        Inline Helpers
       </li>
       <li>
        Custom Helpers Url helpers
       </li>
      </ul>
      <h2>
       USING THE ENTITY FRAMEWORK
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        SQL Server LocalDB
       </li>
       <li>
        Installing the Entity Framework
       </li>
       <li>
        Implementing a DbContext
       </li>
       <li>
        Configuring the Entity Framework Services
       </li>
       <li>
        Entity Framework Migrations
       </li>
       <li>
        Up and Running
       </li>
       <li>
        Summary
       </li>
      </ul>
      <h2>
       DEPENDENCY INJECTION IN ASP.NET CORE
      </h2>
      <ul>
       <li>
        Understanding Dependency Injection.
       </li>
       <li>
        Advantages of Dependency Injection.
       </li>
       <li>
        Using of Dependency Injection in ASP.NET Core.
       </li>
      </ul>
      <h2>
       ASP.NET CORE AUTHENTICATION AND AUTHORIZATION
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        An Overview of ASP.NET Identity
       </li>
       <li>
        An Overview of OpenID Connect
       </li>
       <li>
        Using Secure Connections and Registering the App
       </li>
       <li>
        OpenID Connect Configuration
       </li>
       <li>
        Authentication Services and Middleware
       </li>
       <li>
        User Authorization
       </li>
       <li>
        Identities and Claims
       </li>
       <li>
        Summary
       </li>
      </ul>
      <h2>
       HOSTING AND DEPLOYMENT
      </h2>
      <ul>
       <li>
        Host ASP.NET Core on Windows with IIS
       </li>
       <li>
        Host an ASP.NET Core app in a Windows Service
       </li>
       <li>
        Host ASP.NET Core in Docker containers
       </li>
      </ul>
      <h2>
       ASP.Net Core WEBAPI
      </h2>
      <ul>
       <li>
        What is WebAPI?
       </li>
       <li>
        Why select Web API?
       </li>
       <li>
        Advantage of WebAPI
       </li>
       <li>
        Understanding Http Request Verbs &amp; Constraints
       </li>
       <li>
        Http Response Http Status Code
       </li>
       <li>
        Creating a Web API Project
       </li>
       <li>
        Implementing GET, PUT, POST &amp; DELETE method
       </li>
       <li>
        ASP NET Web API and SQL Server
       </li>
       <li>
        Web API Content Negotiation
       </li>
       <li>
        Web API MediaTypeFormatter
       </li>
       <li>
        Web API query string parameters
       </li>
       <li>
        FromBody and FromUri in Web API
       </li>
       <li>
        Call ASP NET Web API from jQuery
       </li>
       <li>
        Calling Web API service in a cross domain using jQuery ajax
       </li>
       <li>
        Cross origin resource sharing ASP NET Web API
       </li>
       <li>
        Enable SSL in Visual Studio Development Server
       </li>
       <li>
        ASP NET Web API enable HTTPS
       </li>
       <li>
        Implementing basic authentication in Web API
       </li>
       <li>
        Call web api service with basic authentication using jquery ajax
       </li>
       <li>
        Web API token authentication
       </li>
       <li>
        Web API user registration
       </li>
       <li>
        Using asp net identity with Web API
       </li>
       <li>
        Using fiddler to test Web API token based authentication
       </li>
       <li>
        Web API login &amp; logout page
       </li>
       <li>
        How to get authenticated user identity name in asp net web api
       </li>
       <li>
        ASP NET Web API Google &amp; Facebook authentication
       </li>
       <li>
        Web API attribute routing constraints
       </li>
       <li>
        Generating links using route names in asp net web api
       </li>
       <li>
        IHttpActionResult vs HttpResponseMessage
       </li>
       <li>
        Web API versioning using querystring parameter
       </li>
       <li>
        Web API versioning using a custom header &amp; accept header
       </li>
       <li>
        Web API versioning using Web API versioning using custom media types
       </li>
      </ul>
      <h2>
       jQuery
      </h2>
      <ul>
       <li>
        What is jQuery
       </li>
       <li>
        What is $document ready function in jquery
       </li>
       <li>
        Benefits of using CDN
       </li>
       <li>
        jQuery #id selector
       </li>
       <li>
        jQuery Element Selector
       </li>
       <li>
        Working with jQuery class , attribute and value selector
       </li>
       <li>
        jQuery case insensitive attribute selector
       </li>
       <li>
        jQuery input vs input
       </li>
       <li>
        Working with jquery Controls
       </li>
       <li>
        jQuery each function
       </li>
       <li>
        jQuery method chaining
       </li>
       <li>
        What is JSON
       </li>
       <li>
        Convert JSON object to string &amp; string to object
       </li>
       <li>
        jQuery wrap elements
       </li>
       <li>
        jQuery insert element before and after
       </li>
       <li>
        Difference between $ each and each
       </li>
       <li>
        Difference between each and map in jquery
       </li>
       <li>
        Working with JQuery Event
       </li>
       <li>
        Implementing jQuery image gallery
       </li>
       <li>
        jQuery image slideshow with thumbnails
       </li>
       <li>
        Simple jquery progress bar
       </li>
       <li>
        jquery ajax load Razor page
       </li>
       <li>
        jquery ajax get function
       </li>
       <li>
        load json data using jquery ajax
       </li>
       <li>
        jquery ajax get xml data
       </li>
       <li>
        jquery ajax method
       </li>
       <li>
        Calling web API using jquery ajax
       </li>
       <li>
        Handling json data returned from web API
       </li>
       <li>
        Handling json arrays returned from web API with jquery
       </li>
       <li>
        Save data using asp net web API and jquery ajax
       </li>
       <li>
        Check if username exists in database with ajax
       </li>
       <li>
        How to suggest available username
       </li>
       <li>
        Cascading dropdownlist using jquery and asp net
       </li>
       <li>
        jQuery datatables plugin
       </li>
       <li>
        jQuery datatables get data from database table
       </li>
       <li>
        jQuery datatables individual column search
       </li>
       <li>
        jQuery datatable show hide columns
       </li>
       <li>
        jQuery datatables stored procedure for paging sorting and searching
       </li>
      </ul>
      <h2>
       TypeScript
      </h2>
      <ul>
       <li>
        Basic Types
       </li>
       <li>
        Variable Declarations
       </li>
       <li>
        Classes
       </li>
       <li>
        Functions
       </li>
       <li>
        Interfaces
       </li>
       <li>
        Generics
       </li>
       <li>
        Enums
       </li>
       <li>
        Type Inference
       </li>
       <li>
        Namespaces
       </li>
       <li>
        Modules
       </li>
       <li>
        Mixins
       </li>
       <li>
        Iterators and Generators
       </li>
       <li>
        TypeScript 2.X Features
       </li>
      </ul>
      <h2>
       ASP.Net Core AngularCLI
      </h2>
      <ul>
       <li>
        What is Angular CLI
       </li>
       <li>
        Installing Angular CLI
       </li>
       <li>
        Customize Command Prompt
       </li>
       <li>
        Configuration of Angular CLI file
       </li>
       <li>
        Understanding Angular CLI project structure
       </li>
       <li>
        Angular CLI component
       </li>
       <li>
        Angular CLI Service
       </li>
       <li>
        Angular CLI Module
       </li>
       <li>
        Angular CLI generate directives, pipes and routing guards
       </li>
       <li>
        Angular CLI generate class, interface and Enum
       </li>
       <li>
        Introduction of Linting TypeScript
       </li>
       <li>
        Implementing routing in separate module in angular CLI
       </li>
      </ul>
      <h2>
       ASP.Net Core Angular CRUD
      </h2>
      <ul>
       <li>
        Angular project setup
       </li>
       <li>
        Reading data in angular
       </li>
       <li>
        Understanding Angular routing and navigation
       </li>
       <li>
        Working of Angular base href
       </li>
       <li>
        Difference b/w Angular value vs ngvalue
       </li>
       <li>
        Pass data from parent to child component in angular
       </li>
       <li>
        Angular route params
       </li>
       <li>
        Implementing Angular optional route parameters
       </li>
       <li>
        Working with Data filtering in angular component
       </li>
       <li>
        Create observable from array
       </li>
       <li>
        Implementing Edit form in angular
       </li>
       <li>
        Implementing Delete form in angular
       </li>
       <li>
        Understanding Angular content projection
       </li>
       <li>
        Understanding Angular client server architecture
       </li>
       <li>
        Implementing Angular http client get example
       </li>
       <li>
        Working with Angular http client error handling
       </li>
       <li>
        Working with Angular http client Post
       </li>
       <li>
        Working with Angular http client Put
       </li>
       <li>
        Working with Angular http client Delete
       </li>
      </ul>
      <h2>
       ASP.Net Core Angular6
      </h2>
      <ul>
       <li>
        Introductions of Angular6
       </li>
       <li>
        New Features in Angular6
       </li>
       <li>
        Install Bootstrap for Angular 6
       </li>
       <li>
        Angular form control and form group
       </li>
       <li>
        Angular setvalue and patchvalue methods
       </li>
       <li>
        Angular formbuilder example
       </li>
       <li>
        Implementing Form validation in Angular
       </li>
       <li>
        Angular reactive forms validation
       </li>
       <li>
        Using reactive form custom validator with parameter
       </li>
       <li>
        Angular reactive forms cross field validation
       </li>
       <li>
        Creating formarray of formgroup objects in Angular
       </li>
       <li>
        Working with Angular dynamic forms
       </li>
       <li>
        Remove dynamically created form controls in angular
       </li>
       <li>
        Angular reactive forms post ,put &amp; edit, example
       </li>
       <li>
        Lazy loading in angular
       </li>
       <li>
        Preloading angular modules
       </li>
       <li>
        Angular custom preloading strategy
       </li>
      </ul>
      <h2>
       Technical Design &amp; Development (Project Based)
      </h2>
      <ul>
       <li>
        Working with Project
       </li>
       <li>
        Programming Language: C# (MVC Razor)
       </li>
       <li>
        WebApi Service
       </li>
       <li>
        Designing Tools
       </li>
       <li>
        CSHTML
       </li>
       <li>
        Using CSS
       </li>
       <li>
        Using Ajax
       </li>
       <li>
        Using JQuery
       </li>
       <li>
        Using Angular
       </li>
      </ul>
      <h2>
       Interview Question and Prepartion for placement
      </h2>
      <ul>
       <li>
        Technical Interview Preparation
       </li>
       <li>
        Mock Interview preparation
       </li>
       <li>
        HR Session
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="coredotnettraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="28 + 86 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="114">
       <input name="url" type="hidden" value="/coredotnettraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>