<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Contact Us
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Contact Us
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <div class="contentAcc">
     <h2>
      DUCAT, NOIDA
     </h2>
     <div class="row">
      <div class="col-md-3">
       <h5>
        DUCAT, NOIDA
       </h5>
       <p>
        A - 43 &amp; A - 52 Sector - 16, Noida (U.P) (Near McDonalds)
       </p>
       <hr/>
       <h5>
        PHONE:
       </h5>
       <p>
        0120-4646464
       </p>
       <hr/>
       <h5>
        MOBILE OR WHATSAPP:
       </h5>
       <p>
        +91- 9871055180
       </p>
       <hr/>
       <h5>
        EMAIL:
       </h5>
       <p>
        info@ducatindia.com
       </p>
      </div>
      <!-- End Of Col Md 3 -->
      <div class="col-md-9">
       <iframe frameborder="0" height="350" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d112109.34354670039!2d77.31377161328128!3d28.58726486652894!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xc4ff2d18067f51e0!2sDUCAT!5e0!3m2!1sen!2sin!4v1420181071374" style="border:0" width="100%">
       </iframe>
      </div>
      <!-- End Of Col Md 9 -->
     </div>
     <!-- End Of Row -->
     <h2>
      DUCAT, GHAZIABAD
     </h2>
     <div class="row">
      <div class="col-md-3">
       <p>
        Anand Industrial Estate Near ITS College, Mohan Nagar Ghaziabad (U.P.)
       </p>
       <hr/>
       <h5>
        PHONE:
       </h5>
       <p>
        0120-4835400
       </p>
       <hr/>
       <h5>
        MOBILE:
       </h5>
       <p>
        +91-9810851363 / +91-9818106660
       </p>
       <hr/>
       <h5>
        EMAIL:
       </h5>
       <p>
        mohannagar@ducatindia.com, ghaziabad@ducatindia.com
       </p>
      </div>
      <!-- End Of Col Md 3 -->
      <div class="col-md-9">
       <iframe allowfullscreen="" frameborder="0" height="450" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3500.411534082992!2d77.3859451!3d28.6773337!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x610ea7095f736a3!2sDUCAT+Ghaziabad!5e0!3m2!1sen!2sin!4v1546952871498" style="border:0" width="600">
       </iframe>
      </div>
      <!-- End Of Col Md 9 -->
     </div>
     <!-- End Of Row -->
     <h2>
      DUCAT, FARIDABAD
     </h2>
     <div class="row">
      <div class="col-md-3">
       <p>
        SCO-32, First Floor Sector-16, Faridabad (Haryana)
       </p>
       <hr/>
       <h5>
        PHONE:
       </h5>
       <p>
        0129-4150605 - 09
       </p>
       <hr/>
       <h5>
        MOBILE OR WHATSAPP:
       </h5>
       <p>
        +91-9811612707
       </p>
       <hr/>
       <h5>
        EMAIL:
       </h5>
       <p>
        faridabad@ducatindia.com
       </p>
      </div>
      <!-- End Of Col Md 3 -->
      <div class="col-md-9">
       <iframe frameborder="0" height="350" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d56109.34954919905!2d77.324524!3d28.484535!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cddb1cc598675%3A0xa218e95b7cb7aeaf!2sDucat!5e0!3m2!1sen!2sin!4v1420181285504" style="border:0" width="100%">
       </iframe>
      </div>
      <!-- End Of Col Md 9 -->
     </div>
     <!-- End Of Row -->
     <h2>
      DUCAT, GURGAON
     </h2>
     <div class="row">
      <div class="col-md-3">
       <p>
        1808/2, 2nd FLOOR OLD DLF NEAR HONDA SHOWROOM, SECTOR-14, Gurgaon (Haryana)
       </p>
       <hr/>
       <h5>
        PHONE:
       </h5>
       <p>
        0124 - 4219095 - 96 - 97 - 98
       </p>
       <hr/>
       <h5>
        MOBILE OR WHATSAPP:
       </h5>
       <p>
        +91-09873477222/333
       </p>
       <hr/>
       <h5>
        EMAIL:
       </h5>
       <p>
        gurgaon@ducatindia.com
       </p>
      </div>
      <!-- End Of Col Md 3 -->
      <div class="col-md-9">
       <iframe frameborder="0" height="350" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d28058.660419349962!2d77.043283!3d28.46953!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8a240a583b4f015c!2sDUCAT!5e0!3m2!1sen!2sin!4v1420181415929" style="border:0" width="100%">
       </iframe>
      </div>
      <!-- End Of Col Md 9 -->
     </div>
     <!-- End Of Row -->
     <h2>
      DUCAT, GREATER NOIDA
     </h2>
     <div class="row">
      <div class="col-md-3">
       <p>
        F-205 Neelkanth Plaza Alpha 1 Commercial Belt Opposite to Alpha Metro Station Greater Noida (U.P.)
       </p>
       <hr/>
       <h5>
        PHONE:
       </h5>
       <p>
        0120-4345190-97
       </p>
       <hr/>
       <h5>
        MOBILE:
       </h5>
       <p>
        +91 9899909738, 9711591451
       </p>
       <hr/>
       <h5>
        EMAIL:
       </h5>
       <p>
        g.noida@ducatindia.com
       </p>
      </div>
      <!-- End Of Col Md 3 -->
      <div class="col-md-9">
       <iframe frameborder="0" height="350" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3507.667431782078!2d77.523707!3d28.45944!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cea7e6d100001%3A0x3538c9120d3cd7ff!2sDucat+Greater+Noida!5e0!3m2!1sen!2sin!4v1420181648847" style="border:0" width="100%">
       </iframe>
      </div>
      <!-- End Of Col Md 9 -->
     </div>
     <!-- End Of Row -->
    </div>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>