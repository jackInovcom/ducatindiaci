<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     ANDROID 6 WEEKS
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     ANDROID 6 WEEKS
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      ANDROID SUMMER TRAINING
     </h4>
     <h5>
      <b>
       Learn Android 6 weeks summer Training
      </b>
     </h5>
     <p>
      Ducat, the most renowned name in IT training,development and consultancy is once again leading the torch in the darkness to take you to the future of technology.In this regard one of the most coveted courses available with us is 6-weeks summer training in Android. Android is one of the stepping stone for learning technologies with their intricate upgraded versions of all times.At Ducat, students get to learn about how to use various technologies in Android simplify development of large-scale web projects. Android 6 weeks summer training classes involves thorough learning of all aspects of the technology in the present scenario as well as lays emphasis on future practical applicability.Thus our courses are an interface for today's academicians with tomorrows IT executives.The training program will let you work on live projects under the supervision of highly qualified faculty.They work with you at every step, to let you reach to your goal successfully.Our training program is in coherence with college curriculum thus it lets students pursue their industrial training from Ducat itself. This way they also get industrial training certificate with appropriate experience from us as asked by the college.Not just we educate you but we make our best efforts to empower you with jobs in your hands.So, our consultancy wing on regular basis ask companies to procure students for placements. we have made our presence felt in centers of Noida, Ghaziabad, Faridabad, Gurgaon,Greater Noida, Jaipur, Gwalior.In the future to come we hope that most of you, grasp the stars that you seek for.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction to Android
      </h2>
      <ul>
       <li>
        Brief history of Android
       </li>
       <li>
        What is Android?
       </li>
       <li>
        Why is Android important?
       </li>
       <li>
        What is OHA?
       </li>
       <li>
        Advantages of Android
       </li>
       <li>
        Android features
       </li>
       <li>
        Android Market
       </li>
      </ul>
      <h2>
       Installation and Configuration of Android
      </h2>
      <ul>
       <li>
        Details about the software requirement
       </li>
       <li>
        Download and installation process of
       </li>
       <li>
        Android Studio
       </li>
       <li>
        Creation of AVD
       </li>
       <li>
        Details of AVD
       </li>
       <li>
        Platforms
       </li>
       <li>
        Tools
       </li>
       <li>
        Version
       </li>
      </ul>
      <h2>
       Getting Started
      </h2>
      <ul>
       <li>
        How to Select Android Version
       </li>
       <li>
        Step To Create Android Project
       </li>
       <li>
        Running Your Application
       </li>
       <li>
        Create Switching App
       </li>
       <li>
        Run Application in your Android Mobile
       </li>
       <li>
        Create a List by the use of Listview
       </li>
      </ul>
      <h2>
       Introduction To Application Component
      </h2>
      <ul>
       <li>
        What is Activity
       </li>
       <li>
        Activity Life Cycle
       </li>
       <li>
        The ManifestFile.xml
       </li>
       <li>
        Layout Resources
       </li>
       <li>
        Project File and Folder
       </li>
      </ul>
      <h2>
       Listeners
      </h2>
      <ul>
       <li>
        Introduction of Listeners
       </li>
       <li>
        Working with Their Abstract methods
       </li>
       <li>
        OnClickListener
       </li>
       <li>
        OnLongClickListener
       </li>
       <li>
        OnCheckChangedListener
       </li>
       <li>
        OnItemClickListener
       </li>
       <li>
        OnItemSelectedListener
       </li>
       <li>
        OnKeyFocus
       </li>
       <li>
        OnInitListener
       </li>
      </ul>
      <h2>
       Android Components
      </h2>
      <ul>
       <li>
        Button
       </li>
       <li>
        Toggle Button
       </li>
       <li>
        Checkbox
       </li>
       <li>
        Spinner
       </li>
       <li>
        Progress Bar
       </li>
       <li>
        Radio Group
       </li>
       <li>
        Rating Bar
       </li>
       <li>
        Switch
       </li>
      </ul>
      <h2>
       Layouts
      </h2>
      <ul>
       <li>
        Linear Layout
       </li>
       <li>
        Relative Layout
       </li>
       <li>
        Frame Layout
       </li>
       <li>
        Constraint Layout
       </li>
      </ul>
      <h2>
       Composite
      </h2>
      <ul>
       <li>
        List View
       </li>
       <li>
        Grid View
       </li>
       <li>
        Scroll View
       </li>
       <li>
        Horizontal Scroll View
       </li>
       <li>
        Sliding Drawer
       </li>
       <li>
        Web View
       </li>
       <li>
        Recycler View
       </li>
       <li>
        Card View
       </li>
      </ul>
      <h2>
       Image And Media
      </h2>
      <ul>
       <li>
        Image View
       </li>
       <li>
        Image Button
       </li>
       <li>
        Gallery
       </li>
       <li>
        Media Controller
       </li>
       <li>
        Video View
       </li>
       <li>
        How to Play Audio
       </li>
       <li>
        How to Play Video
       </li>
       <li>
        Audio Recording
       </li>
       <li>
        Video Recording
       </li>
       <li>
        How to Click Picture Using Camera
       </li>
       <li>
        How to Set Wallpaper
       </li>
       <li>
        Time Picker And Date Picker
       </li>
      </ul>
      <h2>
       Android Menu
      </h2>
      <ul>
       <li>
        How To Create Menu
       </li>
       <li>
        Option Menu
       </li>
       <li>
        Context Menu
       </li>
       <li>
        Popup Menu
       </li>
      </ul>
      <h2>
       Creating Dialogs
      </h2>
      <ul>
       <li>
        Introduction to Dialogs
       </li>
       <li>
        Showing and Dismissing of dialog Boxes.
       </li>
       <li>
        Alert Dialog
       </li>
       <li>
        Progress Dialog
       </li>
       <li>
        Threading and Handler
       </li>
      </ul>
      <h2>
       Intent, BroadcastReceiver, adapter
      </h2>
      <ul>
       <li>
        Different Type of Intent?
       </li>
       <li>
        What is intent-Filter
       </li>
       <li>
        What is Pending Intent
       </li>
       <li>
        What is a sticky Intent
       </li>
       <li>
        Using Intent to take picture
       </li>
      </ul>
      <h2>
       Working with Background
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Creating Controlling Services
       </li>
       <li>
        Unbinded and Binded Services
       </li>
       <li>
        Explicit and Implicit Services
       </li>
       <li>
        TextToSpeech Service
       </li>
       <li>
        SpeechToText Service
       </li>
       <li>
        Registering a Service in Manifest
       </li>
       <li>
        Starting Controlling And interacting with a service
       </li>
      </ul>
      <h2>
       Fragment
      </h2>
      <ul>
       <li>
        Multipane Fragment
       </li>
       <li>
        Fragment Life Cycle
       </li>
       <li>
        Addition Fragment
       </li>
      </ul>
      <h2>
       Maps ,Geo Coding and location based service
      </h2>
      <ul>
       <li>
        Using Location Based Service
       </li>
       <li>
        Working with Location Manager
       </li>
       <li>
        Working With Location Class
       </li>
       <li>
        SharedPrefrences
       </li>
      </ul>
      <h2>
       Data Storage
      </h2>
      <ul>
       <li>
        Editor
       </li>
       <li>
        Modes In SharedPrefrences
       </li>
       <li>
        ContentProvider
       </li>
       <li>
        External Storage
       </li>
       <li>
        Internal Storage
       </li>
       <li>
        Database Structure
       </li>
       <li>
        SqliteDatabase
       </li>
       <li>
        SqliteOpenHelper
       </li>
       <li>
        CRUD operation
       </li>
       <li>
        Handling Database
       </li>
      </ul>
      <h2>
       Animation
      </h2>
      <ul>
       <li>
        Rolling your own Widgets
       </li>
       <li>
        Drawables
       </li>
       <li>
        BitMaps
       </li>
       <li>
        Paints
       </li>
      </ul>
      <h2>
       Bluetooth And Wi-Fi
      </h2>
      <ul>
       <li>
        Introduction of Bluetooth
       </li>
       <li>
        How to make Device On, Off and Discoverable.
       </li>
       <li>
        Introduction of Wi-Fi
       </li>
       <li>
        How To Vibrate Your Device
       </li>
      </ul>
      <h2>
       Introducation to SMS
      </h2>
      <ul>
       <li>
        Using SMS in your Application
       </li>
       <li>
        Using Intent and the native client
       </li>
       <li>
        How to send Message to other Mobile
       </li>
      </ul>
      <h2>
       Android Telephony
      </h2>
      <ul>
       <li>
        Launching the dialer to initiate phone
       </li>
       <li>
        Accessing phone and network properties and status
       </li>
       <li>
        Reading Phone Device Details
       </li>
       <li>
        Reading Network Details
       </li>
      </ul>
      <h2>
       Sensor Device
      </h2>
      <ul>
       <li>
        Using Sensor and Sensor Manager
       </li>
       <li>
        Supporting Android Sensor
       </li>
       <li>
        Finding Sensor
       </li>
       <li>
        Using Sensor
       </li>
       <li>
        Introducing accelerometer
       </li>
       <li>
        Detecting accelerometer changes
       </li>
      </ul>
      <h2>
       WebServices
      </h2>
      <ul>
       <li>
        What is Web Services
       </li>
       <li>
        Web Service Architecture
       </li>
       <li>
        Rest and Soap
       </li>
       <li>
        Parsing Techniques
       </li>
       <li>
        CRUD operation over Server
       </li>
      </ul>
      <h2>
       Firebase
      </h2>
      <ul>
       <li>
        Realtime database
       </li>
       <li>
        Cloud Storage
       </li>
       <li>
        Authentication
       </li>
      </ul>
      <h2>
       PROJECT with Ducat
      </h2>
      <ul>
       <li>
        After the completion, apps you see in the play
       </li>
       <li>
        store now can be build by your own
       </li>
       <li>
        Some Of the App example that can be made after this course -
       </li>
       <li>
        Chatting Apps
       </li>
       <li>
        Dictionary Apps
       </li>
       <li>
        Employee Relationship Management Apps
       </li>
       <li>
        Reminder Apps
       </li>
       <li>
        Company Management Apps
       </li>
       <li>
        College Management Apps
       </li>
       <li>
        Student Management Apps
       </li>
       <li>
        Sensor Gaming Apps and Many More
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="android-summer-training.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="51 + 83 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="134">
       <input name="url" type="hidden" value="/android-summer-training/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>