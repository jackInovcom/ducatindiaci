<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     PL/SQL Training
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     PL/SQL
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      PL/SQL Training
     </h4>
     <p>
      SQL with PLSQL abbreviated as Procedural Training/Structured Source Language that is associated with Oracle Corporation's procedural extension for SQL and also the information. PL/SQL Training includes procedural language components like loops and conditions that permit declaration of constants and variables, procedures and functions and might handle runtime errors.
     </p>
     &lt;
     <div class="contentAcc">
      <h2>
       Introduction to SQL
      </h2>
      <h2>
       Introduction to Oracle Database
      </h2>
      <ul>
       <li>
        List the features of Oracle Database 12c
       </li>
       <li>
        Discuss the basic design, theoretical, and physical aspects of a relational database
       </li>
       <li>
        Categorize the different types of SQL statements
       </li>
       <li>
        Describe the data set used by the course
       </li>
       <li>
        Log on to the database using SQL Developer environment
       </li>
       <li>
        Save queries to files and use script files in SQL Developer
       </li>
      </ul>
      <h2>
       Retrieve Data using the SQL SELECT Statement
      </h2>
      <ul>
       <li>
        List the capabilities of SQL SELECT statements
       </li>
       <li>
        Generate a report of data from the output of a basic SELECT statement
       </li>
       <li>
        Select All Columns
       </li>
       <li>
        Select Specific Columns
       </li>
       <li>
        Use Column Heading Defaults
       </li>
       <li>
        Use Arithmetic Operators
       </li>
       <li>
        Understand Operator Precedence
       </li>
       <li>
        Learn the DESCRIBE command to display the table structure
       </li>
      </ul>
      <h2>
       Learn to Restrict and Sort Data
      </h2>
      <ul>
       <li>
        Write queries that contain a WHERE clause to limit the output retrieved
       </li>
       <li>
        List the comparison operators and logical operators that are used in a WHERE clause
       </li>
       <li>
        Describe the rules of precedence for comparison and logical operators
       </li>
       <li>
        Use character string literals in the WHERE clause
       </li>
       <li>
        Write queries that contain an ORDER BY clause to sort the output of a SELECT statement
       </li>
       <li>
        Sort output in descending and ascending order
       </li>
      </ul>
      <h2>
       Usage of Single-Row Functions to Customize Output
      </h2>
      <ul>
       <li>
        Describe the differences between single row and multiple row functions
       </li>
       <li>
        Manipulate strings with character function in the SELECT and WHERE clauses
       </li>
       <li>
        Manipulate numbers with the ROUND, TRUNC, and MOD functions
       </li>
       <li>
        Perform arithmetic with date data
       </li>
       <li>
        Manipulate dates with the DATE functions
       </li>
      </ul>
      <h2>
       Invoke Conversion Functions and Conditional Expressions
      </h2>
      <ul>
       <li>
        Describe implicit and explicit data type conversion
       </li>
       <li>
        Use the TO_CHAR, TO_NUMBER, and TO_DATE conversion functions
       </li>
       <li>
        Nest multiple functions
       </li>
       <li>
        Apply the NVL, NULLIF, and COALESCE functions to data
       </li>
       <li>
        Use conditional IF THEN ELSE logic in a SELECT statement
       </li>
      </ul>
      <h2>
       Aggregate Data Using the Group Functions
      </h2>
      <ul>
       <li>
        Use the aggregation functions to produce meaningful reports
       </li>
       <li>
        Divide the retrieved data in groups by using the GROUP BY clause
       </li>
       <li>
        Exclude groups of data by using the HAVING clause
       </li>
      </ul>
      <h2>
       Display Data From Multiple Tables Using Joins
      </h2>
      <ul>
       <li>
        Write SELECT statements to access data from more than one table
       </li>
       <li>
        View data that generally does not meet a join condition by using outer joins
       </li>
       <li>
        Join a table to itself by using a self-join
       </li>
      </ul>
      <h2>
       Use Sub-queries to Solve Queries
      </h2>
      <ul>
       <li>
        Describe the types of problem that sub-queries can solve
       </li>
       <li>
        Define sub-queries
       </li>
       <li>
        List the types of sub-queries
       </li>
       <li>
        Write single-row and multiple-row sub-queries
       </li>
      </ul>
      <h2>
       The SET Operators
      </h2>
      <ul>
       <li>
        Describe the SET operators
       </li>
       <li>
        Use a SET operator to combine multiple queries into a single query
       </li>
       <li>
        Control the order of rows returned
       </li>
      </ul>
      <h2>
       Data Manipulation Statements
      </h2>
      <ul>
       <li>
        Describe each DML statement
       </li>
       <li>
        Insert rows into a table
       </li>
       <li>
        Change rows in a table by the UPDATE statement
       </li>
       <li>
        Delete rows from a table with the DELETE statement
       </li>
       <li>
        Save and discard changes with the COMMIT and ROLLBACK statements
       </li>
       <li>
        Explain read consistency
       </li>
      </ul>
      <h2>
       Use of DDL Statements to Create and Manage Tables
      </h2>
      <ul>
       <li>
        Categorize the main database objects
       </li>
       <li>
        Review the table structure
       </li>
       <li>
        List the data types available for columns
       </li>
       <li>
        Create a simple table
       </li>
       <li>
        Decipher how constraints can be created at table creation
       </li>
       <li>
        Describe how schema objects work
       </li>
      </ul>
      <h2>
       Other Schema Objects
      </h2>
      <ul>
       <li>
        Create a simple and complex view
       </li>
       <li>
        Retrieve data from views
       </li>
       <li>
        Create, maintain, and use sequences
       </li>
       <li>
        Create and maintain indexes
       </li>
       <li>
        Create private and public synonyms
       </li>
      </ul>
      <h2>
       Control User Access
      </h2>
      <ul>
       <li>
        Differentiate system privileges from object privileges
       </li>
       <li>
        Create Users
       </li>
       <li>
        Grant System Privileges
       </li>
       <li>
        Create and Grant Privileges to a Role
       </li>
       <li>
        Change Your Password
       </li>
       <li>
        Grant Object Privileges
       </li>
       <li>
        How to pass on privileges?
       </li>
       <li>
        Revoke Object Privileges
       </li>
      </ul>
      <h2>
       Management of Schema Objects
      </h2>
      <ul>
       <li>
        Add, Modify, and Drop a Column
       </li>
       <li>
        Add, Drop, and Defer a Constraint
       </li>
       <li>
        How to enable and Disable a Constraint?
       </li>
       <li>
        Create and Remove Indexes
       </li>
       <li>
        Create a Function-Based Index
       </li>
       <li>
        Perform Flashback Operations
       </li>
       <li>
        Create an External Table by Using ORACLE_LOADER and by Using ORACLE_DATAPUMP
       </li>
       <li>
        Query External Tables
       </li>
      </ul>
      <h2>
       Manage Objects with Data Dictionary Views
      </h2>
      <ul>
       <li>
        Explain the data dictionary
       </li>
       <li>
        Use the Dictionary Views
       </li>
       <li>
        USER_OBJECTS and ALL_OBJECTS Views
       </li>
       <li>
        Table and Column Information
       </li>
       <li>
        Query the dictionary views for constraint information
       </li>
       <li>
        Query the dictionary views for view, sequence, index and synonym information
       </li>
       <li>
        Add a comment to a table
       </li>
       <li>
        Query the dictionary views for comment information
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>