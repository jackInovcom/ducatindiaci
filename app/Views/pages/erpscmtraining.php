<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     ERP SCM TRAINING
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     ERP SCM
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      When it comes to Supply Chain Management (SCM), businesses need to interact with numerous suppliers and partners in order to obtain the raw materials and resources needed to bring finished goods to market. ERP plays a vital role in combating inefficiency; reducing waste and ensuring that workers are better able direct their efforts. The integration of both systems may pose some unique challenges. It is in your company�s best interest to ensure that you and your staff fully understand the role of ERP within the SCM process.
     </p>
     <div class="contentAcc">
      <h2>
       ERP Overview
      </h2>
      <ul>
       <li>
        Theoretical Overview ERP.
       </li>
       <li>
        Overview of Different ERP Tools of Industries.
       </li>
       <li>
        Future of ERP in current Market.
       </li>
       <li>
        Current Leading ERP in Industries.
       </li>
      </ul>
      <h2>
       Enterprise Structure
      </h2>
      <ul>
       <li>
        Fundamental of ES.
       </li>
       <li>
        Building blocks of ES.
       </li>
       <li>
        ES structure creation in SAP.
       </li>
       <li>
        ES Structure for MM Module.
       </li>
      </ul>
      <h2>
       Master Data Management
      </h2>
      <ul>
       <li>
        Purchase Group
       </li>
       <li>
        Material Group
       </li>
       <li>
        Material master
       </li>
       <li>
        Vendor Master
       </li>
      </ul>
      <h2>
       Procurement to Pay Cycle
      </h2>
      <ul>
       <li>
        Purchase Info record
       </li>
       <li>
        Source list
       </li>
       <li>
        Vendor Development and Evaluation
       </li>
       <li>
        RFQ
       </li>
       <li>
        Price Maintenance
       </li>
       <li>
        Price comparison
       </li>
       <li>
        Outline Agreement
       </li>
       <li>
        Contract Management
       </li>
       <li>
        Value Contract
       </li>
       <li>
        Quantity Contract
       </li>
       <li>
        Quota Arrangement
       </li>
       <li>
        Purchase Requisition
       </li>
       <li>
        PO Creation
       </li>
      </ul>
      <h2>
       Pricing Procedure
      </h2>
      <ul>
       <li>
        Defining condition Type
       </li>
       <li>
        Defining Schema Group
       </li>
       <li>
        Defining Calculation Schema
       </li>
       <li>
        Defining Schema determination process
       </li>
      </ul>
      <h2>
       MIGO/Goods Receipt
      </h2>
      <ul>
       <li>
        Configuration of MIGO w.r.t material document
       </li>
       <li>
        Integration of MM &amp; FI Module for document posting
       </li>
       <li>
        GL Account creation
       </li>
       <li>
        Automatic posting setting configuration
       </li>
       <li>
        Posting period closing and opening
       </li>
       <li>
        Goods Receipt against material document
       </li>
       <li>
        Goods Receipt against STO
       </li>
      </ul>
      <h2>
       Material requirement planning for CBP
      </h2>
      <ul>
       <li>
        MRP Overview
       </li>
       <li>
        CBP MRP
       </li>
       <li>
        Defining Prerequisites of MRP
       </li>
       <li>
        MRP Run
       </li>
       <li>
        Verification of MRP proposals.
       </li>
      </ul>
      <h2>
       Goods Movement process.
      </h2>
      <ul>
       <li>
        Transfer Posting for Plant to plant
       </li>
       <li>
        Transfer Posting Storage location to Storage location
       </li>
       <li>
        STO Posting
       </li>
      </ul>
      <h2>
       Physical Inventory Management
      </h2>
      <ul>
       <li>
        Physical Inventory Document.
       </li>
       <li>
        Count maintenance
       </li>
       <li>
        Post difference
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>