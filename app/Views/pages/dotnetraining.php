<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     .NET SIX MONTHS
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     .NET SIX MONTHS
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h2>
      MICROSOFT .NET 4.6 2015 TRAINING
     </h2>
     <h4>
      .Net training institute in Noida, Ghaziabad, Gurgaon, Gr. Noida, Faridabad
     </h4>
     <p>
      .Net Framework known as �dot net� is a software framework, created by Microsoft that can work with many programming languages like C#, VB.NET, F#, and C++. Because of its language interoperability, it can be used to develop various applications ranging from web to mobile to Windows-based applications. A software programmer skilled in .Net has a wide range of opportunities. The faculty at Ducat
      <strong>
       .Net training institute in Noida, Ghaziabad, Gurgaon, Gr. Noida, Faridabad
      </strong>
      believes in preparing each student as a fullstack .Net developer.
     </p>
     <h2>
      Why should you learn .Net?
     </h2>
     <p>
      .Net is a free, cross-platform,open source developer platform used for building web apps, mobile apps,and gaming apps. A trained .Net programmer can write .NET apps in C#, F#, or Visual Basic.A .Net developer uses multiple languages, editors and libraries to create solutions for web, mobile, desktop, gaming and Internet of Things, which is the future of programming. Our expert trainers at Ducat make it the best
      <strong>
       .Net training institute in Noida, Ghaziabad, Gurgaon, Gr. Noida, Faridabad.
      </strong>
     </p>
     <h2>
      Jobs for .Net Programmers
     </h2>
     <p>
      .Net is a software framework that has been created keeping the future in mind. .Net Core, a set of tools containing the runtime, library and compiler components has enabled the programmers to create apps that run on Windows, Mac OS X as well as Linux. Listed here are few of the possibilities for a .Net programmer:
Web Applications Developer,
Android App Developer,
UI Designer,
IOS Developer,
Web Developer,
Technology Architect,
C#, VB or Python Developer
     </p>
     <h2>
      Training and Placement
     </h2>
     <p>
      Ducat has handpicked trainers from the industry who conduct training for software programmers in corporate offices. Our trainers have worked with companies like SDG, XANSA, HCL, WIPRO and the leading MNCs. These well-equipped trainers will mentor you from the beginning in becoming a well-rounded .Net programmer. We have the most comprehensive syllabus that is updated as per the industry requirement and development.
     </p>
     <p>
      We provide our students with live projects to gain hands-on experience and test their learning. The students at Ducat have access to state of the art labs. We constantly upgrade our course content in consultation with our Technical Panel who are leading industry experts from different domains; working at companies like IBM, FIDELITY, STERIA, HCL, WIPRO, and BIRLASOFT. Our constant endeavor to facilitate the best learning environment to our students has made Ducat a leading
      <strong>
       .Net training institute in Noida, Ghaziabad, Gurgaon, Gr. Noida, Faridabad.
      </strong>
     </p>
     <p>
      We prepare you for interviews and provide 100% assistance in job placement with leading tech MNC.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction of Dot Net Framework
      </h2>
      <ul>
       <li>
        What is Dot Net?
       </li>
       <li>
        Why Dot Net?
       </li>
       <li>
        Advantages of Dot Net
       </li>
       <li>
        Component of Dot Net
       </li>
       <li>
        What is Framework?
       </li>
       <li>
        Versions of Framework
       </li>
       <li>
        CLR architecture
       </li>
       <li>
        CTS architecture
       </li>
       <li>
        CLS architecture
       </li>
       <li>
        DLR architectur
       </li>
       <li>
        What is JIT?
       </li>
       <li>
        Types of JIT?
       </li>
       <ul>
        <li>
         Pre-JIT COMPILER
        </li>
        <li>
         Econo-JIT COMPILER
        </li>
        <li>
         Normal-JIT COMPILER
        </li>
       </ul>
      </ul>
      <h2>
       Getting Started with Visual Studio
      </h2>
      <ul>
       <li>
        Understanding Project and Solution Explorer
       </li>
       <li>
        Creating a Project and Solution
       </li>
       <li>
        Understanding Solution Explorer
       </li>
       <li>
        Comments in C#
       </li>
       <li>
        Debug Project
       </li>
       <li>
        Understanding Execution Process
       </li>
      </ul>
      <h2>
       Assemblies in .Net
      </h2>
      <ul>
       <li>
        What is Assembly?
       </li>
       <li>
        Types of Assembly?
       </li>
       <li>
        How to make Private Assembly
       </li>
       <li>
        How to make Shared Assembly
       </li>
       <li>
        What is DLL HALL Problem?
       </li>
       <li>
        What is SN Key?
       </li>
       <li>
        What is GAC?
       </li>
       <li>
        How to Deploy Assembly in Cache
       </li>
       <li>
        What is MSIL?
       </li>
       <li>
        What is DLL &amp; EXE?
       </li>
       <li>
        How make DLL&amp; EXE.
       </li>
       <li>
        Use of Ilasm.exe (IL Assembler) &amp; Use of Ildasm.exe (IL Disassembler).
       </li>
      </ul>
      <h2>
       Object Oriented Programming System (OOPS)
      </h2>
      <ul>
       <li>
        What is OOPS?
       </li>
       <li>
        What is Class &amp; Object?
       </li>
       <li>
        Difference b/w Class, struct, interface and enum
       </li>
       <li>
        Types of Variable
       </li>
       <li>
        Static Variable
       </li>
       <li>
        Const Variable
       </li>
       <li>
        Read Only Variable
       </li>
       <li>
        Instance &amp; Static Members
       </li>
       <li>
        Inheritance &amp; Types of Inheritance
       </li>
       <li>
        What is Interface?
       </li>
       <li>
        Encapsulation
       </li>
       <li>
        Abstraction
       </li>
       <li>
        Polymorphism
       </li>
       <li>
        Types of Polymorphism
       </li>
       <ul>
        <li>
         Static Polymorphism
        </li>
        <li>
         Dynamic Polymorphism
        </li>
        <li>
         Concept of Method Overloading
        </li>
        <li>
         Concept of Method Overriding
        </li>
        <li>
         Concept of Early Binding
        </li>
        <li>
         Concept of Late Binding
        </li>
        <li>
         Concept of Up Casting
        </li>
       </ul>
       <li>
        Operator Overloading?
       </li>
       <li>
        What is Constructor?
       </li>
       <li>
        Types of Constructor
       </li>
       <ul>
        <li>
         Default Constructor
        </li>
        <li>
         Parameterized Constructor
        </li>
        <li>
         Copy Constructor
        </li>
        <li>
         Private Constructor
        </li>
        <li>
         Static Constructor
        </li>
        <li>
         Concept of This Keyword in Constructor
        </li>
       </ul>
       <li>
        Concept of Base Keyword in Constructor
       </li>
       <ul>
        <li>
         Concept of Base Keyword in Constructor
        </li>
        <li>
         Abstract Class &amp; Abstract Method
        </li>
        <li>
         Static Class &amp; Static Method
        </li>
        <li>
         Sealed Class &amp; Sealed Method
        </li>
        <li>
         Partial Class &amp; Partial Method
        </li>
       </ul>
       <li>
        Different types of Parameter
       </li>
       <ul>
        <li>
         Input Type
        </li>
        <li>
         Output Type
        </li>
        <li>
         Ref Type
        </li>
        <li>
         Params Type
        </li>
        <li>
         Dynamic Type
        </li>
       </ul>
      </ul>
      <h2>
       C# LANGUAGE FUNDAMENTALS
      </h2>
      <h2>
       Introductions of C#
      </h2>
      <ul>
       <li>
        What is C#?
       </li>
       <li>
        Namespaces in C#?
       </li>
       <li>
        Create namespace in C#
       </li>
       <li>
        Data types in C#
       </li>
       <li>
        Value type
       </li>
       <li>
        Reference type
       </li>
       <li>
        Null able type
       </li>
       <li>
        Heap
       </li>
       <li>
        Stack
       </li>
       <li>
        Looping in C#
       </li>
       <ul>
        <li>
         For,While,Dowhile
        </li>
        <li>
         Foreach
        </li>
       </ul>
       <li>
        Conditional statement
       </li>
       <ul>
        <li>
         Switch case
        </li>
        <li>
         If, if-else, Nested if
        </li>
        <li>
         GoTo
        </li>
        <li>
         Break
        </li>
        <li>
         Continue
        </li>
       </ul>
       <li>
        Convertion Types
       </li>
       <ul>
        <li>
         Implicit {Boxing}
        </li>
        <li>
         Explicit {UnBoxing}
        </li>
       </ul>
      </ul>
      <h2>
       Array
      </h2>
      <ul>
       <li>
        What is Array?
       </li>
       <li>
        Types of Array
       </li>
       <ul>
        <li>
         Simple Array
        </li>
        <li>
         Multi Array
        </li>
        <li>
         Jagged Array
        </li>
       </ul>
      </ul>
      <h2>
       File Handling
      </h2>
      <ul>
       <li>
        Introductions of File Handing
       </li>
       <li>
        Working with Files and Directories
       </li>
       <li>
        Working with Directories
       </li>
       <li>
        Working with Files Using the File and FileInfo Classes
       </li>
       <li>
        Working with Directories Using the Directories and Directories Info Classes
       </li>
      </ul>
      <h2>
       Exception Handling
      </h2>
      <ul>
       <li>
        Introductions of Exception Handling
       </li>
       <li>
        Role of Exception Handling
       </li>
       <li>
        What is Error, bug and exceptions?
       </li>
       <li>
        Try, catch &amp; Finally block
       </li>
       <li>
        System Level &amp; Application Level Exception
       </li>
       <li>
        Handel Multiple Exception
       </li>
       <li>
        Throw Exception
       </li>
      </ul>
      <h2>
       Software Engineering Models and Concept (Project Based)
      </h2>
      <ul>
       <li>
        What is Software Development Life Cycle?
       </li>
       <li>
        Phases of a SDLC
       </li>
       <li>
        Project initiation and planning
       </li>
       <li>
        Feasibility study
       </li>
       <li>
        System design
       </li>
       <li>
        Coding
       </li>
       <li>
        Testing
       </li>
       <li>
        Implementation
       </li>
       <li>
        Maintenance
       </li>
      </ul>
      <h2>
       Collections
      </h2>
      <ul>
       <li>
        Introductions of Collections
       </li>
       <li>
        Types of collections
       </li>
       <li>
        Generic
       </li>
       <ul>
        <li>
         List
        </li>
        <li>
         Dictionary
        </li>
        <li>
         Stack
        </li>
        <li>
         Queue
        </li>
       </ul>
       <li>
        Non Generic
       </li>
       <ul>
        <li>
         ArrayList
        </li>
        <li>
         HashTable
        </li>
        <li>
         Stack
        </li>
        <li>
         Queue
        </li>
       </ul>
      </ul>
      <h2>
       Delegate
      </h2>
      <ul>
       <li>
        Introductions of Delegate
       </li>
       <li>
        Types of delegate
       </li>
       <li>
        SingleCast
       </li>
       <li>
        MultiCast
       </li>
       <li>
        Generic delegate
       </li>
      </ul>
      <h2>
       Events
      </h2>
      <ul>
       <li>
        Introductions of Events
       </li>
       <li>
        Understanding Events and Event Handler
       </li>
       <li>
        Types of Event
       </li>
       <li>
        System Define Event
       </li>
       <li>
        User Define Event
       </li>
       <li>
        Event Life Cycle
       </li>
       <li>
        Binding Events
       </li>
       <li>
        Working with Event Handler
       </li>
      </ul>
      <h2>
       Reflection
      </h2>
      <ul>
       <li>
        Introductions of Reflection
       </li>
       <li>
        Reflection Namespace
       </li>
       <li>
        Reflection Classes
       </li>
       <li>
        Access Assembly through Reflection
       </li>
       <li>
        Member Info
       </li>
       <li>
        Method Info
       </li>
       <li>
        Property Info
       </li>
       <li>
        Constructor Info
       </li>
       <li>
        Browsing Members
       </li>
       <li>
        Runtime Information
       </li>
      </ul>
      <h2>
       Threading
      </h2>
      <ul>
       <li>
        Introductions of Threading
       </li>
       <li>
        Threading Namespace
       </li>
       <li>
        Understanding Thread LifeCycle
       </li>
       <li>
        Default Threading
       </li>
       <li>
        Multi Threading
       </li>
       <li>
        Priority in threading Member Info
       </li>
      </ul>
      <h2>
       Introductions of Properties
      </h2>
      <ul>
       <li>
        set
       </li>
       <li>
        get
       </li>
      </ul>
      <h2>
       Indexer
      </h2>
      <ul>
       <li>
        Define Indexer
       </li>
       <li>
        Advantages of Indexer
       </li>
      </ul>
      <h2>
       Business Requirement Specification (Project Based)
      </h2>
      <ul>
       <li>
        Objective of Project
       </li>
       <li>
        DFD
       </li>
       <li>
        UML Diagram
       </li>
       <li>
        Class Diagram
       </li>
       <li>
        ER Diagram
       </li>
      </ul>
      <h2>
       The SQL Server Management
      </h2>
      <ul>
       <li>
        What is SQL?
       </li>
       <li>
        What is Database?
       </li>
       <li>
        Create Statement
       </li>
       <li>
        Create database
       </li>
       <li>
        Create table
       </li>
       <li>
        Use Identity
       </li>
       <li>
        Select Statement
       </li>
       <li>
        Insert statement
       </li>
       <li>
        Update statement
       </li>
       <li>
        Delete statement
       </li>
       <li>
        Drop statement
       </li>
       <li>
        Truncate statement
       </li>
       <li>
        Difference between delete, drop &amp; truncate
       </li>
       <li>
        Alter Query
       </li>
       <li>
        Clauses
       </li>
       <li>
        Group By
       </li>
       <li>
        Order By
       </li>
       <li>
        Basic Queries
       </li>
       <li>
        Stored Procedure
       </li>
       <li>
        Use Input and Output Type parameter in Stored Procedure
       </li>
       <li>
        Function
       </li>
       <li>
        Types of Functions
       </li>
       <li>
        Trigger
       </li>
       <li>
        Cursor
       </li>
       <li>
        Union &amp; Union All
       </li>
       <li>
        Transaction
       </li>
       <li>
        Joins
       </li>
       <li>
        Indexes
       </li>
       <li>
        Views
       </li>
       <li>
        Constraints
       </li>
       <li>
        Tracing sql query with SqlProfiler
       </li>
      </ul>
      <h2>
       System Requirement Database Structure (Project Based)
      </h2>
      <ul>
       <li>
        Database Table Structure
       </li>
       <li>
        Database Design
       </li>
       <li>
        DDL
       </li>
       <li>
        DML
       </li>
       <li>
        Stored Procedure
       </li>
       <li>
        Database Security
       </li>
      </ul>
      <h2>
       ADO .NET 10
      </h2>
      <ul>
       <li>
        What is ADO.NET?
       </li>
       <li>
        Difference between ADO &amp; ADO.NET?
       </li>
       <li>
        Architecture of ADO.NET
       </li>
       <li>
        Disconnected mode Architecture
       </li>
       <ul>
        <li>
         Using SqlDataAdapter Class
        </li>
        <li>
         Using DataSet Class
        </li>
        <li>
         SqlCommandBuilder Class
        </li>
       </ul>
       <li>
        Connected Mode Architecture
       </li>
       <ul>
        <li>
         Using SqlConnection Class
        </li>
        <li>
         Using SqlCommand Class
        </li>
        <li>
         Using SqlDataReader Class
        </li>
        <li>
         Understanding ExecuteNonQuery method
        </li>
        <li>
         Understanding ExecuteScalar method
        </li>
        <li>
         Understanding ExecuteReader method
        </li>
       </ul>
       <li>
        Working with DataTable class
       </li>
       <li>
        Creating PrimaryKey Column on VirtualTable
       </li>
       <li>
        Using Constraints Class
       </li>
       <ul>
        <li>
         ForeignKeyConstraints Class
        </li>
        <li>
         UniqueConstraints Class
        </li>
       </ul>
       <li>
        Store table in datatable
       </li>
       <li>
        Apply Insert,update,delete operations on VirtualTable
       </li>
       <li>
        Show, next,first,last,all by datatable
       </li>
       <li>
        Executing Stored Procedure
       </li>
       <li>
        Executing Function
       </li>
       <li>
        What is Sql Injection
       </li>
       <li>
        Applying SqlInjection
       </li>
      </ul>
      <h2>
       ASP.NET 4.6 Framework
      </h2>
      <ul>
       <li>
        Asp.Net Introduction
       </li>
       <ul>
        <li>
         Evolution of Web Development
        </li>
        <li>
         ASP .NET different versions
        </li>
        <li>
         Understanding protocol and domain
        </li>
        <li>
         Understanding static &amp; dynamic page
        </li>
        <li>
         Server-side programming
        </li>
        <li>
         Client-side programming
        </li>
        <li>
         HTML and HTML Forms
        </li>
        <li>
         Creating a simple html page
        </li>
       </ul>
       <li>
        Developing ASP.NET Applications
       </li>
       <ul>
        <li>
         Creating Websites
        </li>
        <li>
         Creating a New Web Application
        </li>
        <li>
         Page Life Cycle
        </li>
        <li>
         Understanding Events of Page Life Cycle
        </li>
        <li>
         Websites and Web Projects
        </li>
        <li>
         Adding Web Forms
        </li>
        <li>
         Migrating a Website from a Previous Version
        </li>
        <li>
         Difference b/w inline and code behind concept
        </li>
        <li>
         Asp.Net special directory
        </li>
        <li>
         What is Application Directories
        </li>
        <li>
         Different types of Application Directories
        </li>
        <li>
         Understanding application label and page label variable
        </li>
        <li>
         Need of Global class
        </li>
        <li>
         What is Configuration file?
        </li>
        <li>
         Need of Configuration file
        </li>
        <li>
         Difference b/t app.config &amp; web.con file
        </li>
        <li>
         Nested Configuration
        </li>
       </ul>
       <li>
        State Management
       </li>
       <ul>
        <li>
         Need of state management technique
        </li>
        <li>
         Types of state management
        </li>
        <li>
         Understanding Server side and Client side state management
        </li>
        <li>
         ViewState
        </li>
        <li>
         HiddenField
        </li>
        <li>
         Cookies
        </li>
        <li>
         Cache
        </li>
        <li>
         QueryString
        </li>
        <li>
         Session
        </li>
        <li>
         Session Tracking
        </li>
        <li>
         Session Configuration
        </li>
        <li>
         Session mode
        </li>
        <li>
         Cookieless
        </li>
        <li>
         Session Timeout
        </li>
        <li>
         Session Mode
        </li>
        <li>
         Application
        </li>
        <li>
         Transferring Information between Pages
        </li>
        <li>
         Transferring Information between Cross Pages
        </li>
       </ul>
       <li>
        Web Controls in ASP.Net
       </li>
       <ul>
        <li>
         Types of web controls
        </li>
        <li>
         Server Controls
        </li>
        <li>
         Standard Controls
        </li>
        <li>
         Data Controls
        </li>
        <li>
         Validation Controls
        </li>
        <li>
         Navigation Controls
        </li>
        <li>
         Login Controls
        </li>
        <li>
         Ajax Extensions
        </li>
        <li>
         Ajax ToolKit
        </li>
       </ul>
       <li>
        Html Controls
       </li>
       <li>
        User Controls
       </li>
       <li>
        Data Controls
       </li>
       <ul>
        <li>
         DataList
        </li>
        <li>
         DetailsView
        </li>
        <li>
         FormsView
        </li>
        <li>
         GridView
        </li>
        <li>
         ListView
        </li>
        <li>
         Repeater
        </li>
        <li>
         DataGrid
        </li>
        <li>
         EntityDataSource
        </li>
        <li>
         Object DataSource
        </li>
        <li>
         LinqDataSource
        </li>
        <li>
         XmlDataSource
        </li>
       </ul>
       <li>
        Working with Templates of Data Controls
       </li>
       <li>
        Working with events of Data Controls
       </li>
       <li>
        CSS
       </li>
       <ul>
        <li>
         Understanding CSS
        </li>
        <li>
         Types of CSS
        </li>
        <li>
         Inline CSS
        </li>
        <li>
         Internal CSS
        </li>
        <li>
         External CSS
        </li>
        <li>
         Selectors in CSS
        </li>
       </ul>
       <li>
        Themes &amp; Skins
       </li>
       <li>
        Working with Image gallery
       </li>
       <li>
        Working with ImageSlide
       </li>
       <li>
        Master Page
       </li>
       <li>
        Nested Master Page
       </li>
       <li>
        Page Tracing
       </li>
       <li>
        Mail &amp; Messaging
       </li>
       <li>
        Caching Techniques
       </li>
       <ul>
        <li>
         Understanding Caching
        </li>
        <li>
         Types of Caching
        </li>
        <li>
         Output Caching
        </li>
        <li>
         Data Caching
        </li>
       </ul>
       <li>
        Web Security
       </li>
       <ul>
        <li>
         Need of security
        </li>
        <li>
         Types of security
        </li>
        <li>
         Forms Authentication
        </li>
        <li>
         Windows Authentication
        </li>
        <li>
         Confidentiality with SSL
        </li>
        <li>
         Password Format
        </li>
        <li>
         Types of password format
        </li>
        <li>
         MD5
        </li>
        <li>
         SHA1
        </li>
        <li>
         Clear
        </li>
       </ul>
       <li>
        Performance tuning
       </li>
       <li>
        Ajax
       </li>
       <li>
        Understanding Gmail,Facebook and Twitter API's
       </li>
       <li>
        Deploying ASP.NET Applications
       </li>
       <ul>
        <li>
         ASP.NET Applications and the Web Server
        </li>
        <li>
         How Web Servers Work
        </li>
        <li>
         Understanding Virtual Directory
        </li>
        <li>
         Web Application URLs
        </li>
        <li>
         Web Farms
        </li>
       </ul>
       <li>
        Internet Information Services (IIS)
       </li>
       <ul>
        <li>
         Installing IIS
        </li>
        <li>
         Registering the ASP.NET File Mappings
        </li>
        <li>
         Verifying That ASP.NET Is Correctly Installed
        </li>
       </ul>
       <li>
        Managing Websites with IIS Manager
       </li>
       <ul>
        <li>
         Creating a Virtual Directory
        </li>
        <li>
         Configuring a Virtual Directory
        </li>
       </ul>
       <li>
        Deploying a Simple Site
       </li>
       <ul>
        <li>
         Web Applications and Components
        </li>
        <li>
         Other Configuration Steps
        </li>
        <li>
         Code Compilation
        </li>
        <li>
         Verifying That ASP.NET Is Correctly Installed
        </li>
       </ul>
       <li>
        Deploying a Visual Studio IDE
       </li>
       <ul>
        <li>
         Creating a Virtual Directory for a New Project
        </li>
        <li>
         Copying a Website
        </li>
        <li>
         Publishing a Website
        </li>
        <li>
         Verifying That ASP.NET Is Correctly Installed
        </li>
       </ul>
      </ul>
      <h2>
       Technical Design &amp; Development (Project Based)
      </h2>
      <ul>
       <li>
        Working with Project
       </li>
       <li>
        Programming Language: C# (Asp.Net)
       </li>
       <li>
        Designing Tools
       </li>
       <li>
        HTML
       </li>
       <li>
        Using CSS
       </li>
       <li>
        Using Ajax
       </li>
       <li>
        Using JavaScript
       </li>
      </ul>
      <h2>
       ADVANCE .NET MVC
      </h2>
      <h2>
       Language Integrated Query (LINQ)
      </h2>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>