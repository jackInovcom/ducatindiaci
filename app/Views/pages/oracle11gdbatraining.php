<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     ORACLE (6 weeks)
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     ORACLE 6 weeks
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      DUCAT is the only training institute giving technical training with a difference. The institute not only provides training but takes proper care that the training is meaningful. DUCAT provides training on Oracle 11g DBA in the most technical way. Oracle 11g DBA is an Oracle revolution which is a boom for database administrators. It deals with self and storage management having clustering features. It releases data administrators from doing low value work and to gives them an opportunity to increase their value. It also helps them to technically more sound in their respective field. DUCAT provides a training which teaches this new technique in a focused manner. The motive of the training is that it teaches you the new technology in a way that you can put it into your work field quickly. You learn directly from the source so the knowledge gained is perfect. DUCAT training staff are the most experienced in this new Oracle technology. They give lessons, shows and explain practical examples, creates work environment where this technology can be used. So, after taking the training one becomes an expert of this technology which is in demand all over in every industry. The training makes one expert in a field which helps to earn a comfortable salary.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction to Oracle Database
      </h2>
      <ul>
       <li>
        List the features of Oracle Database 12c
       </li>
       <li>
        Discuss the basic design of Database
       </li>
       <li>
        Categorize the different types of SQL statements
       </li>
       <li>
        Describe the data set used by the course
       </li>
       <li>
        Log on to the database using SQL Developer environment
       </li>
       <li>
        Save queries to files and use script files in SQL Developer
       </li>
      </ul>
      <h2>
       Retrieve Data using the SQL SELECT Statement
      </h2>
      <ul>
       <li>
        List the capabilities of SQL SELECT statements
       </li>
       <li>
        Select Table Data With All Columns
       </li>
       <li>
        Select Table Data With Specific Columns
       </li>
       <li>
        Use Arithmetic Operators
       </li>
       <li>
        Use Concatenation Operators
       </li>
       <li>
        Learn the DESCRIBE command to display the table structure
       </li>
      </ul>
      <h2>
       Learn to Restrict and Sort Data
      </h2>
      <ul>
       <li>
        Use WHERE clause to limit the output retrieved
       </li>
       <li>
        List the comparison operators and logical operators
       </li>
       <li>
        Describe the rules of precedence for comparison and logical operators
       </li>
       <li>
        Use character string literals in the WHERE clause
       </li>
       <li>
        Write queries that contain an ORDER BY clause to sort Data
       </li>
       <li>
        SQL Row Limiting Clause (Fitch First,Offset and With Ties)
       </li>
      </ul>
      <h2>
       Usage of Single-Row Functions to Customize Output
      </h2>
      <ul>
       <li>
        Differences between single row and multiple row functions
       </li>
       <li>
        Manipulate strings with character function in the SELECT and WHERE clauses
       </li>
       <li>
        Manipulate numbers with the ROUND, TRUNC, and MOD functions
       </li>
       <li>
        Perform arithmetic with date data
       </li>
       <li>
        Manipulate dates with the DATE functions
       </li>
      </ul>
      <h2>
       Invoke Conversion Functions and Conditional Expressions
      </h2>
      <ul>
       <li>
        Describe implicit and explicit data type conversion
       </li>
       <li>
        Use the TO_CHAR, TO_NUMBER, and TO_DATE conversion functions
       </li>
       <li>
        Nest multiple functions
       </li>
       <li>
        Apply the NVL, NULLIF, and COALESCE functions to data
       </li>
       <li>
        Use conditional IF THEN ELSE logic in a SELECT statement
       </li>
      </ul>
      <h2>
       Aggregate Data Using the Group Functions
      </h2>
      <ul>
       <li>
        Use the aggregation functions to produce meaningful reports
       </li>
       <li>
        Divide the retrieved data in groups by using the GROUP BY clause
       </li>
       <li>
        Exclude groups of data by using the HAVING clause
       </li>
      </ul>
      <h2>
       Display Data From Multiple Tables Using Joins
      </h2>
      <ul>
       <li>
        Write SELECT statements to access data from more than one table
       </li>
       <li>
        View data that does not meet join condition by using outer joins
       </li>
       <li>
        Join a table to itself by using a self-join
       </li>
      </ul>
      <h2>
       Use Sub-queries to Solve Queries
      </h2>
      <ul>
       <li>
        Describe the types of problem that sub-queries can solve
       </li>
       <li>
        Define sub-queries
       </li>
       <li>
        List the types of sub-queries
       </li>
       <li>
        Write single-row and multiple-row sub-queries
       </li>
      </ul>
      <h2>
       The SET Operators
      </h2>
      <ul>
       <li>
        Describe the SET operators
       </li>
       <li>
        UNION and UNION ALL to Combine Result of Multiple Queries
       </li>
       <li>
        Use of INTERSECT
       </li>
       <li>
        Use of MINUS
       </li>
       <li>
        Control the order of rows returned
       </li>
      </ul>
      <h2>
       Data Manipulation Statements
      </h2>
      <ul>
       <li>
        Describe each DML statement
       </li>
       <li>
        Insert rows into a table
       </li>
       <li>
        Change rows in a table by the UPDATE statement
       </li>
       <li>
        Delete rows from a table with the DELETE statement
       </li>
       <li>
        Save and discard changes with the COMMIT and ROLLBACK statements
       </li>
       <li>
        Explain read consistency
       </li>
      </ul>
      <h2>
       Use of DDL Statements to Create and Manage Tables
      </h2>
      <ul>
       <li>
        Create a simple table
       </li>
       <li>
        Alter and Truncate Table
       </li>
       <li>
        Drop Table and Concept of Recyclebin
       </li>
       <li>
        Stop Entry of Invalid Data Through CONSTRAINTS
       </li>
       <li>
        Primary Key , Foreign Key
       </li>
       <li>
        Not Null , Unique and Check Constraints
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>