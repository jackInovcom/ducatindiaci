<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Corporate Training
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Corporate Training
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <h3>
     Corporate Training At Ducat
    </h3>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
    <div class="coursesArea">
     <br/>
     <p>
      <img alt="corporate-training" class="img-responsive" src="../images/corporate-training1.jpg"/>
     </p>
     <p>
      DUCAT Corporate provides corporate  education and custom-content training solutions to some of India's largest private, public and Government organizations .We serve clients in Various sectors such as information technology, financial services,pharmaceuticals,tele communications,education,professional services. Ducat corporate is proud of its ability to assess learning  needs and develop and deliver training solutions to support and enhances return on learner's training investments.
     </p>
     <p>
      Ducat envisages strengthening its training base on industry scenario and technology development issues, developing its training capabilities. it further acts as an interface with software companies to promote exchange of innovations, training methodologies, design on training curriculum and monitoring the software industry trends, country wide.
     </p>
     <p>
      Ducat corporate has been offering software training, drawn from experience in research ,training and facilitation in the areas of process improvement, product engineering,quality and knowledge management. Our holistic approach to process improvement training is based on the objective of integrating people, process, and technology, which are critical to the performance of a trainee.
     </p>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/it1.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        Tailor-made Program/Training
       </h3>
       <p class="paraText">
        Ducat offers a full spectrum of vendor authorized technical, business skills, project management and application courses designed to suit every skill level, as well as the ability to consult directly with organizations to tailor made learning plans for any number of employees.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/it2.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        Real Time Trainers
       </h3>
       <p class="paraText">
        The Core Strength of DUCAT are its trainers. Getting trained by these experts gives the complete insight of the market scenario with latest trends followed by various IT giants.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/it3.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        International Certification Training
       </h3>
       <p class="paraText">
        Not Only the course content but the deliveries imparted helps in clearing the Global certification hence giving the candidate an Edge from others.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/it4.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        Updated  Course Content &amp; Courseware
       </h3>
       <p class="paraText">
        The course contents &amp; Course ware are designed under the strict supervision of professionals from the relevant industries, focusing on the needs of the market with complete practical approach.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <form class="searchForm">
     <input placeholder="Search" type="text"/>
    </form>
    <div class="widgetArea">
     <h5>
      CONTACT INFO
     </h5>
     <address>
      <span class="address">
       A - 43 &amp; A - 52 Sector - 16,
       <br/>
       Noida (U.P) (Near McDonalds)
      </span>
      <br>
       <span class="phone">
        <strong>
         Phone:
        </strong>
        0120-4646464, +91- 9871055180
       </span>
       <br/>
       <span class="email">
        <strong>
         E-Mail:
        </strong>
        <a href="mailto:info@ducatindia.com">
         info@ducatindia.com
        </a>
       </span>
       <br/>
       <span class="web">
        <strong>
         Web:
        </strong>
        <a href="http://www.ducatindia.com/">
         http://www.ducatindia.com/
        </a>
       </span>
      </br>
     </address>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section class="text-center" id="offices">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <h5>
     CORPORATE OFFICE NOIDA:
     <span>
      0120 - 4646464
     </span>
    </h5>
    <p>
     GR.NOIDA:
     <span>
      0120-4345190
     </span>
     GHAZIABAD:
     <span>
      0120-4835400
     </span>
     FARIDABAD:
     <span>
      0129-4150605
     </span>
     GURGAON:
     <span>
      0124-4219095
     </span>
     JAIPUR:
     <span>
      0141-2550077
     </span>
    </p>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>