<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Students Details Form
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="#">
     Students Details
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9 text-center">
    <h3>
     Student Placement Form
    </h3>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
    <form action="../logics_database/placement.php" class="onlineRegistration text-left" method="post">
     <div class="row">
      <div class="col-md-6">
       <input autofocus="" id="name" name="fname" pattern="[a-zA-Z ]+" placeholder="Enter First Name" required="" tabindex="1" type="text"/>
       <input id="telephone" name="telephone" pattern="\d{10}" placeholder="Phone number" required="" tabindex="3" type="text"/>
       <input autofocus="" id="branch" name="dsid" pattern="[a-zA-Z0-9_-]{4,12}" placeholder="Ducat Student ID" required="" tabindex="5" type="text"/>
       <input autofocus="" class="txtinput" id="branch" name="tname" placeholder="Trainer Name in Ducat" required="" tabindex="7" type="text"/>
       <select autofocus="" class="txtinput" id="calendar" name="dtcy" required="" tabindex="9">
        <option>
         DUCAT:Training Completed Year
        </option>
        <option value="1999">
         1999
        </option>
        <option value="2000">
         2000
        </option>
        <option value="2001">
         2001
        </option>
        <option value="2002">
         2002
        </option>
        <option value="2003">
         2003
        </option>
        <option value="2004">
         2004
        </option>
        <option value="2005">
         2005
        </option>
        <option value="2006">
         2006
        </option>
        <option value="2007">
         2007
        </option>
        <option value="2008">
         2008
        </option>
        <option value="2009">
         2009
        </option>
        <option value="2010">
         2010
        </option>
        <option value="2011">
         2011
        </option>
        <option value="2012">
         2012
        </option>
        <option value="2013">
         2013
        </option>
        <option value="2014">
         2014
        </option>
        <option value="2015">
         2015
        </option>
        <option value="2016">
         2016
        </option>
       </select>
       <input autofocus="" name="college" placeholder="College Name" tabindex="11" type="text">
        <input autofocus="" name="ten" placeholder=" In 10th %" required="" tabindex="13" type="text"/>
        <input autofocus="" name="gra" placeholder=" In Graduation % " tabindex="15" type="text"/>
        <input autocomplete="off" autofocus="" name="passy" placeholder=" Passout Year" required="" tabindex="17" type="text"/>
       </input>
      </div>
      <!-- End Of Col MD 6 -->
      <div class="col-md-6">
       <input autofocus="" id="name" name="lname" pattern="[a-zA-Z ]+" placeholder="Enter Last Name" required="" tabindex="2" type="text"/>
       <input autocomplete="off" id="email" name="email" placeholder="E-mail" required="" tabindex="4" type="text"/>
       <select autofocus="" name="branch" required="" tabindex="6">
        <option value="Select Branch">
         Select Branch
        </option>
        <option value="Noida">
         Noida
        </option>
        <option value="Faridabad">
         Faridabad
        </option>
        <option value="Ghaziabad">
         Ghaziabad
        </option>
        <option value="Greater Noida">
         Greater Noida
        </option>
        <option value="Gurgaon">
         Gurgaon
        </option>
        <option value="Jaipur">
         Jaipur
        </option>
       </select>
       <select autofocus="" name="course" required="" tabindex="8">
        <option>
         Course Done
        </option>
        <option value="3D Studio Max">
         3D Studio Max
        </option>
        <option value="Adv. Digital System Design">
         Adv. Digital System Design
        </option>
        <option value="Advance QTP">
         Advance QTP
        </option>
        <option value="Adobe Flex-3.0">
         Adobe Flex-3.0
        </option>
        <option value="Android">
         Android
        </option>
        <option value="Autocad">
         Autocad
        </option>
        <option value="BI Cognos 8.4">
         BI Cognos 8.4
        </option>
        <option value="C Language">
         C Language
        </option>
        <option value="CAD Mechanical Six Months">
         CAD Mechanical Six Months
        </option>
        <option value="CAD_Civil_SM">
         CAD_Civil_SM
        </option>
        <option value="CAKE PHP">
         CAKE PHP
        </option>
        <option value="CCNA">
         CCNA
        </option>
        <option value="CCNP">
         CCNP
        </option>
        <option value="Catia">
         Catia
        </option>
        <option value="Cloud Computing six Weeks">
         Cloud Computing six Weeks
        </option>
        <option value="Cloud Computing Full Course">
         Cloud Computing Full Course
        </option>
        <option value="Cmos Based Design">
         Cmos Based Design
        </option>
        <option value="Coreldraw">
         Coreldraw
        </option>
        <option value="C++ Language">
         C++ Language
        </option>
        <option value="Dataware Housing">
         Dataware Housing
        </option>
        <option value="Digital Marketing">
         Digital Marketing
        </option>
        <option value="Flash Action">
         Flash Action
        </option>
        <option value="Flash Basic">
         Flash Basic
        </option>
        <option value="HR GENERALIST">
         HR GENERALIST
        </option>
        <option value="Apache Hadoop">
         Apache Hadoop
        </option>
        <option value="IBM MAINFRAME">
         IBM MAINFRAME
        </option>
        <option value="I-Phone">
         I-Phone
        </option>
        <option value="Illistrator">
         Illistrator
        </option>
        <option value="Indesign">
         Indesign
        </option>
        <option value="JAVA J2EE">
         JAVA J2EE
        </option>
        <option value="JAVA Spring &amp; Hibernate">
         JAVA Spring &amp; Hibernate
        </option>
        <option value="Java six Months">
         Java six Months
        </option>
        <option value="Java6w">
         Java6w
        </option>
        <option value="MCITP">
         MCITP
        </option>
        <option value="MCSE">
         MCSE
        </option>
        <option value="Magento PHP">
         Magento PHP
        </option>
        <option value="Multimedia Animation">
         Multimedia Animation
        </option>
        <option value="Networking Six Months">
         Networking Six Months
        </option>
        <option value="PHP Six Months">
         PHP Six Months
        </option>
        <option value="PHP SIX WEEKS">
         PHP SIX WEEKS
        </option>
        <option value="PHP ++">
         PHP ++
        </option>
        <option value="PLC SCADA 3 months">
         PLC SCADA 3 months
        </option>
        <option value="PLC SCADA FULL COURCE">
         PLC SCADA FULL COURCE
        </option>
        <option value="PLC SCADA six weeks">
         PLC SCADA six weeks
        </option>
        <option value="Primaverproject Planner">
         Primaverproject Planner
        </option>
        <option value="RHCVA">
         RHCVA
        </option>
        <option value="Ravit Architecture">
         Ravit Architecture
        </option>
        <option value="SAS">
         SAS
        </option>
        <option value="SAS BI">
         SAS BI
        </option>
        <option value="SEO">
         SEO
        </option>
        <option value="SE-PD(Personality Dev.)">
         SE-PD(Personality Dev.)
        </option>
        <option value="SOAP-UI Testing">
         SOAP-UI Testing
        </option>
        <option value="SQT Six Months">
         SQT Six Months
        </option>
        <option value="SQT Six weeks">
         SQT Six weeks
        </option>
        <option value="Sharepoint Development">
         Sharepoint Development
        </option>
        <option value="Silverlight 4.0">
         Silverlight 4.0
        </option>
        <option value="Solidworks">
         Solidworks
        </option>
        <option value="TIBCO">
         TIBCO
        </option>
        <option value="UNIX SHELL Scripting">
         UNIX SHELL Scripting
        </option>
        <option value="Unigraphics">
         Unigraphics
        </option>
        <option value="VLSI Design Flow">
         VLSI Design Flow
        </option>
        <option value="Python">
         ZEND PHP
        </option>
        <option value="ZEND PHP">
         Python
        </option>
        <option value=".NET six Months">
         .NET six Months
        </option>
        <option value=".NET six weeks">
         .NET six weeks
        </option>
        <option value="Embedded 3.6 Months">
         Embedded 3.6 Months
        </option>
        <option value="Embedded Six Months">
         Embedded Six Months
        </option>
        <option value="Embedded six Weeks">
         Embedded six Weeks
        </option>
        <option value="ERP Level-2">
         ERP Level-2
        </option>
        <option value="oracle 10G">
         oracle 10G
        </option>
        <option value="oracle 10G DBA">
         oracle 10G DBA
        </option>
        <option value="oracle 10G RAC">
         oracle 10G RAC
        </option>
        <option value="oracle app's XII Financials">
         oracle app's XII Financials
        </option>
        <option value="oracle app's XII MFG">
         oracle app's XII MFG
        </option>
        <option value="oracle app's XII Tecnical">
         oracle app's XII Tecnical
        </option>
        <option value="photoshop">
         photoshop
        </option>
        <option value="Drupal">
         Drupal
        </option>
        <option value="Joomla">
         Joomla
        </option>
        <option value="OS-Commerce">
         OS-Commerce
        </option>
        <option value="Smarty">
         Smarty
        </option>
        <option value="Wordpress">
         Wordpress
        </option>
        <option value="pro-e">
         pro-e
        </option>
        <option value="Redhat Linux 6.0">
         Redhat Linux 6.0
        </option>
        <option value="ROBOTICS COURSE">
         ROBOTICS COURSE
        </option>
        <option value="Software Quality Testing">
         Software Quality Testing
        </option>
       </select>
       <select autofocus="" class="txtinput" id="calendar" name="dtcm" required="" tabindex="10">
        <option>
         DUCAT:Training Completed Month
        </option>
        <option value="JAN">
         JAN
        </option>
        <option value="FEB">
         FEB
        </option>
        <option value="MARCH">
         MARCH
        </option>
        <option value="APRIL">
         APRIL
        </option>
        <option value="MAY">
         MAY
        </option>
        <option value="JUNE">
         JUNE
        </option>
        <option value="JULY">
         JULY
        </option>
        <option value="AUG">
         AUG
        </option>
        <option value="SEP">
         SEP
        </option>
        <option value="OCT">
         OCT
        </option>
        <option value="NOV">
         NOV
        </option>
        <option value="DEC">
         DEC
        </option>
       </select>
       <input autofocus="" id="course" name="qualification" placeholder="Qualification " required="" tabindex="12" type="text"/>
       <input autofocus="" name="twe" placeholder=" In 12th  / (10+2)%" required="" tabindex="14" type="text"/>
       <input autocomplete="off" autofocus="" id="percentage" name="pg" placeholder=" In Post Graduation " tabindex="16" type="text"/>
       <select autofocus="" class="txtinput" name="poall" required="" tabindex="18">
        <option>
         If Overall Above 60%
        </option>
        <option value="yes">
         Yes
        </option>
        <option value="no">
         No
        </option>
       </select>
      </div>
      <!-- End Of Col MD 6 -->
      <div class="col-md-12">
       <input name="submitReg" type="submit" value="Submit"/>
      </div>
      <!-- End Of Col MD 6 -->
     </div>
     <!-- End Of Row -->
    </form>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <form class="searchForm">
     <input placeholder="Search" type="text"/>
    </form>
    <div class="widgetArea">
     <h5>
      CONTACT INFO
     </h5>
     <address>
      <span class="address">
       A - 43 &amp; A - 52 Sector - 16,
       <br/>
       Noida (U.P) (Near McDonalds)
      </span>
      <br>
       <span class="phone">
        <strong>
         Phone:
        </strong>
        0120-4646464, +91- 9871055180
       </span>
       <br/>
       <span class="email">
        <strong>
         E-Mail:
        </strong>
        <a href="mailto:info@ducatindia.com">
         info@ducatindia.com
        </a>
       </span>
       <br/>
       <span class="web">
        <strong>
         Web:
        </strong>
        <a href="http://www.ducatindia.com/">
         http://www.ducatindia.com/
        </a>
       </span>
      </br>
     </address>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section class="text-center" id="offices">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <h5>
     CORPORATE OFFICE NOIDA:
     <span>
      0120 - 4646464
     </span>
    </h5>
    <p>
     GR.NOIDA:
     <span>
      0120-4345190
     </span>
     GHAZIABAD:
     <span>
      0120-4835400
     </span>
     FARIDABAD:
     <span>
      0129-4150605
     </span>
     GURGAON:
     <span>
      0124-4219095
     </span>
     JAIPUR:
     <span>
      0141-2550077
     </span>
    </p>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>