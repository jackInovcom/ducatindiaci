<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     Java For Beginners Training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Java For Beginners
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Java For Beginners! Learn a little bit about what Java methods are, how to write them, and how to use them. This tutorial won't cover everything, but it'll be just enough to get you to use them and serves as a jump-off point to the more advanced tutorials.
     </p>
     <div class="contentAcc">
      <h2>
       INTRODUCTION TO JAVA
      </h2>
      <ul>
       <li>
        Why Java was Developed
       </li>
       <li>
        Application Areas of Java
       </li>
       <li>
        History of Java
       </li>
       <li>
        Platform Independency in Java
       </li>
       <li>
        USP of Java: Java Features
       </li>
       <li>
        Sun-Oracle Deal
       </li>
       <li>
        Different Java Platforms
       </li>
       <li>
        Difference between JDK,JRE,JVM
       </li>
       <li>
        Java Versions
       </li>
       <li>
        JVM Architecture
       </li>
       <li>
        Installing Java on Windows
       </li>
       <li>
        Understanding Path Variable: Why Set Path
       </li>
      </ul>
      <h2>
       CREATING FIRST JAVA PROGRAM
      </h2>
      <ul>
       <li>
        Understanding Text Editors to Write Programs
       </li>
       <li>
        How to compile java file
       </li>
       <li>
        Byte Code and class file
       </li>
       <li>
        How to run class file
       </li>
      </ul>
      <h2>
       JAVA LANGUAGE FUNDAMENTALS
      </h2>
      <ul>
       <li>
        Identifiers
       </li>
       <li>
        Keywords
       </li>
       <li>
        Variables
       </li>
       <li>
        Literals
       </li>
       <li>
        Data Types
       </li>
       <li>
        Operators
       </li>
       <li>
        Comments
       </li>
       <li>
        Looping Statements
       </li>
       <li>
        Condition Statements
       </li>
       <li>
        Type Casting
       </li>
      </ul>
      <h2>
       OOP IMPLEMENTATION (PIE)
      </h2>
      <ul>
       <li>
        Why OOP
       </li>
       <li>
        OOP Concepts with Real life examples
       </li>
       <li>
        Class&amp; it's Syntax
       </li>
       <li>
        Object&amp; it's Syntax
       </li>
       <li>
        Reference Variable
       </li>
       <li>
        Constructors
       </li>
       <li>
        Instance(Non-Static)&amp; Static Variables
       </li>
       <li>
        Instance(Non-Static) &amp; Static Methods
       </li>
       <li>
        this Keyword and it's usages
       </li>
       <li>
        Object &amp; Static Initializers(Anonymous Blocks)
       </li>
       <li>
        Understanding '+' Operator
       </li>
       <li>
        Inheritance&amp; it's Syntax
       </li>
       <li>
        Types of Inheritance
       </li>
       <li>
        Object Class as Root of Java Class Hierarchy
       </li>
       <li>
        Variable Hiding
       </li>
       <li>
        Method Hiding
       </li>
       <li>
        Method Overriding
       </li>
       <li>
        Method Overloading
       </li>
       <li>
        Super keyword and it's usages
       </li>
       <li>
        Final keyword and it's usages
       </li>
       <li>
        Constructor Chaining
       </li>
       <li>
        Upcasting and Downcasting
       </li>
       <li>
        Static &amp;Dynamic Binding
       </li>
       <li>
        Run Time Polymorphism
       </li>
       <li>
        Abstract Keyword(Abstract classes and methods)
       </li>
       <li>
        Understanding Interfaces
       </li>
       <li>
        Implementation of Encapsulation
       </li>
       <li>
        Association with Implementation
       </li>
      </ul>
      <h2>
       PACKAGES
      </h2>
      <ul>
       <li>
        Understanding Packages
       </li>
       <li>
        Setting Class path
       </li>
       <li>
        Reading Input from Keyboard
       </li>
       <li>
        Access Modifiers
       </li>
      </ul>
      <h2>
       NESTED TYPES
      </h2>
      <ul>
       <li>
        Static Nested Class
       </li>
       <li>
        Non-static Nested Class
       </li>
       <li>
        Local Class
       </li>
       <li>
        Anonymous Class
       </li>
       <li>
        Nested Interface
       </li>
      </ul>
      <h2>
       ARRAYS
      </h2>
      <ul>
       <li>
        General Definition of Array
       </li>
       <li>
        Advantages from Array
       </li>
       <li>
        Arrays in Java
       </li>
       <li>
        1-d Arrays
       </li>
       <li>
        2-d Arrays
       </li>
       <li>
        Jagged Arrays
       </li>
       <li>
        Array of reference type
       </li>
       <li>
        Operations on Arrays
       </li>
      </ul>
      <h2>
       COMMAND LINE ARGUMENTS AND WRAPPER CLASSES
      </h2>
      <ul>
       <li>
        How to read command line arguments
       </li>
       <li>
        Wrapper Classes
       </li>
       <li>
        Parsing of Numeric Strings
       </li>
       <li>
        String representation of Primitives
       </li>
      </ul>
      <h2>
       EXCEPTION HANDLING
      </h2>
      <ul>
       <li>
        Types of Runtime Errors
       </li>
       <li>
        Understanding Exceptions
       </li>
       <li>
        Exception Class Hierarchy
       </li>
       <li>
        Try &amp; Catch Blocks
       </li>
       <li>
        Patterns of Catch Block
       </li>
       <li>
        Nested Try statements
       </li>
       <li>
        Throw, throws and finally
       </li>
       <li>
        Creating Custom Exceptions
       </li>
       <li>
        Checked &amp; Unchecked Exceptions
       </li>
       <li>
        Assertion
       </li>
      </ul>
      <h2>
       WORKING WITH STRINGS
      </h2>
      <ul>
       <li>
        What is String
       </li>
       <li>
        String Class
       </li>
       <li>
        Creating String Object
       </li>
       <li>
        Operations on String
       </li>
       <li>
        String Buffer Class and it's Methods
       </li>
       <li>
        Difference between String and StringBuffer class
       </li>
       <li>
        String Builder Class and it's Methods
       </li>
       <li>
        Difference between StringBuffer and StringBuilder
       </li>
      </ul>
      <h2>
       SWING
      </h2>
      <ul>
       <li>
        Introduction to AWT
       </li>
       <li>
        Introduction to Swing Components
       </li>
       <li>
        Look And Feel of Swing Components
       </li>
       <li>
        MVC Architecture of Swing Components
       </li>
       <li>
        Working with Image
       </li>
       <li>
        Advance Swing Components
       </li>
       <ul>
        <li>
         JOptionPane,JTree,JTable,JTabbedPane
        </li>
        <li>
         JfileChooser,JcolorChooser
        </li>
       </ul>
       <li>
        Menu Components
       </li>
       <ul>
        <li>
         JMenu
        </li>
        <li>
         JMenuItem
        </li>
        <li>
         JMenubar
        </li>
       </ul>
      </ul>
      <h2>
       MULTITHREADED PROGRAMMING
      </h2>
      <ul>
       <li>
        Multitasking: Why Concurrent Execution
       </li>
       <li>
        Multiprocessing v/s Multithreading
       </li>
       <li>
        Main Thread (Default Java Thread)
       </li>
       <li>
        Creating Child Threads and understanding context switching
       </li>
       <li>
        Thread States
       </li>
       <li>
        Thread Group
       </li>
       <li>
        Thread Synchronization: Methods and Blocks
       </li>
       <li>
        Inter-Thread communication
       </li>
       <li>
        Daemon Threads
       </li>
       <li>
        Deadlock
       </li>
      </ul>
      <h2>
       I/O STREAMS
      </h2>
      <ul>
       <li>
        What is I/O
       </li>
       <li>
        Why Need Streams
       </li>
       <li>
        Byte Streams and Character Streams
       </li>
       <li>
        Read/Write operations with file
       </li>
       <li>
        Scanner Class
       </li>
       <li>
        Object Serialization&amp; Deserialization
       </li>
       <li>
        Transient keyword
       </li>
       <li>
        File Class and it's Methods
       </li>
      </ul>
      <h2>
       SOCKET PROGRAMMING
      </h2>
      <ul>
       <li>
        Understanding Fundamentals of a Network
       </li>
       <li>
        Socket and ServerSocket Classes
       </li>
       <li>
        InetAddress Class
       </li>
       <li>
        DatagramSocket and DatagramPacket Classes
       </li>
       <li>
        URL,URLConnection,HttpURLConnection Classes
       </li>
      </ul>
      <h2>
       REFLECTION
      </h2>
      <ul>
       <li>
        Understanding the Need Of Reflection
       </li>
       <li>
        Getting information about class's modifiers, fields, methods, constructors and super classes
       </li>
       <li>
        Finding out constant and method declaration belong to an interface
       </li>
       <li>
        Creating an instance of the class whose name is not known until runtime
       </li>
       <li>
        Getting and setting values of an object's field if field name is unknown until runtime
       </li>
       <li>
        Invoking a method on an object if the method is unknown until runtime
       </li>
       <li>
        Invoking Private Methods
       </li>
      </ul>
      <h2>
       EXTENDED &amp; UTILITY CONCEPTS
      </h2>
      <ul>
       <li>
        Generics
       </li>
       <li>
        Lambda Expression
       </li>
       <li>
        Annotations
       </li>
       <li>
        Object Cloning
       </li>
       <li>
        Vargs
       </li>
       <li>
        Static-import
       </li>
       <li>
        Enum
       </li>
       <li>
        Static, Default and Private Methods of Interface
       </li>
       <li>
        Var Type
       </li>
       <li>
        Java Modules
       </li>
      </ul>
      <h2>
       COLLECTIONS FRAMEWORK
      </h2>
      <ul>
       <li>
        What is Collection?
       </li>
       <li>
        What is Framework?
       </li>
       <li>
        Collections Framework
       </li>
       <li>
        Core Interfaces
       </li>
       <li>
        Collection, List, Queue,Deque
       </li>
       <li>
        Set,NavigableSet, SortedSet
       </li>
       <li>
        Map,NavigableMap, SortedMap
       </li>
       <li>
        Core Classes
       </li>
       <li>
        ArrayList, LinkedList,PriorityQueue,ArrayDeque
       </li>
       <li>
        HashSet,LinkedHasSet,TreeSet,
       </li>
       <li>
        HashMap,IdentityHashMap,WeakHashMap,LinkedHashMap,Tree Map
       </li>
       <li>
        Accessing a Collection via an Iterator
       </li>
       <li>
        Accessing List via ListIterator
       </li>
       <li>
        Accessing a Collection via for each loop
       </li>
       <li>
        Working with User Defined Objects
       </li>
       <li>
        The Comparator and Comparable Interfaces
       </li>
       <li>
        The Legacy classes and Interfaces.
       </li>
       <li>
        Enumeration, Vector ,Stack
       </li>
       <li>
        Hashtable, Properties
       </li>
      </ul>
      <h2>
       DATE &amp; TIME API
      </h2>
      <ul>
       <li>
        java.util.Date
       </li>
       <li>
        java.util.Calender
       </li>
       <li>
        java.sql.Date
       </li>
      </ul>
      <h2>
       JODA API
      </h2>
      <ul>
       <li>
        java.time.LocalDate
       </li>
       <li>
        java.time.LocalTime
       </li>
       <li>
        java.time.LocalDateTime
       </li>
      </ul>
      <h2>
       SYSTEM PROPERTIES &amp; INTERNATIONALIZATION (I18N)
      </h2>
      <ul>
       <li>
        Understanding Locale
       </li>
       <li>
        Resource Bundle
       </li>
       <li>
        Usage of properties file
       </li>
       <li>
        Fetching text from Resource Bundle
       </li>
       <li>
        Displaying the text in HINDI
       </li>
       <li>
        Displaying date in Hindi
       </li>
      </ul>
      <h2>
       INTRODUCTION TO SQL (PROJECT BASED)
      </h2>
      <h2>
       DATABASE PROGRAMMING USING JDBC
      </h2>
      <ul>
       <li>
        Need Of JDBC
       </li>
       <li>
        JDBC Drivers
       </li>
       <li>
        Statement, PreparedStatement, CallableStatement
       </li>
       <li>
        Scrollable and Updatable ResultSet
       </li>
       <li>
        Batch Updates
       </li>
       <li>
        Transaction
       </li>
       <li>
        Metadata
       </li>
      </ul>
      <h2>
       PROJECT CLASSES
      </h2>
      <ul>
       <li>
        Front End Coding
       </li>
       <li>
        Form Designing
       </li>
       <li>
        Back End Coding
       </li>
       <li>
        Database Designing
       </li>
       <li>
        Connecting forms to database
       </li>
       <li>
        Writing Business Logic
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="javabeginners.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="4 + 93 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="97">
       <input name="url" type="hidden" value="/java-for-beginners/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>