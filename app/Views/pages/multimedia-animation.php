<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     MULTIMEDIA &amp; ANIMATION
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     MULTIMEDIA &amp; ANIMATION
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      A Training That Makes You the Best in Technical Field
     </h4>
     <p>
      We provide you the real time production training in animation, VFX, Maya, Web Designing. which is relevant, practical and industry level. Our students will have the support, ideas and encouragement from experienced practioners, visiting lecturers and professionals.
     </p>
     <div class="contentAcc">
      <h2>
       MULTIMEDIA AND ANIMATION Document Overview
      </h2>
      <ul>
       <li>
        The Structure Tags
       </li>
       <li>
        Components of the HTML Code
       </li>
       <li>
        Getting Started With HTML
       </li>
       <li>
        Writing the Code
       </li>
      </ul>
      <h2>
       Head Elements
      </h2>
      <ul>
       <li>
        HTML Tags
       </li>
       <li>
        HEAD Tags
       </li>
       <li>
        Title Tags
       </li>
       <li>
        Body Tag
       </li>
       <li>
        Displaying a web page in a web Browser
       </li>
      </ul>
      <h2>
       Block Oriented Elements
      </h2>
      <ul>
       <li>
        Defining Paragraphs and new Lines
       </li>
       <li>
        Inserting Spaces
       </li>
       <li>
        Perforating the Text
       </li>
       <li>
        FONT Tag
       </li>
       <li>
        Changing Background Colour
       </li>
      </ul>
      <h2>
       List
      </h2>
      <ul>
       <li>
        Ordered List
       </li>
       <li>
        Unordered List
       </li>
       <li>
        Definition List
       </li>
      </ul>
      <h2>
       Attributes of List
      </h2>
      <ul>
       <li>
        Using Type attribute with Unordered list
       </li>
       <li>
        Using Stat attribute with Ordered List
       </li>
       <li>
        Some other elements
       </li>
       <li>
        Visual Markup
       </li>
      </ul>
      <h2>
       Uniform Resource Locators
      </h2>
      <ul>
       <li>
        Types of URLs
       </li>
       <li>
        Absolute URLs
       </li>
       <li>
        Relative URLs
       </li>
       <li>
        Document-Relative URLs
       </li>
       <li>
        Server-relative URLs
       </li>
      </ul>
      <h2>
       Hypertext Links
      </h2>
      <ul>
       <li>
        Why links fail?
       </li>
      </ul>
      <h2>
       Images
      </h2>
      <ul>
       <li>
        Adding Images
       </li>
       <li>
        Adding Border to an image
       </li>
      </ul>
      <h2>
       Tables
      </h2>
      <ul>
       <li>
        Adding Border to a Table
       </li>
       <li>
        Apply Formatting Features to Table
       </li>
       <li>
        Spanning Columns and Rows
       </li>
       <li>
        Align Data in Table
       </li>
      </ul>
      <h2>
       Frames Forms
      </h2>
      <ul>
       <li>
        CGI Script
       </li>
       <li>
        Creating a Form
       </li>
      </ul>
      <h2>
       Adding Special Characters Introduction to style   Sheets
      </h2>
      <ul>
       <li>
        How style sheets work
       </li>
       <li>
        Style Sheet Structure
       </li>
      </ul>
      <h2>
       Changing the Background
      </h2>
      <ul>
       <li>
        Changing the color of the text
       </li>
       <li>
        Changing the front
       </li>
       <li>
        Changing the Indent of the text
       </li>
       <li>
        Changing the Line Spacing
       </li>
       <li>
        Hide a Style Sheet
       </li>
      </ul>
      <h2>
       Creating Style Classes
      </h2>
      <ul>
       <li>
        Div
       </li>
       <li>
        Span
       </li>
       <li>
        Layer
       </li>
       <li>
        Meta
       </li>
      </ul>
      <h2>
       Add Audio and Video in Web Page
      </h2>
      <ul>
       <li>
        Audio in Web Page
       </li>
       <li>
        Video
       </li>
      </ul>
      <h2>
       Introduction to Dreamweaver MX
      </h2>
      <ul>
       <li>
        The Designer Workspace
       </li>
       <li>
        The Code Workspace
       </li>
       <li>
        Introduction to Panels and Toolbars
       </li>
       <li>
        Defining a Local Site
       </li>
       <li>
        Managing Site Info
       </li>
       <li>
        Viewing the code Inspector
       </li>
      </ul>
      <h2>
       Designing Your Page
      </h2>
      <ul>
       <li>
        Setting Page Properties
       </li>
       <li>
        Inserting Text and Setting Their Properties
       </li>
       <li>
        Inserting Images
       </li>
       <li>
        Inserting a Horizontal Rule
       </li>
       <li>
        Inserting Email Link
       </li>
       <li>
        Insetting Special Characters
       </li>
       <li>
        Spell Checking
       </li>
       <li>
        Saving the File
       </li>
       <li>
        Saving and Previewing the Files
       </li>
      </ul>
      <h2>
       Introducing Tables
      </h2>
      <ul>
       <li>
        Fundamentals of HTML Tables
       </li>
       <li>
        Inserting Tables in Dreamweaver
       </li>
       <li>
        Selecting Table Properties
       </li>
       <li>
        Nested Ta
       </li>
      </ul>
      <h2>
       Working with Anchors
      </h2>
      <ul>
       <li>
        Creating a Named Anchor
       </li>
       <li>
        Linking a Named Anchor Externally
       </li>
       <li>
        Linking a Named Anchor Internally
       </li>
      </ul>
      <h2>
       Introducing Framesets
      </h2>
      <ul>
       <li>
        Creating Frames and Framesets
       </li>
       <li>
        Predefined Frameset
       </li>
       <li>
        Create a Predefined Frameset/li&gt;
       </li>
       <li>
        Designing a Frameset
       </li>
       <li>
        Setting Framework Properties
       </li>
       <li>
        Setting a Title
       </li>
       <li>
        Insert Content into Frames
       </li>
       <li>
        Saving Frame and Frameset Files
       </li>
       <li>
        Saving a Frameset
       </li>
       <li>
        Saving a Document that Appears in a
       </li>
       <li>
        Frame
       </li>
       <li>
        Nested framesets
       </li>
       <li>
        Selecting Frame in the Frames Panel
       </li>
       <li>
        Selecting Frameset in the Frames Panel
       </li>
       <li>
        Exercise
       </li>
      </ul>
      <h2>
       Style Sheets
      </h2>
      <ul>
       <li>
        Style Sheets
       </li>
       <li>
        External style sheets
       </li>
       <li>
        Internal Style sheets
       </li>
       <li>
        Embedded style rules
       </li>
       <li>
        Styles and their Attributes
       </li>
       <li>
        Type Category
       </li>
       <li>
        Background Category
       </li>
       <li>
        Block Category
       </li>
       <li>
        Box Category
       </li>
       <li>
        Border Category
       </li>
       <li>
        Positioning Category
       </li>
       <li>
        Extensions Category
       </li>
       <li>
        Creating HTML Style Sheet
       </li>
       <li>
        Creating a Style Sheet
       </li>
       <li>
        Creating External Style Sheet
       </li>
       <li>
        Linking to an External Style Sheet
       </li>
       <li>
        Defining Style Sheet in a Document
       </li>
       <li>
        Exercise
       </li>
      </ul>
      <h2>
       Forms at a Glance
      </h2>
      <ul>
       <li>
        Inserting a Form
       </li>
       <li>
        Forms Panel in the Insert bar
       </li>
       <li>
        Form Objects
       </li>
       <li>
        Using Text Fields
       </li>
       <li>
        Setting the Form Using Tables
       </li>
       <li>
        Exercise
       </li>
      </ul>
      <h2>
       Behaviors
      </h2>
      <ul>
       <li>
        Using the Behaviors Panel
       </li>
       <li>
        Using Some Standard Behavior
       </li>
       <li>
        Call JavaScript
       </li>
       <li>
        Change Property
       </li>
       <li>
        Go to URL
       </li>
       <li>
        Check Plugin
       </li>
       <li>
        Control Shockwave or Flash
       </li>
       <li>
        Drag Layer
       </li>
       <li>
        Open Browser Window
       </li>
       <li>
        Popup Message
       </li>
       <li>
        Show-Hide Layers
       </li>
       <li>
        Inserting Rollover Images
       </li>
       <li>
        Creating a Rollover Image
       </li>
       <li>
        Inserting Navigation Bar
       </li>
       <li>
        Inserting a Navigation Bar
       </li>
       <li>
        Exercise
       </li>
      </ul>
      <h2>
       Media Elements
      </h2>
      <ul>
       <li>
        Inserting Media Elements
       </li>
       <li>
        Creating Flash buttons
       </li>
       <li>
        Media Element Properties
       </li>
       <li>
        Commands Menu
       </li>
       <li>
        Creating a Command
       </li>
       <li>
        Templates
       </li>
       <li>
        Creating a TemplatE
       </li>
       <li>
        Inserting an Editable Regions
       </li>
       <li>
        Inserting an Editable Template Region
       </li>
       <li>
        Selecting Editable Regions
       </li>
       <li>
        Removing an Editable Region
       </li>
       <li>
        Locked Region
       </li>
       <li>
        Repeating Region
       </li>
       <li>
        Optional Region
       </li>
       <li>
        Applying a Template
       </li>
       <li>
        Applying a Template
       </li>
       <li>
        Nested Templates
       </li>
       <li>
        Exercise
       </li>
      </ul>
      <h2>
       Flash
      </h2>
      <ul>
       <li>
        Introduction to Flash 8
       </li>
       <li>
        Stating Flash 8
       </li>
       <li>
        Flash 8 Workspace
       </li>
       <li>
        The Stage
       </li>
       <li>
        Tools
       </li>
       <li>
        The Selection Tool
       </li>
       <li>
        Using the Selection Tool to reshape objects
       </li>
       <li>
        The Sub selection Tool
       </li>
       <li>
        The Free Transform Tool
       </li>
       <li>
        The Gradient Transform Tool
       </li>
       <li>
        Drawing Straight Lines with Line Tool
       </li>
       <li>
        The Lasso Tool
       </li>
       <li>
        The Pen Tool
       </li>
       <li>
        Setting Pen Tool Preferences
       </li>
       <li>
        Creating a Straight Line Path
       </li>
       <li>
        Creating a Curved Path
       </li>
       <li>
        Modifying paths created with Pen tools
       </li>
       <li>
        The Text Tool
       </li>
       <li>
        Drawing Oval and Circles with the Oval Tools
       </li>
       <li>
        The Pencil Tool
       </li>
       <li>
        Painting with the Brush Tool
       </li>
       <li>
        Fills and Outlines
       </li>
       <li>
        Using the Property Inspector to set thefill and outline attributes
       </li>
       <li>
        Using the tools to Modify the Fill and Stroke
       </li>
       <li>
        The Paint Bucket Tool
       </li>
       <li>
        Zooming
       </li>
       <li>
        Zooming using the Zoom Tool
       </li>
       <li>
        Timeline
       </li>
       <li>
        Layers in Flash
       </li>
       <li>
        The Properties of layer
       </li>
       <li>
        Insert Layer Creating a Layer
       </li>
       <li>
        Deleting a Layer
       </li>
       <li>
        Insert a Layer Folders
       </li>
       <li>
        The Property Inspector
       </li>
       <li>
        Alignment Panel
       </li>
       <li>
        Applying Locked Gradient as a Fill
       </li>
       <li>
        Info Panel
       </li>
       <li>
        Transform Panel
       </li>
       <li>
        Library Panel
       </li>
       <li>
        Exercise
       </li>
      </ul>
      <h2>
       File Management
      </h2>
      <ul>
       <li>
        Open a New File
       </li>
       <li>
        Save a File
       </li>
       <li>
        Save as Template
       </li>
       <li>
        Import
       </li>
       <li>
        Export Movie and Export Image
       </li>
       <li>
        Export Image
       </li>
       <li>
        Publishing a Flash Movie
       </li>
       <li>
        Publish
       </li>
       <li>
        Publish Setting
       </li>
       <li>
        The Concept of Convert the Bitmap to a vector art with Trace Bitmap
       </li>
       <li>
        Combine Objects
       </li>
       <li>
        Exercise
       </li>
      </ul>
      <h2>
       Concept of Animation
      </h2>
      <ul>
       <li>
        The Pencil Tool
       </li>
       <li>
        Animation
       </li>
       <li>
        Persistence of Vision
       </li>
       <li>
        Concept of Story Boarding
       </li>
       <li>
        The Concept of Frame-by-Frame Animation
       </li>
       <li>
        The Key Frame, Static Frame and Blank Key Frame
       </li>
       <li>
        Inserting Frames, Keyframes of Blank frames
       </li>
       <li>
        Deleting Frames
       </li>
       <li>
        Deleting Key Frames
       </li>
       <li>
        The Concept of FPS (Frames Per Second)
       </li>
       <li>
        NTSC
       </li>
       <li>
        PAL
       </li>
       <li>
        The Concept of Tweening Animation
       </li>
       <li>
        The use of Shape Tweening Aimation
       </li>
       <li>
        Shape Tweening Text
       </li>
       <li>
        Animating Gradient Colors
       </li>
       <li>
        The Movement of Gradient Color with the help of Gradient Transform Tool
       </li>
       <li>
        Shape Hint
       </li>
       <li>
        Shape Tween Properties
       </li>
       <li>
        Ease Setting
       </li>
       <li>
        Blend Setting
       </li>
       <li>
        The use of Break Apart
       </li>
       <li>
        Distribute to Layers
       </li>
       <li>
        Exercise
       </li>
      </ul>
      <h2>
       All About Symbols and Library
      </h2>
      <ul>
       <li>
        Creating Symbols
       </li>
       <li>
        Converting an Existing Object into a Symbol
       </li>
       <li>
        Creating a New Symbol
       </li>
       <li>
        Editing Symbols
       </li>
       <li>
        Edit in Symbol-Editing Mode
       </li>
       <li>
        Edit in Place
       </li>
       <li>
        Edit in a New Window
       </li>
       <li>
        Modifying the Symbol properties
       </li>
       <li>
        Modifying the Instance of a Symbol
       </li>
       <li>
        Color
       </li>
       <li>
        Using the Library
       </li>
       <li>
        Working with Folders in the Library Panel
       </li>
       <li>
        Sorting Items in the Library Panel
       </li>
       <li>
        Deleting Items in Library Panel
       </li>
       <li>
        Adding Front Symbol in the Library Panel
       </li>
       <li>
        Adding Video in the Library Panel
       </li>
       <li>
        Motion Tween with Change of Position, Rotation, and Scale
       </li>
       <li>
        Rotating Objects
       </li>
       <li>
        Working with the Custom Ease In / Ease Out Feature
       </li>
       <li>
        Moving an Object along a defined path using Motion Guide
       </li>
       <li>
        Creating Masking Effects
       </li>
       <li>
        Moving a text using Motion Tweening
       </li>
       <li>
        Adding Sound Overview
       </li>
       <li>
        Importing Sounds
       </li>
       <li>
        Adding Sound to a Movie
       </li>
       <li>
        Exercise
       </li>
      </ul>
      <h2>
       Character Animation
      </h2>
      <ul>
       <li>
        Timeline effect
       </li>
       <li>
        Working with Time Effects in the Transform / Transition Categories
       </li>
       <li>
        Working with Timeline Effects in the Assistant category
       </li>
       <li>
        Working with Timeline Effects in the Effects Category
       </li>
       <li>
        Using the Drop Shadow Effect
       </li>
       <li>
        Filter
       </li>
       <li>
        Exercise
       </li>
      </ul>
      <h2>
       The Concept of Button
      </h2>
      <ul>
       <li>
        Creating a Button
       </li>
       <li>
        The Concept of creating a Project
       </li>
       <li>
        How to optimize the Flash Project?
       </li>
       <li>
        The Concept of Steaming
       </li>
       <li>
        Scene
       </li>
       <li>
        Create Projects in Different Scenes
       </li>
       <li>
        Motion Tweening
       </li>
       <li>
        Motion Guide
       </li>
       <li>
        Motion Tweening Text
       </li>
       <li>
        Creating Masking Effect
       </li>
       <li>
        Onion Skin
       </li>
       <li>
        Exercise
       </li>
      </ul>
      <h2>
       The Concept of Action Script
      </h2>
      <ul>
       <li>
        The Concept of ActionScript
       </li>
       <li>
        Modes of ActionScript
       </li>
       <li>
        The Concept of Apply action on Frame, Button and Movie-Clip
       </li>
       <li>
        The Concept of Action
       </li>
       <li>
        The Concept of Event
       </li>
       <li>
        How Are Events Responsible for the Execution of an Action?
       </li>
       <li>
        Basic Action
       </li>
       <li>
        FS Command
       </li>
       <li>
        The Concept of a Variable
       </li>
       <li>
        Load Movie
       </li>
       <li>
        Navigate TOURL Function
       </li>
       <li>
        Start Drag and Stop Drag
       </li>
       <li>
        Removing a Movie Clip
       </li>
       <li>
        Concept of Object
       </li>
       <li>
        Flash Shortcut Keys
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="multimediatraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="98 + 99 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="197">
       <input name="url" type="hidden" value="/multimedia-animation/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>