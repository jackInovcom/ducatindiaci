<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     6 WEEKS SUMMER TRAINING
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     6 WEEKS SUMMER TRAINING
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <p>
    The
    <strong>
     Summer Training Ducat
    </strong>
    Courses deliver a broad range of fundamental and specialty Summer Training courses designed to help build a competent, qualified, and efficient workforce.
   </p>
   <p>
    <strong>
     Ducat India
    </strong>
    Courses provide a powerful training tool that can be used directly where training is required, e.g., in the classroom, at the plant, in the office. It offers all the important content in the appropriate context, comprehensive assessments, and the latest tools to evaluate performance. With seamless integration into Mind-Sight, the Ducat �Courses provide a multitude of ways to fulfill Summer Training needs.
   </p>
   <h4>
    FEATURES &amp; BENEFITS
   </h4>
   <ul>
    <li>
     Broad range of fundamental and specialty Summer Training courses
    </li>
    <li>
     Used by top-tier manufacturing, industrial, and technical associations
    </li>
    <li>
     Comprehensive assessments
    </li>
    <li>
     Seamless integration within the Mind-Sight learning and content management system
    </li>
   </ul>
   <br/>
   <p>
    <strong>
     Project Based Summer Training
    </strong>
    is conducted by Ducat India with a range of courses to be delivered across number of locations. The programs usually conducted twice a year, beginning in the month of January and July. The duration of the program varies from 4 to 6 months depending on student needs and qualified partner professionals are engaged to deliver the program and guide the trainees throughout the program. There are around 50-60 participants in a batch with a good student-trainer ratio for effective learning. Upon completion of the program, training certificate and project letter will be provided to successful students.
   </p>
   <p>
    <strong>
     Ducat
    </strong>
    is providing great opportunities in the growing field to the students. We have best career boosting options for the learners. At
    <strong>
     Ducat
    </strong>
    we provide training to the BCA, Bsc. IT, B.tech and MCA students which are required to get engage in the large software, web development and internet marketing companies. It has been observed that after completing education from the colleges with very good percentage, it becomes difficult for the fresher to get a suitable job. It is not their fault; however in colleges they don�t get chance to learn job oriented education. We are here to make them efficient so that it became easy for them to get job in any required field in the IT industry. We focus to give best training sessions to the students that help them in their skill enhancement. We provide different training courses to our employees that include complete teaching sessions and live projects handling experience. It helps them to enhance their basic as well as practical knowledge. We also provide job opportunities to our trainees if they learn and grow with our training programs
   </p>
   <div class="col-md-4">
    <div align="center" class="eventBox">
     <img alt="Event" class="img-responsive" src="../images/java.jpg"/>
     <div class="clearfix">
     </div>
     <h4>
      JAVA SIX WEEKS TRAINING
     </h4>
     <br/>
     <p>
      JAVA IN 6 Weeks course at DUCAT prepares the participants to leverage this demand by making them conceptually and practically strong in the technology
     </p>
     <a href="../javasixweekstraining">
      Continue Reading
     </a>
    </div>
   </div>
   <!-- End Of Col MD 4 -->
   <div class="col-md-4">
    <div align="center" class="eventBox">
     <img alt="Event" class="img-responsive" src="../images/dot-net.jpg"/>
     <div class="clearfix">
     </div>
     <h4>
      .NET Six WeeksTraining
     </h4>
     <br/>
     <p>
      DUCAT has a training course named MICROSOFT .NET IN 6 Weeks. This   course would make you technically, practically and fundamentally   strong....
     </p>
     <a href="../dotnetsixweekstraining">
      Continue Reading
     </a>
    </div>
    <!-- End Of Col MD 4 -->
   </div>
   <div class="col-md-4">
    <div align="center" class="eventBox">
     <img alt="Event" class="img-responsive" src="../images/php.jpg"/>
     <div class="clearfix">
     </div>
     <h4>
      PHP Six weeks Training
     </h4>
     <br/>
     <p>
      DUCAT provides the best known experienced faculties with a vast sea of knowledge about their particular stream...
     </p>
     <a href="../phpsixweekstraining">
      Continue Reading
     </a>
    </div>
   </div>
   <!-- End Of Col MD 4 -->
  </div>
  <!-- End Of Row -->
  <div class="row">
   <div class="col-md-4">
    <div align="center" class="eventBox">
     <img alt="Event" class="img-responsive" src="../images/embedded-system.jpg"/>
     <div class="clearfix">
     </div>
     <h4>
      Embedded Six Weeks Training
     </h4>
     <br/>
     <p>
      A never before in the history of automation, was quite possible without embedded system....
     </p>
     <a href="../8051-microcontroller-training">
      Continue Reading
     </a>
    </div>
   </div>
   <div class="col-md-4">
    <div align="center" class="eventBox">
     <img alt="Event" class="img-responsive" src="../images/sqt.jpg"/>
     <div class="clearfix">
     </div>
     <h4>
      SQT Six Weeks Training
     </h4>
     <br/>
     <p>
      Be a Profession with Technical Training Courses. DUCAT� is one of the most preferred places....
     </p>
     <a href="../sqtsixweekstraining">
      Continue Reading
     </a>
    </div>
   </div>
   <div class="col-md-4">
    <div align="center" class="eventBox">
     <img alt="Event" class="img-responsive" src="../images/cloud.jpg"/>
     <div class="clearfix">
     </div>
     <h4>
      CLOUD Six Weeks Training
     </h4>
     <br/>
     <p>
      CLOUD COMPUTING involves large no. of computers connected through real time communication network like internet...
     </p>
     <a href="../cloudtraining">
      Continue Reading
     </a>
    </div>
   </div>
  </div>
  <div class="row">
   <div class="col-md-4">
    <div align="center" class="eventBox">
     <img alt="Event" class="img-responsive" src="../images/autocad.png"/>
     <div class="clearfix">
     </div>
     <h4>
      Autocad Six Weeks Training
     </h4>
     <br/>
     <p>
      People who want to have a respectable job must have CAD 6 Weeks   training. DUCAT is the best choice for them to have this training...
     </p>
     <a href="../autocadtraining">
      Continue Reading
     </a>
    </div>
   </div>
   <div class="col-md-4">
    <div align="center" class="eventBox">
     <img alt="Event" class="img-responsive" src="../images/oracel.jpg"/>
     <div class="clearfix">
     </div>
     <h4>
      Oracle Six Weeks Training
     </h4>
     <br/>
     <p>
      DUCAT is the only training institute giving technical training with a difference. The institute not only provides training but takes proper care that the  training..
     </p>
     <a href="../oracle11gdbatraining">
      Continue Reading
     </a>
    </div>
   </div>
   <div class="col-md-4">
    <div align="center" class="eventBox">
     <img alt="Event" class="img-responsive" src="../images/networking.jpg"/>
     <div class="clearfix">
     </div>
     <h4>
      Networking Six weeks Training
     </h4>
     <br/>
     <p>
      DUCAT is a well esteemed classroom that provides networking training   programs for fresh IT graduates. We provide live projects for candidates   that greatly..
     </p>
     <a href="../ccnatraining">
      Continue Reading
     </a>
    </div>
   </div>
  </div>
  <!-- <div class="row">
               
                     <div class="col-md-4">
                      
                      <div class="eventBox" align="center">
                        
                          <img src="../images/3dmax.png" alt="Event" class="img-responsive">

                          
                          

                          <div class="clearfix"></div>

                          <h4>3D Studio Max Training
 Certification</h4>
                            <br/>
                          <p>DUCAT is the ideal place where anyone aspirant to learn 3D Studio Max Certification !</p>
                          <a href="3dmaxtraining">Continue Reading</a>

                      </div>
                  </div>  
                  <div class="col-md-4">
                      
                      <div class="eventBox" align="center">
                        
                          <img src="../images/3dmax.png" alt="Event" class="img-responsive">

                          
                          

                          <div class="clearfix"></div>

                          <h4>3D Studio Max Training
 Certification</h4>
                            <br/>
                          <p>DUCAT is the ideal place where anyone aspirant to learn 3D Studio Max Certification !</p>
                          <a href="3dmaxtraining">Continue Reading</a>

                      </div>
                  </div>  
                  <div class="col-md-4">
                      
                      <div class="eventBox" align="center">
                        
                          <img src="../images/3dmax.png" alt="Event" class="img-responsive">

                          
                          

                          <div class="clearfix"></div>

                          <h4>3D Studio Max Training
 Certification</h4>
                            <br/>
                          <p>DUCAT is the ideal place where anyone aspirant to learn 3D Studio Max Certification !</p>
                          <a href="3dmaxtraining">Continue Reading</a>

                      </div>
                  </div>  
              </div>
              <div class="row">
               
                     <div class="col-md-4">
                      
                      <div class="eventBox" align="center">
                        
                          <img src="../images/3dmax.png" alt="Event" class="img-responsive">

                          
                          

                          <div class="clearfix"></div>

                          <h4>3D Studio Max Training
 Certification</h4>
                            <br/>
                          <p>DUCAT is the ideal place where anyone aspirant to learn 3D Studio Max Certification !</p>
                          <a href="3dmaxtraining">Continue Reading</a>

                      </div>
                  </div>  
                  <div class="col-md-4">
                      
                      <div class="eventBox" align="center">
                        
                          <img src="../images/3dmax.png" alt="Event" class="img-responsive">

                          
                          

                          <div class="clearfix"></div>

                          <h4>3D Studio Max Training
 Certification</h4>
                            <br/>
                          <p>DUCAT is the ideal place where anyone aspirant to learn 3D Studio Max Certification !</p>
                          <a href="3dmaxtraining">Continue Reading</a>

                      </div>
                  </div>  
                  <div class="col-md-4">
                      
                      <div class="eventBox" align="center">
                        
                          <img src="../images/3dmax.png" alt="Event" class="img-responsive">

                          
                          

                          <div class="clearfix"></div>

                          <h4>3D Studio Max Training
 Certification</h4>
                            <br/>
                          <p>DUCAT is the ideal place where anyone aspirant to learn 3D Studio Max Certification !</p>
                          <a href="3dmaxtraining">Continue Reading</a>

                      </div>
                  </div>  
              </div>
              <div class="row">
               
                     <div class="col-md-4">
                      
                      <div class="eventBox" align="center">
                        
                          <img src="../images/3dmax.png" alt="Event" class="img-responsive">

                          
                          

                          <div class="clearfix"></div>

                          <h4>3D Studio Max Training
 Certification</h4>
                            <br/>
                          <p>DUCAT is the ideal place where anyone aspirant to learn 3D Studio Max Certification !</p>
                          <a href="3dmaxtraining">Continue Reading</a>

                      </div>
                  </div>  
                  <div class="col-md-4">
                      
                      <div class="eventBox" align="center">
                        
                          <img src="../images/3dmax.png" alt="Event" class="img-responsive">

                          
                          

                          <div class="clearfix"></div>

                          <h4>3D Studio Max Training
 Certification</h4>
                            <br/>
                          <p>DUCAT is the ideal place where anyone aspirant to learn 3D Studio Max Certification !</p>
                          <a href="3dmaxtraining">Continue Reading</a>

                      </div>
                  </div>  
                  <div class="col-md-4">
                      
                      <div class="eventBox" align="center">
                        
                          <img src="../images/3dmax.png" alt="Event" class="img-responsive">

                          
                          

                          <div class="clearfix"></div>

                          <h4>3D Studio Max Training
 Certification</h4>
                            <br/>
                          <p>DUCAT is the ideal place where anyone aspirant to learn 3D Studio Max Certification !</p>
                          <a href="3dmaxtraining">Continue Reading</a>

                      </div>
                  </div>  
              </div><!--row--->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>