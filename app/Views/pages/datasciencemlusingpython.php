<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     DATA SCIENCE &amp; ML USING Python
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     DATA SCIENCE &amp; ML USING Python
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      DATA SCIENCE &amp; ML USING Python Training
     </h4>
     <p>
      This course is designed for both complete beginners with no programming experience or experienced developers looking to make the jump to Data Science!

Python is a general-purpose interpreted, interactive, object-oriented and high-level programming language.
Currently Python is the most popular Language in IT. Python adopted as a language of choice for almost all
the domain in IT including Web Development, Cloud Computing (AWS, OpenStack, VMware, Google Cloud,
etc.. ), Infrastructure Automations , Software Testing, Mobile Testing, Big Data and Hadoop, Data Science,
etc. This course to set you on a journey in python by playing with data, creating your own application, and
also testing the same.
     </p>
     <div class="contentAcc">
      <h2>
       An Introduction to Python
      </h2>
      <ul>
       <li>
        Brief History
       </li>
       <li>
        Why Python
       </li>
       <li>
        Where to use
       </li>
      </ul>
      <h2>
       Beginning Python Basics
      </h2>
      <ul>
       <li>
        The print statement
       </li>
       <li>
        Comments
       </li>
       <li>
        Python Data Structures &amp; Data Types
       </li>
       <li>
        String Operations in Python
       </li>
       <li>
        Simple Input &amp; Output
       </li>
       <li>
        Simple Output Formatting
       </li>
      </ul>
      <h2>
       Python Program Flow
      </h2>
      <ul>
       <li>
        Indentation
       </li>
       <li>
        The If statement and its' related statement
       </li>
       <li>
        An example with if and it's related statement
       </li>
       <li>
        The while loop
       </li>
       <li>
        The for loop
       </li>
       <li>
        The range statement
       </li>
       <li>
        Break &amp; Continue
       </li>
       <li>
        Assert
       </li>
       <li>
        Examples for looping
       </li>
      </ul>
      <h2>
       Functions &amp; Modules
      </h2>
      <ul>
       <li>
        Create your own functions
       </li>
       <li>
        Functions Parameters
       </li>
       <li>
        Variable Arguments
       </li>
       <li>
        Scope of a Function
       </li>
       <li>
        Function Documentation/Docstrings
       </li>
       <li>
        Lambda Functions &amp; map
       </li>
       <li>
        An Exercise with functions
       </li>
       <li>
        Create a Module
       </li>
       <li>
        Standard Modules
       </li>
      </ul>
      <h2>
       Exceptions
      </h2>
      <ul>
       <li>
        Errors
       </li>
       <li>
        Exception Handling with try
       </li>
       <li>
        Handling Multiple Exceptions
       </li>
       <li>
        Writing your own Exceptions
       </li>
      </ul>
      <h2>
       File Handling
      </h2>
      <ul>
       <li>
        File Handling Modes
       </li>
       <li>
        Reading Files
       </li>
       <li>
        Writing &amp; Appending to Files
       </li>
       <li>
        Handling File Exceptions
       </li>
       <li>
        The with statement
       </li>
      </ul>
      <h2>
       Classes In Python
      </h2>
      <ul>
       <li>
        New Style Classes
       </li>
       <li>
        Variable Type
       </li>
       <li>
        Static Variable in class
       </li>
       <li>
        Creating Classes
       </li>
       <li>
        Instance Methods
       </li>
       <li>
        Inheritance
       </li>
       <li>
        Polymorphism
       </li>
       <li>
        Encapsulation
       </li>
       <li>
        Scope and Visibility of Variables
       </li>
       <li>
        Exception Classes &amp; Custom Exceptions
       </li>
      </ul>
      <h2>
       Regular Expressions
      </h2>
      <ul>
       <li>
        Simple Character Matches
       </li>
       <li>
        Special Characters
       </li>
       <li>
        Character Classes
       </li>
       <li>
        Quantifiers
       </li>
       <li>
        The Dot Character
       </li>
       <li>
        Greedy Matches
       </li>
       <li>
        Grouping
       </li>
       <li>
        Matching at Beginning or End
       </li>
       <li>
        Match Objects
       </li>
       <li>
        Substituting
       </li>
       <li>
        Splitting a String
       </li>
       <li>
        Compiling Regular Expressions
       </li>
       <li>
        Flags
       </li>
      </ul>
      <h2>
       Data Structures
      </h2>
      <ul>
       <li>
        List Comprehensions
       </li>
       <li>
        Nested List Comprehensions
       </li>
       <li>
        Dictionary Comprehensions
       </li>
       <li>
        Functions
       </li>
       <li>
        Default Parameters
       </li>
       <li>
        Variable Arguments
       </li>
       <li>
        Specialized Sorts
       </li>
       <li>
        Iterators
       </li>
       <li>
        Generators
       </li>
       <li>
        The Functions any and all
       </li>
       <li>
        The with Statement
       </li>
       <li>
        Data Compression
       </li>
       <li>
        Closer
       </li>
       <li>
        Decorator
       </li>
      </ul>
      <h2>
       Writing GUIs in Python
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Components and Events
       </li>
       <li>
        An Example GUI
       </li>
       <li>
        The root Component
       </li>
       <li>
        Adding a Button
       </li>
       <li>
        Entry Widgets
       </li>
       <li>
        Text Widgets
       </li>
       <li>
        Checkbuttons
       </li>
       <li>
        Radiobuttons
       </li>
       <li>
        Listboxes
       </li>
       <li>
        Frames
       </li>
       <li>
        Menus
       </li>
       <li>
        Binding Events to Widgets
       </li>
      </ul>
      <h2>
       Thread In Python
      </h2>
      <ul>
       <li>
        Thread life Cycle
       </li>
       <li>
        Thread Definition
       </li>
       <li>
        Thread Implementation
       </li>
      </ul>
      <h2>
       Network Programming
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        A Daytime Server
       </li>
       <li>
        Clients and Servers
       </li>
       <li>
        The Client Program
       </li>
       <li>
        The Server Program
       </li>
       <li>
        Client and Server Architecture
       </li>
       <li>
        Threaded Server
       </li>
      </ul>
      <h2>
       Python &amp; MySQL Database Connection
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Installation
       </li>
       <li>
        DB Connection
       </li>
       <li>
        Creating DB Table
       </li>
       <li>
        Insert, Read,Update, Delete operations
       </li>
       <li>
        Commit &amp; Rollback operation
       </li>
       <li>
        Handling Errors
       </li>
       <li>
        Mini project on Python with TKinter
       </li>
      </ul>
      <h2>
       Data Science &amp; Machine Learning
      </h2>
      <h2>
       Course Overview
      </h2>
      <ul>
       <li>
        Overview of Data science
       </li>
       <li>
        What is Data Science
       </li>
       <li>
        Different Sectors Using Data Science
       </li>
      </ul>
      <h2>
       Mathematical Computing with python(NumPy)
      </h2>
      <ul>
       <li>
        Introduction to Numpy
       </li>
       <li>
        Activity-Sequence
       </li>
       <li>
        Creating and Printing an ndarray
       </li>
       <li>
        Class and Attributes of ndarray
       </li>
       <li>
        Basic Operations
       </li>
       <li>
        Activity � Slicing
       </li>
       <li>
        Copy and Views
       </li>
       <li>
        Mathematical Functions of Numpy
       </li>
       <li>
        Advance Slicing
       </li>
       <li>
        Transpose and arance
       </li>
       <li>
        Searching
       </li>
      </ul>
      <h2>
       Data Manipulation with Pandas
      </h2>
      <ul>
       <li>
        Introduction of Pandas
       </li>
       <li>
        Data Types in Pandas
       </li>
       <li>
        Understanding Series
       </li>
       <li>
        Understanding DataFrame
       </li>
       <li>
        View and Select Data Demo
       </li>
       <li>
        Missing Values
       </li>
       <li>
        Data Operations
       </li>
       <li>
        File Read and Write Support
       </li>
       <li>
        Pandas Sql Operation
       </li>
      </ul>
      <h2>
       Python for Data Visualization-Matplotlib
      </h2>
      <ul>
       <li>
        Introduction to Matplotlib
       </li>
       <li>
        Matplotlib Part 1 Set up
       </li>
       <li>
        Matplotlib Part 2 Plot
       </li>
       <li>
        Matplotlib Part 3 Next steps
       </li>
       <li>
        Matplotlib Exercises Overview
       </li>
       <li>
        Matplotlib Exercises � Solutions
       </li>
      </ul>
      <h2>
       Introduction to Machine Learning
      </h2>
      <ul>
       <li>
        Introduction to Machine Learning
       </li>
       <li>
        Machine Learning with Python
       </li>
      </ul>
      <h2>
       Linear Regression
      </h2>
      <ul>
       <li>
        Linear Regression Theory
       </li>
       <li>
        Model selection Updates for SciKit Learn
       </li>
       <li>
        Linear Regression with Python
       </li>
       <li>
        Linear Regression Project Overview and Project Solution
       </li>
      </ul>
      <h2>
       Logistic Regression
      </h2>
      <ul>
       <li>
        Logistic Regression Theory � Introduction
       </li>
       <li>
        Logistic Regression with Python � Part 1 � Logistics
       </li>
       <li>
        Logistic Regression with Python � Part 2 � Regression
       </li>
       <li>
        Logistic Regression with Python � Part 3 � Conclusion
       </li>
       <li>
        Logistic Regression Project Overview and Project Solutions
       </li>
      </ul>
      <h2>
       K Nearest Neighbours
      </h2>
      <ul>
       <li>
        KNN Theory
       </li>
       <li>
        KNN with Python
       </li>
       <li>
        KNN Project Overview and Project Solutions
       </li>
      </ul>
      <h2>
       Decision Trees and Random Forests
      </h2>
      <ul>
       <li>
        Introduction to Tree Methods
       </li>
       <li>
        Decision Trees and Random Forest with Python
       </li>
       <li>
        Decision Trees and Random Forest Project Overview
       </li>
       <li>
        Decision Trees and Random Forest Solutions Part 1
       </li>
       <li>
        Decision Trees and Random Forest Solutions Part 2
       </li>
      </ul>
      <h2>
       Support Vector Machines
      </h2>
      <ul>
       <li>
        SVM Theory
       </li>
       <li>
        Support Vector Machines with Python
       </li>
       <li>
        SVM Project Overview
       </li>
       <li>
        SVM Project Solutions
       </li>
      </ul>
      <h2>
       K Means Clustering
      </h2>
      <ul>
       <li>
        K Means Algorithm Theory
       </li>
       <li>
        K Means with Python
       </li>
       <li>
        K Means Project Overview
       </li>
       <li>
        K Means Project Solutions
       </li>
      </ul>
      <h2>
       Machine Learning Basics
      </h2>
      <ul>
       <li>
        Converting business problems to data problems
       </li>
       <li>
        Understanding supervised and unsupervised learning with examples
       </li>
       <li>
        Understanding biases associated with any machine learning algorithm
       </li>
       <li>
        Ways of reducing bias and increasing generalisation capabilites
       </li>
       <li>
        Drivers of machine learning algorithms
       </li>
       <li>
        Cost functions
       </li>
       <li>
        Brief introduction to gradient descent
       </li>
       <li>
        Importance of model validation
       </li>
       <li>
        Methods of model validation
       </li>
       <li>
        Cross validation &amp; average error
       </li>
      </ul>
      <h2>
       Generalised Linear Models in Python
      </h2>
      <ul>
       <li>
        Linear Regression
       </li>
       <li>
        Regularisation of Generalised Linear Models
       </li>
       <li>
        Ridge and Lasso Regression
       </li>
       <li>
        Logistic Regression
       </li>
       <li>
        Methods of threshold determination and performance measures for classification score models
       </li>
      </ul>
      <h2>
       Tree Models using Python
      </h2>
      <ul>
       <li>
        Introduction to decision trees
       </li>
       <li>
        Tuning tree size with cross validation
       </li>
       <li>
        Introduction to bagging algorithm
       </li>
       <li>
        Random Forests
       </li>
       <li>
        Grid search and randomized grid search
       </li>
       <li>
        ExtraTrees (Extremely Randomised Trees)
       </li>
       <li>
        Partial dependence plots
       </li>
      </ul>
      <h2>
       Boosting Algorithms using Python
      </h2>
      <ul>
       <li>
        Concept of weak learners
       </li>
       <li>
        Introduction to boosting algorithms
       </li>
       <li>
        Adaptive Boosting
       </li>
       <li>
        Extreme Gradient Boosting (XGBoost)
       </li>
      </ul>
      <h2>
       Support Vector Machines (SVM) &amp; kNN in Python
      </h2>
      <ul>
       <li>
        Introduction to idea of observation based learning
       </li>
       <li>
        Distances and similarities
       </li>
       <li>
        k Nearest Neighbours (kNN) for classification
       </li>
       <li>
        Brief mathematical background on SVM/li&gt;
       </li>
       <li>
        Regression with kNN &amp; SVM
       </li>
      </ul>
      <h2>
       Unsupervised learning in Python
      </h2>
      <ul>
       <li>
        Need for dimensionality reduction
       </li>
       <li>
        Principal Component Analysis (PCA)
       </li>
       <li>
        Difference between PCAs and Latent Factors
       </li>
       <li>
        Factor Analysis
       </li>
       <li>
        Hierarchical, K-means &amp; DBSCAN Clustering
       </li>
      </ul>
      <h2>
       Text Mining in Python
      </h2>
      <ul>
       <li>
        Gathering text data using web scraping with urllib
       </li>
       <li>
        Processing raw web data with BeautifulSoup
       </li>
       <li>
        Interacting with Google search using urllib with custom user agent
       </li>
       <li>
        Collecting twitter data with Twitter API
       </li>
       <li>
        Naive Bayes Algorithm
       </li>
       <li>
        Feature Engineering with text data
       </li>
       <li>
        Sentiment analysis
       </li>
      </ul>
      <h2>
       OpenCV
      </h2>
      <ul>
       <li>
        Basic of Computer Vision &amp; OpenCV
       </li>
       <li>
        Images Manipulations
       </li>
       <li>
        Image Segmentation
       </li>
       <li>
        Object Detection
       </li>
       <li>
        Face, People and Car Detection
       </li>
       <li>
        Face Analysis and Fulters
       </li>
       <li>
        Machine Learning in Computer Vision
       </li>
       <li>
        Motion Analysis &amp; Object Tracking
       </li>
       <li>
        Project on OpenCV
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="dsmlusingpython.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="22 + 0 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="22">
       <input name="url" type="hidden" value="/datasciencemlusingpython/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>