<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     PRIMAVERA PROJECT PLANNER
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     PRIMAVERA PROJECT PLANNER
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      People desiring to have training in Primavera Project Plannermust choose for DUCAT. DUCAT is the best in compliments to communicate training in any Primavera Project Planner programs. The teaching transmitted over here makes a being most suitable to have a permanent IT profession. Primavera Project Planner gives modern project managers and schedulers the one thing they worth most: control. It is the clear preference of professionals in target based businesses. P3 is the documented customary for high-end, high routine scheduling and supply control. From large, separate projects to highly sensitive, short period and serious projects allotment limited resources helps you administer them all. It supports concurrent secure access to project files by manifold users, which means up-to-the-minute information. It is pointless to say that the training scheduled by DUCAT is the best. Anyone aspiring to have training in Primavera Project Plannershould come to DUCAT. The teaching imparted makes anyone specialist in the field and theyexceed anyone to have a specialized job. The training gives in hand understanding of all the features of the process. Mock conditions are created so that one can guess their level of perceptive and experience. The teaching staffs at DUCAT are all the most knowledgeable in this field. They share all their knowledge and expertise during teaching to their students. So, after taking the education the students become experts themselves and can have the most excellent job in the market.
     </p>
     <div class="contentAcc">
      <h2>
       Primavera Project Planner
      </h2>
      <ul>
       <li>
        Introduction to Primavera P6 1 Hrs
       </li>
       <li>
        Introduction to Enterprise Project Structure 1 Hrs
       </li>
       <li>
        Types of Creating Enterprise Project Structure 2 Hrs
       </li>
       <li>
        Creating Organizational Breakdown Structure 2 Hrs
       </li>
       <li>
        Linking O.B.S to E.P.S 2 Hrs
       </li>
       <li>
        Creating a New Project 4 Hrs
       </li>
       <li>
        Introduction to Calendar 1 Hrs
       </li>
       <li>
        Creating a New Calendar (Global, Project, Resource) 2 Hrs
       </li>
       <li>
        Introduction to Work Breakdown Structure 1 Hrs
       </li>
       <li>
        Creating W.B.S 2 Hrs
       </li>
       <li>
        Creating new Activity 1 Hrs
       </li>
       <li>
        Activity ID, Description 1 Hrs
       </li>
       <li>
        Activity Relationship Types 1 Hrs
       </li>
       <li>
        Linking Relationship Between Activities 2 Hrs
       </li>
       <li>
        Currency (Create &amp; Assign) 1 Hrs
       </li>
       <li>
        Resources (Create &amp; Assign) 1 Hrs
       </li>
       <li>
        Roles (Create &amp; Assign) 1 Hrs
       </li>
       <li>
        Creating New Units for Measurement 2 Hrs
       </li>
       <li>
        Resources Curve 1 Hrs
       </li>
       <li>
        Different Types of Views 1 Hrs
       </li>
       <li>
        Activity Network 1 Hrs
       </li>
       <li>
        Admin Preferences 1 Hrs
       </li>
       <li>
        User Preferences 1 Hrs
       </li>
       <li>
        Grouping, Sorting &amp; Filtering 1 Hrs
       </li>
       <li>
        Bars 1 Hrs
       </li>
       <li>
        Timescale 1 Hrs
       </li>
       <li>
        Scheduling 1 Hrs
       </li>
       <li>
        Updating 1 Hrs
       </li>
       <li>
        Leveling 2 Hrs
       </li>
       <li>
        Codes (Activity, Project, Resources) 4 Hrs
       </li>
       <li>
        Working With Layouts 2 Hrs
       </li>
       <li>
        Working With Columns 2 Hrs
       </li>
       <li>
        Managing Baseline 4 Hrs
       </li>
       <li>
        Float 2 Hrs
       </li>
       <li>
        Reports 2 Hrs
       </li>
       <li>
        Import &amp; Export 2 Hrs
       </li>
       <li>
        Printing 1 Hrs
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>