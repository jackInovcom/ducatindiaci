<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     MIS Training in Noida
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     MIS
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      MIS Training
     </h4>
     <p>
      When companies need help in improving their current information system or creating a new one, they turn to the expertise of management information systems analysts, or computer systems analysts. These data professionals offer their services as either outside consultants or working in house.
     </p>
     <div class="contentAcc">
      <h2>
       Advanced Excel Course - Overview of the Basics of Excel
      </h2>
      <ul>
       <li>
        ustomizing common options in Excel
       </li>
       <li>
        Absolute and relative cells
       </li>
       <li>
        Protecting and un-protecting worksheets and cells
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Working with Functions
      </h2>
      <ul>
       <li>
        Writing conditional expressions (using IF)
       </li>
       <li>
        Using logical functions (AND, OR, NOT)
       </li>
       <li>
        Using lookup and reference functions (VLOOKUP, HLOOKUP, MATCH, INDEX)
       </li>
       <li>
        VlookUP with Exact Match, Approximate Match
       </li>
       <li>
        Nested VlookUP with Exact Match
       </li>
       <li>
        VlookUP with Tables, Dynamic Ranges
       </li>
       <li>
        Nested VlookUP with Exact Match
       </li>
       <li>
        Using VLookUP to consolidate Data from Multiple Sheets
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Data Validations
      </h2>
      <ul>
       <li>
        Specifying a valid range of values for a cell
       </li>
       <li>
        Specifying a list of valid values for a cell
       </li>
       <li>
        Specifying custom validations based on formula for a cell
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Working with Templates
      </h2>
      <ul>
       <li>
        Designing the structure of a template
       </li>
       <li>
        Using templates for standardization of worksheets
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Sorting and Filtering Data
      </h2>
      <ul>
       <li>
        Sorting tables
       </li>
       <li>
        Using multiple-level sorting
       </li>
       <li>
        Using custom sorting
       </li>
       <li>
        Filtering data for selected view (AutoFilter)
       </li>
       <li>
        Using advanced filter options
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Working with Reports
      </h2>
      <ul>
       <li>
        Creating subtotals
       </li>
       <li>
        Multiple-level subtotals
       </li>
       <li>
        Creating Pivot tables
       </li>
       <li>
        Formatting and customizing Pivot tables
       </li>
       <li>
        Using advanced options of Pivot tables
       </li>
       <li>
        Pivot charts
       </li>
       <li>
        consolidating data from multiple sheets and files using Pivot tables
       </li>
       <li>
        Using external data sources
       </li>
       <li>
        Using data consolidation feature to consolidate data
       </li>
       <li>
        Show Value As ( % of Row, % of Column, Running Total, Compare with Specific Field)
       </li>
       <li>
        Viewing Subtotal under Pivot
       </li>
       <li>
        Creating Slicers ( Version 2010 &amp; Above)
       </li>
      </ul>
      <h2>
       Advanced Excel Course - More Functions
      </h2>
      <ul>
       <li>
        Date and time functions
       </li>
       <li>
        Text functions
       </li>
       <li>
        Database functions
       </li>
       <li>
        Power Functions (CountIf, CountIFS, SumIF, SumIfS)
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Formatting
      </h2>
      <ul>
       <li>
        Using auto formatting option for worksheets
       </li>
       <li>
        Using conditional formatting option for rows, columns and cells
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Macros
      </h2>
      <ul>
       <li>
        Relative &amp; Absolute Macros?
       </li>
       <li>
        Editing Macro's
       </li>
      </ul>
      <h2>
       Advanced Excel Course - WhatIf Analysis
      </h2>
      <ul>
       <li>
        Goal Seek
       </li>
       <li>
        Data Tables
       </li>
       <li>
        Scenario Manager
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Charts
      </h2>
      <li>
       Using Charts
      </li>
      <li>
       Formatting Charts
      </li>
      <li>
       Using 3D Graphs)
      </li>
      <li>
       Using Bar and Line Chart together
      </li>
      <li>
       Using Secondary Axis in Graphs
      </li>
      <li>
       Sharing Charts with PowerPoint / MS Word, Dynamically
      </li>
      <li>
       (Data Modified in Excel, Chart would automatically get updated)
      </li>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>