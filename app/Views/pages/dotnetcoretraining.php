<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     .NET FOUR MONTHS
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     .NET FOUR MONTHS
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h2>
      MICROSOFT .NET 4.6 2015 TRAINING
     </h2>
     <h4>
      IT'S A NEEDS LITTLE EXTRA TO BE LITTLE DIFFERENT FROM OTHERS
     </h4>
     <p>
      DOT NET is the one of high in demand course today. There are many professionals already in the market quipped with this knowledge. But you can stand apart and above all of them by doing the course from an experienced and reputed .net training institute like DUCAT. DUCAT provides a little more , little differently to give that extra impetus to your CV. DUCAT has a training course named
      <h2>
       MICROSOFT .NET Training
      </h2>
      IN 4 MONTHS. This course would make you technically, practically and fundamentally h2 in this technology along with live project experience in 4months time. Learning at DUCAT is a very pleasant experience as the whole course is students get practical exposure to all the concepts, contents are well-structured to meet the industry requirements covering all the verticals and parallels in the technology, live project experience under the guidance of experts from the industries. The experienced faculties understand the varying understanding capacities of individuals and provide one to one attention to every student. DUCAT wants to ensure every student benefits out of the course. Last but not the least , a certification from DUCAT can give you a glittering edge over others.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction of Dot Net Framework
      </h2>
      <ul>
       <li>
        What is Dot Net?
       </li>
       <li>
        Why Dot Net?
       </li>
       <li>
        Advantages of Dot Net
       </li>
       <li>
        Component of Dot Net
       </li>
       <li>
        What is Framework?
       </li>
       <li>
        Versions of Framework
       </li>
       <li>
        CLR architecture
       </li>
       <li>
        CTS architecture
       </li>
       <li>
        CLS architecture
       </li>
       <li>
        DLR architectur
       </li>
       <li>
        What is JIT?
       </li>
       <li>
        Types of JIT?
       </li>
       <ul>
        <li>
         Pre-JIT COMPILER
        </li>
        <li>
         Econo-JIT COMPILER
        </li>
        <li>
         Normal-JIT COMPILER
        </li>
       </ul>
      </ul>
      <h2>
       Getting Started with Visual Studio
      </h2>
      <ul>
       <li>
        Understanding Project and Solution Explorer
       </li>
       <li>
        Creating a Project and Solution
       </li>
       <li>
        Understanding Solution Explorer
       </li>
       <li>
        Comments in C#
       </li>
       <li>
        Debug Project
       </li>
       <li>
        Understanding Execution Process
       </li>
      </ul>
      <h2>
       Assemblies in .Net
      </h2>
      <ul>
       <li>
        What is Assembly?
       </li>
       <li>
        Types of Assembly?
       </li>
       <li>
        How to make Private Assembly
       </li>
       <li>
        How to make Shared Assembly
       </li>
       <li>
        What is DLL HALL Problem?
       </li>
       <li>
        What is SN Key?
       </li>
       <li>
        What is GAC?
       </li>
       <li>
        How to Deploy Assembly in Cache
       </li>
       <li>
        What is MSIL?
       </li>
       <li>
        What is DLL &amp; EXE?
       </li>
       <li>
        How make DLL&amp; EXE.
       </li>
       <li>
        Use of Ilasm.exe (IL Assembler) &amp; Use of Ildasm.exe (IL Disassembler).
       </li>
      </ul>
      <h2>
       Object Oriented Programming System (OOPS)
      </h2>
      <ul>
       <li>
        What is OOPS?
       </li>
       <li>
        What is Class &amp; Object?
       </li>
       <li>
        Difference b/w Class, struct, interface and enum
       </li>
       <li>
        Types of Variable
       </li>
       <li>
        Static Variable
       </li>
       <li>
        Const Variable
       </li>
       <li>
        Read Only Variable
       </li>
       <li>
        Instance &amp; Static Members
       </li>
       <li>
        Inheritance &amp; Types of Inheritance
       </li>
       <li>
        What is Interface?
       </li>
       <li>
        Encapsulation
       </li>
       <li>
        Abstraction
       </li>
       <li>
        Polymorphism
       </li>
       <li>
        Types of Polymorphism
       </li>
       <ul>
        <li>
         Static Polymorphism
        </li>
        <li>
         Dynamic Polymorphism
        </li>
        <li>
         Concept of Method Overloading
        </li>
        <li>
         Concept of Method Overriding
        </li>
        <li>
         Concept of Early Binding
        </li>
        <li>
         Concept of Late Binding
        </li>
        <li>
         Concept of Up Casting
        </li>
       </ul>
       <li>
        Operator Overloading?
       </li>
       <li>
        What is Constructor?
       </li>
       <li>
        Types of Constructor
       </li>
       <ul>
        <li>
         Default Constructor
        </li>
        <li>
         Parameterized Constructor
        </li>
        <li>
         Copy Constructor
        </li>
        <li>
         Private Constructor
        </li>
        <li>
         Static Constructor
        </li>
        <li>
         Concept of This Keyword in Constructor
        </li>
       </ul>
       <li>
        Concept of Base Keyword in Constructor
       </li>
       <ul>
        <li>
         Concept of Base Keyword in Constructor
        </li>
        <li>
         Abstract Class &amp; Abstract Method
        </li>
        <li>
         Static Class &amp; Static Method
        </li>
        <li>
         Sealed Class &amp; Sealed Method
        </li>
        <li>
         Partial Class &amp; Partial Method
        </li>
       </ul>
       <li>
        Different types of Parameter
       </li>
       <ul>
        <li>
         Input Type
        </li>
        <li>
         Output Type
        </li>
        <li>
         Ref Type
        </li>
        <li>
         Params Type
        </li>
        <li>
         Dynamic Type
        </li>
       </ul>
      </ul>
      <h2>
       C# LANGUAGE FUNDAMENTALS
      </h2>
      <h2>
       Introductions of C#
      </h2>
      <ul>
       <li>
        What is C#?
       </li>
       <li>
        Namespaces in C#?
       </li>
       <li>
        Create namespace in C#
       </li>
       <li>
        Data types in C#
       </li>
       <li>
        Value type
       </li>
       <li>
        Reference type
       </li>
       <li>
        Null able type
       </li>
       <li>
        Heap
       </li>
       <li>
        Stack
       </li>
       <li>
        Looping in C#
       </li>
       <ul>
        <li>
         For,While,Dowhile
        </li>
        <li>
         Foreach
        </li>
       </ul>
       <li>
        Conditional statement
       </li>
       <ul>
        <li>
         Switch case
        </li>
        <li>
         If, if-else, Nested if
        </li>
        <li>
         GoTo
        </li>
        <li>
         Break
        </li>
        <li>
         Continue
        </li>
       </ul>
       <li>
        Convertion Types
       </li>
       <ul>
        <li>
         Implicit {Boxing}
        </li>
        <li>
         Explicit {UnBoxing}
        </li>
       </ul>
      </ul>
      <h2>
       Array
      </h2>
      <ul>
       <li>
        What is Array?
       </li>
       <li>
        Types of Array
       </li>
       <ul>
        <li>
         Simple Array
        </li>
        <li>
         Multi Array
        </li>
        <li>
         Jagged Array
        </li>
       </ul>
      </ul>
      <h2>
       File Handling
      </h2>
      <ul>
       <li>
        Introductions of File Handing
       </li>
       <li>
        Working with Files and Directories
       </li>
       <li>
        Working with Directories
       </li>
       <li>
        Working with Files Using the File and FileInfo Classes
       </li>
       <li>
        Working with Directories Using the Directories and Directories Info Classes
       </li>
      </ul>
      <h2>
       Exception Handling
      </h2>
      <ul>
       <li>
        Introductions of Exception Handling
       </li>
       <li>
        Role of Exception Handling
       </li>
       <li>
        What is Error, bug and exceptions?
       </li>
       <li>
        Try, catch &amp; Finally block
       </li>
       <li>
        System Level &amp; Application Level Exception
       </li>
       <li>
        Handel Multiple Exception
       </li>
       <li>
        Throw Exception
       </li>
      </ul>
      <h2>
       Software Engineering Models and Concept (Project Based)
      </h2>
      <ul>
       <li>
        What is Software Development Life Cycle?
       </li>
       <li>
        Phases of a SDLC
       </li>
       <li>
        Project initiation and planning
       </li>
       <li>
        Feasibility study
       </li>
       <li>
        System design
       </li>
       <li>
        Coding
       </li>
       <li>
        Testing
       </li>
       <li>
        Implementation
       </li>
       <li>
        Maintenance
       </li>
      </ul>
      <h2>
       Collections
      </h2>
      <ul>
       <li>
        Introductions of Collections
       </li>
       <li>
        Types of collections
       </li>
       <li>
        Generic
       </li>
       <ul>
        <li>
         List
        </li>
        <li>
         Dictionary
        </li>
        <li>
         Stack
        </li>
        <li>
         Queue
        </li>
       </ul>
       <li>
        Non Generic
       </li>
       <ul>
        <li>
         ArrayList
        </li>
        <li>
         HashTable
        </li>
        <li>
         Stack
        </li>
        <li>
         Queue
        </li>
       </ul>
      </ul>
      <h2>
       Delegate
      </h2>
      <ul>
       <li>
        Introductions of Delegate
       </li>
       <li>
        Types of delegate
       </li>
       <li>
        SingleCast
       </li>
       <li>
        MultiCast
       </li>
       <li>
        Generic delegate
       </li>
      </ul>
      <h2>
       Events
      </h2>
      <ul>
       <li>
        Introductions of Events
       </li>
       <li>
        Understanding Events and Event Handler
       </li>
       <li>
        Types of Event
       </li>
       <li>
        System Define Event
       </li>
       <li>
        User Define Event
       </li>
       <li>
        Event Life Cycle
       </li>
       <li>
        Binding Events
       </li>
       <li>
        Working with Event Handler
       </li>
      </ul>
      <h2>
       Reflection
      </h2>
      <ul>
       <li>
        Introductions of Reflection
       </li>
       <li>
        Reflection Namespace
       </li>
       <li>
        Reflection Classes
       </li>
       <li>
        Access Assembly through Reflection
       </li>
       <li>
        Member Info
       </li>
       <li>
        Method Info
       </li>
       <li>
        Property Info
       </li>
       <li>
        Constructor Info
       </li>
       <li>
        Browsing Members
       </li>
       <li>
        Runtime Information
       </li>
      </ul>
      <h2>
       Threading
      </h2>
      <ul>
       <li>
        Introductions of Threading
       </li>
       <li>
        Threading Namespace
       </li>
       <li>
        Understanding Thread LifeCycle
       </li>
       <li>
        Default Threading
       </li>
       <li>
        Multi Threading
       </li>
       <li>
        Priority in threading Member Info
       </li>
      </ul>
      <h2>
       Introductions of Properties
      </h2>
      <ul>
       <li>
        set
       </li>
       <li>
        get
       </li>
      </ul>
      <h2>
       Indexer
      </h2>
      <ul>
       <li>
        Define Indexer
       </li>
       <li>
        Advantages of Indexer
       </li>
      </ul>
      <h2>
       Business Requirement Specification (Project Based)
      </h2>
      <ul>
       <li>
        Objective of Project
       </li>
       <li>
        DFD
       </li>
       <li>
        UML Diagram
       </li>
       <li>
        Class Diagram
       </li>
       <li>
        ER Diagram
       </li>
      </ul>
      <h2>
       The SQL Server Management
      </h2>
      <ul>
       <li>
        What is SQL?
       </li>
       <li>
        What is Database?
       </li>
       <li>
        Create Statement
       </li>
       <li>
        Create database
       </li>
       <li>
        Create table
       </li>
       <li>
        Use Identity
       </li>
       <li>
        Select Statement
       </li>
       <li>
        Insert statement
       </li>
       <li>
        Update statement
       </li>
       <li>
        Delete statement
       </li>
       <li>
        Drop statement
       </li>
       <li>
        Truncate statement
       </li>
       <li>
        Difference between delete, drop &amp; truncate
       </li>
       <li>
        Alter Query
       </li>
       <li>
        Clauses
       </li>
       <li>
        Group By
       </li>
       <li>
        Order By
       </li>
       <li>
        Basic Queries
       </li>
       <li>
        Stored Procedure
       </li>
       <li>
        Use Input and Output Type parameter in Stored Procedure
       </li>
       <li>
        Function
       </li>
       <li>
        Types of Functions
       </li>
       <li>
        Trigger
       </li>
       <li>
        Cursor
       </li>
       <li>
        Union &amp; Union All
       </li>
       <li>
        Transaction
       </li>
       <li>
        Joins
       </li>
       <li>
        Indexes
       </li>
       <li>
        Views
       </li>
       <li>
        Constraints
       </li>
       <li>
        Tracing sql query with SqlProfiler
       </li>
      </ul>
      <h2>
       System Requirement Database Structure (Project Based)
      </h2>
      <ul>
       <li>
        Database Table Structure
       </li>
       <li>
        Database Design
       </li>
       <li>
        DDL
       </li>
       <li>
        DML
       </li>
       <li>
        Stored Procedure
       </li>
       <li>
        Database Security
       </li>
      </ul>
      <h2>
       ADO .NET 10
      </h2>
      <ul>
       <li>
        What is ADO.NET?
       </li>
       <li>
        Difference between ADO &amp; ADO.NET?
       </li>
       <li>
        Architecture of ADO.NET
       </li>
       <li>
        Disconnected mode Architecture
       </li>
       <ul>
        <li>
         Using SqlDataAdapter Class
        </li>
        <li>
         Using DataSet Class
        </li>
        <li>
         SqlCommandBuilder Class
        </li>
       </ul>
       <li>
        Connected Mode Architecture
       </li>
       <ul>
        <li>
         Using SqlConnection Class
        </li>
        <li>
         Using SqlCommand Class
        </li>
        <li>
         Using SqlDataReader Class
        </li>
        <li>
         Understanding ExecuteNonQuery method
        </li>
        <li>
         Understanding ExecuteScalar method
        </li>
        <li>
         Understanding ExecuteReader method
        </li>
       </ul>
       <li>
        Working with DataTable class
       </li>
       <li>
        Creating PrimaryKey Column on VirtualTable
       </li>
       <li>
        Using Constraints Class
       </li>
       <ul>
        <li>
         ForeignKeyConstraints Class
        </li>
        <li>
         UniqueConstraints Class
        </li>
       </ul>
       <li>
        Store table in datatable
       </li>
       <li>
        Apply Insert,update,delete operations on VirtualTable
       </li>
       <li>
        Show, next,first,last,all by datatable
       </li>
       <li>
        Executing Stored Procedure
       </li>
       <li>
        Executing Function
       </li>
       <li>
        What is Sql Injection
       </li>
       <li>
        Applying SqlInjection
       </li>
      </ul>
      <h2>
       ASP.NET 4.6 Framework
      </h2>
      <ul>
       <li>
        Asp.Net Introduction
       </li>
       <ul>
        <li>
         Evolution of Web Development
        </li>
        <li>
         ASP .NET different versions
        </li>
        <li>
         Understanding protocol and domain
        </li>
        <li>
         Understanding static &amp; dynamic page
        </li>
        <li>
         Server-side programming
        </li>
        <li>
         Client-side programming
        </li>
        <li>
         HTML and HTML Forms
        </li>
        <li>
         Creating a simple html page
        </li>
       </ul>
       <li>
        Developing ASP.NET Applications
       </li>
       <ul>
        <li>
         Creating Websites
        </li>
        <li>
         Creating a New Web Application
        </li>
        <li>
         Page Life Cycle
        </li>
        <li>
         Understanding Events of Page Life Cycle
        </li>
        <li>
         Websites and Web Projects
        </li>
        <li>
         Adding Web Forms
        </li>
        <li>
         Migrating a Website from a Previous Version
        </li>
        <li>
         Difference b/w inline and code behind concept
        </li>
        <li>
         Asp.Net special directory
        </li>
        <li>
         What is Application Directories
        </li>
        <li>
         Different types of Application Directories
        </li>
        <li>
         Understanding application label and page label variable
        </li>
        <li>
         Need of Global class
        </li>
        <li>
         What is Configuration file?
        </li>
        <li>
         Need of Configuration file
        </li>
        <li>
         Difference b/t app.config &amp; web.con file
        </li>
        <li>
         Nested Configuration
        </li>
       </ul>
       <li>
        State Management
       </li>
       <ul>
        <li>
         Need of state management technique
        </li>
        <li>
         Types of state management
        </li>
        <li>
         Understanding Server side and Client side state management
        </li>
        <li>
         ViewState
        </li>
        <li>
         HiddenField
        </li>
        <li>
         Cookies
        </li>
        <li>
         Cache
        </li>
        <li>
         QueryString
        </li>
        <li>
         Session
        </li>
        <li>
         Session Tracking
        </li>
        <li>
         Session Configuration
        </li>
        <li>
         Session mode
        </li>
        <li>
         Cookieless
        </li>
        <li>
         Session Timeout
        </li>
        <li>
         Session Mode
        </li>
        <li>
         Application
        </li>
        <li>
         Transferring Information between Pages
        </li>
        <li>
         Transferring Information between Cross Pages
        </li>
       </ul>
       <li>
        Web Controls in ASP.Net
       </li>
       <ul>
        <li>
         Types of web controls
        </li>
        <li>
         Server Controls
        </li>
        <li>
         Standard Controls
        </li>
        <li>
         Data Controls
        </li>
        <li>
         Validation Controls
        </li>
        <li>
         Navigation Controls
        </li>
        <li>
         Login Controls
        </li>
        <li>
         Ajax Extensions
        </li>
        <li>
         Ajax ToolKit
        </li>
       </ul>
       <li>
        Html Controls
       </li>
       <li>
        User Controls
       </li>
       <li>
        Data Controls
       </li>
       <ul>
        <li>
         DataList
        </li>
        <li>
         DetailsView
        </li>
        <li>
         FormsView
        </li>
        <li>
         GridView
        </li>
        <li>
         ListView
        </li>
        <li>
         Repeater
        </li>
        <li>
         DataGrid
        </li>
        <li>
         EntityDataSource
        </li>
        <li>
         Object DataSource
        </li>
        <li>
         LinqDataSource
        </li>
        <li>
         XmlDataSource
        </li>
       </ul>
       <li>
        Working with Templates of Data Controls
       </li>
       <li>
        Working with events of Data Controls
       </li>
       <li>
        CSS
       </li>
       <ul>
        <li>
         Understanding CSS
        </li>
        <li>
         Types of CSS
        </li>
        <li>
         Inline CSS
        </li>
        <li>
         Internal CSS
        </li>
        <li>
         External CSS
        </li>
        <li>
         Selectors in CSS
        </li>
       </ul>
       <li>
        Themes &amp; Skins
       </li>
       <li>
        Working with Image gallery
       </li>
       <li>
        Working with ImageSlide
       </li>
       <li>
        Master Page
       </li>
       <li>
        Nested Master Page
       </li>
       <li>
        Page Tracing
       </li>
       <li>
        Mail &amp; Messaging
       </li>
       <li>
        Caching Techniques
       </li>
       <ul>
        <li>
         Understanding Caching
        </li>
        <li>
         Types of Caching
        </li>
        <li>
         Output Caching
        </li>
        <li>
         Data Caching
        </li>
       </ul>
       <li>
        Web Security
       </li>
       <ul>
        <li>
         Need of security
        </li>
        <li>
         Types of security
        </li>
        <li>
         Forms Authentication
        </li>
        <li>
         Windows Authentication
        </li>
        <li>
         Confidentiality with SSL
        </li>
        <li>
         Password Format
        </li>
        <li>
         Types of password format
        </li>
        <li>
         MD5
        </li>
        <li>
         SHA1
        </li>
        <li>
         Clear
        </li>
       </ul>
       <li>
        Performance tuning
       </li>
       <li>
        Ajax
       </li>
       <li>
        Understanding Gmail,Facebook and Twitter API's
       </li>
       <li>
        Deploying ASP.NET Applications
       </li>
       <ul>
        <li>
         ASP.NET Applications and the Web Server
        </li>
        <li>
         How Web Servers Work
        </li>
        <li>
         Understanding Virtual Directory
        </li>
        <li>
         Web Application URLs
        </li>
        <li>
         Web Farms
        </li>
       </ul>
       <li>
        Internet Information Services (IIS)
       </li>
       <ul>
        <li>
         Installing IIS
        </li>
        <li>
         Registering the ASP.NET File Mappings
        </li>
        <li>
         Verifying That ASP.NET Is Correctly Installed
        </li>
       </ul>
       <li>
        Managing Websites with IIS Manager
       </li>
       <ul>
        <li>
         Creating a Virtual Directory
        </li>
        <li>
         Configuring a Virtual Directory
        </li>
       </ul>
       <li>
        Deploying a Simple Site
       </li>
       <ul>
        <li>
         Web Applications and Components
        </li>
        <li>
         Other Configuration Steps
        </li>
        <li>
         Code Compilation
        </li>
        <li>
         Verifying That ASP.NET Is Correctly Installed
        </li>
       </ul>
       <li>
        Deploying a Visual Studio IDE
       </li>
       <ul>
        <li>
         Creating a Virtual Directory for a New Project
        </li>
        <li>
         Copying a Website
        </li>
        <li>
         Publishing a Website
        </li>
        <li>
         Verifying That ASP.NET Is Correctly Installed
        </li>
       </ul>
      </ul>
      <h2>
       Technical Design &amp; Development (Project Based)
      </h2>
      <ul>
       <li>
        Working with Project
       </li>
       <li>
        Programming Language: C# (Asp.Net)
       </li>
       <li>
        Designing Tools
       </li>
       <li>
        HTML
       </li>
       <li>
        Using CSS
       </li>
       <li>
        Using Ajax
       </li>
       <li>
        Using JavaScript
       </li>
      </ul>
      <h2>
       ADVANCE .NET MVC
      </h2>
      <h2>
       Introduction to LINQ
      </h2>
      <ul>
       <li>
        What is linq?
       </li>
       <li>
        Limitation of linq
       </li>
       <li>
        Sorting Data
       </li>
       <li>
        Filtering Data
       </li>
       <li>
        Grouping Data
       </li>
       <li>
        Joining Data
       </li>
       <li>
        Set Operations
       </li>
       <li>
        Datacontext in linq
       </li>
       <li>
        Select data by linq
       </li>
       <li>
        Perform dml by linq
       </li>
       <li>
        Stored procedure by linq?
       </li>
       <li>
        Linq with dataset
       </li>
       <li>
        Linq query expression
       </li>
       <li>
        Linq with generic &amp; non generic collection
       </li>
      </ul>
      <h2>
       Entity Framework 6.0
      </h2>
      <ul>
       <li>
        Overview of the Entity Framework
       </li>
       <li>
        Difference between Linq to sql and Entity Framework
       </li>
       <li>
        Learn how to create an Entity Data Model
       </li>
       <li>
        Conceptual schema definition languages (CSDL)
       </li>
       <li>
        Store schema definition languages (SSDL)
       </li>
       <li>
        Mapping specification language (MSL)
       </li>
       <li>
        Techniques for using LINQ to Entities to query data in SQL Server database
       </li>
       <li>
        Creating/Managing 1-Many and many-1 relations
       </li>
       <li>
        Associations &amp; Foreign Keys
       </li>
       <li>
        Procedure Mapping
       </li>
       <li>
        Mapping object to Data
       </li>
      </ul>
      <h2>
       Introduction To WCF
      </h2>
      <ul>
       <li>
        What is Distributed Application
       </li>
       <li>
        Evolution of WCF
       </li>
       <li>
        Evolution of Service Oriented Architecture (SOA)
       </li>
       <li>
        Four Tenets of SOA
       </li>
       <li>
        What is WCF.
       </li>
       <li>
        Where does WCF Services fit in?
       </li>
       <li>
        WCF comparison with WebServices and
       </li>
       <li>
        .Net Remoting.
       </li>
      </ul>
      <h2>
       Endpoints in WCF
      </h2>
      <ul>
       <li>
        What is Endpoint in WCF
       </li>
       <li>
        End points in Configuration File.
       </li>
       <li>
        ABC - Address, Binding &amp; Contract of the service in Configuration File.
       </li>
       <li>
        Understanding importance of base address.
       </li>
       <li>
        Importance of IMetadataExchange
       </li>
       <li>
        Contract / Endpoint.
       </li>
       <li>
        IMetadataExchange Contract/Endpoint for Http and tcp.
       </li>
       <li>
        Configuring service behavior in configuration file
       </li>
       <li>
        WCF Service Configuration Editor
       </li>
       <li>
        Creating Endpoints through Code
       </li>
      </ul>
      <h2>
       Developing WCF Service Application
      </h2>
      <ul>
       <li>
        Hosting WCF Service in IIS/ASP .NET Development Server
       </li>
       <li>
        Hosting WCF service in Managed Application(Self Hosting).
       </li>
       <li>
        Use of WCF Service Library Project.
       </li>
       <li>
        Use of Wcf Service with Entity Framework
       </li>
      </ul>
      <h2>
       Understand Contracts in WCF service
      </h2>
      <ul>
       <li>
        ServiceContract.
       </li>
       <li>
        OperationContract.
       </li>
       <li>
        DataContract
       </li>
       <li>
        Fault Contract
       </li>
       <li>
        Message Contract
       </li>
      </ul>
      <h2>
       Understand Instancing behavior in WCF service
      </h2>
      <ul>
       <li>
        Single
       </li>
       <li>
        PerCall
       </li>
       <li>
        PerSession
       </li>
      </ul>
      <h2>
       MVC 5 (Model View Controller) Getting Started
      </h2>
      <ul>
       <li>
        What is MVC
       </li>
       <li>
        Life cycle of MVC
       </li>
       <li>
        The MVC Pattern
       </li>
       <li>
        The Road to MVC
       </li>
      </ul>
      <h2>
       Models
      </h2>
      <ul>
       <li>
        Scaffolding
       </li>
       <li>
        What Is Scaffolding
       </li>
       <li>
        Scaffolding and the Entity Framework
       </li>
       <li>
        Executing the Scaffolding Template
       </li>
       <li>
        Executing the Scaffolding Code
       </li>
       <li>
        Editing
       </li>
       <li>
        Building a Resource to Edit
       </li>
       <li>
        Responding to the Edit POST Request
       </li>
       <li>
        Model Binding
       </li>
       <li>
        The DefaultModelBinder
       </li>
       <li>
        Explicit Model Binding
       </li>
      </ul>
      <h2>
       Views
      </h2>
      <ul>
       <li>
        The Purpose of Views
       </li>
       <li>
        View Basics
       </li>
       <li>
        Understanding View Conventions
       </li>
       <li>
        h2ly Typed Views
       </li>
       <li>
        How View Bag Falls Short
       </li>
       <li>
        Understanding View Bag, View Data, and TempData
       </li>
       <li>
        View Models
       </li>
       <li>
        Adding a View
       </li>
       <li>
        The Razor View Engine
       </li>
       <li>
        What Is Razor
       </li>
       <li>
        HTML Encoding
       </li>
       <li>
        Code Blocks
       </li>
       <li>
        Razor Syntax Samples
       </li>
       <li>
        Layouts
       </li>
       <li>
        View Start
       </li>
       <li>
        Specifying a Partial View
       </li>
      </ul>
      <h2>
       Forms &amp; Html Helpers
      </h2>
      <ul>
       <li>
        Using Forms
       </li>
       <li>
        The Action and the Method
       </li>
       <li>
        To GET or to POST?
       </li>
       <li>
        HTML Helpers
       </li>
       <li>
        Automatic Encoding
       </li>
       <li>
        Making Helpers Do Your Bidding
       </li>
       <li>
        Inside HTML Helpers
       </li>
       <li>
        Adding Inputs
       </li>
       <li>
        Helpers, Models, and View Data
       </li>
       <li>
        h2ly Typed Helpers
       </li>
       <li>
        Helpers and Model Metadata
       </li>
       <li>
        Templated Helpers
       </li>
       <li>
        Helpers and ModelState
       </li>
       <li>
        Other Input Helpers
       </li>
       <li>
        Html.Hidden
       </li>
       <li>
        Html.Password
       </li>
       <li>
        Html.RadioButton
       </li>
       <li>
        Html.CheckBox
       </li>
       <li>
        Rendering Helpers
       </li>
       <li>
        Html.ActionLink and Html.RouteLink
       </li>
       <li>
        URL Helpers
       </li>
       <li>
        Html.Partial and Html.RenderPartial
       </li>
       <li>
        Html.Action and Html.RenderAction
       </li>
      </ul>
      <h2>
       Data Annotations &amp; Validations
      </h2>
      <ul>
       <li>
        Annotating Orders for Validation
       </li>
       <li>
        Using Validation Annotations
       </li>
       <li>
        Controller Actions and Validation Errors
       </li>
       <li>
        ValidatableObject
       </li>
       <li>
        Display and Edit Annotations
       </li>
       <li>
        Display
       </li>
       <li>
        Scaffold Column
       </li>
       <li>
        Display Format
       </li>
      </ul>
      <h2>
       Controllers
      </h2>
      <ul>
       <li>
        Controllers, Actions, and Action Results
       </li>
       <li>
        Routing to Controller Actions
       </li>
       <li>
        Working with the Application Model
       </li>
      </ul>
      <h2>
       Routing
      </h2>
      <ul>
       <li>
        Introduction to Routing
       </li>
       <li>
        Comparing Routing to URL Rewriting
       </li>
       <li>
        Routing Approaches
       </li>
       <li>
        Defining Attribute Routes
       </li>
       <li>
        Defining Traditional Routes
       </li>
       <li>
        Choosing Attribute Routes or Traditional Routes
       </li>
       <li>
        Named Routes
       </li>
       <li>
        MVC Areas
       </li>
       <li>
        Catch-All Parameter
       </li>
       <li>
        Multiple Route Parameters in a Segment
       </li>
       <li>
        StopRoutingHandler and IgnoreRoute
       </li>
       <li>
        Debugging Routes
       </li>
       <li>
        Inside Routing: How Routes Generate URLs
       </li>
      </ul>
      <h2>
       Bootstrap Introduction
      </h2>
      <ul>
       <li>
        Responsive app,layouts
       </li>
       <li>
        Components
       </li>
       <li>
        Developing site with bootstrap
       </li>
      </ul>
      <h2>
       Interview Preparation :
      </h2>
      <ul>
       <li>
        Technical Interview Preparation
       </li>
       <li>
        Mock interview Preparation
       </li>
       <li>
        HR Session
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="dotnet4months.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="86 + 84 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="170">
       <input name="url" type="hidden" value="/dotnetcoretraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>