<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     CCNP
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     CCNP
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Are you ready to become a "journeyman" with a CCNP certification? We at DUCAT focus on providing the best line of training in routing and switching of scalable networks through campuses and intranet. CISCO created the CCNP certification to recognize advanced skills in computer networking and especially in the field of support and installation of LAN / WAN infrastructures. It also equips candidates to work on complex networking systems or work independently on solutions. To obtain a CCNP certificate, students must qualify two of four exams that include switching, routing, network support and remote access. In total, the exam costs about $500. One must also have an active CCNA certificate to be eligible to apply for CCNP certification. We at DUCAT provide students with all the materials in the form of both books and practice exams to ensure hands on experience with the CISCO platform. Our lectures will imbibe the best practices within students such that they not only clear the exams but are ready to handle the responsibilities that come with the degree. Graduating from our course opens up a highly rewarding job market.
     </p>
     <div class="contentAcc">
      <h2>
       CISCO CERTIFIED NETWORK PROFESSIONAL
      </h2>
      <ul>
       <li>
        Determine network resources needed for implementing EIGRP on a network
       </li>
       <li>
        Create an EIGRP implementation plan
       </li>
       <li>
        Create an EIGRP verification plan
       </li>
       <li>
        Configure EIGRP routing
       </li>
       <li>
        Verify EIGRP solution was implemented properly using show and debug commands
       </li>
       <li>
        Document result of EIGRP implement and verification
       </li>
      </ul>
      <h2>
       Implement a multi area OSPF Network, given a network design and a set of requirement
      </h2>
      <ul>
       <li>
        Determine network resources needed for implementing OSPF on a network
       </li>
       <li>
        Create an OSPF implementation plan
       </li>
       <li>
        Create an OSPF verification plan
       </li>
       <li>
        Configure OSPF routing
       </li>
       <li>
        Verify OSPF solution was implemented properly using show and debug commands
       </li>
       <li>
        Document result of OSPF implement and verification
       </li>
      </ul>
      <h2>
       Implement a multi area eBGP solution, given a network design and a set of requirement
      </h2>
      <ul>
       <li>
        Determine network resources needed for implementing eBGP on a network
       </li>
       <li>
        Create an eBGP implementation plan
       </li>
       <li>
        Create an eBGP verification plan
       </li>
       <li>
        Configure eBGP routing
       </li>
       <li>
        Verify eBGP solution was implemented properly using show and debug commands
       </li>
       <li>
        Document result of eBGP implement and verification
       </li>
      </ul>
      <h2>
       Implement an IPv6 solution, given a network design and a set of requirement
      </h2>
      <ul>
       <li>
        Determine network resources needed for implementing IPv6 on a network
       </li>
       <li>
        Create an IPv6 implementation plan
       </li>
       <li>
        Create an IPv6 verification plan
       </li>
       <li>
        Configure IPv6 routing
       </li>
       <li>
        Verify IPv6 solution was implemented properly using show and debug commands
       </li>
       <li>
        Document result of IPv6 implement and verification
       </li>
      </ul>
      <h2>
       Implement an IPv4 or IPv6 based redistribution solution, given a network design and a set of requirement
      </h2>
      <ul>
       <li>
        Create a redistribution implementation plan based upon the result of redistribution analysis
       </li>
       <li>
        Create a redistribution verification plan
       </li>
       <li>
        Configure a redistribution solution
       </li>
       <li>
        Verify the redistribution solution was implemented and verification plan
       </li>
       <li>
        Document result of redistribution implement and verification
       </li>
       <li>
        Identify the difference between implementing IPv4 and IPv6 redistribution solution
       </li>
      </ul>
      <h2>
       Implement layer 3 Path control solution
      </h2>
      <ul>
       <li>
        Create a layer 3 path control implementation plan based upon the result of redistribution analysis
       </li>
       <li>
        Create a layer 3 path control verification plan
       </li>
       <li>
        Configure a layer 3 path control
       </li>
       <li>
        Verify that layer 3 path control was implemented and verification plan
       </li>
       <li>
        Document result of layer 3 path control implement and verification
       </li>
       <li>
        Implement basic teleworker and branch services
       </li>
       <li>
        Describe broadband technologies
       </li>
       <li>
        Configure basic broadband connection
       </li>
       <li>
        Describe basic VPN technologies
       </li>
       <li>
        Configure GRE
       </li>
       <li>
        Configure branch access technologies
       </li>
      </ul>
      <h2>
       300-115 SWITCH Implement a VLAN based solution, given a networking design and a set of requirements
      </h2>
      <ul>
       <li>
        Determine network resources needed for implementing VLAN based solution on a network
       </li>
       <li>
        Create an VLAN based implementation plan
       </li>
       <li>
        Create an VLAN based verification plan
       </li>
       <li>
        Configure switch-to-switch connectivity for VLAN based solution
       </li>
       <li>
        Configure loop prevention for the VLAN based solution
       </li>
       <li>
        Configure access port for the VLAN Based solution
       </li>
       <li>
        Verify VLAN based solution was implemented properly using show and debug commands
       </li>
       <li>
        Document result of VLAN implement and verification
       </li>
      </ul>
      <h2>
       Implement a security extension of a layer 2 extension, given a networking design and a set of requirements
      </h2>
      <ul>
       <li>
        Determine network resources needed for implementing a security solution
       </li>
       <li>
        Create a implementation plan for security solution
       </li>
       <li>
        Create a verification plan for the security solution
       </li>
       <li>
        Configure port security features
       </li>
       <li>
        Configure general switch security features
       </li>
       <li>
        Configure private VLAN configure VACL, PACL
       </li>
       <li>
        Verify security based solution was implemented properly using show and debug commands
       </li>
       <li>
        Document results of security implement and verification
       </li>
      </ul>
      <h2>
       Implement a switch based layer 3 services, given a networking design and a set of requirements
      </h2>
      <ul>
       <li>
        Determine network resources needed for implementing a switch based layer 3 solution
       </li>
       <li>
        Create a implementation plan for the switch based layer 3 solution
       </li>
       <li>
        Create a verification plan for the switch based layer 3 solution
       </li>
       <li>
        Configure routing interfaces layer 3 security
       </li>
       <li>
        Verify the switch based layer 3solution was implemented properly using show and debug commands
       </li>
       <li>
        Document results of switch based layer 3 implement and verification
       </li>
      </ul>
      <h2>
       Prepare infrastructure to support advanced services
      </h2>
      <ul>
       <li>
        Implement wireless extension of a layer 2 solution
       </li>
       <li>
        Implement VoIP support solution
       </li>
       <li>
        Implement Video support solution
       </li>
      </ul>
      <h2>
       Implement High availability, given a networking design and a set of requirements
      </h2>
      <ul>
       <li>
        Determine network resources needed for implementing High availability on a network
       </li>
       <li>
        Create an High availability implementation plan
       </li>
       <li>
        Create an High availability verification plan
       </li>
       <li>
        Implementing first hop redundancy protocol
       </li>
       <li>
        Implementing switch supervisor redundancy
       </li>
       <li>
        Verify High availability solution was implemented properly using show and debug commands
       </li>
       <li>
        Document result of High availability implement and verification
       </li>
      </ul>
      <h2>
       300-135 TSHOOT Maintain and monitor network performances Develop a plan to maintain and monitor
      </h2>
      <ul>
       <li>
        Perform network monitoring using IOS tools
       </li>
       <li>
        Perform routine IOS device Maintenance
       </li>
       <li>
        Isolate sub-optimal internetwork operation at the correctly defied IOS layer
       </li>
      </ul>
      <h2>
       Troubleshoot multi �protocol system network
      </h2>
      <ul>
       <li>
        Troubleshoot EIGRP
       </li>
       <li>
        Troubleshoot OSPF
       </li>
       <li>
        Troubleshoot eBGP
       </li>
       <li>
        Troubleshoot routing distribution solution
       </li>
       <li>
        Troubleshoot a DHCP client and server solution
       </li>
       <li>
        Troubleshoot NAT
       </li>
       <li>
        Troubleshoot first hop redundancy protocol
       </li>
       <li>
        Troubleshoot IPv6 routing
       </li>
       <li>
        Troubleshoot IPv4  IPv6 interoperability
       </li>
       <li>
        Troubleshoot switch-to-switch connectivity for VLAN based solution
       </li>
       <li>
        Troubleshoot loop prevention for the VLAN based solution
       </li>
       <li>
        Troubleshoot access port for the VLAN Based solution
       </li>
       <li>
        Troubleshoot private VLAN
       </li>
       <li>
        Troubleshoot port security
       </li>
       <li>
        Troubleshoot general switch security
       </li>
       <li>
        Troubleshoot VACL  PACL
       </li>
       <li>
        Troubleshoot switch virtual interfaces (SVI)
       </li>
       <li>
        Troubleshoot switch supervisor redundancy
       </li>
       <li>
        Troubleshoot switch support advance services (wireless, VoIP, videos��)
       </li>
       <li>
        Troubleshoot VoIP support solution
       </li>
       <li>
        Troubleshoot Video support solution
       </li>
       <li>
        Troubleshoot layer 3 security
       </li>
       <li>
        Troubleshoot issue related to ACLs used to secure access to Cisco router
       </li>
       <li>
        Troubleshoot configuration to issues related to accessing the AAA server for authentication purposes
       </li>
       <li>
        Troubleshoot security issues related to IOS services (i.e. finger,NTP,HTTP,FTP,RCP,etc)
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="ccnptraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="41 + 59 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="100">
       <input name="url" type="hidden" value="/ccnptraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>