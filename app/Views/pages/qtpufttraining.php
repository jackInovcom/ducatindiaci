<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     Best QTP/UFT Training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     RPA
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Are You Looking For QTP-UFT Training??
     </p>
     <p>
      You are at the Right Place
     </p>
     <p>
      Start From Sctrach and become a certified Professional in QTP-UFT . QTP was formely known as Hp Quicktest Professional . Its a Regression test automation for Software applications.
These training will clear all concepts and gives you advanced level technical knowledge in  QTP-UFT.
  
Come And Join the Program and Upgrade your skills and Make Productive for your career.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction to Automation Testing
      </h2>
      <ul>
       <li>
        What is Automation Testing?
       </li>
       <li>
        When Automation is needed
       </li>
       <li>
        When Automation is not needed
       </li>
       <li>
        Advantages of Automation Testing
       </li>
       <li>
        Disadvantages of Automation Testing
       </li>
       <li>
        What are the popular Automation Tools in the industry?
       </li>
       <li>
        What is the difference between various Automation Testing Tools?
       </li>
      </ul>
      <h2>
       Introduction to QTP
      </h2>
      <ul>
       <li>
        QTP IDE,Basic Components in QTP,Addin Manager
       </li>
       <li>
        How does QTP works/Object recognition concept
       </li>
       <li>
        Record a sample Test
       </li>
       <li>
        Understand the Script
       </li>
       <li>
        Execution of a Test
       </li>
       <li>
        Enhancement of recorded scrip
       </li>
      </ul>
      <h2>
       Framework in QTP
      </h2>
      <ul>
       <li>
        What is Framework?
       </li>
       <li>
        Types of Framework
       </li>
       <li>
        Linear Scripting
       </li>
       <li>
        Structured Scripting
       </li>
       <li>
        Data Driven
       </li>
       <li>
        Keyword Driven
       </li>
       <li>
        Modular Driven
       </li>
       <li>
        Hybrid
       </li>
      </ul>
      <h2>
       Object Repository
      </h2>
      <ul>
       <li>
        QTP Classes and Objects
       </li>
       <li>
        Details of OR
       </li>
       <li>
        Types of OR
       </li>
       <li>
        How to create OR
       </li>
       <li>
        Test Object vs. Run time Object
       </li>
       <li>
        Configuring Object identification
       </li>
       <li>
        Object Spy
       </li>
       <li>
        Object Properties
       </li>
       <li>
        Logical Name
       </li>
       <li>
        Mandatory/Assistive properties /Ordinal identifier
       </li>
       <li>
        Smart Identification
       </li>
       <li>
        Compare and Merge options
       </li>
      </ul>
      <h2>
       Recording Modes
      </h2>
      <ul>
       <li>
        What is Recording Modes?
       </li>
       <li>
        Types of Recording Modes
       </li>
       <li>
        Normal Recording Mode
       </li>
       <li>
        Analog Recording Mode
       </li>
       <li>
        Low Level Recording Mode
       </li>
      </ul>
      <h2>
       Checkpoints
      </h2>
      <ul>
       <li>
        What is Checkpoint?
       </li>
       <li>
        Why Checkpoint is needed
       </li>
       <li>
        Types of Checkpoint
       </li>
       <li>
        Different ways of Inserting Checkpoints
       </li>
       <li>
        Parameterization
       </li>
      </ul>
      <h2>
       What is Parameterization?
      </h2>
      <ul>
       <li>
        Why Parameterization is needed
       </li>
       <li>
        Types of Parameterization
       </li>
       <li>
        Datatable, Random Number and Environment Number Parameterization
       </li>
       <li>
        How to access data from Global Sheet and Local sheet
       </li>
      </ul>
      <h2>
       Actions
      </h2>
      <ul>
       <li>
        What is Action?
       </li>
       <li>
        Types of Actions
       </li>
       <li>
        Methods to import an Action
       </li>
       <li>
        Call to New
       </li>
       <li>
        Call to Existing
       </li>
       <li>
        Call to Copy
       </li>
      </ul>
      <h2>
       Regular Expression
      </h2>
      <ul>
       <li>
        Regular Expression
       </li>
       <li>
        When to Use Regular Expression
       </li>
       <li>
        How to use Regular Expression in Descriptive Programming
       </li>
      </ul>
      <h2>
       Recovery Scenarios
      </h2>
      <ul>
       <li>
        Handling the exception using Recovery Scenario Manager
       </li>
       <li>
        Usage of Recovery Scenario Wizard
       </li>
       <li>
        Completing a Recovery Scenario
       </li>
       <li>
        Creation and Association of .QRS file for RecoveryScenario
       </li>
      </ul>
      <h2>
       Step Generator
      </h2>
      <ul>
       <li>
        What is Step Generator?
       </li>
       <li>
        How to generatescript using Step Generator
       </li>
       <li>
        Advantages &amp; Disadvantages of using Step Generator
       </li>
      </ul>
      <h2>
       Virtual Object
      </h2>
      <ul>
       <li>
        What is Virtual Object?
       </li>
       <li>
        When Virtual Object is used
       </li>
       <li>
        Limitations of Virtual Object.
       </li>
      </ul>
      <h2>
       Debugging
      </h2>
      <ul>
       <li>
        When Debugging is used
       </li>
       <li>
        Step Into
       </li>
       <li>
        Step Out
       </li>
       <li>
        Step Over.
       </li>
      </ul>
      <h2>
       Descriptive Programming
      </h2>
      <ul>
       <li>
        What is Descriptive Programming?
       </li>
       <li>
        Types of Descriptive Programming
       </li>
       <li>
        Working with DP Object
       </li>
       <li>
        Working with Object Collection
       </li>
      </ul>
      <h2>
       Basics of VB Scripting
      </h2>
      <ul>
       <li>
        Data Types, Variables, Constant
       </li>
       <li>
        Operators-Arithmetic,comparison,Concatenation,Logical
       </li>
       <li>
        Conditional constructsIf Then Else and Select Case
       </li>
       <li>
        Looping Construct Do,While,For Next, For Each Next
       </li>
       <li>
        Arrays and Functions
       </li>
       <li>
        Important Inbuilt functions
       </li>
      </ul>
      <h2>
       Interview Preparation :
      </h2>
      <ul>
       <li>
        Technical Interview Preparation
       </li>
       <li>
        Mock interview Preparation
       </li>
       <li>
        HR Session
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="qtfuft.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="99 + 32 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="131">
       <input name="url" type="hidden" value="/qtpufttraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>