<?php echo view("includes/header.php"); ?>
<section id="breadCrumb">

    <div class="container">

        <div class="row">

            <div class="col-md-6">

                <h3>FARIDABAD BATCHES</h3>

            </div>
            <!-- End Of Col MD 6 -->

            <div class="col-md-6 text-right">

                <a href="/">Home</a> / <a href="#">FARIDABAD BATCHES</a>

            </div>
            <!-- End Of Col MD 6 -->

        </div>
        <!-- End Of Row -->

    </div>
    <!-- End OF Container -->

</section>
<!-- End Of Bread Crumb -->

<section id="mainArea">

    <div class="container">

        <div class="row">

            <div class="col-md-9 text-center">

                <h3 style="text-align:left">FARIDABAD BATCHES AT DUCAT</h3>
                <div class="separator short">
                    <div class="separator_line"></div>
                </div>

                <br/>
                <div style="text-align:left">

                    <table width="80%" cellpadding="1" border="2" align="center">
                        <tr>
                            <th scope="row">Technology</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Register</th>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="col-md-3">

                <form class="searchForm">
                    <input type="text" placeholder="Search">
                </form>

                <div class="widgetArea">
                    <h5>CONTACT INFO</h5>

                    <address>
                            <span class="address">A - 43 &amp; A - 52 Sector - 16, <br>Noida (U.P) (Near McDonalds)</span>
                            <br/>
                            <span class="phone"><strong>Phone:</strong> 0120-4646464, +91- 9871055180</span>
                            <br/>
                            <span class="email"><strong>E-Mail:</strong> <a href="mailto:info@ducatindia.com">info@ducatindia.com</a></span>
                            <br/>
                            <span class="web"><strong>Web:</strong> <a href="http://www.ducatindia.com/">http://www.ducatindia.com/</a></span>
                          </address>
                </div>

            </div>
            <!-- End Of Col MD 3 -->

        </div>
        <!-- End Of Row -->

    </div>
    <!-- End OF Container -->

</section>

<section id="offices" class="text-center">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h5>CORPORATE OFFICE NOIDA: <span>0120 - 4646464</span></h5>
                <p>GR.NOIDA: <span>0120-4345190</span> GHAZIABAD: <span>0120-4835400</span> FARIDABAD: <span>0129-4150605</span> GURGAON: <span>0124-4219095</span> JAIPUR: <span>0141-2550077</span></p>

            </div>
            <!-- End Of Col MD 12 -->

        </div>
        <!-- End Of Row -->

    </div>
    <!-- End OF Container -->

</section>
<?php echo view("includes/footer.php"); ?>