<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     Appium Training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Appium
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      APPIUM is a freely distributed open source mobile application UI Testing framework. Appium allows native, hybrid and web application testing and supports automation test on physical devices as well as an emulator or simulator both. It offers cross-platform application testing, i.e. single API works for both Android and iOS platform test scripts.
     </p>
     <p>
      It has NO dependency on Mobile device OS. Because APPIUM has framework or wrapper that translate Selenium Webdriver commands into UIAutomation (iOS) or UIAutomator (Android) commands depending on the device type, not any OS type.
Appium supports all languages that have Selenium client libraries like- Java, Objective-C, JavaScript with node.js, PHP, Ruby, Python, C#, etc
     </p>
     <div class="contentAcc">
      <h2>
       Appium Introduction
      </h2>
      <ul>
       <li>
        Appium Feature.
       </li>
       <li>
        Appium Internal Architecture.
       </li>
      </ul>
      <h2>
       Appium Installation on window for Android Automation
      </h2>
      <ul>
       <li>
        Installing Android Studio and java.
       </li>
       <li>
        Configuration System variable for both android SDK.
       </li>
       <li>
        Configuring virtual mobile devices for running test.
       </li>
       <li>
        Downloding Node.js and install via npm.
       </li>
       <li>
        Importance Information on appium version.
       </li>
       <li>
        configuring appium java client jars for frontend execution.
       </li>
       <li>
        Step by Step Installation.
       </li>
      </ul>
      <h2>
       Appium First Program
      </h2>
      <ul>
       <li>
        Android application download for practice.
       </li>
       <li>
        What are Desired Capablities? How to talk to Appium Server.
       </li>
       <li>
        Invoking Android Driver � Creating base program.
       </li>
       <li>
        Execution of Appium first program on mobile Native Application.
       </li>
       <li>
        UIAutomator tool usage � Inspection of elements..
       </li>
      </ul>
      <h2>
       Native ApplicationAutomation:
      </h2>
      <ul>
       <li>
        Automation application UI using xpath and text attribute.
       </li>
       <li>
        Appium API's for UI Interaction with ID's and Class Name.
       </li>
       <li>
        Handling Multiple similar object of Application with index.
       </li>
       <li>
        AndroidUI Automator usage in Indentifying object of application.
       </li>
       <li>
        Touch action in Appium.
       </li>
       <li>
        Tapping and longpress event.
       </li>
       <li>
        Touch action sequence execution.
       </li>
       <li>
        Strategy for Automating swipping event .
       </li>
      </ul>
      <h2>
       Advance Gesture Automation with Key Event Handling
      </h2>
      <ul>
       <li>
        Demo on Swiping the clock .
       </li>
       <li>
        How to Automate Scrolling?
       </li>
       <li>
        Drag and Drop on native Application with Appium.
       </li>
       <li>
        Automating Android Key event with Appium.
       </li>
       <li>
        Miscelleanous key event handing with Appium.
       </li>
       <li>
        Invoking Application with package activity.
       </li>
       <li>
        Example on package name and activity invoking application.
       </li>
       <li>
        Automation on real device.
       </li>
      </ul>
      <h2>
       Web Application Automation
      </h2>
      <ul>
       <li>
        Configuring real device to system for running Appium Test.
       </li>
       <li>
        Real Devices � Native Application automation.
       </li>
       <li>
        Desired Capablitiesfor real devices execution.
       </li>
       <li>
        Important thing to know when execution on physical / emulator devices.
       </li>
       <li>
        Mobile Browser configuration setup for Appium Test.
       </li>
       <li>
        Automating the chrome mobile browser.
       </li>
      </ul>
      <h2>
       Hybrid Application Automation
      </h2>
      <ul>
       <li>
        Hybrid application feature and ways to test .
       </li>
       <li>
        Views switching mechanism.
       </li>
       <li>
        Example on Hybrid Application Handling.
       </li>
      </ul>
      <h2>
       Appium Framework � Part 1 (TestNG)
      </h2>
      <ul>
       <li>
        TestNG Installation and setup in eclipse .
       </li>
       <li>
        TestNG basic annotation role in desiging.
       </li>
       <li>
        TestNG Annotation in depth.
       </li>
       <ul>
        <li>
         Importance of TestNG prerequestise annotaion.
        </li>
        <li>
         Importance of testng.xml file in framework.
        </li>
        <li>
         Including and Excluding the test case from execution with TestNG.xml file.
        </li>
       </ul>
      </ul>
      <h2>
       Appium Framework � Part 2 (Maven)
      </h2>
      <ul>
       <li>
        Introduction to Maven.
       </li>
       <li>
        Configurationto Maven.
       </li>
       <li>
        Create basics Maven Project.
       </li>
       <li>
        Importance of Maven POM.xml file.
       </li>
       <li>
        Creating Appium � Maven Project .
       </li>
       <li>
        Impoting Appium Maven dependencies.
       </li>
       <li>
        Running Appium Script in Maven.
       </li>
      </ul>
      <h2>
       TestNG with Maven � Part 3
      </h2>
      <ul>
       <li>
        Creating TestNG XML File for Appium Maven Project.
       </li>
       <li>
        Creating Multiple Test Suite and Configuring them in xml files.
       </li>
       <li>
        Running Appium Server Programatically.
       </li>
       <li>
        Creating .bat file and command to trigger appium server
       </li>
       <li>
        Appium Server start code.
       </li>
       <li>
        Creating Multiple TestNG profile and trigerring from Maven.
       </li>
       <li>
        Running End to End Framework on single click with Maven.
       </li>
      </ul>
      <h2>
       Scheduling in Jenkins CI Tool � Part 4
      </h2>
      <ul>
       <li>
        Why Jenkins? Jenkins Advantages.
       </li>
       <li>
        Installing &amp; Configuring Jenkins Appium Project.
       </li>
       <li>
        Deploying Framework in Jenkins and Trigger the build.
       </li>
       <li>
        Schedulingthe framework to run at any time with enkins.
       </li>
      </ul>
      <h2>
       PageObject Pattern &amp; Page Factory
      </h2>
      <ul>
       <li>
        What is PageObject Model?
       </li>
       <li>
        Page Factory annotation @FindBy.
       </li>
       <li>
        Practical Example explaning Page Factory Model-1.
       </li>
       <li>
        Practical Example explaning Page Factory Model-2
       </li>
      </ul>
      <h2>
       DataDriven from Excel for feeding data into Appium Test Case.
      </h2>
      <ul>
       <li>
        What is Apavchi POI &amp; Download?
       </li>
       <li>
        Excel API method Explanation.
       </li>
       <li>
        Program for retriveing data from Excel.
       </li>
       <li>
        DataDriven Testing using POI and TestNG.
       </li>
      </ul>
      <h2>
       Logging Framework � Log4j
      </h2>
      <ul>
       <li>
        Log4j Explanation Theoritical part.
       </li>
       <li>
        Log4j Practical.
       </li>
       <li>
        Log4j Properties files.
       </li>
      </ul>
      <h2>
       Database Connection to Selelenium / Appium Test Case
      </h2>
      <ul>
       <li>
        Step to Connect Database to Selenium Test Case.
       </li>
       <ul>
        <li>
         My SQL download Instruction.
        </li>
        <li>
         MySQL database connection procedure.
        </li>
        <li>
         Creating Datavbase in My SQL Server.
        </li>
        <li>
         Creating Table in Database.
        </li>
        <li>
         JDBC and ODBC Connection Overview.
        </li>
        <li>
         Integrating of database with JDBC API.
        </li>
        <li>
         Steps top connect database info to Selenium.
        </li>
       </ul>
      </ul>
      <h2>
       Interview Question and Prepartion for placement
      </h2>
      <ul>
       <li>
        Technical Interview Preparation
       </li>
       <li>
        Mock Interview preparation
       </li>
       <li>
        HR Session
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="Appium.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="78 + 76 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="154">
       <input name="url" type="hidden" value="/appiumtraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>