<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Workshop At Colleges
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="">
     Home
    </a>
    /
    <a href="">
     Workshop At Colleges
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-12 detailsStyle">
    <h3>
     ABOUT WORKSHOPS:
    </h3>
    <p>
     DUCAT is specialized in delivering workshops on different technologies(voluntarily- these workshops are completly free of cost). These workshop can be considered as a co-curricular activity that enriches the knowledge base of student in an addition to what they learn at the college.The workshops are delivered by the eminent speakers from the industry who share the real time experiences with the student,hence enlightening them as per industry norms.These workshops not only help the students to know about a particular technology but also help them to apt for an appropriate career(as per their aptitude)in future when they face the job market.
    </p>
    <h3>
     OBJECTIVE OF THE WORKSHOPS
    </h3>
    <p>
     With the market getting more competitive,the industry is looking for professionals who are not only qualified but also skilled.The candidates equipped with skills apart from a professional degree are more readily absorbed by the industry.Though the student today are aware of this fact,but Ducat with its "SKILL DEVELOPMENT Workshops" is trying to emphasize on:
    </p>
    <ul>
     <li>
      Current developments in various technologies.
     </li>
     <li>
      Career aspects related to these technologies.
     </li>
    </ul>
    <p>
     DUCAT specializes in delivering workshop on below mentioned technologies:
    </p>
    <ul>
     <li>
      JAVA(core/ADVANCE/SPRING/HIBERNATE)
     </li>
     <li>
      MICROSOFT-.NET-2010/SILVERLIGHT
     </li>
     <li>
      PHP
     </li>
     <li>
      SOFTWARE TESTING
     </li>
     <li>
      ORACLE developer &amp; DBA
     </li>
     <li>
      AUTOCAD/PRO-E/CATIA/Solid Works/REVIT ARCHITECTURE/3DS MAX/Stad-Pro
     </li>
     <li>
      MCSE/MCITP/CCNA/CCNP
     </li>
     <li>
      RADHAT LINUX
     </li>
     <li>
      ANDROID
     </li>
     <li>
      CLOUD COMPUTING
     </li>
     <li>
      EMBEDDED SYSTEM
     </li>
     <li>
      VLSI
     </li>
     <li>
      ROBOTICS
     </li>
     <li>
      MATLAB
     </li>
     <li>
      PLC Scada
     </li>
     <li>
      DATA WAREHOUSING
     </li>
     <li>
      ORACLE APPLICATION
     </li>
     <li>
      ERP
     </li>
     <li>
      SAS
     </li>
     <li>
      SE-PD(SPOKEN ENGLISH &amp;PERSONALITY DEVELOPMENT)
     </li>
    </ul>
    <h3>
     SKILL DEVELOPMENT WORKSHOPS BY DUCAT
    </h3>
    <h4>
     ABOUT THE CONSULTANTS:
    </h4>
    <p>
     The consultants at Ducat are industry professionals,who share their live industry experience with the students.We have very skilled professional trainers and project mangers who are the key aspects of the workshops.They impart training,and share their experiences with the candidates/students.
    </p>
    <h4>
     WHO ALL CAN ATTEND THESE WORKSHOPS?
    </h4>
    <p>
     Thought the workshops are benifical for all the student but are most suitable for trhe third and fourth year students of all stream,who mare seeking placements.
    </p>
    <h3>
     ROLES &amp; RESPONSIBILITIES:
    </h3>
    <h4>
     DUCAT RESPONSIBILITIES:
    </h4>
    <ul>
     <li>
      Ducat would be responsible for arranging .the skilled industry consultants,to import the workshop.
     </li>
     <li>
      The the participation certificats will be issued to the students attending the workshop,by Ducat.
     </li>
     <li>
      the handouts and notes related to the workshop will be provided by Ducat.
     </li>
    </ul>
    <h4>
     COLLEGE RESPONSIBILITES:
    </h4>
    <ul>
     <li>
      The workshop infrastructure- seminar hall &amp; lab has to be made available by the college.
     </li>
     <li>
      The attendaqnce sheet of the partcipants has to be provided by the college.
     </li>
     <li>
      The college would be responsible for the participation of a minimum of 35 students in the workshop.
     </li>
    </ul>
    <h4>
     OTHER FEATURE AND BENEFITS:
    </h4>
    <ul>
     <li>
      These workshop are completely free of cost.
     </li>
     <li>
      The duration of the workshop depends on the requirement of the college or the students.
     </li>
     <li>
      The technology on which the workshop has to be conducted can be chosen by the college or the students.
     </li>
     <li>
      more than one technology can be chosen for the workshop.
     </li>
    </ul>
    <p>
     we look forward to a positive response and anticipate getting an opportunity to make this session possible.
    </p>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
 <section id="b2brelation">
  <div class="container">
   <div class="row">
    <div class="col-md-6 text-center">
     <h2>
      UPCOMING EVENTS
     </h2>
     <div class="separator short">
      <div class="separator_line">
      </div>
     </div>
     <div class="row">
      <!-- End Of Event Box -->
     </div>
     <!-- End Of Row -->
    </div>
    <!-- End Of Col MD 6 -->
    <div class="col-md-6 text-center">
     <h2>
      ACCOMPLISHED EVENTS
     </h2>
     <div class="separator short">
      <div class="separator_line">
      </div>
     </div>
     <div class="row">
      <div class="eventBox col-md-6">
       <img alt="Event" class="img-responsive" src="../images/event.jpg"/>
       <div class="logoBox">
        <img src="../images/logo-square.jpg"/>
       </div>
       <h6>
        By Ducat India
       </h6>
       <div class="clearfix">
       </div>
       <h4>
        Workshop@ Graphic Era
       </h4>
       <h5>
        2016-05-06
       </h5>
       <p>
        Matlab
       </p>
       <a href="#">
        Continue Reading
       </a>
      </div>
      <!-- End Of Event Box -->
      <!-- End Of Event Box -->
     </div>
     <!-- End Of Row -->
    </div>
    <!-- End Of Col MD 6 -->
   </div>
   <!-- End Of Row -->
  </div>
  <!-- End OF Container -->
 </section>
</section>
<section id="b2brelation">
 <div class="container">
  <div class="row">
   <div class="col-md-6 text-center">
    <h2>
     UPCOMING EVENTS
    </h2>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
    <div class="row">
     <!-- End Of Event Box -->
    </div>
    <!-- End Of Row -->
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-center">
    <h2>
     ACCOMPLISHED EVENTS
    </h2>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
    <div class="row">
     <div class="eventBox col-md-6">
      <img alt="Event" class="img-responsive" src="../images/event.jpg"/>
      <div class="logoBox">
       <img src="../images/logo-square.jpg"/>
      </div>
      <h6>
       By Ducat India
      </h6>
      <div class="clearfix">
      </div>
      <h4>
       Workshop@ Graphic Era
      </h4>
      <h5>
       2016-05-06
      </h5>
      <p>
       Matlab
      </p>
      <a href="#">
       Continue Reading
      </a>
     </div>
     <!-- End Of Event Box -->
     <!-- End Of Event Box -->
    </div>
    <!-- End Of Row -->
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>