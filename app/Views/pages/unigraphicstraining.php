<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     UNIGRAPHICS
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     UNIGRAPHICS
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      DUCAT is a teaching institute which is most appropriate for transmitting technological training. People wishing to have technical training on UNIGRAPHICS must go to this organization to have the training. UNIGRAPHICS is anexclusive CAD/CAM system, obtainable for student use at USC. The system is authoritative, but it also is big and multifaceted. The following explanation are meant as a roadmap to make easy learning the essentials of the system, so as to be able to model substance in it, and to generate files. The training that is given is on the different aspects of UNIGRAPHICS. It trains to effectual make the trainees learn the virtualization aspect by generating replica situation. The trainee can understand the benefit and drawback of this knowledge only when they feel it which happens during the teaching at DUCAT. Students learn how to fit the process and to run it proficiently. The supervision teaches all the features of UNIGRAPHICS in such level that the trainees become well familiar with them all. The training is implemented by facility members that are the finest. They themselves are specialist in the field. They impart all their proficiency to their trainees. The dummy scenarios are made so actual that the trainees get an immediate experience of what actually needs to be done as an administrator. The training imparted makes the learner get good jobs in presumed companies.
     </p>
     <div class="contentAcc">
      <h2>
       Unigraphics
      </h2>
      <ul>
       <li>
        Introduction to CAD /CAM
       </li>
       <li>
        Basic approach to Solid Modeling , Operating Systems , UNIX Commands
       </li>
       <li>
        Preference Sketch , Modeling preferences
       </li>
       <li>
        Layers Layer Settings , visible in view , Layer Category, Move to Layer , Copy to Layer Sub Functions point , Vector ,Plane Sub functions
       </li>
       <li>
        Sketch create Sketch , Basic Courves ,Point , Associative Point, Rectangle , Fillet , Ellipse Spline , General Conic , Edit Curves, Constraints , Mirror Sketch , Alternate Solutions , Drag, Show /Remove Constraints
       </li>
       <li>
        Animate , Add object to sketch , Add Extracted Curve to Sketch , Offset Extracted Curve ,Edit Defining String ,Convert To/From Reference , Position , Reattach ,Activate ,Deactivate ,Retain Dimension Display , Update Model Form feature
       </li>
      </ul>
      <h2>
       Reference Feature
      </h2>
      <ul>
       <li>
        Datum Plan
       </li>
       <li>
        Axis CSYS
       </li>
       <li>
        Swept Features
       </li>
       <li>
        Extrudec Body
       </li>
       <li>
        Revolved Body
       </li>
       <li>
        Sweep along Guide
       </li>
       <li>
        Tube
       </li>
      </ul>
      <h2>
       Special Features
      </h2>
      <ul>
       <li>
        Hole
       </li>
       <li>
        Boss
       </li>
       <li>
        Pocket
       </li>
       <li>
        Pad
       </li>
       <li>
        Slot
       </li>
       <li>
        Groove
       </li>
      </ul>
      <h2>
       Library Manager
      </h2>
      <ul>
       <li>
        User Defined Feature
       </li>
      </ul>
      <h2>
       Associative Features
      </h2>
      <ul>
       <li>
        Extract
       </li>
       <li>
        Sheet from Curves
       </li>
       <li>
        Bounded Plane
       </li>
       <li>
        Thicken Sheet
       </li>
       <li>
        Sheets to Solid Assistant
       </li>
      </ul>
      <h2>
       Primitive
      </h2>
      <ul>
       <li>
        Cylinder
       </li>
       <li>
        Cone
       </li>
       <li>
        Sphere
       </li>
      </ul>
      <h2>
       Feature Operation
      </h2>
      <ul>
       <li>
        Taper
       </li>
       <li>
        Edge
       </li>
       <li>
        Face
       </li>
       <li>
        Soft Blend
       </li>
       <li>
        Chamfer
       </li>
       <li>
        Hollow
       </li>
       <li>
        Thread
       </li>
       <li>
        Instance
       </li>
       <li>
        Saw
       </li>
       <li>
        Patch Simplify Body
       </li>
       <li>
        Wrap Geometry
       </li>
       <li>
        Offset
       </li>
       <li>
        Scale
       </li>
       <li>
        Trim
       </li>
       <li>
        Split Body
       </li>
      </ul>
      <h2>
       Boolean Operations
      </h2>
      <ul>
       <li>
        Create
       </li>
       <li>
        Unite
       </li>
       <li>
        Subtract
       </li>
       <li>
        Intersect
       </li>
       <li>
        Edit Feature &amp; Model Navigator
       </li>
       <li>
        Paramters
       </li>
       <li>
        Positioning
       </li>
       <li>
        Delete
       </li>
       <li>
        Replace
       </li>
       <li>
        Suppress
       </li>
       <li>
        Unsuppress
       </li>
       <li>
        Suppress by expression
       </li>
       <li>
        Remove Parameter
       </li>
       <li>
        Solid Density
       </li>
       <li>
        Play Back
       </li>
       <li>
        Resize fixed Datums
       </li>
       <li>
        Display Dimension
       </li>
       <li>
        Make current feature
       </li>
       <li>
        Parent curves
       </li>
       <li>
        Rename
       </li>
       <li>
        Macro
       </li>
       <li>
        Customize
       </li>
      </ul>
      <h2>
       Analysis
      </h2>
      <ul>
       <li>
        Distance
       </li>
       <li>
        Angle
       </li>
      </ul>
      <h2>
       Advance Modeling (Leve 2)
      </h2>
      <ul>
       <li>
        Infrotuction to CAD /Cam
       </li>
       <li>
        Basic approach SurfaceModeling
       </li>
      </ul>
      <h2>
       Wcs
      </h2>
      <ul>
       <li>
        Orgine
       </li>
       <li>
        Dynamics
       </li>
       <li>
        Rotale
       </li>
       <li>
        Change xc
       </li>
       <li>
        Y c Direction
       </li>
       <li>
        Display
       </li>
       <li>
        Save.
       </li>
      </ul>
      <h2>
       Current Operation
      </h2>
      <ul>
       <li>
        Offset
       </li>
       <li>
        Bridge
       </li>
       <li>
        Simplify
       </li>
       <li>
        Join
       </li>
       <li>
        Project
       </li>
       <li>
        Combined Projection
       </li>
       <li>
        Intersect
       </li>
       <li>
        Section
       </li>
       <li>
        Extract
       </li>
       <li>
        Offset in Face
       </li>
       <li>
        Wrap/Unwrap
       </li>
      </ul>
      <h2>
       Free form Feature
      </h2>
      <ul>
       <li>
        Throuh points
       </li>
       <li>
        From P Pole
       </li>
       <li>
        From Point Cloud
       </li>
       <li>
        Ruled
       </li>
       <li>
        Through
       </li>
       <li>
        Curves
       </li>
       <li>
        Through
       </li>
       <li>
        Curve Mesh
       </li>
       <li>
        Swept
       </li>
       <li>
        Section
       </li>
       <li>
        Bridge
       </li>
       <li>
        N-Sided
       </li>
       <li>
        Surface Extension
       </li>
       <li>
        Law Extension
       </li>
       <li>
        Enlarge
       </li>
       <li>
        Offset
       </li>
       <li>
        Rough
       </li>
       <li>
        Offset
       </li>
       <li>
        Quilt
       </li>
       <li>
        Unsuppress Component
       </li>
       <li>
        Replace Reference set
       </li>
       <li>
        Define Mating
       </li>
       <li>
        Varify Mating Alternate
       </li>
       <li>
        Part Family Update
       </li>
       <li>
        Propertise
       </li>
       <li>
        Check Clearances
       </li>
      </ul>
      <h2>
       Exploded Views
      </h2>
      <ul>
       <li>
        Create
       </li>
       <li>
        Edit
       </li>
       <li>
        Delete
       </li>
       <li>
        Hide
       </li>
       <li>
        Show Explosion
       </li>
       <li>
        Object Display
       </li>
       <li>
        Blank
       </li>
       <li>
        Transformations
       </li>
       <li>
        Copy Feature
       </li>
       <li>
        Delayed Update on Edit , Update
       </li>
      </ul>
      <h2>
       View
      </h2>
      <ul>
       <li>
        Referesh
       </li>
       <li>
        Operation
       </li>
       <li>
        Orentation
       </li>
       <li>
        Toolbar
       </li>
      </ul>
      <h2>
       Format
      </h2>
      <ul>
       <li>
        Layout , New . Open , Open Drawing
       </li>
       <li>
        FitA all view
       </li>
       <li>
        Uddate Display
       </li>
       <li>
        Regenerate
       </li>
       <li>
        Replace View
       </li>
       <li>
        Delete
       </li>
       <li>
        Save
       </li>
       <li>
        Saveas
       </li>
      </ul>
      <h2>
       Tools
      </h2>
      <ul>
       <li>
        Expre3session
       </li>
       <li>
        Swoop
       </li>
       <li>
        Studio Surface
       </li>
       <li>
        Styled Blend
       </li>
       <li>
        Global Shapping
       </li>
       <li>
        Trimmed Sheet
       </li>
       <li>
        Fillet
       </li>
       <li>
        Foregion
       </li>
      </ul>
      <h2>
       Miscellaneious Edit
      </h2>
      <ul>
       <li>
        Curve
       </li>
       <li>
        Infer Point
       </li>
       <li>
        Free form Feature
       </li>
       <li>
        Syntax
       </li>
       <li>
        Face
       </li>
       <li>
        Format
       </li>
       <li>
        Group
       </li>
       <li>
        Attributes
       </li>
       <li>
        Group Features
       </li>
       <li>
        Tools
       </li>
       <li>
        Visual Editor
       </li>
       <li>
        Spread Sheet
       </li>
       <li>
        Smart Models (Product Definition , Gemetric Teleracing )
       </li>
       <li>
        Material
       </li>
       <li>
        Propertise
       </li>
       <li>
        Introduction to knowledge Fusion
       </li>
       <li>
        Part Families
       </li>
      </ul>
      <h2>
       Advance Modeling (Level 3)
      </h2>
      <ul>
       <li>
        Assembly Structure
       </li>
       <li>
        Top Down
       </li>
       <li>
        Bottom up Design
       </li>
      </ul>
      <h2>
       Mating Conditions
      </h2>
      <ul>
       <li>
        Mate
       </li>
       <li>
        Align
       </li>
       <li>
        Angle
       </li>
       <li>
        Parallel
       </li>
       <li>
        Perpendicualr
       </li>
       <li>
        Center
       </li>
       <li>
        Distance
       </li>
       <li>
        Tangent
       </li>
      </ul>
      <h2>
       Component
      </h2>
      <ul>
       <li>
        Add Exesting
       </li>
       <li>
        Create new
       </li>
       <li>
        Create Array
       </li>
       <li>
        Substitude
       </li>
       <li>
        Reposition
       </li>
       <li>
        Mate
       </li>
       <li>
        Suppress
       </li>
       <li>
        Assembly Diagram
       </li>
       <li>
        Family Report
       </li>
      </ul>
      <h2>
       Context Control
      </h2>
      <ul>
       <li>
        Find
       </li>
       <li>
        Open Locate Comonents
       </li>
       <li>
        Open by Proximity
       </li>
       <li>
        Show Product Outline Save
       </li>
       <li>
        Restore Context
       </li>
       <li>
        Define Product Outline
       </li>
       <li>
        Set work/Display part
       </li>
      </ul>
      <h2>
       Envelop
      </h2>
      <ul>
       <li>
        Wrap Assembly
       </li>
       <li>
        Linked Exterior
       </li>
      </ul>
      <h2>
       Miscellanceous
      </h2>
      <ul>
       <li>
        Delay Interpart Updates
       </li>
       <li>
        Update Session
       </li>
       <li>
        Varient Configuration
       </li>
       <li>
        Edit Component
       </li>
       <li>
        Arrays
       </li>
       <li>
        Wave Geometry Liker
       </li>
       <li>
        Reference sets's
       </li>
       <li>
        Znnes Representations
       </li>
       <li>
        Comon Fillter
       </li>
       <li>
        Parts List
       </li>
      </ul>
      <h2>
       Assambly nagvigtor Assabmly preferences Drawing
      </h2>
      <ul>
       <li>
        New
       </li>
       <li>
        Open
       </li>
       <li>
        Delete
       </li>
       <li>
        Edit
       </li>
       <li>
        Add View [Import , Orthographic , Auxiliary , Detail view , Simple ,Steepped , Half , Revoled , Fold Section cut , Simple Steeped . Half Section Cut From Pictroal view]
       </li>
       <li>
        Remove
       </li>
       <li>
        Move /copy
       </li>
       <li>
        Align
       </li>
       <li>
        Edit view
       </li>
       <li>
        Define view boundary
       </li>
       <li>
        Brake out
       </li>
       <li>
        Section
       </li>
       <li>
        Broken view
       </li>
       <li>
        Display Drawing
       </li>
       <li>
        Updae view
       </li>
      </ul>
      <h2>
       Insert
      </h2>
      <ul>
       <li>
        Dimension [Inferred , Horizontal , vertialcal , Parallel , Radius , Radius to center , Folded Radious Concentic Circle , Arc Length , Horizontal chain , Vertiacal chain , Horizontal
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="unigraphicstraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="68 + 72 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="140">
       <input name="url" type="hidden" value="/unigraphicstraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>