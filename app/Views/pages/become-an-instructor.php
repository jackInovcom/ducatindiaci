<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     BECOME AN INSTRUCTOR
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="#">
     Become An INSTRUCTOR
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9 text-center">
    <h3 style="text-align:left">
     BECOME AN INSTRUCTOR AT DUCAT
    </h3>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
    <p>
     <img src="../images/become-an-instructor.jpg"/>
    </p>
    <br/>
    <div style="text-align:left">
     <p>
      Being an instructor is not a bad job because through this profession you can deliver your skills to others in the most efficient way so that other people can with the other companies.
     </p>
     <h4>
      THE QUESTION ARISES WHERE TO GO?
     </h4>
     <p>
      We at Ducat provide the opportunities to the newbies to make their career as an instructor. At Ducat you can apply for more than one zone as a trainer for the technology they are good at. Trainers have always played a vital role in the enhancement of the knowledge as every instructor has his own way to deal and deliver to his trainees and at Ducat, you can find range of instructors with the most innovative ways of teaching.
     </p>
     <p>
      It is not easy to find a right place to work at but at Ducat we end your search because Ducat is the best place for the all the instructors whether new or experienced to work at because it is one of the good education and training industries where you can get the best exposure in every technological field if you have that talent in you.
     </p>
     <p>
      Instructors are those who are responsible for whatever a trainee learns that is why it is becoming an instructor is not an easy job and to be one amongst the known instructors and to increase your value it is necessary to work with the one where you get the real value and Ducat is the right place for you if you want to enlist your name as a good instructor.
     </p>
    </div>
    <h4>
     PLEASE FILL IN THE FORM AND WE SHALL CONTACT YOU SOON!
    </h4>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
    <form action="../logics_database/become_instructor.php" class="onlineRegistration text-left" method="post">
     <div class="row">
      <div class="col-md-6">
       <input autofocus="" name="fname" placeholder="Enter First Name" required="" tabindex="1" type="text"/>
       <input id="telephone" name="telephone" pattern="[0-9]{10,11}" placeholder="Phone number" required="" tabindex="3" type="text"/>
       <input name="course" pattern="[a-zA-Z0-9_-]*{1,20}" placeholder="Course Name" required="" tabindex="5" type="text"/>
       <textarea class="txtblock" name="message1" placeholder="About The Course!" required="" tabindex="7"></textarea>
       <textarea class="txtblock" name="message3" placeholder="Your Current Location" required="" tabindex="9"></textarea>
      </div>
      <!-- End Of Col MD 6 -->
      <div class="col-md-6">
       <input autofocus="" name="lname" placeholder="Enter Last Name" required="" tabindex="2" type="text"/>
       <input autocomplete="off" name="email" placeholder="E-mail" required="" tabindex="4" type="text"/>
       <input autocomplete="off" name="linkedIn" pattern="[a-zA-Z0-9_-]*{6,30}" placeholder="linkedin Profile" required="" tabindex="6" type="text"/>
       <textarea name="message2" placeholder="Tell Us More About Your Self!" required="" tabindex="8"></textarea>
       <textarea name="message4" placeholder="Part Time/Full Time" required="" tabindex="10"></textarea>
      </div>
      <!-- End Of Col MD 6 -->
      <div class="col-md-12">
       <input name="submitReg" type="submit" value="Submit"/>
      </div>
      <!-- End Of Col MD 6 -->
     </div>
     <!-- End Of Row -->
    </form>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <form class="searchForm">
     <input placeholder="Search" type="text"/>
    </form>
    <div class="widgetArea">
     <h5>
      CONTACT INFO
     </h5>
     <address>
      <span class="address">
       A - 43 &amp; A - 52 Sector - 16,
       <br/>
       Noida (U.P) (Near McDonalds)
      </span>
      <br>
       <span class="phone">
        <strong>
         Phone:
        </strong>
        0120-4646464, +91- 9871055180
       </span>
       <br/>
       <span class="email">
        <strong>
         E-Mail:
        </strong>
        <a href="mailto:info@ducatindia.com">
         info@ducatindia.com
        </a>
       </span>
       <br/>
       <span class="web">
        <strong>
         Web:
        </strong>
        <a href="http://www.ducatindia.com/">
         http://www.ducatindia.com/
        </a>
       </span>
      </br>
     </address>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section class="text-center" id="offices">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <h5>
     CORPORATE OFFICE NOIDA:
     <span>
      0120 - 4646464
     </span>
    </h5>
    <p>
     GR.NOIDA:
     <span>
      0120-4345190
     </span>
     GHAZIABAD:
     <span>
      0120-4835400
     </span>
     FARIDABAD:
     <span>
      0129-4150605
     </span>
     GURGAON:
     <span>
      0124-4219095
     </span>
     JAIPUR:
     <span>
      0141-2550077
     </span>
    </p>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>