<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     Microsoft sql server Training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Microsoft sql server
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Microsoft SQL Server is Microsoft�s relational database management system for Windows. Any of our SQL Server 2008 courses can also be taught using SQL Server 2008 R2.Accelebrate�s SQL Server 2008 training classes cover a wide range of subjects, including administration, tuning, query writing with T-SQL, reporting with SSRS, data moving and transformation with SSIS, and multi-dimensional analysis with SSAS and MDX.Request pricing for SQL Server 2008 training for your team.
     </p>
     <div class="contentAcc">
      <h2>
       Basics
      </h2>
      <ul>
       <li>
        Database
       </li>
       <li>
        DBMS
       </li>
       <li>
        RDBMS
       </li>
      </ul>
      <h2>
       SQL Server Environment
      </h2>
      <ul>
       <li>
        SQL Server 2005 and 2008, 2012
       </li>
      </ul>
      <h2>
       Membership, Authorization And Security
      </h2>
      <ul>
       <li>
        DDL
       </li>
       <li>
        DML (INSERT, UPDATE, DELETE, MERGE)
       </li>
       <li>
        TCL
       </li>
       <li>
        DDL
       </li>
       <li>
        DCL
       </li>
      </ul>
      <h2>
       Data Integrity
      </h2>
      <ul>
       <li>
        Domain Integrity
       </li>
       <li>
        Entity Integrity
       </li>
       <li>
        Referential Integrity
       </li>
      </ul>
      <h2>
       Joins and Sub Queries
      </h2>
      <ul>
       <li>
        SQL Operators
       </li>
       <ul>
        <li>
         Relational Operators
        </li>
        <ul>
         <li>
          Basic Relational operators
         </li>
         <li>
          Advanced Relational operators
         </li>
        </ul>
       </ul>
       <li>
        3. Logical operators
       </li>
       <li>
        Simple Queries
       </li>
       <li>
        Built in Functions
       </li>
       <ul>
        <li>
         Character Functions
        </li>
        <li>
         Number Functions
        </li>
        <li>
         Date Functions
        </li>
       </ul>
       <li>
        Joins
       </li>
       <li>
        Sub-Queries
       </li>
       <li>
        Advanced Queries
       </li>
      </ul>
      <h2>
       Views
      </h2>
      <ul>
       <li>
        Create View
       </li>
       <li>
        Advantage of Views
       </li>
       <li>
        Updatable Views
       </li>
       <li>
        Non Updatable Views
       </li>
       <li>
        Indexed View
       </li>
      </ul>
      <h2>
       Introduction to T-SQL
      </h2>
      <ul>
       <li>
        Anonymous Block
       </li>
       <li>
        Stored Block
       </li>
      </ul>
      <h2>
       Anonymous block(including 2008 features)
      </h2>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>