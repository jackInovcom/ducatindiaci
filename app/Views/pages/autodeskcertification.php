<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Autodesk Partner(Ghaziabad)
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Autodesk Partner(Ghaziabad)
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-4">
    <div align="center" class="eventBox">
     <img alt="Event" class="img-responsive" src="../images/autocad.png"/>
     <div class="clearfix">
     </div>
     <h4>
      Autocad Training Certification
     </h4>
     <br/>
     <p>
      AUTOCAD as it says AUTOMATIC COMPUTER-AIDED DESIGN is a software program developed to create 2D and 3D !
     </p>
     <a href="../autocadtraining">
      Continue Reading
     </a>
    </div>
   </div>
   <!-- End Of Col MD 4 -->
   <div class="col-md-4">
    <div align="center" class="eventBox">
     <img alt="Event" class="img-responsive" src="../images/revit.png"/>
     <div class="clearfix">
     </div>
     <h4>
      Revit Architecture Training
 Certification
     </h4>
     <br/>
     <p>
      DUCAT is the most perfect place where anyone applicant to learn REVIT Architecture can be at.!
     </p>
     <a href="../revittraining">
      Continue Reading
     </a>
    </div>
    <!-- End Of Col MD 4 -->
   </div>
   <div class="col-md-4">
    <div align="center" class="eventBox">
     <img alt="Event" class="img-responsive" src="../images/3dmax.png"/>
     <div class="clearfix">
     </div>
     <h4>
      3D Studio Max Training
 Certification
     </h4>
     <br/>
     <p>
      DUCAT is the ideal place where anyone aspirant to learn 3D Studio Max Certification !
     </p>
     <a href="../3dmaxtraining">
      Continue Reading
     </a>
    </div>
   </div>
   <!-- End Of Col MD 4 -->
  </div>
  <!-- End Of Row -->
  <div class="row">
   <div class="col-md-6">
    <div align="center" class="eventBox">
     <img alt="Event" class="img-responsive" src="../images/autocadcivil.png"/>
     <div class="clearfix">
     </div>
     <h4>
      CAD Civil Training Certification
     </h4>
     <br/>
     <p>
      People who want to have a respectable job must have CAD Civil training Certification !
     </p>
     <a href="../cadciviltraining">
      Continue Reading
     </a>
    </div>
   </div>
   <div class="col-md-6">
    <div align="center" class="eventBox">
     <img alt="Event" class="img-responsive" src="../images/autocad.png"/>
     <div class="clearfix">
     </div>
     <h4>
      CAD Mechanical Training Certification
     </h4>
     <br/>
     <p>
      Computer Aided Design (CAD)implicates the use of computer hardware and graphics software!
     </p>
     <a href="../cadmechanicaltraining">
      Continue Reading
     </a>
    </div>
   </div>
  </div>
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>