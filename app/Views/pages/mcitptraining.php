<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     MCITP
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     MCITP
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Earn an MCITP certification and be recognized for your problem solving skills and leadership in Windows Server 2008. The Microsoft Certified IT Professional (MCITP) certification validates a unique job skill set including project management, operations management, system design, and planning with Windows Server platforms. DUCAT courses will bring you the knowledge and skills required for managing, deploying, building, designing, optimizing and operating upon MS Windows Server Technology. For obtaining a MCTIP certification, candidates must first obtain one or more of the prerequisites for a MCTS certification and then qualify the PRO exam. With our state of the art facilities, we have been successful in providing students with the best skill set for qualifying MCTIP certification for over a decade. DUCAT course structure is a balanced mix of theoretical lectures and practical session that provides hands on experience to candidates and readied them for the competitive job market. Our faculty is a team of more than 100 certified and highly experienced engineers who bring forth their knowledge directly from the demanding workplaces. We ensure that candidates not only secure a MCITP certification but are able to put the knowledge into best use.
     </p>
     <div class="contentAcc">
      <h2>
       MCITP 1. 70-680 Windows 7 INSTALLING, UPGRADING, AND MIGRATING TO WINDOWS 7
      </h2>
      <ul>
       <li>
        Describe the keys feature, editions, and hardware requirements of Windows 7
       </li>
       <li>
        Perform a clean installation of windows 7
       </li>
       <li>
        Upgrade and Migrate to Windows 7 from an earlier version of Windows
       </li>
       <li>
        Perform an image-based installation of Windows 7
       </li>
       <li>
        Resolve common application compatibility issues.
       </li>
      </ul>
      <h2>
       CONFIGURRING USER ACCOUNT AND GROUP MANAGEMENT
      </h2>
      <ul>
       <li>
        Local User and group
       </li>
       <li>
        Built-in User and groupe
       </li>
      </ul>
      <h2>
       CONFIGURING DISKS AND DEVICE DRIVERS
      </h2>
      <ul>
       <li>
        Partitioning Disk in windows 7
       </li>
       <li>
        Managing disk volumes
       </li>
       <li>
        Maintaining disks in Windows 7
       </li>
       <li>
        Installing and configuration Device drivers
       </li>
       <li>
        Configuring Disk Quota (optional)
       </li>
       <li>
        Updating a Device driver
       </li>
      </ul>
      <h2>
       CONFIGURING FILE ACCESS ON WINDOWS 7 CLIENT COMPUTERS
      </h2>
      <ul>
       <li>
        Overview of Windows file system
       </li>
       <li>
        Managing file Access in windows 7
       </li>
       <li>
        Managing shared folders
       </li>
       <li>
        x file compression
       </li>
       <li>
        Create and Configuring a public shared folder for all user
       </li>
      </ul>
      <h2>
       CONFIGURING NETWORK CONNECTIVITY
      </h2>
      <ul>
       <li>
        Configuring IPv4 Network Connectivity
       </li>
       <li>
        Configuring Ipv6 Network connectivity
       </li>
       <li>
        Implementing Automatic IP address Allocation
       </li>
       <li>
        Overview of Name resolutione
       </li>
       <li>
        Troubleshooting Network Issue
       </li>
       <li>
        Configuring Network connectivity
       </li>
      </ul>
      <h2>
       SECURING WINDOWS 7 DESKTOPS
      </h2>
      <ul>
       <li>
        Overview of security Management in Windows 7
       </li>
       <li>
        Securing a windows 7 client computer by using Local security policy setting
       </li>
       <li>
        Security Data by Using EFS
       </li>
       <li>
        Configuring User Account control
       </li>
       <li>
        Configuring Windows firewall
       </li>
       <li>
        Configuring security setting in Internet Explorer 8
       </li>
       <li>
        Configuring Windows defender
       </li>
      </ul>
      <h2>
       CONFIGURING REMOTE ACCESS IN WINDOWS 7
      </h2>
      <ul>
       <li>
        Configuring remote Desktop and Remote Assistance for Remote Access
       </li>
      </ul>
      <h2>
       2. 70-647 WINDOWS SERVER 2008, ENTERPRISE ADMINISTRATOR OVERVIEW OF NETWORK INFRASTRUCTURE DESIGN
      </h2>
      <ul>
       <li>
        Preparing for Network infrastructure design
       </li>
       <li>
        Designing network topology
       </li>
       <li>
        Designing Network infrastructure for virtualization
       </li>
       <li>
        Designing a change Management structure for a Network
       </li>
      </ul>
      <h2>
       DESIGNING NETWORK SECURITY
      </h2>
      <ul>
       <li>
        Overview of Network Security design
       </li>
       <li>
        Creating a Network security Plan
       </li>
       <li>
        Identifying threats to Network security
       </li>
       <li>
        Analyzing security risks
       </li>
      </ul>
      <h2>
       DESIGNING IP ADDRESS
      </h2>
      <ul>
       <li>
        Designing an IPv4 addressing scheme
       </li>
       <li>
        Designing an IPv6 addressing scheme
       </li>
       <li>
        Designing DHCP implementation
       </li>
       <li>
        Designing DHCP configuration process
       </li>
      </ul>
      <h2>
       DESIGNING ROUTING AND SWITCHING
      </h2>
      <ul>
       <li>
        Preparing for Designing a Networking routing Topology
       </li>
       <li>
        Selecting Network devices
       </li>
       <li>
        Designing internet connectivity and perimeter Networks
       </li>
       <li>
        Designing routing Communications
       </li>
       <li>
        Evaluating Network performance
       </li>
      </ul>
      <h2>
       DESIGNING FILE SERVICES AND DFS IN WINDOWS SERVER 2008
      </h2>
      <ul>
       <li>
        Designing file services
       </li>
       <li>
        Designing DFS
       </li>
       <li>
        Designing the FSRM configuration
       </li>
      </ul>
      <h2>
       DESIGNING AD LDS IMPLEMENTATION
      </h2>
      <ul>
       <li>
        AD LDS deployment scenarios
       </li>
       <li>
        Overview of an AD LDS implementation Design
       </li>
       <li>
        Designing AD LDS schema and replication
       </li>
       <li>
        Integrating AD LDS with AD DS
       </li>
      </ul>
      <h2>
       DESIGNING ACTIVE DIRECTORY MIGRATIONS IN WINDOWS SERVER 2008
      </h2>
      <ul>
       <li>
        Choosing an Active directory Migration strategy
       </li>
       <li>
        Designing a domain Upgrade strategy
       </li>
       <li>
        Designing a domain restructure strategy
       </li>
      </ul>
      <h2>
       70-642 NETWORK INFRASTRUCTURE PLANNING AND CONFIGURING IPV4
      </h2>
      <ul>
       <li>
        Implementing an IPv4 network infrastructure
       </li>
       <li>
        Overview of Name resolution services in an IPv4
       </li>
       <li>
        Selecting an IPv4 addressing scheme for branch offices
       </li>
       <li>
        Implementing and verifying IPv4 in the branch offices
       </li>
      </ul>
      <h2>
       CONFIGURATION AND TROUBLESHOOTING DHCP
      </h2>
      <ul>
       <li>
        Overview of the DHCP server Role
       </li>
       <li>
        Configuring DHCP Scopes
       </li>
       <li>
        Configuration DHCP options
       </li>
       <li>
        Managing a DHCP Database
       </li>
       <li>
        Monitoring and troubleshooting DHCP
       </li>
       <li>
        Configuration DHCP security
       </li>
      </ul>
      <h2>
       CONFIGURATION AND TROUBLESHOOTING DNS
      </h2>
      <ul>
       <li>
        Installing the DNS server role
       </li>
       <li>
        Configuring the DNS server role
       </li>
       <li>
        Configuring DNS Zones
       </li>
       <li>
        Configuring DNS zone transfers
       </li>
       <li>
        Managing and troubleshooting DNS
       </li>
      </ul>
      <h2>
       CONFIGURATION AND TROUBLESHOOTING IPv6 TCP/IP
      </h2>
      <ul>
       <li>
        Overview of IPv6
       </li>
       <li>
        IPv6 addressing
       </li>
       <li>
        Coexistence with IPv6
       </li>
       <li>
        IPv6 technologies
       </li>
       <li>
        Transitioning from IPv4 to IPv6
       </li>
      </ul>
      <h2>
       CONFIGURATION AND TROUBLESHOOTING ROUTING AND REMOTE ACCESS
      </h2>
      <ul>
       <li>
        Configuration network access
       </li>
       <li>
        Configuration VPN access
       </li>
       <li>
        Overview of Network policies
       </li>
       <li>
        Troubleshooting routing and remote access
       </li>
       <li>
        Configure the PKI environment
       </li>
       <li>
        Configuration RADIUS clients and servers
       </li>
      </ul>
      <h2>
       IMPLEMENTATION NETWORK ACCESS PROTECTION IN THIS MODULE
      </h2>
      <ul>
       <li>
        How NAP works
       </li>
       <li>
        Configuring NAP
       </li>
      </ul>
      <h2>
       INCREASING SECURITY FOR WINDOWS SERVER SECURITY IS AN ESSENTIAL CONSIDERATION FOR NETWORKING WITH WINDOWS SERVER 2008
      </h2>
      <h2>
       INCREASING SECURITY FOR NETWORK COMMUNICATION INTERNET PROTOCOL SECURITY (Ipsec)
      </h2>
      <ul>
       <li>
        Overview of Ipsec
       </li>
       <li>
        Configuring connection security rules
       </li>
       <li>
        Monitoring and troubleshooting Ipsec
       </li>
      </ul>
      <h2>
       CONFIGURATION AND TROUBLESHOOTING NETWWORK FILE AND PRINT SERVICES
      </h2>
      <ul>
       <li>
        Configuration and troubleshooting file shares
       </li>
       <li>
        Encryption Network files with EFS
       </li>
       <li>
        Encrypting partitions with BitLocker
       </li>
       <li>
        Configuration and troubleshooting Network printing
       </li>
      </ul>
      <h2>
       OPTIMIZING DATA ACCESS FOR BRANCH OFFICES
      </h2>
      <ul>
       <li>
        Branch offices data access
       </li>
       <li>
        DFS overview
       </li>
       <li>
        Overview of DFS Namespace
       </li>
       <li>
        Configuring DFS replication
       </li>
      </ul>
      <h2>
       CONTROLLING AND MONITORING NETWORK STORAGE
      </h2>
      <ul>
       <li>
        Monitoring Network storage
       </li>
       <li>
        Controlling network storage utilization
       </li>
       <li>
        Managing file types on Network storage
       </li>
      </ul>
      <h2>
       RECOVERING NETWORK DATA AND SERVER
      </h2>
      <ul>
       <li>
        Recovering network data with volume shadow copies
       </li>
       <li>
        Recovering network data and server with windows server backup
       </li>
      </ul>
      <h2>
       MONITORING WINDOWS SERVER 2008 NETWORK INFRASTRUCTURE SERVER
      </h2>
      <ul>
       <li>
        Monitoring tools
       </li>
       <li>
        Using performance monitor
       </li>
       <li>
        Monitoring event logs
       </li>
      </ul>
      <h2>
       70-640 ACTIVE DIRECTORY INTRODUCTION ACTIVE DIRECTORY DOMAIN SERVICES
      </h2>
      <ul>
       <li>
        Overview of active directory, identity and Access
       </li>
       <li>
        Active directory components and concept
       </li>
       <li>
        Install active directory domain services
       </li>
      </ul>
      <h2>
       DESIGNING AN ACTIVE DIRECTORY FOREST INFRASTRUCTURE IN WINDOWS SERVER 2008
      </h2>
      <ul>
       <li>
        Determine active directory forest design requirements
       </li>
       <li>
        Design an Active directory forest
       </li>
       <li>
        Designing active directory forest trusts
       </li>
       <li>
        Managing active directory schema
       </li>
       <li>
        Understanding windows Time services
       </li>
      </ul>
      <h2>
       DESIGNING AN ACTIVE DIRECTORY DOMAIN INFRASTRUCTURE IN WINDOWS SERVER 2008
      </h2>
      <ul>
       <li>
        Designing active directory Domains
       </li>
       <li>
        Designing the Integration of DNS Namespaces
       </li>
       <li>
        Designing the placement of domain controllers
       </li>
       <li>
        Designing Active directory domain trusts
       </li>
      </ul>
      <h2>
       ADMINISTERING ACTIVE DIRECTORY SECURITY AND EFFICIENTLY
      </h2>
      <ul>
       <li>
        Work with Active directory Administration tools
       </li>
       <li>
        Find objects in Active directory
       </li>
      </ul>
      <h2>
       MANAGING USERS AND SERVER ACCOUNTS
      </h2>
      <ul>
       <li>
        Create and Administer user Account
       </li>
       <li>
        Configure user object attributes
       </li>
       <li>
        Create and configure managed service Accounts
       </li>
      </ul>
      <h2>
       MANAGING GROUPS
      </h2>
      <ul>
       <li>
        Overview of groups
       </li>
       <li>
        Administer groups
       </li>
       <li>
        Best practices for group management
       </li>
      </ul>
      <h2>
       MANAGING COMPUTER ACCOUNTS
      </h2>
      <ul>
       <li>
        Create computer and Join the domain
       </li>
       <li>
        Administer computer objects and accounts
       </li>
       <li>
        Perform an offline domain join
       </li>
      </ul>
      <h2>
       IMPLEMENTING A GROUP POLICY INFRASTRUCTURE
      </h2>
      <ul>
       <li>
        Understand group policy
       </li>
       <li>
        Implement GPO's
       </li>
       <li>
        Management group policy scope
       </li>
       <li>
        Group policy processing
       </li>
       <li>
        Trouble policy application
       </li>
      </ul>
      <h2>
       MANGING USER DESKTOP WITH GROUP POLICY
      </h2>
      <ul>
       <li>
        Implement administrative Templates
       </li>
       <li>
        Configure group policy preference
       </li>
       <li>
        Manage soft ware with GPSI
       </li>
      </ul>
      <h2>
       MANAGING ENTERPRISE SECURITY AND CONFIGURATION WITH GROUP POLICY SETTING
      </h2>
      <ul>
       <li>
        Manage Group membership by using group policy setting
       </li>
       <li>
        Manage security setting
       </li>
       <li>
        Auditing
       </li>
      </ul>
      <h2>
       SECURING ADMINISTRATION
      </h2>
      <ul>
       <li>
        Delegate administrative permission
       </li>
       <li>
        Audit active directory administration
       </li>
      </ul>
      <h2>
       IMPROVING THE SECURITY OF AUTHENTICATION IN AN AD DS DOMAIN
      </h2>
      <ul>
       <li>
        Configure password and Lockout policies
       </li>
       <li>
        Audit authentication
       </li>
       <li>
        Configure read only domain controller
       </li>
      </ul>
      <h2>
       CONFIGURING DOMAIN NAME SYSTEM
      </h2>
      <ul>
       <li>
        Install and configure DNS in an AD DS domain
       </li>
       <li>
        Integration of AD DS , DNS , and Windows
       </li>
       <li>
        Advanced DNS configuration and Administration
       </li>
      </ul>
      <h2>
       ADMINISTRATION AD DS DOMAIN CONTROLLERS
      </h2>
      <ul>
       <li>
        domain controller installation options
       </li>
       <li>
        install a server core domain controller
       </li>
       <li>
        manage operations Master
       </li>
       <li>
        configure global catalog
       </li>
      </ul>
      <h2>
       MANAGING SITES AND ACTIVE DIRECTORY REPLICATION
      </h2>
      <ul>
       <li>
        Configure sites and subnets
       </li>
       <li>
        Configure replication
       </li>
      </ul>
      <h2>
       DIRECTORY SERVICE CONTINUITY
      </h2>
      <ul>
       <li>
        Configure sites and subnets
       </li>
       <li>
        Configure replication
       </li>
      </ul>
      <h2>
       MANAGING MULTIPLE DOMAINS AND FORESTS
      </h2>
      <ul>
       <li>
        Configure domain and forests functional levels
       </li>
       <li>
        Manage multiple domains and trusts relationships
       </li>
       <li>
        Move objects between domains and forests
       </li>
      </ul>
      <h2>
       DEPLOYING AND CONFIGURING ACTIVE DIRECTORY CERTIFICATE
      </h2>
      <ul>
       <li>
        Overview of active directory lightweight directory services
       </li>
       <li>
        Deploying and configuring active directory lightweight directory services
       </li>
       <li>
        Configure AD LDS instances and partitions
       </li>
       <li>
        Configuring active directory lightweight directory services
       </li>
       <li>
        Replication
       </li>
       <li>
        Troubleshooting active directory lightweight directory services
       </li>
      </ul>
      <h2>
       DEPLOYING AND CONFIGURING ACTIVE DIRECTORY LIGHTWEIGHT DIRECTORY SERVICES
      </h2>
      <ul>
       <li>
        Overview of active directory light weight directory services
       </li>
       <li>
        Deploying and configuring active directory lightweight directory service
       </li>
       <li>
        Configure AD DS instances and partitions
       </li>
       <li>
        Configuring Active directory lightweight directory service
       </li>
       <li>
        Replication
       </li>
       <li>
        Troubleshooting active directory lightweight directory service
       </li>
      </ul>
      <h2>
       DEPLOYING AND CONFIGURING ACTIVE DIRECTORY RIGHTS MANAGEMENT SERVICES
      </h2>
      <ul>
       <li>
        Overview of active directory rights managements services
       </li>
       <li>
        Deploying and configuring active directory rights management services
       </li>
       <li>
        Configuring AD RMS right policy templates and Exclusion policies
       </li>
       <li>
        Configuring active directory right management services trust policies
       </li>
       <li>
        Troubleshooting active directory rights management services
       </li>
      </ul>
      <h2>
       5. 70-643 APPLICATIONS INFRASTRUCTURE, CONFIGURING INSTALLING AND CONFIGURING MICROSOFT WINDOWS SERVER 2008 R2
      </h2>
      <ul>
       <li>
        Planning Microsoft windows server 2008 R2 installations
       </li>
       <li>
        Performing a Microsoft windows server 2008 R2
       </li>
       <li>
        Configuring Microsoft windows server 2008 R2 following installation
       </li>
       <li>
        Automating server activation using Microsoft volume activation
       </li>
      </ul>
      <h2>
       IMPLEMENTATION DEPLOYMENT TECHNOLOGIES
      </h2>
      <ul>
       <li>
        Selecting a deployment strategy
       </li>
       <li>
        Using the windows automated installation kit
       </li>
       <li>
        Working with images
       </li>
       <li>
        Working with unattended answer files
       </li>
      </ul>
      <h2>
       USING WINDOWS DEPLOYMENT SERVICES
      </h2>
      <ul>
       <li>
        Overview of windows deployment services
       </li>
       <li>
        Implementing deployment with WD
       </li>
       <li>
        Maintaining images
       </li>
      </ul>
      <h2>
       ACTIVE DIRECTORY DIRECTORY SERVICE
      </h2>
      <ul>
       <li>
        Overview of upgrading windows server 2008 R2 active directory
       </li>
       <li>
        Upgrading domain controllers to windows server 2008 R2
       </li>
      </ul>
      <h2>
       FILE, PRINT AND WEB SERVICES
      </h2>
      <ul>
       <li>
        Introducing the windows file and print servers
       </li>
      </ul>
      <h2>
       DEPLOYING BRANCH OFFICES AND REMOTE ACCESS SERVICES
      </h2>
      <ul>
       <li>
        Implementing read-only domain controllers
       </li>
       <li>
        Implementing remote infrastructure
       </li>
      </ul>
      <h2>
       MIGRATING WORKLOADS TO MICROSOFT VIRTUAL MACHINE
      </h2>
      <ul>
       <li>
        Overview of Microsoft virtualization
       </li>
       <li>
        Installing Hyper -V
       </li>
      </ul>
      <h2>
       CONFIGURING TERMINAL SERVICES CORE FUNCTIONALITY
      </h2>
      <ul>
       <li>
        Configuring TS Role
       </li>
       <li>
        Configuring TS settings
       </li>
      </ul>
      <h2>
       Configuring and Managing Terminal Services Licensing
      </h2>
      <ul>
       <li>
        Configuring TS Licensing
       </li>
       <li>
        Managing TS Licenses
       </li>
       <li>
        Installing Applications
       </li>
       <li>
        Configuring Remote Applications
       </li>
       <li>
        Configuring Printers
       </li>
       <li>
        Configuring Terminal Services Web Access
       </li>
       <li>
        Installing the TS Web Access Role Service
       </li>
       <li>
        Configuring Session Broker
       </li>
      </ul>
      <h2>
       Configuring and Troubleshooting Terminal Services Gateway
      </h2>
      <ul>
       <li>
        Configuring TS Gateway
       </li>
       <li>
        Troubleshooting TS Gateway Connectivity Issues
       </li>
      </ul>
      <h2>
       Configuring an IIS 7.0 Web Server
      </h2>
      <ul>
       <li>
        Introducing Internet Information Services 7.0
       </li>
       <li>
        Installing the Web Server Role in Windows Server 2008
       </li>
       <li>
        Configuring Application Development, Health, and HTTP Features
       </li>
       <li>
        Configuring Performance, Security, and Server Component Features
       </li>
      </ul>
      <h2>
       Configuring IIS 7.0 Web Sites and Application Pools
      </h2>
      <ul>
       <li>
        Introducing Web Sites and Application Pools
       </li>
       <li>
        Creating a Web Site
       </li>
       <li>
        Creating an Application Pool
       </li>
       <li>
        Maintaining an Application Pool
       </li>
      </ul>
      <h2>
       Securing the IIS 7.0 Web Server and Web Sites
      </h2>
      <ul>
       <li>
        Configuring Secure Web Sites and Servers
       </li>
       <li>
        Configuring Logging for IIS 7.0
       </li>
      </ul>
      <h2>
       Configuring Delegation and Remote Administration
      </h2>
      <ul>
       <li>
        Configuring Remote Administration
       </li>
       <li>
        Configuring Feature Delegation
       </li>
       <li>
        Backing Up and Restoring Web Sites
       </li>
       <li>
        Working with Shared Configurations
       </li>
      </ul>
      <h2>
       5. 70-647 Windows Server 2008, Enterprise Administrator Overview of Network Infrastructure Design
      </h2>
      <ul>
       <li>
        Preparing for Network Infrastructure Design
       </li>
       <li>
        Designing the Network Topology
       </li>
       <li>
        Designing Network Infrastructure for Virtualization
       </li>
       <li>
        Designing a Change Management Structure for a Network
       </li>
      </ul>
      <h2>
       Designing Network Security
      </h2>
      <ul>
       <li>
        Overview of Network Security Design
       </li>
       <li>
        Creating a Network Security Plan
       </li>
       <li>
        Identifying Threats to Network Security
       </li>
       <li>
        Analyzing Security Risks
       </li>
      </ul>
      <h2>
       Designing IP Addressing
      </h2>
      <ul>
       <li>
        Designing an IPv4 Addressing Scheme
       </li>
       <li>
        Designing an IPv6 Addressing Scheme
       </li>
       <li>
        Designing DHCP Implementation
       </li>
       <li>
        Designing DHCP Configuration Options
       </li>
      </ul>
      <h2>
       Designing Routing and Switching
      </h2>
      <ul>
       <li>
        Preparing for Designing a Network Routing Topology
       </li>
       <li>
        Selecting Network Devices
       </li>
       <li>
        Designing Internet Connectivity and Perimeter Networks
       </li>
       <li>
        Designing Routing Communications
       </li>
       <li>
        Evaluating Network Performance
       </li>
      </ul>
      <h2>
       Designing Security for Internal Networks
      </h2>
      <ul>
       <li>
        Designing Windows Firewall Implementation
       </li>
       <li>
        Overview of IPSec
       </li>
       <li>
        Designing IPSec Implementation
       </li>
      </ul>
      <h2>
       Designing Name Resolution
      </h2>
      <ul>
       <li>
        Collecting Information for a Name Resolution Design
       </li>
       <li>
        Designing a DNS Server Strategy
       </li>
       <li>
        Designing a DNS Namespace
       </li>
       <li>
        Designing DNS Zone Implementation
       </li>
       <li>
        Designing Zone Replication and Delegation
       </li>
      </ul>
      <h2>
       Designing Advanced Name Resolution
      </h2>
      <ul>
       <li>
        Planning and Deploying the Application Virtualization Management System
       </li>
       <li>
        Designing DNS for High Availability
       </li>
       <li>
        Designing a WINS Name Resolution Strategy
       </li>
      </ul>
      <h2>
       PHP Syntax
      </h2>
      <ul>
       <li>
        Gathering Data for Designing Network Access Solutions
       </li>
       <li>
        Securing and Controlling Network Access
       </li>
       <li>
        Designing Remote Access Services
       </li>
       <li>
        Designing RADIUS Authentication with Network Policy Services
       </li>
      </ul>
      <h2>
       Designing Network Access Protection
      </h2>
      <ul>
       <li>
        Designing the NAP Platform Architecture
       </li>
       <li>
        NAP Architecture
       </li>
       <li>
        Designing NAP Policy
       </li>
      </ul>
      <h2>
       Designing Operating System Deployment and Maintenance
      </h2>
      <ul>
       <li>
        Determining Operating System Deployment Requirements
       </li>
       <li>
        Deploying an Operating System by Using WDS
       </li>
       <li>
        Planning for the Creation and Modification of Images
       </li>
      </ul>
      <h2>
       Designing File Services and DFS in Windows Server 2008
      </h2>
      <ul>
       <li>
        Designing File Services
       </li>
       <li>
        Designing DFS
       </li>
       <li>
        Designing the FSRM Configuration
       </li>
      </ul>
      <h2>
       Designing Print Services in Windows Server 2008
      </h2>
      <ul>
       <li>
        Overview of a Print Services Design
       </li>
       <li>
        Windows Server 2008 Printing Features
       </li>
       <li>
        Designing Print Services
       </li>
      </ul>
      <h2>
       Designing an Active Directory Forest Infrastructure in Windows Server 2008
      </h2>
      <ul>
       <li>
        Determining Active Directory Forest Design Requirements
       </li>
       <li>
        Designing an Active Directory Forest
       </li>
       <li>
        Designing Active Directory Forest Trusts
       </li>
       <li>
        Managing the Active Directory Schema
       </li>
       <li>
        Understanding Windows Time Service
       </li>
      </ul>
      <h2>
       Designing an Active Directory Domain Infrastructure in Windows Server 2008
      </h2>
      <ul>
       <li>
        Designing Active Directory Domains
       </li>
       <li>
        Designing the Integration of DNS Namespaces
       </li>
       <li>
        Designing the Placement of Domain Controllers
       </li>
       <li>
        Designing Active Directory Domain Trusts
       </li>
      </ul>
      <h2>
       Designing Active Directory Sites and Replication in Windows Server 2008
      </h2>
      <ul>
       <li>
        Designing Active Directory Sites
       </li>
       <li>
        Designing AD DS Replication
       </li>
       <li>
        Designing the Placement of Domain Controllers in AD DS Sites
       </li>
      </ul>
      <h2>
       Designing Active Directory Domain Administrative Structures in Windows Server 2008
      </h2>
      <ul>
       <li>
        Planning an Active Directory Domain Administrative Structure
       </li>
       <li>
        Designing Organizational Unit Structures
       </li>
       <li>
        Designing an Active Directory Group Strategy
       </li>
       <li>
        Planning for User and Computer Account Management
       </li>
      </ul>
      <h2>
       Designing Active Directory Group Policy in Windows Server 2008
      </h2>
      <ul>
       <li>
        Gathering Information for Group Policy Deployment
       </li>
       <li>
        Designing Group Policy Settings
       </li>
       <li>
        Designing Group Policy Processing
       </li>
       <li>
        Designing Group Policy Management
       </li>
      </ul>
      <h2>
       Designing AD DS Security in Windows Server 2008
      </h2>
      <ul>
       <li>
        Designing Active Directory Security Policies
       </li>
       <li>
        Designing Active Directory Domain Controller Security
       </li>
       <li>
        Designing Administrator Security and Delegation
       </li>
      </ul>
      <h2>
       Designing Active Directory High Availability in Windows Server 2008
      </h2>
      <ul>
       <li>
        Understanding AD DS High Availability
       </li>
       <li>
        Designing an AD DS High Availability Strategy
       </li>
      </ul>
      <h2>
       Designing an AD DS High Availability Strategy
      </h2>
      <ul>
       <li>
        Designing an Active Directory Database Maintenance Strategy
       </li>
       <li>
        Designing an Active Directory Backup and Recovery Strategy
       </li>
       <li>
        Designing an AD DS Monitoring Strategy
       </li>
      </ul>
      <h2>
       Designing Public Key Infrastructure in Windows Server 2008
      </h2>
      <ul>
       <li>
        Overview of PKI and Active Directory Certificate Services
       </li>
       <li>
        Designing a Certification Authority Hierarchy
       </li>
       <li>
        Designing Certificate Template
       </li>
       <li>
        Designing Certificate Distribution and Revocation
       </li>
      </ul>
      <h2>
       Designing an AD RMS Infrastructure in Windows Server 2008
      </h2>
      <ul>
       <li>
        Gathering Information for an Active Directory Rights Management Services (AD RMS) Design
       </li>
       <li>
        Designing AD RMS Clusters and Access
       </li>
       <li>
        Designing AD RMS Backup and Recovery
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="mcitptraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="88 + 41 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="129">
       <input name="url" type="hidden" value="/mcitptraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>