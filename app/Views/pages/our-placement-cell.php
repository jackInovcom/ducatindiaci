<?php echo view("includes/header.php"); ?>
<section id="breadCrumb">
            
          <div class="container">
            
              <div class="row">
                
                  <div class="col-md-6">
                    
                      <h3>Our Placement Cell</h3>

                  </div> <!-- End Of Col MD 6 -->
                
                  <div class="col-md-6 text-right">
                    
                      <a href="">Home</a> / <a href="">Our Placement Cell</a>

                  </div> <!-- End Of Col MD 6 -->

              </div> <!-- End Of Row -->

          </div> <!-- End OF Container -->

      </section> <!-- End Of Bread Crumb -->

      <section id="mainArea">
            
          <div class="container">
            
              <div class="row">
                
                  <div class="col-md-12">

                    <div class="detailsStyle">

                      <p>In today’s time when IT industry is going through the phase of cut throat competition in job’s market, students prefer to join institutes that offer them job’s guarantee and campus selection. This may be because the fresher’s job are limited in the job market. Most organizations want at least one year trained person. Under such circumstances, campus selection appears very important to students.</p>

                      <p>Most institutes, which offer short term courses to their students for six months or less than six months cannot offer job guarantee to their students. Some institutes also make false promises to lure students and students have to suffer in the end.
It is usually not possible for small institutes to offer job opportunities to their students because of the lesser number of students that they train in a short term course and therefore lack of business partnerships with good organizations. Another reason for not offering job opportunities to the students by small training institutes is the quality of education they impart to their students. Because of lack of funds due to lesser number of students, they fail to hire better teaching faculty for their courses.</p>
                      
                      <p>Ducat is a very big training institute which has multiple training centers all around India. Due to its big size, well trained faculty members, business partnerships with number of companies, and job placement cell of its own, it ensures 100% placement guarantee to its students for both short and long term courses.
More than 50 Indian as well as multi-national companies participate in the job fair organized by Ducat each year. Thousands of Ducat students as well as non-Ducat students participate in the job fair to get jobs. Thousands of students have already been placed and are working efficiently in company’s which picked them through campus selection and job fair.

</p>
                      
                      <p>The placement cell of Ducat is very active. The job opportunities are displayed on the website itself to encourage students to apply for their best suited job. The students have bright chances of selection because they are trained in a professional manner on live industrial projects. 
Ducat has its Facebook page, where people can directly communicate with Ducat and get their questions answered, give feedback to the institute and rate its faculty member and services. This is a big advantage to students in today’s time when social media use is at its peak to exchange the information and feedback to any organization.</p>

                    </div>

                      <div class="text-center">
                          <h2>UPCOMING EVENTS</h2>
                          <div class="separator short"><div class="separator_line"></div></div>  
                      </div>

                      <br/><br/>
                                             
                     

                      <div class="text-center">
                          <h2>ACCOMPLISHED EVENTS</h2>
                          <div class="separator short"><div class="separator_line"></div></div>  
                      </div>

                      <br/><br/>
                                              <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DEVEOPS</h4>

                                  <h6><span>ADDVERB </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IT DevOps Engineer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech (CS/IT) (60% THROUGHOUT</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> DEVEOPS</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DEVEOPS</h4>

                                  <h6><span>TAGNPIN WEB SOLUTIONS LLP </span> <i class="fa fa-map-marker" aria-hidden="true"></i> New Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> DEVEOPS ENGINEER</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> DEVEOPS</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>TAGNPIN WEB SOLUTIONS LLP </span> <i class="fa fa-map-marker" aria-hidden="true"></i> New Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing Executive</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR GRAPHIC DESIGNING</h4>

                                  <h6><span>TAGNPIN WEB SOLUTIONS LLP </span> <i class="fa fa-map-marker" aria-hidden="true"></i> New Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> GRAPHIC DESIGNER</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> GRAPHIC DESIGNING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR HR</h4>

                                  <h6><span>TAGNPIN WEB SOLUTIONS LLP </span> <i class="fa fa-map-marker" aria-hidden="true"></i> New Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> HR RECRUITER</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> FRESHERS/EXP</h5>
                                
                                  <h6><span>Technology:</span> HR </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>TECTREE SOFTWARE CONSULTING PVT LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> JAVA DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> JAVA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR LINUX</h4>

                                  <h6><span>ADDVERB </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IT Support Engineer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech (CS/IT) (60% THROUGHOUT</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> LINUX </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>TAGNPIN WEB SOLUTIONS LLP </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> WEB DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> ANY GRADUATE</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> WEB DESIGNING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR ANDROID CANDIDATES IN DUCAT GURUGRAM ON 03’ OCT, 2019</h4>

                                  <h6><span>SSOFTWARES PVT LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> ANDROID Developer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> ANDROID</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR DIGITAL MARKETING CANDIDATES IN DUCAT GURUGRAM ON 03’ OCT, 2019</h4>

                                  <h6><span>SSOFTWARES PVT LTD </span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> SEO</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR IOS CANDIDATES IN DUCAT GURUGRAM ON 03’ OCT, 2019</h4>

                                  <h6><span>SSOFTWARES PVT LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> IOS</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR PHP CANDIDATES IN DUCAT GURUGRAM ON 03’ OCT, 2019</h4>

                                  <h6><span>SSOFTWARES PVT LTD </span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR JAVA CANDIDATES IN DUCAT NOIDA ON (27 Sep’19)</h4>

                                  <h6><span> iSocietyManager</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java Developer</h5>
                                
                                  <h6><span>Criteria:</span> BE/ BTech/ MCA/ MSc. Through Out 60% marks in 10th 12th & Graduation</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Java </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR PHP CANDIDATES IN DUCAT NOIDA ON (27 Sep’19)</h4>

                                  <h6><span>iSocietyManager</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> BE/ BTech/ MCA/ MSc. Through Out 60% marks in 10th 12th & Graduation</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR PYTHON CANDIDATES IN DUCAT NOIDA ON (27 Sep’19)</h4>

                                  <h6><span>iSocietyManager</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Python Developer </h5>
                                
                                  <h6><span>Criteria:</span> BE/ BTech/ MCA/ MSc. Through Out 60% marks in 10th 12th & Graduation</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Python </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID</h4>

                                  <h6><span>SSOFTWARES PVT LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> ANDROID Developer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> ANDROID</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR BUSINESS ANALYST</h4>

                                  <h6><span>ANR Software Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Business Analyst</h5>
                                
                                  <h6><span>Criteria:</span> BCA/B.Tech/MCA/M.tech</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-3Yr</h5>
                                
                                  <h6><span>Technology:</span> JAVA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>SSOFTWARES PVT LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> SEO</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR EMBEDDED</h4>

                                  <h6><span>Neotech Systems Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Embedded System Engineer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/BE/MCA/BCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1Yrs</h5>
                                
                                  <h6><span>Technology:</span> Embedded</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS</h4>

                                  <h6><span>SSOFTWARES PVT LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> IOS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Servosys Solutions</span> <i class="fa fa-map-marker" aria-hidden="true"></i> New Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech / B.E / MCA(CS/IT)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Sri Aurobindo Society</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Ghaziabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1-3yrs</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>SSOFTWARES PVT LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR AWS CANDIDATES IN DUCAT NOIDA ON 20th SEP, 2019</h4>

                                  <h6><span>TRAVELKHANA.COM</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA	</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> DevOps Engineer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0 TO 5 Year </h5>
                                
                                  <h6><span>Technology:</span> AWS/DevOps </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR TESTING CANDIDATES IN DUCAT NOIDA ON 20th SEP, 2019</h4>

                                  <h6><span>TRAVELKHANA.COM</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA	</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> QA Trainee</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> FRESHER’S</h5>
                                
                                  <h6><span>Technology:</span>  TESTING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR AUTOCAD</h4>

                                  <h6><span>Dimension India Networks (P) Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Junior CAD Engineer</h5>
                                
                                  <h6><span>Criteria:</span> BE/B.Tech /Diploma(Civil, Mechanical, Electrical)     </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> AUTOCAD</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR DIGITAL MARKETING</h4>

                                  <h6><span>Aurelius Corporate Solutions (P) Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Management Trainee</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR IOS</h4>

                                  <h6><span>KareXpert Technologies Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/B.E. and M.Tech/M.E(IT & CS)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> IOS</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR MEANSTACK</h4>

                                  <h6><span>KareXpert Technologies Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/B.E. and M.Tech/M.E(IT & CS)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Mean Stack</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR ORCALE</h4>

                                  <h6><span>Eastern Software Systems Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> SOFTWARE ENGINEER</h5>
                                
                                  <h6><span>Criteria:</span> Btech/BCA/MCA(CS/IT/EC)     </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Oracle DBA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR TESTING</h4>

                                  <h6><span>KareXpert Technologies Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> QA</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/B.E. and M.Tech/M.E(IT & CS)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> TESTING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR .Net CANDIDATES IN DUCAT NOIDA ON 13 th  Sep,2019</h4>

                                  <h6><span>Stout Web</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Ghaziabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> DOT NET DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> DOT NET</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   12                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR JAVA</h4>

                                  <h6><span>KareXpert Technologies Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> JAVA Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/B.E. and M.Tech/M.E(IT & CS)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> JAVA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR JAVA Developer </h4>

                                  <h6><span>Agami Technologies Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> greater Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Developer </h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate/Post-Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1year</h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR Python Developer </h4>

                                  <h6><span>Agami Technologies Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> greater Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Developer  </h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate/Post-Graduate </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>   0-1 years</h5>
                                
                                  <h6><span>Technology:</span> python</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR DATA SCIENCE</h4>

                                  <h6><span>Optimal virtual Employee</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Data Science Trainee</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Data Science </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR  Digital Marketing  </h4>

                                  <h6><span>W3villa Technologies Private Limited</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing Executive </h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate/Post-Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR MEANSTACK</h4>

                                  <h6><span>Opalina Technologies Private Limited</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Meanstack developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/BE/MCA (2018 and 2019 pass out only)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s         </h5>
                                
                                  <h6><span>Technology:</span> Meanstack</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR DATA SCIENCE</h4>

                                  <h6><span>Optimal virtual Employee</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Data Science Trainee</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Data Science </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR CCNA</h4>

                                  <h6><span>KMG Infotech Limited</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Network Engineer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> CCNA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR DOT NET</h4>

                                  <h6><span>Tender News </span> <i class="fa fa-map-marker" aria-hidden="true"></i> New Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> DOT NET DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> MCA/ BCA /B.Tech in Computer Science.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> DOT NET</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR PHP CANDIDATES </h4>

                                  <h6><span>DIGITAL NINZA PVT. LTD.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> FARIDABAD</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR ANGULAR</h4>

                                  <h6><span>Humanitics Dimensions Software Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> New Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Angular DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate/Post-Graduate </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Angular</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR SALESFORCE</h4>

                                  <h6><span>HIC Global Solutions</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Salesforce Trainee</h5>
                                
                                  <h6><span>Criteria:</span> MCA/ BCA /B.Tech /B.E/BSC Any Specialization</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Salesforce</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR CCNA</h4>

                                  <h6><span>Tech Mahindra</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> ASSOCIATE TECHNICAL SUPPORT</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate/Post-Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> CCNA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>InnovationM Technology Solution</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing Trainee</h5>
                                
                                  <h6><span>Criteria:</span> BCA/MCA/BTECH</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>STJ Electronics Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> New Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot Net Developer       </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech /M.Tech/ MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-2 Yrs</h5>
                                
                                  <h6><span>Technology:</span> DOT NET</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR LINUX</h4>

                                  <h6><span>Spectrum Talent Management</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> LINUX Admin</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 2-5yrs</h5>
                                
                                  <h6><span>Technology:</span> linux</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ORACLE DBA</h4>

                                  <h6><span>STJ Electronics Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> New Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Engineer – Remote Support (SW)</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech /M.Tech/ MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-2Yr</h5>
                                
                                  <h6><span>Technology:</span> Oracle DBA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Hatch Concepts</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR ORACLE DBA</h4>

                                  <h6><span>Acutec Global Servies </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Technical Support Engineer </h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Oracle DBA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANGULAR</h4>

                                  <h6><span>CodeBlock Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Frontend Developers</h5>
                                
                                  <h6><span>Criteria:</span> BE/B.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-2 Yrs</h5>
                                
                                  <h6><span>Technology:</span> Angular</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR MEANSTACK</h4>

                                  <h6><span>CodeBlock Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Backend Developers</h5>
                                
                                  <h6><span>Criteria:</span> BE/B.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-2 Yrs</h5>
                                
                                  <h6><span>Technology:</span> Meanstack</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ORACLE DBA</h4>

                                  <h6><span>Acutec Global Servies LLP.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Technical Support Engineer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech IT,CS/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Oracle DBA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Designoweb Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Woodpecker Technologies Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> New Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher/Exp</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR REACT NATIVE</h4>

                                  <h6><span>INNOVATIONM</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> React Native Developers</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> React Native </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>AUG</span>
                                   28                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR DOTNET</h4>

                                  <h6><span>Anstel Private Limited</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> DOT NET DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> DOT NET</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>AUG</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE .NET CANDIDATES</h4>

                                  <h6><span>RV solutions pvt ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .NET Developer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> .NET</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>AUG</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE Meanstack</h4>

                                  <h6><span>RV solutions pvt ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Developer  </h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Mean Stack</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>AUG</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR PHP CANDIDATES </h4>

                                  <h6><span>RV solutions pvt ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>AUG</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR TESTING  CANDIDATES </h4>

                                  <h6><span>RV solutions pvt ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> QA</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> TESTING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS</h4>

                                  <h6><span>QUAINTECH IT PVT LTD     </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> BE/B.TECH/BCA/MCA       </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0 TO 1 YEAR        </h5>
                                
                                  <h6><span>Technology:</span> IOS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA </h4>

                                  <h6><span>BPAAS </span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> JAVA RESOURSE   </h5>
                                
                                  <h6><span>Criteria:</span> BE/B.TECH/BCA/MCA       </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0 TO 1 YEAR        </h5>
                                
                                  <h6><span>Technology:</span>  JAVA     </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA DEVELOPER</h4>

                                  <h6><span>OUAINTEC IT PVT LTD        </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> JAVA DEVELOPER      </h5>
                                
                                  <h6><span>Criteria:</span> BE/B.TECH/BCA/MCA        </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0 TO 1 YEAR         </h5>
                                
                                  <h6><span>Technology:</span> JAVA DEVELOPER</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID </h4>

                                  <h6><span>OUAINTECH IT PVT LTD          </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> ANDRIOD APPLICATION DEVELOPER       </h5>
                                
                                  <h6><span>Criteria:</span> BE/B.TECH/BCA/MCA        </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0 TO 1 YEAR        </h5>
                                
                                  <h6><span>Technology:</span>  ANDROID        </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   16                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ERP (FINANCE)</h4>

                                  <h6><span>Envecon Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida / Mumbai / Bangalore</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> ERP IMPLEMENTATION CONSULTANT</h5>
                                
                                  <h6><span>Criteria:</span> MBA (Finance) /M.Com/MBA (Finance)/CA/CWAdge</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1 – 5 years of total experience with thorough knowle in ERP Product and or ERP implementation</h5>
                                
                                  <h6><span>Technology:</span>  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   16                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR Business Intelligence ( BI )</h4>

                                  <h6><span>Algoworks Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Ghaziabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Business Intelligence Developer</h5>
                                
                                  <h6><span>Criteria:</span> BE/B.Tech/ M.Tech/MCA (60% throw out)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span>  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   16                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR HADOOP</h4>

                                  <h6><span>Entrada Infospace</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Developer</h5>
                                
                                  <h6><span>Criteria:</span> BCA/B.Tech/MCA/Mtech</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Hadoop</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                    OPENING FOR ANDROID</h4>

                                  <h6><span>E Com Shipping Solutions (P) ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android Application Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA/Diploma Holder</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1+Yrs</h5>
                                
                                  <h6><span>Technology:</span> Andriod</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR LINUX</h4>

                                  <h6><span>MphRx (My Personal Health Records Express)</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurugram</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Technical	 Services Engineer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1Yr</h5>
                                
                                  <h6><span>Technology:</span>  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                    OPENING FOR DOT NET</h4>

                                  <h6><span>E Com Shipping Solutions (P) ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> ASP. NET/Windows Application Develop</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 2+Yrs</h5>
                                
                                  <h6><span>Technology:</span> DOT NET</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                    OPENING FOR DOT NET</h4>

                                  <h6><span>ATLANTA SYSTEMS PVT. LTD.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Asp.Net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech,MBA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1+Yrs</h5>
                                
                                  <h6><span>Technology:</span> Dot Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>      OPENING FOR ORACLE DBA</h4>

                                  <h6><span>ATLANTA SYSTEMS PVT. LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> -                       DATABASE ADMINISTRATOR</h5>
                                
                                  <h6><span>Criteria:</span> Any Grauduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1+Yrs</h5>
                                
                                  <h6><span>Technology:</span> Oracle DBA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                    OPENING FOR TESTING</h4>

                                  <h6><span>ANR Software Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> ANR Software Pvt Ltd</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> QA Engineer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-3Yrs</h5>
                                
                                  <h6><span>Technology:</span> Testing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                     OPENING FOR ANGULAR</h4>

                                  <h6><span>CodeBlock Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Angular Developers</h5>
                                
                                  <h6><span>Criteria:</span> BE/B.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-2 Yrs</h5>
                                
                                  <h6><span>Technology:</span> Angular</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR NODE</h4>

                                  <h6><span>CodeBlock Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Node JS Developers</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1-2 Yrs</h5>
                                
                                  <h6><span>Technology:</span> Node JS</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Ripen Apps Technology</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 6Month-2Yrs</h5>
                                
                                  <h6><span>Technology:</span>  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>Neoark Software DRIVE FOR Junior iOS IN Ghaziabad ON (5th July ’19)</h4>

                                  <h6><span>Neoark Software Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Developer  </h5>
                                
                                  <h6><span>Criteria:</span>   Any Graduate/Post-Graduate (CS ONLY)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-6 year</h5>
                                
                                  <h6><span>Technology:</span> Junior IOS</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR JAVA IN DUCAT NOIDA ON (5th JULY ’19)</h4>

                                  <h6><span>Ksolves India Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/BCA/M.Tech/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>Neoark Software DRIVE FOR PHP Developer IN Ghaziabad ON (5th July ’19)</h4>

                                  <h6><span>Neoark Software Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Developer  </h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate/Post-Graduate </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-6 year</h5>
                                
                                  <h6><span>Technology:</span> PHP Developer</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR PYTHON IN DUCAT NOIDA ON (5th JULY ’19)</h4>

                                  <h6><span>Ksolves India Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/BCA/M.Tech/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>                Fresher</h5>
                                
                                  <h6><span>Technology:</span>  Python</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>Neoark Software DRIVE FOR Web Designer IN Ghaziabad ON (5th July ’19)</h4>

                                  <h6><span>Neoark Software Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate/Post-Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-6 year</h5>
                                
                                  <h6><span>Technology:</span> Web Designer</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>Neoark Software DRIVE FOR Web Designer IN Ghaziabad ON (5th July ’19)</h4>

                                  <h6><span>Neoark Software Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate/Post-Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-6 year</h5>
                                
                                  <h6><span>Technology:</span> Web Designer</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>WEB TREE HUB PVT LTD       </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA   </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing Executive    </h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate      </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1.5 years        </h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing       </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>ACRO TECHNOLOGIES (INDIA) PVT LTD        </span> <i class="fa fa-map-marker" aria-hidden="true"></i>  NOIDA                  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Asp.Net Developer           </h5>
                                
                                  <h6><span>Criteria:</span> B.TECH/M.CA/BE</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-5 years</h5>
                                
                                  <h6><span>Technology:</span> Dot Net   </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JULY</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR SEO</h4>

                                  <h6><span>MBA Universe.Com         </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> SEO Executive           </h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher</h5>
                                
                                  <h6><span>Technology:</span> SEO Executive         </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   28                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>PCAMPUS DRIVE FOR JAVA IN GREATER NOIDA ON (28th June  ’19)</h4>

                                  <h6><span>UE Developers </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida/Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Developer  </h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate/Post-Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-6 year</h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   28                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR PHP IN GREATER NOIDA ON (28th June  ’19)</h4>

                                  <h6><span>UE Developers </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida/Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Developer  </h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate/Post-Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-6 year</h5>
                                
                                  <h6><span>Technology:</span>  PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Ripen Apps Technology</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0.6-1Yr</h5>
                                
                                  <h6><span>Technology:</span>  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>KSolves India Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span>  Testers</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span>  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR SEO</h4>

                                  <h6><span>Keyword India Network Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Ghaziabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> SEO & GOOGLE ADWORDS</h5>
                                
                                  <h6><span>Criteria:</span> SEO & GOOGLE ADWORDS</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span>  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   25                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ORACLE</h4>

                                  <h6><span>Oracle DBA</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Oracle Database Administration</h5>
                                
                                  <h6><span>Criteria:</span> BE/B.Tech/MCA (Service Agreement – 2.5 years)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1 years</h5>
                                
                                  <h6><span>Technology:</span> Oracle DBA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   25                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR AUTOCAD(Mechanical)</h4>

                                  <h6><span>Sona Core LLP</span> <i class="fa fa-map-marker" aria-hidden="true"></i> New Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Design Engineer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech in Mechanical </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-2Yr</h5>
                                
                                  <h6><span>Technology:</span> AutoCAD</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID</h4>

                                  <h6><span>Wow Services</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech, MCA BCA.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>    0-1 years                      </h5>
                                
                                  <h6><span>Technology:</span> ANDROID</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANGULAR</h4>

                                  <h6><span>Wow Services</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Angular DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech, MCA BCA.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>   0-1 years</h5>
                                
                                  <h6><span>Technology:</span> Angular</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>  Candour Systems</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> SOFTWARE ENGINEER</h5>
                                
                                  <h6><span>Criteria:</span> B.E/B.Tech, MCA, M.E/M.Tech , bca</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  FRESHERS</h5>
                                
                                  <h6><span>Technology:</span>  Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Sahyog Software Consulting LLP</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech, MCA BCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1 years</h5>
                                
                                  <h6><span>Technology:</span>  PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>Wow Services</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> QA </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech, MCA BCA.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1 years</h5>
                                
                                  <h6><span>Technology:</span> TESTING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>-        Indigram Labs Foundation</span> <i class="fa fa-map-marker" aria-hidden="true"></i> -                         Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> -               B.E/B.Tech/ MC/ BCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 3-5 YEARS</h5>
                                
                                  <h6><span>Technology:</span> -                   PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>Netcomm Labs Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> DOT NET DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> B.E/B.Tech/ MC/ BCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-4 Years</h5>
                                
                                  <h6><span>Technology:</span> DOT NET</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   29                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR DOT NET CANDIDATES IN DUCAT NOIDA ON 31st MAY,2019</h4>

                                  <h6><span>Vinculum Solutions</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot Net Developer       </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/MCA/M.Tech pass out in 2018 and 2019 (throughout 60% marks in academic with no backlog)      </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s/ Experience</h5>
                                
                                  <h6><span>Technology:</span> Dot Net Developer       </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   29                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR JAVA CANDIDATES IN DUCAT NOIDA ON 31st MAY,2019</h4>

                                  <h6><span>Vinculum Solutions</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java Developer       </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/MCA/M.Tech pass out in 2018 and 2019 (throughout 60% marks in academic with no backlog) </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s/ Experience</h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   28                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR MEAN STACK</h4>

                                  <h6><span>WEBBOOZA TECHNOLOGIES PVT LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Mean Stack Developer</h5>
                                
                                  <h6><span>Criteria:</span> BTech/ BE/ MCA/BCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Mean Stack</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   28                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>WEBBOOZA TECHNOLOGIES PVT LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP</h5>
                                
                                  <h6><span>Criteria:</span> BTech/ BE/ MCA/BCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   28                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>WEBBOOZA TECHNOLOGIES PVT LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designer</h5>
                                
                                  <h6><span>Criteria:</span> BTech/ BE/ MCA/BCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Web Designer</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   28                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>CONTATA SOLUTION PVT.LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Trainee- Software Developer</h5>
                                
                                  <h6><span>Criteria:</span> BE/B.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Asp.net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   28                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR LINUX</h4>

                                  <h6><span>Capgemini Technology Services India Limited</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Linux</h5>
                                
                                  <h6><span>Criteria:</span> BCA/ BE Computer Sciences fresher (Red had Certification)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> linux</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   28                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>NovelVox</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech -CS or IT  /MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Enthuons Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span>   PHP developer</h5>
                                
                                  <h6><span>Criteria:</span> BCA/ MCA/ B.Tech</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>Elixir Softech Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> QA</h5>
                                
                                  <h6><span>Criteria:</span> Bachelor’s degree in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> TESTING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   21                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANGULAR</h4>

                                  <h6><span>Moptra Infotech Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> ANGULAR</h5>
                                
                                  <h6><span>Criteria:</span> Graduate (BCA/B.Tech CS branch) OR Any Post Graduate (M.Tech or MCA)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span>  ANGULAR </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   21                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Moptra Infotech Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> JAVA</h5>
                                
                                  <h6><span>Criteria:</span> BCA/B.Tech ,M.Tech or MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   21                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>  OPENING FOR REACT JS</h4>

                                  <h6><span>Moptra Infotech Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> React js</h5>
                                
                                  <h6><span>Criteria:</span> Graduate (BCA/B.Tech CS branch) OR Any Post Graduate (M.Tech or MCA)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> React JS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Hexagon Digital Labs Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Intern- Web Development</h5>
                                
                                  <h6><span>Criteria:</span>    B.Tech/ MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> FRESHERS</h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   16                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ORACLE DBA</h4>

                                  <h6><span>Infocare Digital System Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> DELHI</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> ORACLE DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-4 YEARS</h5>
                                
                                  <h6><span>Technology:</span> ORACLE</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS</h4>

                                  <h6><span>FutureSoft India Private Limited</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech -  2018/2019 batch</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 6 months to 2 year</h5>
                                
                                  <h6><span>Technology:</span> IOS</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   07                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID</h4>

                                  <h6><span>Trinity Mobile App Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> ANDROID DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> BE/B.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> ANDROID</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR Java</h4>

                                  <h6><span>Source Soft Solutions Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Engineer.</h5>
                                
                                  <h6><span>Criteria:</span> BE/B.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR AWS</h4>

                                  <h6><span>Source Soft Solutions Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> AWS Engineer</h5>
                                
                                  <h6><span>Criteria:</span> BE/B.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> AWS</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID</h4>

                                  <h6><span>Augmatiks</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android developer</h5>
                                
                                  <h6><span>Criteria:</span> Bachelor’s degree in IT</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers</h5>
                                
                                  <h6><span>Technology:</span> Android</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>AVSS EFORRM (INDIA) PVT. LTD.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.E/B.Tech, MCA, M.E/M.Tech</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> FRESHERS</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>LeewayHertz</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Full stack developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech in CS / IT / MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers</h5>
                                
                                  <h6><span>Technology:</span>  JAVA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span> Augmatiks</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP developer</h5>
                                
                                  <h6><span>Criteria:</span> Bachelor’s degree in IT</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Freshers</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR SEO</h4>

                                  <h6><span>Nurturing Green</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> SEO intern</h5>
                                
                                  <h6><span>Criteria:</span> ANY GRADUATE</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers</h5>
                                
                                  <h6><span>Technology:</span> SEO</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR JAVA IN DUCAT NOIDA ON (5th APRIL ’19)</h4>

                                  <h6><span>Ksolves India Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/BCA/M.Tech/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> JAVA </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Invenio Business Solutions Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> SAP Trainee</h5>
                                
                                  <h6><span>Criteria:</span> B. Tech/B.E (CS/IT/EC) /MCA (2017/2018 pass outs will be preferred)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Quy Technology Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurugram</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Trainee Developer </h5>
                                
                                  <h6><span>Criteria:</span> MCA /MSC /B.Tech CS /IT/ ECE 2018 and 2019 Batch</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   16                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR  JAVA</h4>

                                  <h6><span>In2 IT TECHNOLOGY</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.E/B.Tech, MCA, M.E/M.Tech btech, bca</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> FRESHERS</h5>
                                
                                  <h6><span>Technology:</span> Java </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   16                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>Webtel Electrosoft Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Developer</h5>
                                
                                  <h6><span>Criteria:</span> BE/ B.tech/ MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1 years</h5>
                                
                                  <h6><span>Technology:</span> DOT NET</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Acezd Consultancy Services Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> New Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> New Delhi</h5>
                                
                                  <h6><span>Criteria:</span> MCA/ MTech / B.Tech / BE (CS/IT) / BSc (CS) / BCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PYTHON</h4>

                                  <h6><span>Invenio Business Solutions Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> SAP Trainee</h5>
                                
                                  <h6><span>Criteria:</span> B. Tech/B.E (CS/IT/EC) /MCA (2017/2018 pass outs will be preferred</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PYTHON</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS </h4>

                                  <h6><span>MobileCoderz Technologies Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech graduate of CS/IT background or MCA students</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s/ Experience</h5>
                                
                                  <h6><span>Technology:</span> IOS</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>Goranga Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Products Executive</h5>
                                
                                  <h6><span>Criteria:</span> BTECH(CS/IT/EC)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> FRESHERS</h5>
                                
                                  <h6><span>Technology:</span> DOT NET</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>TINSLAB </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> DOT NET DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> BTech/ BE/ MCA/BCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1 YEAR</h5>
                                
                                  <h6><span>Technology:</span> DOT NET</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ERP </h4>

                                  <h6><span>ES 3 India PVT LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> ERP Functional Consultant</h5>
                                
                                  <h6><span>Criteria:</span> BTech/ BE/ MCA/BCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 2 Years’ Experience</h5>
                                
                                  <h6><span>Technology:</span> ERP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB/UI DEVELOPER</h4>

                                  <h6><span>ES 3 India PVT LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Developer/UI Developer</h5>
                                
                                  <h6><span>Criteria:</span> BTech/ BE/ MCA/BCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1</h5>
                                
                                  <h6><span>Technology:</span> WEB/UI DEVELOPER</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR .NET CANDIDATES IN DUCAT Gurugram  ON   (4th  FEB ’19)</h4>

                                  <h6><span>Daikin Airconditioniong India Pvt.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurugram  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .Net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B. Tech preferably in Electrical or Electronics Engineering</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher </h5>
                                
                                  <h6><span>Technology:</span> .Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANGULAR</h4>

                                  <h6><span>CODE5 Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Angular</h5>
                                
                                  <h6><span>Criteria:</span> BTech/ BE/ MCA/BCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  FRESHERS</h5>
                                
                                  <h6><span>Technology:</span> Angular</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>YES IT Labs </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Tester</h5>
                                
                                  <h6><span>Criteria:</span> BTech/ BE/ MCA/BCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> FRESHERS</h5>
                                
                                  <h6><span>Technology:</span> TESTING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR CCNA</h4>

                                  <h6><span>IQ Infotech </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida/Vaishali</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> NOC Engineer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span>   CCNA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANGULAR JS</h4>

                                  <h6><span>Benepik Technology Pvt. Ltd.,</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurugram</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Angular Developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Freshers</h5>
                                
                                  <h6><span>Technology:</span> Angular</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR SEO</h4>

                                  <h6><span>IQ Infotech </span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> SEO Executive</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1 Yrs</h5>
                                
                                  <h6><span>Technology:</span> SEO</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET </h4>

                                  <h6><span>Daikin Airconditioniong India Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurugram </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot Net developer</h5>
                                
                                  <h6><span>Criteria:</span> B. Tech preferably in Electrical or Electronics Engineering </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot Net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID</h4>

                                  <h6><span>Wow Services</span> <i class="fa fa-map-marker" aria-hidden="true"></i> -                     Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech, MCA BCA.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1 years</h5>
                                
                                  <h6><span>Technology:</span> -                 ANDROID</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR DIGITALMARKETING CANDIDATES IN DUCAT NOIDA ON (19th NOV’18)</h4>

                                  <h6><span>IQ INFOTECH & CO</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing Executive</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1Yr</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PYTHON</h4>

                                  <h6><span>CRON Systems</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurugram</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PYTHON  DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  0-4 years</h5>
                                
                                  <h6><span>Technology:</span>  Python</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   12                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>Flexsin</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> DOT NET DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-4 years</h5>
                                
                                  <h6><span>Technology:</span>  DOT NET</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   12                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR SEO</h4>

                                  <h6><span>BizBrolly Solutions </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span>  SEO</h5>
                                
                                  <h6><span>Criteria:</span> ANY GRADUATE</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span>  SEO</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   12                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Suretek  Infosoft Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Engineer</h5>
                                
                                  <h6><span>Criteria:</span>  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher </h5>
                                
                                  <h6><span>Technology:</span> WEB DESIGNING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR MCSA</h4>

                                  <h6><span>ProVal Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Trainee Technical Support</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> MCSA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR C#</h4>

                                  <h6><span>Mansukh Goup</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Developer  </h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> C#</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR MCSA</h4>

                                  <h6><span>ProVal Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Trainee Technical Support</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> MCSA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR SAS</h4>

                                  <h6><span>ShyftPath</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Technical Content Writer </h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> SAS</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Redian Software LLC</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span>        B.Tech./M.C.A</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-2 Yrs</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PYTHON</h4>

                                  <h6><span>ShyftPath</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Technical Content Writer </h5>
                                
                                  <h6><span>Criteria:</span>  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PYTHON</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR R-PROGRAMMING</h4>

                                  <h6><span>ShyftPath</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Technical Content Writer </h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> R Programming</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>NOV</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR  LINUX</h4>

                                  <h6><span>DreamSol TeleSolutions Pvt. Ltd.   </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Linux Admin</h5>
                                
                                  <h6><span>Criteria:</span> Bachelors(any stream) / Diploma in Computer Applications</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers</h5>
                                
                                  <h6><span>Technology:</span>  LINUX</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>NOV</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR  PHP</h4>

                                  <h6><span>TechPAPA  Software Development Company </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher/Internship.                             </h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>NOV</span>
                                   21                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>iShore Software Solutions</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing Executive</h5>
                                
                                  <h6><span>Criteria:</span> BTech/ BE/ MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> FRESHER</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>NOV</span>
                                   21                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>Royal Datamatics Pvt. Ltd. </span> <i class="fa fa-map-marker" aria-hidden="true"></i> DELHI</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> DOT NET INTERNS</h5>
                                
                                  <h6><span>Criteria:</span> BTech/ BE/ MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> FRESHERS</h5>
                                
                                  <h6><span>Technology:</span> DOT NET </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>NOV</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID</h4>

                                  <h6><span>INOVOTEQ</span> <i class="fa fa-map-marker" aria-hidden="true"></i> DELHI</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/MCA/BCA/MTECH (CS)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0- 1 years</h5>
                                
                                  <h6><span>Technology:</span>  Android </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>NOV</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR CCNA CANDIDATES IN DUCAT NOIDA ON (19 th NOV’18)</h4>

                                  <h6><span>IQ INFOTECH & CO.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Vaishali</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> NOC Engineer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1Yr</h5>
                                
                                  <h6><span>Technology:</span> CCNA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>NOV</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>Interra Systems</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Engineer/ Automation Test Engineer </h5>
                                
                                  <h6><span>Criteria:</span> BTech/ BE/ MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>    0-3 Years</h5>
                                
                                  <h6><span>Technology:</span> TESTING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR  JAVA</h4>

                                  <h6><span>DreamSol TeleSolutions Pvt. Ltd.   </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  DEVELOPER </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/ MCA with specialised training in Java technologies</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers</h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR  LINUX</h4>

                                  <h6><span>DreamSol TeleSolutions Pvt. Ltd.   </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Linux Admin</h5>
                                
                                  <h6><span>Criteria:</span> Bachelors(any stream) / Diploma in Computer Applications</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers</h5>
                                
                                  <h6><span>Technology:</span> Linux </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR MANUAL TESTING</h4>

                                  <h6><span>DreamSol TeleSolutions Pvt. Ltd.   </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Tester</h5>
                                
                                  <h6><span>Criteria:</span> Bachelors(any stream) / Diploma in Computer Applications</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1 YEARS</h5>
                                
                                  <h6><span>Technology:</span> Manual testing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Kito Infocom Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Website Developer               </h5>
                                
                                  <h6><span>Criteria:</span>  B.Tech, BCA, MCA and above qualifications in C.S. and I.T stream.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-2Yrs</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR SEO</h4>

                                  <h6><span>Kito Infocom Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> SEO Executive</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-2Yrs</h5>
                                
                                  <h6><span>Technology:</span>  SEO</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   12                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR PHP CANDIDATES IN DUCAT GREATER NOIDA ON (12th OCT,2018)</h4>

                                  <h6><span>Ekadrishta Hospitality Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GREATER NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> BTECH,BE /MTECH,MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> FRESHERS</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   11                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR GRAPHIC DESIGNING</h4>

                                  <h6><span>Storidigital</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Graphic Designer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/MCA/BCA/MTECH (CS)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0 to 2 yrs.</h5>
                                
                                  <h6><span>Technology:</span> Graphic Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   11                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR FULLSTACK DEVELOPER</h4>

                                  <h6><span>Storidigital</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> WordPress Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/MCA/BCA/MTECH (CS)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0 to 2 yrs.</h5>
                                
                                  <h6><span>Technology:</span>  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   11                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR ANDROID CANDIDATES IN DUCAT GREATER NOIDA ON (12th OCT,2018)</h4>

                                  <h6><span>Ekadrishta Hospitality Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GREATER NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> ANDROID Developer</h5>
                                
                                  <h6><span>Criteria:</span> BTECH,BE /MTECH,MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> FRESHERS</h5>
                                
                                  <h6><span>Technology:</span> ANDROID</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ORACLE DBA</h4>

                                  <h6><span>Pratyaksh Interactive Solutions Pvt. Ltd.Oracle DBA</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Engineer – Remote Support (SW)</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech /M.Tech/ MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-3yrs</h5>
                                
                                  <h6><span>Technology:</span> Oracle DBA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>Pratyaksh Interactive Solutions Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> DOT NET Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-2 years</h5>
                                
                                  <h6><span>Technology:</span> DOT NET </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET </h4>

                                  <h6><span>Valtech India Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dotnet Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech CSE / IT 2018 and 2019 Batch 65% throughout</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dotnet</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR Testing</h4>

                                  <h6><span>Valtech India Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Testing Engineer </h5>
                                
                                  <h6><span>Criteria:</span> -              B.Tech CSE / IT 2018 and 2019 Batch 65% throughout.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Testing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR CCNA/CCNP</h4>

                                  <h6><span>IDC Technology Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurugram</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Desktop Support (Field Engineer)</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> CCNA/CCNP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Aimil Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/BCA/MCA/M.Tech</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Aimil Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/BCA/MCA/M.Tech</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span>  PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>Innodata India P Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> DOT NET Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/MCA/BCA/MTECH (CS)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 2-2.5yrs </h5>
                                
                                  <h6><span>Technology:</span> DOT NET </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ORACLE DBA</h4>

                                  <h6><span>HCL Technologies  </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida	</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Trainee </h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 2yrs</h5>
                                
                                  <h6><span>Technology:</span> Oracle DBA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ORACLE DBA</h4>

                                  <h6><span>STJ Electronics Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Engineer – Remote Support (SW)</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech /M.Tech/ MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  0-3yrs</h5>
                                
                                  <h6><span>Technology:</span> Oracle DBA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>  InfiCare Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Trainee</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Wyoming Telemedia Services (P) Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA/B.E</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1Yr</h5>
                                
                                  <h6><span>Technology:</span>  PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Coalshastra</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/MCA/BCA/MTECH (CS)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  FRESHER</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Wyoming Telemedia Services (P) Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Wyoming Telemedia Services (P) Ltd</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA/B.E</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Web Designer</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA DEVELOPER</h4>

                                  <h6><span>Adjecti Solutions Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/MCA/BCA/MTECH (Through Out 65%)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>CODERITHUM</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing Executive</h5>
                                
                                  <h6><span>Criteria:</span> B.E/B.Tech, MCA, M.E/M.Tech </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0- 1 YEAR</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>CODERITHUM</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designer & Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.E/B.Tech, MCA, M.E/M.Tech </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0- 1 YEAR</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>Smart Matrix Consultants</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  2yr </h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>KSolves India Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> W</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR SEO</h4>

                                  <h6><span>Smart Matrix Consultants</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> SEO</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 2yr </h5>
                                
                                  <h6><span>Technology:</span> SEO</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR JAVA CANDIDATES IN DUCAT FARIDABAD ON (04th OCT, 2018)</h4>

                                  <h6><span>Nile Technologies Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java Developer</h5>
                                
                                  <h6><span>Criteria:</span> BTECH,BE /MTECH,MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>OCT</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PYTHON</h4>

                                  <h6><span>GNT Online Solutions Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> DELHI</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Python developers </h5>
                                
                                  <h6><span>Criteria:</span> B.E/B.Tech, MCA, M.E/M.Tech </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0- 1 YEAR</h5>
                                
                                  <h6><span>Technology:</span> Python</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   28                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANGULAR JS</h4>

                                  <h6><span>Apparent Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Angular JS Developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 6 Month-1Year</h5>
                                
                                  <h6><span>Technology:</span> Angular JS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   28                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Apparent Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 6 Month-1Year</h5>
                                
                                  <h6><span>Technology:</span>  PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   28                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                     OPENING FOR UI DEVELOPER</h4>

                                  <h6><span>Apparent Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> UI Developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 6 Month-1Year</h5>
                                
                                  <h6><span>Technology:</span> UI</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>SEP</span>
                                   11                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA </h4>

                                  <h6><span>Valtech India Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech CSE / IT 2018 and 2019 Batch 65% throughout.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Java </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR DOT NET CANDIDATES IN DUCAT NOIDA ON </h4>

                                  <h6><span>SEWA ONLINE</span> <i class="fa fa-map-marker" aria-hidden="true"></i> DELHI</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA/B.E</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher</h5>
                                
                                  <h6><span>Technology:</span> Dot net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAY</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR JAVACANDIDATES IN DUCAT NOIDA ON (10th  MAY’18)</h4>

                                  <h6><span>Campus Labs Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> JAVA Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech (CS/IT)/ MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span>  JAVA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR .NET</h4>

                                  <h6><span>Redcliffe Energy Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .Net developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/ MS/MSc</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1 YEARS</h5>
                                
                                  <h6><span>Technology:</span> .Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>JeannyPr Softech Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot Net Developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Dot Net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Apparent Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 6 Month-1Year</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR DOT NET CANDIDATES IN DUCAT NOIDA ON </h4>

                                  <h6><span>Hard Shell Technologies Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span>   B.Tech/M.Tech/BCA/MCA/B.E</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA/B.E</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Dot Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR  JAVA</h4>

                                  <h6><span>  Designboxindia</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Java </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR  JAVA</h4>

                                  <h6><span>Oodles Technologies Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span>     Java Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Java  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>  OPENING FOR ADVANCE EXCEL</h4>

                                  <h6><span>ANR Software Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Trainee/Junior Solution Engineer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-3Yr</h5>
                                
                                  <h6><span>Technology:</span> Advance Excel</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR CCNA</h4>

                                  <h6><span>ANR Software Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Trainee/Junior Solution Engineer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-3Yr</h5>
                                
                                  <h6><span>Technology:</span> CCNA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR LINUX</h4>

                                  <h6><span>Attainet</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Linux Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s /Experience </h5>
                                
                                  <h6><span>Technology:</span> Linux</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ORACLE DBA</h4>

                                  <h6><span>ANR Software Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Oracle Developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Oracle DBA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>ANR Software Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing </h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-3Yr</h5>
                                
                                  <h6><span>Technology:</span> Testing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>  OPENING FOR JAVA </h4>

                                  <h6><span>ANR Software Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-3Yr</h5>
                                
                                  <h6><span>Technology:</span> Java  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>APRL</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR  DOT NET </h4>

                                  <h6><span>Lelogix Software Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida,</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net Developer</h5>
                                
                                  <h6><span>Criteria:</span>  B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0to 1Year </h5>
                                
                                  <h6><span>Technology:</span> .Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR  JAVA</h4>

                                  <h6><span>Cognizione Consulting and solution Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> -           B.Tech/M.Tech/BCA/MCA (2014,2015,2016 &2017</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s/ Experience</h5>
                                
                                  <h6><span>Technology:</span> Java  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANGULAR</h4>

                                  <h6><span>Igniters Hub </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Angular  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech. CS/IT, MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Angular 2.0</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR GRAPHIC DESIGNING</h4>

                                  <h6><span>Igniters Hub </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> UI/UX Designer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech. CS/IT, MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Graphic Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA DEVELOPER </h4>

                                  <h6><span>Igniters Hub </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech. CS/IT, MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Java  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR  JAVA</h4>

                                  <h6><span>Cognizione Consulting and solution Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA (2014,2015,2016 &2017)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s/ Experience</h5>
                                
                                  <h6><span>Technology:</span> Java  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR EMBEDDED</h4>

                                  <h6><span>Atlanta Systems Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Embedded Hardware Engineer Trainee</h5>
                                
                                  <h6><span>Criteria:</span> Diploma Holder in Electronics, B.Tech (ECE/EE/EEE)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Embedded</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                     OPENING FOR IOS DEVELOPER</h4>

                                  <h6><span>I Web Services</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer </h5>
                                
                                  <h6><span>Criteria:</span>  B.tech [CS/IT] & MCA with 60% overall academics</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> IOS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>I Web Services</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech [CS/IT] & MCA with 60% overall academics</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Outright Systems Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech, BCA, B.Sc-Computers (above 65% )</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Freshers</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>ISKPRO Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span>  B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span>  PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR  JAVA</h4>

                                  <h6><span>Cognizione Consulting and solution Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span>   B.Tech/M.Tech/BCA/MCA (2014,2015,2016 &2017)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Experience</h5>
                                
                                  <h6><span>Technology:</span> Java  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR CCNA</h4>

                                  <h6><span>Velocis Systems Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Helpdesk Executive</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span>  CCNA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4> OPENING FOR CCNP</h4>

                                  <h6><span>Velocis Systems Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Helpdesk Executive</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> CCNP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>Infinity Lab</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> Post Graduated in Any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING </h4>

                                  <h6><span>Binates bits Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>OM SOFT SOLUTION </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>       0.6- 3 years</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OM SOFT SOLUTION</h4>

                                  <h6><span>OM SOFT SOLUTION</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0.6- 3 years</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WORDPRESS DEVELOPER</h4>

                                  <h6><span>OM SOFT SOLUTION</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Wordpress Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0.6- 3 years</h5>
                                
                                  <h6><span>Technology:</span> Wordpress</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   23                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                                     OPENING FOR TESTING</h4>

                                  <h6><span>ANR Software Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Testing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   23                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>ANR Software Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Java </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   23                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP PROGRAMMER</h4>

                                  <h6><span>Saga Techsys Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   23                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>ISKPRO Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   22                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>Binates Bits Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   22                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Einfachab Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   22                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Headfield Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   22                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Sagar Informatics Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> .Net DEVELOPER</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   21                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>Mediazotic Solutions</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   21                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Web World Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   21                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Web World Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR  JAVA</h4>

                                  <h6><span>TekMindz</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Java </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID DEVELOPER</h4>

                                  <h6><span>Approrio Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Android </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>GLOBLE PET SET VET</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS </h4>

                                  <h6><span>Apporio  Pvt Ltd .</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Ios </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Red Logics Technologies Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>Hexaview</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Testing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Red Logics Technologies Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA DEVELOPER</h4>

                                  <h6><span>MphRx (My Personal Health Records Express)</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Professional Services Engineer – L-1</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech. CS/IT(2016,2017,2018) Over all Throughout 65% </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span>   Java </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                        OPENING FOR LINUX</h4>

                                  <h6><span>MphRx (My Personal Health Records Express)</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Professional Services Engineer – L-</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher</h5>
                                
                                  <h6><span>Technology:</span> Linux</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ORACLE DBA</h4>

                                  <h6><span>MphRx (My Personal Health Records Express)</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Professional Services Engineer – L-1</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Oracle DBA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PYTHON</h4>

                                  <h6><span>MphRx (My Personal Health Records Express)</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Professional Services Engineer – L-1</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Python</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR  ANDROID</h4>

                                  <h6><span>Qspear Consultancy Services</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech , MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Android </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>Lava International Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR GRAPHIC DESIGNING</h4>

                                  <h6><span>Lava International Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> B.tech/M.tech/BCA/MCA</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Graphic Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS </h4>

                                  <h6><span>Business News and Information Services Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> IOS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Etelligens Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> B.E/B.Tech/ Mca</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Lava International Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA DEVELOPER</h4>

                                  <h6><span>-         Business News and Information Services Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Java  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   16                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR BIG DATA HADOOP</h4>

                                  <h6><span>Decision Point</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Opening for bigdata Hadoop</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Big Data Hadoop</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                     OPENING FOR ORACLE DBA</h4>

                                  <h6><span>Decision Point</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Oracle DBA </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers</h5>
                                
                                  <h6><span>Technology:</span> Oracle DBA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Shipgig Ventures Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>Shipgig Ventures Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Testing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANGULAR JS</h4>

                                  <h6><span>Shipgig Ventures Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Angular  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Angular Js </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ASP DOT NET </h4>

                                  <h6><span>Shipgig Ventures Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> .Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   12                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR  JAVA</h4>

                                  <h6><span>Qspear Consultancy Services</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech , MCA.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s/ Experience</h5>
                                
                                  <h6><span>Technology:</span>  Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   12                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR CCNA</h4>

                                  <h6><span>Attainet</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span>  </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s /Experience </h5>
                                
                                  <h6><span>Technology:</span>  CCNA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   12                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR GRAPHIC DESIGNING</h4>

                                  <h6><span>ABP Holding</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Graphic Designer </h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Graphic Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   12                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR MCSE</h4>

                                  <h6><span>Ingenious e-Brain solutions</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> System Administrator</h5>
                                
                                  <h6><span>Criteria:</span> B.Sc IT, BCA, (Non Btech Candidates)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher </h5>
                                
                                  <h6><span>Technology:</span> MCSE</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   12                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span> v2infotech</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING </h4>

                                  <h6><span>Mathologic Technologies Private Limted</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID DEVELOPER</h4>

                                  <h6><span>Decimal Technologies Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurugram</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Android Developer</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR GRAPHIC DESIGNING</h4>

                                  <h6><span>Smart Matrix Consultants</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Graphic Designing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s or 1 year EXPERIENCED </h5>
                                
                                  <h6><span>Technology:</span> Graphic Designing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS </h4>

                                  <h6><span>-           Etelligens Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> IOS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                     OPENING FOR PHP PROGRAMMER</h4>

                                  <h6><span>Saga Techsys Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Etelligens Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>Etelligens Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Testing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Decimal Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Digital Solutions Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> .Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>ADG Online Solutions Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1 year exp</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR GRAPHIC DESIGNING</h4>

                                  <h6><span>WE bee Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Graphic Designer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Graphic Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ORACLE D2K</h4>

                                  <h6><span>Ace joinings </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Oracle D2k</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Oracle D2K/Developer</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>WE bee Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Instants System</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>ADG Online Solutions Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR SEO</h4>

                                  <h6><span>ADG Online Solutions Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> SEO Executive</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> seo</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Web BEE eSolutions  Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   07                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>A R Software Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Faridabad  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher</h5>
                                
                                  <h6><span>Technology:</span> Dot net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   07                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ADVANCE EXCEL</h4>

                                  <h6><span>Shubham Hydrotch Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Advance Excel </h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher </h5>
                                
                                  <h6><span>Technology:</span>  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   07                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Ezimax Technologies Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   07                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR C& C++</h4>

                                  <h6><span>TriAyaam Vision Labs Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> c &c++</h5>
                                
                                  <h6><span>Criteria:</span> B tech CS/EC/IT/EE(Minimum 65% Marks)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> c& c++</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   07                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID DEVELOPER</h4>

                                  <h6><span>Armantec Systems</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android Developers</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Android </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PYTHON</h4>

                                  <h6><span>I Web Services</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Python Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech [CS/IT] & MCA with 60% overall academics</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Python</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING </h4>

                                  <h6><span>Misha infotech Pvt. Ltd. </span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Lead Generation</h5>
                                
                                  <h6><span>Criteria:</span>  B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR LINUX</h4>

                                  <h6><span>CSS Infotech Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Ghaziabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Linux Engineer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech /MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Linux</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ORACLE DBA</h4>

                                  <h6><span>Acumen Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Mumbai</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Technical Support Engineer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Oracle DBA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Burgeon Software Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech / MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PYTHON</h4>

                                  <h6><span>TransOrg Analytics</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Python Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 2-7 Yr</h5>
                                
                                  <h6><span>Technology:</span> Python</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>Hexaview</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing</h5>
                                
                                  <h6><span>Criteria:</span> B.E/B.Tech/ MC. </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> MANUAL TESTING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID DEVELOPER</h4>

                                  <h6><span>Web Architech Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android Developers</h5>
                                
                                  <h6><span>Criteria:</span> BE/ B.Tech /MCA(2016-2017 Passout)                                </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s and Experience </h5>
                                
                                  <h6><span>Technology:</span> Android </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>CSS Infotech Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Ghaziabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Web Architech Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s </h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS </h4>

                                  <h6><span>Binates Bits  Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Ios </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR UI DEVELOPER</h4>

                                  <h6><span>Saga Infotech Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> UI DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  </h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Abalone  Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> php</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>E3 Event Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Abalone technologies Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> .Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS </h4>

                                  <h6><span>Saga Infotech Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> IOS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR UI</h4>

                                  <h6><span>Lets Go Social</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> UI DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> UI/UX</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>Smart Logics Software Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M/Tech / BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1-2yr Experience</h5>
                                
                                  <h6><span>Technology:</span> Dot Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR MCSA</h4>

                                  <h6><span>Smart Logics Software Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IT Server Maintenance</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1-3yr Experience</h5>
                                
                                  <h6><span>Technology:</span> MCSA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>App Agicent Pvt Ltd  </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>App Agicent  Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR GRAPHIC DESIGNING</h4>

                                  <h6><span>Think Biz Solution</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Graphic Designer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream (Only Male Candidate)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 2-3Yr</h5>
                                
                                  <h6><span>Technology:</span> Graphic Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS </h4>

                                  <h6><span>Binates Bits Pvt Ltd .</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> IOS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Mind Digital Group</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Min 2yr Exp</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Think Biz Solution</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream (Only Male Candidate)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1yr Experience</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Instants System</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/MCA degree ( 2016/2017 pass out only )</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR SEO</h4>

                                  <h6><span>Think Biz Solution</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> SEO Executive</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream (Only Male Candidate)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  1-2Yr</h5>
                                
                                  <h6><span>Technology:</span> SEO</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>AppSierra Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Manual Testing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Think Biz Solution</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> HTML Designer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream (Only Male Candidate)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1Yr</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>GA Technocare Technology</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Global Groupware Solutions</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher’s </h5>
                                
                                  <h6><span>Technology:</span> Java</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR C &C++</h4>

                                  <h6><span>MoMagic Technologies Pvt Ltd. </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> c& c++</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING </h4>

                                  <h6><span>Global pet set vet</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>MoMagic Technologies Pvt Ltd. </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Java  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                           OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Smart Matrix Consultants</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   31                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET </h4>

                                  <h6><span>Holostik India Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   31                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS </h4>

                                  <h6><span>Holostik India Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> IOS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   31                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR MANUAL TESTING</h4>

                                  <h6><span>Stout Web Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Ghaziabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing </h5>
                                
                                  <h6><span>Criteria:</span> B-tech (CS) /MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span>     Manual Testing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   31                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID DEVELOPER</h4>

                                  <h6><span>Armantec Systems</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android Developers</h5>
                                
                                  <h6><span>Criteria:</span> BE/ B.Tech /BCA/MCA                           </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s and Experience                 </h5>
                                
                                  <h6><span>Technology:</span> Android </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   31                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID DEVELOPER</h4>

                                  <h6><span>Holostik India Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android Developers</h5>
                                
                                  <h6><span>Criteria:</span> BE/ B.Tech /BCA/MCA                             </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s and Experience                 </h5>
                                
                                  <h6><span>Technology:</span> Android </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   31                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Armantec Systems</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher’s </h5>
                                
                                  <h6><span>Technology:</span> Java  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   29                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID DEVELOPER</h4>

                                  <h6><span>Decimal Technologies Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Android </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   29                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                     OPENING FOR ERP -SAP</h4>

                                  <h6><span>Vishal Mega Mart Private Limited</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurugram</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Consultant</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> ERP -SAP ABAP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   29                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ADVANCE EXCEL</h4>

                                  <h6><span>Shubham Hydrotch Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Advance Excel </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Advance Excel</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   29                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR LINUX</h4>

                                  <h6><span>Avis e Solutions Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Linux Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Linux</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   29                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Help You Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   29                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>KRS Infotech Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   29                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>KRS Infotech Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> .Net DEVELOPER</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>Web BEE eSolutions  Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Experience</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>Pillar Global</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Experience</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Pillar Global</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Experience</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR UI DESIGNING</h4>

                                  <h6><span>Pillar Global</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> UI DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Experience</h5>
                                
                                  <h6><span>Technology:</span> UI/UX</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID DEVELOPER</h4>

                                  <h6><span>Pillar Global</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Experience</h5>
                                
                                  <h6><span>Technology:</span> Android </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>App Agicent Pvt Ltd  </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Experience</h5>
                                
                                  <h6><span>Technology:</span> Dot net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   25                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ORACLE DEVELOPER</h4>

                                  <h6><span>MM info systems pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Oracle Developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate/MCA/BCA/Diploma in IT.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Minimum 1 yr / Fresher </h5>
                                
                                  <h6><span>Technology:</span> ORACLE </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   25                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Medic Creations Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   25                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Abalone technologies Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   25                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>Abalone Technologies Pvt Ltd .</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Testing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   25                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ASP DOT NET </h4>

                                  <h6><span>Vanisb</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> ASP Dot net </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   25                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR AUTOCAD </h4>

                                  <h6><span>Vanisb</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Autocad Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Autocad </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   25                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Vanisb</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Java  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4> AUTOCAD</h4>

                                  <h6><span>Shubham Hydrotch Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Autocad Engineer </h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / 1yr Experience</h5>
                                
                                  <h6><span>Technology:</span> Autocad</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET </h4>

                                  <h6><span>Apparrant Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot Net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1yr Experience</h5>
                                
                                  <h6><span>Technology:</span> Dot net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS </h4>

                                  <h6><span>Apparrant Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1yr Experience</h5>
                                
                                  <h6><span>Technology:</span> IOS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP PROGRAMMER</h4>

                                  <h6><span>Syscom Softech (P) Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> DELHI</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Programmer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.C.A./M.Sc./B.Sc</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>Precise Testing Solution</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BE/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s/Experience (0-2)</h5>
                                
                                  <h6><span>Technology:</span> Manual Testing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   24                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>Syscom Softech (P) Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BE/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> MANUAL TESTING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID</h4>

                                  <h6><span>Logicsoft International Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurugram</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/BCA/M.Tech/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 2yr – 3yr Experience)</h5>
                                
                                  <h6><span>Technology:</span> Android </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Kito Infocom Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> BTECH/ BCA/ MCA/ BE</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> FRESHERS</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANGULAR </h4>

                                  <h6><span>App Routes Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Angular  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/ /MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Angular  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>Code Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> B.Tech/M.Tech/BCA/MCA</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>  CAMPUS DRIVE FOR PHP CANDIDATES IN DUCAT NOIDA ON 16th  JANUARY</h4>

                                  <h6><span>Binates Bits  Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer       </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s     </h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>Mana Life Care Private Limited</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span>    B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Abalone technologies Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>   Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>Abalone Technologies Pvt Ltd .</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher’s</h5>
                                
                                  <h6><span>Technology:</span>   </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Tech Papa Technologies Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR ANDROID CANDIDATES IN DUCAT NOIDA ON 16th  JANUARY,2018</h4>

                                  <h6><span>Binates Bits  Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android Developer     </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s       </h5>
                                
                                  <h6><span>Technology:</span> Android </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR .Net CANDIDATES IN DUCAT NOIDA ON 15th  JANUARY,2018</h4>

                                  <h6><span>Successive Software Pvt  Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .Net Developer       </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/ BCA/MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s   </h5>
                                
                                  <h6><span>Technology:</span> .Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   13                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR IOS  CANDIDATES IN DUCAT NOIDA ON 16th  JANUARY,2018</h4>

                                  <h6><span>Binates Bits Pvt  Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .Net Developer       </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher’s       </h5>
                                
                                  <h6><span>Technology:</span> .Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   12                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Parahit Technologies Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Jr. Java Developer</h5>
                                
                                  <h6><span>Criteria:</span>    B.tech/M.tech/BCA/MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s /1to 3Years Exp</h5>
                                
                                  <h6><span>Technology:</span> Java </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   12                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>DP Project Development Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Ghaziabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   11                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR BIG DATA HADOOP CANDIDATES IN DUCAT NOIDA ON   (12th Janurary’18)</h4>

                                  <h6><span>Worthfull Industries Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Data Analysts/Data Engineers/Data Scienctists</h5>
                                
                                  <h6><span>Criteria:</span> BE, MCA/MBA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Experience</h5>
                                
                                  <h6><span>Technology:</span> Big Data Hadoop</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   11                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR MACHINE LEARNING CANDIDATES IN DUCAT NOIDA ON            (12th Janurary’18)</h4>

                                  <h6><span>Worthfull Industries Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i>    Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Data Analysts/Data Engineers/Data Scientists</h5>
                                
                                  <h6><span>Criteria:</span> BE, MCA/MBA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Experience</h5>
                                
                                  <h6><span>Technology:</span> Machine Learning</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   11                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR ORACLE D2K CANDIDATES IN DUCAT NOIDA ON            (12th Janurary’18)</h4>

                                  <h6><span>Worthfull Industries Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Data Analysts/Data Engineers/Data Scientists</h5>
                                
                                  <h6><span>Criteria:</span> BE, MCA/MBA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Experience</h5>
                                
                                  <h6><span>Technology:</span> Oracle D2K/Developer</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   11                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR PYTHON CANDIDATES IN DUCAT NOIDA ON   (12th Janurary’18)</h4>

                                  <h6><span>Worthfull Industries Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Data Analysts/Data Engineers/Data Scientists</h5>
                                
                                  <h6><span>Criteria:</span> BE, MCA/MBA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Experience</h5>
                                
                                  <h6><span>Technology:</span> Python</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   11                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR R- PROGRAMMING CANDIDATES IN DUCAT NOIDA ON   (12th Janurary’18)</h4>

                                  <h6><span>Web BEE eSolutions  Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Data Analysts/Data Engineers/Data Scientists</h5>
                                
                                  <h6><span>Criteria:</span> BE, MCA/MBA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Experience</h5>
                                
                                  <h6><span>Technology:</span> R- Programming</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   11                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR SAS PROGRAMMING CANDIDATES IN DUCAT NOIDA ON   (12th Janurary’18)</h4>

                                  <h6><span>Worthfull Industries Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Data Analysts/Data Engineers/Data Scientists</h5>
                                
                                  <h6><span>Criteria:</span> BE, MCA/MBA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Experience</h5>
                                
                                  <h6><span>Technology:</span> SAS Programming</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   11                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR MATLAB CANDIDATES IN DUCAT NOIDA ON   (12th Janurary’18)</h4>

                                  <h6><span>Worthfull Industries Pvt Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Data Analysts/Data Engineers/Data Scientists</h5>
                                
                                  <h6><span>Criteria:</span> BE, MCA/MBA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Experience</h5>
                                
                                  <h6><span>Technology:</span> MATLAB</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   11                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Fine soft Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i>  Kaushambi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Intern</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate/ B.E./ B.Tech/ MCA/ BCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s </h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   11                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING </h4>

                                  <h6><span>Chronicle Publications Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate B.tech/M.tech/ /MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   11                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Chronicle Publications Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s/ Minimum 1-2 Years </h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>  CAMPUS DRIVE FOR .Net CANDIDATES IN DUCAT NOIDA ON 10th  JANUARY,2018</h4>

                                  <h6><span>Simcomm Pvt  Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span>  .Net Developer       </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s       </h5>
                                
                                  <h6><span>Technology:</span> Dot net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR MANUAL TESTING</h4>

                                  <h6><span>Stout Web Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Ghaziabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing </h5>
                                
                                  <h6><span>Criteria:</span> -                B-tech (CS) /MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> MANUAL TESTING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR PHP CANDIDATES IN DUCAT NOIDA ON 11th  JANUARY, 2018</h4>

                                  <h6><span>It MNC Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer </h5>
                                
                                  <h6><span>Criteria:</span>   B.Tech/M.Tech/BCA/MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Striderinfotech Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developers</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate/ B.E./ B.Tech/ MCA/ BCA/ PGDCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s /1TO 5year Exp</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PYTHON</h4>

                                  <h6><span>Renew buy</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurugram</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> B.tech/M.tech/BCA/MCA</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s /1TO5YEAR</h5>
                                
                                  <h6><span>Technology:</span> Python </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR UI/UX</h4>

                                  <h6><span>Renew buy</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurugram</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Engineer – UI/UX</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  1TO5YEAR</h5>
                                
                                  <h6><span>Technology:</span> UI/UX</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Fonantrix</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Trainee</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech graduate of CS/IT background or MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s </h5>
                                
                                  <h6><span>Technology:</span> JAVA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR AUTOMATION</h4>

                                  <h6><span>AUTOMATION</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Trainee</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech graduate of CS/IT background or MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s </h5>
                                
                                  <h6><span>Technology:</span> Fonantrix</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR CLOUD COMPUTING</h4>

                                  <h6><span>Progressive Infotech Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Cloud Engineer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/BCA/M.Tech/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1-4 yr Experience</h5>
                                
                                  <h6><span>Technology:</span> CLOUD COMPUTING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Striderinfotech Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Any Graduate/ B.E./ B.Tech/ MCA/ BCA/ </h5>
                                
                                  <h6><span>Criteria:</span> Fresher’s/1TO 5Year Exp</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s/1TO 5Year Exp</h5>
                                
                                  <h6><span>Technology:</span> Dot net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DEVELOPER</h4>

                                  <h6><span>V3A info Technologies Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Ghaziabad </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s/Exp upto 1 Year</h5>
                                
                                  <h6><span>Technology:</span> Web Developer</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   10                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR NETWORK ENGINEER</h4>

                                  <h6><span>Rhythmus Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurugram</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Network Engineer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers</h5>
                                
                                  <h6><span>Technology:</span> Network Engineer</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>  CAMPUS DRIVE FOR WEB DESIGNING CANDIDATES IN DUCAT NOIDA ON 10th JANUARY</h4>

                                  <h6><span>S-Curious Research & Technologies Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing      </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s     </h5>
                                
                                  <h6><span>Technology:</span> Web Designing      </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANGULAR DEVELOPER</h4>

                                  <h6><span>App Routes Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Angular Developer</h5>
                                
                                  <h6><span>Criteria:</span>    B.tech/M.tech /MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Angular JS</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR AUTOCAD </h4>

                                  <h6><span>Novo Technologies Pvt. Ltd. </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Autocad Engineer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech(EE,EC), Diploma(EC,EE,CE)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s/0-2 Years</h5>
                                
                                  <h6><span>Technology:</span> Autocad</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Simcomm Technologies Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Dot net </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> B.tech/M.tech/BCA/MCA</h5>
                                
                                  <h6><span>Criteria:</span>  B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Misha infotech Pvt. Ltd. </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .NET Developer</h5>
                                
                                  <h6><span>Criteria:</span> BE/B.tech(CS/IT)/BSc–IT 2016 - 2017 pass out</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> .NET </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Mascot Business Solution </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Any Graduate</h5>
                                
                                  <h6><span>Criteria:</span> 10-15 K</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s/6 months experience</h5>
                                
                                  <h6><span>Technology:</span>  Php </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   08                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>Web BEE eSolutions  Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Manual Testing</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> B.tech/M.tech/BCA/MCA</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 2 TO 3 Years Experienced</h5>
                                
                                  <h6><span>Technology:</span> Manual Testing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   06                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR R PROGRAMMING</h4>

                                  <h6><span>TransOrg Analytics</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> R Programmer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 2-7 Yr</h5>
                                
                                  <h6><span>Technology:</span> R Programming</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Easy Solutions Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>CresTech Software Systems</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Software Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS DEVELOPER</h4>

                                  <h6><span>Intigate Technologies Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech /MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> IOS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Intigate technologies Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ADVANCE EXCEL</h4>

                                  <h6><span>Progressive Infotech Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Operation Executive</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers   </h5>
                                
                                  <h6><span>Technology:</span> Advance Excel</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   05                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR CCNA / NETWORKING</h4>

                                  <h6><span>Progressive Infotech Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Graduate in any stream  </h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers </h5>
                                
                                  <h6><span>Technology:</span> Networking / CCNA </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET</h4>

                                  <h6><span>AWC Software Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech (2014/2015/2016 Passout)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> .net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR SHAREPOINT</h4>

                                  <h6><span>AWC Software Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Sharepoint</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech (2014/2015/2016 Passout)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Sharepoint</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR NETWORK ENGINEER</h4>

                                  <h6><span>MothersonSumi INfotech & Designs Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Network Engineer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> CCNA / CCNP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS </h4>

                                  <h6><span>Binates Bits Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> ios </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                     OPENING FOR ORACLE D2K</h4>

                                  <h6><span>AWC Software Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Oracle D2k</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> ORACLE </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>    Algoworks Pvt Ltd .</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Testing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Innovation InfosoftTechnologies Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> .net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR NETWORKING ENGINEER</h4>

                                  <h6><span>24 Frames Digital</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida / Banglore</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Networking Engineer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> CCNA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS</h4>

                                  <h6><span>Rocket Flyer Technology Private Limited</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s </h5>
                                
                                  <h6><span>Technology:</span> ios</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ADVANCE EXCEL</h4>

                                  <h6><span>FleetLink</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Customer Relationship Executive</h5>
                                
                                  <h6><span>Criteria:</span> Graduate </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher    </h5>
                                
                                  <h6><span>Technology:</span> ADVANCE EXCEL</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   02                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR NETWORKING ENGINEER</h4>

                                  <h6><span>24 Frames Digital</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida / Banglore</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Network Engineer</h5>
                                
                                  <h6><span>Criteria:</span> BTECH/BE, BCA, MCA, BSC</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers /  6 Months - 1yr    </h5>
                                
                                  <h6><span>Technology:</span> Network Engineer</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>  CAMPUS DRIVE FOR WEB DESIGNING CANDIDATES IN DUCAT NOIDA ON 11th JANUARY</h4>

                                  <h6><span>IT MNC Pvt Ltd Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing      </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s </h5>
                                
                                  <h6><span>Technology:</span> Web Designing      </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   30                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR NETWORKING ENGINEER</h4>

                                  <h6><span>IOPEX Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Bangalore</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Network Engineer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in Any Stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1-6yr    </h5>
                                
                                  <h6><span>Technology:</span> NETWORKING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   29                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID DEVELOPER</h4>

                                  <h6><span>App Agicent Pvt Ltd  </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Android </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   29                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR GRAPHIC DESIGNING</h4>

                                  <h6><span>Dztul Technology</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Graphic Designer </h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Graphic Designer </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   28                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4> CAMPUS DRIVE FOR WEB DESIGNING CANDIDATES IN DUCAT FARIDABAD ON (29th  DECEMBER’17)</h4>

                                  <h6><span>Saga Techsys Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Creative Designer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   28                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR NETWORKING CCNA</h4>

                                  <h6><span>Ksolves India Pvt. Ltd.	</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Networking Engineer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Graduate in any stream</h5>
                                
                                  <h6><span>Technology:</span>  CCNA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR CCNA / CCNP CANDIDATES IN DUCAT NOIDA ON (29th   DECEMBER’17)</h4>

                                  <h6><span>Grapes Telecoms</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Support Engineer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> CCNA / CCNP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ADVANCE EXCEL</h4>

                                  <h6><span>Millennium Group</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Operations Executive</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> ADVANCE EXCEL</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>Saga Techsys Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing</h5>
                                
                                  <h6><span>Criteria:</span>  Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1yr Experience </h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Gyan Solutions Pvt  Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Magnetech Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> B.tech/M.tech/BCA/MCA</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4> CAMPUS DRIVE FOR PHP CANDIDATES IN DUCAT FARIDABAD ON (29th   DECEMBER’17)</h4>

                                  <h6><span>Saga Techsys Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Programmer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR  JAVA CANDIDATES IN DUCAT NOIDA  ON 28th DEC,2017</h4>

                                  <h6><span>Aigutech Technologies Pvt. Ltd” . </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Jaipur</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java Developer       </h5>
                                
                                  <h6><span>Criteria:</span> -       B.E./B.Tech (CS)/ MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers</h5>
                                
                                  <h6><span>Technology:</span> Java </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Easy Solutions Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers</h5>
                                
                                  <h6><span>Technology:</span> Dot net Developer</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ADVANCE EXCEL</h4>

                                  <h6><span>Sugam Parivahan Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Process Co-ordinator</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> ADVANCE EXCEL</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR CCNA</h4>

                                  <h6><span>Grapes Telecoms</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Support Engineer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> CCNA</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>Grapes Telecoms</h4>

                                  <h6><span>Grapes Telecoms</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Support Engineer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers</h5>
                                
                                  <h6><span>Technology:</span> Support Engineer</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING </h4>

                                  <h6><span>OPENING FOR DIGITAL MARKETING </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/ /MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshers</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS </h4>

                                  <h6><span>-           Saga Infotech Pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> IOS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>UE Solutions Pvt  Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> php</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   26                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>UE Solutions Pvt  Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   23                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>Lentix Pvt Ltd .</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> DIGITAL MARKETING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   23                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR GRAPHIC DESIGNER</h4>

                                  <h6><span>Lentix  Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Graphic Designer</h5>
                                
                                  <h6><span>Criteria:</span>  B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> GRAPHIC DESIGNER</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   23                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR MANUAL TESTING</h4>

                                  <h6><span>Techperspect Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing </h5>
                                
                                  <h6><span>Criteria:</span>  B-tech/M.tech/BCA/MCA(Overall 60%)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Manual Testing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   23                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Lentix Pvt Ltd  </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   22                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS</h4>

                                  <h6><span>Stout web Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Ghaziabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s </h5>
                                
                                  <h6><span>Technology:</span> IOS</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   22                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR MANUAL TESTING</h4>

                                  <h6><span>Stout web Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Ghaziabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing </h5>
                                
                                  <h6><span>Criteria:</span> B-tech (CS) /MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> MANUAL TESTING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   22                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Rise in Pvt Ltd  </span> <i class="fa fa-map-marker" aria-hidden="true"></i>     Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span>  Web Designing</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> WEB DESIGNING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   22                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Mymind infotech Pvt  Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   21                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>CAMPUS DRIVE FOR PHP CANDIDATES IN DUCAT NOIDA ON (23rd   DECEMBER’17)</h4>

                                  <h6><span>Saga Techsys Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP or HTML Programmer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   21                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4> CAMPUS DRIVE FOR WEB DESIGNING CANDIDATES IN DUCAT NOIDA ON (23rd  DECEMBER’17)</h4>

                                  <h6><span>Saga Techsys Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Creative Designer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   21                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP</h4>

                                  <h6><span>Aeropack Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> PHP </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Sr-PHP Developers</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 3 years</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   21                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR JAVA</h4>

                                  <h6><span>Aigutech Technologies Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> JAIPUR</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java Developers  </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s </h5>
                                
                                  <h6><span>Technology:</span> Java  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   21                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID DEVELOPER</h4>

                                  <h6><span>Aeropack  Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android Developers</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1 Year </h5>
                                
                                  <h6><span>Technology:</span> ANDROID</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   21                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP PROGRAMMER</h4>

                                  <h6><span>   Redian software</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Programmer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1 Yr Experience </h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR NODE JS</h4>

                                  <h6><span>Deligence Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi (Janakpuri)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Node JS</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/BCA/M.Tec/MCA  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s   </h5>
                                
                                  <h6><span>Technology:</span> NODE JS</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP PROGRAMMER</h4>

                                  <h6><span>Federation of Indian Animal Protection Organisations (FIAPO)</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Programmer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher / Min 2yr Experience </h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP DEVELOPER</h4>

                                  <h6><span>Risein  Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR CCNA / NETWORKING</h4>

                                  <h6><span>Progressive Infotech Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IT Helpdesk Coordinator</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s   </h5>
                                
                                  <h6><span>Technology:</span> Networking / CCNA </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ERP -SAP ABAP</h4>

                                  <h6><span>Vishal Mega Mart Private Limited</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurugram </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Trainee</h5>
                                
                                  <h6><span>Criteria:</span> B.E /CS/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Freshe r</h5>
                                
                                  <h6><span>Technology:</span> ERP -SAP ABAP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   20                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Green trace Pvt  Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR JAVA</h4>

                                  <h6><span>Agami TechnologiesPvt Ltd  </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NEW Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Java  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING</h4>

                                  <h6><span>Ishore Pvt Ltd .</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Digital Marketing </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR MANUAL TESTING</h4>

                                  <h6><span>I shore Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing </h5>
                                
                                  <h6><span>Criteria:</span> B-tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> MANUAL TESTING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR PHP PROGRAMMER</h4>

                                  <h6><span>Saga Techsys Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP Programmer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>Saga Techsys Pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Faridabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Creative Designer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate in any stream</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> WEB DESIGNING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   19                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANGULAR JS</h4>

                                  <h6><span>Stout web Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Ghaziabad</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Angular Js Developer</h5>
                                
                                  <h6><span>Criteria:</span> B-tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Angular Js </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR JAVA</h4>

                                  <h6><span>CresTech Software Systems</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Java  Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/2016/2017</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s </h5>
                                
                                  <h6><span>Technology:</span> Java  </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>GreenTrace  Pvt Ltd  </span> <i class="fa fa-map-marker" aria-hidden="true"></i> GreenTrace  Pvt Ltd  </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designing</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Green Trace Pvt  Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot net Developer</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR AUTOCAD</h4>

                                  <h6><span>Algowire Trading </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NEW Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Autocad Developer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate pass out till 2017</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Autocad </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR WEB DEVLOPER</h4>

                                  <h6><span>Algowire Trading </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NEW Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   17                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ORACLE </h4>

                                  <h6><span>MM info systems pvt. Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Oracle Developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate/MCA/BCA/Diploma in IT.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Minimum 1 year</h5>
                                
                                  <h6><span>Technology:</span> ORACLE </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>DEC</span>
                                   15                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>Opening For Php</h4>

                                  <h6><span>Diction Technology</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Php Software Trainee</h5>
                                
                                  <h6><span>Criteria:</span> B.tech,Bca,Mca,B.sc-It </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> Php</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>NOV</span>
                                   30                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS </h4>

                                  <h6><span>App Agicent Pvt Ltd .</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Ios Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> IOS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>NOV</span>
                                   23                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ERP-SAP</h4>

                                  <h6><span>MM info systems pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> ERP Support & Implementation</h5>
                                
                                  <h6><span>Criteria:</span> Graduate/MCA/BCA/Diploma in IT.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher & Minimum 1 yr Exp.</h5>
                                
                                  <h6><span>Technology:</span> ERP-SAP</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   25                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR .NET</h4>

                                  <h6><span>Sona Core LLP</span> <i class="fa fa-map-marker" aria-hidden="true"></i> New Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> .Net Developer (MVC)</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech BCA/MCA /</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-2Yr</h5>
                                
                                  <h6><span>Technology:</span> .NET</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JUN</span>
                                   18                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANGULAR</h4>

                                  <h6><span>Wow Services</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Angular DEVELOPER</h5>
                                
                                  <h6><span>Criteria:</span> B.Tech, MCA BCA.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1 years</h5>
                                
                                  <h6><span>Technology:</span> Angular</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>MAR</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ADVANCE EXCEL</h4>

                                  <h6><span>FleetLink</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Advance Excel</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Advanced Excel</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   12                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ORACLE DEVELOPER</h4>

                                  <h6><span>MM info systems pvt. Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Gurgaon</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Oracle Developer</h5>
                                
                                  <h6><span>Criteria:</span> Graduate/MCA/BCA/Diploma in IT.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 6 Month - 1 yr</h5>
                                
                                  <h6><span>Technology:</span> Oracle D2K</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>FEB</span>
                                   03                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR TESTING</h4>

                                  <h6><span>Hestabit Technologies Pvt. Ltd..</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Manual Testing </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/M.Tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> MANUAL TESTING</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   27                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DIGITAL MARKETING </h4>

                                  <h6><span>Gyan Solutions Pvt  Ltd.</span> <i class="fa fa-map-marker" aria-hidden="true"></i>     Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> B.tech/M.tech/ /MCA</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/ /MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Digital Marketing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   09                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>  CAMPUS DRIVE FOR ANDROID CANDIDATES IN DUCAT NOIDA ON 10th  JANUARY,2018</h4>

                                  <h6><span>S-Curious Research & Technologies Pvt Ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span>   Android Developer     </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span>  Fresher’s  </h5>
                                
                                  <h6><span>Technology:</span> Android </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   04                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS</h4>

                                  <h6><span>Rocket Flyer Technology Private Limited</span> <i class="fa fa-map-marker" aria-hidden="true"></i> NOIDA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> ios </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR ANDROID</h4>

                                  <h6><span>Algowire Trading </span> <i class="fa fa-map-marker" aria-hidden="true"></i> NEW Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Android developer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate pass out till 2017</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> ANDROID</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET DEVELOPER</h4>

                                  <h6><span>Ishore Pvt  Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> Dot net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>WALKINN FOR DOT NET DEVELOPERS</h4>

                                  <h6><span>Greentrace Consultancy PVT.LTD. (INDIA)</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot Net Developer</h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 0-1yr</h5>
                                
                                  <h6><span>Technology:</span> Dot Net </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>S-Curious Research & Technologies Pvt Ltd </h4>

                                  <h6><span>S-Curious Research & Technologies Pvt Ltd </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> PHP </h5>
                                
                                  <h6><span>Criteria:</span> -             B.Tech/M.Tech/BCA/MCA </h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> PHP </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR DOT NET </h4>

                                  <h6><span>Snap-on Business Solutions </span> <i class="fa fa-map-marker" aria-hidden="true"></i> Noida</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Dot net </h5>
                                
                                  <h6><span>Criteria:</span> B.Tech/B.E (IT/CS) MCA/M.Tech 2016-17 batch with 60% overall and regular academic background.</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s </h5>
                                
                                  <h6><span>Technology:</span> Dot Net</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>                    OPENING FOR WEB DESIGNING</h4>

                                  <h6><span>E Com Shipping Solutions (P) ltd</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> Web Designer</h5>
                                
                                  <h6><span>Criteria:</span>  B.Tech/M.Tech/BCA/MCA(Diploma Also)</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> 1+Yrs</h5>
                                
                                  <h6><span>Technology:</span> Web Designing</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR ANDROID</h4>

                                  <h6><span>SSOFTWARES PVT LTD</span> <i class="fa fa-map-marker" aria-hidden="true"></i> GURGAON</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> ANDROID Developer</h5>
                                
                                  <h6><span>Criteria:</span> Any Graduate</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher</h5>
                                
                                  <h6><span>Technology:</span> ANDROID</h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                                         <div class="eventsBoxNew">

                         
                        
                          <div class="row">
                            
                              <div class="col-md-1">
                                
                                  <div class="dateBox">
                                                                          <span>JAN</span>
                                   01                                  </div>

                              </div> <!-- End of COl MD 1 -->
                            
                              <div class="col-md-4">
                                
                                  <h4>OPENING FOR IOS </h4>

                                  <h6><span>Etelligens Technologies</span> <i class="fa fa-map-marker" aria-hidden="true"></i> Delhi</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-4">
                                
                                  <h5><span>Job Profile:</span> IOS Developer </h5>
                                
                                  <h6><span>Criteria:</span> B.tech/M.tech/BCA/MCA</h6>

                              </div> <!-- End of COl MD 4 -->
                            
                              <div class="col-md-3">
                                
                                  <h5><span>Experiance:</span> Fresher’s</h5>
                                
                                  <h6><span>Technology:</span> IOS </h6>

                              </div> <!-- End of COl MD 3 -->

                          </div> <!-- End of ROW -->

                      </div>
                      
                      

                  </div> <!-- End Of Col MD 12 -->

              </div> <!-- End Of Row -->

          </div> <!-- End OF Container -->

      </section>
<?php echo view("includes/footer.php"); ?>