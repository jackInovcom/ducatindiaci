<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     VLSI DESIGN FLOW
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     VLSI DESIGN FLOW
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      LEARNING TECHNOLOGY IN THE MOST PROFICIENT WAY
     </h4>
     <p>
      People wanting to have VLSI Design Flow education must come to DUCAT. DUCAT is the best for any IT connected teaching. The training given is fully specialized and it's unnecessary to say that after schooling the trainees become specialized in the field. This course gives an in-depth awareness going into details starting from system design,problem connected to definition, analysis related necessities and others details of testing. This course will includes practical tests and industrial test which provides self-assurance to students to clear industrial interview and pass the certification exam with VLSI Design Flow. Software testing teaching will increase portability, maintainability, compatibility, ability, dependability, competence and usability of the candidate. This training program helps businessman and job applicant to know animportant way to developmentof productivity and cost reduction. DUCAT has faculties who are expert in this field. They communicate their ability in a way that the learner very easily relates with the subject and outshine in the field. The teaching imparted makes them very much competent of taking correct condition based choice and to solve issues, making them more preferred candidates than the other in the job market.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction to VLSI
      </h2>
      <ul>
       <li>
        What is VLSI
       </li>
       <li>
        VLSI Design Flow
       </li>
       <li>
        ASIC
       </li>
       <li>
        SoC
       </li>
      </ul>
      <h2>
       Fundamentals of Digital Design
      </h2>
      <ul>
       <li>
        Basic Digital Circuits
       </li>
       <li>
        Logic gates   Boolean Algebra
       </li>
       <li>
        Number System
       </li>
       <li>
        Digital Logic Families
       </li>
      </ul>
      <h2>
       Combinational Logic Design
      </h2>
      <ul>
       <li>
        Multiplexers
       </li>
       <li>
        MUX based design for digital circuits
       </li>
       <li>
        Demultiplexers/Decoders
       </li>
       <li>
        Adders/Sub tractors
       </li>
       <li>
        BCD Arithmetic   ALU
       </li>
       <li>
        Comparators   Parity Generator
       </li>
       <li>
        Code Converters/Encoders
       </li>
       <li>
        Decoders
       </li>
       <li>
        Multipliers/Divider
       </li>
      </ul>
      <h2>
       Sequential Logic Design Priciples
      </h2>
      <ul>
       <li>
        Bistable Elements,
       </li>
       <li>
        Latches and Flip-Flops
       </li>
       <li>
        Counters and its application
       </li>
       <li>
        Synchronous Design Methodology
       </li>
       <li>
        Impediments to Synchronous Design
       </li>
       <li>
        Shift Registers
       </li>
       <li>
        Design Examples   Case studies
       </li>
      </ul>
      <h2>
       Advanced Digital Design
      </h2>
      <ul>
       <li>
        Synchronous/Asynchronous Sequential Circuits
       </li>
       <li>
        Clocked Synchronous State-Machine Analysis.
       </li>
       <li>
        Clocked Synchronous State-Machine Design
       </li>
       <li>
        Finite state machine
       </li>
       <li>
        Mealy and Moore machine
       </li>
       <li>
        State reduction technique
       </li>
       <li>
        Sequence Detectors
       </li>
       <li>
        ASM Charts
       </li>
       <li>
        Synchronizer Failure and Metastability Estimation
       </li>
       <li>
        Clock Dividers
       </li>
       <li>
        Synchronizers   Arbiters
       </li>
       <li>
        FIFO   Pipelining
       </li>
       <li>
        PLD + CPLD
       </li>
      </ul>
      <h2>
       VHDL OVERVIEW AND CONCEPTS
      </h2>
      <ul>
       <li>
        Types, object
       </li>
       <li>
        classes, design units, compilation, elaboration.
       </li>
       <li>
        BASIC LANGUAGE ELEMENTS: Lexical elements,
       </li>
       <li>
        syntax, operators, types and subtypes (scalar, physical,
       </li>
       <li>
        real, composite (arrays, records), access files).
       </li>
      </ul>
      <h2>
       DRIVERS
      </h2>
      <ul>
       <li>
        Resolution function, drivers (definition,
       </li>
       <li>
        initialization, creation ), ports
       </li>
       <li>
        TIMING:
       </li>
       <li>
        Signal attributes, "wait" statement, delta time,
       </li>
       <li>
        simulation engine, modeling with delta time delays, VITAL
       </li>
       <li>
        tables, inertial / transport delay
       </li>
      </ul>
      <h2>
       ELEMENTS OF ENTITY/ARCHITECTURE
      </h2>
      <ul>
       <li>
        Entity,
       </li>
       <li>
        architecture, (process, concurrent signal assignment,
       </li>
       <li>
        component instantiation and port association rules,
       </li>
       <li>
        consurrent procedure, generate, concurrent assertion, block, guarded signal).
       </li>
      </ul>
      <h2>
       SUBPROGRAMS
      </h2>
      <ul>
       <li>
        Rules and guidelines (unconstrained
       </li>
       <li>
        arrays, interface class, initialization, implicit signal
       </li>
       <li>
        attributes, drivers, signal characteristics in procedure
       </li>
       <li>
        calls, side effects) overloading, functions (resolution
       </li>
       <li>
        function, operator overloading), concurrent procedure.
       </li>
      </ul>
      <h2>
       PACKAGES
      </h2>
      <ul>
       <li>
        Declaration, body, deferred Constant, "use"
       </li>
       <li>
        Clause, Signals, resolution function, subprograms,
       </li>
       <li>
        converting typed object to strings, TEXTIO, printing
       </li>
       <li>
        objects, linear feedback shift register, random number
       </li>
       <li>
        generation compilation order
       </li>
      </ul>
      <h2>
       USER DEFINED ATTRIBUTES, SPECIFICATIONS, AND CONFIGURATION
      </h2>
      <ul>
       <li>
        Attributes declarations, attributes
       </li>
       <li>
        specification, configuration specification and binding,
       </li>
       <li>
        configuration declaration and binding, configuration of
       </li>
       <li>
        generate statements.
       </li>
      </ul>
      <h2>
       DESIGN FOR SYNTHESIS
      </h2>
      <ul>
       <li>
        Constructs, register interface,
       </li>
       <li>
        combinational logic interface, state machine and
       </li>
       <li>
        design styles, arithmetic operations.
       </li>
      </ul>
      <h2>
       FUNCTIONAL MODELS AND TESTBENCHES
      </h2>
      <ul>
       <li>
        Test
       </li>
       <li>
        bench design methodology, BFM Modeling,scenario
       </li>
       <li>
        generation schemes, waveform generator, client/server,
       </li>
       <li>
        text command file, binary command file.
       </li>
      </ul>
      <h2>
       VERILOG
      </h2>
      <ul>
       <li>
        Evolution of CAD, emergence of HDLs, typical HDLbased
       </li>
       <li>
        design flow, why Verilog HDL?, trends in HDLs.
       </li>
      </ul>
      <h2>
       Hierarchical Modeling Concepts
      </h2>
      <ul>
       <li>
        Top-down and bottom-up design methodology,
       </li>
       <li>
        differences between modules and module instances,parts
       </li>
       <li>
        of a simulation, design block, stimulus block.
       </li>
      </ul>
      <h2>
       Basic Concepts
      </h2>
      <ul>
       <li>
        Lexical conventions, data types, system tasks, compiler
       </li>
       <li>
        variable
       </li>
       <li>
        directives.
       </li>
      </ul>
      <h2>
       Modules and Ports
      </h2>
      <ul>
       <li>
        Modules definition, port declaration, connecting ports,
       </li>
       <li>
        hierarchical name referencing.
       </li>
      </ul>
      <h2>
       Gate-Level Modeling
      </h2>
      <ul>
       <li>
        Modeling using basic Verilog gate primitives, description
       </li>
       <li>
        of and/or and Buf/not type gates, rise, fall and turn-off
       </li>
       <li>
        delays, min, max and typical delays.
       </li>
      </ul>
      <h2>
       Dataflow Modeling
      </h2>
      <ul>
       <li>
        Continuous assignments, delay specification,
       </li>
       <li>
        expressions, operators, operands, operator types.
       </li>
      </ul>
      <h2>
       Structured procedures, initial and always, blocking and
      </h2>
      <ul>
       <li>
        nonblocking statements, delay control, generate
       </li>
       <li>
        statement, event control, conditional statements,
       </li>
       <li>
        multiway branching, loops, sequential and parallel blocks.
       </li>
      </ul>
      <h2>
       Tasks and Functions
      </h2>
      <ul>
       <li>
        Differences between tasks and functions, declaration,
       </li>
       <li>
        invocation, automatic tasks and functions.
       </li>
       <li>
        Datatype
       </li>
      </ul>
      <h2>
       Useful Modeling Techniques
      </h2>
      <ul>
       <li>
        Procedural continuous assignments, overriding
       </li>
       <li>
        parameters, conditional compilation and execution, useful
       </li>
       <li>
        system tasks.
       </li>
      </ul>
      <h2>
       Advanced Verilog Topics
      </h2>
      <ul>
       <li>
        Timing and Delays
       </li>
       <li>
        Distributed, lumped and pin-to-pin delays, specify blocks,
       </li>
       <li>
        parallel and full connection, timing checks, delay backannotation.
       </li>
      </ul>
      <h2>
       Switch-Level Modeling
      </h2>
      <ul>
       <li>
        Syntax
       </li>
       <li>
        variable
       </li>
       <li>
        Datatype
       </li>
      </ul>
      <h2>
       PHP Syntax
      </h2>
      <ul>
       <li>
        MOS and CMOS Switches, bidirectional switches,
       </li>
       <li>
        modeling of power and ground, resistive switches, delay
       </li>
       <li>
        specification on switches.
       </li>
      </ul>
      <h2>
       User-Deined Primitives
      </h2>
      <ul>
       <li>
        Parts of UDP, UDP rules, combinational UDPs, sequential
       </li>
       <li>
        UDPs Shorthand symbols.
       </li>
      </ul>
      <h2>
       Logic Synthesis with Verilog HDL
      </h2>
      <ul>
       <li>
        Introduction to logic synthesis, impact of logic synthesis,
       </li>
       <li>
        Verilog HDL constructs and operators for logic synthesis,
       </li>
       <li>
        synthesis design flow, verification of synthesized circuits,
       </li>
       <li>
        modeling tips, design partitioning.
       </li>
      </ul>
      <h2>
       Advanced Verification Techniques
      </h2>
      <ul>
       <li>
        Introduction to a simple verification flow, architectural
       </li>
       <li>
        modeling, test vectors/testbenches,simulation
       </li>
       <li>
        acceleration emulation, analysis/coverage, assertion
       </li>
       <li>
        checking, formal verification, semi-formal verification,
       </li>
       <li>
        equivalence checking.
       </li>
      </ul>
      <h2>
       Introduction to ASIC DESIGN METHODOLOGY
      </h2>
      <ul>
       <li>
        Typical Design Flow
       </li>
       <li>
        Specification and RTL Coding
       </li>
       <li>
        Dynamic Simulation
       </li>
      </ul>
      <h2>
       PHP Syntax
      </h2>
      <ul>
       <li>
        Syntax
       </li>
       <li>
        variable
       </li>
       <li>
        Constraints, Synthesis
       </li>
       <li>
        Formal Verification
       </li>
       <li>
        Statics Timing Analysis
       </li>
       <li>
        Placement Routing and Verification
       </li>
       <li>
        Engineering Change Order
       </li>
      </ul>
      <h2>
       Front End Implementation SYNTHESIS
      </h2>
      <ul>
       <li>
        Synthesis Environment
       </li>
       <li>
        Design Constraint
       </li>
       <li>
        Design Enty
       </li>
       <li>
        Technology Library
       </li>
       <li>
        Delay Calculation
       </li>
       <li>
        Delay Model
       </li>
      </ul>
      <h2>
       PARTITIONING AND CODING STYLES
      </h2>
      <ul>
       <li>
        Partitioning for Synthesis
       </li>
       <li>
        RTL: Software Vs Hardware
       </li>
       <li>
        General guidelines
       </li>
       <li>
        Technology Independence
       </li>
       <li>
        Clock Logic
       </li>
       <li>
        Clock Stretching
       </li>
       <li>
        Guidelines for FSM Synthesis
       </li>
       <li>
        Logic Inference
       </li>
       <li>
        Memory element inference
       </li>
       <li>
        Multiplexer Inference
       </li>
       <li>
        Three state Inference
       </li>
      </ul>
      <h2>
       System Verilog
      </h2>
      <ul>
       <li>
        Introduction to system verilog
       </li>
       <li>
        Data types:-
       </li>
       <li>
        Datatype
       </li>
       <li>
        Integer data type
       </li>
       <li>
        Real and short real
       </li>
       <li>
        Void data types
       </li>
       <li>
        Strings
       </li>
       <li>
        Event
       </li>
       <li>
        User defined
       </li>
       <li>
        Data declaration- Constant variables net reg logic
       </li>
       <li>
        signal aliasing
       </li>
       <li>
        Enumerations
       </li>
       <li>
        Structure and Union
       </li>
       <li>
        Classes
       </li>
       <li>
        Casting
       </li>
       <li>
        Arrays
       </li>
       <li>
        Packed and unpacked
       </li>
       <li>
        Dynamic arrays
       </li>
       <li>
        Queues
       </li>
       <li>
        Operators and Expressions
       </li>
       <li>
        Arithmetic
       </li>
       <li>
        Logical
       </li>
       <li>
        Operator Loading
       </li>
       <li>
        Conditional
       </li>
       <li>
        Procedural statements and Control flow
       </li>
       <li>
        Blocking and non blocking assignments
       </li>
       <li>
        Selection Statements
       </li>
       <li>
        Loops
       </li>
       <li>
        jump
       </li>
       <li>
        Final block
       </li>
       <li>
        Named block
       </li>
       <li>
        Event control
       </li>
       <li>
        Level sensitive seq. control
       </li>
      </ul>
      <h2>
       Task and functions
      </h2>
      <ul>
       <li>
        Argument passing
       </li>
       <li>
        Import and export functions
       </li>
       <li>
        Intro
       </li>
       <li>
        Object and its properties and methods
       </li>
       <li>
        Constructor
       </li>
       <li>
        Inheritances
       </li>
       <li>
        Sub classes
       </li>
       <li>
        Overridden members
       </li>
       <li>
        Super class
       </li>
       <li>
        Casting
       </li>
       <li>
        Data hiding and encapsulation
       </li>
       <li>
        Constant class and virtual methods
       </li>
       <li>
        Polymirphism
       </li>
       <li>
        Assertions
       </li>
       <li>
        Immediate assertion
       </li>
       <li>
        Concurrent assertion overview
       </li>
       <li>
        Boolean exp
       </li>
       <li>
        Seq.
       </li>
       <li>
        Sequence operation
       </li>
       <li>
        Manipulating data in sequence
       </li>
       <li>
        Calling sub routines on the match of sequence
       </li>
       <li>
        Concurrent assertions
       </li>
      </ul>
      <h2>
       List of Projects
      </h2>
      <ul>
       <li>
        Microcontroller Design
       </li>
       <li>
        RISC   CISC Processor Design
       </li>
       <li>
        Multiplier/Divider using different Algorithms
       </li>
       <li>
        DDR Controller
       </li>
       <li>
        I2C,AMBA,Wishbone Conmax
       </li>
       <li>
        JTAG: Boundary SCAN
       </li>
       <li>
        JPC, PCI, Ethernet
       </li>
       <li>
        CORDIC Algorithm
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="vlsidesignflowtraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="81 + 69 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="150">
       <input name="url" type="hidden" value="/vlsidesignflowtraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>