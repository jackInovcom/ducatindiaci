<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     Best Robotics Process Automation UI Path Training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     RPA
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Ducat Robotics Process Automation UI Path training will make you an expert in UiPath RPA tool, so that you can drive RPA initiatives in your organisation. You'll master the concepts of key considerations while designing a RPA solution using UiPath, perform Image and text automation, create RPA Bots, perform Data Manipulation and debug and handle the exceptions, using real-life case studies.
     </p>
     <div class="contentAcc">
      <h2>
       Robotics Process Automation
      </h2>
      <ul>
       <li>
        Robotics Process Automation Concepts
       </li>
       <li>
        What to be Automated
       </li>
       <li>
        Why to automate task/process
       </li>
       <li>
        Architecture
       </li>
       <li>
        Monitoring / Reporting / Auditing
       </li>
       <li>
        Tools under RPA
       </li>
       <li>
        Blue Prism
       </li>
       <li>
        UIPath
       </li>
       <li>
        AA
       </li>
       <li>
        Work Fusion
       </li>
       <li>
        Life cycle
       </li>
       <li>
        Various Tools and their Limitation
       </li>
       <li>
        Areas- where to go for normal automation and where to use RPA
       </li>
      </ul>
      <h2>
       UI-PATH
      </h2>
      <h2>
       Overview
      </h2>
      <ul>
       <li>
        About the Course
       </li>
       <li>
        About Me
       </li>
       <li>
        Why UiPath?
       </li>
       <li>
        Who can use the "free" version of UiPath
       </li>
       <li>
        End to End Demo: Build a Simple Software Robot
       </li>
       <li>
        Where to go for the fastest technical assistance
       </li>
      </ul>
      <h2>
       Community Edition Installation (Windows)
      </h2>
      <ul>
       <li>
        Install UiPath Studio
       </li>
       <li>
        Install Important Packages
       </li>
       <li>
        Examine the installed Activities
       </li>
       <li>
        Set up browsers
       </li>
       <li>
        Renewing a community edition license
       </li>
      </ul>
      <h2>
       Orientation
      </h2>
      <ul>
       <li>
        Overview
       </li>
       <li>
        Create new projects
       </li>
       <li>
        Open &amp; copy existing projects
       </li>
       <li>
        Project &amp; tool windows tour
       </li>
      </ul>
      <h2>
       Basic UiPath Skills
      </h2>
      <ul>
       <li>
        Overview
       </li>
       <li>
        Quick tour of all activities
       </li>
       <li>
        Sequences and Flowcharts
       </li>
       <li>
        Display a message box
       </li>
       <li>
        Logging to the output window
       </li>
       <li>
        Open and read a text file
       </li>
       <li>
        Use a loop to repeat work
       </li>
       <li>
        Send email
       </li>
      </ul>
      <h2>
       Debugging
      </h2>
      <ul>
       <li>
        Run in debug mode
       </li>
       <li>
        Set a break point and debug line by line
       </li>
      </ul>
      <h2>
       Using Variables to Handle Data
      </h2>
      <ul>
       <li>
        Overview
       </li>
       <li>
        Using the Variables pane
       </li>
       <li>
        Browsing for special variable types
       </li>
       <li>
        Using the "Assign" Activity
       </li>
       <li>
        Variables as the output of activities
       </li>
       <li>
        Variable scope
       </li>
      </ul>
      <h2>
       Script Control Flow
      </h2>
      <ul>
       <li>
        If/Else: Making a choice
       </li>
       <li>
        Flowchart decision diamond
       </li>
       <li>
        Switch: Handling many choices
       </li>
       <li>
        Flow Switch: Handling many choices
       </li>
      </ul>
      <h2>
       Handling Errors
      </h2>
      <ul>
       <li>
        Try/Catch intro - Catch an exception
       </li>
       <li>
        Create &amp; throw a new exception
       </li>
      </ul>
      <h2>
       Control Your Robots with Orchestrator
      </h2>
      <ul>
       <li>
        Orchestrator overview
       </li>
       <li>
        Create an Orchestrator account
       </li>
       <li>
        Login, setup, and tour the UI
       </li>
       <li>
        Connecting a robot
       </li>
       <li>
        Create an environment and associate the robot
       </li>
       <li>
        Publish a package
       </li>
       <li>
        Create a process from the package
       </li>
       <li>
        Run an attended robot and examine logs
       </li>
       <li>
        Fix the robot and re-publish
       </li>
       <li>
        Schedule and run an unattended robot
       </li>
       <li>
        Assets: overview &amp; creation
       </li>
       <li>
        Assets: consuming from a robot
       </li>
       <li>
        Robot-level input parameters
       </li>
       <li>
        Pause an attended robot
       </li>
      </ul>
      <h2>
       Web Browser Automation Basics
      </h2>
      <ul>
       <li>
        Create the assets &amp; project
       </li>
       <li>
        Retrieve the assets
       </li>
       <li>
        Open and close the browser
       </li>
       <li>
        Login and logout
       </li>
       <li>
        Perform a search
       </li>
       <li>
        Enabling pop up windows
       </li>
      </ul>
      <h2>
       Reusable/Shareable Workflows
      </h2>
      <ul>
       <li>
        Overview
       </li>
       <li>
        Create a reusable/shareable workflow
       </li>
       <li>
        Pass data into a reusable workflow
       </li>
       <li>
        Get data out of a reusable workflow
       </li>
       <li>
        "Extract" a workflow
       </li>
      </ul>
      <h2>
       Data Tables
      </h2>
      <ul>
       <li>
        Overview
       </li>
       <li>
        Tour of data table activities
       </li>
       <li>
        Create a data table
       </li>
       <li>
        (v2018.3 +) Edit data table columns!
       </li>
       <li>
        Add data table rows
       </li>
       <li>
        Loop through the rows of a data table
       </li>
       <li>
        Query the table
       </li>
       <li>
        Delete a data table row (difficult!)
       </li>
      </ul>
      <h2>
       Excel Automation Basics
      </h2>
      <ul>
       <li>
        Overview
       </li>
       <li>
        Excel/worksheet activity overview
       </li>
       <li>
        Read worksheet into a data table
       </li>
       <li>
        Write data table into a worksheet
       </li>
       <li>
        Append data table into a worksheet
       </li>
       <li>
        Read and write worksheet CELL data
       </li>
       <li>
        Read range, transform table, and write range
       </li>
      </ul>
      <h2>
       Mainframe Automation Basics
      </h2>
      <ul>
       <li>
        Mainframe intro
       </li>
       <li>
        Mainframe automation
       </li>
      </ul>
      <h2>
       Case Study:
      </h2>
      <ul>
       <li>
        Case Study 1 (Real Estate)
       </li>
       <li>
        Case Study 2 (Telecom)
       </li>
       <li>
        Case Study 3 (HR Domain)
       </li>
      </ul>
      <h2>
       PoCs:
      </h2>
      <ul>
       <li>
        Proof of Concepts (2 project)
       </li>
      </ul>
      <h2>
       Live Project Overview:
      </h2>
      <ul>
       <li>
        At least 1 Project
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="rpauipath.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="88 + 55 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="143">
       <input name="url" type="hidden" value="/rpauipathtraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>