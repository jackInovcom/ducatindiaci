<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     JAVA SIX WEEKS
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     JAVA SIX WEEKS
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Java is a very powerful object oriented programming language used world over for developing variety of applications. Students pursuing a course on Java have plenty of career options such as take up job as Java programmer, proceed for Sun Java Certification exams and/or learn advanced Java technologies. JAVA IN 6 WEEKS from DUCAT is a quick fire course that includes all the basics and necessary concepts in Java. This course would give an insight on use of various technologies in J2EE and use of Struts framework to unwind development in large scale projects. Students would get a window view of leveraging Java in the current scenario and also its future applications as well. This training course is designed in line with curriculum used in colleges. The students can opt to continue with industrial training at DUCAT too. They would get an Industrial training certificate on successful completion of training for academic records of the college. DUCAT has placement consultancy wing too which can help the students to get jobs. Students pursuing courses at DUCAT not only learn the subject, moreover earn an experience certificate to get job assistance.
     </p>
     <div class="contentAcc">
      <h2>
       INTRODUCTION TO JAVA
      </h2>
      <ul>
       <li>
        Understanding Requirement: Why Java
       </li>
       <li>
        Why Java is important to the Internet
       </li>
       <li>
        JAVA on LINUX Platform
       </li>
      </ul>
      <h2>
       INTRODUCTION TO JAVA VIRTUAL MACHINE
      </h2>
      <ul>
       <li>
        Java Virtual Machine Architecture
       </li>
       <li>
        Class loading process by Classloaders
       </li>
       <li>
        Role of Just in Time Compiler (JIT)
       </li>
       <li>
        Execution Engine
       </li>
      </ul>
      <h2>
       AN OVERVIEW OF JAVA AND BUZZWORDS
      </h2>
      <ul>
       <li>
        Data Types, Variables ad Arrays
       </li>
       <li>
        Operators
       </li>
       <li>
        Control statements
       </li>
       <li>
        Object oriented Paradigms
       </li>
       <li>
        Abstraction
       </li>
       <li>
        The Three OOP Principles (Encapsulation, Inheritance and Polymorphism)
       </li>
      </ul>
      <h2>
       JAVA CLASSES AND OOP IMPLEMENTATION
      </h2>
      <ul>
       <li>
        Class Fundamentals
       </li>
       <li>
        Command Line Arguments
       </li>
       <li>
        Learning static initializer
       </li>
       <li>
        Declaration on of Objects
       </li>
       <li>
        Instance Variable Hiding
       </li>
       <li>
        Overloading and Overriding of Methods
       </li>
       <li>
        Understanding of Access Controls:
       </li>
       <ul>
        <li>
         Private, Public and Protected
        </li>
       </ul>
       <li>
        Learning Nested and Inner Classes
       </li>
       <li>
        Dynamic method Dispatching
       </li>
       <li>
        Using Abstract Classes
       </li>
       <li>
        Using final to prevent Overriding &amp; Inheritance
       </li>
       <li>
        Garbage Collection
       </li>
      </ul>
      <h2>
       PACKAGES AND INTERFACES
      </h2>
      <ul>
       <li>
        Defining a Package
       </li>
       <li>
        Understanding CLASSPATH
       </li>
       <li>
        Access Protection
       </li>
       <li>
        Importing Packages
       </li>
       <li>
        Defining and implementing an Interface
       </li>
       <li>
        Abstract classes Vs Interfaces
       </li>
       <li>
        Genrics
       </li>
       <li>
        Annotations
       </li>
       <li>
        Vargs
       </li>
       <li>
        For Each
       </li>
      </ul>
      <h2>
       INTRODUCTION TO ARRAY
      </h2>
      <ul>
       <li>
        Single dimension array
       </li>
       <li>
        Multi dimension array
       </li>
      </ul>
      <h2>
       EXCEPTION HANDLING
      </h2>
      <ul>
       <li>
        Fundamentals of exception on handling
       </li>
       <li>
        Types of exceptions
       </li>
       <li>
        Learning exception handlers
       </li>
       <li>
        Try and catch
       </li>
       <li>
        Multiple catch clauses
       </li>
       <li>
        Nested try statements
       </li>
       <li>
        Throw, throws and finally
       </li>
       <li>
        Custom Exception
       </li>
      </ul>
      <h2>
       STRING HANDLING
      </h2>
      <ul>
       <li>
        Learning String Operations
       </li>
       <li>
        Learning Character Extraction
       </li>
       <li>
        Learning String Comparison
       </li>
       <li>
        Understanding of String Buffer Class &amp; String Builder Class
       </li>
      </ul>
      <h2>
       MULTITHREADED PROGRAMMING
      </h2>
      <ul>
       <li>
        The Java Thread Model
       </li>
       <li>
        Creating a Thread: Extending Thread, Implementing Runnable
       </li>
       <li>
        Creating Multiple Threads and Context Switching
       </li>
       <li>
        Synchronization: Methods and Statement
       </li>
       <li>
        Interthread Communication
       </li>
      </ul>
      <h2>
       MANAGING INPUT AND OUTPUT IN JAVA
      </h2>
      <ul>
       <li>
        Introduction to I/O streams
       </li>
       <li>
        File Handling
       </li>
       <li>
        Binary Streams
       </li>
       <li>
        Character Streams
       </li>
       <li>
        Data Streams
       </li>
       <li>
        Serialization
       </li>
      </ul>
      <h2>
       NETWORKING
      </h2>
      <ul>
       <li>
        Introduction to Networking
       </li>
       <li>
        URL, InetAddress
       </li>
       <li>
        Socket and Server Socket
       </li>
       <li>
        Datagram Socket
       </li>
      </ul>
      <h2>
       COLLECTION API
      </h2>
      <ul>
       <li>
        Collection Overview
       </li>
       <li>
        The Collection Interfaces (List, Set, SortedSet)
       </li>
       <li>
        The Collection Classes (ArrayList, LinkedList, HashSet, TreeSet)
       </li>
       <li>
        Accessing a Collection via an Iterator
       </li>
       <li>
        Working with Maps
       </li>
      </ul>
      <h2>
       J2EE (JAVA 2 ENTERPRISE EDITION)
      </h2>
      <h2>
       INTRODUCTION TO JAVA
      </h2>
      <ul>
       <li>
        Introduction to J2EE Architecture
       </li>
       <ul>
        <li>
         Tier Architecture
        </li>
        <ul>
         <li>
          Single Tier
         </li>
         <li>
          Two Tier
         </li>
         <li>
          Three Tier
         </li>
         <li>
          N Tier
         </li>
        </ul>
       </ul>
      </ul>
      <h2>
       J2EE COMPONENTS
      </h2>
      <ul>
       <li>
        Web Components
       </li>
       <li>
        Business Components
       </li>
      </ul>
      <h2>
       J2EE CONTAINERS
      </h2>
      <ul>
       <li>
        Container Types
       </li>
       <li>
        Container Services
       </li>
      </ul>
      <h2>
       JDBC
      </h2>
      <ul>
       <li>
        Introduction to JDBC
       </li>
       <li>
        JDBC Drivers
       </li>
       <li>
        Statements
       </li>
       <li>
        Metadata
       </li>
       <li>
        Scrollable &amp; Updatable ResultSet
       </li>
       <li>
        Batch Updates
       </li>
      </ul>
      <h2>
       JAVA SERVLET
      </h2>
      <ul>
       <li>
        Introduction to Web
       </li>
       <li>
        Programming
       </li>
       <li>
        Advantages of Servlets
       </li>
       <li>
        Servlet Lifecycle
       </li>
       <li>
        Packing and Deployment
       </li>
       <li>
        Session Tracking
       </li>
       <li>
        Request Dispatching
       </li>
      </ul>
      <h2>
       PROJECT WORK
      </h2>
      <h2>
       JAVA SERVER PAGES (JSP)
      </h2>
      <ul>
       <li>
        JSP Architecture
       </li>
       <li>
        JSP Elements
       </li>
       <li>
        JSP Objects
       </li>
       <li>
        Custom Tags
       </li>
      </ul>
      <h2>
       Ajax
      </h2>
      <ul>
       <li>
        XMLHTTP Request
       </li>
       <li>
        Ready State
       </li>
       <li>
        Onreadystate Change
       </li>
       <li>
        ResponseText
       </li>
       <li>
        ResponseXML
       </li>
       <li>
        Status
       </li>
      </ul>
      <h2>
       Jquery
      </h2>
      <ul>
       <li>
        Jquery with Ajax
       </li>
       <li>
        Jquery Event
       </li>
       <li>
        Jquery Selectors
       </li>
       <li>
        JSON
       </li>
      </ul>
      <h2>
       RMI
      </h2>
      <ul>
       <li>
        Distributed Applications
       </li>
       <li>
        RMI Architecture
       </li>
       <li>
        Implementation
       </li>
      </ul>
      <h2>
       JAVA Mail API
      </h2>
      <h2>
       Utilities
      </h2>
      <ul>
       <li>
        My Eclipse
       </li>
       <li>
        Net Beans
       </li>
      </ul>
      <h2>
       Web Server
      </h2>
      <ul>
       <li>
        Apache Tomcat
       </li>
      </ul>
      <h2>
       Application Server
      </h2>
      <ul>
       <li>
        Bea's Web logic
       </li>
      </ul>
      <h2>
       Introduction to Framework
      </h2>
      <ul>
       <li>
        Spring
       </li>
       <li>
        Hibernate
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="javasixweekstraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="44 + 74 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="118">
       <input name="url" type="hidden" value="/javasixweekstraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>