<?php echo view("includes/header.php"); ?>

<section id="slider">
     <h5 style="position: absolute;">&nbsp;</h5>
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">

                <a href="javahadooptraining" target="_blank">
                    <img src="images/javahadooptraining.jpg" alt="Java Hadoop training">
                </a>
            </div>

            <div class="item">
                <a href="pythontraining" target="_blank"><img src="images/python6months.jpg" alt="pega training in noida"></a>
            </div>
            <div class="item">
                <a href="pegatraininginnoida" target="_blank"><img src="images/pegat.jpg" alt="pega training in noida"></a>
            </div>

            <div class="item">
                <a href="6-months-industrial-training" target="_blank"><img src="images/industrialtraining.jpg" alt="6 months industrial training"></a>
            </div>

            <div class="item">
                <a href="DucatIntegratedIndustrialProfessionalProgram" target="_blank">
                    <img src="images/slide0.jpg" alt="Ducat Integrated Industrial Professional Program - DIIPP"></a>
            </div>
            <div class="item">
                <img src="images/slide1.jpg" alt="dot net training">
            </div>
            <div class="item">
                <img src="images/slide3.jpg" alt="Ducat">
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>

<section id="serviceSection">

    <div class="container">

        <div class="row">

            <div class="col-md-3 text-center serviceBox">

                <div class="iconHolder">
                    <a href="global-alliances" style="color:#000"> <i class="icon fa fa-globe"></i></a>
                </div>

                <h4>GLOBAL ALLIANCES</h4>
                <p>We have a strong network of partners with a joint objective of helping our trainee become expert by maximizing</p>
                <a href="global-alliances">Read More</a>

            </div>
            <!-- End Of Col MD 3 -->

            <div class="col-md-3 text-center serviceBox">

                <div class="iconHolder">
                    <a href="become-an-instructor" style="color:#000"><i class="icon fa fa-pencil"></i></a>
                </div>

                <h4>BECOME AN INSTRUCTOR</h4>
                <p>Start Your career as a trainer of several technologies with the best place to work in education and training industry!</p>
                <a href="become-an-instructor">Read More</a>

            </div>
            <!-- End Of Col MD 3 -->

            <div class="col-md-3 text-center serviceBox">

                <div class="iconHolder">
                    <a href="upcoming-batches" style="color:#000"><i class="icon fa fa-pie-chart"></i></a>
                </div>

                <h4>UPCOMING BATCHES</h4>
                <p>Ducat is a leading name in IT Training Industry. Courses are for individuals as well as for corporates</p>
                <a href="upcoming-batches">Read More</a>

            </div>
            <!-- End Of Col MD 3 -->

            <div class="col-md-3 text-center serviceBox">

                <div class="iconHolder">
                    <a href="schedule-a-free-demo" style="color:#000"><i class="icon fa fa-mobile"></i></a>
                </div>

                <h4>SCHEDULE A FREE DEMO</h4>
                <p>Join Demonstration classes to get proper analysis in a best way about your technology and its bright future!</p>
                <a href="schedule-a-free-demo">Read More</a>

            </div>
            <!-- End Of Col MD 3 -->

        </div>
        <!-- End Of Row -->

    </div>
    <!-- End OF Container -->

</section>
<!-- End Of Main Container -->

<section id="banner" class="text-center">

    <div class="container">

        <div class="row">

            <div class="col-md-12 text-center">
                <a href="6-months-industrial-training" target="_blank"><img src="images/sixmonth.jpg" alt="6 months training" class="img-responsive"></a>

            </div>
            <!-- End Of Col MD 3 -->

        </div>
        <!-- End Of Row -->
        <h5>&nbsp;</h5>

    </div>
    <!-- End OF Container -->

</section>

<section id="events">
    <div class="container">
        <div class="row">

            <div class="col-md-12 text-center">

                <h2>EVENTS AT DUCAT</h2>
                <div class="separator short">
                    <div class="separator_line"></div>
                </div>

            </div>
            <!-- End Of Col MD 12 -->

        </div>
        <!-- End Of Row -->

        <div class="row">
            <div class="col-md-4">

                <div class="eventBox">

                    <img src="images/workshops.jpg" alt="ducat Event" class="img-responsive">

                    <div class="clearfix"></div>
                    <br>
                    <h4>WORKSHOP AT COLLEGES</h4>
          <br>

          <p>These face-to-face workshops bring student the latest developments, tools and resources</p>
          <a href="workshop-at-colleges">Continue Reading</a>

        </div> <!-- End Of Event Box -->

      </div> <!-- End Of Col MD 4 -->

      <div class="col-md-4">

        <div class="eventBox">

          <img src="images/placement-cell.jpg" alt="Event ducat " class="img-responsive">

          <div class="clearfix"></div>
          <br>
          <h4>OUR PLACEMENT CELL</h4>
          <br>

          <p>Through various global associations DUCAT provide job opportunity for the trainee!</p>
          <a href="our-placement-cell">Continue Reading</a>

        </div> <!-- End Of Event Box -->
        <!-- End Of Event Box -->

      </div> <!-- End Of Col MD 4 -->

      <div class="col-md-4">

        <div class="eventBox">

          <img src="images/workshop-at-ducat.jpg" alt="Event" class="img-responsive">

          <div class="clearfix"></div>
          <br>
          <h4>WORKSHOP AT DUCAT </h4>
          <br>

          <p>Bring you the latest developments, tools and resources to help!</p>
          <a href="jobfair-registration">Continue Reading</a>

        </div> <!-- End Of Event Box -->

      </div> <!-- End Of Col MD 4 -->

    </div> <!-- End Of Row -->

  </div> <!-- End OF Container -->

</section> <!-- End Of Events -->

<section id="courses">

    <div class="container">

        <div class="row">

            <div class="col-md-8">

                <img src="images/ducatindia.jpg" alt="Ducat India" class="img-responsive">

            </div>
            <!-- End Of Col MD 8 -->

            <div class="col-md-4 text-center">

                <h2>COURSES AT DUCAT</h2>
                <div class="separator short">
                    <div class="separator_line"></div>
                </div>

                <ul class="listStyle">
                    <li><i class="fa fa-check-square-o"></i> Programming Language</li>
                    <li><i class="fa fa-check-square-o"></i> CAD / Modeling</li>
                    <li><i class="fa fa-check-square-o"></i> Industry Trends</li>
                    <li><i class="fa fa-check-square-o"></i> Database</li>
                    <li><i class="fa fa-check-square-o"></i> Software Testing+</li>
                    <li><i class="fa fa-check-square-o"></i> Networking</li>
                    <li><i class="fa fa-check-square-o"></i> Graphics / Web Design</li>
                    <li><i class="fa fa-check-square-o"></i> Linux / Administration</li>
                    <li><i class="fa fa-check-square-o"></i> Mobile Application</li>
                    <li><i class="fa fa-check-square-o"></i> Electronics</li>
                </ul>

            </div>
            <!-- End Of Col MD 4 -->

        </div>
        <!-- End Of Row -->

    </div>
    <!-- End OF Container -->

</section>
<!-- End Of Courses -->

<section id="ourMentor">

    <div class="container">

        <div class="row">

            <div class="col-md-12 text-center">

                <h2>OUR MENTORS</h2>
                <div class="separator short">
                    <div class="separator_line"></div>
                </div>

            </div>
            <!-- End Of Col MD 12 -->

        </div>
        <!-- End Of Row -->

        <div class="row">

            <div class="col-md-6">
                <br/>
                <p>Mentors play an essential role in an institute, the level of education, development of student’s skills are based on their trainers. If you do not have a good mentor then you may lagin many things from others and that is why we at Ducat gives you the facility of skilled employees so that you do not feel unsecured about the academics.</p>

                <p>Personality development and academic status are some of those things which lie on mentor’s hands. If you are trained well then you can do well in your future and knowing its importance Ducat always tries to give you the best.</p>

                <p>We have a great team of skilled mentors who are always ready to direct their trainees in the best possible way they ca and to ensure the skills of mentors we held many skill development programs as well so that each and every mentor can develop their own skills with the demands of the companies so that they can prepare a complete packaged trainee.</p>

            </div>
            <!-- End Of Col MD 6 -->

        </div>
        <!-- End Of Row -->

    </div>
    <!-- End OF Container -->

</section>
<!-- End Of Our Mentor -->

<section id="clientCarousel">

    <div class="container">

        <div class="row">


            <div class="col-md-6 text-center">

                <h2>CLIENT TESTIMONIALS</h2>
                <div class="separator short">
                    <div class="separator_line"></div>
                </div>

                <div id="myCarouselClient" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarouselClient" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarouselClient" data-slide-to="1"></li>
                        <li data-target="#myCarouselClient" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="row">
                                <div class="col-md-3 text-left">

                                    <img src="images/logo-client.jpg" alt="Ducat client">

                                </div>
                                <!-- End Of Col MD 3 -->

                                <div class="col-md-9 text-left">

                                    <p>"Its been a very good experience working with DUCAT. The kind of students they have and the kind of training they give is commendable.The support from the team is also very good.Till date i</p>
                                    <span>- PRIYANKA, AM-HR (Zetasoft Technologies)</span>

                                </div>
                                <!-- End Of Col MD 9 -->
                            </div>
                            <!-- End Of Row -->
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-3 text-left">

                                    <img src="images/logo-client.jpg" alt="Ducat client">

                                </div>
                                <!-- End Of Col MD 3 -->

                                <div class="col-md-9 text-left">

                                    <p>"As you are aware that Logic Eastern is pioneer in Set Top Box Designing and Manufacturing. For recruitment we have conducted Campus Interview for Embedded Developer and System Administrator.
                                    </p>
                                    <span>- K.S.WALIS, Head (Technology and Quality) (LOGIC EASTERN)</span>

                                </div>
                                <!-- End Of Col MD 9 -->
                            </div>
                            <!-- End Of Row -->
                        </div>

                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarouselClient" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarouselClient" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </div>
            <!-- End Of Col MD 6 -->

            <div class="col-md-6 text-center">

                <h2>STUDENT SPEAK</h2>
                <div class="separator short">
                    <div class="separator_line"></div>
                </div>

                <div id="myCarouselSpeak" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarouselSpeak" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarouselSpeak" data-slide-to="1"></li>
                        <li data-target="#myCarouselSpeak" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="row">
                                <div class="col-md-3 text-left">

                                    <img src="images/logo-client.jpg" alt="Ducatindia">

                                </div>
                                <!-- End Of Col MD 3 -->

                                <div class="col-md-9 text-left">

                                    <p>"I would not make any difference between the experience and learning at ducat because as far as I am concerned without learning something there would be no experience."
                                    </p>
                                    <span>- AMIT BANSAL, SQL,PL/SQL,C,C++,ENGLISH CLASSES HCL TECHNOLOGIES</span>

                                </div>
                                <!-- End Of Col MD 9 -->
                            </div>
                            <!-- End Of Row -->
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-3 text-left">

                                    <img src="images/logo-client.jpg" alt="Ducat">

                                </div>
                                <!-- End Of Col MD 3 -->

                                <div class="col-md-9 text-left">

                                    <p>"Experience at DUCAT:-Extensive knowledge imparted by trainer in a six month training program of Microsoft .NET. Covered almost all the subject topics. Proper and valuable guidance provided during."
                                    </p>
                                    <span>- KHANSA IQBAL, Microsoft .NET FISERV INDIA PRIVATE LIMITED</span>

                                </div>
                                <!-- End Of Col MD 9 -->
                            </div>
                            <!-- End Of Row -->
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-3 text-left">

                                    <img src="images/logo-client.jpg" alt="Ducat">

                                </div>
                                <!-- End Of Col MD 3 -->

                                <div class="col-md-9 text-left">

                                    <p>"Experience in Ducat was very good for me because I got clear every basics point from Ducat family. Actually I was from Electronics & Communication Branch and having no knowledge of C, C++ but I want to be a .Net Developer."
                                    </p>
                                    <span>- PANKAJ SHARMA, .NET FISERV</span>

                                </div>
                                <!-- End Of Col MD 9 -->
                            </div>
                            <!-- End Of Row -->
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarouselSpeak" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarouselSpeak" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </div>
            <!-- End Of Col MD 6 -->

        </div>
        <!-- End Of Row -->

    </div>
    <!-- End OF Container -->

</section>
<!-- End Of Our Mentor -->

<section id="b2brelation">

    <div class="container">

        <div class="row">

            <div class="col-md-12 text-center">

                <h2>B2B RELATIONS</h2>
                <div class="separator short">
                    <div class="separator_line"></div>
                </div>

            </div>
            <!-- End Of Col MD 12 -->

        </div>
        <!-- End Of Row -->

        <div class="row">

            <div class="col-md-12 text-center">

                <img src="images/companies.png" alt="Ducat java training" class="img-responsive">

            </div>
            <!-- End Of Col MD 12 -->

        </div>
        <!-- End Of Row -->

    </div>
    <!-- End OF Container -->

</section>
<!-- End Of B2B Relation -->

<section id="gallery">

    <div class="container">

        <div class="row">

            <div class="col-md-12 text-center">

                <h2>DUCAT GALLERY</h2>
                <div class="separator short">
                    <div class="separator_line"></div>
                </div>

            </div>
            <!-- End Of Col MD 12 -->

        </div>
        <!-- End Of Row -->

        <div class="row">

            <div class="col-md-3 text-center">

                <a class="fancybox" href="images/st5.jpg" data-fancybox-group="gallery"><img src="images/s5.jpg" class="img-responsive" alt="java training" /></a>

            </div>
            <!-- End Of Col MD 3 -->

            <div class="col-md-3 text-center">

                <a class="fancybox" href="images/st2.jpg" data-fancybox-group="gallery"><img src="images/s2.jpg" class="img-responsive" alt="php training" /></a>

            </div>
            <!-- End Of Col MD 3 -->

            <div class="col-md-3 text-center">

                <a class="fancybox" href="images/st3.jpg" data-fancybox-group="gallery"><img src="images/s3.jpg" class="img-responsive" alt="dotnet training" /></a>

            </div>
            <!-- End Of Col MD 3 -->

            <div class="col-md-3 text-center">

                <a class="fancybox" href="images/st4.jpg" data-fancybox-group="gallery"><img src="images/s4.jpg" class="img-responsive" alt="embedded training" /></a>

            </div>
            <!-- End Of Col MD 3 -->

        </div>
        <!-- End Of Row -->

        <div class="col-md-3 text-center">

            <div class="counter">
                <span class="count">247689</span>
                <h5>OUR STUDENTS</h5>
            </div>

        </div>
        <!-- End Of Col MD 3 -->

        <div class="col-md-3 text-center">

            <div class="counter">
                <span class="count">146</span>
                <h5>OUR CONSULTANTS</h5>
            </div>

        </div>
        <!-- End Of Col MD 3 -->

        <div class="col-md-3 text-center">

            <div class="counter">
                <span class="count">123</span>
                <h5>OUR COURSES</h5>
            </div>

        </div>
        <!-- End Of Col MD 3 -->

        <div class="col-md-3 text-center">

            <div class="counter">
                <span class="count">794</span>
                <h5>OUR CLIENTS</h5>
            </div>

        </div>
        <!-- End Of Col MD 3 -->

    </div>
    <!-- End OF Container -->

</section>
<!-- End Of B2B Relation -->

<section id="offices" class="text-center">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h5>CORPORATE OFFICE NOIDA: <span>0120 - 4646464</span></h5>
                <p>GR.NOIDA: <span>0120-4345190</span> GHAZIABAD: <span>0120-4835400</span> FARIDABAD: <span>0129-4150605</span> GURGAON: <span>0124-4219095</span> </p>

            </div>
            <!-- End Of Col MD 12 -->

        </div>
        <!-- End Of Row -->

    </div>
    <!-- End OF Container -->

</section>
<!-- End Of Office -->






<?php echo view("includes/footer.php"); ?>