<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Xamarin
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Xamarin
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      Xamarin Training
     </h4>
     <p>
      Ducat is one of the most recommended Xamarin Training Institute in Delhi NCR that offers hands on practical knowledge / practical implementation on live projects and will ensure the job with the help of advance level Xamarin Training Courses. At Ducat Xamarin Training is conducted by Industry Expert.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction to Xamarin
      </h2>
      <ul>
       <li>
        Xamarin vs. Hybrid Framework vs. Native Framework
       </li>
       <li>
        Xamarin Development IDE  Visual Studio and Xamarin Studio
       </li>
       <li>
        Xamarin Architecture
       </li>
       <li>
        Introduction to Mono
       </li>
      </ul>
      <h2>
       Xamarin Development
      </h2>
      <ul>
       <li>
        Xamarin Cross platform solutions
       </li>
       <li>
        Xamarin Family
       </li>
       <li>
        Xamarin Development Approaches
       </li>
       <li>
        Xamarin Advantages
       </li>
       <li>
        Xamarin Disadvantages
       </li>
      </ul>
      <h2>
       Setup the Development Environment on Windows
      </h2>
      <ul>
       <li>
        Setting up Xamarin on Windows
       </li>
       <li>
        Configuring Visual Studio and SDK for Android and Windows UWP
       </li>
       <li>
        Configuring Visual Studio Emulator for Android and UWP
       </li>
       <li>
        Verify your Xamarin environment on Windows
       </li>
      </ul>
      <h2>
       Setup the Development Environment on Mac
      </h2>
      <ul>
       <li>
        Setting up Xamarin on Mac
       </li>
       <li>
        Configuring XCode, Visual Studio and Android SDK
       </li>
       <li>
        Verify your Xamarin environment
       </li>
      </ul>
      <h2>
       Xamarin Project Types
      </h2>
      <ul>
       <li>
        Xamarin Shared Projects
       </li>
       <li>
        Xamarin Portable Class Libraries
       </li>
      </ul>
      <h2>
       XAMARIN.ANDROID
      </h2>
      <ul>
       <li>
        Introduction to Android
       </li>
       <li>
        Android Architecture
       </li>
       <li>
        Introduction to Xamarin.Android
       </li>
       <li>
        Xamarin.Android Application Structure
       </li>
       <li>
        Xamarin.Android Architecture
       </li>
       <li>
        Xamarin.Android Code Compilation
       </li>
       <li>
        Xamarin.Android APIs
       </li>
      </ul>
      <h2>
       Android fundamentals
      </h2>
      <ul>
       <li>
        Activities
       </li>
       <li>
        Views
       </li>
       <li>
        Layouts
       </li>
       <li>
        Resources
       </li>
       <li>
        Manifest
       </li>
      </ul>
      <h2>
       Android Activity
      </h2>
      <ul>
       <li>
        Activity Life Cycle
       </li>
       <li>
        Activity Life Cycle Events
       </li>
       <li>
        Activity States
       </li>
      </ul>
      <h2>
       Running, Deployment, Testing
      </h2>
      <ul>
       <li>
        Deploying app to Emulator and Devices
       </li>
       <li>
        Debugging in Emulator
       </li>
       <li>
        Debugging in Device
       </li>
       <li>
        Android Debug Log
       </li>
       <li>
        Testing app in different emulator
       </li>
      </ul>
      <h2>
       XAMARIN.IOS
      </h2>
      <ul>
       <li>
        Introduction to iOS
       </li>
       <li>
        iOS Architecture
       </li>
       <li>
        Introduction to Xamarin.iOS
       </li>
       <li>
        Xamarin.iOS Application Structure
       </li>
       <li>
        Xamarin.iOS Architecture
       </li>
       <li>
        Xamarin.iOS Code Compilation (AOT)
       </li>
       <li>
        Xamarin.iOS APIs
       </li>
      </ul>
      <h2>
       iOS fundamentals
      </h2>
      <ul>
       <li>
        Storyboard
       </li>
       <li>
        Segues
       </li>
       <li>
        Views
       </li>
       <li>
        View Controllers
       </li>
       <li>
        Layouts
       </li>
       <li>
        Resources
       </li>
      </ul>
      <h2>
       iOS Life Cycle
      </h2>
      <ul>
       <li>
        iOS Life Cycle
       </li>
       <li>
        iOS Life Cycle States
       </li>
      </ul>
      <h2>
       Running, Deployment, Testing
      </h2>
      <ul>
       <li>
        Deploying app to Simulator and Devices
       </li>
       <li>
        Debugging in Simulator
       </li>
       <li>
        Debugging in Device
       </li>
       <li>
        Testing app in different Simulator
       </li>
      </ul>
      <h2>
       Windows UWP
      </h2>
      <ul>
       <li>
        Page
       </li>
       <li>
        Controls
       </li>
       <li>
        Layouts
       </li>
       <li>
        Resources
       </li>
      </ul>
      <h2>
       Windows UWP Life Cycle
      </h2>
      <ul>
       <li>
        Windows UWP Life Cycle
       </li>
       <li>
        Windows UWP Life Cycle States
       </li>
       <li>
        Running, Deployment, Testing
       </li>
       <li>
        Deploying app to Emulator and Devices
       </li>
       <li>
        Debugging in Emulator
       </li>
       <li>
        Debugging in Device
       </li>
       <li>
        Testing app in different Emulators
       </li>
      </ul>
      <h2>
      </h2>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>