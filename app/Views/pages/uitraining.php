<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     UI SPECIALIZATION
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     UI SPECIALIZATION
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      UI SPECIALIZATION BY DUCAT
     </h4>
     <p>
      UI Specialization course enable you to design and development excellent looking browser-based user interfaces and UI web solutions from the requirements stage to deployment onto the production web farm.In information technology, the user interface (UI) is everything designed into an information device with which a human being may interact. The UI Developer operates effectively as a member of the development team,Also operates effectively as an individual for quick turnaround of enhancements and fixes. This Course is Designed For 1)Job seekers who want to double their chances to get placement as User Interface Developer 2)Developers who are already familiar with any Server Side Languange and want to improve their knowledge of advanced development techniques. 3)Professional Designer who want to write more efficient , secure and Clean code.
     </p>
     <div class="contentAcc">
      <h2>
       HTML 8 Hrs.
      </h2>
      <ul>
       <li>
        What is HTML?
       </li>
       <li>
        What is a Web Browser?
       </li>
       <li>
        What are Versions of HTML?
       </li>
       <li>
        What can You Do with HTML?
       </li>
       <li>
        HTML Development Environments
       </li>
       <li>
        Using a WYSIWYG Editor
       </li>
       <li>
        Using an HTML Editor
       </li>
       <li>
        Writing Code with a Text Editor
       </li>
       <li>
        Publishing Documents
       </li>
      </ul>
      <h2>
       Review of HTML Elements
      </h2>
      <ul>
       <li>
        Rules of Syntax
       </li>
       <li>
        Making your Code Readable
       </li>
       <li>
        Making your Code XHTML Compliant
       </li>
       <li>
        Building a Document
       </li>
       <li>
        Using Colors
       </li>
       <li>
        Adding Color to your Page
       </li>
       <li>
        Using Headings
       </li>
       <li>
        Using Paragraphs
       </li>
       <li>
        Using Block quotes
       </li>
       <li>
        Using Entities
       </li>
       <li>
        Aligning Block-Level Elements
       </li>
      </ul>
      <h2>
       Inserting Spaces and Line Breaks
      </h2>
      <ul>
       <li>
        Displaying Preformatted Text
       </li>
       <li>
        Formatting with Inline Elements
       </li>
       <li>
        Controlling Fonts
       </li>
       <li>
        Introducing List Elements
       </li>
       <li>
        Creating Unordered Lists
       </li>
       <li>
        Creating Ordered Lists
       </li>
       <li>
        Creating Definition Lists
       </li>
       <li>
        Nesting Lists Indenting Text with the &lt;ul&gt; Tag
       </li>
      </ul>
      <h2>
       What is an HTML Table?
      </h2>
      <ul>
       <li>
        Building a Table
       </li>
       <li>
        Using the Border Attribute
       </li>
       <li>
        Cell Padding and Cell Spacing
       </li>
       <li>
        Controlling Table and Cell Width
       </li>
       <li>
        Aligning a Table on the Page
       </li>
       <li>
        Aligning Tables and Text
       </li>
       <li>
        Aligning Table Data
       </li>
       <li>
        Spanning Columns and Rows
       </li>
       <li>
        Nesting Tables
       </li>
       <li>
        Adding Color to Tables
       </li>
       <li>
        Using Tables as a Design Tool
       </li>
      </ul>
      <h2>
       Creating a Hyperlink
      </h2>
      <ul>
       <li>
        Understanding and Using URLs
       </li>
       <li>
        Linking to a Web Document
       </li>
       <li>
        Linking to a Local Document
       </li>
       <li>
        Linking to Anchors
       </li>
       <li>
        Opening a New Browser Window
       </li>
       <li>
        iFrame
       </li>
      </ul>
      <h2>
       Graphic File Formats
      </h2>
      <ul>
       <li>
        Optimizing Image and File Size
       </li>
       <li>
        Inserting Inline Images, ImageMap, Sprite Image
       </li>
       <li>
        Aligning and Formatting Images
       </li>
       <li>
        Using Images to Anchor Links
       </li>
       <li>
        Creating a Look-and-Feel
       </li>
       <li>
        Sizing and Scaling Images
       </li>
       <li>
        Using Transparent Images
       </li>
       <li>
        Using GIF Animation
       </li>
      </ul>
      <h2>
       Forms and Controls
      </h2>
      <ul>
       <li>
        Forms , Form Elements
       </li>
       <li>
        Form Actions, Form Methods , Form Design
       </li>
      </ul>
      <h2>
       CSS 3.0 8 Hrs.
      </h2>
      <h2>
       Model-1 1 Hrs.
      </h2>
      <h2>
       Introduction to CSS3.0
      </h2>
      <h2>
       What's new in CSS 3.0
      </h2>
      <h2>
       r
      </h2>
      <ul>
       <li>
        border-image
       </li>
       <li>
        border-radius
       </li>
      </ul>
      <h2>
       hadows
      </h2>
      <ul>
       <li>
        Text-shadow
       </li>
       <li>
        Box-shadow
       </li>
      </ul>
      <h2>
       round
      </h2>
      <ul>
       <li>
        background-clip
       </li>
       <li>
        background-size
       </li>
       <li>
        background-origin
       </li>
       <li>
        background-image
       </li>
      </ul>
      <h2>
       Model-2 1 Hrs.
      </h2>
      <h2>
       ne
      </h2>
      <h2>
       itions
      </h2>
      <ul>
       <li>
        transition
       </li>
       <li>
        transition-delay
       </li>
       <li>
        transition-duration
       </li>
       <li>
        transition-property
       </li>
      </ul>
      <h2>
       form
      </h2>
      <h2>
       ansforms
      </h2>
      <ul>
       <li>
        transform
       </li>
       <li>
        matrix()
       </li>
       <li>
        translate(x,y)
       </li>
       <li>
        scale(x,y)
       </li>
      </ul>
      <h2>
       Model-3 1 Hrs.
      </h2>
      <h2>
       ansforms
      </h2>
      <ul>
       <li>
        transform
       </li>
       <li>
        transform-style
       </li>
       <li>
        perspective
       </li>
       <li>
        transform-origin
       </li>
      </ul>
      <h2>
       Model-4 1 Hrs.
      </h2>
      <h2>
       tions
      </h2>
      <ul>
       <li>
        @keyframes
       </li>
       <li>
        animation
       </li>
       <li>
        animation-direction
       </li>
       <li>
        animation-duration
       </li>
       <li>
        animation-name
       </li>
      </ul>
      <h2>
       Model-5 1 Hrs.
      </h2>
      <h2>
       tors
      </h2>
      <ul>
       <li>
        CSS combinations
       </li>
       <li>
        Pseudo Elements
       </li>
      </ul>
      <h2>
       ents
      </h2>
      <ul>
       <li>
        Linear Gradients
       </li>
       <li>
        Radial Gradients
       </li>
      </ul>
      <h2>
       ple Columns
      </h2>
      <ul>
       <li>
        column-count
       </li>
       <li>
        column-fill
       </li>
       <li>
        column-gap
       </li>
       <li>
        column-width
       </li>
       <li>
        columns
       </li>
       <li>
        column-rule
       </li>
       <li>
        column-rule-color
       </li>
       <li>
        column-rule-style
       </li>
       <li>
        column-rule-width
       </li>
      </ul>
      <h2>
       Model-6 1 Hrs.
      </h2>
      <h2>
       Interface
      </h2>
      <ul>
       <li>
        resize
       </li>
       <li>
        box-sizing
       </li>
       <li>
        outline-offset
       </li>
      </ul>
      <h2>
       ilters
      </h2>
      <ul>
       <li>
        Blur
       </li>
       <li>
        Brightness
       </li>
       <li>
        Contrast
       </li>
       <li>
        Grayscale
       </li>
       <li>
        Hue-rotate
       </li>
       <li>
        Invert
       </li>
       <li>
        Opacity
       </li>
       <li>
        Saturate
       </li>
       <li>
        Sepia
       </li>
       <li>
        Drop-shadow
       </li>
       <li>
        rotate(angle)
       </li>
       <li>
        skew(x-angle,y-angle)
       </li>
      </ul>
      <h2>
       Model-7 1 Hrs.
      </h2>
      <h2>
       Query
      </h2>
      <ul>
       <li>
        What is Responsive Web Design
       </li>
       <li>
        Intro to the Viewport
       </li>
       <li>
        The Viewport Tag
       </li>
       <li>
        Media Queries
       </li>
       <li>
        Fluid Layouts
       </li>
       <li>
        Tablet Styles
       </li>
       <li>
        Mobile Styles
       </li>
       <li>
        Making a Mobile Drop-down Menu
       </li>
       <li>
        Responsive Images &amp; Polyfills
       </li>
      </ul>
      <h2>
       Model-8 1 Hrs.
      </h2>
      <h2>
       onts
      </h2>
      <ul>
       <li>
        @font-face
       </li>
       <li>
        font-family
       </li>
       <li>
        src
       </li>
       <li>
        font-stretch
       </li>
       <li>
        font-style
       </li>
       <li>
        font-weight
       </li>
      </ul>
      <h2>
       ox
      </h2>
      <ul>
       <li>
        flex-grow
       </li>
       <li>
        flex-shrink
       </li>
       <li>
        flex-basis
       </li>
       <li>
        flex
       </li>
       <li>
        flex-wrap
       </li>
       <li>
        flex-direction
       </li>
       <li>
        flex-flow
       </li>
       <li>
        justify-content
       </li>
       <li>
        align-items
       </li>
       <li>
        order
       </li>
      </ul>
      <h2>
       cript  26 Hrs.
      </h2>
      <ul>
       <li>
        Introduction to JavaScript
       </li>
       <li>
        Origins of JavaScript
       </li>
       <li>
        JavaScript Characteristics
       </li>
       <li>
        Common Programming Concepts
       </li>
       <li>
        Java and JavaScript
       </li>
       <li>
        Server-side vs. Client-side Applications
       </li>
       <li>
        Annotating Your Code with Comments
       </li>
       <li>
        Working with Variables and Data
       </li>
       <li>
        Communicating with the User
       </li>
       <li>
        Using Data More Than Once: Variables
       </li>
       <li>
        JavaScript Reserved and Keywords
       </li>
       <li>
        Expressions and Operators
       </li>
       <li>
        Inline Scripting, Simple User Events, and the onLoad and onUnload Event Handlers
       </li>
       <li>
        Functions, Methods, and Events
       </li>
       <li>
        Methods as Functions
       </li>
       <li>
        Conditional Operators
       </li>
       <li>
        Defining and Calling a Function
       </li>
       <li>
        The confirm() Method and Forms
       </li>
       <li>
        User Events and JavaScript Event Handlers
       </li>
       <li>
        Developing Interactive Forms
       </li>
       <li>
        Form Controls
       </li>
       <li>
        Referring to form Objects
       </li>
       <li>
        The button, checkbox, text, textarea, radio button, and select Objects
       </li>
       <li>
        Controlling Program Flow
       </li>
       <li>
        The if...else, while, for, break, and continue Statements
        <li>
         The Modules Operator
        </li>
        <li>
         Using continue in a while Loop
        </li>
        <li>
         The switch Statement
        </li>
        <li>
         The do...while Statement
        </li>
        <li>
         JavaScript Object Model
        </li>
        <li>
         JavaScript Object Hierarchy Model
        </li>
        <li>
         Commonly Used Objects
        </li>
        <li>
         The window, document, history, and location Objects
        </li>
        <li>
         JavaScript Language Objects
        </li>
        <li>
         The String, Array, Date, and Math Objects
        </li>
        <li>
         Evaluating Strings
        </li>
        <li>
         Setting and Extracting Time Information
        </li>
       </li>
      </ul>
      <h2>
       Jquery 10 Hrs.
      </h2>
      <ul>
       <li>
        Getting Started with jQuery
       </li>
       <li>
        Selecting Elements
       </li>
       <li>
        Manipulating the Page
       </li>
       <li>
        Traversing the DOM and Chaining
       </li>
       <li>
        jQuery Utility Methods
       </li>
       <li>
        Handling Events and Event Delegation
       </li>
       <li>
        AJAX, JSON and Deferreds
       </li>
       <li>
        Enhancing with Animation Effects
       </li>
       <li>
        Grids , Tables with Ajax , Pagination, JQuery UI
       </li>
       <li>
        jQuery Best Practices
       </li>
      </ul>
      <h2>
       HTML 5 10 Hrs.
      </h2>
      <li>
       Page Structure
      </li>
      <li>
       New HTML5 Structural Tags
      </li>
      <li>
       Page Simplification
      </li>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>