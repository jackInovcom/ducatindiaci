<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     SEO
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     SEO
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      TRAINING THAT MAKES YOU FOCUS ON THE CORRECT BUSINESS
     </h4>
     <p>
      Today's market is competitive and one has to be top in his field to make profits and stay in the business. Proper techniques and methods are needed to start a business.SEO b2b business through internet is a new profit making market which is booming. DUCAT can make one an expert in that field ready to compete with the big shots. This SEO training from DUCAT will specifically teach you the entire optimization process, ranking of your websites, how it works, how to improve the volume of traffic and quality and much more. A person can become a pro in no time. Proper guidance will be received and one will be able to manage a business out of it in no time. Internet and website access are done by approximately 90% of the people in the world. Among which near about 21 billion web pages are fighting to reach the top and to hold the first ranking position. There are more billion of companies and their websites getting launched in the market worldwide, it can be the best possible business for anyone. DUCAT and its faculties can help a person to can them know the market which will provide them a successful career.
     </p>
     <div class="contentAcc">
      <h2>
       SEARCH ENGINES
      </h2>
      <h2>
       Types of SEO
      </h2>
      <ul>
       <li>
        White Hat SEO
       </li>
       <li>
        Grey Hat SEO
       </li>
       <li>
        Black Hat SEO
       </li>
      </ul>
      <h2>
       What are search Engines?
      </h2>
      <ul>
       <li>
        Types of Search Engines
       </li>
       <li>
        How Search Engine Works and how they rank websites based upon a search term?
       </li>
       <li>
        What are Directories and how they differ from Search Engines?
       </li>
       <li>
        Difference between Search Engine and Directories
       </li>
       <li>
        What is site Popularity?
       </li>
       <li>
        How do you monitor the performance of your website in the search engines?
       </li>
       <li>
        What is the importance of search for website and how can SEO save valuable money in advertising expenses?
       </li>
       <li>
        Areas of Operation for Search Engine Optimization Professionals
       </li>
       <li>
        Uploading websites through FTP Software's like Filezilla Core FTP etc
       </li>
       <li>
        Dreamweaver, Basic of HTML
       </li>
       <li>
        HTML Static &amp; Dynamic Website Optimization
       </li>
       <li>
        About Online Digital Marketing
       </li>
       <li>
        Affiliate Marketing
       </li>
      </ul>
      <h2>
       On-page Optimization Activities
      </h2>
      <ul>
       <li>
        Initial Site Analysis
       </li>
       <li>
        Importance of Domain names and Value
       </li>
       <li>
        Domain Selection
       </li>
       <li>
        Website Structure and Navigation Menu Optimization
       </li>
       <li>
        Advance Keyword Research
       </li>
       <li>
        Keyword Analysis
       </li>
       <li>
        Keyword Planning
       </li>
       <li>
        Keyword Application on Website
       </li>
       <li>
        Keyword Competition Review
       </li>
       <li>
        How to do SEO for Dynamic CMS like Joomla Wordpress Drupal etc
       </li>
       <li>
        Site Structure Analysis
       </li>
       <li>
        Title and Meta Tag Development
       </li>
       <li>
        H1, H2, H3 tags
       </li>
       <li>
        Anchor Text
       </li>
       <li>
        Header and Footer Links
       </li>
       <li>
        Image tag and it's attributes
       </li>
       <li>
        HTML validation using W3C
       </li>
       <li>
        URL renaming/re-writing
       </li>
       <li>
        Content Development Check
       </li>
       <li>
        Existing Web Content Optimization
       </li>
       <li>
        Creation of XML/HTML/Text Sitemaps
       </li>
       <li>
        Submitting Sites to Google and yahoo Webmasters
       </li>
       <li>
        Canonical/404 Implementation
       </li>
       <li>
        Robots.txt Creation
       </li>
       <li>
        Internal Link Strategy
       </li>
       <li>
        Google SEO Guideline
       </li>
      </ul>
      <h2>
       Off-page Optimization Activities
      </h2>
      <ul>
       <li>
        Introduction to off-Page Optimization
       </li>
       <li>
        Link Building and Link Exchange strategies
       </li>
       <li>
        Google Page Rank
       </li>
       <li>
        Directory Submission
       </li>
       <li>
        Search Engine Submission
       </li>
       <li>
        Social Bookmarking
       </li>
       <li>
        Article Distribution
       </li>
       <li>
        Press Release Distribution
       </li>
       <li>
        Google Map Submission
       </li>
       <li>
        Viral Marketing
       </li>
       <li>
        E-mail Marketing
       </li>
       <li>
        Geo Listing and Local Listing includes Classified
       </li>
       <li>
        Local and Regional Search Engine Indexing
       </li>
       <li>
        RSS feeds Submission
       </li>
       <li>
        Video Submissions
       </li>
       <li>
        Blog Creation, Weekly Posting and Blog Promotion
       </li>
       <li>
        Forum Posting
       </li>
      </ul>
      <h2>
       SMO (Social Media Optimization):
      </h2>
      <ul>
       <li>
        How to optimize Social media and use it in favor of your Brand Building
       </li>
       <li>
        How to do promotion on Social Networking Sites like:
       </li>
       <li>
        Facebook
       </li>
       <li>
        Twitter
       </li>
       <li>
        Linkeding and Many more
       </li>
       <li>
        How to do Promotion of your Sites using
       </li>
       <li>
        Multiple Sharing Options:
       </li>
       <li>
        Yahoo Buzz
       </li>
       <li>
        Digg
       </li>
       <li>
        Stumble upon and Many more
       </li>
      </ul>
      <h2>
       In-Organic Search
      </h2>
      <ul>
       <li>
        PPC Campain Management
       </li>
       <li>
        Google Adsense.
       </li>
       <li>
        Google Adwords (pay per Click )
       </li>
       <li>
        Google keywords Selection Tool.
       </li>
       <li>
        Google Web Analytic
       </li>
       <li>
        Google webmaster tool
       </li>
       <li>
        Ranking Report
       </li>
       <li>
        Keywords Ranking Report
       </li>
      </ul>
      <h2>
       Additional Tactics
      </h2>
      <ul>
       <li>
        Website &amp; Competitor Analysis
       </li>
       <li>
        Website Content &amp; Architecture Analysis.
       </li>
       <li>
        Keywords Research &amp; Analysis
       </li>
       <li>
        Creating Effective Title.
       </li>
       <li>
        How to write SEO Friendly Content.
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="seotraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="23 + 10 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="33">
       <input name="url" type="hidden" value="/seotraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>