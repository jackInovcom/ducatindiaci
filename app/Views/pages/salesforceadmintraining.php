<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     SALESFORCE ADMINISTRATION
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     SALESFORCE ADMINISTRATION
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      LEARN LATEST SALESFORCE ADMINISTRATION TRAINING
     </h4>
     <p>
      This course is for experienced administrators who are ready to increase their skills and knowledge about how Salesforce can solve their most pressing business needs. Using realworld scenarios, Administration Essentials for Experienced Admins covers topics such as extending Salesforce with custom objects and apps, generating complex reports and dashboards, and automating complex business processes to help you work more efficiently and get more from Salesforce. This comprehensive hands-on course is a must for new Salesforce administrators. Administration Essentials for New Admins is the core training that ensures your success with Salesforce. For maximum benefit, we recommend administrators take this course before starting a Salesforce deployment or when taking over an existing deployment. This comprehensive hands-on course is a must for all administrators of Salesforce Professional Edition. Administration Essentials is the core training that ensures your success with Salesforce. For maximum benefit, we recommend administrators take this cours ebefore starting a Salesforce deployment or when taking over an existing deployment.
     </p>
     <div class="contentAcc">
      <h2>
       Troubleshoot Record Access and Field Visibility
      </h2>
      <ul>
       <li>
        Troubleshooting Record Access
       </li>
       <li>
        Understanding the Impact of Territory Management on the Sharing Model
       </li>
       <li>
        Managing Field Visibility
       </li>
      </ul>
      <h2>
       Change Management
      </h2>
      <ul>
       <li>
        Managing Changes in a Sandbox
       </li>
       <li>
        Deploying Changes Using Change Sets
       </li>
      </ul>
      <h2>
       Extend Salesforce with Custom Objects and Apps
      </h2>
      <ul>
       <li>
        Building Custom Objects, Tabs, and Apps
       </li>
       <li>
        Creating Relationships between Objects
       </li>
       <li>
        Using Apps from the AppExchange
       </li>
      </ul>
      <h2>
       Extend Analytics
      </h2>
      <ul>
       <li>
        Creating Custom Report Types
       </li>
       <li>
        Building Exception Reports with Cross Filters
       </li>
       <li>
        Categorizing Report Data with Bucketing
       </li>
       <li>
        Extending Summaries in Reports and Dashboards
       </li>
       <li>
        Displaying Multiple Views of Data Using Joined Reports
       </li>
       <li>
        Analyzing Data Over Time with Analytic Snapshots
       </li>
      </ul>
      <h2>
       Improve Data Quality
      </h2>
      <ul>
       <li>
        Assessing, Cleansing, and Maintaining Data Quality
       </li>
       <li>
        Merging Duplicate Records
       </li>
       <li>
        Cleaning and Enriching Data with Data.com
       </li>
      </ul>
      <h2>
       Automate Complex Business Processes
      </h2>
      <ul>
       <li>
        Creating Formulas and Workflow Rules to Enforce
       </li>
       <li>
        Business Processes
       </li>
       <li>
        Troubleshooting Business Process Execution Issues
       </li>
       <li>
        Understanding When Business Process Automation
       </li>
       <li>
        Must Be Extended Using Programmatic Means
       </li>
      </ul>
      <h2>
       Streamline Requests with Approval Processes
      </h2>
      <ul>
       <li>
        Managing Approval Processes
       </li>
       <li>
        Troubleshooting Approval Processes
       </li>
      </ul>
      <h2>
       Increase Productivity with Visual Workflow
      </h2>
      <ul>
       <li>
        Understanding Use Cases for Visual Workflow
       </li>
       <li>
        Building and Deploying a Flow
       </li>
      </ul>
      <h2>
       Administration Essentials for New Admins (Enterprise Edition/Unlimited Edition)
      </h2>
      <h2>
       Getting Around the App
      </h2>
      <ul>
       <li>
        Data Model and Navigation
       </li>
       <li>
        Help &amp; Training
       </li>
      </ul>
      <h2>
       Setting up the User Interface
      </h2>
      <ul>
       <li>
        Setting Up the UI and Search Options
       </li>
      </ul>
      <h2>
       Getting Your Organization Ready for Users
      </h2>
      <ul>
       <li>
        Setting Up the Company Profile
       </li>
       <li>
        Configuring the UI
       </li>
       <li>
        Configuring Search Settings
       </li>
      </ul>
      <h2>
       Setting Up and Managing Users
      </h2>
      <ul>
       <li>
        Managing User Profiles
       </li>
       <li>
        Managing Users
       </li>
       <li>
        Troubleshooting Login Issues
       </li>
      </ul>
      <h2>
       Security and Data Access
      </h2>
      <ul>
       <li>
        Restricting Logins
       </li>
       <li>
        Determining Object Access
       </li>
       <li>
        Setting Up Record Access
       </li>
       <li>
        Creating a Role Hierarchy
       </li>
       <li>
        Dealing with Record Access Exceptions
       </li>
       <li>
        Managing field-level security
       </li>
      </ul>
      <h2>
       Customization: Fields
      </h2>
      <ul>
       <li>
        Administrating Standard Fields
       </li>
       <li>
        Creating New Custom Fields
       </li>
       <li>
        Creating Selection Fields: Picklists and Lookups
       </li>
       <li>
        Creating Formula Fields
       </li>
       <li>
        Working with Page Layouts
       </li>
       <li>
        Working with Record Types and Business Processes
       </li>
       <li>
        Maintaining data quality
       </li>
      </ul>
      <h2>
       Managing Data
      </h2>
      <ul>
       <li>
        Import Wizards
       </li>
       <li>
        Data Loader
       </li>
       <li>
        Mass Transfer
       </li>
       <li>
        Backing Up Data
       </li>
       <li>
        Mass Delete and the Recycle Bin
       </li>
      </ul>
      <h2>
       Reports and Dashboards
      </h2>
      <ul>
       <li>
        Running and Modifying Reports
       </li>
       <li>
        Creating New Reports with the Report Builder
       </li>
       <li>
        Working with Report Filters
       </li>
       <li>
        Summarizing with Formulas and Visual Summaries
       </li>
       <li>
        Printing, Exporting, and Emailing Reports
       </li>
       <li>
        Building Dashboards
       </li>
      </ul>
      <h2>
       Automation
      </h2>
      <ul>
       <li>
        Workflow Rules
       </li>
       <li>
        Lead and Case Automation
       </li>
      </ul>
      <h2>
       Collaboration
      </h2>
      <ul>
       <li>
        Chatter and Chatter Free
       </li>
       <li>
        Email Administration and Email Templates
       </li>
       <li>
        Tracking Tasks and Events
       </li>
      </ul>
      <h2>
       Service Cloud
      </h2>
      <ul>
       <li>
        Automating Support
       </li>
       <li>
        Understanding the Service Cloud Console
       </li>
       <li>
        Collaborating in the Service Cloud
       </li>
       <li>
        Analyzing Support Data: Support Reports and Dashboards
       </li>
      </ul>
      <h2>
       Administration Essentials Professional Edition
      </h2>
      <h2>
       Getting Around the App
      </h2>
      <ul>
       <li>
        Data Model and Navigation
       </li>
       <li>
        Help &amp; Training
       </li>
      </ul>
      <h2>
       Getting Your Organization Ready for Users
      </h2>
      <ul>
       <li>
        Setting Up the Company Profile
       </li>
       <li>
        Setting Up Currencies
       </li>
       <li>
        Configuring the User Interface
       </li>
       <li>
        Configuring Search Settings
       </li>
      </ul>
      <h2>
       Setting Up and Managing Users
      </h2>
      <ul>
       <li>
        Understanding User Profiles
       </li>
       <li>
        Managing Users
       </li>
       <li>
        Troubleshooting Login Issues
       </li>
       <li>
        Computer Activation
       </li>
      </ul>
      <h2>
       Security and Data Access
      </h2>
      <ul>
       <li>
        Setting Up Record Access
       </li>
       <li>
        Creating a Role Hierarchy
       </li>
       <li>
        Dealing with Record Access Exceptions
       </li>
      </ul>
      <h2>
       Customization: Fields
      </h2>
      <ul>
       <li>
        Administrating Standard Fields
       </li>
       <li>
        Creating New Custom Fields
       </li>
       <li>
        Creating Selection Fields: Picklists and Lookups
       </li>
       <li>
        Creating Formula Fields
       </li>
       <li>
        Working with Page Layouts
       </li>
       <li>
        Working with Record Types and Business Processes
       </li>
       <li>
        Maintaining Data Quality with Validation Rules
       </li>
      </ul>
      <h2>
       Managing Data
      </h2>
      <ul>
       <li>
        Import Wizards
       </li>
       <li>
        Mass Transfer
       </li>
       <li>
        Backing Up Data
       </li>
       <li>
        Mass Delete and the Recycle Bin
       </li>
      </ul>
      <h2>
       Reports and Dashboards
      </h2>
      <ul>
       <li>
        Running and Modifying Reports
       </li>
       <li>
        Creating New Reports with the Report Builder
       </li>
       <li>
        Working with Report Filters
       </li>
       <li>
        Summarizing with Formulas and Visual Summaries
       </li>
       <li>
        Printing, Exporting, and Emailing Reports
       </li>
       <li>
        Building Dashboards
       </li>
      </ul>
      <h2>
       Automation
      </h2>
      <ul>
       <li>
        Automating Leads and Cases
       </li>
       <li>
        Creating Queues and Assignment Rules
       </li>
       <li>
        Creating Auto-Response Rules and Web Forms
       </li>
      </ul>
      <h2>
       Collaboration
      </h2>
      <ul>
       <li>
        Using Chatter
       </li>
       <li>
        Email Administration and Email Templates
       </li>
       <li>
        Tracking Tasks and Events
       </li>
      </ul>
      <h2>
       Industry Interface Program
      </h2>
      <h2>
       Projects
      </h2>
      <ul>
       <li>
        Modular Assignments
       </li>
       <li>
        Mini Projects
       </li>
      </ul>
      <h2>
       Domains / Industry
      </h2>
      <ul>
       <li>
        Retail Industry
       </li>
       <li>
        Banking &amp; Finance
       </li>
       <li>
        Service
       </li>
       <li>
        E-Commerce
       </li>
       <li>
        Manufacturing &amp; Production
       </li>
       <li>
        Web Application Development
       </li>
       <li>
        Research &amp; Analytics
       </li>
       <li>
        HR &amp; Consultancy
       </li>
       <li>
        FMCG
       </li>
       <li>
        Consumer Electronics
       </li>
       <li>
        Event Management Industry
       </li>
       Telecom
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="salesforceadmintraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="53 + 14 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="67">
       <input name="url" type="hidden" value="/salesforceadmintraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>