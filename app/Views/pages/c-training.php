<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     C and Data Structure training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     C PROGRAMMING
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      The computing world has undergone a revolutionary change. Big computers turned into laptops, bigger programs broke out to small modules, audio chat to video chat and many more. During this time, many languages came to the market; some were great and some were just a mere wastage of time. Everyone has their own opinion towards every language. Among all the existing top languages, C is referred as more complicated and structured language. Moreover, there is plenty of competition among those language but the features and the perspective of C is still simple. The growing popularity of this language, involves many creation of compilers, operating systems, games and editors. Moreover, it is the general-purpose language with ability to draw the expressions, data flow, and data structures. It is true! The language is important to every branch of students whether it may be engineering, master's degree or any other course. Apart from the colleges, there are many training institutes, which offer this course. DUCAT is one among those. Over many years, DUCAT is providing the platform of every class of students to master the C language. Enrolling to this institute assures a student to be above the developer. Therefore, rush to our nearest centre, and be placed in top MNC's.
     </p>
     <div class="contentAcc">
      <h2>
       C Language Contents:
      </h2>
      <ul>
       <li>
        Introduction to C Language
       </li>
       <li>
        Role Of Compilers and Assemblers
       </li>
       <li>
        Procedural programming Approach
       </li>
       <li>
        Top to Bottom Approach
       </li>
      </ul>
      <h2>
       Introduction to C Basic
      </h2>
      <ul>
       <li>
        Keywords
       </li>
       <li>
        Data types
       </li>
       <li>
        Variables
       </li>
       <li>
        Constants
       </li>
       <li>
        Identifiers
       </li>
       <li>
        Tokens
       </li>
       <li>
        Operators
       </li>
       <li>
        memory management in prog
       </li>
      </ul>
      <h2>
       Flow Control Statements
      </h2>
      <ul>
       <li>
        Jump statements
       </li>
      </ul>
      <li>
       Goto
      </li>
      <li>
       Break
      </li>
      <li>
       Continue
      </li>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>