<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Certificate Request Form
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Certificate Request Form
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9 text-center">
    <h3>
     FILL ALL THE INFORMATION CORRECTLY
    </h3>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
    <h5>
     Register Now
    </h5>
    <div class="form-group form-inline">
     <form action="../logics_database/course_enquiry2.php" class="enquiryForm" method="post">
      <input name="city" placeholder="Ducat ID is Mandatory" required="" type="text"/>
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="techR">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max (6 Weeks)">
        3D Studio Max 6 week
       </option>
       <option value="436_cluster (1 Month)">
        436 Cluster (1 Month)
       </option>
       <option value="microcontroller (6 Weeks)">
        8051-Microcontroller(6 Weeks)
       </option>
       <option value=".NET Adv MVC (3 Months)">
        .NET Adv MVC(3 Months)
       </option>
       <option value=".NET (four Months)">
        .NET (Four Months)
       </option>
       <option value=".NET (six Months)">
        .(NET six Months)
       </option>
       <option value=".NET six Weeks">
        .NET six Weeks
       </option>
       <option value="Advance Digital marketing (4.5 Months)">
        Advance Digital marketing (4.5 Months)
       </option>
       <option value="Adv. Digital System Design (6 Weeks)">
        Adv. Digital System Design (6 Weeks)
       </option>
       <option value="Advance QTP (1.5 Months)">
        Advance QTP (1.5 Months)
       </option>
       <option value="Amazon6Weeks">
        Amazon (6 Weeks)eeks
       </option>
       <option value="Android (2 Months)">
        Android (2 Months)
       </option>
       <option value="Angular 4 (1.5 Months)">
        Angular 4 (1.5 Months)
       </option>
       <option value="Apache Hadoop (3 Months)">
        Apache Hadoop (3 Months)
       </option>
       <option value="arm (1.5 Months)">
        Arm (1.5 Months)
       </option>
       <option value="arduino (6 Weeks)">
        Arduino (6 Weeks)
       </option>
       <option value="Autocad 45 d">
        Autocad 45 d
       </option>
       <option value="avr-microcontroller (6 Weeks)">
        Avr-Microcontroller (6 Weeks)
       </option>
       <option value="Azure (3 Months)">
        Azure (3 Months)
       </option>
       <option value="BI Cognos 8.4 (1.5 Months) ">
        BI Cognos 8.4 (1.5 Months)
       </option>
       <option value="Big Commerce (1.5 Months)">
        Big Commerce (1.5 Months)
       </option>
       <option value="Big data (3 Months)">
        Big Data (3 Months)onths
       </option>
       <option value="Big data (6 Months)">
        Big Data (6 Months)
       </option>
       <option value="C Language (2 Months)">
        C Language (2 Months)
       </option>
       <option value="C++ Language (2 Months)">
        C++ Language (2 Months)
       </option>
       <option value="CAD_Civil_SM (6 Weeks)">
        CAD_Civil_SM (6 Weeks)
       </option>
       <option value="CADcustomization (1.5 Months)">
        Cad Customization (1.5 Months)
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia 45 Days">
        Catia 45 Days
       </option>
       <option value="CCNA (6Weeks) ">
        CCNA (6 Weeks)
       </option>
       <option value="Ccnasecurity (6Weeks)">
        CCNA Security (6 Weeks)
       </option>
       <option value="CCNP (3 Months)">
        CCNP (3 Months)
       </option>
       <option value="Checkpoint (6 Weeks)">
        Checkpoint (6 Weeks)
       </option>
       <option value="Cloud Computing Full Course (4.5 Months)">
        Cloud Computing Full Course (4.5 Months)
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing (six Weeks)
       </option>
       <option value="Coreldraw (1 Month)">
        CorelDraw (1 Month)
       </option>
       <option value="Dataware Housing (3 Months)">
        Dataware Housing (3 Months)
       </option>
       <option value="Data Structures (1 Month)">
        Data Structures (1 Month)
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING (4.5 Months)">
        Data Science &amp; Machine Learning Using R Programming (4.5 Months)
       </option>
       <option value="Datasciencepython (3.5 Months)">
        DataSciencePython (3.5 Months)
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING (3.5 Months)">
        Data Science Using R Programming (3.5 Months)
       </option>
       <option value="deeplearning (2.5 Months)">
        Deep Learning (2.5 Months)
       </option>
       <option value="deeplearninginpython (3.5 Months)">
        Deep Learning in python (3.5 Months)
       </option>
       <option value="device-drivers (1.5 Months)">
        Device-Drivers (1.5 Months)
       </option>
       <option value="Devops (2 Months)">
        Devops (2 Months)
       </option>
       <option value="Digital Marketing (3 Months)">
        Digital Marketing (3 Months)
       </option>
       <option value="digitalmarketing(sixWeeks)">
        Digital Marketing (6 Weeks)
       </option>
       <option value="diipp (100Days)">
        DIIPP (100Days)
       </option>
       <option value="Diploma In Hardware Networking (3 Months)">
        Diploma In Hardware Networking (3 Months)
       </option>
       <option value="Diploma In Hardware Networking (6 Months)">
        Diploma In Hardware Networking (6 Months)
       </option>
       <option value="Django (2 Months)">
        Django (2 Months)
       </option>
       <option value="DO_407_Ansible (1 month)">
        DO 407 Ansible (1 month)
       </option>
       <option value="Embedded 3.6 Months">
        Embedded (3.6 Months)
       </option>
       <option value="Embedded (Six Months)">
        Embedded (Six Months)
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="erpscm (3 Months)">
        ERP SCM (3 Months)
       </option>
       <option value="GD and T (1 month)">
        GD &amp; T (1 month)
       </option>
       <option value="hardware-and-electronics (4 Months)">
        Hardware-And-Electronics (4 Months)
       </option>
       <option value="HR GENERALIST (1.5 Months)">
        HR Generalist (1.5 Months)
       </option>
       <option value="IBM MAINFRAME (2.5 Months)">
        IBM Mainframe (2.5 Months)
       </option>
       <option value="IOT (6 Weeks)">
        IOT (6 Weeks)
       </option>
       <option value="IOT (3 Months)">
        IOT (3 Months)
       </option>
       <option value="IOT (6 Months)">
        IOT (6 Months)
       </option>
       <option value="I-Phone (2 Months)">
        I-Phone (2 Months)
       </option>
       <option value="Java(6 Weeks)eek">
        Java(6 Weeks)eek
       </option>
       <option value="Javaexperts (6 Months)">
        Java Experts (6 Months)
       </option>
       <option value="Javabeginners (3 Months)">
        Java Beginners (3 Months)
       </option>
       <option value="JAVA ANDROID KOTLIN (6 Months)">
        Java Android Kotlin (6 Months)
       </option>
       <option value="JAVA HADOOP (5 Months)">
        Java Hadoop (5 Months)
       </option>
       <option value="JAVA J2EE (4 Months)">
        Java J2ee (4 Months)
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate (2 Months)">
        Java Spring &amp; Amp; Hibernate (2 Months)
       </option>
       <option value="JAVA UI (6 Months)">
        Java UI (6 Months)
       </option>
       <option value="javawithangular4 (2 Months)">
        Java With Angular 4 2
       </option>
       <option value="javasql (45 Days)">
        Java With SQL (45 Days)
       </option>
       <option value="laravel (20 Days)">
        Laravel (20 Days)
       </option>
       <option value="MACHINELEARNING (2 Months)">
        Machine Learning (2 Months)
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING 3.(5 Months)">
        Machine Learning Using R Programming 3.(5 Months)
       </option>
       <option value="Magento PHP 20 Days">
        Magento PHP 20 Days
       </option>
       <option value="Manual-selenium (4 Months)">
        Manual + selenium (4 Months)
       </option>
       <option value="Mean (3 Months)">
        Mean (3 Months)
       </option>
       <option value="MCSA 2016 (3 Months)">
        MCSA 2016 (3 Months)
       </option>
       <option value="MCSE (4.5 Months)">
        MCSE (4.5 Months)
       </option>
       <option value="Mis (3 Months)">
        Mis (3 Months)
       </option>
       <option value="Msbi (3 Months)">
        MSBI (3 Months)
       </option>
       <option value="mssql (6 Months)">
        MS SQL (6 Months)
       </option>
       <option value="Multimedia Animation (3 Months)">
        Multimedia Animation (3 Months)
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="oracle 12C (3 Months)">
        Oracle 12C (3 Months)
       </option>
       <option value="oracle 11G DBA (3 Months)">
        Oracle 11G DBA (3 Months)
       </option>
       <option value="oracle 11G RAC (2.5 Months)">
        Oracle 11G RAC (2.5 Months)
       </option>
       <option value="oracle app's XII Financials (3 Months)">
        Oracle App's XII Financials (3 Months)
       </option>
       <option value="oracle app's XII Technical (3 Months)">
        Oracle App's XII Technical (3 Months)
       </option>
       <option value="Powerbi (2 Months)">
        Power BI (2 Months)
       </option>
       <option value="Performance_Tuning (1 Month)">
        Performance Tuning (1 Month)
       </option>
       <option value="Perl script (3 Months)">
        Perl Script (3 Months)
       </option>
       <option value="photoshop (1 Month)">
        Photoshop (1 Month)
       </option>
       <option value="PHP ++ 1.(5 Months)">
        PHP ++ 1.(5 Months)
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller (6 Months)">
        Pic-Microcontroller (6 Months)
       </option>
       <option value="PLC SCADA (3 Months)onths">
        PLC Scada (3 Months)onths
       </option>
       <option value="PLC SCADA six Weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE (6 Months)">
        PLC Scada Full Course (6 Months)
       </option>
       <option value="plsql (1 Month)">
        PLSQL (1 Month)
       </option>
       <option value="Primaverproject Planner (1 Month)">
        Primaver Project Planner (1 Month)
       </option>
       <option value="Creo 45 Days">
        Creo 45Days
       </option>
       <option value="Python 45 Days">
        Python 45 Days
       </option>
       <option value="PythonDjango 3.(5 Months)">
        Python &amp; Django 3.(5 Months)
       </option>
       <option value="pythonwithiot (3 Months)">
        Python with IOT (3 Months)
       </option>
       <option value="Pythonwithmachinelearning (4.5 Months)">
        Python With MachineLearning (4.5 Months)
       </option>
       <option value="pythonwithselenium (3 Months)">
        Python With Selenium (3 Months)
       </option>
       <option value="Randdataanalytics (3 Months)">
        R And Data Analytics (3 Months)
       </option>
       <option value="raspberry-pi (6 Weeks)">
        Raspberry-pi (6 Weeks)
       </option>
       <option value="Redhat Linux 7.0 (3 Months)">
        Redhat Linux 7.0 (3 Months)
       </option>
       <option value="Rprogramming (2.5 Months)">
        R Programming (2.5 Months)
       </option>
       <option value="Ravit Architecture (6 Weeks)">
        Ravit Architecture (6 Weeks)
       </option>
       <option value="RH_236 1.(5 Months)">
        RH 236 1.(5 Months)
       </option>
       <option value="RH_310_openstack (1 Month)">
        RH 310 Openstack (1 Month)
       </option>
       <option value="RH342">
        RH342 (1 Month)
       </option>
       <option value="RHCVA 1.(5 Months)">
        RHCVA 1.5,
       </option>
       <option value="RHCE (3 Months)">
        RHCE (3 Months)
       </option>
       <option value="ROBOTICS COURSE (2 Months)">
        Robotics Course (2 Months)
       </option>
       <option value="SAS (3 Months)">
        SAS (3 Months)
       </option>
       <option value="SEO 45Days">
        SEO 45 Days
       </option>
       <option value="Selenium (3 Months)">
        Selenium (3 Months)
       </option>
       <option value="Sharepoint Development (2 Months)">
        Sharepoint Development (2 Months)
       </option>
       <option value="Solidworks (6 Weeks)">
        Solidworks (6 Weeks)
       </option>
       <option value="Software Quality Testing (6 Weeks)">
        Software Quality Testing (6 Weeks)
       </option>
       <option value="SQL Server DBA (3 Months)">
        SQL Server DBA (3 Months)
       </option>
       <option value="SQT Six Weeks">
        SQT Six Weeks
       </option>
       <option value="Staad Pro (6 Weeks)">
        Staad Pro (6 Weeks)
       </option>
       <option value="Tableau (2.5 Months)">
        Tableau (2.5 Months)
       </option>
       <option value="tally erp (2.5 Months)">
        Tally Erp 9 (2.5 Months)
       </option>
       <option value="UI (3 Months)">
        UI (3 Months)
       </option>
       <option value="UIandangularjs (4.5 Months)">
        UI And Angular js (4.5 Months)
       </option>
       <option value="Unigraphics (6 Weeks)">
        Unigraphics (6 Weeks)
       </option>
       <option value="UNIX SHELL Scripting (2.5 Months)">
        Unix Shell Scripting (2.5 Months)
       </option>
       <option value="Upgrade mvc (1Month)">
        Upgrade MVC 1 Month
       </option>
       <option value="vlsiverilogtraining (6 Weeks)">
        VLSI VERILOG (6 Weeks)
       </option>
       <option value="vlsivhdltraining (3 Months)">
        VLSI/VHDL (3 Months)
       </option>
      </select>
      <div>
       <div style="width:50%;display:inline-block;">
        <label style="width:20%;display:inline-block;">
         From:
        </label>
        <input name="from" style="padding:8px;margin-bottom:10px;width:80%;display:inline-block;" type="date">
        </input>
       </div>
       <div style="width:50%;display:inline-block;">
        <label style="width:20%;display:inline-block;">
         To:
        </label>
        <input name="to" style="padding:8px;margin-bottom:10px;width:80%;display:inline-block;" type="date">
        </input>
       </div>
      </div>
      <input name="cp" placeholder="49 + 78 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="127">
       <input name="url" type="hidden" value="/certificationrequest/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
  </div>
 </div>
</section>
<section class="text-center" id="offices">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <h5>
     CORPORATE OFFICE NOIDA:
     <span>
      0120 - 4646464
     </span>
    </h5>
    <p>
     GR.NOIDA:
     <span>
      0120-4345190
     </span>
     GHAZIABAD:
     <span>
      0120-4835400
     </span>
     FARIDABAD:
     <span>
      0129-4150605
     </span>
     GURGAON:
     <span>
      0124-4219095
     </span>
    </p>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>