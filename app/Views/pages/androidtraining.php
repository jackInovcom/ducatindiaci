<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     Best Android Training In Noida
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Android Training
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      LEARN LATEST ANDROID APPLICATION FROM DUCAT
     </h4>
     <p>
      Another name of world's best platform for mobile applications is Android. It is a software package that has operating system, middleware and key applications. For Java based Android applications Software Development Kit (SDK) provides tools and APIs. Android platform runs multiple applications at one time. For e.g. Android user can receive notification, listen music and use GPS. Android applications have a vast market. Its applications are used by many prominent mobile phone manufacturing companies. It provides easier application maintenance and market exposure to developer. Android is a versatile application which is flexible with hardware and carrier. Besides this, Android APIs helps a developer in developing standard apps. One who has work experience or good hands on Java can easily learn Android development. Usually the candidates with computer science and related engineering background and MCA &amp; MSc can learn this course but anyone with good command over java can take
      <strong>
       Android training
      </strong>
      . DUCAT's Android course provides you a well designed training program according to IT job market demand. Enhanced and frequent exposure to labs and experienced faculties will give a sharp edge in
      <strong>
       Android Training
      </strong>
      development. Live project development, regular revision and tips for coding are a part of course.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction to Android
      </h2>
      <ul>
       <li>
        Introduction to Android
       </li>
       <li>
        Brief history of Android
       </li>
       <li>
        What is Android?
       </li>
       <li>
        Why is Android important?
       </li>
       <li>
        What benefits does Android have?
       </li>
       <li>
        What is OHA?
       </li>
       <li>
        Why to choose Android?
       </li>
       <li>
        Software architecture of Android
       </li>
       <li>
        Advantages of Android
       </li>
       <li>
        Android features Android market
       </li>
       <li>
        Comparing Android with other platform
       </li>
       <li>
        Terms and acronyms
       </li>
      </ul>
      <h2>
       2. Installation and Configuration of Android
      </h2>
      <ul>
       <li>
        Details about the software requirement
       </li>
       <li>
        Download and installation process of Android SDK
       </li>
       <li>
        How to select Android version?
       </li>
       <li>
        Step to create new project?
       </li>
       <li>
        Running your application
       </li>
       <li>
        Creation of new AVD
       </li>
       <li>
        Android studio
       </li>
      </ul>
      <h2>
       3. Getting Started
      </h2>
      <ul>
       <li>
        How to select Android version?
       </li>
       <li>
        Step to create new project
       </li>
       <li>
        Running Your Application
       </li>
       <li>
        Creation of New AVD
       </li>
       <li>
        Creating run configuration
       </li>
       <li>
        Creating your first Android activity
       </li>
       <li>
        List of basic sample programs
       </li>
      </ul>
      <h2>
       4. Introductions to Application Components Activities
      </h2>
      <ul>
       <li>
        Services
       </li>
       <li>
        Broadcast receivers
       </li>
       <li>
        Content providers Intents
       </li>
       <li>
        Notifications
       </li>
       <li>
        Activating and shutting down components
       </li>
       <li>
        Brief idea about manifest file
       </li>
       <li>
        Brief idea about activities and Task
       </li>
      </ul>
      <h2>
       5. Android Component Life Cycle
      </h2>
      <ul>
       <li>
        Activity Life Cycle with sample program
       </li>
       <li>
        Service Life Cycle with sample program
       </li>
      </ul>
      <h2>
       6. Android Layouts
      </h2>
      <ul>
       <li>
        What are views, Layouts and there classification?
       </li>
       <li>
        How Android Draws views and Layout Classification?
       </li>
       <li>
        Table Layout ln detail with Example
       </li>
       <li>
        Tab Layout ln detail with Example
       </li>
       <li>
        Frame Layout ln detail with Example
       </li>
       <li>
        Linear Layout in detail with Example
       </li>
      </ul>
      <h2>
       7. Android Views
      </h2>
      <ul>
       <li>
        Grid View In detail With Example
       </li>
       <li>
        Map View In detail with Example
       </li>
       <li>
        Cont. Sub Topical Views
       </li>
       <li>
        Web View In detail with Example
       </li>
       <li>
        Spinner In detail with Example
       </li>
       <li>
        Gallery In detail with Example
       </li>
       <li>
        Google Map View In detail with Example
       </li>
       <li>
        Introduction to creating activity user with views
       </li>
       <li>
        Different ways of creating views Using xml
       </li>
      </ul>
      <h2>
       8. Styles and Themes
      </h2>
      <ul>
       <li>
        Providing resources
       </li>
       <li>
        Different resource file location
       </li>
       <li>
        Providing alternative resources
       </li>
       <li>
        Android finds the best matching resource
       </li>
       <li>
        Accessing resources
       </li>
       <li>
        Accessing platform resources
       </li>
       <li>
        Handling runtime changes
       </li>
       <li>
        Introduction to Jquery
       </li>
       <li>
        JQuery work in Android
       </li>
      </ul>
      <h2>
       9. What are Fragments?
      </h2>
      <ul>
       <li>
        Multipane &amp; Singlepane
       </li>
       <li>
        Fragment Life Cycle
       </li>
       <li>
        Addition of Fragments
       </li>
       <li>
        Fragments Working without U
       </li>
      </ul>
      <h2>
       10. Introduction to Menus
      </h2>
      <ul>
       <li>
        How to create menus?
       </li>
       <li>
        Types of Android Application Menus
       </li>
       <li>
        Option Menu
       </li>
       <li>
        Expanded - In detail with Example
       </li>
       <li>
        Context Menu ln detail with example
       </li>
       <li>
        Sub Menu-In detail with example
       </li>
      </ul>
      <h2>
       11. Introduction to Drawers
      </h2>
      <ul>
       <li>
        Navigation Drawer
       </li>
       <li>
        Simple Side Drawer
       </li>
       <li>
        Tab Drawer
       </li>
       <li>
        Extra Drawer
       </li>
      </ul>
      <h2>
       12. Handling User Interaction Events
      </h2>
      <ul>
       <li>
        Handling user events
       </li>
       <li>
        Different types of event listener
       </li>
       <li>
        OnClickO
       </li>
       <li>
        OnLongClickO
       </li>
       <li>
        OnFocusChangeO
       </li>
       <li>
        OnKeyO OnTouchO
       </li>
       <li>
        OnCreateContextMenuO
       </li>
       <li>
        Different types of event handler
       </li>
       <li>
        OnKeyDown (int, KeyEvent)
       </li>
       <li>
        OnKeyUp (int, KeyEvent)
       </li>
       <li>
        OnTrackballEvent (MotionEvent)
       </li>
       <li>
        OnTouchEvent (MotionEvent)
       </li>
       <li>
        OnFocusChanged (boolean,int,Rect)
       </li>
      </ul>
      <h2>
       13. Creating Dialogs
      </h2>
      <ul>
       <li>
        Introduction to dialogs
       </li>
       <li>
        Showing and dismissing of dialog boxes
       </li>
       <li>
        Alert dialog In detail with example
       </li>
       <li>
        Progress dialog In detail with example
       </li>
       <li>
        Threading and handler
       </li>
       <li>
        Creating running applications-Events
       </li>
       <li>
        Creating running applications-Dialogs
       </li>
      </ul>
      <h2>
       14. Notifications
      </h2>
      <ul>
       <li>
        Notifying Users
       </li>
       <li>
        Status bar Notification
       </li>
       <li>
        Toast Notification
       </li>
       <li>
        Dialog Notification
       </li>
      </ul>
      <h2>
       15. Intents, Broadcast Receivers, Adapters and Internet
      </h2>
      <ul>
       <li>
        Different types of intent?
       </li>
       <li>
        Launching sub-activities
       </li>
       <li>
        What is intent filter
       </li>
       <li>
        Intent objects---In detail with example
       </li>
       <li>
        Using intents to take pictures
       </li>
       <li>
        Handling sub activity results
       </li>
      </ul>
      <h2>
       16. Data Storage
      </h2>
      <ul>
       <li>
        Android techniques for data storage
       </li>
       <li>
        Creating and saving shared preferences
       </li>
       <li>
        Retrieving shared preferences
       </li>
       <li>
        Storing in files
       </li>
       <li>
        Loading from files.
       </li>
       <li>
        Storing in databases.
       </li>
      </ul>
      <h2>
       17.Working with SQL Lite
      </h2>
      <ul>
       <li>
        Introducing SQLite database.
       </li>
       <li>
        Working with Android databases.
       </li>
       <li>
        Using SQLite OpenHelper.
       </li>
       <li>
        Cursors and content values.
       </li>
       <li>
        Opening and closing Database
       </li>
      </ul>
      <h2>
       18. Working in Background
      </h2>
      <ul>
       <li>
        Introducing services
       </li>
       <li>
        Creating and controlling services
       </li>
       <li>
        Registering a service in the manifest
       </li>
       <li>
        Starting, controlling, and interacting with a service
       </li>
      </ul>
      <h2>
       19. Using the Camera, Taking Pictures and the Media API
      </h2>
      <ul>
       <li>
        Controlling the camera and taking pictures
       </li>
       <li>
        Playing audio and video
       </li>
       <li>
        Introducing the media player
       </li>
       <li>
        Preparing audio for playback
       </li>
       <li>
        Packaging audio as an application resource
       </li>
       <li>
        Initializing audio content for playback
       </li>
       <li>
        Preparing for video playback
       </li>
       <li>
        Playing video using the video view
       </li>
       <li>
        Setting up a surface for video playback
       </li>
       <li>
        Initializing video content for playback
       </li>
       <li>
        Supported video formats
       </li>
       <li>
        Controlling playback
       </li>
       <li>
        Managing media playback output
       </li>
       <li>
        Multimedia supported audio formats
       </li>
       <li>
        Recording audio and video
       </li>
       <li>
        Using Intents to Record Video
       </li>
       <li>
        Configuring and Controlling Video Recording
       </li>
       <li>
        Previewing Video Recording
       </li>
       <li>
        Reading and Writing JPEG EXIF Image Details
       </li>
       <li>
        Adding new media to media storage Using the Media Scanner
       </li>
       <li>
        Inserting Media into the Media Store Raw video manipulation
       </li>
       <li>
        Recording Sound with Audio Record
       </li>
       <li>
        Playing Sound with Audio Track Speech recognition
       </li>
       <li>
        Creating and Running and Testing
       </li>
      </ul>
      <h2>
       20. Maps, GEO coding and Location Based Services
      </h2>
      <ul>
       <li>
        Using Location Based Services
       </li>
       <li>
        Working with the location manager
       </li>
       <li>
        Configuring the Emulator to Test Location Based Services
       </li>
      </ul>
      <h2>
       21. About ADB (Android Debug Bridge)
      </h2>
      <ul>
       <li>
        DDMS: Dalvik debug monitor Service
       </li>
       <li>
        Trace View
       </li>
      </ul>
      <h2>
       22.Data Security and Permission
      </h2>
      <ul>
       <li>
        Security Architecture
       </li>
       <li>
        User Ids and File Access
       </li>
       <li>
        Using Permissions
       </li>
       <li>
        Declaring and Enforcing Permissions
       </li>
      </ul>
      <h2>
       23. Drawing 2D and 3D Graphics
      </h2>
      <ul>
       <li>
        Rolling your own Widgets
       </li>
       <li>
        Drawables
       </li>
       <li>
        Bitmaps
       </li>
       <li>
        Paint
       </li>
      </ul>
      <h2>
       24. Using Bluetooth and Managing and Monitoring Wi-Fi
      </h2>
      <ul>
       <li>
        Accessing the Local Bluetooth Device Adapter
       </li>
       <li>
        Managing Bluetooth Properties and State
       </li>
       <li>
        Managing Device Discoverability
       </li>
       <li>
        Discovering Remote Devices
       </li>
       <li>
        Monitoring Active Connection Details
       </li>
       <li>
        Scanning for Hotspots
       </li>
       <li>
        Managing Wi-Fi Configurations
       </li>
       <li>
        Creating Wi-Fi Network Configurations
       </li>
       <li>
        Device Vibration
       </li>
       <li>
        Controlling device vibration
       </li>
      </ul>
      <h2>
       25. Introduction SMS and MMS
      </h2>
      <ul>
       <li>
        Using SMS and MMS in Your Application
       </li>
       <li>
        Sending SMS and MMS from your Application
       </li>
       <li>
        Using Intents and the Native Client Sending SMS Messages Manually Tracking and Conforming
SMS Message Delivery
       </li>
       <li>
        Conforming to the Maximum SMS
       </li>
       <li>
        Message Size Sending DAT Messages
       </li>
      </ul>
      <h2>
       26. Content Providers
      </h2>
      <ul>
       <li>
        What is content provider
       </li>
       <li>
        How to access build in Content provider
       </li>
       <li>
        Retrieving build - in Content provider data
       </li>
      </ul>
      <h2>
       27. Android Telephony
      </h2>
      <ul>
       <li>
        Launching the Dialer to Initiate Phone Calls Replacing the Native Dialer
       </li>
       <li>
        Accessing phone and Network Properties &amp; Status
       </li>
       <li>
        Reading Phone Device Details
       </li>
       <li>
        Reading Data Connection and Transfer State Reading Network Details
       </li>
      </ul>
      <h2>
       28. Sensor Device
      </h2>
      <ul>
       <li>
        Using sensors and the sensor manager
       </li>
       <li>
        Introducing Sensors
       </li>
       <li>
        Supported Android Sensers
       </li>
       <li>
        Finding Sensors
       </li>
       <li>
        Using Sensors
       </li>
       <li>
        Interpreting the sensor values
       </li>
       <li>
        Using the compass , accelerometer and orientation sensors
       </li>
       <li>
        Introducing Accelerometers Detecting Acceleration Changes Creating a G-Forceometer
       </li>
      </ul>
      <h2>
       29. Further Advanced Topics
      </h2>
      <ul>
       <li>
        Binding Activities to services
       </li>
       <li>
        Prioritizing Background services
       </li>
       <li>
        Binding data with service
       </li>
      </ul>
      <h2>
       30.Web Services with Architecture
      </h2>
      <ul>
       <li>
        What are webservices
       </li>
       <li>
        Web service Architecture
       </li>
       <li>
        Asyn Task
       </li>
       <li>
        REST &amp; SOAP
       </li>
       <li>
        Parsing Techniques JSON, XML Consuming WebServices CRUD Operations over Server
       </li>
      </ul>
      <h2>
       31.JSON
      </h2>
      <ul>
       <li>
        Introduction to JSON
       </li>
       <li>
        Advantages of JSON over XML
       </li>
       <li>
        Syntax &amp; Structure of JSON
       </li>
       <li>
        Why is JSON is preferred for mobile applications Different types JSON Parsers ,simple json ,
Jackson , GSON to parse the JSON
       </li>
      </ul>
      <h2>
       32. Volley Library
      </h2>
      <ul>
       <li>
        Introduction Volley Library
       </li>
       <li>
        Volley Library Advantages
       </li>
       <li>
        Volley Library Components
       </li>
       <li>
        How to setup the Environment Creating Volley singleton class Different Types of Requests Adding
request headers Handling Volley Cache
       </li>
      </ul>
      <h2>
       33. FIREBASE
      </h2>
      <ul>
       <li>
        Realtime Database
       </li>
       <li>
        Cloud Storage
       </li>
       <li>
        Authentication
       </li>
       <li>
        Cloud Messaging
       </li>
       <li>
        AdMob
       </li>
       <li>
        Crash Reporting
       </li>
      </ul>
      <h2>
       34. Google Cloud Messaging
      </h2>
      <ul>
       <li>
        What is GCM ?
       </li>
       <li>
        GCM Architecture
       </li>
       <li>
        GCM Services
       </li>
       <li>
        Instant Messaging
       </li>
       <li>
        TTL (Time to Live)
       </li>
       <li>
        Group Messaging
       </li>
       <li>
        Server Communication
       </li>
      </ul>
      <h2>
       35. Integration
      </h2>
      <ul>
       <li>
        Facebook Integration
       </li>
       <li>
        Twitter Integration
       </li>
       <li>
        Google/Gmail Integration
       </li>
       <li>
        Payment Gateway Integration
       </li>
      </ul>
      <h2>
       36. Material Design
      </h2>
      <ul>
       <li>
        List and Card
       </li>
       <li>
        View Shadows/ Custom Shadows
       </li>
       <li>
        App Bar /Action Bar
       </li>
       <li>
        Matterial Theme
       </li>
       <li>
        Adapter view and Recycler View
       </li>
      </ul>
      <h2>
       37. Animation
      </h2>
      <ul>
       <li>
        Clockwise
       </li>
       <li>
        Zoom
       </li>
       <li>
        Fade
       </li>
       <li>
        Blink
       </li>
       <li>
        Move
       </li>
       <li>
        Slide
       </li>
      </ul>
      <h2>
       38. Debugging and testing Android Apps
      </h2>
      <ul>
       <li>
        20g Cat
       </li>
       <li>
        Debugger
       </li>
       <li>
        Trace View
       </li>
       <li>
        Monkey Runner
       </li>
       <li>
        UI Automator
       </li>
      </ul>
      <h2>
       Project
      </h2>
      <ul>
       <li>
        Covering all the concepts
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="androidtraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="29 + 17 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="46">
       <input name="url" type="hidden" value="/androidtraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>