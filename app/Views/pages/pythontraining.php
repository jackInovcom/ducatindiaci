<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     PYTHON TRAINING
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Python
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      Python TRAINING
     </h4>
     <p>
      Python is mainly stated as high-level, general-purpose programming language, which emphasizes code readability. The syntax helps the programmers to express their concepts in few general "lines of code" when compared with other promising languages, like Java or C++. Through our courses, you can easily construct clear programs, on both large and small scales.As the importance of Python programming language is gaining huge popularity, therefore; the need to understand and know the language is increasing among people. When you think about Python training, you must look for an Ducat expert help.
     </p>
     <div class="contentAcc">
      <h2>
       An Introduction to Python
      </h2>
      <ul>
       <li>
        Brief History
       </li>
       <li>
        Why Python
       </li>
       <li>
        Where to use
       </li>
      </ul>
      <h2>
       Beginning Python Basics
      </h2>
      <ul>
       <li>
        The print statement
       </li>
       <li>
        Comments
       </li>
       <li>
        Python Data Structures &amp; Data Types
       </li>
       <li>
        String Operations in Python
       </li>
       <li>
        Simple Input &amp; Output
       </li>
       <li>
        Simple Output Formatting
       </li>
      </ul>
      <h2>
       Python Program Flow
      </h2>
      <ul>
       <li>
        Indentation
       </li>
       <li>
        The If statement and its' related statement
       </li>
       <li>
        An example with if and it's related statement
       </li>
       <li>
        The while loop
       </li>
       <li>
        The for loop
       </li>
       <li>
        The range statement
       </li>
       <li>
        Break &amp; Continue
       </li>
       <li>
        Assert
       </li>
       <li>
        Examples for looping
       </li>
      </ul>
      <h2>
       Functions &amp; Modules
      </h2>
      <ul>
       <li>
        Create your own functions
       </li>
       <li>
        Functions Parameters
       </li>
       <li>
        Variable Arguments
       </li>
       <li>
        Scope of a Function
       </li>
       <li>
        Function Documentation/Docstrings
       </li>
       <li>
        Lambda Functions &amp; map
       </li>
       <li>
        An Exercise with functions
       </li>
       <li>
        Create a Module
       </li>
       <li>
        Standard Modules
       </li>
      </ul>
      <h2>
       Exceptions
      </h2>
      <ul>
       <li>
        Errors
       </li>
       <li>
        Exception Handling with try
       </li>
       <li>
        Handling Multiple Exceptions
       </li>
       <li>
        Writing your own Exceptions
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>