<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Oracle 11g Development Training
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Oracle 11g Development Training
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      DUCAT imparts training in a way that is more practical and efficient. Anyone who wants to learn Oracle 11g development needs to look no further than DUCAT. It gives training so that after the training ORACLE 11G DEVELOPMENT becomes a part of you. Oracle 11g development starts where Oracle Parallel Server ends. It allows someone to approach the same database simultaneously. It ensures full tolerance, load balancing and of course performance. The heart of this technology is a concept of shared disk subsystem. In case of system failure it allows recovery of that node from other nodes. DUCAT imparts training not only by lectures or lessons but by giving real access to Oracle 11g development. One can understand the advantage and disadvantage of this technology only when they feel it which happens during the training at DUCAT. Students learn how to install the process and to run it efficiently. The training teaches all the features of Oracle 11g development in depth. DUCAT faculties are the best in this field of technology. They themselves are experts and they make their students also experts. They teach so that their students shine in their field and make themselves proud.
     </p>
     <div class="contentAcc">
      <h2>
       INTRODUCTION TO ORACLE DATABASE
      </h2>
      <ul>
       <li>
        List the features of Oracle Database 12c
       </li>
       <li>
        Discuss the basic design, theoretical, and physical aspects of a relational database
       </li>
       <li>
        Categorize the different types of SQL statements
       </li>
       <li>
        Describe the data set used by the course
       </li>
       <li>
        Log on to the database using SQL Developer environment
       </li>
       <li>
        Save queries to files and use script files in SQL
       </li>
       <li>
        Developer
       </li>
      </ul>
      <h2>
       RETRIEVE DATA USING THE SQL SELECT STATEMENT
      </h2>
      <ul>
       <li>
        List the capabilities of SQL SELECT statements
       </li>
       <li>
        Generate a report of data from the output of a basic SELECT statement
       </li>
       <li>
        Select All Columns
       </li>
       <li>
        Select Specific Columns
       </li>
       <li>
        Use Column Heading Defaults
       </li>
       <li>
        Use Arithmetic Operators
       </li>
       <li>
        Understand Operator Precedence
       </li>
       <li>
        Learn the DESCRIBE command to display the table structure
       </li>
      </ul>
      <h2>
       LEARN TO RESTRICT AND SORT DATA
      </h2>
      <ul>
       <li>
        Write queries that contain a WHERE clause to limit the output retrieved
       </li>
       <li>
        List the comparison operators and logical operators that are used in a WHERE clause
       </li>
       <li>
        Describe the rules of precedence for comparison and logical operators
       </li>
       <li>
        Use character string literals in the WHERE clause
       </li>
       <li>
        Write queries that contain an ORDER BY clause to sort the output of a SELECT statement
       </li>
       <li>
        Sort output in descending and ascending order
       </li>
      </ul>
      <h2>
       USAGE OF SINGLE-ROW FUNCTIONS TO CUSTOMIZE OUTPUT
      </h2>
      <ul>
       <li>
        Describe the differences between single row and multiple row functions
       </li>
       <li>
        Manipulate strings with character function in the SELECT and WHERE clauses
       </li>
       <li>
        Manipulate numbers with the ROUND, TRUNC, and MOD functions
       </li>
       <li>
        Perform arithmetic with date data
       </li>
       <li>
        Manipulate dates with the DATE functions
       </li>
      </ul>
      <h2>
       INVOKE CONVERSION FUNCTIONS AND CONDITIONAL EXPRESSIONS
      </h2>
      <ul>
       <li>
        Describe implicit and explicit data type conversion
       </li>
       <li>
        Use the TO_CHAR, TO_NUMBER, and TO_DATE conversion functions
       </li>
       <li>
        Nest multiple functions
       </li>
       <li>
        Apply the NVL, NULLIF, and COALESCE functions to data
       </li>
       <li>
        Use conditional IF THEN ELSE logic in a SELECT statement
       </li>
      </ul>
      <h2>
       AGGREGATE DATA USING THE GROUP FUNCTIONS
      </h2>
      <ul>
       <li>
        Use the aggregation functions to produce meaningful reports
       </li>
       <li>
        Divide the retrieved data in groups by using the GROUP BY clause
       </li>
       <li>
        Exclude groups of data by using the HAVING clause
       </li>
      </ul>
      <h2>
       DISPLAY DATA FROM MULTIPLE TABLES USING JOINS
      </h2>
      <ul>
       <li>
        Write SELECT statements to access data from more than one table
       </li>
       <li>
        View data that generally does not meet a join condition by using outer joins
       </li>
       <li>
        Join a table to itself by using a self-join
       </li>
      </ul>
      <h2>
       USE SUB-QUERIES TO SOLVE QUERIES
      </h2>
      <ul>
       <li>
        Describe the types of problem that sub-queries can solve
       </li>
       <li>
        Define sub-queries
       </li>
       <li>
        List the types of sub-queries
       </li>
       <li>
        Write single-row and multiple-row sub-queries
       </li>
      </ul>
      <h2>
       THE SET OPERATORS
      </h2>
      <ul>
       <li>
        Describe the SET operators
       </li>
       <li>
        Use a SET operator to combine multiple queries into a single query
       </li>
       <li>
        Control the order of rows returned
       </li>
      </ul>
      <h2>
       DATA MANIPULATION STATEMENTS
      </h2>
      <ul>
       <li>
        Describe each DML statement
       </li>
       <li>
        Insert rows into a table
       </li>
       <li>
        Change rows in a table by the UPDATE statement
       </li>
       <li>
        Delete rows from a table with the DELETE statement
       </li>
       <li>
        Save and discard changes with the COMMIT and ROLLBACK statements
       </li>
       <li>
        Explain read consistency
       </li>
      </ul>
      <h2>
       USE OF DDL STATEMENTS TO CREATE AND MANAGE TABLES
      </h2>
      <ul>
       <li>
        Categorize the main database objects
       </li>
       <li>
        Review the table structure
       </li>
       <li>
        List the data types available for columns
       </li>
       <li>
        Create a simple table
       </li>
       <li>
        Decipher how constraints can be created at table creation
       </li>
       <li>
        Describe how schema objects work
       </li>
      </ul>
      <h2>
       OTHER SCHEMA OBJECTS
      </h2>
      <ul>
       <li>
        Create a simple and complex view
       </li>
       <li>
        Retrieve data from views
       </li>
       <li>
        Create, maintain, and use sequences
       </li>
       <li>
        Create and maintain indexes
       </li>
       <li>
        Create private and public synonyms
       </li>
      </ul>
      <h2>
       CONTROL USER ACCESS
      </h2>
      <ul>
       <li>
        Differentiate system privileges from object privileges
       </li>
       <li>
        Create Users
       </li>
       <li>
        Grant System Privileges
       </li>
       <li>
        Create and Grant Privileges to a Role
       </li>
       <li>
        Change Your Password
       </li>
       <li>
        Grant Object Privileges
       </li>
       <li>
        How to pass on privileges?
       </li>
       <li>
        Revoke Object Privileges
       </li>
      </ul>
      <h2>
       MANAGEMENT OF SCHEMA OBJECTS
      </h2>
      <ul>
       <li>
        Add, Modify, and Drop a Column
       </li>
       <li>
        Add, Drop, and Defer a Constraint
       </li>
       <li>
        How to enable and Disable a Constraint?
       </li>
       <li>
        Create and Remove Indexes
       </li>
       <li>
        Create a Function-Based Index
       </li>
       <li>
        Perform Flashback Operations
       </li>
       <li>
        Create an External Table by Using ORACLE_LOADER and by Using ORACLE_DATAPUMP
       </li>
       <li>
        Query External Tables
       </li>
       <li>
       </li>
      </ul>
      <h2>
       MANAGE OBJECTS WITH DATA DICTIONARY VIEWS
      </h2>
      <ul>
       <li>
        Explain the data dictionary
       </li>
       <li>
        Use the Dictionary Views
       </li>
       <li>
        USER_OBJECTS and ALL_OBJECTS Views
       </li>
       <li>
        Table and Column Information
       </li>
       <li>
        Query the dictionary views for constraint information
       </li>
       <li>
        Query the dictionary views for view, sequence, index and synonym information
       </li>
       <li>
        Add a comment to a table
       </li>
       <li>
        Query the dictionary views for comment information
       </li>
       <li>
       </li>
      </ul>
      <h2>
       MANIPULATE LARGE DATA SETS
      </h2>
      <ul>
       <li>
        Use Subqueries to Manipulate Data
       </li>
       <li>
        Retrieve Data Using a Subquery as Source
       </li>
       <li>
        Insert Using a Subquery as a Target
       </li>
       <li>
        Usage of the WITH CHECK OPTION Keyword on DML Statements
       </li>
       <li>
        List the types of Multitable INSERT Statements
       </li>
       <li>
        Use Multitable INSERT Statements
       </li>
       <li>
        Merge rows in a table
       </li>
       <li>
        Track Changes in Data over a period of time
       </li>
      </ul>
      <h2>
       DATA MANAGEMENT IN DIFFERENT TIME ZONES
      </h2>
      <ul>
       <li>
        Time Zones
       </li>
       <li>
        CURRENT_DATE, CURRENT_TIMESTAMP, and LOCALTIMESTAMP
       </li>
       <li>
        Compare Date and Time in a Session's Time Zone
       </li>
       <li>
        DBTIMEZONE and SESSIONTIMEZONE
       </li>
       <li>
        Difference between DATE and TIMESTAMP
       </li>
       <li>
        INTERVAL Data Types
       </li>
       <li>
        Use EXTRACT, TZ_OFFSET and FROM_TZ
       </li>
       <li>
        Invoke TO_TIMESTAMP,TO_YMINTERVAL and TO_DSINTERVAL
       </li>
      </ul>
      <h2>
       RETRIEVE DATA USING SUB-QUERIES
      </h2>
      <ul>
       <li>
        Multiple-Column Subqueries
       </li>
       <li>
        Pairwise and Non-pairwise Comparison
       </li>
       <li>
        Scalar Subquery Expressions
       </li>
       <li>
        Solve problems with Correlated Subqueries
       </li>
       <li>
        Update and Delete Rows Using Correlated Subqueries
       </li>
       <li>
        The EXISTS and NOT EXISTS operators
       </li>
       <li>
        Invoke the WITH clause
       </li>
       <li>
        The Recursive WITH clause
       </li>
      </ul>
      <h2>
       REGULAR EXPRESSION SUPPORT
      </h2>
      <ul>
       <li>
        Use the Regular Expressions Functions and Conditions in SQL
       </li>
       <li>
        Use Meta Characters with Regular Expressions
       </li>
       <li>
        Perform a Basic Search using the REGEXP_LIKE function
       </li>
       <li>
        Find patterns using the REGEXP_INSTR function
       </li>
       <li>
        Extract Substrings using the REGEXP_SUBSTR function
       </li>
       <li>
        Replace Patterns Using the REGEXP_REPLACE function
       </li>
       <li>
        Usage of Sub-Expressions with Regular Expression Support
       </li>
       <li>
        Implement the REGEXP_COUNT function
       </li>
      </ul>
      <h2>
       ORACLE DATABASE PROGRAM WITH PL/SQL
      </h2>
      <ul>
       <li>
        Overview of PL/SQL
       </li>
       <li>
        Identify the benefits of PL/SQL Subprograms
       </li>
       <li>
        Overview of the types of PL/SQL blocks
       </li>
       <li>
        Create a Simple Anonymous Block
       </li>
       <li>
        How to generate output from a PL/SQL Block?
       </li>
      </ul>
      <h2>
       DECLARE PL/SQL IDENTIFIERS
      </h2>
      <ul>
       <li>
        List the different Types of Identifiers in a PL/SQL subprogram
       </li>
       <li>
        Usage of the Declarative Section to Define Identifiers
       </li>
       <li>
        Use variables to store data
       </li>
       <li>
        Identify Scalar Data Types
       </li>
       <li>
        The %TYPE Attribute
       </li>
       <li>
        What are Bind Variables?
       </li>
       <li>
        Sequences in PL/SQL Expressions
       </li>
      </ul>
      <h2>
       WRITE EXECUTABLE STATEMENTS
      </h2>
      <ul>
       <li>
        Describe Basic PL/SQL Block Syntax Guidelines
       </li>
       <li>
        Learn to Comment the Code
       </li>
       <li>
        Deployment of SQL Functions in PL/SQL
       </li>
       <li>
        How to convert Data Types?
       </li>
       <li>
        Describe Nested Blocks
       </li>
       <li>
        Identify the Operators in PL/SQL
       </li>
      </ul>
      <h2>
       INTERACTION WITH THE ORACLE SERVER
      </h2>
      <ul>
       <li>
        Invoke SELECT Statements in PL/SQL
       </li>
       <li>
        Retrieve Data in PL/SQL
       </li>
       <li>
        SQL Cursor concept
       </li>
       <li>
        Avoid Errors by using Naming Conventions when using
       </li>
       <li>
        Retrieval and DML Statements
       </li>
       <li>
        Data Manipulation in the Server using PL/SQL
       </li>
       <li>
        Understand the SQL Cursor concept
       </li>
       <li>
        Use SQL Cursor Attributes to Obtain Feedback on DML
       </li>
       <li>
        Save and Discard Transactions
       </li>
      </ul>
      <h2>
       CONTROL STRUCTURES
      </h2>
      <ul>
       <li>
        Conditional processing using IF Statements
       </li>
       <li>
        Conditional processing using CASE Statements
       </li>
       <li>
        Describe simple Loop Statement
       </li>
       <li>
        Describe While Loop Statement
       </li>
       <li>
        Describe For Loop Statement
       </li>
       <li>
        Use the Continue Statement
       </li>
       <li>
       </li>
      </ul>
      <h2>
       COMPOSITE DATA TYPES
      </h2>
      <ul>
       <li>
        Use PL/SQL Records
       </li>
       <li>
        The %ROWTYPE Attribute
       </li>
       <li>
        Insert and Update with PL/SQL Records
       </li>
       <li>
        INDEX BY Tables
       </li>
       <li>
        Examine INDEX BY Table Methods
       </li>
       <li>
        Use INDEX BY Table of Records
       </li>
      </ul>
      <h2>
       EXPLICIT CURSORS
      </h2>
      <ul>
       <li>
        What are Explicit Cursors?
       </li>
       <li>
        Declare the Cursor
       </li>
       <li>
        Open the Cursor
       </li>
       <li>
        Fetch data from the Cursor
       </li>
       <li>
        Close the Cursor
       </li>
       <li>
        Cursor FOR loop
       </li>
       <li>
        The %NOTFOUND and %ROWCOUNT Attributes
       </li>
       <li>
        Describe the FOR UPDATE Clause and WHERE CURRENT Clause
       </li>
      </ul>
      <h2>
       EXCEPTION HANDLING
      </h2>
      <ul>
       <li>
        Understand Exceptions
       </li>
       <li>
        Handle Exceptions with PL/SQL
       </li>
       <li>
        Trap Predefined Oracle Server Errors
       </li>
       <li>
        Trap Non-Predefined Oracle Server Errors
       </li>
       <li>
        Trap User-Defined Exceptions
       </li>
       <li>
        Propagate Exceptions
       </li>
       <li>
        RAISE_APPLICATION_ERROR Procedure
       </li>
      </ul>
      <h2>
       STORED PROCEDURES
      </h2>
      <ul>
       <li>
        Create a Modularized and Layered Subprogram Design
       </li>
       <li>
        Modularize Development With PL/SQL Blocks
       </li>
       <li>
        Understand the PL/SQL Execution Environment
       </li>
       <li>
        List the benefits of using PL/SQL Subprograms
       </li>
       <li>
        List the differences between Anonymous Blocks and Subprograms
       </li>
       <li>
        Create, Call, and Remove Stored Procedures
       </li>
       <li>
        Implement Procedures Parameters and Parameters Modes
       </li>
       <li>
        View Procedure Information
       </li>
      </ul>
      <h2>
       STORED FUNCTIONS AND DEBUGGING SUBPROGRAMS
      </h2>
      <ul>
       <li>
        Create, Call, and Remove a Stored Function
       </li>
       <li>
        Identify the advantages of using Stored Functions
       </li>
       <li>
        Identify the steps to create a stored function
       </li>
       <li>
        Invoke User-Defined Functions in SQL Statements
       </li>
       <li>
        Restrictions when calling Functions
       </li>
       <li>
        Control side effects when calling Functions
       </li>
       <li>
        View Functions Information
       </li>
       <li>
        How to debug Functions and Procedures?
       </li>
      </ul>
      <h2>
       PACKAGES
      </h2>
      <ul>
       <li>
        Listing the advantages of Packages
       </li>
       <li>
        Describe Packages
       </li>
       <li>
        What are the components of a Package?
       </li>
       <li>
        Develop a Package
       </li>
       <li>
        How to enable visibility of a Package's Components?
       </li>
       <li>
        Create the Package Specification and Body using the SQL CREATE Statement and SQL Developer
       </li>
       <li>
        Invoke the Package Constructs
       </li>
       <li>
        View the PL/SQL Source Code using the Data Dictionary
       </li>
      </ul>
      <h2>
       DEPLOYING PACKAGES
      </h2>
      <ul>
       <li>
        Overloading Subprograms in PL/SQL
       </li>
       <li>
        Use the STANDARD Package
       </li>
       <li>
        Use Forward Declarations to solve Illegal Procedure Reference
       </li>
       <li>
        Implement Package Functions in SQL and Restrictions
       </li>
       <li>
        Persistent State of Packages
       </li>
       <li>
        Persistent State of a Package Cursor
       </li>
       <li>
        Control side effects of PL/SQL Subprograms
       </li>
       <li>
        Invoke PL/SQL Tables of Records in Packages
       </li>
      </ul>
      <h2>
       IMPLEMENT ORACLE-SUPPLIED PACKAGES IN APPLICATION DEVELOPMENT
      </h2>
      <ul>
       <li>
        What are Oracle-Supplied Packages?
       </li>
       <li>
        Examples of some of the Oracle-Supplied Packages
       </li>
       <li>
        How does the DBMS_OUTPUT Package work?
       </li>
       <li>
        Use the UTL_FILE Package to Interact with Operating System Files
       </li>
       <li>
        Invoke the UTL_MAIL Package
       </li>
       <li>
        Write UTL_MAIL Subprograms
       </li>
      </ul>
      <h2>
       DYNAMIC SQL
      </h2>
      <ul>
       <li>
        The Execution Flow of SQL
       </li>
       <li>
        What is Dynamic SQL?
       </li>
       <li>
        Declare Cursor Variables
       </li>
       <li>
        Dynamically Executing a PL/SQL Block
       </li>
       <li>
        Configure Native Dynamic SQL to Compile PL/SQL Code
       </li>
       <li>
        How to invoke DBMS_SQL Package?
       </li>
       <li>
        Implement DBMS_SQL with a Parameterized DML Statement
       </li>
       <li>
        Dynamic SQL Functional Completeness
       </li>
      </ul>
      <h2>
       DESIGN CONSIDERATIONS FOR PL/SQL CODE
      </h2>
      <ul>
       <li>
        Standardize Constants and Exceptions
       </li>
       <li>
        Understand Local Subprograms
       </li>
       <li>
        Write Autonomous Transactions
       </li>
       <li>
        Implement the NOCOPY Compiler Hint
       </li>
       <li>
        Invoke the PARALLEL_ENABLE Hint
       </li>
       <li>
        The Cross-Session PL/SQL Function Result Cache
       </li>
       <li>
        The DETERMINISTIC Clause with Functions
       </li>
       <li>
        Usage of Bulk Binding to Improve Performance
       </li>
      </ul>
      <h2>
       TRIGGERS
      </h2>
      <ul>
       <li>
        Describe Triggers
       </li>
       <li>
        Identify the Trigger Event Types and Body
       </li>
       <li>
        Business Application Scenarios for Implementing Triggers
       </li>
       <li>
        Create DML Triggers using the CREATE TRIGGER Statement and SQL Developer
       </li>
       <li>
        Identify the Trigger Event Types, Body, and Firing (Timing)
       </li>
       <li>
        Differences between Statement Level Triggers and Row Level Triggers
       </li>
       <li>
        Create Instead of and Disabled Triggers
       </li>
       <li>
        How to Manage, Test and Remove Triggers?
       </li>
      </ul>
      <h2>
       CREATING COMPOUND, DDL, AND EVENT DATABASE TRIGGERS
      </h2>
      <ul>
       <li>
        What are Compound Triggers?
       </li>
       <li>
        Identify the Timing-Point Sections of a Table Compound Trigger
       </li>
       <li>
        Understand the Compound Trigger Structure for Tables and Views
       </li>
       <li>
        Implement a Compound Trigger to Resolve the Mutating Table Error
       </li>
       <li>
        Comparison of Database Triggers to Stored Procedures
       </li>
       <li>
        Create Triggers on DDL Statements
       </li>
       <li>
        Create Database-Event and System-Events Triggers
       </li>
       <li>
        System Privileges Required to Manage Triggers
       </li>
      </ul>
      <h2>
       PL/SQL COMPILER
      </h2>
      <ul>
       <li>
        What is the PL/SQL Compiler?
       </li>
       <li>
        Describe the Initialization Parameters for PL/SQL Compilation
       </li>
       <li>
        List the new PL/SQL Compile Time Warnings
       </li>
       <li>
        Overview of PL/SQL Compile Time Warnings for Subprograms
       </li>
       <li>
        List the benefits of Compiler Warnings
       </li>
       <li>
        List the PL/SQL Compile Time Warning Messages Categories
       </li>
       <li>
        Setting the Warning Messages Levels: Using SQL Developer, PLSQL_WARNINGS Initialization Parameter, and the DBMS_WARNING Package Subprograms
       </li>
       <li>
        View Compiler Warnings: Using SQL Developer,
       </li>
       <li>
        SQL*Plus, or the Data Dictionary Views
       </li>
      </ul>
      <h2>
       MANAGE DEPENDENCIES
      </h2>
      <ul>
       <li>
        Overview of Schema Object Dependencies
       </li>
       <li>
        Query Direct Object Dependencies using the USER_DEPENDENCIES View
       </li>
       <li>
        Query an Object's Status
       </li>
       <li>
        Invalidation of Dependent Objects
       </li>
       <li>
        Display the Direct and Indirect Dependencies
       </li>
       <li>
        Fine-Grained Dependency Management in Oracle Database 12c
       </li>
       <li>
        Understand Remote Dependencies
       </li>
       <li>
        Recompile a PL/SQL Program Unit
       </li>
      </ul>
      <h2>
       ORACLE FUSION MIDDLEWARE 11G: BUILD APPLICATIONS WITH ORACLE FORMS
      </h2>
      <ul>
       <li>
        Running a Form
       </li>
       <li>
        Identifying the Data Elements
       </li>
       <li>
        Navigating a Forms Application
       </li>
       <li>
        Using the Modes of Operation
       </li>
       <li>
        Querying Data
       </li>
       <li>
        Inserting, Updating, and Deleting Records
       </li>
       <li>
        Saving Changes
       </li>
       <li>
        Displaying Errors
       </li>
      </ul>
      <h2>
       WORKING IN THE FORMS BUILDER ENVIRONMENT
      </h2>
      <ul>
       <li>
        Forms Builder Key Features
       </li>
       <li>
        Forms Builder Components
       </li>
       <li>
        Navigating the Forms Builder Interface
       </li>
       <li>
        Forms Builder Module Object Hierarchy
       </li>
       <li>
        Customizing Your Forms Builder Session
       </li>
       <li>
        Forms Executables and Module Types
       </li>
       <li>
        Defining Environment Variables
       </li>
       <li>
        Testing a Form with the Run Form Button
       </li>
      </ul>
      <h2>
       CREATING A BASIC FORM MODULE
      </h2>
      <ul>
       <li>
        Creating a New Form Module
       </li>
       <li>
        Creating a New Data Bock
       </li>
       <li>
        Using Template Forms
       </li>
       <li>
        Saving and Compiling a Form Module
       </li>
       <li>
        Module Types and Storage Formats
       </li>
       <li>
        Deploying a Form Module
       </li>
       <li>
        Producing Documentation
       </li>
      </ul>
      <h2>
       CREATING A MASTER-DETAIL FORM
      </h2>
      <ul>
       <li>
        Creating Data Blocks with Relationships
       </li>
       <li>
        Running a Master-Detail Form Module
       </li>
       <li>
        Modifying the Structure of a Data Block
       </li>
       <li>
        Modifying the Layout of a Data Block
       </li>
      </ul>
      <h2>
       WORKING DATA BLOCKS AND FRAMES
      </h2>
      <ul>
       <li>
        Managing Object Properties
       </li>
       <li>
        Creating Visual Attributes
       </li>
       <li>
        Controlling the Behavior and Appearance of Data Blocks
       </li>
       <li>
        Controlling Frame Properties
       </li>
       <li>
        Displaying Multiple Property Palettes
       </li>
       <li>
        Setting Properties on Multiple Objects
       </li>
       <li>
        Copying Properties
       </li>
       <li>
        Creating Control Blocks
       </li>
      </ul>
      <h2>
       WORKING WITH TEXT ITEMS
      </h2>
      <ul>
       <li>
        Creating a Text Item
       </li>
       <li>
        Modifying the Appearance of a Text Item
       </li>
       <li>
        Controlling the Data of a Text Item
       </li>
       <li>
        Altering the Navigational Behavior of a Text Item
       </li>
       <li>
        Enhancing the Relationship between Text Item and Database
       </li>
       <li>
        Adding Functionality to a Text Item
       </li>
       <li>
        Displaying Helpful Messages
       </li>
      </ul>
      <h2>
       CREATING LOVS AND EDITORS
      </h2>
      <ul>
       <li>
        LOVs and Record Groups
       </li>
       <li>
        Creating an LOV Manually
       </li>
       <li>
        Using the LOV Wizard to Create an LOV
       </li>
       <li>
        Setting LOV Properties
       </li>
       <li>
        LOV Column Mapping
       </li>
       <li>
        Defining an Editor
       </li>
       <li>
        Setting Editor Properties
       </li>
       <li>
        Associating an Editor with a Text Item
       </li>
      </ul>
      <h2>
       CREATING ADDITIONAL INPUT ITEMS
      </h2>
      <ul>
       <li>
        Input Items Overview
       </li>
       <li>
        Creating a Check Box
       </li>
       <li>
        Creating a List Item
       </li>
       <li>
        Creating a Radio Group
       </li>
      </ul>
      <h2>
       CREATING NONINPUT ITEMS
      </h2>
      <ul>
       <li>
        Noninput Items Overview
       </li>
       <li>
        Creating a Display Item
       </li>
       <li>
        Creating an Image Item
       </li>
       <li>
        Creating a Push Button
       </li>
       <li>
        Creating a Calculated Item
       </li>
       <li>
        Creating a Hierarchical Tree Item
       </li>
       <li>
        Creating a Bean Area Item
       </li>
      </ul>
      <h2>
       CREATING WINDOWS AND CONTENT CANVASES
      </h2>
      <ul>
       <li>
        Displaying a Form Module in Multiple Windows
       </li>
       <li>
        Creating a New Window
       </li>
       <li>
        Displaying a Form Module on Multiple Layouts
       </li>
       <li>
        Creating a New Content Canvas
       </li>
      </ul>
      <h2>
       WORKING WITH OTHER CANVAS TYPES
      </h2>
      <ul>
       <li>
        Overview of Canvas Types
       </li>
       <li>
        Creating a Stacked Canvas
       </li>
       <li>
        Creating a Toolbar
       </li>
       <li>
        Creating a Tab Canvas
       </li>
       <li>
       </li>
      </ul>
      <h2>
       PRODUCING AND DEBUGGING TRIGGERS
      </h2>
      <ul>
       <li>
        Trigger Overview
       </li>
       <li>
        Creating Triggers in Forms Builder
       </li>
       <li>
        Specifying Execution Hierarchy
       </li>
       <li>
        PL/SQL Editor Features
       </li>
       <li>
        Using the Database Trigger Editor
       </li>
       <li>
        Using Variables in Triggers
       </li>
       <li>
        Adding Functionality with Built-in Subprograms
       </li>
       <li>
        Using the Forms Debugger
       </li>
      </ul>
      <h2>
       ADDING FUNCTIONALITY TO ITEMS
      </h2>
      <ul>
       <li>
        Coding Item Interaction Triggers
       </li>
       <li>
        Interacting with Noninput Items
       </li>
      </ul>
      <h2>
       DISPLAYING RUN-TIME MESSAGES AND ALERTS
      </h2>
      <ul>
       <li>
        Built-Ins and Handling Errors
       </li>
       <li>
        Controlling System Messages
       </li>
       <li>
        The FORM_TRIGGER_FAILURE Exception
       </li>
       <li>
        Triggers for Intercepting System Messages
       </li>
       <li>
        Creating and Controlling Alerts
       </li>
       <li>
        Handling Server Errors
       </li>
      </ul>
      <h2>
       USING QUERY TRIGGERS
      </h2>
      <ul>
       <li>
        Query Processing Overview
       </li>
       <li>
        SELECT Statements Issued During Query Processing
       </li>
       <li>
        Setting WHERE and ORDER BY clauses and ONETIME_WHERE property
       </li>
       <li>
        Writing Query Triggers
       </li>
       <li>
        Query Array Processing
       </li>
       <li>
        Coding Triggers for Enter-Query Mode
       </li>
       <li>
        Overriding Default Query Processing
       </li>
       <li>
        Obtaining Query Information at Run Time
       </li>
      </ul>
      <h2>
       VALIDATING USER INPUT
      </h2>
      <ul>
       <li>
        Validation Process
       </li>
       <li>
        Controlling Validation by Using Properties
       </li>
       <li>
        Controlling Validation by Using Triggers
       </li>
       <li>
        Performing Client-Side Validation with PJCs
       </li>
       <li>
        Tracking Validation Status
       </li>
       <li>
        Using Built-ins to Control When Validation Occurs
       </li>
      </ul>
      <h2>
       CONTROLLING NAVIGATION
      </h2>
      <ul>
       <li>
        Using Object Properties to Control Navigation
       </li>
       <li>
        Writing Navigation Triggers
       </li>
       <li>
        Avoiding the Navigation Trap
       </li>
       <li>
        Using Navigation Built-Ins in Triggers
       </li>
      </ul>
      <h2>
       OVERRIDING OR SUPPLEMENTING TRANSACTION PROCESSING
      </h2>
      <ul>
       <li>
        Transaction Processing Overview
       </li>
       <li>
        Using Commit Triggers
       </li>
       <li>
        Testing the Results of Trigger DML
       </li>
       <li>
        DML Statements Issued during Commit Processing
       </li>
       <li>
        Overriding Default Transaction Processing
       </li>
       <li>
        Getting and Setting the Commit Status
       </li>
       <li>
        Implementing Array DML
       </li>
      </ul>
      <h2>
       WRITING FLEXIBLE CODE
      </h2>
      <ul>
       <li>
        What is Flexible Code?
       </li>
       <li>
        Using System Variables for Flexible Coding
       </li>
       <li>
        Using Built-in Subprograms for Flexible Coding
       </li>
       <li>
        Copying and Subclassing Objects and Code
       </li>
       <li>
        Referencing Objects by Internal ID
       </li>
       <li>
        Referencing Items Indirectly
       </li>
      </ul>
      <h2>
       SHARING OBJECTS AND CODE
      </h2>
      <ul>
       <li>
        Benefits of Reusable Objects and Code
       </li>
       <li>
        Working with Property Classes
       </li>
       <li>
        Working with Object Groups
       </li>
       <li>
        Copying and Subclassing Objects and Code
       </li>
       <li>
        Working with Object Libraries
       </li>
       <li>
        Working with SmartClasses
       </li>
       <li>
        Reusing PL/SQL
       </li>
       <li>
        Working with PL/SQL Libraries
       </li>
      </ul>
      <h2>
       USING WEBUTIL TO INTERACT WITH THE CLIENT
      </h2>
      <ul>
       <li>
        Benefits of WebUtil
       </li>
       <li>
        Integrating WebUtil into a Form
       </li>
       <li>
        Interacting with the Client
       </li>
      </ul>
      <h2>
       INTRODUCING MULTIPLE FORM APPLICATIONS
      </h2>
      <ul>
       <li>
        Multiple Form Applications Overview
       </li>
       <li>
        Starting Another Form Module
       </li>
       <li>
        Defining Multiple Form Functionality
       </li>
       <li>
        Sharing Data among Modules
       </li>
      </ul>
      <h2>
       ORACLE REPORTS DEVELOPER 11G: BUILD REPORTS
      </h2>
      <ul>
       <li>
        Business Intelligence
       </li>
       <li>
        Enterprise Reporting
       </li>
       <li>
        Oracle Reports Developer
       </li>
       <li>
        Oracle Database 11g
       </li>
       <li>
        Oracle Developer Suite 11g
       </li>
       <li>
        Oracle Application Server 11g
       </li>
       <li>
        OracleAS Reports Services
       </li>
       <li>
        OracleAS Reports Services Architecture for the Web
       </li>
      </ul>
      <h2>
       DESIGNING AND RUNNING REPORTS
      </h2>
      <ul>
       <li>
        Understanding User Requirements
       </li>
       <li>
        Designing Reports
       </li>
       <li>
        Tabular
       </li>
       <li>
        Master-Detail
       </li>
       <li>
        Master with Two Details
       </li>
       <li>
        Matrix
       </li>
       <li>
        Retrieving and Sharing Data
       </li>
       <li>
        Running a Report
       </li>
      </ul>
      <h2>
       EXPLORING ORACLE REPORTS DEVELOPER
      </h2>
      <ul>
       <li>
        Reports Developer Executables
       </li>
       <li>
        Invoking Reports Builder
       </li>
       <li>
        Reports Builder Modules
       </li>
       <li>
        Report Data and Layout
       </li>
       <li>
        Reports Builder Components
       </li>
       <li>
        Object Navigator
       </li>
       <li>
        Report-Level Objects
       </li>
       <li>
        Data Model Objects
       </li>
      </ul>
      <h2>
       CREATING A PAPER REPORT
      </h2>
      <ul>
       <li>
        Report Module Components
       </li>
       <li>
        Building a Paper Report
       </li>
       <li>
        Viewing the Paper Report Output
       </li>
       <li>
        Saving the Report Definition
       </li>
       <li>
        Reentering the Wizard
       </li>
       <li>
        Creating Break Reports
       </li>
       <li>
        Break Report Labels
       </li>
       <li>
        Creating Mailing Labels and Letters
       </li>
      </ul>
      <h2>
       ENHANCING A BASIC PAPER REPORT
      </h2>
      <ul>
       <li>
        What Is the Paper Design?
       </li>
       <li>
        The Paper Design Window
       </li>
       <li>
        Modifying a Report
       </li>
       <li>
        Aligning Columns
       </li>
       <li>
        Setting a Format Mask
       </li>
       <li>
        Manipulating Objects
       </li>
       <li>
        Modifying Visual Attributes
       </li>
       <li>
        Applying Conditional Formatting
       </li>
      </ul>
      <h2>
       MANAGING REPORT TEMPLATES
      </h2>
      <ul>
       <li>
        Using Report Templates
       </li>
       <li>
        Modifying a Template
       </li>
       <li>
        Customizing the Template Margin
       </li>
       <li>
        Customizing the Template Body
       </li>
       <li>
        Adding Web Links to a Template for Report HTML Output
       </li>
       <li>
        Predefining Your Own Templates
       </li>
       <li>
        Adding a Template Preview Image
       </li>
      </ul>
      <h2>
       CREATING A WEB REPORT
      </h2>
      <ul>
       <li>
        What Is JSP Technology?
       </li>
       <li>
        JSP Advantages
       </li>
       <li>
        Simple JSP Example
       </li>
       <li>
        Building a Web Report
       </li>
       <li>
        Using the Report Wizard
       </li>
       <li>
        Report Editor: Web Source View
       </li>
       <li>
        JSP Tags
       </li>
       <li>
        Web Source Example
       </li>
      </ul>
      <h2>
       ENHANCING REPORTS USING THE DATA MODEL: QUERIES AND GROUPS
      </h2>
      <ul>
       <li>
        The Data Model Objects
       </li>
       <li>
        Modifying Properties of a Query
       </li>
       <li>
        Applying Changes
       </li>
       <li>
        Changing the Group Structure
       </li>
       <li>
        Group Hierarchy
       </li>
       <li>
        Ordering Data in a Group
       </li>
       <li>
        Query Modifications
       </li>
       <li>
        Filtering Data in a Group
       </li>
      </ul>
      <h2>
       ENHANCING REPORTS USING THE DATA MODEL: DATA SOURCES
      </h2>
      <ul>
       <li>
        Data Source Types
       </li>
       <li>
        Pluggable Data Sources
       </li>
       <li>
        Using XML as a Data Source
       </li>
       <li>
        Document Type Definition File
       </li>
       <li>
        OLAP Data Source
       </li>
       <li>
        Using Text as a Data Source
       </li>
       <li>
        Using JDBC as a Data Source
       </li>
       <li>
        Using REF Cursor Queries
       </li>
      </ul>
      <h2>
       ENHANCING REPORTS USING THE DATA MODEL: CREATING COLUMNS
      </h2>
      <ul>
       <li>
        Data Model Columns
       </li>
       <li>
        Maintaining Data Source Columns
       </li>
       <li>
        Producing File Content Output
       </li>
       <li>
        Creating a Column
       </li>
       <li>
        Creating Summary Columns
       </li>
       <li>
        Displaying Subtotals
       </li>
       <li>
        Displaying Percentages
       </li>
       <li>
        Creating a Formula Column
       </li>
      </ul>
      <h2>
       ENHANCING REPORTS USING THE PAPER LAYOUT
      </h2>
      <ul>
       <li>
        Viewing the Paper Layout
       </li>
       <li>
        Designing Multipanel Reports
       </li>
       <li>
        Printing Multipanel Reports
       </li>
       <li>
        Different Objects in the Paper Layout
       </li>
       <li>
        The Paper Layout Layers
       </li>
       <li>
        Report Processing
       </li>
       <li>
        Paper Layout Tools
       </li>
       <li>
        Report Bursting
       </li>
      </ul>
      <h2>
       CONTROLLING THE PAPER LAYOUT: COMMON PROPERTIES
      </h2>
      <ul>
       <li>
        Modifying Paper Layout Object Properties
       </li>
       <li>
        Common Layout Properties
       </li>
       <li>
        Sizing Objects
       </li>
       <li>
        Anchors
       </li>
       <li>
        Layout Object Relationships
       </li>
       <li>
        Pagination Icons in the Paper Layout
       </li>
       <li>
        Controlling Print Frequency
       </li>
       <li>
        Using Format Triggers
       </li>
      </ul>
      <h2>
       CONTROLLING THE PAPER LAYOUT: SPECIFIC PROPERTIES
      </h2>
      <ul>
       <li>
        Properties of a Repeating Frame
       </li>
       <li>
        Specifying Print Direction
       </li>
       <li>
        Controlling the Number of Records per Page
       </li>
       <li>
        Controlling Spacing Between Records
       </li>
       <li>
        Minimum Widow Records
       </li>
       <li>
        System Variables
       </li>
       <li>
        Valid Source Columns
       </li>
       <li>
        Displaying File Contents
       </li>
      </ul>
      <h2>
       WEB REPORTING
      </h2>
      <ul>
       <li>
        Comparing Static and Dynamic Reporting
       </li>
       <li>
        Adding Dynamic Content
       </li>
       <li>
        Creating a Report Block
       </li>
       <li>
        Invoking the Report Block Wizard
       </li>
       <li>
        Examining the Web Source Code
       </li>
       <li>
        rw:foreach Tag
       </li>
       <li>
        rw:field Tag
       </li>
       <li>
        Customizing Reports JSPs
       </li>
      </ul>
      <h2>
       EXTENDING FUNCTIONALITY USING XML
      </h2>
      <ul>
       <li>
        Why Use XML Report Definitions?
       </li>
       <li>
        Creating XML Report Definitions
       </li>
       <li>
        Partial Report Definitions: Format Modification Example
       </li>
       <li>
        Partial Report Definitions: Format Exception Example
       </li>
       <li>
        Full Report Definition: Data Model Modification Example
       </li>
       <li>
        Running XML Report Definitions
       </li>
       <li>
        Debugging XML Report Definitions
       </li>
      </ul>
      <h2>
       CREATING AND USING REPORT PARAMETERS
      </h2>
      <ul>
       <li>
        Creating User Parameters
       </li>
       <li>
        Referencing Parameters in a Report Query
       </li>
       <li>
        Using Bind References
       </li>
       <li>
        Using Lexical References
       </li>
       <li>
        Hints and Tips When Referencing Parameters
       </li>
       <li>
        Creating a List of Values
       </li>
       <li>
        Referencing System Parameters
       </li>
       <li>
        Building a Paper Parameter Form
       </li>
      </ul>
      <h2>
       EMBEDDING A GRAPH IN A REPORT
      </h2>
      <ul>
       <li>
        Adding a Graph to a Paper Report
       </li>
       <li>
        Adding a Graph to a Web Report
       </li>
       <li>
        Selecting the Graph Type
       </li>
       <li>
        Selecting the Graph Data
       </li>
       <li>
        Adding Options to the Graph
       </li>
       <li>
        Customizing Web Graphs
       </li>
       <li>
        The rw:graph Tag
       </li>
       <li>
        Customizing Graphs Using the Graph.XML File
       </li>
       <li>
       </li>
      </ul>
      <h2>
       ENHANCING MATRIX REPORTS
      </h2>
      <ul>
       <li>
        The Matrix Data Model
       </li>
       <li>
        The Matrix Paper Layout
       </li>
       <li>
        Creating Matrix Summaries
       </li>
       <li>
        Creating the Matrix Manually
       </li>
       <li>
        The Matrix with Group Data Model
       </li>
       <li>
        The Matrix with Group Layout
       </li>
       <li>
        Building a Nested Matrix
       </li>
       <li>
        Nested Matrix Paper Layout
       </li>
      </ul>
      <h2>
       CODING PL/SQL TRIGGERS
      </h2>
      <ul>
       <li>
        Types of Triggers in Reports
       </li>
       <li>
        Trigger Code
       </li>
       <li>
        Using Report Triggers
       </li>
       <li>
        Using Data Model Triggers: PL/SQL Group Filter
       </li>
       <li>
        Using Data Model Triggers: Parameter Validation
       </li>
       <li>
        Using Layout Triggers
       </li>
       <li>
        Using Format Triggers
       </li>
       <li>
        Event-Based Reporting
       </li>
      </ul>
      <h2>
       EXTENDING FUNCTIONALITY USING THE SRW PACKAGE
      </h2>
      <ul>
       <li>
        Contents of the SRW Package
       </li>
       <li>
        Outputting Messages
       </li>
       <li>
        Executing a Nested Report
       </li>
       <li>
        Restricting Data
       </li>
       <li>
        Initializing Fields
       </li>
       <li>
        Creating a Table of Contents
       </li>
       <li>
        Performing DDL Statements
       </li>
       <li>
        Setting Format Attributes
       </li>
      </ul>
      <h2>
       MAXIMIZING PERFORMANCE USING ORACLEAS REPORTS SERVICES
      </h2>
      <ul>
       <li>
        Running Reports Using OracleAS Reports Services
       </li>
       <li>
        Report Request Methods
       </li>
       <li>
        Oracle Application Server Components
       </li>
       <li>
        Enabling Single Sign-On Access
       </li>
       <li>
        Running the Web Layout: JSP Run-time Architecture
       </li>
       <li>
        Running the Paper Layout: Servlet Run-time Architecture
       </li>
       <li>
        Running a Paper Report on the Web
       </li>
       <li>
        Queue Manager
       </li>
      </ul>
      <h2>
       BUILDING REPORTS: EFFICIENCY GUIDELINES
      </h2>
      <ul>
       <li>
        Tuning Reports
       </li>
       <li>
        Performance Measurement
       </li>
       <li>
        Non SQL Data Sources
       </li>
       <li>
        Investigating the Data Model
       </li>
       <li>
        Investigating the Paper Layout
       </li>
       <li>
        Running the Report
       </li>
       <li>
        Different Development and Run-Time Environments
       </li>
       <li>
        Developing Reports to Run in Different GUIs
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="oracle11gdevtrainingindelhincr.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="99 + 39 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="138">
       <input name="url" type="hidden" value="/oracle11gdevtrainingindelhincr/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>