<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     JAVA TRAINING Noida
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     JAVA SIX MONTHS
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      STRONG FOUNDATION IS VERY IMPORTANT FOR STRONG BUILDING
     </h4>
     <p>
      6 MONTHS
      <strong>
       JAVA TRAINING
      </strong>
      is very popular course at DUCAT. This course has the perfect mix of contents to prepare the participants to become a Java developer or programmer. Equipped with Java Training from DUCAT will open a flood gates of opportunities for the participants. The major reasons are the technology itself and the course content. JAVA is used for developing web based and non-web based application and even for programming for mobiles. This makes it a very powerful and sought after technology. Therefore, there is lot of demand for qualified professionals in the field. The JAVA IN 6 MONTHS course at DUCAT prepares the participants to leverage this demand by making them conceptually and practically strong in the technology. This course in DUCAT is designed by industry veterans with a goal to make the learners industry ready. Therefore, along with theory, there are lots of examples and lab exercises for the students to apply their knowledge practically. The participants would get to develop a stand - alone application in Java which includes problem definition, requirements analysis, system design, Java coding and testing. Learning the course doesn't have any prerequisite. The aim of the course is to make the participants would be very strong on basics of Java language and its application as Object Oriented Programming Language (OOPs). Post the course, with this strong foundation in course the participants can opt to do take up job and/or upgrade knowledge learning advanced
      <strong>
       Java Training Noida
      </strong>
     </p>
     <div class="contentAcc">
      <h2>
       J2SE (JAVA 2 STANDARD EDITION) INTRODUCTION To JAVA
      </h2>
      <ul>
       <li>
        Understanding Requirement: why JAVA
       </li>
       <li>
        Why java important to the internet
       </li>
       <li>
        JAVA on LINUX PLATFORM
       </li>
      </ul>
      <h2>
       INTRODUCTION TO JAVA VIRTUAL MACHINE
      </h2>
      <ul>
       <li>
        Java Virtual machine Architecture
       </li>
       <li>
        Class loading process by class loaders
       </li>
       <li>
        Role of Just In time compiler (JIT)
       </li>
       <li>
        Execution Engine
       </li>
      </ul>
      <h2>
       AN OVERVIEW OF JAVA AND BUZZWORDS
      </h2>
      <ul>
       <li>
        Data Types , Variables ad Arrays
       </li>
       <li>
        Operators
       </li>
       <li>
        Control statements
       </li>
       <li>
        Object oriented paradigms
       </li>
       <li>
        Abstractions
       </li>
       <li>
        The Three OOP Principles:
       </li>
       <li>
        (Encapsulation, Inheritance, Polymorphism)
       </li>
      </ul>
      <h2>
       JAVA CLASSES AND OOP IMPLEMENTATION
      </h2>
      <ul>
       <li>
        Class fundamentals
       </li>
       <li>
        Commands line arguments
       </li>
       <li>
        Learning Static Initializer
       </li>
       <li>
        Declaration of objects
       </li>
       <li>
        Instance variable Hiding
       </li>
       <li>
        Overloading and Overriding of Methods
       </li>
       <li>
        Understanding of Access Controls (private, public and protected)
       </li>
       <li>
        Learning Nested and Inner classes
       </li>
       <li>
        Dynamic method Dispatching
       </li>
       <li>
        Using Abstract classes
       </li>
       <li>
        Using final to prevent Overriding &amp; Inheritance
       </li>
       <li>
        Garbage collection
       </li>
      </ul>
      <h2>
       PACKAGES AND INTERFACES
      </h2>
      <ul>
       <li>
        Defining a package
       </li>
       <li>
        Understanding CLASSPATH
       </li>
       <li>
        Access Protection
       </li>
       <li>
        Importing packages
       </li>
       <li>
        Defining and Implementing interfaces
       </li>
       <li>
        Anonymous classes
       </li>
       <li>
        Abstract classes Vs Interfaces
       </li>
       <li>
        Adapter classes
       </li>
      </ul>
      <h2>
       EXCEPTION HANDLING
      </h2>
      <ul>
       <li>
        Fundamental of Exception handling
       </li>
       <li>
        Types of Exceptions
       </li>
       <li>
        Learning exceptions handlers
       </li>
       <li>
        Try and Catch
       </li>
       <li>
        Multiple catch Clauses
       </li>
       <li>
        Nested Try statements
       </li>
       <li>
        Throw , throws and finally
       </li>
       <li>
        Creating custom exceptions
       </li>
       <li>
        Assertion
       </li>
      </ul>
      <h2>
       STRING HANDLING
      </h2>
      <ul>
       <li>
        Learning String Operation
       </li>
       <li>
        Learning character Extraction
       </li>
       <li>
        Learning string Comparison
       </li>
       <li>
        Understanding string Buffer Classes
       </li>
       <li>
        String builder class
       </li>
       <li>
        Creating Immutable Class
       </li>
      </ul>
      <h2>
       NEW IN JDK 5/6/7
      </h2>
      <ul>
       <li>
        Premain method , Object size
       </li>
       <li>
        Generics
       </li>
       <li>
        Annotations
       </li>
       <li>
        Vargs
       </li>
       <li>
        Static Import
       </li>
       <li>
        For each
       </li>
       <li>
        String in which
       </li>
       <li>
        Multiple exception handling
       </li>
       <li>
        Dimond Operator
       </li>
       <li>
        Finding out constant and method declaration belong to an interface
       </li>
       <li>
        Creating an instance of a class whose name is not known until runtime
       </li>
       <li>
        Getting and setting value of an objects field if field name is unknown until runtime
       </li>
       <li>
        Invoking a method on an object if the method is unknown until runtime
       </li>
       <li>
        Creating a new array whose size and components type are not know until runtime
       </li>
       <li>
        Invoking private member of a class
       </li>
      </ul>
      <h2>
       WINDOWS PROGRAMMING Swing &amp; AWT
      </h2>
      <ul>
       <li>
        Introduction to JFC
       </li>
       <li>
        Controls
       </li>
      </ul>
      <h2>
       Event Delegation Method
      </h2>
      <ul>
       <li>
        Event Classes
       </li>
       <li>
        Event listeners
       </li>
      </ul>
      <h2>
       APPLENET
      </h2>
      <ul>
       <li>
        Applenet Basics
       </li>
       <li>
        Architecture and Skeleton
       </li>
       <li>
        Simple Apple Display Methods
       </li>
       <li>
        The HTML Applet Tag
       </li>
       <li>
        Inter Applet communication
       </li>
       <li>
        Trusted Applet (Applet with Database)
       </li>
      </ul>
      <h2>
       Multi threaded Programming
      </h2>
      <ul>
       <li>
        The java thread model
       </li>
       <li>
        Creating a thread: Extended Thread and Implementing Runable
       </li>
       <li>
        Creating multiple Thread and Context Switching
       </li>
       <li>
        Synchronization: methods and statements
       </li>
       <li>
        Inter thread Communication
       </li>
       <li>
        Thread local
       </li>
       <li>
        Dead lock
       </li>
       <li>
        Threadpool executer class
       </li>
       <li>
        Shutdown hookup
       </li>
       <li>
        Re-entrent locking
       </li>
       <li>
        The java thread model
       </li>
       <li>
        Creating a thread: Extended Thread and Implementing Runable
       </li>
       <li>
        Creating multiple Thread and Context Switching
       </li>
       <li>
        Synchronization: methods and statements
       </li>
       <li>
        Inter thread Communication
       </li>
       <li>
        Thread local
       </li>
       <li>
        Dead lock
       </li>
       <li>
        Threadpool executer class
       </li>
       <li>
        Shutdown hookup
       </li>
       <li>
        Re-entrent locking
       </li>
      </ul>
      <h2>
       Introduction to I/O streams
      </h2>
      <ul>
       <li>
        File handling
       </li>
       <li>
        Binary Streams
       </li>
       <li>
        Character stream
       </li>
       <li>
        Serialization
       </li>
       <li>
        Scanner
       </li>
       <li>
        Stream tokenizer
       </li>
       <li>
        String tokenizer
       </li>
       <li>
        GzipOutStream class
       </li>
       <li>
        Data Encryption &amp; decryption
       </li>
      </ul>
      <h2>
       NETWORKING
      </h2>
      <ul>
       <li>
        URL, InetAddress
       </li>
       <li>
        Socket And Server Socket
       </li>
       <li>
        Datagram socket
       </li>
       <li>
        Socket factories
       </li>
      </ul>
      <h2>
       Reflection API
      </h2>
      <ul>
       <li>
        Determining the class of an object
       </li>
       <li>
        Getting information about classes modifier, fields, methods, constructor, and super classes
       </li>
      </ul>
      <h2>
       ADVANCED JAVA/J2EE (JAVA 2 ENTERPRISE EDITION)
      </h2>
      <h2>
       COLLECTION FRAMEWORK
      </h2>
      <ul>
       <li>
        The Collection Interfaces (list , set, Sorted set)
       </li>
       <li>
        The collection classes (The array list, Linked list, Hash set, Tree set)
       </li>
       <li>
        Accessing a Collection via an Iterator
       </li>
       <li>
        Working with maps
       </li>
       <li>
        Working with Comparators
       </li>
       <li>
        The Collection Algorithms
       </li>
       <li>
        The Legacy Classes and Interfaces (Enumeration, Vector, Stack, Dictionary, Hash table)
       </li>
       <li>
        Date and Time Handling
       </li>
       <li>
        COLLECTION CLASS
       </li>
       <li>
        Array class
       </li>
      </ul>
      <h2>
       SYSTEM PROPERTIES &amp; INTERNATIONALIZATION
      </h2>
      <ul>
       <li>
        Usage of Property file
       </li>
       <li>
        Define the locale
       </li>
       <li>
        ResourceBundle
       </li>
       <li>
        Fetching Text from ResourceBundle
       </li>
      </ul>
      <h2>
       REMOTE METHOD INVOCATION (RMI)
      </h2>
      <ul>
       <li>
        Distributed Applications
       </li>
       <li>
        RMI Architecture
       </li>
       <li>
        Implementation
       </li>
       <li>
        Call-Back Mechanism
       </li>
       <li>
        @WebFilter
       </li>
       <li>
        @WebInitParam
       </li>
       <li>
        @WebListener
       </li>
       <li>
        @WebServlet
       </li>
       <li>
        @MultipartConfig
       </li>
       <li>
        @ServletSecurity
       </li>
       <li>
        File uploading/file downloading
       </li>
       <li>
        Security
       </li>
       <li>
        Refreshing servlet
       </li>
      </ul>
      <h2>
       DATABASE PROGRAMMING USING JDBC(4.1)
      </h2>
      <ul>
       <li>
        JDBC Drivers
       </li>
       <li>
        Statements
       </li>
       <li>
        Metadata
       </li>
       <li>
        Scrollable &amp; Updatable Result set
       </li>
       <li>
        Batch Updates
       </li>
       <li>
        Data Sources And Connecting Pooling
       </li>
       <li>
        Row sets
       </li>
       <li>
        Transaction (commit , rollback,savepoint)
       </li>
       <li>
        Getting Data from excel sheet
       </li>
       <li>
        Generating log file
       </li>
      </ul>
      <h2>
       INTRODUCTION TO J2EE ARCHITECTURE TIER ARCHITECTURE
      </h2>
      <ul>
       <li>
        Single Tier
       </li>
       <li>
        Two Tier
       </li>
       <li>
        Three Tier
       </li>
       <li>
        N Tier
       </li>
      </ul>
      <h2>
       J2EE COMPONENTS
      </h2>
      <ul>
       <li>
        Web components
       </li>
       <li>
        Business components
       </li>
      </ul>
      <h2>
       J2EE CONTAINERS
      </h2>
      <ul>
       <li>
        Containers Type
       </li>
       <li>
        Containers Services
       </li>
      </ul>
      <h2>
       J2EE Services
      </h2>
      <ul>
       <li>
        Java Naming and Directory Interfaces
       </li>
       <li>
        Java Transaction Services
       </li>
       <li>
        Java Messaging Services
       </li>
       <li>
        Java Authentication &amp; Authorization Services
       </li>
      </ul>
      <h2>
       INTRODUCATION TO UML
      </h2>
      <ul>
       <li>
        Use Cases
       </li>
       <li>
        Diagrams
       </li>
      </ul>
      <h2>
       INTRODUCTION TO XML
      </h2>
      <ul>
       <li>
        Web components
       </li>
       <li>
        Business components
       </li>
      </ul>
      <h2>
       J2EE COMPONENTS
      </h2>
      <ul>
       <li>
        Document type Definition (DTD)
       </li>
       <li>
        XML parsers
       </li>
       <li>
        Document object module(DOM)
       </li>
       <li>
        Simple API for XML (SAX)
       </li>
      </ul>
      <h2>
       JAVA SERVLET
      </h2>
      <ul>
       <li>
        Introduction to Web Programming
       </li>
       <li>
        Advantages to Servlet
       </li>
       <li>
        Servlet Lifecycle
       </li>
       <li>
        Request Dispatching
       </li>
       <li>
        Session Tracker
       </li>
       <li>
        Event Listener
       </li>
       <li>
        Dependency Injection
       </li>
       <li>
        Filters
       </li>
       <li>
        Servlet with Annotation
       </li>
      </ul>
      <h2>
       JAVA SERVER PAGES (JSP) &amp; JSTL
      </h2>
      <ul>
       <li>
        JSP Architecture
       </li>
       <li>
        JSP Element
       </li>
       <li>
        JSP Directives
       </li>
       <li>
        JSP Actions
       </li>
       <li>
        JSP Objects
       </li>
       <li>
        Custom Tags
       </li>
       <li>
        Using Tags of JSTL
       </li>
       <li>
        Expression Language
       </li>
       <li>
        Exception handling in JSP
       </li>
      </ul>
      <h2>
       Ajax
      </h2>
      <ul>
       <li>
        XMLHTTPRequest
       </li>
       <li>
        Ready State
       </li>
       <li>
        OnreadystateChange
       </li>
       <li>
        ResponseText
       </li>
       <li>
        ResponseXML
       </li>
       <li>
        Status
       </li>
       <li>
        StatusText
       </li>
       <li>
        Div Tag
       </li>
      </ul>
      <h2>
       Jquery
      </h2>
      <ul>
       <li>
        Jquery with Ajax
       </li>
       <li>
        Jquery Event
       </li>
       <li>
        Jquery Selectors
       </li>
       <li>
        JSON
       </li>
       <li>
        Calling JSP with Jquery
       </li>
       <li>
        Animation
       </li>
       <li>
        get ( ) Function
       </li>
       <li>
        Jquery Ajax Event
       </li>
      </ul>
      <h2>
       ENTERPRISE JAVA BEANS (EJB-3.2)
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Architecture
       </li>
       <li>
        Types of EJB
       </li>
      </ul>
      <h2>
       SESSION BEANS
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        State Management
       </li>
       <li>
        Dpendency Injection
       </li>
       <li>
        Interceptors
       </li>
       <li>
        Timer Service
       </li>
       <li>
        Life cycle Callback Methods
       </li>
       <li>
        @PostConstruct
       </li>
       <li>
        @PreDestroy
       </li>
       <li>
        @PreActivate
       </li>
       <li>
        @PrePassivate
       </li>
       <li>
        Types
       </li>
       <li>
        Stateless
       </li>
       <li>
        State full
       </li>
       <li>
        Singleton
       </li>
      </ul>
      <h2>
       ENTITY BEANS
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Java Persistence API
       </li>
       <li>
        Java Persistence Query
       </li>
       <li>
        Language(JPQL)
       </li>
       <li>
        The Entity Life Cycle
       </li>
       <li>
        Entity Relationships
       </li>
       <li>
        @One To One
       </li>
       <li>
        @One To Many
       </li>
       <li>
        @Many To One
       </li>
       <li>
        @Many To Many
       </li>
       <li>
        @Entity
       </li>
       <li>
        @Id
       </li>
       <li>
        @Table
       </li>
       <li>
        @Column
       </li>
       <li>
        @Basic
       </li>
      </ul>
      <h2>
       MESSAGE DRIVEN BEANS &amp; JMS2.0
      </h2>
      <ul>
       <li>
        Messaging overview
       </li>
       <li>
        Messaging models
       </li>
       <li>
        Point to point models
       </li>
       <li>
        Topic subscriber models
       </li>
       <li>
        JMS Implementation
       </li>
       <li>
        Life cycle
       </li>
       <li>
        @MessageDriven
       </li>
       <li>
        @ActivationConfigProperty
       </li>
      </ul>
      <h2>
       J2EE DESIGN PATTERN
      </h2>
      <ul>
       <li>
        Why design pattern?
       </li>
       <li>
        Front controller
       </li>
       <li>
        Composite view
       </li>
       <li>
        Session fa�ade
       </li>
       <li>
        Service Locator
       </li>
       <li>
        Data Access Object
       </li>
       <li>
        Value object
       </li>
       <li>
        Singleton pattern , factory pattern
       </li>
      </ul>
      <h2>
       JAVA MAIL
      </h2>
      <ul>
       <li>
        Email system and Protocols
       </li>
       <li>
        Architecture
       </li>
       <li>
        Sending mail
       </li>
       <li>
        Receiving mail
       </li>
       <li>
        Handling attachments
       </li>
       <li>
        Replying &amp; forwarding
       </li>
      </ul>
      <h2>
       PACKAGING AND DEPLOYMENT USING ANT
      </h2>
      <h2>
       BLUETOOTH API
      </h2>
      <ul>
       <li>
        Local Device
       </li>
       <li>
        UUID class
       </li>
       <li>
        Discovery Listener
       </li>
      </ul>
      <h2>
       Introduction to Web services
      </h2>
      <ul>
       <li>
        A conceptual overview of Web Services
       </li>
       <li>
        Web services requirements
       </li>
       <li>
        SOAP
       </li>
       <li>
        WSDL
       </li>
       <li>
        UDDI
       </li>
      </ul>
      <h2>
       UTILITIES
      </h2>
      <ul>
       <li>
        My Eclipse 2013
       </li>
       <li>
        NetBeans 6.5
       </li>
      </ul>
      <h2>
       WEB SERVER
      </h2>
      <ul>
       <li>
        Apache Tomcat 7.0
       </li>
      </ul>
      <h2>
       APPLICATION SERVER
      </h2>
      <ul>
       <li>
        Bea's Web logic 10.3/12c
       </li>
       <li>
        J Boss
       </li>
       <li>
        Sun Application server
       </li>
       <li>
        Web share
       </li>
      </ul>
      <h2>
       STRUTS 2.X curriculum
      </h2>
      <h2>
       STRUTS 2 FRAMEWORK
      </h2>
      <ul>
       <li>
        MVC /Model2
       </li>
       <li>
        Filter
       </li>
       <li>
        Action
       </li>
       <li>
        Result
       </li>
       <li>
        Interceptors
       </li>
       <li>
        Valuestack, ONGC and DATA transfer
       </li>
       <li>
        Action Context
       </li>
      </ul>
      <h2>
       WORKING WITH STRUTS 2 ACTIONS
      </h2>
      <ul>
       <li>
        Introducing Struts 2 actions
       </li>
       <li>
        Packing your actions
       </li>
       <li>
        Implementing Actions
       </li>
       <li>
        Transferring data onto objects
       </li>
       <li>
        File uploading: a case study
       </li>
      </ul>
      <h2>
       ADDING WORKFLOW WITH INTERCEPTO
      </h2>
      <ul>
       <li>
        Why intercept requests
       </li>
       <li>
        Interceptors in actions
       </li>
       <li>
        Surveying the built �in struts 2 interceptor
       </li>
       <li>
        Building your own interceptor
       </li>
      </ul>
      <h2>
       DATA TRANSFER: OGNL AND TYPE CONVERSION
      </h2>
      <ul>
       <li>
        Data transfer and type conversion
       </li>
       <li>
        OGNL and Struts 2
       </li>
       <li>
        Built-in type convertors
       </li>
       <li>
        Customizing type conversion
       </li>
      </ul>
      <h2>
       BUILDING A VIEW : TAGS
      </h2>
      <ul>
       <li>
        An overview of struts tags
       </li>
       <li>
        Data tags
       </li>
       <li>
        Miscellaneous tags
       </li>
       <li>
        Using JSTL and other native tags
       </li>
       <li>
        A brief primer for the OGNL expression language
       </li>
      </ul>
      <h2>
       UI COMPONENT TAGS
      </h2>
      <ul>
       <li>
        Why we need UI components tags
       </li>
       <li>
        Tags, template, and themes
       </li>
       <li>
        UI component tag reference
       </li>
      </ul>
      <h2>
       RESULTS IN DETAIL
      </h2>
      <ul>
       <li>
        Life after action
       </li>
       <li>
        Commonly used result types
       </li>
       <li>
        Global results
       </li>
      </ul>
      <h2>
       INTEGRATING WITH SPRING AND HIBERNATE/JPA
      </h2>
      <ul>
       <li>
        Why use spring with struts 2?
       </li>
       <li>
        Adding to spring to struts 2
       </li>
       <li>
        Why use the java persistence API with struts 2?
       </li>
      </ul>
      <h2>
       EXPLORING THE VALIDATION FRAMEWORK
      </h2>
      <ul>
       <li>
        Getting familiar with the validation framework
       </li>
       <li>
        Wiring your actions for validation
       </li>
       <li>
        Writing a custom validator
       </li>
       <li>
        Validation framework advanced topics
       </li>
      </ul>
      <h2>
       UNDERSTANDING INTERNATIONALIZATION
      </h2>
      <ul>
       <li>
        The struts 2 framework and JAVA i18n
       </li>
       <li>
        A struts 2 i18n demo
       </li>
       <li>
        Struts 2 i18n: the details
       </li>
       <li>
        Overloading the framework's default locale determination
       </li>
      </ul>
      <h2>
       Struts with Annotation
      </h2>
      <h2>
       DATABASE CURRICULUM
      </h2>
      <h2>
       SQL Basic
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Select
       </li>
       <li>
        Where
       </li>
       <li>
        Insert
       </li>
       <li>
        Update
       </li>
       <li>
        Delete
       </li>
      </ul>
      <h2>
       SQL ADVANCED
      </h2>
      <ul>
       <li>
        SQL order By
       </li>
       <li>
        SQL AND &amp; OR
       </li>
       <li>
        SQL in
       </li>
       <li>
        SQL between
       </li>
       <li>
        SQL join
       </li>
       <li>
        SQL union
       </li>
       <li>
        SQL create
       </li>
       <li>
        SQL drop
       </li>
       <li>
        SQL alter
       </li>
       <li>
        Decode SQL
       </li>
       <li>
        SQL group By
       </li>
       <li>
        SQL select by
       </li>
       <li>
        SQL create view
       </li>
       <li>
        Creating index
       </li>
      </ul>
      <h2>
       SQL Functions
      </h2>
      <ul>
       <li>
        Aggregate functions
       </li>
       <li>
        Scalar functions
       </li>
      </ul>
      <h2>
       Working with Dates Introduction to PL Working with Procedure and Functions Working with Triggers Working with Sequences
      </h2>
      <h2>
       HIBERNATE 3.0
      </h2>
      <h2>
       INTRODUCTION TO MAPPING OBJECTS TO RELATIONAL DATABASES
      </h2>
      <ul>
       <li>
        Serialization 2
       </li>
       <li>
        XML2
       </li>
       <li>
        Object- oriented Database system
       </li>
       <li>
        Mapping
       </li>
      </ul>
      <h2>
       INTRODUCTION TO HIBERNATE
      </h2>
      <ul>
       <li>
        Hibernate Architecture
       </li>
       <li>
        Hibernate configuration
       </li>
       <li>
        Hibernate's Support for Other Technologies
       </li>
       <li>
        Installing Hibernate
       </li>
       <li>
        A �Hello world� stand alone application
       </li>
       <li>
        A Servlet�Based Hibernate application
       </li>
      </ul>
      <h2>
       CREATING PERSISTING CLASSES
      </h2>
      <ul>
       <li>
        Mapping a basic Java Class
       </li>
       <li>
        Mapping a Class with Binary Data
       </li>
       <li>
        Mapping a Serializable Class
       </li>
       <li>
        Mapping a class with Data/ calendar attributes
       </li>
       <li>
        Mapping a Read-only class
       </li>
       <li>
        Mapping a class using Versioning /Timestamps
       </li>
      </ul>
      <h2>
       MAPPING INHERITENCE WITH JAVA CLASSES
      </h2>
      <ul>
       <li>
        Table-Per �class Hierarchy Mapping
       </li>
       <li>
        Table-Per �subclass Hierarchy Mapping
       </li>
       <li>
        Table-Per �concrete-subclass Hierarchy Mapping
       </li>
       <li>
        Persistence interfaces
       </li>
      </ul>
      <h2>
       WORKING WITH COLLECTIONS
      </h2>
      <ul>
       <li>
        Associations
       </li>
       <li>
        Lazy initialization
       </li>
       <li>
        Mapping Maps/Sorted Maps
       </li>
       <li>
        Mapping Sets/Sorted Sets
       </li>
       <li>
        Mapping lists
       </li>
       <li>
        Mapping Arrays
       </li>
       <li>
        Mapping a Bidirectional Association
       </li>
      </ul>
      <h2>
       USING PERSISTENT OBJECTS
      </h2>
      <ul>
       <li>
        Persisting Objects
       </li>
       <li>
        Loading Data into an Object
       </li>
       <li>
        Updating and finding Objects
       </li>
      </ul>
      <h2>
       SCALAR QUERIES AND HIBERNATE QUERY LANGUAGE
      </h2>
      <ul>
       <li>
        Queries
       </li>
       <li>
        Named Queries
       </li>
       <li>
        SQL Queries
       </li>
       <li>
        Hibernate Queries language
       </li>
      </ul>
      <h2>
       HIBERNATE CACHING
      </h2>
      <ul>
       <li>
        Setting Up a Session Factory Cache
       </li>
       <li>
        Using the persistent Object cache
       </li>
       <li>
        Using the Query cache
       </li>
       <li>
        Setting Up EHcache
       </li>
      </ul>
      <h2>
       HIBERNATE TRANSACTIONS AND LOCKING
      </h2>
      <ul>
       <li>
        Configuration
       </li>
       <li>
        Database support
       </li>
       <li>
        Using Transactions
       </li>
       <li>
        The Transactions API
       </li>
       <li>
        Transaction Example Using Oracle
       </li>
       <li>
        Locking
       </li>
      </ul>
      <h2>
       HIBERNATE AND XDOCLET
      </h2>
      <ul>
       <li>
        Introduction to XDoclet
       </li>
       <li>
        Using XDOclet with Hibernate
       </li>
       <li>
        Hibernate XDoclet Tags and description
       </li>
      </ul>
      <h2>
       HIBERNATE AND ECLIPSE
      </h2>
      <ul>
       <li>
        Hibernator
       </li>
       <li>
        HiberClipse
       </li>
       <li>
        Hibernate synchronizer
       </li>
      </ul>
      <h2>
       INTEGRATION OF STRUTS 2, HIBERNATE AND SPRINGINTRODUCTION TO HIBERNATE
      </h2>
      <ul>
       <li>
        Struts and Hibernate
       </li>
       <li>
        Spring and Hibernate
       </li>
       <li>
        Struts, Spring and Hibernate
       </li>
      </ul>
      <h2>
       SPRING v3.0
      </h2>
      <h2>
       What is Spring?
      </h2>
      <ul>
       <li>
        Spring modules
       </li>
       <li>
        Understanding dependency Injection
       </li>
       <li>
        Applying aspect-oriented programming
       </li>
      </ul>
      <h2>
       Basic bean wiring
      </h2>
      <ul>
       <li>
        Containing your Bean
       </li>
       <li>
        Creating bean
       </li>
       <li>
        Injecting into bean properties
       </li>
       <li>
        Auto wiring
       </li>
       <li>
        Controlling bean creation
       </li>
      </ul>
      <h2>
       Advanced Bean wiring
      </h2>
      <ul>
       <li>
        Declaring parent and Child Bean
       </li>
       <li>
        Applying method injection
       </li>
       <li>
        Injecting Non-spring Beans
       </li>
       <li>
        Registering Custom property editors
       </li>
      </ul>
      <h2>
       Advising Beans
      </h2>
      <ul>
       <li>
        Introducing AOP
       </li>
       <li>
        Creating classic spring aspects
       </li>
       <li>
        Creating advice
       </li>
       <li>
        Defining Pointcuts and Advisors
       </li>
       <li>
        Using proxyFactory Bean
       </li>
       <li>
        Autoproxying
       </li>
      </ul>
      <h2>
       Hitting the Database
      </h2>
      <ul>
       <li>
        Learning spring's data Access Philosphy
       </li>
       <li>
        Configuring a data source
       </li>
       <li>
        Using JDBC with Spring
       </li>
       <li>
        Working with JDBC Templates
       </li>
       <li>
        Using Spring's DAO Support Classes for JDBC
       </li>
       <li>
        Integrating Hibernate with Spring
       </li>
       <li>
        Caching
       </li>
      </ul>
      <h2>
       Spring and POJO based remote services
      </h2>
      <ul>
       <li>
        An overview of Spring remoting
       </li>
       <li>
        Working with RMI
       </li>
       <li>
        Remoting with Hessian and Burlap
       </li>
       <li>
        Using Spring's Http Invoker
       </li>
       <li>
        Spring and Web services
       </li>
      </ul>
      <h2>
       Building Contract-First Web services in spring
      </h2>
      <ul>
       <li>
        Introducing Spring-WS
       </li>
       <li>
        Defining Contract (First!)
       </li>
       <li>
        Handling messages with service endpoints
       </li>
       <li>
        Wiring it all together
       </li>
       <li>
        Consuming Spring-WS Web services
       </li>
      </ul>
      <h2>
       Spring and Enterprise JAVA beans
      </h2>
      <ul>
       <li>
        Wiring EJBs in Spring
       </li>
       <li>
        Spring and EJB3
       </li>
      </ul>
      <h2>
       Handling Web Requests
      </h2>
      <ul>
       <li>
        Getting started with Spring MVC
       </li>
       <li>
        Mapping requests to controller
       </li>
       <li>
        Handling request with controller
       </li>
       <li>
        Handling exceptions
       </li>
      </ul>
      <h2>
       Integrating with other Web frameworks
      </h2>
      <ul>
       <li>
        Using spring with struts
       </li>
       <li>
        Registering the Spring plug-in with struts
       </li>
       <li>
        Writing springware struts actions
       </li>
       <li>
        What about struts 2?
       </li>
       <li>
        Working Spring into webwork2 / struts 2
       </li>
      </ul>
      <h2>
       USPs
      </h2>
      <h2>
       Books
      </h2>
      <h2>
       LABS
      </h2>
      <ul>
       <li>
        7:30AM To 10:30PM
       </li>
      </ul>
      <h2>
       LIVE PROJECTS
      </h2>
      <h2>
       COURSE CERTIFICATE
      </h2>
      <h2>
       CONFIRMATION LETTER TRAINING CERTIFICATE PROJECT CERTIFICATEINTRODUCTION TO HIBERNATE
      </h2>
      <ul>
       <li>
        For Degree Pursing Candidates to Submit in Colleges
       </li>
      </ul>
      <h2>
       UPDATE COURSE CONTENTS(U.C.C)
      </h2>
      <ul>
       <li>
        Regular Updated Course Contents According To Submit in Colleges
       </li>
      </ul>
      <h2>
       CONSULTANTS/FACULTIES
      </h2>
      <ul>
       <li>
        Best Part of the Industry who is talking Corporate Batches
       </li>
       <li>
        Eg. SDG, XANSA, HCL, WIPRO, PYRAMID, CMS, PATNI &amp; MANYM More...
       </li>
      </ul>
      <h2>
       INDUSTRY COLLABORATION &amp; AWARENESS FROM
      </h2>
      <ul>
       <li>
        Bangalore, Hyderabad &amp; Pune
       </li>
      </ul>
      <h2>
       TECHNICAL PANEL FROM INDUSTRY IN DIFFERENT DOMAINS
      </h2>
      <ul>
       <li>
        IBM, FIDELITY, STERIA, HCL, WIPRO, BIRLA SOFT &amp; ManyMore...
       </li>
      </ul>
      <h2>
       100% JOB ASSISTANCE
      </h2>
      <ul>
       <li>
        Outsourcing
       </li>
       <li>
        Resourcing Through Consultancy Services
       </li>
       <li>
        Regular Walk Ins
       </li>
       <li>
        Exclusive Walk Ins For DUCATiens
       </li>
       <li>
        Campus Interviews
       </li>
       <li>
        Resume Sending
       </li>
       <li>
        References
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="javatraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="22 + 73 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="95">
       <input name="url" type="hidden" value="/javatraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>