<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     EMBEDDED 6 MONTHS
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     EMBEDDED 6 MONTHS
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h5>
      6 MONTH EMBEDDED SYSTEM PROGRAM- BE CREATIVE &amp; DESERVING TO ATTAIN THE GOAL
     </h5>
     <p>
      Have you ever thought that you are working in firm and the boss gives you a task to furnish in a time constraint? You take worry search the tasks on Google in single world you start exploring. This is the synonymous word engineering means exploring. But the exploration should be in the right direction else you get confused. We the electronics engineer should have good platform in hardware &amp; software domain both so that we can attain a better career in our core domains. However the hardware platforms require budget and little more time than APIs but this is the work EC &amp; EE engineers are meant. Once you are good in both prospectuses then only you can go for the core companies which has requirement for hardware &amp; software both. Embedded process logic is a much needed platform you should learn so that you have good base in both hardware and software. This base level knowledge is compulsory to attain good carrier in core EC/EE domain. The course cover both the hardware &amp; software concepts with real time implementation and makes you eligible to apply for the software companies, hardware companies and our core domain companies which require both.
     </p>
     <p>
      DUCAT takes responsibility to guide you to bring up the technology understanding and implementation in a easy way and minimum frame of time as technology is boosting up fast and you are well know first deserve then desire.! We try our best to make aware, capable &amp; deserving for the specific field. DUCAT, offers 3-6 Months training program with the live classes by the experienced and industrial professional to help the students to get the right concept in a short span of time. This training session is hardly priced and of very much worthy.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction to  'C'
      </h2>
      <ul>
       <li>
        Objectives of C
       </li>
       <li>
        Applications of       C
       </li>
       <li>
        Relational and       logical operators
       </li>
       <li>
        Bit wise       operators
       </li>
       <li>
        The assignment       statement
       </li>
       <li>
        Intermixing of       data types
       </li>
       <li>
        type conversion
       </li>
       <li>
        cast Operator
       </li>
       <li>
        Multiple       assignment
       </li>
       <li>
        Type definitions
       </li>
       <li>
        Input/Output       Routines
       </li>
       <li>
        Formatted and       unformatted I/O operations
       </li>
      </ul>
      <h2>
       Control Flow  Statements In 'C'
      </h2>
      <ul>
       <li>
        If statement
       </li>
       <li>
        else-if       statement
       </li>
       <li>
        While statement
       </li>
       <li>
        for loop, do,       while loop
       </li>
       <li>
        Switch
       </li>
       <li>
        break and       continue
       </li>
       <li>
        goto
       </li>
      </ul>
      <h2>
       Functions
      </h2>
      <ul>
       <li>
        Definition of       function and it's uses
       </li>
       <li>
        Format of a       function
       </li>
       <li>
        Calling the       function
       </li>
       <li>
        C storage       classes - extern
       </li>
       <li>
        Automatic       variables
       </li>
       <li>
        Static variables
       </li>
       <li>
        Register       variables Recursive functions
       </li>
       <li>
        Command line       arguments
       </li>
      </ul>
      <h2>
       Array and  String
      </h2>
      <ul>
       <li>
        1-D,2-D array       and string
       </li>
       <li>
        String handling       library functions
       </li>
       <li>
        Additional       string functions
       </li>
       <li>
        Searching
       </li>
       <li>
        Sorting with       different algorithms
       </li>
       <li>
        ferror()
       </li>
       <li>
        ftell()
       </li>
       <li>
        feof(),
       </li>
       <li>
        fopen,fwrite,fread,
       </li>
       <li>
        File handling       system calls - open(),
       </li>
       <li>
        read(), write(),       lseek(), close(),
       </li>
       <li>
        Database       handling in C
       </li>
      </ul>
      <h2>
       Pointers
      </h2>
      <ul>
       <li>
        Introduction to       pointers
       </li>
       <li>
        The 'address of'       and 'indirection' operators
       </li>
       <li>
        Pointer       expression
       </li>
       <li>
        Data types of       pointers
       </li>
       <li>
        Pointers and       arrays
       </li>
       <li>
        Assignment of       pointers
       </li>
       <li>
        Pointer       arithmetic
       </li>
       <li>
        Comparison of       two pointers
       </li>
       <li>
        Pointers and       functions
       </li>
       <li>
        Pointers and       strings
       </li>
      </ul>
      <h2>
       C Preprocessor
      </h2>
      <ul>
       <li>
        Macros with       Arguments
       </li>
       <li>
        Macro Versus       Function
       </li>
       <li>
        Directive
       </li>
       <li>
        Conditional       Directive
       </li>
      </ul>
      <h2>
       Structures
      </h2>
      <ul>
       <li>
        Introduction to       structures
       </li>
       <li>
        Declaration and       reference,
       </li>
       <li>
        Accessing       structure elements,
       </li>
       <li>
        Array of       structures,
       </li>
       <li>
        Nested       structures,
       </li>
       <li>
        Self-referential       structures,
       </li>
      </ul>
      <h2>
       Union    Enumerated Data Type
      </h2>
      <ul>
       <li>
        Introduction to       Union
       </li>
       <li>
        Dynamic memory       allocation
       </li>
       <li>
        Typedef       statement
       </li>
      </ul>
      <h2>
       Files
      </h2>
      <ul>
       <li>
        Introduction and       need for a file
       </li>
       <li>
        Library       functions to open/close a file,
       </li>
       <li>
        Functions to       read/write a single
       </li>
       <li>
        Character from a       file
       </li>
       <li>
        Formatted input       output functions used in file
       </li>
       <li>
        handling       fscanf(),
       </li>
       <li>
        fprintf(),
       </li>
       <li>
        fgets(), fputs()
       </li>
       <li>
        Flushing       buffers,
       </li>
       <li>
        Functions used       in file handling - fseek(),
       </li>
      </ul>
      <h2>
       Interfacing of  Seven and Fourteen Segment Display
      </h2>
      <ul>
       <li>
        Introduction to       7 Segment Display
       </li>
       <li>
        Types of 7       Segment Display
       </li>
       <li>
        Interfacing       Circuit Description of 7 Segment Display
       </li>
       <li>
        Programming of 7       Segment Display Interfacing
       </li>
       <li>
        Introduction to       14 Segment Display
       </li>
       <li>
        Types of 14       Segment Display
       </li>
       <li>
        Interfacing       Circuit Description of 14 Segment Display
       </li>
      </ul>
      <h2>
       8051 (MicroControllers  using Assembly and C
      </h2>
      <h2>
       Introduction to  Embedded System
      </h2>
      <ul>
       <li>
        History         Need of Embedded System
       </li>
       <li>
        Basic components       of Embedded System
       </li>
       <li>
        Hardware       Classification of Embedded System
       </li>
       <li>
        Programming       Language Classification of Embedded System
       </li>
       <li>
        Advantage         Disadvantage of Low level   High level programming language of       Embedded System
       </li>
      </ul>
      <h2>
       Microprocessor    Microcontroller Classification
      </h2>
      <ul>
       <li>
        Difference       between Microprocessor   Microcontroller
       </li>
       <li>
        Classification       based on architecture
       </li>
       <li>
        Classification       based on Instruction Set
       </li>
       <li>
        Type of       Microcontroller
       </li>
       <li>
        Memory       Classification
       </li>
      </ul>
      <h2>
       Introduction to  8051 Microcontroller
      </h2>
      <ul>
       <li>
        Introduction of       ATMEL 8051 family
       </li>
       <li>
        Block diagram       description of AT89C51
       </li>
       <li>
        Special feature       of AT89C51
       </li>
       <li>
        Pin description       of AT89C51
       </li>
      </ul>
      <h2>
       Registers    Memory of AT89C51
      </h2>
      <ul>
       <li>
        Description of       RAM
       </li>
       <li>
        Description of       CPU Registers
       </li>
       <li>
        Function of SFR
       </li>
      </ul>
      <h2>
       Assembly  Language Programming of AT89C51
      </h2>
      <ul>
       <li>
        Addressing modes       of AT89C51
       </li>
       <li>
        Directives of Assembly       Language
       </li>
       <li>
        Data Transfer       Instruction
       </li>
       <li>
        Jump Instruction
       </li>
       <li>
        Arithmetic       Instruction
       </li>
       <li>
        Logical       Instruction
       </li>
       <li>
        Branching       Instruction
       </li>
      </ul>
      <h2>
       Interfacing of  LED AND MATRIX
      </h2>
      <ul>
       <li>
        Introduction of       LED's
       </li>
       <li>
        Interfacing       Circuit Description of LED's
       </li>
       <li>
        Programming of       LED's Interfacing
       </li>
       <li>
        Interfacing of       LED Matrix
       </li>
      </ul>
      <h2>
       Interfacing of  ADC
      </h2>
      <ul>
       <li>
        Introduction to       ADC
       </li>
       <li>
        Interfacing       circuit of ADC
       </li>
       <li>
        Working   Interfacing       of Temperature Sensor (DS1621   LM35)
       </li>
      </ul>
      <h2>
       Interfacing of  External Memory
      </h2>
      <ul>
       <li>
        Introduction to       External Memory Interfacing
       </li>
       <li>
        Introduction to       I2C Protocol
       </li>
       <li>
        Using I2C       library to read/write External Memory
       </li>
      </ul>
      <h2>
       Introduction of  EMBEDDED C
      </h2>
      <ul>
       <li>
        Introduction to       Embedded C
       </li>
       <li>
        Different       between C   Embedded C
       </li>
       <li>
        Data Type of       Embedded C
       </li>
       <li>
        Operators of       Embedded C
       </li>
       <li>
        Statements         Loops of Embedded C
       </li>
      </ul>
      <h2>
       Interworking of  Assembly   Embedded C
      </h2>
      <ul>
       <li>
        Inline Function
       </li>
       <li>
        Inline Assembly       Routines
       </li>
      </ul>
      <h2>
       Programming    Interfacing using EMBEDDED C
      </h2>
      <ul>
       <li>
        Programming of       Timer   Counter
       </li>
       <li>
        Programming of       Serial Port
       </li>
       <li>
        Programming of       Interrupt
       </li>
       <li>
        LCD Interfacing
       </li>
       <li>
        Motor       Interfacing
       </li>
       <li>
        Key board Matrix       Interfacing
       </li>
      </ul>
      <h2>
       Interfacing of  LCD
      </h2>
      <ul>
       <li>
        Introduction to       16 x 2 LCD
       </li>
       <li>
        Commands of 16 x       2 LCD
       </li>
       <li>
        Interfacing       Circuit Description of 16 x 2 LCD
       </li>
       <li>
        Programming of       16 x 2 LCD
       </li>
      </ul>
      <h2>
       Interfacing of  Switches   Keyboard Matrix
      </h2>
      <ul>
       <li>
        Introduction to       Switches   Keyboard Matrix
       </li>
       <li>
        Interfacing       Circuit of Switches   Keyboard Matrix
       </li>
       <li>
        Programming of       Keyboard Matrix   Switches
       </li>
       <li>
        Controlling of       LED's by using Switches
       </li>
       <li>
        Key board Matrix         LCD Interfacing Program
       </li>
      </ul>
      <h2>
       Interfacing of  Motors
      </h2>
      <ul>
       <li>
        Introduction to       Motors
       </li>
       <li>
        Types of Motors       used in Embedded System
       </li>
       <li>
        Programming         Controlling of Motors in Embedded System
       </li>
      </ul>
      <h2>
       Timers    Counter Programming
      </h2>
      <ul>
       <li>
        Introduction to       Timer   Counter
       </li>
       <li>
        Difference       between Timer and Counter
       </li>
       <li>
        Description of       SFR associated with Timer   Counter
       </li>
       <li>
        Programming of       Timer   Counter
       </li>
      </ul>
      <h2>
       Serial  Communication Programming
      </h2>
      <ul>
       <li>
        Introduction to       Serial Communication
       </li>
       <li>
        Types of Serial       Communication
       </li>
       <li>
        Description of       SFR associated with Serial Communication
       </li>
       <li>
        Introduction         Interfacing of UART
       </li>
       <li>
        Programming of       UART
       </li>
      </ul>
      <h2>
       Interrupt  driven Programming
      </h2>
      <ul>
       <li>
        Introduction to       Interrupts
       </li>
       <li>
        Types of       Interrupts
       </li>
       <li>
        Programming of       Software   Hardware Interrupts
       </li>
       <li>
        Difference       between RISC and CISC
       </li>
       <li>
        Memory       Classification
       </li>
      </ul>
      <h2>
       Computer  Languages
      </h2>
      <ul>
       <li>
        Low Level       Languages
       </li>
       <li>
        Middle Level       Language
       </li>
       <li>
        High Level       Language
       </li>
       <li>
        Advantage         Disadvantage of Low level   High level programming language of       Embedded System
       </li>
       <li>
        Interaction of       language with Compilers
       </li>
      </ul>
      <h2>
       Embedded  Development Tools
      </h2>
      <ul>
       <li>
        Assembler
       </li>
       <li>
        Interpreter
       </li>
       <li>
        Compiler
       </li>
       <li>
        Simulator
       </li>
       <li>
        Emulator
       </li>
       <li>
        Debugger
       </li>
      </ul>
      <h2>
       Designing with  Microcontrollers
      </h2>
      <ul>
       <li>
        Introduction to       8051 and Family
       </li>
       <li>
        Introduction to       Microchip and Family
       </li>
       <li>
        Block       Description of PIC 18F458
       </li>
       <li>
        PIN diagram       Description of PIC 18F458
       </li>
       <li>
        Introduction of       File Register (RAM)
       </li>
       <li>
        Introduction To       RAM Architecture
       </li>
       <li>
        Access Bank
       </li>
       <li>
        Special Features       of PIC18F458
       </li>
      </ul>
      <p>
       [/vc_column_text][/vc_accordion_tab][/vc_accordion][vc_empty_space  height="20px &lt;/h2&gt;
       <br/>
       <strong>
        Advanced Embedded System  PIC ( PIC18XXXX)
       </strong>
       <br/>
       <h2>
        Introduction to  Embedded Systems
       </h2>
       <ul>
        <li>
         History         need of Embedded System
        </li>
        <li>
         Basic components       of Embedded System
        </li>
        <li>
         Hardware       Classification of Embedded System
        </li>
        <li>
         Programming       Language Classification of Embedded System
        </li>
       </ul>
       <h2>
        Classification  of Microprocessor   Microcontroller
       </h2>
       <ul>
        <li>
         Difference       between Microprocessor   Microcontroller
        </li>
        <li>
         Classification       based on Architecture
        </li>
        <li>
         Classification       based on Instruction Set
        </li>
        <li>
         Type of       Microcontroller
        </li>
        <li>
         Memory       Classification
        </li>
       </ul>
       <h2>
        Brief  Introduction to Computer Architecture
       </h2>
       <ul>
        <li>
         Classification       of Von-Neumann and Harvard Architecture
        </li>
       </ul>
       <h2>
        Introduction to  PIC18 Compiler/ Simulator
       </h2>
       <ul>
        <li>
         MPLAB Compiler
        </li>
        <li>
         MPLAB C 18       Compiler
        </li>
        <li>
         Micro Pro C       Compiler
        </li>
        <li>
         PIC18 Simulator       IDE
        </li>
        <li>
         Proteus
        </li>
       </ul>
       <h2>
        Real world  interfacing  LED
       </h2>
       <ul>
        <li>
         Brief       introduction to P-N Junction Semiconductor Devices and LED
        </li>
        <li>
         Circuit       Description of Interfacing LED
        </li>
        <li>
         LED Programming       Patterns
        </li>
       </ul>
       <h2>
        Real world  interfacing  7 segment display
       </h2>
       <ul>
        <li>
         Theory of       7-Segment Displays
        </li>
        <li>
         Writing Decoding       Chart for 0-f character
        </li>
        <li>
         Writing one       digit UP/DOWN Counter Program
        </li>
        <li>
         Programming 2       Digit/3 Digit /4 Digit Counter
        </li>
        <li>
         Introduction To       TLC (Traffic Light Controller) Programming
        </li>
       </ul>
       <h2>
        Real world  interfacing  LCD
       </h2>
       <ul>
        <li>
         Block Diagram of       LCD
        </li>
        <li>
         Types of LCD
        </li>
        <li>
         Pin Structure of       16x2 LCD
        </li>
        <li>
         Hardware       Interfacing Circuit
        </li>
        <li>
         LCD Command set
        </li>
        <li>
         Writing program       to drive LCD
        </li>
       </ul>
       <h2>
        Timer/Counter  programming
       </h2>
       <ul>
        <li>
         Description of       SFR associated with Timer/Counter
        </li>
        <li>
         Configuring as a       Timer
        </li>
        <li>
         Configuring as       Counter
        </li>
        <li>
         Delay Count       Calculations
        </li>
       </ul>
       <h2>
        Interfacing of  switches   keyboard matrix
       </h2>
       <ul>
        <li>
         Introduction to       Switches   Keyboard Matrix
        </li>
        <li>
         Interfacing       Circuit of Switches   Keyboard Matrix
        </li>
        <li>
         Programming of       Keyboard Matrix   Switches
        </li>
        <li>
         Controlling of       LED's by using Switches
        </li>
        <li>
         Key board Matrix         LCD Interfacing Program
        </li>
       </ul>
       <h2>
        Real world  interfacing  MOTORS
       </h2>
       <ul>
        <li>
         Different kind       of Motors
        </li>
        <li>
         Interfacing of       DC Motors and Stepper Motor
        </li>
        <li>
         Motor Drivers       Interfacing
        </li>
       </ul>
       <h2>
        Using  Internal/External Memories
       </h2>
       <ul>
        <li>
         Introduction to       External Memory Interfacing using Intel Bus Timing
        </li>
        <li>
         SFR       configuration to read/write Internal Memory (EEPROM)
        </li>
        <li>
         Using library to       read/write Internal EEPROM
        </li>
       </ul>
       <h2>
        Introduction of  EMBEDDED C
       </h2>
       <ul>
        <li>
         Why C
        </li>
        <li>
         Benefits of C       over Assembly
        </li>
        <li>
         Constants,       Variables   Data Types
        </li>
        <li>
         Operators
        </li>
        <li>
         Control       Statement and Loops
        </li>
        <li>
         Introduction to       preprocessor directives
        </li>
        <li>
         Assembly within       C (Inline Assembly)
        </li>
       </ul>
       <h2>
        Serial  Communication programming
       </h2>
       <ul>
        <li>
         Introduction to       the Communication System
        </li>
        <li>
         Types of       communication System
        </li>
        <li>
         Introduction to       Serial Communication
        </li>
        <li>
         Description of       SFR associated with Serial Communication
        </li>
        <li>
         Data Framing and       UART Introduction
        </li>
        <li>
         Introduction         Interfacing of UART
        </li>
        <li>
         Programming of       UART
        </li>
       </ul>
       <h2>
        Interrupt  driven programming
       </h2>
       <ul>
        <li>
         SFR associated       with Interrupts
        </li>
        <li>
         Interrupt       Handling Methods
        </li>
        <li>
         Programming       Hardware Interrupts
        </li>
        <li>
         Programming       Timer Interrupts
        </li>
        <li>
         Programming       Serial Interrupts
        </li>
       </ul>
       <h2>
        Using and  configuring adc
       </h2>
       <ul>
        <li>
         Introduction to       ADC
        </li>
        <li>
         ADC       Initialization
        </li>
       </ul>
       <h2>
        Introduction of  sensors
       </h2>
       <ul>
        <li>
         Introduction of       Transducers
        </li>
        <li>
         Types of Sensors
        </li>
        <li>
         Sensor       Interfacing
        </li>
       </ul>
       <h2>
        Introduction to  signal Decoder IC
       </h2>
       <ul>
        <li>
         DTMF
        </li>
       </ul>
       <h2>
        Protocol  Interfacing
       </h2>
       <ul>
        <li>
         SPI Protocol
        </li>
        <li>
         I2C Protocol
        </li>
       </ul>
       <h2>
        Introduction to  CCP and ECCP programming
       </h2>
       <ul>
        <li>
         Standard CCP       Module
        </li>
        <li>
         Enhanced CCP       Module
        </li>
        <li>
         Compare mode       programming
        </li>
        <li>
         Capture mode       programming
        </li>
        <li>
         Motor       interfacing
        </li>
        <li>
         16x2 LCD Display
        </li>
       </ul>
       <p>
        [/vc_column_text][/vc_accordion_tab][/vc_accordion][vc_empty_space  height="20px &lt;/h2&gt;
        <br/>
        <strong>
         Embedded Systems with  Microcontroller (AVR)
        </strong>
        <br/>
        <h2>
         Embedded system
        </h2>
        <ul>
         <li>
          Brief idea of       Embedded Systems   Industrial applications
         </li>
         <li>
          Application/Area       wise need of Embedded
         </li>
         <li>
          Hardware       classification for Embedded
         </li>
        </ul>
        <h2>
         Brain of  Embedded Appliances
        </h2>
        <ul>
         <li>
          Brief idea of       Micro-controller/processor
         </li>
         <li>
          Why       Microcontroller?
         </li>
         <li>
          Architecture of       Microcontroller
         </li>
         <li>
          System       architecture  RISC, CISC, Harvard, Von-Neumann
         </li>
         <li>
          Architecture of       Microcontroller
         </li>
        </ul>
        <h2>
         Embedded C
        </h2>
        <ul>
         <li>
          Introduction       classes
         </li>
         <li>
          Conditional       statements or looping
         </li>
         <li>
          C  Array
         </li>
        </ul>
        <h2>
         Microcontroller  (AVR)
        </h2>
        <ul>
         <li>
          Features of       microcontroller
         </li>
         <li>
          Pin out of       microcontroller
         </li>
         <li>
          GP I/O Port       specification
         </li>
         <li>
          Description       about all Ports
         </li>
         <li>
          Description       about IDE for programming
         </li>
         <li>
          Proteus       Simulation for microcontroller
         </li>
         <li>
          I/O programming       using Embedded C
         </li>
         <li>
          Led on/off       programming
         </li>
         <li>
          Delay generation       through function
         </li>
        </ul>
        <h2>
         Advanced  features of Microcontroller
        </h2>
        <ul>
         <li>
          TIMER
         </li>
         <li>
          ADC
         </li>
         <li>
          Serial       communication
         </li>
         <li>
          Advance       Communication Protocol
         </li>
         <li>
          Project as per       Module
         </li>
        </ul>
        <p>
         [/vc_column_text][/vc_accordion_tab][/vc_accordion][vc_empty_space  height="20px &lt;/h2&gt;
         <br/>
         <strong>
          Embedded Systems (ARM)
         </strong>
         <br/>
         <h2>
          Introduction to  Electronics
         </h2>
         <ul>
          <li>
           Resistors
          </li>
          <li>
           Capacitors
          </li>
          <li>
           Diodes
          </li>
          <li>
           Transistor
          </li>
          <li>
           Transformers
          </li>
          <li>
           Power supply
          </li>
         </ul>
         <h2>
          Introduction to  Embedded system
         </h2>
         <ul>
          <li>
           History         need of Embedded System
          </li>
          <li>
           Basic components       of Embedded System
          </li>
          <li>
           Hardware       Classification of Embedded System
          </li>
          <li>
           Programming       Language Classification of Embedded System
          </li>
          <li>
           Advantage         Disadvantage of Low level   High level programming language of       Embedded System
          </li>
         </ul>
         <h2>
          Microprocessor    microcontroller classification
         </h2>
         <ul>
          <li>
           Difference       between Microprocessor   Microcontroller
          </li>
          <li>
           Classification       based on architecture
          </li>
          <li>
           Classification       based on Instruction Set
          </li>
          <li>
           Type of       Microcontroller
          </li>
          <li>
           Introduction to       Microcontrollers
          </li>
          <li>
           Introduction to       Microprocessor
          </li>
          <li>
           Other       Programmable devices
          </li>
          <li>
           Difference b/w       various processing devices
          </li>
         </ul>
         <h2>
          Brief  introduction to Computer Architecture
         </h2>
         <ul>
          <li>
           Classification       based on architecture
          </li>
          <li>
           Classification       based on Instruction Set
          </li>
          <li>
           Memory Classification       and its organization
          </li>
         </ul>
         <h2>
          Computer  Languages
         </h2>
         <ul>
          <li>
           Low Level       Languages
          </li>
          <li>
           Middle Level       Language
          </li>
          <li>
           High Level       Language
          </li>
          <li>
           Interaction of       language with Compilers
          </li>
         </ul>
         <h2>
          Embedded  development tools
         </h2>
         <ul>
          <li>
           Assembler
          </li>
          <li>
           Interpreter
          </li>
          <li>
           Compiler
          </li>
          <li>
           Simulator
          </li>
          <li>
           Difference       between C and Embedded C
          </li>
          <li>
           Compiler       handling
          </li>
          <li>
           Creating and       modifying projects in Compiler Conventional programs
          </li>
          <li>
           Basic Embedded       programs structure
          </li>
          <li>
           Getting your       programs into a compiler, writing your programs
          </li>
         </ul>
         <h2>
          ARM  Architecture
         </h2>
         <ul>
          <li>
           Introduction to       ARM Architecture
          </li>
          <li>
           Block Diagram
          </li>
          <li>
           Functional       Diagram
          </li>
          <li>
           AMBA bus       architecture
          </li>
          <li>
           ARM versions
          </li>
          <li>
           The endian issue
          </li>
         </ul>
         <h2>
          Register and  memory of ARM7TDMI
         </h2>
         <ul>
          <li>
           ARM Register Set
          </li>
          <li>
           Modes in ARM
          </li>
          <li>
           Exception entry       and return from different modes
          </li>
          <li>
           32 bit CPU registers
          </li>
          <li>
           CPSR and SPSR       register
          </li>
          <li>
           States in arm
          </li>
          <li>
           ARM naming
          </li>
         </ul>
         <h2>
          ARM Instruction  Set's
         </h2>
         <ul>
          <li>
           Introduction to       32 bit ARM instruction set
          </li>
          <li>
           Introduction to       16 bit THUMB instruction set
          </li>
          <li>
           Introduction to       8- bit Jazelle instruction set
          </li>
          <li>
           Data processing       instruction
          </li>
          <li>
           Barrel shifter       instruction
          </li>
          <li>
           Load and store       instruction
          </li>
          <li>
           Arithmetic       Instruction
          </li>
          <li>
           Logical       Instruction
          </li>
          <li>
           Branching       Instruction
          </li>
         </ul>
         <h2>
          Introduction to  LPC21xx series Microcontroller
         </h2>
         <ul>
          <li>
           Introduction of       lpc2000family
          </li>
          <li>
           Block diagram       description of lpc2148
          </li>
          <li>
           Special feature       of lpc2148
          </li>
          <li>
           Pin description       of lpc2148
          </li>
         </ul>
         <h2>
          Registers    memory of lpc2148
         </h2>
         <ul>
          <li>
           Description of       RAM
          </li>
          <li>
           Description of       CPU Registers
          </li>
          <li>
           Function of SFR
          </li>
         </ul>
         <h2>
          PIN Control  Block
         </h2>
         <ul>
          <li>
           Pin       Configuration
          </li>
          <li>
           Pin Connect       Block
          </li>
          <li>
           General Purpose       I/P
          </li>
         </ul>
         <h2>
          System control  block in lpc2148
         </h2>
         <ul>
          <li>
           Power control       programming
          </li>
          <li>
           Pll programming
          </li>
          <li>
           Vpb programming
          </li>
         </ul>
         <h2>
          Interfacing of  switches   keyboard matrix
         </h2>
         <ul>
          <li>
           Introduction to       Switches   Keyboard Matrix
          </li>
          <li>
           Interfacing       Circuit of Switches   Keyboard Matrix
          </li>
          <li>
           Programming of       Keyboard Matrix   Switches
          </li>
          <li>
           Controlling of       LED's by using Switches
          </li>
          <li>
           Key board Matrix         LCD Interfacing Program
          </li>
         </ul>
         <h2>
          Real world  interfacing  MOTORS
         </h2>
         <ul>
          <li>
           Introduction to       Motors
          </li>
          <li>
           Introduction to       Motors
          </li>
          <li>
           Types of Motors       used in Embedded System
          </li>
          <li>
           AC motor
          </li>
          <li>
           DC motor
          </li>
          <li>
           Stepper motor
          </li>
          <li>
           Servo motor
          </li>
          <li>
           DC geared motor
          </li>
          <li>
           Programming         Controlling of Motors in Embedded System
          </li>
          <li>
           Different kind       of Motors
          </li>
          <li>
           Interfacing of       DC Motors and Stepper Motor
          </li>
          <li>
           Motor Drivers       Interfacing
          </li>
         </ul>
         <h2>
          Motor  Controlling circuits
         </h2>
         <ul>
          <li>
           Motor       controlling using driver ICs IC's
          </li>
          <li>
           LM358(dual op-       amp)
          </li>
          <li>
           LM35(Temperature       sensor)
          </li>
          <li>
           L293D(dual       H-bridge IC
          </li>
          <li>
           7805(Voltage       regulator)
          </li>
          <li>
           Lm317 IC
          </li>
         </ul>
         <h2>
          Types of  sensors
         </h2>
         <ul>
          <li>
           Introduction to       Sensing Devices
          </li>
          <li>
           IR sensor
          </li>
          <li>
           Light searching       sensor
          </li>
          <li>
           Temperature       sensor
          </li>
          <li>
           Touch sensor
          </li>
          <li>
           Motion sensor
          </li>
         </ul>
         <h2>
          Introduction to  signal Decoder IC
         </h2>
         <ul>
          <li>
           DTMF
          </li>
         </ul>
         <h2>
          Timers    counter programming
         </h2>
         <ul>
          <li>
           Introduction to       Timer   Counter
          </li>
          <li>
           Difference       between Timer and Counter
          </li>
          <li>
           Description of       SFR associated with Timer   Counter
          </li>
          <li>
           Programming of       Timer   Counter
          </li>
          <li>
           Timer 0 and       Timer 1 Features
          </li>
          <li>
           Pin Description
          </li>
          <li>
           Basics of Timer       Handling
          </li>
          <li>
           Capture and       match modules in lpc2148
          </li>
         </ul>
         <h2>
          Other  communication protocols
         </h2>
         <ul>
          <li>
           Features
          </li>
          <li>
           Applications
          </li>
          <li>
           Pin Description
          </li>
          <li>
           Architecture and       Register Description
          </li>
         </ul>
         <p>
          [/vc_column_text][/vc_accordion_tab][/vc_accordion][vc_empty_space  height="20px &lt;/h2&gt;
          <br/>
          <strong>
           Linux Internals    Device Drivers
          </strong>
          <br/>
          <h2>
           Getting Started
          </h2>
          <ul>
           <li>
            Introduction to       Unix and Linux
           </li>
           <li>
            Programming       Linux
           </li>
           <li>
            Linux Compiler
           </li>
           <li>
            History of Linux
           </li>
           <li>
            Types of Linux       platform
           </li>
           <li>
            Real time system
           </li>
          </ul>
          <h2>
           Linux basics  and commands
          </h2>
          <ul>
           <li>
            File Handling
           </li>
           <li>
            Text Processing
           </li>
           <li>
            System       Administration
           </li>
           <li>
            Process       Management
           </li>
           <li>
            Archival
           </li>
           <li>
            Network
           </li>
           <li>
            File Systems
           </li>
           <li>
            Advanced       Commands
           </li>
          </ul>
          <h2>
           Working with  file
          </h2>
          <ul>
           <li>
            Unix file       Structure
           </li>
           <li>
            File and       directory maintenance
           </li>
           <li>
            Changing the       attributes of a file systems
           </li>
           <li>
            File handling       concepts
           </li>
           <li>
            Normal level       file handling
           </li>
           <li>
            Low level File       handling
           </li>
          </ul>
          <h2>
           Sockets
          </h2>
          <ul>
           <li>
            What is socket
           </li>
           <li>
            Socket       connection
           </li>
           <li>
            Socket       Attributes
           </li>
           <li>
            Creating a       Socket
           </li>
           <li>
            Socket Addresses
           </li>
           <li>
            Naming a Socket
           </li>
           <li>
            Creating a       Socket Queue
           </li>
           <li>
            Accepting       Connections
           </li>
           <li>
            Requesting       Connections
           </li>
           <li>
            Closing a Socket
           </li>
           <li>
            Socket       Communications
           </li>
          </ul>
          <h2>
           Device Driver
          </h2>
          <ul>
           <li>
            An Introduction       to Device Drivers
           </li>
           <li>
            The Role of the       Device Driver
           </li>
           <li>
            Splitting the       Kernel
           </li>
           <li>
            Classes of       Devices and Modules
           </li>
           <li>
            Security Issues
           </li>
           <li>
            Version       Numbering
           </li>
           <li>
            Overview of the       Book
           </li>
           <li>
            Building and       Running Modules
           </li>
           <li>
            Setting Up Your       Test System
           </li>
           <li>
            The Hello World       Module
           </li>
           <li>
            Kernel Modules       Versus Applications
           </li>
           <li>
            Compiling and       Loading
           </li>
           <li>
            The Kernel       Symbol Table
           </li>
           <li>
            Preliminaries
           </li>
           <li>
            Initialization       and Shutdown
           </li>
           <li>
            Module       Parameters
           </li>
           <li>
            Doing It in User       Space
           </li>
           <li>
            Quick Reference
           </li>
           <li>
            3. Char Drivers
           </li>
           <li>
            The Design of       scull
           </li>
           <li>
            Major and Minor       Numbers
           </li>
           <li>
            Some Important       Data Structures
           </li>
           <li>
            Char Device       Registration
           </li>
           <li>
            open and release
           </li>
           <li>
            scull's Memory       Usage
           </li>
           <li>
            read and write
           </li>
           <li>
            Playing with the       New Devices
           </li>
          </ul>
          <h2>
           Brief  introduction to Computer Architecture
          </h2>
          <ul>
           <li>
            Classification       based on architecture
           </li>
           <li>
            Classification       based on Instruction Set
           </li>
           <li>
            Memory       Classification and its organization
           </li>
          </ul>
          <h2>
           Computer  Languages
          </h2>
          <ul>
           <li>
            Low Level       Languages
           </li>
           <li>
            Middle Level Language
           </li>
           <li>
            High Level       Language
           </li>
           <li>
            Interaction of       language with Compilers
           </li>
          </ul>
          <h2>
           Embedded  development tools
          </h2>
          <ul>
           <li>
            Assembler
           </li>
           <li>
            Interpreter
           </li>
           <li>
            Compiler
           </li>
           <li>
            Simulator
           </li>
           <li>
            Emulator
           </li>
           <li>
            Debugger
           </li>
          </ul>
          <h2>
           Embedded C  programming
          </h2>
          <ul>
           <li>
            C programming       basics
           </li>
           <li>
            Operators
           </li>
           <li>
            Control       Statement and Loops
           </li>
           <li>
            Introduction to       preprocessor directives
           </li>
          </ul>
          <h2>
           GPIO register  and peripheral register
          </h2>
          <ul>
           <li>
            Gpio register       with peripheral speed
           </li>
           <li>
            Gpio register       with CPU speed
           </li>
           <li>
            Pinselect       registers
           </li>
          </ul>
          <h2>
           Interfacing of  LED Matrix
          </h2>
          <ul>
           <li>
            Introduction of       LED's
           </li>
           <li>
            Brief       introduction to P-N Junction Semiconductor Devices and LED
           </li>
           <li>
            Circuit       Description of Interfacing LED
           </li>
           <li>
            Programming of       LED's Interfacing
           </li>
           <li>
            LED Programming       Patterns
           </li>
           <li>
            Introduction to       common cathode and common anode type Led matrix
           </li>
           <li>
            Interfacing hardware       of Led matrix
           </li>
           <li>
            Programming led       matrix
           </li>
           <li>
            Introduction to       multiplexed led matrix using shifter ic
           </li>
          </ul>
          <h2>
           Real World  Interfacing  Segment Display
          </h2>
          <ul>
           <li>
            Theory of       7-Segment Displays
           </li>
           <li>
            Types of 7       Segment Display
           </li>
           <li>
            Writing Decoding       Chart for 0-f character
           </li>
           <li>
            Writing one       digit UP/DOWN Counter Program
           </li>
           <li>
            Programming 2       Digit/3 Digit /4 Digit Counter
           </li>
           <li>
            TLC (Traffic       Light Controller) Programming
           </li>
           <li>
            Introduction to       Multiplexed 7 segment displays
           </li>
           <li>
            Interfacing       Multiplexed 7 segment displays
           </li>
           <li>
            Theory of       14-Segment Displays
           </li>
           <li>
            Writing Decoding       Chart for 14 segment character
           </li>
           <li>
            Theory of       16-Segment Displays
           </li>
           <li>
            Writing Decoding       Chart for 16 segment character
           </li>
          </ul>
          <h2>
           LCD Interfacing
          </h2>
          <ul>
           <li>
            Block Diagram of       LCD
           </li>
           <li>
            Types of LCD
           </li>
           <li>
            Introduction to       16x1,16 x2 and 16x 4 LCD
           </li>
           <li>
            Pin Structure of       16x1,16 x2 and 16x 4 LCD
           </li>
           <li>
            Hardware       Interfacing Circuit
           </li>
           <li>
            LCD Command set
           </li>
           <li>
            Commands of       16x1,16 x2 and 16x 4 LCD
           </li>
           <li>
            Programming of       16x1,16 x2 and 16x 4 LCD
           </li>
           <li>
            To move data on       LCD in 8-bit
           </li>
           <li>
            To move data on       LCD in 4-bit
           </li>
           <li>
            To display data       on both rows in 4 and 8-bit Mode
           </li>
           <li>
            Scrolling       message display on LCD in 4 and 8 bits Mode.
           </li>
           <li>
            Introduction to       graphical LCD
           </li>
          </ul>
          <h2>
           Serial  communication programming
          </h2>
          <ul>
           <li>
            Introduction to       the Communication System
           </li>
           <li>
            Types of       communication System
           </li>
           <li>
            Introduction to       Serial Communication
           </li>
           <li>
            Description of       SFR associated with Serial Communication
           </li>
           <li>
            Data Framing and       UART Introduction
           </li>
           <li>
            Introduction         Interfacing of UART
           </li>
           <li>
            Interfacing with       PC using UART/RS232
           </li>
          </ul>
          <h2>
           Interrupt  driven programming
          </h2>
          <ul>
           <li>
            Introduction to       Interrupts
           </li>
           <li>
            Difference       between polling and interrupt method
           </li>
           <li>
            Types of       Interrupts
           </li>
           <li>
            Interrupt       service routine (ISR)
           </li>
           <li>
            Vector Interrupt       Control
           </li>
           <li>
            Programming of       Software   Hardware Interrupts
           </li>
           <li>
            Interrupt       Priority
           </li>
           <li>
            Timer Interrupts       Programming
           </li>
           <li>
            External       Hardware Interrupts Programming
           </li>
           <li>
            SFR associated       with Interrupts
           </li>
           <li>
            Programming       Serial Interrupts
           </li>
           <li>
            RTC interrupt
           </li>
          </ul>
          <h2>
           Interfacing of  ADC
          </h2>
          <ul>
           <li>
            Introduction to       ADC
           </li>
           <li>
            Registers for       ADC
           </li>
           <li>
            Interfacing       circuit of ADC
           </li>
           <li>
            ADC       Initialization
           </li>
           <li>
            To display       digital data on LED
           </li>
           <li>
            To display       digital data on LCD
           </li>
          </ul>
          <h2>
           Real time clock
          </h2>
          <ul>
           <li>
            Feathers
           </li>
           <li>
            Resister       Description
           </li>
           <li>
            RTC Interrupts
           </li>
          </ul>
          <h2>
           Pulse width  modulation
          </h2>
          <ul>
           <li>
            PWM Generator
           </li>
           <li>
            Register       Description
           </li>
           <li>
            Application
           </li>
          </ul>
          <h2>
           Processes and  Signals
          </h2>
          <ul>
           <li>
            What is process?
           </li>
           <li>
            Process       Structure
           </li>
           <li>
            The Process       Table
           </li>
           <li>
            Viewing       Processes
           </li>
           <li>
            System Processes
           </li>
           <li>
            Process       Scheduling
           </li>
          </ul>
          <h2>
           Getting Started
          </h2>
          <ul>
           <li>
            Kill a process
           </li>
           <li>
            Fork
           </li>
           <li>
            Starting New       Processes
           </li>
           <li>
            Waiting for a       Process
           </li>
           <li>
            Zombie Processes
           </li>
           <li>
            Input and Output       Redirection
           </li>
           <li>
            Execve ,exec       ,execv, execlp ,execl ,execve
           </li>
           <li>
            Process commands
           </li>
           <li>
            Signal handling
           </li>
          </ul>
          <h2>
           Threads
          </h2>
          <ul>
           <li>
            What is thread
           </li>
           <li>
            Thread       programming
           </li>
           <li>
            Wait queues
           </li>
           <li>
            Spin lock
           </li>
           <li>
            Synchronization
           </li>
          </ul>
          <h2>
           Pipes
          </h2>
          <ul>
           <li>
            what is pipe
           </li>
           <li>
            Process Pipes
           </li>
           <li>
            The pipe call
           </li>
           <li>
            Parent and child       processes
           </li>
           <li>
            Named pipes
           </li>
          </ul>
          <h2>
           Semaphores,  message queues and shared memory
          </h2>
          <ul>
           <li>
            Semaphores
           </li>
           <li>
            Semaphore       Definition
           </li>
           <li>
            Linux Semaphore       Facilities
           </li>
           <li>
            Using Semaphores
           </li>
          </ul>
          <h2>
           Shared Memory
          </h2>
          <ul>
           <li>
            shmget
           </li>
           <li>
            shmat
           </li>
           <li>
            shmdt
           </li>
           <li>
            shmctl
           </li>
          </ul>
          <h2>
           Message Queues
          </h2>
          <ul>
           <li>
            msgget
           </li>
           <li>
            msgsnd
           </li>
           <li>
            msgrcv
           </li>
           <li>
            msgctl
           </li>
          </ul>
         </p>
        </p>
       </p>
      </p>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="embeddedsystemtraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="64 + 1 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="65">
       <input name="url" type="hidden" value="/embeddedsystemtraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>