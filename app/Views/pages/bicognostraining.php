<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     COGNOS 10 BI
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     COGNOS 10 BI
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      A NEW REVOLUTION TO DATA ANALYSIS TECHNOLOGY
     </h4>
     <p>
      BI CONGOS 10 helps users in extracting corporate data and manage it. It includes features like Professional report authoring tool, Query studio which allows instant data preview with adhoc report authoring and Analysis studio which helps in answering business queries. Besides this, other features like frame work manager powerplay studio, Congos express reporter make the product much advance in data analysis technology. The package comes in two varieties: with power play and without power play. Without power play is more popular among the companies. DUCAT offers you a course which is a compact package of various aspects of BI CONGOS 10. Expertise training is being divided in different modules according to the area of specialization.
     </p>
     <p>
      BI CONGOS 10 REPORT STUDIO The course is designed by experienced trainers keeping in mind the need of report authors. The course will help them to learn report building techniques and how to enhance, manage and distribute professional report. Before entering to course one must have a strong command over Windows operating system, web browsers and XML.
     </p>
     <p>
      BI CONGOS 10 ADMINISTRATION The course is short duration course led by Trainers who will make new administrators explore the fundamentals of administering a server and content in BI CONGOS 10. Course is an add on to system and content administrators. Building AD-HOC reports and web server architecture knowledge will help in getting edge in course.
     </p>
     <p>
      BI CONGOS 10 FRAMEWORK MANAGER It is a 5 day course that provides knowledge of metadata modeling concepts for predictable reporting. Trainers will train by giving training on how to apply security to model and let participants create analysis objects.
     </p>
     <div class="contentAcc">
      <h2>
       COGNOS DATA WAREHOUSING
      </h2>
      <ul>
       <li>
        Introduction to DATA WAREHOUSING
       </li>
       <li>
        DATA WAREHOUSING CONCEPTS
       </li>
       <li>
        DIFFERENCES BETWEEN TRANSECTION DATA AND WAREHOUSING DATA
       </li>
       <li>
        DATA WAREHOUSING ARCHITECTURE
       </li>
       <li>
        source data,staging data and target data
       </li>
       <li>
        olap types
       </li>
       <li>
        MOLAP,ROLAP,HOLAP
       </li>
       <li>
        OPERATIONAL AND DATA STORE(ODS)
       </li>
       <li>
        METADATA AND DATA MINING
       </li>
       <li>
        DIFFERENCES BETWEEN DATA MART AND DATA WAREHOUSE
       </li>
       <li>
        SCHEMAS USED IN DATA WAREHOUSING
       </li>
       <li>
        SLOWLY CHANGING DIMENSIONS
       </li>
       <li>
        REAL TIME DATA WAREHOUSE
       </li>
      </ul>
      <h2>
       DATABASE DESIGN AND DATA MODELLING
      </h2>
      <ul>
       <li>
        ENTITY-RELATIONAL MODEL(E-R MODEL) FOR TRANSACTIONAL DATABASE
       </li>
       <li>
        DEFINING ENTITIES AND THEIR RELATIONSHIP
       </li>
       <li>
        DISTRIBUTION OF ATTRIBUTES UNDER ENTITES
       </li>
       <li>
        NORMALIZATION OF ENTITES
       </li>
       <li>
        CONVERSION OF LOGICAL MODAL INTO PHYSICAL MODEL
       </li>
       <li>
        MODELING LOGICAL E-R MODEL USING ERWIN TOOL
       </li>
       <li>
        DESCRIPTION ABOUT STRUCTURED QUERY LANGUAGE
       </li>
       <li>
        CASE STUDIES
       </li>
      </ul>
      <h2>
       DIMENSIONAL MODEL FOR DATAWAREHOUSING
      </h2>
      <ul>
       <li>
        DEFINE DIMEENSIONS AND MEASURES
       </li>
       <li>
        DEFINING LEVELS AN DHIERARCHIES
       </li>
       <li>
        GRANULARITY OF DATA
       </li>
       <li>
        DESCRIPTION ABOUT SNOW FLAK SCHEMA
       </li>
       <li>
        DIMENSION ABOUT SNOW FLAK SCHEMA
       </li>
       <li>
        DIMENSION TABLE EAND FACT TABLE
       </li>
       <li>
        FACTLESS FACT TABLE
       </li>
       <li>
        ADDITIVE MEASURES, NON-ADDITIVE AND SEMI-ADDITIVE
       </li>
       <li>
        NORMALIZATION AND DENOMALIZATION
       </li>
       <li>
        MANY tO MANY DIMENSIONS
       </li>
       <li>
        CASE STUDIES
       </li>
      </ul>
      <h2>
       OLAP USING COGNOS 10 BI
      </h2>
      <ul>
       <li>
        INTRODUCTION OF DATA WAREHOUSING PROCESS
       </li>
       <li>
        DETAILED EXPLANATION ABOUT OLTP SYSTEM
       </li>
       <li>
        DETAILED EXPLANATION ABOUT OLAP SYSTEM
       </li>
       <li>
        EXPLANATION OF DWH LIFE CYCLE
       </li>
       <li>
        DETAILED EXPLANATION OF OLAP LIFE TYPES
       </li>
       <li>
        DETAILED EXPLANATION OF COGNOS 10 BI ARCHITECTURE
       </li>
       <li>
        EXPLANATION OF OLAP TYPES
       </li>
       <li>
        DETAILED EXPLANATION OF DIMENSIONS MODELING
       </li>
       <li>
        DETAILED EXPLANATION OF STAR AND SNOW FLAK SCHEMA
       </li>
      </ul>
      <h2>
       DEVELOPER ROLES INCLUDING
      </h2>
      <ul>
       <li>
        RESPONSIBLE FOR BUIDING DATABASE AND IMPORTING DATABASE
       </li>
       <li>
        MODELS OR APPLICATIONS FOR END -USER INCLUDE DATA MART DEVELOPEMENT
       </li>
       <li>
        METADATA AND OLAP MODLES
       </li>
       <li>
        ADMINSTRATION HANDLING
       </li>
      </ul>
      <h2>
       CREATING PACKAGE AND MODELS BY USING COGNOS 10BI FRAME WORK MANAGER
      </h2>
      <ul>
       <li>
        COGNOS 10BI FRAMEWORK MANAGER: DESIGNING METADATA MODELS
       </li>
       <li>
        DESCRIPTION ABOUT BUSINESS IDEAS
       </li>
       <li>
        CREATING RELATIONSHIP BETWEEN FACTS AND DIMENSIONS
       </li>
       <li>
        WORKING ON GOVERNORS AND DETERMINANTS
       </li>
       <li>
        CREATION OF PACKAGE ACCORDING TO THE BUSINESS REQUIREMENTS
       </li>
       <li>
        PUBLISHING PACKAGE
       </li>
      </ul>
      <h2>
       CREATING OF POWER CUBES USING IBM COGNOS TRANSFORMER
      </h2>
      <ul>
       <li>
        CREATIONS HIERARCHIES AND THE LEVELS
       </li>
       <li>
        CREATION OF CUSTOM VIEWS
       </li>
       <li>
        WORKING ON TIME HIERARCHIES
       </li>
       <li>
        CREATING AND PUBLISHING THE POWER CUBES
       </li>
       <li>
        CREATION OF REPORTS IN ANALYSIS FOR CUBE DATA
       </li>
      </ul>
      <h2>
       GENERATING REPORTS WITH REPORTS STUDIO
      </h2>
      <ul>
       <li>
        AUTHORING PROFESSIONAL REPORTS
       </li>
       <li>
        AUTHORING REPORTS WITH MULTI DIMENSIONAL DATA
       </li>
       <li>
        WORKING WITH MASTER DETAILED , DRILL THROUGH AND DATE REPORTS
       </li>
       <li>
        WORKING WITH YTD, MTD, QTD ETC
       </li>
       <li>
        GENERATING REPORTS WITH BURST OPTIONS
       </li>
       <li>
        SCHEDULING REPORTS USING BI SERVER
       </li>
      </ul>
      <h2>
       GENERATING REPORTS WITH ANALYSIS STUDIO
      </h2>
      <ul>
       <li>
        GENRATING CHARTS AND MULTI-DIMENSIONAL REPORTS USING COGNOS BI SUITE
       </li>
       <li>
        DRILL DOWN AND DRILL UP PROCESS
       </li>
       <li>
        PUBLISHING REPORT ANALYSIS AND REVIEW
       </li>
      </ul>
      <h2>
       GENERATING REPORTS WITH EVENT STUDIO
      </h2>
      <ul>
       <li>
        CREATING ,SCHEDULING AND MANAGING REPORTS
       </li>
       <li>
        CREATING OF AGENTS AND SPECIFYING EVENT CONDITION
       </li>
       <li>
        TROUBLESHOOTING ON EVENT STUDIO
       </li>
      </ul>
      <h2>
       GENERATING OF DASHBOARD BOARD USING BUSINESS INSIGHT
      </h2>
      <ul>
       <li>
        CREATION OF END USER REPORTS USING BUSINESS INSIGHT
       </li>
       <li>
        MERGING OF REPORTS ACCORDING TO BUSINESS REQUIREMENTS
       </li>
       <li>
        COGNOS BI ADMINISTRATION AND USER CREATION WITH PRIVILEGES
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="bicognostraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="66 + 92 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="158">
       <input name="url" type="hidden" value="/bicognostraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>