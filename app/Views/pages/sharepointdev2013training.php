<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     SHAREPOINT DEVELOPMENT 2013 TRAINING
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     SHAREPOINT DEVELOPMENT 2013 TRAINING
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h5>
      Knowledge is power!
     </h5>
     <p>
      SharePoint Server is one of the powerful creations of Microsoft that enhances real time communication abilities of theorganization giving an edge over others. DUCAT offers training on MICROSOFT SHAREPOINT 2013 Admin to corporates and individuals to help them leverage the essence of this Microsoft Technology. The course begins with the fundamentals and goes on to cover advanced concepts such as content management, business process implementation, collaboration and document management. The course is well structured to suit the needs of those working in IT and non-IT as SharePoint is adapted by organization from all sectors. The participants of the course benefit via the experienced and knowledgeable faculties,learning by doing, hands on training, live projects, project guidance from IT experts and workshops and seminars detailing on the actual challenges faced in the job environments with regards to SharePoint implementation and usage. The MICROSOFT SHAREPOINT DEVELOPMENT training course at DUCAT trains the participants in multiple SharePoint skills, which will help them be productive in the varied tasks. Thus a course for DUCAT can open up lateral and vertical growth for the participants within the organization and outside too. In this module, participants will learn how to create custom site pages and data views. In addition this module will also dive into the aspects of customizing the SharePoint user interface for custom branding experiences as well as customizing the master pages.
     </p>
     <div class="contentAcc">
      <h2>
       UNDERSTANDING SHAREPOINT 2013
      </h2>
      <ul>
       <li>
        SharePoint Version History
       </li>
       <li>
        SharePoint 2013 Capabilities
       </li>
       <li>
        Significant Changes from       SharePoint 2010
       </li>
       <li>
        SharePoint 2013 Farm       Topologies &amp; Architecture
       </li>
       <li>
        Hardware &amp; Software       prerequisites for SharePoint 2013
       </li>
       <li>
        Course Introduction
       </li>
      </ul>
      <h2>
       SHAREPOINT 2013 INSTALLATION AND  CONFIGURATION
      </h2>
      <ul>
       <li>
        Install &amp; Configure a       Windows Server 2012
       </li>
       <li>
        Install Active Directory Domain       Services
       </li>
       <li>
        Install SQL Server 2012
       </li>
       <li>
        Install &amp; Configure       SharePoint Server 2013
       </li>
       <li>
        Understanding and creating       Web Applications
       </li>
       <li>
        Understanding and creating       SiteCollections
       </li>
      </ul>
      <h2>
       WORKING WITH LISTS AND LIBRARIES
      </h2>
      <ul>
       <li>
        Creating Lists
       </li>
       <li>
        Managing List Columns and       Views
       </li>
       <li>
        Using Content Types in Lists       and Libraries
       </li>
       <li>
        Using Site Columns
       </li>
       <li>
        Check In / Check out and       versioning settings for document libraries
       </li>
      </ul>
      <h2>
       WORKING WITH WIKIS, BLOGS AND SURVEYS
      </h2>
      <ul>
       <li>
        Creating and Configuring       Wikis
       </li>
       <li>
        Blogs
       </li>
       <li>
        Surveys
       </li>
       <li>
        Discussion Boards etc.
       </li>
      </ul>
      <h2>
       SHAREPOINT SECURITY AND PERMISSIONS
      </h2>
      <ul>
       <li>
        Authentication &amp;       Authorization
       </li>
       <li>
        Configuring permission       mechanism at Site Collections, Sub Sites, Lists and item level
       </li>
       <li>
        Custom Permission Levels
       </li>
       <li>
        Breaking Inheritance
       </li>
       <li>
        Securing Apps
       </li>
       <li>
        App Identity
       </li>
       <li>
        OAuth &amp; Server-to-Server       (S2S)
       </li>
      </ul>
      <h2>
       USING SHAREPOINT DESIGNER 2013
      </h2>
      <ul>
       <li>
        Creating Custom Site Pages
       </li>
       <li>
        Creating Data Views
       </li>
       <li>
        Filters on Data Views
       </li>
       <li>
        Master Pages in SharePoint       2013
       </li>
       <li>
        User Interface Design for       Apps
       </li>
      </ul>
      <h2>
       DEVELOPING SHAREPOINT SOLUTIONS
      </h2>
      <ul>
       <li>
        Farm Solutions
       </li>
       <li>
        Sandbox Solutions
       </li>
       <li>
        Solution Packages
       </li>
      </ul>
      <h2>
       SERVER-SIDE SHAREPOINT DEVELOPMENT
      </h2>
      <ul>
       <li>
        Server-Side API Overview
       </li>
       <li>
        Creating, Deploying, and       Debugging Visual Web Parts by Using Visual Studio 2012
       </li>
       <li>
        Retrieve, add, update, and       delete SharePoint list data
       </li>
       <li>
        Retrieve SharePoint list data       using CAML Query
       </li>
       <li>
        Working with List Data       Programmatically
       </li>
      </ul>
      <h2>
       CLIENT-SIDE SHAREPOINT DEVELOPMENT
      </h2>
      <ul>
       <li>
        Overview of CSOM
       </li>
       <li>
        Programming CSOM with C#
       </li>
       <li>
        Calling the SharePoint 2013       REST API
       </li>
       <li>
        Making REST calls with C# and       JavaScript
       </li>
      </ul>
      <h2>
       DEVELOPING EVENT RECEIVERS
      </h2>
      <ul>
       <li>
        Event Receivers Overview
       </li>
       <li>
        Creating List Item Event       Receivers
       </li>
      </ul>
      <h2>
       DEVELOPING SHAREPOINT APPS
      </h2>
      <ul>
       <li>
        Introduction to SharePoint       Apps
       </li>
       <li>
        Developing SharePoint Apps
       </li>
       <li>
        SharePoint-Hosted App
       </li>
       <li>
        Provider-Hosted App
       </li>
      </ul>
      <h2>
       REUSABLE TYPE DEFINITIONS AND TEMPLATES
      </h2>
      <ul>
       <li>
        Field Types &amp; Controls
       </li>
       <li>
        Site Columns &amp; Content       Types
       </li>
       <li>
        List Definitions &amp; Templates
       </li>
       <li>
        Site Definitions
       </li>
       <li>
        Web Templates
       </li>
      </ul>
      <h2>
       DEVELOPING WORKFLOWS
      </h2>
      <ul>
       <li>
        Workflow in SharePoint 2010
       </li>
       <li>
        Workflow in SharePoint 2013
       </li>
       <li>
        SharePoint 2013 Workflow       Improvements
       </li>
       <li>
        SharePoint 2013 Out-of-the-Box       Workflows
       </li>
       <li>
        Creating Custom Workflows
       </li>
       <li>
        Tooling: Visio 2013 &amp;       SharePoint Designer 2013
       </li>
       <li>
        Tooling: Visual Studio 2012
       </li>
      </ul>
      <h2>
       SHAREPOINT 2013 BUSINESS INTELLIGENCE
      </h2>
      <ul>
       <li>
        Using Excel Services
       </li>
       <li>
        Scorecards, KPIs, Reports
       </li>
       <li>
        PerformancePoint Server       Dashboard
       </li>
      </ul>
      <h2>
       BUSINESS CONNECTIVITY SERVICES
      </h2>
      <ul>
       <li>
        BCS Overview &amp;       Architecture
       </li>
       <li>
        External Content Types
       </li>
       <li>
        Using External Content Types
       </li>
      </ul>
      <h2>
       INFOPATH FORMS SERVICES
      </h2>
      <ul>
       <li>
        InfoPath Forms Services       Overview
       </li>
       <li>
        SharePoint Form Library
       </li>
       <li>
        InfoPath form templates
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="sharepointdev2013training.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="39 + 73 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="112">
       <input name="url" type="hidden" value="/sharepointdev2013training/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>