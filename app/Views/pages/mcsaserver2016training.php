<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Mcsa server 2016 Training
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Learn and Earn
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      Mcsa server 2016 Training
     </h4>
     <p>
      MCSA Server 2016 certification qualifies you for a position as a network or computer systems administrator or as a computer network specialist, and it is the first step on your path to becoming a Microsoft Certified Solutions Expert (MCSE).
     </p>
     <div class="contentAcc">
      <h2>
       EXAM CODE 740 Module 1: Installing, upgrading, and migrating servers and workloads
      </h2>
      <ul>
       <li>
        Introducing Windows Server 2016
       </li>
       <li>
        Preparing and installing Nano Server and Server Core
       </li>
       <li>
        Preparing for upgrades and migrations
       </li>
       <li>
        Migrating server roles and workloads
       </li>
       <li>
        Windows Server activation models
       </li>
      </ul>
      <h2>
       Lab : Installing and configuring Nano Server
      </h2>
      <ul>
       <li>
        Installing Nano Server
       </li>
       <li>
        ompleting post-installation tasks on Nano Server
       </li>
       <li>
        erforming remote management
       </li>
       <li>
        After completing this module, students will be able to:
       </li>
       <li>
        escribe the new features of Windows Server 2016.
       </li>
       <li>
        prepare for and install Nano Server and Server Core.
       </li>
       <li>
        Plan a server upgrade and migration strategy.
       </li>
       <li>
        Perform a migration of server roles and workloads within a  domain and across domains.
       </li>
       <li>
        Choose an activation model.
       </li>
      </ul>
      <h2>
       Module 2: Configuring local storage
      </h2>
      <h2>
       This module explains how to manage disks and volumes in Windows Server 2016
      </h2>
      <h2>
       Lessons
      </h2>
      <ul>
       <li>
        Managing disks in Windows Server
       </li>
       <li>
        Managing volumes in Windows Server
       </li>
      </ul>
      <h2>
       Lab : Configuring local storage
      </h2>
      <ul>
       <li>
        Creating and managing volumes
       </li>
       <li>
        Resizing volumes
       </li>
       <li>
        Managing virtual hard disks
       </li>
       <li>
        After completing this module, students will be able to:
       </li>
       <li>
        Manage disks in Windows Server.
       </li>
       <li>
        Manage volumes in Windows Server.
       </li>
      </ul>
      <h2>
       Module 3: Implementing enterprise storage solutions
      </h2>
      <h2>
       Lessons
      </h2>
      <ul>
       <li>
        Overview of DAS, NAS, and SANs
       </li>
       <li>
        Comparing Fibre Channel, iSCSI, and Fibre Channel over  Ethernet
       </li>
       <li>
        Understanding iSNS, DCB, and MPIO
       </li>
       <li>
        Configuring sharing in Windows Server 2016
       </li>
      </ul>
      <h2>
       Lab : Planning and configuring storage technologies and components
      </h2>
      <ul>
       <li>
        Planning storage requirements
       </li>
       <li>
        Configuring iSCSI storage
       </li>
       <li>
        Configuring and managing the share infrastructure
       </li>
       <li>
        After completing this module, students will be able to:
       </li>
       <li>
        Describe DAS, NAS, and SANs.
       </li>
       <li>
        Compare Fibre Channel iSCSI, and FCoE.
       </li>
       <li>
        Explain the use of iSNS, DCB, and MPIO.
       </li>
       <li>
        Configure sharing in Windows Server.
       </li>
      </ul>
      <h2>
       Module 4: Implementing Storage Spaces and Data Deduplication
      </h2>
      <h2>
       Lab : Implementing Storage Spaces
      </h2>
      <h2>
       Lab : Implementing Data Deduplication
      </h2>
      <ul>
       <li>
        Installing Data Deduplication
       </li>
       <li>
        Configuring Data Deduplication
       </li>
       <li>
        After completing this module, students will be able to:
       </li>
       <li>
        Describe and implement the Storage Spaces feature in the  context of enterprise storage needs.
       </li>
       <li>
        Manage and maintain Storage Spaces.
       </li>
       <li>
        Describe and implement Data Deduplication.
       </li>
      </ul>
      <h2>
       Module 5: Installing and configuring Hyper-V and virtual machines
      </h2>
      <h2>
       Lessons
      </h2>
      <ul>
       <li>
        Overview of Hyper-V
       </li>
       <li>
        Installing Hyper-V
       </li>
       <li>
        Configuring storage on Hyper-V host servers
       </li>
       <li>
        Configuring networking on Hyper-V host servers
       </li>
       <li>
        Configuring Hyper-V virtual machines
       </li>
       <li>
        Managing virtual machines
       </li>
      </ul>
      <h2>
       Lab : Installing and configuring Hyper-V
      </h2>
      <ul>
       <li>
        Overview of Hyper-V
       </li>
       <li>
        Installing Hyper-V
       </li>
       <li>
        Configuring storage on Hyper-V host servers
       </li>
       <li>
        Configuring networking on Hyper-V host servers
       </li>
       <li>
        Configuring Hyper-V virtual machines
       </li>
       <li>
        Managing virtual machinesVerify installattion of the Hyper-V server role
       </li>
       <li>
        Configuring Hyper-V networks
       </li>
       <li>
        Creating and configuring a virtual machines
       </li>
       <li>
        Enable nested virtualization for a virtual machine
       </li>
       <li>
        After completing this module, students will be able to:
       </li>
       <li>
        Describe Hyper-V and virtualization.
       </li>
       <li>
        Install Hyper-V.
       </li>
       <li>
        Configure storage on Hyper-V host servers.
       </li>
       <li>
        Configure networking on Hyper-V host servers.
       </li>
       <li>
        Configure Hyper-V virtual machines.
       </li>
       <li>
        Manage Hyper-V virtual machines.
       </li>
      </ul>
      <h2>
       Module 6: Deploying and managing Windows and Hyper-V containers
      </h2>
      <h2>
       Lessons
      </h2>
      <ul>
       <li>
        Overview of containers in Windows Server 2016
       </li>
       <li>
        Deploying Windows Server and Hyper-V containers
       </li>
       <li>
        Installing, configuring, and managing containers by using  Docker
       </li>
      </ul>
      <h2>
       Lab : Installing and configuring containers
      </h2>
      <ul>
       <li>
        Installing and configuring Windows Server containers by  using Windows PowerShell
       </li>
       <li>
        Installing and configuring Windows Server containers by  using Docker Installing
       </li>
       <li>
        After completing this module, students will be able to:
       </li>
       <li>
        Describe containers in Windows Server 2016.
       </li>
       <li>
        Explain how to deploy containers.
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>