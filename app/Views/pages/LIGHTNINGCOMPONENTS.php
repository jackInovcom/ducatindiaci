<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     PROGRAMMING LIGHTNING COMPONENTS TRAINING
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     PROGRAMMING LIGHTNING COMPONENTS
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      PROGRAMMING LIGHTNING COMPONENTS TRAINING
     </h4>
     <p>
      If your responsibilities include developing custom applications running in Lightning Experience and
Salesforce1 that support multiple devices and screen sizes from a single codebase, taking this class
will boost your skills to the next level.
In this five-day instructor-led course, you'll learn how to program Lightning Components with markup,
JavaScript, CSS, Apex, and the Salesforce Lightning Design System (SLDS), and make them available
to Salesforce end users.
     </p>
     <div class="contentAcc">
      <h2>
       OVERVIEW
      </h2>
      <h2>
       WHO SHOULD TAKE THIS COURSE?
      </h2>
      <h2>
       WHEN YOU COMPLETE THIS COURSE, YOU WILL BE ABLE TO
      </h2>
      <ul>
       <li>
        Efficiently create custom, reusable Lightning components and applications.
       </li>
       <li>
        Surface Lightning components and applications throughout the Salesforce ecosystem.
       </li>
       <li>
        Build a Salesforce1 mobile application that marshals data from your org.
       </li>
       <li>
        Define input forms with client-side data validation.
       </li>
       <li>
        Build apps that enable a user to create, read, and update data from a Salesforce org.
       </li>
       <li>
        Programmatically invoke features of Salesforce1.
       </li>
       <li>
        Make components available to other developers through AppExchange and unmanaged packages.
       </li>
       <li>
        Theme your application by using SLDS and Lightning Tokens
       </li>
      </ul>
      <h2>
       PREREQUISITES
      </h2>
      <h2>
       MODULES &amp; TOPICS
      </h2>
      <h2>
       Getting Started
      </h2>
      <ul>
       <li>
        Lightning Components to Develop Apps
       </li>
       <li>
        Defining a Lightning Component
       </li>
       <li>
        Defining and Manipulating Component Attributes
       </li>
       <li>
        Handling User Events
       </li>
       <li>
        Debugging and Troubleshooting Your App
       </li>
       <li>
        Using Helpers for Complex Client-Side Event Handling
       </li>
       <li>
        Documenting a Component
       </li>
       <li>
        Using OOTB Lightning Input Components
       </li>
       <li>
        Working with Apex
       </li>
       <li>
        Testing and Troubleshooting Apex
       </li>
      </ul>
      <h2>
       Surfacing Lightning Components
      </h2>
      <ul>
       <li>
        Surfacing Lightning Components
       </li>
       <li>
        Deep-Diving into Building Lightning Pages with Components and App Builder
       </li>
       <li>
        Building Components for Lightning Experience Record Pages
       </li>
       <li>
        Defining a Lightning Application
       </li>
       <li>
        Using Lightning in Visualforce Pages with Lightning Out
       </li>
       <li>
        Installing and Using Components from AppExchange
       </li>
      </ul>
      <h2>
       Implementing Navigation and Layouts
      </h2>
      <ul>
       <li>
        How to navigate and manage layouts
       </li>
      </ul>
      <h2>
       Building Advanced Components
      </h2>
      <ul>
       <li>
        Accessing the Component Body
       </li>
       <li>
        Dynamically Instantiating and Destroying Components
       </li>
       <li>
        Defining and Handling Nested Sub-tags
       </li>
       <li>
        Using Renderers
       </li>
       <li>
        Using Inheritance
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>