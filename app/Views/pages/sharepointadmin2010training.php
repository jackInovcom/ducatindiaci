<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     SHAREPOINT ADMIN 2010 TRAINING
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     SHAREPOINT ADMIN 2010 TRAINING
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h5>
      MICROSOFT SHAREPOINT Training Programm
     </h5>
     <p>
      SharePoint Server is one of the powerful creations of Microsoft that enhances real time communication abilities of theorganization giving an edge over others. DUCAT offers training on MICROSOFT SHAREPOINT 2010 Admin to corporates and individuals to help them leverage the essence of this Microsoft Technology. The course begins with the fundamentals and goes on to cover advanced concepts such as content management, business process implementation, collaboration and document management. The course is well structured to suit the needs of those working in IT and non-IT as SharePoint is adapted by organization from all sectors. The participants of the course benefit via the experienced and knowledgeable faculties,learning by doing, hands on training, live projects, project guidance from IT experts and workshops and seminars detailing on the actual challenges faced in the job environments with regards to SharePoint implementation and usage. The MICROSOFT SHAREPOINT Admin 2010 training course at DUCAT trains the participants in multiple SharePoint skills, which will help them be productive in the varied tasks. Thus a course for DUCAT can open up lateral and vertical growth for the participants within the organization and outside too.
     </p>
     <div class="contentAcc">
      <h2>
       INTRODUCTION SHAREPOINT ADMIN 2010
      </h2>
      <ul>
       <li>
        Microsoft SharePoint 2010 Introduction
       </li>
       <li>
        SharePoint Version History
       </li>
       <li>
        SharePoint 2010 Capabilities
       </li>
      </ul>
      <h2>
       ARCHITECTURE
      </h2>
      <ul>
       <li>
        Platform Architecture
       </li>
       <li>
        SharePoint 2010 Architecture
       </li>
       <li>
        Logical Architecture
       </li>
       <li>
        SharePoint Farm Topologies
       </li>
      </ul>
      <h2>
       INSTALLATION
      </h2>
      <ul>
       <li>
        Hardware Recommendations
       </li>
       <li>
        Preparing for Installation
       </li>
       <li>
        An Installation Walkthrough
       </li>
       <li>
        Post Installation Configuration
       </li>
      </ul>
      <h2>
       CREATING WEB APPLICATIONS AND SITE COLLECTIONS
      </h2>
      <ul>
       <li>
        Understanding Web Applications
       </li>
       <li>
        Creating Web Applications
       </li>
       <li>
        Understanding Service Applications
       </li>
       <li>
        Site Collection Logical Architecture
       </li>
       <li>
        Planning Site Collections
       </li>
       <li>
        Creating Site Collections
       </li>
      </ul>
      <h2>
       DOCUMENT MANAGEMENT
      </h2>
      <ul>
       <li>
        Document Libraries
       </li>
       <li>
        Check Out / Check In
       </li>
       <li>
        Content Types
       </li>
       <li>
        Document Versioning
       </li>
       <li>
        Document Center
       </li>
      </ul>
      <h2>
       WORKING WITH LISTS
      </h2>
      <ul>
       <li>
        Creating Lists
       </li>
       <li>
        Managing List Columns and Views
       </li>
       <li>
        Using Content Types in List
       </li>
       <li>
        Lookups and Managed Metadata Columns
       </li>
      </ul>
      <h2>
       WORKING WITH WIKIS, BLOGS AND SURVEYS
      </h2>
      <ul>
       <li>
        Creating and Configuring Wikis
       </li>
       <li>
        Blogs
       </li>
       <li>
        Surveys
       </li>
       <li>
        Discussion Boards etc
       </li>
      </ul>
      <h2>
       MANAGING AND EXTENDING WEB APPLICATIONS
      </h2>
      <ul>
       <li>
        Extending and Deleting Web Applications
       </li>
       <li>
        Managing General Settings
       </li>
       <li>
        Managing Features
       </li>
       <li>
        Managed Paths
       </li>
       <li>
        Security Settings
       </li>
      </ul>
      <h2>
       MANAGING SITE COLLECTIONS AND DATABASES
      </h2>
      <ul>
       <li>
        Managing Your Content Databases
       </li>
       <li>
        Add Multiple Content Databases with single Web Application
       </li>
       <li>
        Move SiteCollections in Databases
       </li>
      </ul>
      <h2>
       SHAREPOINT 2010 SERVICE APPLICATION ARCHITECTURE
      </h2>
      <ul>
       <li>
        Configuring and Creating Service Applications
       </li>
       <li>
        Configuring Enterprise Managed Metadata Service Application
       </li>
       <li>
        Configuring Search Service Application
       </li>
       <li>
        Configuring Business Connectivity Service Application
       </li>
       <li>
        Configuring User Profile Service Application
       </li>
       <li>
        Configuring Excel Service Application
       </li>
       <li>
        Configuring Secure Store Service Application
       </li>
      </ul>
      <h2>
       ENTERPRISE MANAGED METADATA
      </h2>
      <ul>
       <li>
        Creating Terms and Term Stores
       </li>
       <li>
        Using Managed Metadata in Lists and Libraries
       </li>
       <li>
        Import and Export Terms
       </li>
       <li>
        Design and Best Practices
       </li>
      </ul>
      <h2>
       SEARCH ADMINISTRATION
      </h2>
      <ul>
       <li>
        SharePoint Search Farm Architecture with multiple Servers
       </li>
       <li>
        Defining Content Source, search scopes, best bets and keywords
       </li>
       <li>
        Using Enterprise Managed Metadata with Search
       </li>
       <li>
        Managing Search
       </li>
       <li>
        Customizing Search
       </li>
      </ul>
      <h2>
       BUSINESS CONNECTIVITY SERVICE
      </h2>
      <ul>
       <li>
        Business Connectivity Services Architecture and Security Model
       </li>
       <li>
        Interact with (Read/Write) external data systems such as ERP, CRM and other Line of Business
       </li>
       <li>
        BCS Web Parts
       </li>
      </ul>
      <h2>
       PEOPLE AND SOCIAL NETWORKING
      </h2>
      <ul>
       <li>
        User Profile Service Configuration
       </li>
       <li>
        User Profile Properties Management
       </li>
       <li>
        My Sites
       </li>
       <li>
        People Search
       </li>
      </ul>
      <h2>
       BUSINESS INTELLIGENCE
      </h2>
      <ul>
       <li>
        Key Performance Indicators (KPIs) and Dashboards
       </li>
       <li>
        PerformancePoint Services
       </li>
       <li>
        Excel Services
       </li>
       <li>
        Visio Services
       </li>
      </ul>
      <h2>
       SECURE STORE SERVICE
      </h2>
      <ul>
       <li>
        Configuring Secure Store Service
       </li>
       <li>
        Consuming Secure Store Service
       </li>
      </ul>
      <h2>
       DISASTER RECOVERY
      </h2>
      <ul>
       <li>
        Disaster Recovery Options
       </li>
       <li>
        Backing up SharePoint Content Dbs
       </li>
       <li>
        Restoring SharePoint Content
       </li>
       <li>
        High Availability Options
       </li>
       <li>
        Ensuring a Complete Farm Recovery
       </li>
      </ul>
      <h2>
       UPGRADE AND MIGRATION
      </h2>
      <ul>
       <li>
        DB Attach Migration
       </li>
       <li>
        In Place Migration
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="sharepointadmin2010training.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="66 + 83 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="149">
       <input name="url" type="hidden" value="/sharepointadmin2010training/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>