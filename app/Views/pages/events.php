<?php echo view("includes/header.php"); ?>
<section id="breadCrumb">

    <div class="container">

        <div class="row">

            <div class="col-md-6">

                <h3>EVENTS</h3>

            </div>
            <!-- End Of Col MD 6 -->

            <div class="col-md-6 text-right">

                <a href="/">Home</a> / <a href="">EVENTS</a>

            </div>
            <!-- End Of Col MD 6 -->

        </div>
        <!-- End Of Row -->

    </div>
    <!-- End OF Container -->

</section>
<!-- End Of Bread Crumb -->

<section id="mainArea">

    <div class="container">

        <div class="row">

            <div class="col-md-4">

                <div class="eventBox">

                    <img src="../images/workshops.jpg" alt="Event" class="img-responsive">

                    <div class="clearfix"></div>

                    <h4>WORKSHOP AT COLLEGES</h4>

                    <p>These face-to-face workshops bring student the latest developments, tools and resources</p>
                    <a href="../workshop-at-colleges">Continue Reading</a>

                </div>

            </div>
            <!-- End Of Col MD 4 -->

            <div class="col-md-4">

                <div class="eventBox">

                    <img src="../images/placement-cell.jpg" alt="Event" class="img-responsive">

                    <div class="clearfix"></div>

                    <h4>OUR PLACEMENT CELL</h4>

                    <p>Through various global associations DUCAT provide job opportunity for the trainee!</p>
                    <a href="../our-placement-cell">Continue Reading</a>

                </div>

            </div>
            <!-- End Of Col MD 4 -->

            <div class="col-md-4">

                <div class="eventBox">

                    <img src="../images/workshop-at-ducat.jpg" alt="Event" class="img-responsive">

                    <div class="clearfix"></div>

                    <h4>WORKSHOP AT DUCAT</h4>

                    <p>Bring you the latest developments , tools and resources to help!</p>
                    <a href="../workshop-at-ducat">Continue Reading</a>

                </div>

            </div>
            <!-- End Of Col MD 4 -->

        </div>
        <!-- End Of Row -->

    </div>
    <!-- End OF Container -->

</section>
<?php echo view("includes/footer.php"); ?>