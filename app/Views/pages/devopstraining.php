<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     DevOps Training
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     DevOps
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      DevOps Training From Ducat
     </h4>
     <p>
      DevOps is a new term emerging from the collision of two major related trends. 1) The first was also called “agile system administration” or “agile operations”; it sprang from applying newer Agile and Lean approaches to operations work. 2)The second is a much expanded understanding of the value of collaboration between development and operations staff throughout all stages of the development lifecycle when creating and operating a service.DevOps is the  union of  people, process and products to enable the  continuous  delivery  of   value to end users. It aims to create a culture and environment where building, testing, and releasing software can happen rapidly, frequently, and more reliably, so you can innovate like a startup and scale for the enterprise
     </p>
     <div class="contentAcc">
      <h2>
       Linux and Python Essentials
      </h2>
      <ul>
       <li>
        Linux commands and working example of them
       </li>
       <li>
        Python commands and working example of them
       </li>
      </ul>
      <h2>
       Introduction to DevOps
      </h2>
      <ul>
       <li>
        Understand DevOps
       </li>
       <li>
        Roles and responsibilities
       </li>
       <li>
        Understand the infrastructure layouts and its challenges
       </li>
       <li>
        Important terminology
       </li>
       <li>
        Continuous Integration and Deployment
       </li>
       <li>
        Introduction to multi-tier architecture Application
       </li>
      </ul>
      <h2>
       AWS: Cloud Computing
      </h2>
      <ul>
       <li>
        What is Cloud Computing?
       </li>
       <li>
        Types and Service models of cloud computing
       </li>
       <li>
        Understanding AWS Architecture
       </li>
       <li>
        About EC2 and types , Pricing
       </li>
       <li>
        EIP ( Elastic IP address), Allocating, associating , releasing
       </li>
       <li>
        Launch Linux Instances in AWS
       </li>
       <li>
        Connecting Linux instances from windows desktop and Linux machines
       </li>
      </ul>
      <h2>
       Software Installation
      </h2>
      <ul>
       <li>
        yum Installation
       </li>
       <li>
        apt-get Installation
       </li>
      </ul>
      <h2>
       Understand Web Servers
      </h2>
      <ul>
       <li>
        What's Apache and HTTP/HTTPS protocol
       </li>
       <li>
        Understanding the Apache compilation
       </li>
       <li>
        Run two apache versions on same machine.
       </li>
       <li>
        Understanding Nginx and use as reverse proxy to serve static content
       </li>
      </ul>
      <h2>
       GIT: Version control system
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        What is Git
       </li>
       <li>
        About Version Control System
       </li>
       <li>
        A short history of GIT
       </li>
       <li>
        GIT Basics
       </li>
       <li>
        GIT Command Line
       </li>
       <li>
        Installing on Linux and Windows/li&gt;
        <li>
         Creating repository
        </li>
        <li>
         Cloning, check-in and committing
        </li>
        <li>
         Fetch pull and remote
        </li>
        <li>
         Branching
        </li>
        <li>
         Creating the Branches, switching the branches, merging the branches
        </li>
       </li>
      </ul>
      <h2>
       Jenkins - Continuous Integration
      </h2>
      <ul>
       <li>
        Understanding continuous integration
       </li>
       <li>
        Introduction about Jenkins
       </li>
       <li>
        Build Cycle
       </li>
       <li>
        Jenkins Architecture
       </li>
       <li>
        Obtaining and installing Jenkins
       </li>
       <li>
        Install and setup Jenkins
       </li>
       <li>
        Exploring Jenkins Dashboard
       </li>
       <li>
        Creating Jobs
       </li>
       <li>
        Running the Jobs
       </li>
       <li>
        Setting up the global environments for Jobs
       </li>
       <li>
        Adding and updating Plugins
       </li>
       <li>
        Disabling and deleting jobs
       </li>
       <li>
        Understanding Deployment
       </li>
       <li>
        Jenkins integration with GIT
       </li>
       <li>
        Jenkins to run script remotely
       </li>
       <li>
        Jenkins integration with Maven
       </li>
       <li>
        Jenkins pipelines
       </li>
      </ul>
      <h2>
       Ansible
      </h2>
      <ul>
       <li>
        Introduction of Ansible
       </li>
       <li>
        Setting up Ansible
       </li>
       <li>
        Execute ad-hoc commands against servers
       </li>
       <li>
        Playbooks
       </li>
       <li>
        Use modules
       </li>
       <li>
        Write Ansible configuration playbooks to deploy a 3-tier web application
       </li>
       <li>
        Ansible use case
       </li>
       <li>
        Ansible Integration with Jenkins
       </li>
       <li>
        Ansible Architecture
       </li>
       <li>
        Ansible terminology
       </li>
       <li>
        Ansible commands
       </li>
       <li>
        Ansible installation and configuration
       </li>
       <li>
        Installing ssh on nodes
       </li>
       <li>
        Components of Ansbile
       </li>
       <li>
        Inventory
       </li>
       <li>
        Configuration
       </li>
      </ul>
      <h2>
       Monitoring: Nagios System &amp; Monitoring Benefits
      </h2>
      <ul>
       <li>
        Understand the Network Monitoring Tools
       </li>
       <li>
        Comprehend the Requirements of a System
       </li>
       <li>
        Comprehend the Plug-in &amp; Add-ons
       </li>
       <li>
        Comprehend the Nagios Dependencies
       </li>
       <li>
        Comprehend the Configuration of Web Interface
       </li>
       <li>
        Install and Configure the NRPE
       </li>
       <li>
        Define Nagios Commands
       </li>
      </ul>
      <h2>
       Containerization
      </h2>
      <ul>
       <li>
        What is a Docker
       </li>
       <li>
        Use case of Docker
       </li>
       <li>
        Platforms for Docker
       </li>
       <li>
        Dockers vs. Virtualization
       </li>
       <li>
        Docker Architecture
       </li>
       <li>
        Understanding the Docker components
       </li>
       <li>
        Installing Docker on Ubuntu
       </li>
       <li>
        Some Docker commands
       </li>
       <li>
        Docker Hub
       </li>
       <li>
        Downloading Docker images
       </li>
       <li>
        Uploading the images in Docker Registry
       </li>
       <li>
        Understanding the containers
       </li>
       <li>
        Running commands in container
       </li>
       <li>
        Running multiple containers
       </li>
       <li>
        Creating a custom image
       </li>
       <li>
        Running a container from the custom image.
       </li>
       <li>
        Publishing the custom image
       </li>
      </ul>
      <h2>
       Puppet for configuration Management
      </h2>
      <ul>
       <li>
        What is Puppet?
       </li>
       <ul>
        <li>
         How puppet works
        </li>
        <li>
         Puppet Architecture
        </li>
        <li>
         Master and Agents
        </li>
        <li>
         Configuration Language
        </li>
        <li>
         Resource Abstrac on Layer
        </li>
        <li>
         Transactional Layer
        </li>
       </ul>
       <li>
        Installation and Configuration
       </li>
       <ul>
        <li>
         Installing Puppet
        </li>
        <li>
         Configuring Puppet Master and Agent
        </li>
        <li>
         Connectng Agents
        </li>
       </ul>
       <li>
        Puppet Master
       </li>
       <ul>
        <li>
         Puppet configuration tree
        </li>
        <li>
         Puppet configuration files
        </li>
       </ul>
       <li>
        Puppet Language Basics
       </li>
       <ul>
        <li>
         The declarative language
        </li>
        <li>
         Resources
        </li>
        <li>
         Resource Collectors
        </li>
        <li>
         Virtual Resources
        </li>
        <li>
         Exported Resources
        </li>
        <li>
         Manifests
        </li>
        <li>
         Relationships and Ordering
        </li>
        <li>
         Modules and Classes
        </li>
        <li>
         Class Parameters
        </li>
        <li>
         Defined Types
        </li>
       </ul>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="devopstraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="22 + 95 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="117">
       <input name="url" type="hidden" value="/devopstraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>