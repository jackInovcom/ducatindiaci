<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     Automation Anywhere training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Automation Anywhere
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Robotic Process Automation (RPA) is one of the central technologies changing the face of work � for the better. RPA takes on the burden of manual, repetitive tasks, releasing individuals to realize their potential. But it doesn�t happen by itself. The new digital workforce powered by bots needs humans � people who know how to design, build, manage, and analyze bots and their work and how RPA fits into broader business strategies and execution. Being AN RPA leader takes ability, each technical and strategic. That data is currently a lot of offered than ever before through world wide training programs and courses. Ducat offers courses and certifications in RPA and teaches how to apply this technology in the real world of work. With a mission to expand the reach and accessibility of RPA knowledge and training to everyone, everywhere, Ducat delivers training in classrooms. 
						  Ducat provides Best Automation Anywhere Training in Noida based on industry standards that helps attendees to secure placements in their dream jobs at MNCs. Ducat Provides Best Automation Anywhere Training in Delhi NCR. Ducat is one of the most credible Automation Anywhere Training institutes in Noida offering hands on practical knowledge and full job assistance with basic as well as advanced level Automation Anywhere Training courses.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction &amp; Understanding of RPA
      </h2>
      <ul>
       <li>
        What is RPA
       </li>
       <li>
        Pre-requisites of RPA
       </li>
       <li>
        Best use of RPA Process
       </li>
       <li>
        RPA Planning
       </li>
       <li>
        Life Cycle of RPA
       </li>
       <li>
        Timeframe of RPA Process
       </li>
       <li>
        Identification of RPA Areas
       </li>
       <li>
        Concept of ROI (Return on Investment)
       </li>
       <li>
        Attended and Unattended RPA
       </li>
      </ul>
      <h2>
       Overview
      </h2>
      <ul>
       <li>
        About the Course
       </li>
       <li>
        About Me
       </li>
       <li>
        Why Automation Anywhere ?
       </li>
       <li>
        Who can use the "trial" version of Automation Anywhere
       </li>
       <li>
        End to End demo: Build a simple Software Robot
       </li>
       <li>
        Installation &amp; Set up Process for Automation Anywhere
       </li>
      </ul>
      <h2>
       Orientation
      </h2>
      <ul>
       <li>
        Overview
       </li>
       <li>
        Create new Projects
       </li>
       <li>
        Open &amp; copy existing projects
       </li>
       <li>
        Project &amp; tool windows tour
       </li>
      </ul>
      <h2>
       Automation Anywhere Control Room
      </h2>
      <ul>
       <li>
        Control Room Dashboard
       </li>
       <li>
        User Management
       </li>
       <li>
        Scheduling Bots in Control Room
       </li>
       <li>
        Credential Management
       </li>
       <li>
        License Management
       </li>
       <li>
        Disaster Management in Automation Anywhere
       </li>
       <li>
        Operation Management
       </li>
       <li>
        Audit Trial
       </li>
       <li>
        Control Room settings
       </li>
      </ul>
      <h2>
       Workflow for RPA Process
      </h2>
      <ul>
       <li>
        Creation of workflow
       </li>
       <li>
        Execution of workflow
       </li>
       <li>
        Validation of workflow
       </li>
      </ul>
      <h2>
       Basic Automation Anywhere skills
      </h2>
      <ul>
       <li>
        Overview
       </li>
       <li>
        Creation &amp; Usage of user defined variables
       </li>
       <li>
        Usage of system defined variables
       </li>
      </ul>
      <h2>
       Debugging
      </h2>
      <ul>
       <li>
        Run in debug mode
       </li>
       <li>
        Set a break point and debug line by line
       </li>
      </ul>
      <h2>
       Handling Error
      </h2>
      <ul>
       <li>
        Try/Catch into - Catch an exception
       </li>
       <li>
        Create &amp; throw a new exception
       </li>
       <li>
        Log Exception
       </li>
       <li>
        Send Email
       </li>
       <li>
        Assign Activity
       </li>
       <li>
        Run task activity
       </li>
       <li>
        Error snapshot
       </li>
      </ul>
      <h2>
       Automation Anywhere Client Commands
      </h2>
      <ul>
       <li>
        Usage of Repository
       </li>
       <li>
        Report Generation of bots
       </li>
       <li>
        Scheduler Manager in AA Client
       </li>
       <li>
        Trigger Manager in AA Client
       </li>
       <li>
        Hot Keys in AA Client
       </li>
       <li>
        Notification and Speed of Task Bots
       </li>
       <li>
        Database command
       </li>
       <li>
        Logs Handling in Bots
       </li>
       <li>
        Email Automation Process
       </li>
       <li>
        File &amp; Folder Automation
       </li>
       <li>
        Image Recognition Command
       </li>
       <li>
        Web Recorder Command
       </li>
       <li>
        Object Cloning
       </li>
       <li>
        Clipboard
       </li>
       <li>
        PGP activity command
       </li>
       <li>
        CSV/text file command
       </li>
       <li>
        If/else command
       </li>
       <li>
        Prompt command
       </li>
       <li>
        Windows activity command
       </li>
       <li>
        MS - Excel Automation Commands
       </li>
       <li>
        Static and Dynamic Delays in Automation Anywhere
       </li>
       <li>
        Loop Implementation in Automation Process For
       </li>
       <li>
        Excel Dataset
       </li>
       <li>
        Internet Explorer Table
       </li>
       <li>
        SQL Query Dataset
       </li>
       <li>
        XML Dataset
       </li>
       <li>
        CSV File Dataset
       </li>
       <li>
        Variable Operation Commands
       </li>
       <li>
        String Operation Commands
       </li>
       <li>
        Execution of Sub Task in Main Bots
       </li>
       <li>
        Screen &amp; Visualize Capture Command
       </li>
       <li>
        Automation of Web Services &amp; Rest Web Service
       </li>
       <li>
        Mouse and Keyboard action automation
       </li>
       <li>
        User Input Automation Command
       </li>
       <li>
        XML Automation Process
       </li>
       <li>
        Debugging of Task Bots
       </li>
       <li>
        Security Implementation through Automation Process
       </li>
       <li>
        PDF Automation Process
       </li>
       <li>
        Automation of Windows Controls
       </li>
       <li>
        Terminal Emulator
       </li>
      </ul>
      <h2>
       Metabots
      </h2>
      <ul>
       <li>
        Creation of metabots
       </li>
       <li>
        Consumption of metaBots
       </li>
       <li>
        Variable declaration in metabots
       </li>
       <li>
        Calibration
       </li>
       <li>
        Creation of Asset
       </li>
       <li>
        Creation of Logic
       </li>
       <li>
        Import Dataset
       </li>
      </ul>
      <h2>
       Documentation
      </h2>
      <ul>
       <li>
        Process Definition Document(PDD)
       </li>
       <li>
        Solution Design Document(SDD)
       </li>
       <li>
        Technical Design Document(TDD)
       </li>
       <li>
        As- Is and To-be process flow
       </li>
       <li>
        BOT management diagram
       </li>
      </ul>
      <h2>
       Case Study
      </h2>
      <ul>
       <li>
        Case Study 1(Bloomberg)
       </li>
       <li>
        Case Study 2(Excel activity)
       </li>
       <li>
        Case Study 3(PDF customer - vendor domain)
       </li>
      </ul>
      <h2>
       Live Project Overview
      </h2>
      <ul>
       <li>
        At least 1 Project
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="automationanywheretraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="90 + 71 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="161">
       <input name="url" type="hidden" value="/automationanywheretraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>