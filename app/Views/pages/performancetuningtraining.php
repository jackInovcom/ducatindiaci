<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Performance Tuning Training
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     Performance Tuning
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      we are offering Performance Tuning Training in more practical way. Ducat Performance Tuning trainers offers Performance Tuning in Classroom training,Performance Tuning Corporate Training services. We framed our syllabus to match with the real world requirements for both beginner level to advanced level.
     </p>
     <div class="contentAcc">
      <h2>
       Elements of Monitoring and Tuning
      </h2>
      <ul>
       <li>
        Monitoring vs Profiling
       </li>
       <li>
        Whole system view
       </li>
       <li>
        Before tuning kernel
       </li>
       <li>
        Kernel Tunables
       </li>
       <li>
        Using system documentation
       </li>
      </ul>
      <h2>
       Simple Network Monitoring
      </h2>
      <ul>
       <li>
        SNMP
       </li>
       <li>
        Management Information Base
       </li>
       <li>
        Installing SNMP
       </li>
       <li>
        Using SNMP for queries
       </li>
       <li>
        Configuring SNMP client
       </li>
       <li>
        Enabling SNMP agents
       </li>
       <li>
        Profiling SNMP host access controls
       </li>
      </ul>
      <h2>
       Graphical Reporting
      </h2>
      <ul>
       <li>
        MRTG
       </li>
       <li>
        Configuring MRTG
       </li>
       <li>
        Ad-hoc utilities
       </li>
       <li>
        iostat and sar
       </li>
       <li>
        Awk
       </li>
       <li>
        Using Gnuplot
       </li>
       <li>
        Creating custom script
       </li>
      </ul>
      <h2>
       Kernel Level Profiling
      </h2>
      <ul>
       <li>
        Installing OProfile
       </li>
       <li>
        Installing kernel-debuginfo
       </li>
       <li>
        OProfile Architecture
       </li>
       <li>
        System Tap
       </li>
       <li>
        System Tap Scripts
       </li>
       <li>
        stap command
       </li>
       <li>
        staprun command
       </li>
      </ul>
      <h2>
       Queuing Theory
      </h2>
      <ul>
       <li>
        Introduction to Queuing Theory
       </li>
       <li>
        Little�s Law
       </li>
       <li>
        Queue Length
       </li>
       <li>
        Wait Time
       </li>
       <li>
        Finding hot spot in codes
       </li>
       <li>
        Arrival rate vs completion rate
       </li>
       <li>
        Predicting system-wide limits
       </li>
       <li>
        Predicting resource limits
       </li>
      </ul>
      <h2>
       Physical Disk Characteristics
      </h2>
      <ul>
       <li>
        Physical factors affecting disk IO
       </li>
       <li>
        Disk storage density
       </li>
       <li>
        Block IO request and cache effect
       </li>
       <li>
        Tuning sequential read access
       </li>
       <li>
        Tuning the disk queue
       </li>
       <li>
        Tuning deadline scheduler
       </li>
       <li>
        Tuning anticipatory scheduler
       </li>
       <li>
        Tuning noop scheduler
       </li>
       <li>
        Tuning cfq scheduler
       </li>
       <li>
        Physical block device interface
       </li>
       <li>
        Virtual block devices
       </li>
      </ul>
      <h2>
       Reducing Disk Visit Count
      </h2>
      <ul>
       <li>
        Virtual File System (VFS)
       </li>
       <li>
        Fragmentation
       </li>
       <li>
        Viewing fragmentation
       </li>
       <li>
        Tuning fragmentation
       </li>
       <li>
        Filesystem limits
       </li>
       <li>
        Journaling
       </li>
       <li>
        Improving journal performance
       </li>
       <li>
        Tuning journal performance
       </li>
       <li>
        Lock contention
       </li>
       <li>
        Chunk size
       </li>
       <li>
        Calculating filesystem stride
       </li>
       <li>
        Tuning RAID
       </li>
      </ul>
      <h2>
       Processes and Scheduler
      </h2>
      <ul>
       <li>
        Types of CPU cache
       </li>
       <li>
        Locality of reference
       </li>
       <li>
        Improving locality of reference
       </li>
       <li>
        Tuning scheduler policy
       </li>
       <li>
        Viewing CPU performance data
       </li>
      </ul>
      <h2>
       Kernel Timing and Process Latency
      </h2>
      <ul>
       <li>
        Tuning system ticks
       </li>
       <li>
        Tuning processor speed
       </li>
       <li>
        IRQ balancing
       </li>
       <li>
        Tuning IRQ affinity
       </li>
       <li>
        Equalizing CPU visit count
       </li>
       <li>
        Hot-plugging CPUs
       </li>
       <li>
        Virtual CPUs
       </li>
       <li>
        Tuning VCPUs at domain creation
       </li>
       <li>
        Tuning VCPU affinity
       </li>
      </ul>
      <h2>
       Memory Addressing and Allocation
      </h2>
      <ul>
       <li>
        Overview of memory addressing
       </li>
       <li>
        Virtual disk space
       </li>
       <li>
        Viewing and Tuning process address space
       </li>
       <li>
        Physical address space
       </li>
       <li>
        Overview of memory allocation
       </li>
       <li>
        Improving RAM performance
       </li>
       <li>
        Improving MMU performance
       </li>
       <li>
        Improving NUMA performance
       </li>
       <li>
        Improving TLB performance
       </li>
       <li>
        Viewing System calls
       </li>
       <li>
        Virtual domain memory
       </li>
       <li>
        Recovering unassigned memory
       </li>
      </ul>
      <h2>
       Memory Cache
      </h2>
      <ul>
       <li>
        Tuning page allocation
       </li>
       <li>
        Tuning overcommit
       </li>
       <li>
        Slab cache
       </li>
       <li>
        ARP cache
       </li>
       <li>
        Tuning ARP cache
       </li>
       <li>
        Page cache
       </li>
       <li>
        Tuning page cache
       </li>
       <li>
        Anonymous pages
       </li>
      </ul>
      <h2>
       Memory Reclamation
      </h2>
      <ul>
       <li>
        Page status
       </li>
       <li>
        Calculating dirty and clean memory
       </li>
       <li>
        Reclaiming dirty pages
       </li>
       <li>
        Tuning pdflush
       </li>
       <li>
        Out of memory killer
       </li>
       <li>
        Tuning OOM policy
       </li>
       <li>
        Detecting memory leaks
       </li>
       <li>
        Understanding swap
       </li>
       <li>
        Improving swap performance
       </li>
       <li>
        Tuning swappiness
       </li>
       <li>
        Tuning swap size
       </li>
       <li>
        Tuning swap for think time
       </li>
       <li>
        Tuning swap visit count
       </li>
       <li>
        Monitoring memory usage
       </li>
      </ul>
      <h2>
       Essential Network Tuning
      </h2>
      <ul>
       <li>
        Calculating per-socket buffer size
       </li>
       <li>
        Tuning core buffer size
       </li>
       <li>
        Tuning TCP buffer size
       </li>
       <li>
        Tuning DMA buffer size
       </li>
       <li>
        Tuning fragmentation buffers
       </li>
       <li>
        Network interrupt handling
       </li>
       <li>
        Network sockets
       </li>
       <li>
        TCP sockets
       </li>
       <li>
        Tuning TCP socket creation
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="performancetuningtraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="13 + 67 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="80">
       <input name="url" type="hidden" value="/performancetuningtraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>