<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     java selenium training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     java selenium
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Ducat java selenium training will make you an expert in Automation tool, so that you can initiatives in your organisation. Selenium with Java Training course designed to teach the basics of the WebDriver API using Java. This course also covers webdriver Interface explanation and Invoking Browser, Identifying locators(id,name,linkText) with developer tools -1, Techniques to automate Ajax calls,Child Windows and IFrames, Difference between HashMap and HashTable, Writing Generic functions to Identify Frames/Windows. Selenium with Java training from Ducat can help your career reach another success ladder. The training consists of full hand on the session such as how can automation web based application and implementation will help in frameworks such as Hybrid and Data driven that can help you in future.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction to Java
      </h2>
      <ul>
       <li>
        Introduction to Java Programming Language,
       </li>
       <li>
        Uses of Java language, Java Syntax Rules &amp; Coding Standards,
       </li>
       <li>
        Java Environment Setup, Write &amp; Execute first Java Program,
       </li>
       <li>
        And Write Java Programs using Eclipse IDE
       </li>
      </ul>
      <h2>
       Java Programming for Selenium
      </h2>
      <ul>
       <li>
        Overview of Java Programming, Java Modifiers, Data Types,
       </li>
       <li>
        Variables, Operators, Flow Control, Arrays, String Handling,
       </li>
       <li>
        File Handling, Exception Handling, and Methods.
       </li>
       <li>
        Java OOPS: Inheritance, Polymorphism, Abstraction, and Encapsulation.
       </li>
      </ul>
      <h2>
       Java Program Structure and Syntax
      </h2>
      <ul>
       <li>
        Java Programming Structure, Writing Comments,
       </li>
       <li>
        Declaring Methods, Declaring Variables,
       </li>
       <li>
        Declaring Constants, Print Statements, usage of Operators,
       </li>
       <li>
        Writing Conditional Statements, Writing Loop Statements and Calling methods
       </li>
      </ul>
      <h2>
       Java Conditional and Loop Statements
      </h2>
      <ul>
       <li>
        Java Conditional and Loop Statements ,explains Types of Conditional statements in Java,
       </li>
       <li>
        Types of Conditions and usage of conditional statements.
       </li>
       <li>
        If statement and switch statements in Java with examples.
       </li>
       <li>
        Java loop structures, for loop, while loop, do while loop and enhanced for loop with examples.
       </li>
      </ul>
      <h2>
       Strings and Arrays in Java
      </h2>
      <ul>
       <li>
        Strings and Arrays in Java, explains String handling in Java,
       </li>
       <li>
        Creating Strings, String Concatenation, and String Comparison.
       </li>
       <li>
        Array declaration, types of Arrays, print Array, copy Array,
       </li>
       <li>
        Array methods and Advantages &amp; Disadvantages of Arrays.
       </li>
      </ul>
      <h2>
       Input Output Operations and File Handling
      </h2>
      <ul>
       <li>
        Input and Output operations and File handling in Java,
       </li>
       <li>
        Explains reading different types of input and displaying output on the console.
       </li>
      </ul>
      <h2>
       Collections Framework
      </h2>
      <ul>
       <li>
        Introductions of Collections
       </li>
       <li>
        Types of collections
       </li>
       <li>
        Generic -List, Dictionary, Stack and Queue
       </li>
       <li>
        Non Generic- ArrayList, HashTable, Stack and Queue
       </li>
      </ul>
      <h2>
       Selenium
      </h2>
      <h2>
       Introduction of Selenium:
      </h2>
      <ul>
       <li>
        What is Automation Testing
       </li>
       <li>
        Use of automation Testing
       </li>
       <li>
        Tools for Automation Testing
       </li>
       <li>
        Why automation is important for you career?
       </li>
       <li>
        What is Selenium.
       </li>
       <li>
        Advantage of Selenium.
       </li>
       <li>
        History of Selenium.
       </li>
       <li>
        Component of Selenium.
       </li>
       <li>
        Architecture of Remote Control.
       </li>
       <li>
        Architecture of WebDriver.
       </li>
       <li>
        Architecture of GRID.
       </li>
       <li>
        Differences between API's Vs GUI's.
       </li>
      </ul>
      <h2>
       Selenium IDE:
      </h2>
      <ul>
       <li>
        Selenium IDE Introduction
       </li>
       <li>
        Record and Playback with Firefox and Chrome
       </li>
       <li>
        Debugging in Selenium IDE Script
       </li>
       <li>
        Introduction Katalone Studio
       </li>
       <li>
        Record and Playback through Katalone Studio
       </li>
      </ul>
      <h2>
       Set up Eclipse:
      </h2>
      <ul>
       <li>
        Download and install java
       </li>
       <li>
        Download and start Eclipse
       </li>
       <li>
        Download and configure WebDriver java client
       </li>
       <li>
        Set up a project
       </li>
       <li>
        Create packages
       </li>
       <li>
        Create a First Java test case
       </li>
       <li>
        Import WebDriver Source file
       </li>
      </ul>
      <h2>
       WebDriver:
      </h2>
      <ul>
       <li>
        WebDriver Interface
       </li>
       <li>
        WebElement Interface
       </li>
       <li>
        Launching Firefox browser
       </li>
      </ul>
      <h2>
       Browser &amp; Navigation Commands:
      </h2>
      <ul>
       <li>
        How to Open a URL
       </li>
       <li>
        Verify Page title
       </li>
       <li>
        Strategy to get the Page Source
       </li>
       <li>
        Difference between Close &amp; Quit
       </li>
       <li>
        Ways to Navigate Back &amp; Forward
       </li>
       <li>
        How to Refresh Page
       </li>
       <li>
        Another way of Navigating to specific Page
       </li>
      </ul>
      <h2>
       Locators:
      </h2>
      <ul>
       <li>
        What are locators.
       </li>
       <li>
        HTML Basics.
       </li>
       <li>
        HTML language tags and attributes.
       </li>
       <li>
        ID, Name, Xpath, CSS etc.
       </li>
       <li>
        Difference between Absolute &amp; Complete Xpath.
       </li>
       <li>
        Finding your first element.
       </li>
       <li>
        WebElement Commands.
       </li>
      </ul>
      <h2>
       Element Identification:
      </h2>
      <ul>
       <li>
        Element Inspector in Mozilla, Chrome and IE
       </li>
       <li>
        Element locator tool for Firefox Locator
       </li>
       <li>
        Firebug&amp;Fire Path Add-Ons in Mozilla
       </li>
       <li>
        Various HTML locator strategies
       </li>
       <li>
        XPath Helper Plug-in for Chrome
       </li>
       <li>
        Selection of Effective Xpath
       </li>
       <li>
        Handling Dynamic objects/ids on the page
       </li>
      </ul>
      <h2>
       Tables, Checkboxes &amp; Radio buttons:
      </h2>
      <ul>
       <li>
        Identify table rows and columns
       </li>
       <li>
        Extracting values from a cell
       </li>
       <li>
        Dynamically Identify Tables Data
       </li>
       <li>
        Select class in Selenium
       </li>
       <li>
        Drop Down Handle.
       </li>
       <li>
        Select multiple values from the list
       </li>
       <li>
        Select &amp; Deselect operations by Index, Value &amp; Visible Text.
       </li>
      </ul>
      <h2>
       Selenium Waits, Alert &amp; Switch Windows:
      </h2>
      <ul>
       <li>
        Implicit and Explicit waits
       </li>
       <li>
        How to use Expected Conditions with Waits
       </li>
       <li>
        PageLoadTimeout &amp; SetScriptTimeout property
       </li>
       <li>
        Simple use of Thread Sleep
       </li>
       <li>
        Concept of Fluent Wait in Selenium
       </li>
       <li>
        WebDriverWait and its uses
       </li>
       <li>
        Different WaitUntil Conditions
       </li>
       <li>
        Ways to handle Simple, Confirmation &amp; Prompt Alert
       </li>
       <li>
        Concepts of Set Interface in Java
       </li>
       <li>
        Difference between Window Handle &amp; Handles
       </li>
       <li>
        Switching &amp; Closing Windows, Tabs &amp; Popup's
       </li>
       <li>
        Concept of window ID
       </li>
       <li>
        Extracting window IDs with Selenium Object reference
       </li>
       <li>
        JavaScriptExecutor Interface
       </li>
       <li>
        Captured Screenshot
       </li>
       <li>
        Cookies Handles
       </li>
      </ul>
      <h2>
       Action Class:
      </h2>
      <ul>
       <li>
        What is Action Class &amp; What can we do with Action Class
       </li>
       <li>
        Mouse Hover &amp; Mouse Movement with Action
       </li>
       <li>
        Finding Coordinates of a Web Object
       </li>
       <li>
        Drag and Drop Action
       </li>
      </ul>
      <h2>
       TestNG Framework:
      </h2>
      <ul>
       <li>
        What is TestNG
       </li>
       <li>
        Benefits and Features of TestNG
       </li>
       <li>
        How to download TestNG
       </li>
       <li>
        Annotations in TestNG
       </li>
       <li>
        How to run Test Suite in TestNG
       </li>
       <li>
        Groups in TestNG
       </li>
       <li>
        Depend on in TestNG
       </li>
       <li>
        Test Case sequencing in TestNG
       </li>
       <li>
        TestNG Asserts
       </li>
       <li>
        TestNG Parameters
       </li>
       <li>
        Multi Browser testing in TestNG
       </li>
       <li>
        Parallel testing in TestNG
       </li>
       <li>
        Extent Report API
       </li>
      </ul>
      <h2>
       Cucumber :
      </h2>
      <ul>
       <li>
        Introduction Cucumber
       </li>
       <li>
        Using Cucumber
       </li>
       <li>
        Gherkins Language
       </li>
       <li>
        Creation of feature files and Step Definition
       </li>
       <li>
        Introduction TDD and BDD
       </li>
       <li>
        TDD Vs BDD
       </li>
       <li>
        Version Control (GitHub Introduction)
       </li>
      </ul>
      <h2>
       Log4j Logging:
      </h2>
      <ul>
       <li>
        Log4j Introduction
       </li>
       <li>
        Download Log4J
       </li>
       <li>
        Add Log4j Jar
       </li>
       <li>
        Test Case with Log4j
       </li>
       <li>
        Log4j Log Manager
       </li>
       <li>
        Log4j Appenders
       </li>
       <li>
        Log4j Loggers
       </li>
      </ul>
      <h2>
       Database Connections:
      </h2>
      <ul>
       <li>
        Database connection
       </li>
       <li>
        Database Testing in Selenium using MySql Server
       </li>
      </ul>
      <h2>
       Automation Framework:
      </h2>
      <ul>
       <li>
        What is Automation Framework
       </li>
       <li>
        Features of Automation Framework
       </li>
       <li>
        Benefits of using Automation Framework
       </li>
       <li>
        Different types of Automation Framework
       </li>
       <li>
        What is Data Driven Framework
       </li>
       <li>
        What is Modular Driven Framework
       </li>
       <li>
        What is Keyword Driven Framework
       </li>
       <li>
        Apache POI API
       </li>
       <li>
        POI Setup and Configuration
       </li>
       <li>
        Read and Write Excel file with Apache POI
       </li>
       <li>
        POM (Page Object Model)
       </li>
       <li>
        Page Factory
       </li>
      </ul>
      <h2>
       Maven:
      </h2>
      <ul>
       <li>
        Maven Introduction
       </li>
       <li>
        Install Maven in Eclipse IDE
       </li>
       <li>
        Install Maven on Windows
       </li>
       <li>
        Install Maven on Mac
       </li>
       <li>
        How to Create a New Maven Project
       </li>
       <li>
        How to Create a New Maven Project in Eclipse
       </li>
       <li>
        Configure Selenium Continuous Integration with Maven
       </li>
      </ul>
      <h2>
       Jenkins:
      </h2>
      <ul>
       <li>
        Jenkins Introduction.
       </li>
       <li>
        Selenium Integration with Jenkins.
       </li>
      </ul>
      <h2>
       Automation Test Script Development (Project Based)
      </h2>
      <ul>
       <li>
        Analyzing Manual Test Script
       </li>
       <li>
        Automation Test Script Development strategy
       </li>
       <li>
        Development Test Script according to manual script
       </li>
      </ul>
      <h2>
       Interview Preparation :
      </h2>
      <ul>
       <li>
        Technical Interview Preparation
       </li>
       <li>
        Mock interview Preparation
       </li>
       <li>
        HR Session
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="javaselenium.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="81 + 49 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="130">
       <input name="url" type="hidden" value="/javaseleniumtraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>