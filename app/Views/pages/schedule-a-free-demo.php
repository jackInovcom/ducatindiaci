<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     Schedule Free Demo
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="#">
     Schedule Demo
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9 text-center">
    <h3>
     PLEASE FILL IN THE FORM AND WE SHALL CONTACT YOU SOON!
    </h3>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
    <form action="../logics_database/schedule_demo.php" class="onlineRegistration text-left" method="post">
     <div class="row">
      <div class="col-md-6">
       <input autofocus="" name="fname" pattern="[a-z ]{2,50}" placeholder="Enter First Name" required="" tabindex="1" type="text"/>
       <input name="telephone" pattern="[0-9]{8,15}" placeholder="Phone number" required="" tabindex="3" type="text"/>
       <input name="course" pattern="[a-zA-Z0-9_-]*{1,20}" placeholder="Course Name" required="" tabindex="5" type="text"/>
      </div>
      <!-- End Of Col MD 6 -->
      <div class="col-md-6">
       <input autofocus="" name="lname" pattern="[a-z ]{1,15}" placeholder="Enter Last Name" required="" tabindex="2" type="text"/>
       <input autocomplete="off" class="txtinput" name="email" placeholder="E-mail" required="" tabindex="4" type="text"/>
       <select name="branch" required="" tabindex="6">
        <option value="Select Branch">
         Select Branch
        </option>
        <option value="Noida">
         Noida
        </option>
        <option value="Faridabad">
         Faridabad
        </option>
        <option value="Ghaziabad">
         Ghaziabad
        </option>
        <option value="Greater Noida">
         Greater Noida
        </option>
        <option value="Gurgaon">
         Gurgaon
        </option>
        <option value="Jaipur">
         Jaipur
        </option>
       </select>
      </div>
      <!-- End Of Col MD 6 -->
      <div class="col-md-12">
       <textarea name="message1" placeholder="Query Here!" required="" tabindex="7"></textarea>
      </div>
      <div class="col-md-12">
       <input name="submitReg" type="submit" value="Submit"/>
      </div>
      <!-- End Of Col MD 6 -->
     </div>
     <!-- End Of Row -->
    </form>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <form class="searchForm">
     <input placeholder="Search" type="text"/>
    </form>
    <div class="widgetArea">
     <h5>
      CONTACT INFO
     </h5>
     <address>
      <span class="address">
       A - 43 &amp; A - 52 Sector - 16,
       <br/>
       Noida (U.P) (Near McDonalds)
      </span>
      <br>
       <span class="phone">
        <strong>
         Phone:
        </strong>
        0120-4646464, +91- 9871055180
       </span>
       <br/>
       <span class="email">
        <strong>
         E-Mail:
        </strong>
        <a href="mailto:info@ducatindia.com">
         info@ducatindia.com
        </a>
       </span>
       <br/>
       <span class="web">
        <strong>
         Web:
        </strong>
        <a href="http://www.ducatindia.com/">
         http://www.ducatindia.com/
        </a>
       </span>
      </br>
     </address>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section class="text-center" id="offices">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <h5>
     CORPORATE OFFICE NOIDA:
     <span>
      0120 - 4646464
     </span>
    </h5>
    <p>
     GR.NOIDA:
     <span>
      0120-4345190
     </span>
     GHAZIABAD:
     <span>
      0120-4835400
     </span>
     FARIDABAD:
     <span>
      0129-4150605
     </span>
     GURGAON:
     <span>
      0124-4219095
     </span>
     JAIPUR:
     <span>
      0141-2550077
     </span>
    </p>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>