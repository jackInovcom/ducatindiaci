<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     I-PHONE training
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     I-PHONE Training
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      LOVE I-PHONE FOR SUPER FAST MULTI-TASKING
     </h4>
     <p>
      DUCAT I-PHONE training program emphasize concept building by lab experiments. Trainers impart training on Objective-c, Xcode and iOS development. Strong training over coding will help students face every kind of interview of all leading companies. Experienced Developers from leading companies has designed the course keeping in mind the mandatory aspect of subject.
     </p>
     <p>
      Iphone runs on an operating system called iOS and is a part of Mac OS X. It includes core animation software component and Power VR hardware. Motion graphics is interfaced with i-phone due to iOS.The best part of this operating system is it takes less than half gigabyte. An i-Phone facilities like audio conferencing, call holding, call merger, caller ID etc. Music libraries in i-Phone divide songs according to artists, albums and videos. Graphical list of available applications are available on screen. Earlier i-Phone runs one application at a time. But the new operating systems helped it achieving multi tasking. Multitasking had been introduced to market as i- Phone4 is launched. After this, iOS7 upgraded it by running more than one open application in background. For e.g. i-Phone allows user to listen music, receive notification and record GPS data at same time. Customizable home screen allows to access active widgets.
     </p>
     <div class="contentAcc">
      <h2>
       Introduction to IOS Application
      </h2>
      <ul>
       <li>
        Understanding of IOS platform
       </li>
       <li>
        IOS Application Life Cycle
       </li>
       <li>
        Understanding of X-Code IDE
       </li>
      </ul>
      <h2>
       Installation and Configuration
      </h2>
      <ul>
       <li>
        How to installed X-Code Environment
       </li>
       <li>
        How to open template for Development Environment
       </li>
      </ul>
      <h2>
       Introduction to Swift &amp; IOS Development
      </h2>
      <ul>
       <li>
        Introduce Swift, IOS Development
       </li>
      </ul>
      <h2>
       The Swift Language Basic Syntax
      </h2>
      <ul>
       <li>
        Discuss the syntax of Swift and commonly used aspects of the language
       </li>
      </ul>
      <h2>
       The Swift Language Object-Oriented Programming
      </h2>
      <ul>
       <li>
        Discuss object-oriented programming concepts
       </li>
      </ul>
      <h2>
       Building Your First App
      </h2>
      <ul>
       <li>
        Create an application in Xcode with an interactive UI
       </li>
      </ul>
      <h2>
       Creating &amp; Configuring Scroll Views
      </h2>
      <ul>
       <li>
        Discuss scroll views
       </li>
      </ul>
      <h2>
       Implementing Navigation with the Tab Bar Controller
      </h2>
      <ul>
       <li>
        Create an app with a tab bar navigation
       </li>
      </ul>
      <h2>
       Delegate Design pattern
      </h2>
      <ul>
       <li>
        UI Text Field
       </li>
       <li>
        Protocols
       </li>
       <li>
        Implement delegate method
       </li>
      </ul>
      <h2>
       Target &amp; Action Design Pattern
      </h2>
      <ul>
       <li>
        Setting target&amp; Action
       </li>
       <li>
        Implementing Methods
       </li>
      </ul>
      <h2>
       View Controllers
      </h2>
      <ul>
       <li>
        Model View Controllers
       </li>
       <li>
        basic of View Controllers
       </li>
       <li>
        Life Cycle of View Controllers
       </li>
       <li>
        Creating of View Controllers
       </li>
       <li>
        Using Interface Builder
       </li>
       <li>
        Programmatically
       </li>
      </ul>
      <h2>
       UITable View and UITable View Controller
      </h2>
      <ul>
       <li>
        Basic UITable
       </li>
       <li>
        Creating of Custom UITable View
       </li>
       <li>
        UITable view Cell
       </li>
       <li>
        Creation of UITable View Controller
       </li>
       <li>
        Using Interface Builder
       </li>
       <li>
        Programmatically
       </li>
       <li>
        UITable View Cell Advanced
       </li>
      </ul>
      <h2>
       Displaying Sets of Data: Table View
      </h2>
      <ul>
       <li>
        Implement various operations, such as inserting, moving and deleting rows, and creating
custom rows in a table view
       </li>
      </ul>
      <h2>
       UI Tab Bar Controller
      </h2>
      <ul>
       <li>
        Basic UI Tab Bar application
       </li>
       <li>
        Creation UI Tab bar Controller
       </li>
       <li>
        Using Interface Builder
       </li>
       <li>
        Programmatically
       </li>
      </ul>
      <h2>
       Displaying Sets of Data: Collection View
      </h2>
      <ul>
       <li>
        Implement various operations, such as inserting, moving and deleting cells, and creating
custom cells in a collection view
       </li>
      </ul>
      <h2>
       Data Persistence
      </h2>
      <ul>
       <li>
        Having looked at the different ways data can be saved on an IOS device, you will modify the
currency converter app so that it saves data to the device and experiment with various data
storage solutions to determine the right fit for your to-do list app.
       </li>
      </ul>
      <h2>
       Data Persistence with FileManager
      </h2>
      <ul>
       <li>
        Discuss how data can be saved and retrieved to the file system using FileManager
       </li>
      </ul>
      <h2>
       Property Lists
      </h2>
      <ul>
       <li>
        Discuss how data can be saved and retrieved to the file system using Property List files
       </li>
       <li>
        Discuss how data can be saved and retrieved to the file system using NSUserDefaults.
This data will be presented and managed from the app
        <li>
         UserDefaults: Presenting Preferences in Settings Discuss how Application Preferences
can be saved and retrieved to and from the file system using UserDefaults and managed in the Settings app
        </li>
        SQLite: Discuss how data can be saved and retrieved to the file system using SQLite
       </li>
       <li>
        Core Data:Discuss how data can be saved and retrieved to the file system using Core Data
       </li>
       <li>
        Final Project Check-In Point: Create an IOS application.
       </li>
      </ul>
      <h2>
       Gesture Programming
      </h2>
      <ul>
       <li>
        Basics of Touch Events
       </li>
       <li>
        Responding to Touch Events
       </li>
       <li>
        Moving an Image View based on gestures
       </li>
      </ul>
      <h2>
       Implementing Gesture Recognition
      </h2>
      <ul>
       <li>
        Examine various types of gesture recognizers and how to use them to make an interactive app
       </li>
      </ul>
      <h2>
       Database Storage
      </h2>
      <ul>
       <li>
        Userdefaults
       </li>
       <li>
        Property List
       </li>
       <li>
        SQLite Database
       </li>
       <li>
        Core Data
       </li>
      </ul>
      <h2>
       Create Project
      </h2>
      <h2>
       Threading
      </h2>
      <ul>
       <li>
        Basics of Thread Programming
       </li>
       <li>
        Create Custom thread to fetch data from Url
       </li>
      </ul>
      <h2>
       XML Programming
      </h2>
      <ul>
       <li>
        Basics of XML Structure
       </li>
       <li>
        XML Parsing
       </li>
      </ul>
      <h2>
       JSON Programming
      </h2>
      <ul>
       <li>
        Basics of JSON Structure
       </li>
       <li>
        JSON Parsing
       </li>
      </ul>
      <h2>
       Working with menus &amp; Dialogs
      </h2>
      <ul>
       <li>
        How to create menus in Storyboard and View-based Application
       </li>
      </ul>
      <h2>
       Notification
      </h2>
      <ul>
       <li>
        Local Notification
       </li>
       <li>
        Application Notification
       </li>
       <li>
        Types of notification
       </li>
      </ul>
      <h2>
       Camera
      </h2>
      <ul>
       <li>
        UI Image View
       </li>
       <li>
        Getting Image FromLibrary
       </li>
       <li>
        Phone Library
       </li>
      </ul>
      <h2>
       Playing Audio And Video
      </h2>
      <ul>
       <li>
        Play Video
       </li>
       <li>
        Play Audio
       </li>
       <li>
        How to Record audio and Video File in i-Phone and Play
       </li>
      </ul>
      <h2>
       Animation Programming
      </h2>
      <ul>
       <li>
        UI View Animation
       </li>
       <li>
        Basics of layers
       </li>
       <li>
        Layer Animation
       </li>
      </ul>
      <h2>
       Core Location Framework
      </h2>
      <ul>
       <li>
        CL Location &amp; CL Location Manager
       </li>
       <li>
        Where am I application
       </li>
       <li>
        Where am I application
       </li>
      </ul>
      <h2>
       Address Book framework
      </h2>
      <ul>
       <li>
        Overview of Address Book API
       </li>
      </ul>
      <h2>
       Map Kit Framework
      </h2>
      <ul>
       <li>
        Overview of map kit programming
       </li>
       <li>
        Where am I app
       </li>
      </ul>
      <h2>
       Web Services Interaction I-Pad Basics
      </h2>
      <ul>
       <li>
        Overview of IPad App
       </li>
       <li>
        UI Pop Over Controller
       </li>
       <li>
        UI Split View Controller
       </li>
      </ul>
      <h2>
       Memory management Tools
      </h2>
      <ul>
       <li>
        Reference Counting
       </li>
       <li>
        object ownership
       </li>
       <li>
        Auto release pools
       </li>
       <li>
        Retain count methods
       </li>
      </ul>
      <h2>
       Social Sharing
      </h2>
      <ul>
       <li>
        Facebook
       </li>
       <li>
        Linked-in
       </li>
       <li>
        Twitter
       </li>
      </ul>
      <h2>
       Advanced Feature
      </h2>
      <ul>
       <li>
        Facebook Integration
       </li>
       <li>
        Twitter Integration
       </li>
       <li>
        Linked-In Integration
       </li>
       <li>
        Google Plus Integration
       </li>
      </ul>
      <h2>
       Storyboard
      </h2>
      <ul>
       <li>
        How to work StoryboardHow to work Layout in Storyboard
       </li>
      </ul>
      <h2>
       Performance and Debugging
      </h2>
      <ul>
       <li>
        Find and fix problems
       </li>
       <li>
        Elimination hot spots and slow code
       </li>
       <li>
        Instruments utility
       </li>
       <li>
        Show data in Log Panel
       </li>
      </ul>
      <h2>
       SQL Queries in IOS
      </h2>
      <ul>
       <li>
        Run Simple Queries
       </li>
       <li>
        How to configure the database
       </li>
       <li>
        Joins
       </li>
      </ul>
      <h2>
       Creating Auto Layouts
      </h2>
      <ul>
       <li>
        After learning about the different ways you can make an app's UI adaptive to the device it is run on,
you will modify the previous currency converter app and make it adaptive so that it can run well and
look great on any IOS device (i.e. iPad, iPhone, or iPod)
       </li>
      </ul>
      <h2>
       Web Services
      </h2>
      <ul>
       <li>
        Create own custom web services &amp; integrate these web service in IOS app
       </li>
      </ul>
      <h2>
       Implement Third Party Libraries
      </h2>
      <ul>
       <li>
        FMDB
       </li>
       <li>
        Almo Fire
       </li>
      </ul>
      <h2>
       App Store Basics
      </h2>
      <ul>
       <li>
        Overview of Deeloper/Provisional certificates
       </li>
       <li>
        Prepare App store build
       </li>
      </ul>
      <h2>
       Final Projects
      </h2>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="swift.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="29 + 78 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="107">
       <input name="url" type="hidden" value="/iphonetraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>