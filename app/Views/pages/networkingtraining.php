<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     NETWORKING SIX MONTHS
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     NETWORKING SIX MONTHS
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Dreaming to be a networking genius? We at DUCAT have the exact training material you have been looking for.

DUCAT is a well esteemed classroom that provides networking training programs for fresh IT graduates. We provide live projects for candidates that greatly help their prospects in the future job run. Our course is structured for six months and includes globally renowned certifications like CCNA, CCNP, MCSE and MCITP. The highly upgraded practical labs at DUCAT include the latest switches, servers and routers. Candidate will be helped out by highly experienced and certified faculty. Students have the option to choose between certifications and we guarantee that they not only will pass the certifications but will be perfectly ready for the competitive job market. DUCAT also provides customized workshops and training programs that could be accommodated as per your needs and limitations. With our accommodative networking training packages, you will have the perfect opportunity to explore your interest and abilities while enjoying the benefits of international certifications. With thorough knowledge in hand, you certainly will be among the first preferences for the employer!
     </p>
     <div class="contentAcc">
      <h2>
       N+
      </h2>
      <ul>
       <li>
        Introduction to Datacomm and Networking
       </li>
       <li>
        Datacomm &amp; Networking fundamentals
       </li>
       <li>
        OSI reference Model
       </li>
       <li>
        LAN fundamentals
       </li>
       <li>
        WAN fundamentals
       </li>
       <li>
        Network Devices
       </li>
       <li>
        Firewall &amp; Application Gateways
       </li>
       <li>
        Designing &amp; Implementing Structured Cabling
       </li>
       <li>
        Ethernet standards
       </li>
       <li>
        FDDI &amp; token Ring Standards
       </li>
       <li>
        Digital Subscriber Line
       </li>
       <li>
        Deploying a Networking and security Measures
       </li>
       <li>
        LAN troubleshooting techniques and Tools
       </li>
       <li>
        TCP/IP Model
       </li>
       <li>
        IP Addressing
       </li>
      </ul>
      <h2>
       CCNA
      </h2>
      <ul>
       <li>
        IPV4 Addressing
       </li>
       <li>
        IPV4 Saving Techniques
       </li>
       <li>
        Cisco Router Introduction Theory
       </li>
       <li>
        IP Routing
       </li>
       <li>
        LAB
       </li>
       <li>
        Dynamic Protocal &amp; Dynamic Routing
       </li>
       <li>
        OSPF(Open Shortest Path First)
       </li>
       <li>
        Type Of Table In OSPF
       </li>
       <li>
        LAB
       </li>
       <li>
        Helpful Command Use In LAB
       </li>
       <li>
        EIGRP(Enhanced Interior Gateway Routing Protocal)
       </li>
       <li>
        LAB
       </li>
       <li>
        Helpful Command Use In LAB
       </li>
       <li>
        How To Manage Cisco Devices
       </li>
       <li>
        Discribe Following Terms
       </li>
       <li>
        LAB
       </li>
       <li>
        IP Services DHCP
       </li>
       <li>
        Packet Filtering Via ACL
       </li>
       <li>
        NTP
       </li>
       <li>
        Advanced Topics
       </li>
       <li>
        LAB
       </li>
       <li>
        LAN Switching Technology
       </li>
       <li>
        Bridge &amp; its Function
       </li>
       <li>
        VLAN &amp; its Function
       </li>
       <li>
        STP Convergence Components
       </li>
       <li>
        STP Convergence Steps
       </li>
       <li>
        LAB
       </li>
       <li>
        Networking Device Security
       </li>
       <li>
        WAN Technologies
       </li>
       <li>
        Introduction of PPP &amp; Its Feature
       </li>
       <li>
        Frame-Relay Logical Topologies
       </li>
       <li>
        LAB
       </li>
       <li>
        Ipv6 Addressing
       </li>
       <li>
        Ipv6 Packet Type
       </li>
       <li>
        LAB
       </li>
       <li>
        OSI(Open System Interconnection)&amp; TCP/IP
       </li>
      </ul>
      <h2>
       MCSA
      </h2>
      <ul>
       <li>
        Installing and Configuring Windows Server 2012
       </li>
      </ul>
      <h2>
       Administering Windows Server 2012
      </h2>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>