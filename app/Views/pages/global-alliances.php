<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     GLOBAL ALLIANCES
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="#">
     GLOBAL ALLIANCES
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9 text-center">
    <h3 style="text-align:left">
     GLOBAL ALLIANCES AT DUCAT
    </h3>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
    <p>
     <img src="../images/global-alliances1.jpg"/>
    </p>
    <br/>
    <div style="text-align:left">
     <p>
      Alliances have always proved to be the best for both the ends i.e. our clients and trainees, with the motto to benefit our trainees in all possible ways we focus on every aspect. We at Ducat try to maximize the quality of our services and work to make you stand out from others in order to maximize the value of your work. We are unlike those alliances who promise a lot but gives less than that rather we are the ones who always try to give you more than promised with the help of the strong network we have with our partners because of which we assure the quality of our services because our collaboration has delivered well-trained trainees to the companies.
     </p>
     <p>
      With the rise in the competition where every other person stands above the other, it has become important to beat the crowd in respect of skills and knowledge to ensure the success. It is not easy to do this unless you challenge yourself and train yourself very hard and ultimately the point is How to do it. You always need the help of experts who can train you in every way to make you an expert and we at Ducat are always ready to answer all your questions and to give you the best training you can get anywhere.
     </p>
     <h4>
      WHAT IS THE NEED OF THESE GLOBAL ALLIANCE PROGRAMS?
     </h4>
     <p>
      Global Alliance is a technique of combining the quality of the program and the demand of the corporate world so that the tension raised by the rise in competition can be reduced to the possible extent. Mainly the new comers find it difficult to assure their position in front of the experience holders but with the help of the programs, we provide our trainees do not find it difficult to book a place for them. We believe in making a strong network with our clients and the trainees as well that is why we provide the training according to the demand of the corporate world.
     </p>
     <h4>
      HOW WE STAND OUT FROM OTHERS?
     </h4>
     <p>
      You must be thinking we are just like others who promise a lot but eventually does not provide anything beneficial. We understand your concern and that is why we are always ready to clear your doubts in all possible ways. Not only us but our partners like Oracle and RedHat wants you to get the best which is why we poses a joint objective to help our trainees to become expert in their respective fields. We train our trainees in all possible ways to maximize the value of their technology investments by providing the best services with the help of our partners.
     </p>
     <h4>
      HOW DOES THE PROGRAMS WE OFFER ARE GOOD FOR YOU?
     </h4>
     <p>
      We provide the necessary training on our partners technology products like RedHat Linux 7.0 or Oracle 11G DATA GUARD because our partner programs are made to give necessary support and resources to our trainees by the help of specifically designed programs, which results in a good command, and hold over the required programs.
     </p>
    </div>
   </div>
   <div class="col-md-3">
    <form class="searchForm">
     <input placeholder="Search" type="text"/>
    </form>
    <div class="widgetArea">
     <h5>
      CONTACT INFO
     </h5>
     <address>
      <span class="address">
       A - 43 &amp; A - 52 Sector - 16,
       <br/>
       Noida (U.P) (Near McDonalds)
      </span>
      <br>
       <span class="phone">
        <strong>
         Phone:
        </strong>
        0120-4646464, +91- 9871055180
       </span>
       <br/>
       <span class="email">
        <strong>
         E-Mail:
        </strong>
        <a href="mailto:info@ducatindia.com">
         info@ducatindia.com
        </a>
       </span>
       <br/>
       <span class="web">
        <strong>
         Web:
        </strong>
        <a href="http://www.ducatindia.com/">
         http://www.ducatindia.com/
        </a>
       </span>
      </br>
     </address>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section class="text-center" id="offices">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <h5>
     CORPORATE OFFICE NOIDA:
     <span>
      0120 - 4646464
     </span>
    </h5>
    <p>
     GR.NOIDA:
     <span>
      0120-4345190
     </span>
     GHAZIABAD:
     <span>
      0120-4835400
     </span>
     FARIDABAD:
     <span>
      0129-4150605
     </span>
     GURGAON:
     <span>
      0124-4219095
     </span>
     JAIPUR:
     <span>
      0141-2550077
     </span>
    </p>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>