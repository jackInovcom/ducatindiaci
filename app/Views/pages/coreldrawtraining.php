<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     CORELDRAW
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     CORELDRAW
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      THE TECHNICAL COURSE THAT MAKES YOU SHINE
     </h4>
     <p>
      Computer runs the human race, announcement and marketplace today. People should be conscious of all the latest expertise and software function running in the market, to take hold of the best job offers. DUCAT understands the need of the market and has intended its programmers of training accordingly. They offer a broad range of diverse courses which not only develops individuality but also shapes their future.
     </p>
     <p>
      This technological idea is in high command in market, companies are keen to hire candidates who are talented in these criteria and hence the job seeker will be one step higher. A learner gets the probability to learn all technological details with DUCAT and become an authority in no time.
     </p>
     <p>
      The DUCAT introduced an exclusive method in its courses that helps a person to develop skills according to once need. They industrial customer communication, English talking, grammar, articulation and more for one individual. One will experience at parity with the industry and will have self-confidence to face any future challenges.
     </p>
     <p>
      The teaching staffs and teaching programs are intended in such a way by DUCAT that it helps people in every aspect. The best teaching staffs are chosen for the teaching so that astonishing solutions can be provided to learners in least cost.
     </p>
     <div class="contentAcc">
      <h2>
       CorelDRAW Basic and Interface Getting   Started
      </h2>
      <ul>
       <li>
        Exploring the CorelDraw Screen
       </li>
       <li>
        File Management
       </li>
       <li>
        Setting Up the Page
       </li>
      </ul>
      <h2>
       Moving Around and Viewing Drawings
      </h2>
      <ul>
       <li>
        Moving Around the Current Page
       </li>
       <li>
        Viewing Modes
       </li>
       <li>
        Inserting and Deleting Pages
       </li>
       <li>
        Changing Page
       </li>
      </ul>
      <h2>
       Customizing Options
      </h2>
      <ul>
       <li>
        Using Multiple Workspaces
       </li>
       <li>
        Customizing the Toolbars
       </li>
       <li>
        Using Shortcuts
       </li>
       <li>
        Saving Defaults
       </li>
       <li>
        Setting File Backups
       </li>
      </ul>
      <h2>
       Creation And Manipulation Drawing and Shaping   Objects
      </h2>
      <ul>
       <li>
        Drawing and Shaping Tools
       </li>
       <li>
        Using the Freehand Tool
       </li>
       <li>
        Drawing Lines and Polylines
       </li>
       <li>
        Drawing Freehand Polygons
       </li>
       <li>
        Drawing Perfect Shapes
       </li>
       <li>
        Reshaping Lines and Polylines
       </li>
       <li>
        Drawing Curves
       </li>
       <li>
        Reshaping Curves
       </li>
       <li>
        Drawing Rectangles
       </li>
       <li>
        Drawing Circles
       </li>
      </ul>
      <h2>
       Selecting &amp; Manipulating Objects
      </h2>
      <ul>
       <li>
        Selecting and Deselecting Objects
       </li>
       <li>
        Moving Objects
       </li>
       <li>
        Copying and Deleting Objects
       </li>
       <li>
        Deleting Objects
       </li>
       <li>
        Sizing Objects
       </li>
      </ul>
      <h2>
       Transforming Objects
      </h2>
      <ul>
       <li>
        Mirroring Objects
       </li>
       <li>
        Rotating and Skewing Objects
       </li>
       <li>
        Using Transform Objects
       </li>
      </ul>
      <h2>
       Outlining &amp; Filling Objects
      </h2>
      <ul>
       <li>
        Eyedropper and Paint bucket Tool
       </li>
       <li>
        The Outline Tool
       </li>
       <li>
        Choosing Outline Thickness
       </li>
       <li>
        Choosing Outline Colors
       </li>
       <li>
        Using Fill Tool
       </li>
       <li>
        Uniform Fill, Fountain Fill, Pattern Fill
       </li>
       <li>
        Interactive Mesh Fill
       </li>
       <li>
        Copying Attributes
       </li>
       <li>
        Setting Outline and Fill Defaults
       </li>
      </ul>
      <h2>
       Arranging Objects
      </h2>
      <ul>
       <li>
        Arranging Objects
       </li>
       <li>
        Grouping and Ungrouping Objects
       </li>
       <li>
        Using Guidelines
       </li>
       <li>
        Using Snap To
       </li>
       <li>
        Aligning Objects
       </li>
       <li>
        Group and Child Objects
       </li>
       <li>
        Welding Objects
       </li>
       <li>
        Using Intersection
       </li>
       <li>
        Using Trim
       </li>
      </ul>
      <h2>
       Using Layers
      </h2>
      <ul>
       <li>
        About Layers
       </li>
       <li>
        Editing Layers
       </li>
       <li>
        Setting Up a Master Layer
       </li>
       <li>
        Moving, Copying, and Layer
       </li>
       <li>
        Reordering Layers
       </li>
       <li>
        Using the Object Manager
       </li>
      </ul>
      <h2>
       PHP Syntax
      </h2>
      <ul>
       <li>
        Syntax
       </li>
       <li>
        variable
       </li>
       <li>
        Datatype
       </li>
      </ul>
      <h2>
       Working with special Effects and Texts Special   Effects
      </h2>
      <ul>
       <li>
        Drawing with The Artistic Media Tool
       </li>
       <li>
        Shaping an Object with an Envelope
       </li>
       <li>
        Extruding an Object
       </li>
       <li>
        Blending two Objects
       </li>
       <li>
        Using the Lens Effect
       </li>
       <li>
        Adding Perspectives
       </li>
       <li>
        Using Power Clips
       </li>
       <li>
        Applying Contours
       </li>
       <li>
        Applying Drop Shadows
       </li>
       <li>
        Using Interactive Transparencies
       </li>
       <li>
        Applying Mesh Fills
       </li>
      </ul>
      <h2>
       Working with Text
      </h2>
      <ul>
       <li>
        The Text Tool
       </li>
       <li>
        Creating Artistic Text
       </li>
       <li>
        Editing Text
       </li>
       <li>
        Formatting Text
       </li>
       <li>
        Setting Text Options
       </li>
       <li>
        Creating Paragraph Text
       </li>
       <li>
        Setting Indents Using the Ruler
       </li>
       <li>
        Importing Text
       </li>
       <li>
        Using the Spell Checker
       </li>
      </ul>
      <h2>
       Working with Paragraph
      </h2>
      <ul>
       <li>
        Text Implementing Color
       </li>
       <li>
        Management
       </li>
       <li>
        Creating Custom Color Palettes
       </li>
       <li>
        Choosing a Color Using Color
       </li>
       <li>
        Harmonies
       </li>
       <li>
        Applying Colors Using the Color
       </li>
       <li>
        Docker
       </li>
       <li>
        Automatically Creating Color Styles
       </li>
       <li>
        Importing and Sizing Paragraph Text
       </li>
       <li>
        Flowing Text Between Frames
       </li>
       <li>
        Formatting Paragraph Frames
       </li>
       <li>
        Wrapping Paragraph Text Around
       </li>
       <li>
        Objects
       </li>
       <li>
        Applying Drop Caps
       </li>
       <li>
        Typing Text into Objects
       </li>
      </ul>
      <h2>
       Special Text Effects
      </h2>
      <ul>
       <li>
        Fitting Text to a Path
       </li>
       <li>
        Converting Text to Curves
       </li>
       <li>
        Creating Blended Text Shadows
       </li>
       <li>
        Special Text Effects
       </li>
       <li>
        Jumpy Text
       </li>
       <li>
        Neon Text
       </li>
       <li>
        Glowing Text
       </li>
       <li>
        Chrome Text
       </li>
       <li>
        Bevel Text
       </li>
       <li>
        Creating Enveloped Text
       </li>
      </ul>
      <h2>
       Using Symbols and Clipart
      </h2>
      <ul>
       <li>
        Inserting Text Symbols
       </li>
       <li>
        Adding Clipart
       </li>
       <li>
        Modifying Clipart
       </li>
      </ul>
      <h2>
       Working with Bitmaps
      </h2>
      <ul>
       <li>
        What is a Bitmaps
       </li>
       <li>
        Importing Bitmap Options
       </li>
       <li>
        Adjusting Color
       </li>
       <li>
        Hiding Certain Colors in a Bitmap
       </li>
       <li>
        Applying Special Bitmap Effects
       </li>
       <li>
        Creating Web Images
       </li>
       <li>
        Advanced GIF Options
       </li>
      </ul>
      <h2>
       Page layout, printing Exporting and Advanced Features Special   Page Layouts
      </h2>
      <ul>
       <li>
        Creating a Greeting Card
       </li>
       <li>
        Print Previewing the Layout
       </li>
       <li>
        Creating Labels
       </li>
      </ul>
      <h2>
       Printing
      </h2>
      <ul>
       <li>
        Print Options
       </li>
       <li>
        Print Previewing
       </li>
      </ul>
      <h2>
       Exporting Drawings
      </h2>
      <ul>
       <li>
        Exporting to Graphic Formats
       </li>
       <li>
        Copy and Pasting Into Other
       </li>
       <li>
        Applications
       </li>
      </ul>
      <h2>
       Using Styles and Templates
      </h2>
      <ul>
       <li>
        About Styles and Templates
       </li>
       <li>
        Creating a style
       </li>
       <li>
        Applying a Style
       </li>
       <li>
        Copying Properties
       </li>
      </ul>
      <h2>
       Custom Creation Tools
      </h2>
      <ul>
       <li>
        Creating Custom Patterns
       </li>
       <li>
        Managing and Using Symbols
       </li>
      </ul>
      <h2>
       Using Corel Trace
      </h2>
      <ul>
       <li>
        Types of Graphic Formats
       </li>
       <li>
        About Corel Trace
       </li>
       <li>
        Tracing Image
       </li>
       <li>
        Importing Traced Files into CorelDraw
       </li>
       <li>
        Special Trace Effects
       </li>
      </ul>
      <h2>
       Using Corel R.A.V.E.
      </h2>
      <ul>
       <li>
        About Corel RAVE
       </li>
       <li>
        Playing sample RAVE
       </li>
       <li>
        Performing the five steps necessary to
       </li>
       <li>
        create RAVE animations
       </li>
       <li>
        Working with the Timeline Docker
       </li>
       <li>
        Tweening objects to create animation
       </li>
       <li>
        Exporting to Macromedia Flash
       </li>
       <li>
        Format
       </li>
       <li>
        Publishing to the web Create web
       </li>
       <li>
        Rollovers
       </li>
       <li>
        Inserting Hyperlinks
       </li>
       <li>
        Creating Sprites and adding
       </li>
       <li>
        behaviors
       </li>
       <li>
        Creating interactive movies
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="coreldrawtraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="64 + 55 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="119">
       <input name="url" type="hidden" value="/coreldrawtraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>