<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     ADVANCED MICROSOFT EXCEL
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     ADVANCED MICROSOFT EXCEL
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <h4>
      ADVANCED MICROSOFT EXCEL 2013 TRAINING!
     </h4>
     <p>
      The Microsoft Excel is a spreadsheet application that is primarily used for dealing with complex data manipulation and analysis related tasks. The application effectively helps in turning vast amounts of raw data into well-organized meaningful information in minimal timeframes, thereby expediting the overall quality and delivery of work. Appropriate knowledge about the optimal use of the Microsoft Excel software considerably adds to one's productivity at the workplace, and this is why advanced level training on Microsoft Excel 2013 is considered to be the need of today for professionals involving Microsoft Excel as a part of their daily work routine. Here is YOUR chance to Learn the Microsoft Excel Tools and Techniques by enrolling in our Hands-on Advanced Microsoft Excel Training workshops where you get to learn the Microsoft Excel formulas, shortcuts, macros and much more that help raise your efficiency and effectiveness at work.
     </p>
     <div class="contentAcc">
      <h2>
       Advanced Excel Course - Overview of the Basics of Excel
      </h2>
      <ul>
       <li>
        Customizing common options in Excel
       </li>
       <li>
        Absolute and relative cells
       </li>
       <li>
        Protecting and un-protecting worksheets and cells
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Working with Functions
      </h2>
      <ul>
       <li>
        Writing conditional expressions (using IF)
       </li>
       <li>
        Using logical functions (AND, OR, NOT)
       </li>
       <li>
        Using lookup and reference functions (VLOOKUP, HLOOKUP, MATCH, INDEX)
       </li>
       <li>
        VlookUP with Exact Match, Approximate Match
       </li>
       <li>
        Nested VlookUP with Exact Match
       </li>
       <li>
        VlookUP with Tables, Dynamic Ranges
       </li>
       <li>
        Nested VlookUP with Exact Match
       </li>
       <li>
        Using VLookUP to consolidate Data from Multiple Sheets
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Data Validations
      </h2>
      <ul>
       <li>
        Specifying a valid range of values for a cell
       </li>
       <li>
        Specifying a list of valid values for a cell
       </li>
       <li>
        Specifying custom validations based on formula for a cell
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Working with Templates
      </h2>
      <ul>
       <li>
        Designing the structure of a template
       </li>
       <li>
        Using templates for standardization of worksheets
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Sorting and Filtering Data
      </h2>
      <ul>
       <li>
        Sorting tables
       </li>
       <li>
        Using multiple-level sorting
       </li>
       <li>
        Using custom sorting
       </li>
       <li>
        Filtering data for selected view (AutoFilter)
       </li>
       <li>
        Using advanced filter options
       </li>
      </ul>
      <h2>
       <li>
        Advanced Excel Course - Working with Reports
       </li>
      </h2>
      <ul>
       <li>
        Creating subtotals
       </li>
       <li>
        Multiple-level subtotals
       </li>
       <li>
        Creating Pivot tables
       </li>
       <li>
        Formatting and customizing Pivot tables
       </li>
       <li>
        Using advanced options of Pivot tables
       </li>
       <li>
        Pivot charts
       </li>
       <li>
        Consolidating data from multiple sheets and files using Pivot tables
       </li>
       <li>
        Using external data sources
       </li>
       <li>
        Using data consolidation feature to consolidate data
       </li>
       <li>
        Show Value As ( % of Row, % of Column, Running Total, Compare with Specific Field)
       </li>
       <li>
        Viewing Subtotal under Pivot
       </li>
       <li>
        Creating Slicers ( Version 2010 &amp; Above)
       </li>
      </ul>
      <h2>
       Advanced Excel Course - More Functions
      </h2>
      <ul>
       <li>
        Date and time functions
       </li>
       <li>
        Text functions
       </li>
       <li>
        Database functions
       </li>
       <li>
        Power Functions (CountIf, CountIFS, SumIF, SumIfS)
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Formatting
      </h2>
      <ul>
       <li>
        Using auto formatting option for worksheets
       </li>
       <li>
        Using conditional formatting option for rows, columns and cells
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Macros
      </h2>
      <ul>
       <li>
        Relative &amp; Absolute Macros
       </li>
       <li>
        Editing Macro's
       </li>
      </ul>
      <h2>
       Advanced Excel Course - WhatIf Analysis
      </h2>
      <ul>
       <li>
        Goal Seek
       </li>
       <li>
        Data Tables
       </li>
       <li>
        Scenario Manager
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Charts
      </h2>
      <ul>
       <li>
        Using Charts
       </li>
       <li>
        Formatting Charts
       </li>
       <li>
        Using 3D Graphs
       </li>
       <li>
        Using Bar and Line Chart together
       </li>
       <li>
        Using Secondary Axis in Graphs
       </li>
       <li>
        Sharing Charts with PowerPoint / MS Word, Dynamically
       </li>
       <li>
        (Data Modified in Excel, Chart would automatically get updated)
       </li>
      </ul>
      <h2>
       Advanced Excel Course - New Features Of Excel
      </h2>
      <ul>
       <li>
        Sparklines, Inline Charts, data Charts
       </li>
       <li>
        Overview of all the new features
       </li>
      </ul>
      <h2>
       Advanced Excel Course - Final Assignment
      </h2>
      <ul>
       <li>
        The Final Assignment would test contains questions to be solved at the end of the Course
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
</section>


<?php echo view('includes/footer.php'); ?>