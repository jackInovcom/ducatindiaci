<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     INDUSTRIAL TRAINING
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     INDUSTRIAL TRAINING
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <h3>
     INDUSTRIAL TRAINING AT DUCAT
    </h3>
    <div class="separator short">
     <div class="separator_line">
     </div>
    </div>
    <div class="coursesArea">
     <br/>
     <p>
      <img alt="corporate-training" class="img-responsive" src="../images/banner-industrial-training.jpg"/>
     </p>
     <br/>
     <h4>
      THE NEED FOR INDUSTRIAL TRAINING
     </h4>
     <p>
      Who says only the people who do not have much knowledge only needs extra assistance? I say, no. It is not necessary that the people that are not good with practical skills or lag somewhere in knowledge only needs people to help them because even the experts of industries needs people that can bring them out of the problems they face. So, who are these people that people even experts? These professional industrial training institutes and consultancies are always ready to help those who are in need.
     </p>
     <p>
      These institutes and consultancies do not only provide the necessary training but they also provide the best solution to their clients. That is why, they are so famous amongst the industrial sector because whether is the financial issue or something like technical problem they provide the best they can with the help of their experts and mind it their experts are not saying experts but they are good at bugging and debugging things.
     </p>
     <p>
      The people who do not need anyone�s help goes to them when are in technical problem because they know the answer which they will not get from anywhere will get it here and Ducat is known for its services.
     </p>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/id1.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        VOCATIONAL TRAINING
       </h3>
       <p class="paraText">
        We at, Ducat  offer the vocational training in a number of courses including Java, Oracle, .Net, PHP, J2EE, Struts and Strings. All of these courses are offered in real time environment along with the practical knowledge of the subjects. The students can test their skills through the live projects.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/id2.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        4-6 WEEKS TRAINING
       </h3>
       <p class="paraText">
        During this training, the students are exposed to live projects, which are running in the college campus or offsite. The students can learn and implement them in real time environment.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/id3.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        SIX MONTHS TRAINING
       </h3>
       <p class="paraText">
        If you opt for six-month training, the students are given the exposure to the live projects. Besides that, we impart knowledge in students in such a manner that they are capable of taking up interviews in an effective manner.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/id4.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        LIVE PROJECT BASED TRAINING
       </h3>
       <p class="paraText">
        We also work as consulting agencies and offer live project training to our students where you can get all sorts of technical solutions. If you have made any application and everything is correct. However, only one thing may create some problem, which is out of your knowledge, and you have already lot of time in debugging it but you did not get any satisfactory result so in this case, you can ask for the help from these consulting agencies and they will get you out of this.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
     <div class="serviceIcon">
      <div class="servicePic">
       <img alt="" src="../images/id5.png"/>
      </div>
      <div class="serviceContent">
       <h3>
        ADVANCED INDUSTRIAL TRAINING AT DUCAT
       </h3>
       <p class="paraText">
        At Ducat  industrial training programs provide training on advanced courses so that one can get the necessary knowledge to develop any program or app. It is not necessary that the person who wants to work on any application should have complete knowledge about developing it because unless you take training it is not possible to be expert of it and developing an app is not difficult but developing advanced and that too flawless app is really difficult. Therefore, we help by providing training of the advanced courses so that you can go hand in hand with the demand of industry.
       </p>
      </div>
      <div class="clearfix">
      </div>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <form class="searchForm">
     <input placeholder="Search" type="text"/>
    </form>
    <div class="widgetArea">
     <h5>
      CONTACT INFO
     </h5>
     <address>
      <span class="address">
       A - 43 &amp; A - 52 Sector - 16,
       <br/>
       Noida (U.P) (Near McDonalds)
      </span>
      <br>
       <span class="phone">
        <strong>
         Phone:
        </strong>
        0120-4646464, +91- 9871055180
       </span>
       <br/>
       <span class="email">
        <strong>
         E-Mail:
        </strong>
        <a href="mailto:info@ducatindia.com">
         info@ducatindia.com
        </a>
       </span>
       <br/>
       <span class="web">
        <strong>
         Web:
        </strong>
        <a href="http://www.ducatindia.com/">
         http://www.ducatindia.com/
        </a>
       </span>
      </br>
     </address>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section class="text-center" id="offices">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <h5>
     CORPORATE OFFICE NOIDA:
     <span>
      0120 - 4646464
     </span>
    </h5>
    <p>
     GR.NOIDA:
     <span>
      0120-4345190
     </span>
     GHAZIABAD:
     <span>
      0120-4835400
     </span>
     FARIDABAD:
     <span>
      0129-4150605
     </span>
     GURGAON:
     <span>
      0124-4219095
     </span>
     JAIPUR:
     <span>
      0141-2550077
     </span>
    </p>
   </div>
   <!-- End Of Col MD 12 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>