<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h1>
     BIG DATA TRAINING
    </h1>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     BIG DATA
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      Big data usually includes data sets with sizes beyond the ability of commonly used software tools to capture, curate, manage, and process data within a tolerable elapsed time. Big data philosophy encompasses unstructured, semi-structured and structured data, however the main focus is on unstructured data. Big data "size" is a constantly moving target, as of 2012 ranging from a few dozen terabytes to many exabytes of data.
     </p>
     <div class="contentAcc">
      <h2>
       COURSE CURRICULUM
      </h2>
      <h2>
       Pre-requisites for the Big Data Hadoop Training Course?
      </h2>
      <p>
       There will be no pre-requisites but Knowledge of Java/ Python, SQL, Linux will be beneficial, but not
mandatory. Ducat provides a crash course for pre-requisites required to initiate Big Data training.
      </p>
      <h2>
       Apache Hadoop on AWS Cloud
      </h2>
      <p>
       This module will help you understand how to configure Hadoop Cluster on AWS Cloud:
      </p>
      <ul>
       <li>
        Introduction to Amazon Elastic MapReduce
       </li>
       <li>
        AWS EMR Cluster
       </li>
       <li>
        AWS EC2 Instance: Multi Node Cluster Configuration
       </li>
       <li>
        AWS EMR Architecture
       </li>
       <li>
        Web Interfaces on Amazon EMR
       </li>
       <li>
        Amazon S3
       </li>
       <li>
        Executing MapReduce Job on EC2 &amp; EMR
       </li>
       <li>
        Apache Spark on AWS, EC2 &amp; EMR
       </li>
       <li>
        Submitting Spark Job on AWS
       </li>
       <li>
        Hive on EMR
       </li>
       <li>
        Available Storage types: S3, RDS &amp; DynamoDB
       </li>
       <li>
        Apache Pig on AWS EMR
       </li>
       <li>
        Processing NY Taxi Data using SPARK on Amazon EMR[Type text]
       </li>
      </ul>
      <h2>
       Learning Big Data and Hadoop
      </h2>
      This module will help you understand Big Data:
      <ul>
       <li>
        Common Hadoop ecosystem components
       </li>
       <li>
        Hadoop Architecture
       </li>
       <li>
        HDFS Architecture
       </li>
       <li>
        Anatomy of File Write and Read
       </li>
       <li>
        How MapReduce Framework works
       </li>
       <li>
        Hadoop high level Architecture
       </li>
       <li>
        MR2 Architecture
       </li>
       <li>
        Hadoop YARN
       </li>
       <li>
        Hadoop 2.x core components
       </li>
       <li>
        Hadoop Distributions
       </li>
       <li>
        Hadoop Cluster Formation
       </li>
      </ul>
      <h2>
       Hadoop Architecture and HDFS
      </h2>
      <p>
       This module will help you to understand Hadoop &amp; HDFS ClusterArchitecture:
      </p>
      <ul>
       <li>
        Configuration files in Hadoop Cluster (FSimage &amp; editlog file)
       </li>
       <li>
        Setting up of Single &amp; Multi node Hadoop Cluster
       </li>
       <li>
        HDFS File permissions
       </li>
       <li>
        HDFS Installation &amp; Shell Commands
       </li>
       <li>
        Deamons of HDFS
       </li>
       <ul>
        <li>
         Node Manager
        </li>
        <li>
         Resource Manager
        </li>
        <li>
         NameNode
        </li>
        <li>
         DataNode
        </li>
        <li>
         Secondary NameNode
        </li>
        <li>
         YARN Deamons
        </li>
        <li>
         HDFS Read &amp; Write Commands
        </li>
        <li>
         NameNode &amp; DataNode Architecture
        </li>
        <li>
         HDFS Operations
        </li>
        <li>
         Hadoop MapReduce Job
        </li>
        <li>
         Executing MapReduce Job
        </li>
       </ul>
      </ul>
      <h2>
       Hadoop MapReduce Framework
      </h2>
      <p>
       This module will help you to understand Hadoop MapReduce framework:
      </p>
      <ul>
       <li>
        How MapReduce works on HDFS data sets
       </li>
       <li>
        MapReduce Algorithm
       </li>
       <li>
        MapReduce Hadoop Implementation
       </li>
       <li>
        Hadoop 2.x MapReduce Architecture
       </li>
       <li>
        MapReduce Components
       </li>
       <li>
        YARN Workflow
       </li>
       <li>
        MapReduce Combiners
       </li>
       <li>
        MapReduce Partitioners
       </li>
       <li>
        MapReduce Hadoop Administration
       </li>
       <li>
        MapReduce APIs
       </li>
       <li>
        Input Split &amp; String Tokenizer in MapReduce
       </li>
       <li>
        MapReduce Use Cases on Data sets
       </li>
      </ul>
      <h2>
       Advanced MapReduce Concepts
      </h2>
      <p>
       This module will help you to learn:
      </p>
      <ul>
       <li>
        Job Submission &amp; Monitoring
       </li>
       <li>
        Counters
       </li>
       <li>
        Distributed Cache
       </li>
       <li>
        Map &amp; Reduce Join
       </li>
       <li>
        Data Compressors
       </li>
       <li>
        Job Configuration
       </li>
       <li>
        Record Reader
       </li>
      </ul>
      <h2>
       Pig
      </h2>
      <p>
       This module will help you to understand Pig Concepts:
      </p>
      <ul>
       <li>
        Pig Architecture
       </li>
       <li>
        Pig Installation
       </li>
       <li>
        Pig Grunt shell
       </li>
       <li>
        Pig Running Modes
       </li>
       <li>
        Pig Latin Basics
       </li>
       <li>
        Pig LOAD &amp; STORE Operators[Type text]
       </li>
       <li>
        Diagnostic Operators
       </li>
       <ul>
        <li>
         DESCRIBE Operator
        </li>
        <li>
         EXPLAIN Operator
        </li>
        <li>
         ILLUSTRATE Operator
        </li>
        <li>
         DUMP Operator
        </li>
       </ul>
       <li>
        Grouping &amp; Joining
       </li>
       <ul>
        <li>
         GROUP Operator
        </li>
        <li>
         COGROUP Operator
        </li>
        <li>
         JOIN Operator
        </li>
        <li>
         CROSS Operator
        </li>
       </ul>
       <li>
        Combining &amp; Splitting
       </li>
       <ul>
        <li>
         UNION Operator
        </li>
        <li>
         SPLIT Operator
        </li>
       </ul>
       <li>
        Filtering
       </li>
       <ul>
        <li>
         FILTER Operator
        </li>
        <li>
         DISTINCT Operator
        </li>
        <li>
         FOREACH Operator
        </li>
       </ul>
       <li>
        Sorting
       </li>
       <ul>
        <li>
         ORDERBYFIRST
        </li>
        <li>
         LIMIT Operator
        </li>
       </ul>
       <li>
        Built in Fuctions
       </li>
       <ul>
        <li>
         EVAL Functions
        </li>
        <li>
         LOAD &amp; STORE Functions
        </li>
        <li>
         Bag &amp; Tuple Functions
        </li>
        <li>
         String Functions
        </li>
        <li>
         Date-Time Functions
        </li>
        <li>
         MATH Functions
        </li>
       </ul>
       <li>
        Pig UDFs (User Defined Functions)
       </li>
       <li>
        Pig Scripts in Local Mode
       </li>
       <li>
        Pig Scripts in MapReduce Mode
       </li>
       <li>
        Analysing XML Data using Pig
       </li>
       <li>
        Pig Use Cases (Data Analysis on Social Media sites, Banking, Stock Market &amp; Others)
       </li>
       <li>
        Analysing JSON data using Pig
       </li>
       <li>
        Testing Pig Sctipts
       </li>
      </ul>
      <h2>
       Hive
      </h2>
      <p>
       This module will build your concepts in learning:
      </p>
      <ul>
       <li>
        Hive Installation
       </li>
       <li>
        Hive Data types
       </li>
       <li>
        Hive Architecture &amp; Components
       </li>
       <li>
        Hive Meta Store
       </li>
       <li>
        Hive Tables(Managed Tables and External Tables)
       </li>
       <li>
        Hive Partitioning &amp; Bucketing
       </li>
       <li>
        Hive Joins &amp; Sub Query
       </li>
       <li>
        Running Hive Scripts
       </li>
       <li>
        Hive Indexing &amp; View
       </li>
       <li>
        Hive Queries (HQL); Order By, Group By, Distribute By, Cluster By, Examples
       </li>
       <li>
        Hive Functions: Built-in &amp; UDF (User Defined Functions)
       </li>
       <li>
        Hive ETL: Loading JSON, XML, Text Data Examples
       </li>
       <li>
        Hive Querying Data
       </li>
       <li>
        Hive Tables (Managed &amp; External Tables)
       </li>
       <li>
        Hive Used Cases
       </li>
       <li>
        Hive Optimization Techniques
       </li>
       <ul>
        <li>
         Partioning(Static &amp; Dynamic Partition) &amp; Bucketing
        </li>
        <li>
         Hive Joins &gt; Map + BucketMap + SMB (SortedBucketMap) + Skew
        </li>
        <li>
         Hive FileFormats ( ORC+SEQUENCE+TEXT+AVRO+PARQUET)
        </li>
        <li>
         CBO
        </li>
        <li>
         Vectorization
        </li>
        <li>
         Indexing (Compact + BitMap)
        </li>
        <li>
         Integration with TEZ &amp; Spark
        </li>
       </ul>
       <li>
        Hive SerDer ( Custom + InBuilt)
       </li>
       <li>
        Hive integration NoSQL (HBase + MongoDB + Cassandra)
       </li>
       <li>
        Thrift API (Thrift Server)
       </li>
       <li>
        UDF, UDTF &amp; UDAF
       </li>
       <li>
        Hive Multiple Delimiters
       </li>
       <li>
        XML &amp; JSON Data Loading HIVE.
       </li>
       <li>
        Aggregation &amp; Windowing Functions in Hive
       </li>
       <li>
        Hive Connect with Tableau
       </li>
      </ul>
      <h2>
       Sqoop
      </h2>
      <ul>
       <li>
        Sqoop Installation
       </li>
       <li>
        Loading Data form RDBMS using Sqoop
       </li>
       <li>
        Sqoop Import &amp; Import-All-Table
       </li>
       <li>
        Fundamentals &amp; Architecture of Apache Sqoop
       </li>
       <li>
        Sqoop Job
       </li>
       <li>
        Sqoop Codegen
       </li>
       <li>
        Sqoop Incremental Import &amp; Incremental Export
       </li>
       <li>
        Sqoop Merge
       </li>
       <li>
        Import Data from MySQL to Hive using Sqoop
       </li>
       <li>
        Sqoop: Hive Import
       </li>
       <li>
        Sqoop Metastore
       </li>
       <li>
        Sqoop Use Cases
       </li>
       <li>
        Sqoop- HCatalog Integration
       </li>
       <li>
        Sqoop Script
       </li>
       <li>
        Sqoop Connectors
       </li>
      </ul>
      <h2>
       Flume
      </h2>
      <p>
       This module will help you to learn Flume Concepts:
      </p>
      <ul>
       <li>
        Flume Introduction
       </li>
       <li>
        Flume Architecture
       </li>
       <li>
        Flume Data Flow
       </li>
       <li>
        Flume Configuration
       </li>
       <li>
        Flume Agent Component Types
       </li>
       <li>
        Flume Setup
       </li>
       <li>
        Flume Interceptors
       </li>
       <li>
        Multiplexing (Fan-Out), Fan-In-Flow
       </li>
       <li>
        Flume Channel Selectors
       </li>
       <li>
        Flume Sync Processors
       </li>
       <li>
        Fetching of Streaming Data using Flume (Social Media Sites: YouTube, LinkedIn, Twitter)
       </li>
       <li>
        Flume + Kafka Integration
       </li>
       <li>
        Flume Use Cases
       </li>
      </ul>
      <h2>
       KAFKA
      </h2>
      <p>
       This module will help you to learn Kafka concepts:
      </p>
      <ul>
       <li>
        Kafka Fundamentals
       </li>
       <li>
        Kafka Cluster Architecture
       </li>
       <li>
        Kafka Workflow
       </li>
       <li>
        Kafka Producer, Consumer Architecture
       </li>
       <li>
        Integration with SPARK
       </li>
       <li>
        Kafka Topic Architecture
       </li>
       <li>
        Zookeeper &amp; Kafka
       </li>
       <li>
        Kafka Partitions
       </li>
       <li>
        Kafka Consumer Groups
       </li>
       <li>
        KSQL (SQL Engine for Kafka)
       </li>
       <li>
        Kafka Connectors
       </li>
       <li>
        Kafka REST Proxy
       </li>
       <li>
        Kafka Offsets
       </li>
      </ul>
      <h2>
       Oozie
      </h2>
      <p>
       This module will help you to understand Oozie concepts:
      </p>
      <ul>
       <li>
        Oozie Introduction
       </li>
       <li>
        Oozie Workflow Specification
       </li>
       <li>
        Oozie Coordinator Functional Specification
       </li>
       <li>
        Oozie H-catalog Integration
       </li>
       <li>
        Oozie Bundle Jobs
       </li>
       <li>
        Oozie CLI Extensions
       </li>
       <li>
        Automate MapReduce, Pig, Hive, Sqoop Jobs using Oozie
       </li>
       <li>
        Packaging &amp; Deploying an Oozie Workflow Application
       </li>
      </ul>
      <h2>
       HBase
      </h2>
      <p>
       This module will help you to learn HBase Architecture:
      </p>
      <ul>
       <li>
        HBase Architecture, Data Flow &amp; Use Cases
       </li>
       <li>
        Apache HBase Configuration
       </li>
       <li>
        HBase Shell &amp; general commands
       </li>
       <li>
        HBase Schema Design
       </li>
       <li>
        HBase Data Model
       </li>
       <li>
        HBase Region &amp; Master Server
       </li>
       <li>
        HBase &amp; MapReduce
       </li>
       <li>
        Bulk Loading in HBase
       </li>
       <li>
        Create, Insert, Read Tables in HBase
       </li>
       <li>
        HBase Admin APIs
       </li>
       <li>
        HBase Security
       </li>
       <li>
        HBase vs Hive
       </li>
       <li>
        Backup &amp; Restore in HBase
       </li>
       <li>
        Apache HBase External APIs (REST, Thrift, Scala)
       </li>
       <li>
        HBase &amp; SPARK
       </li>
       <li>
        Apache HBase Coprocessors
       </li>
       <li>
        HBase Case Studies
       </li>
       <li>
        HBase Trobleshooting
       </li>
      </ul>
      <h2>
       Data Processing with Apache Spark
      </h2>
      <p>
       Spark executes in-memory data processing &amp; how Spark Job runs faster then Hadoop MapReduce Job.
Course will also help you understand the Spark Ecosystem &amp; it related APIs like Spark SQL, Spark
Streaming, Spark MLib, Spark GraphX &amp; Spark Core concepts as well.
This course will help you to understand Data Analytics &amp; Machine Learning algorithms applying to various
datasets to process &amp; to analyze large amount of data.
      </p>
      <ul>
       <li>
        Spark RDDs.
       </li>
       <li>
        Spark RDDs Actions &amp; Transformations.
       </li>
       <li>
        Spark SQL : Connectivity with various Relational sources &amp; its convert it into Data Frame using Spark SQL.
       </li>
       <li>
        Spark Streaming
       </li>
       <li>
        Understanding role of RDD
       </li>
       <li>
        Spark Core concepts : Creating of RDDs: Parrallel RDDs, MappedRDD, HadoopRDD, JdbcRDD.
       </li>
       <li>
        Spark Architecture &amp; Components.
       </li>
      </ul>
      <h2>
       BIG DATA PROJECTS
      </h2>
      <h2>
       Project #1: Working with MapReduce, Pig, Hive &amp; Flume
      </h2>
      <p>
       Problem Statement : Fetch structured &amp; unstructured data sets from various sources like Social Media
Sites, Web Server &amp; structured source like MySQL, Oracle &amp; others and dump it into HDFS and then
analyze the same datasets using PIG,HQL queries &amp; MapReduce technologies to gain proficiency in
Hadoop related stack &amp; its ecosystem tools.
      </p>
      <p>
       Data Analysis Steps in :
      </p>
      <ul>
       <li>
        Dump XML &amp; JSON datasets into HDFS.
       </li>
       <li>
        Convert semi-structured data formats(JSON &amp; XML) into structured format using Pig,Hive &amp; MapReduce.
       </li>
       <li>
        Push the data set into PIG &amp; Hive environment for further analysis.
       </li>
       <li>
        Writing Hive queries to push the output into relational database(RDBMS) using Sqoop.
       </li>
       <li>
        Renders the result in Box Plot, Bar Graph &amp; others using R &amp; Python integration with Hadoop.
       </li>
      </ul>
      <h2>
       Project #2: Analyze Stock Market Data
      </h2>
      Industry: Finance
      <p>
       Data : Data set contains stock information such as daily quotes ,Stock highest price, Stock opening price on
New York Stock Exchange.
Problem Statement: Calculate Co-variance for stock data to solve storage &amp; processing problems related to
huge volume of data.
      </p>
      <ul>
       <li>
        Positive Covariance, If investment instruments or stocks tend to be up or down during the same time
 periods, they have positive covariance.
       </li>
       <li>
        Negative Co-variance, If return move inversely,If investment tends to be up while other is down, this
 shows Negative Co-variance.
       </li>
      </ul>
      <h2>
       Project #3: Hive,Pig &amp; MapReduce with New York City Uber Trips
      </h2>
      <ul>
       <li>
        Problem Statement: What was the busiest dispatch base by trips for a particular day on entire month?
       </li>
       <li>
        What day had the most active vehicles.
       </li>
       <li>
        What day had the most trips sorted by most to fewest.
       </li>
       <li>
        Dispatching_Base_Number is the NYC taxi &amp; Limousine company code of that base that dispatched the
 UBER.
       </li>
       <li>
        active_vehicles shows the number of active UBER vehicles for a particular date &amp; company(base).
 Trips is the number of trips for a particular base &amp; date.
       </li>
      </ul>
      <h2>
       Project #4: Analyze Tourism Data
      </h2>
      <p>
       Data: Tourism Data comprises contains : City Pair, seniors travelling,children traveling, adult traveling,
car booking price &amp; air booking price.
Problem Statement: Analyze Tourism data to find out :
      </p>
      <ul>
       <li>
        Top 20 destinations tourist frequently travel to: Based on given data we can find the most popular
 destinations where people travel frequently, based on the specific initial number of trips booked for a
 particular destination
       </li>
       <li>
        Top 20 high air-revenue destinations, i.e the 20 cities that generate high airline revenues for travel, so
 that the discount offers can be given to attract more bookings for these destinations.
       </li>
       <li>
        Top 20 locations from where most of the trips start based on booked trip count.
       </li>
      </ul>
      <h2>
       Project #5: Airport Flight Data Analysis : We will analyze Airport Information System data that gives
information regarding flight delays,source &amp; destination details diverted routes &amp; others.
      </h2>
      <ul>
       Industry: Aviation
Problem Statement: Analyze Flight Data to:
       <li>
        List of Delayed flights.
       </li>
       <li>
        Find flights with zero stop.
       </li>
       <li>
        List of Active Airlines all countries.
       </li>
       <li>
        Source &amp; Destination details of flights.
       </li>
       <li>
        Reason why flight get delayed.
       </li>
       <li>
        Time in different formats.
       </li>
      </ul>
      <h2>
       Project #6: Analyze Movie Ratings
      </h2>
      Industry: Media
      <p>
       Data: Movie data from sites like rotten tomatoes, IMDB, etc. Problem Statement: Analyze the movie ratings
by different users to:
      </p>
      <ul>
       <li>
        Get the user who has rated the most number of movies
       </li>
       <li>
        Get the user who has rated the least number of movies
       </li>
       <li>
        Get the count of total number of movies rated by user belonging to a specific occupation
       </li>
       <li>
        Get the number of underage users
       </li>
      </ul>
      <h2>
       Project #7: Analyze Social Media Channels :
      </h2>
      <ul>
       <li>
        Facebook
       </li>
       <li>
        Twitter
       </li>
       <li>
        Instagram
       </li>
       <li>
        YouTube
       </li>
       <li>
        Industry: Social Media
       </li>
       <li>
        Data: DataSet Columns : VideoId, Uploader, Internal Day of establishment of You tube &amp; the date of
 uploading of the video,Category,Length,Rating, Number of comments.
       </li>
       <li>
        Problem Statement: Top 5 categories with maximum number of videos uploaded.
       </li>
       <li>
        Problem Statement: Identify the top 5 categories in which the most number of videos are uploaded,
 the top 10 rated videos, and the top 10 most viewed videos.
       </li>
       <li>
        Apart from these there are some twenty more use-cases to choose: Twitter Data Analysis
       </li>
       <li>
        Market data Analysis
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="Bigdatatraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="23 + 31 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="54">
       <input name="url" type="hidden" value="/bigdatatraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>