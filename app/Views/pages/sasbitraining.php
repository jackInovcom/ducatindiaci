<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     SAS BI
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     SAS BI
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      DUCAT is the most ideal place where for anyone aspiring to learn SAS BI. Our lesson offers the elaborate insight in such a method that anyone can learn the pros and cons and be a specialist. SAS BI is a complete, easy-to-use business intellect software resolution that integrates the power of SAS Analytics and SAS Data Management to offer insights that power better decisions. It includes interfaces that are role based for all types of users within a well-defined IT domination framework. This helps organizations simplify and speed business intelligence employment. We at DUCAT impart guidance in such a method that it is a step by step approach to SAS BI. The teaching is planned by many instances that can be done through SA BI and some generated workout which will help to appraise your level of perception. DUCAT training members are a batch of extremely educated professionals who have vast familiarity and coverage. They share their knowledge to make you also an expert in that field. So after the training you also have the same level of know-how and can communicate that in your own business or at your job place. The training makes you the first choice for employers. The guidance imparted not only makes them capable of taking proper carrier decision but also makes them more appropriate to run their own business more suitably.
     </p>
     <div class="contentAcc">
      <h2>
       SAS INTELLIGENCE PLATFORM SAS INTELLIGENCE PLATFORM � BI TOOLS 4.2 VERSIONS SAS BUSINESS INTELLIGENCE TOOLS - COURSE OUTLINE PRACTICAL
      </h2>
      <ul>
       <li>
        SAS Data Integration Studio 4.2
       </li>
       <li>
        SAS Olap Cube Studio 4.2
       </li>
       <li>
        SAS Management Console 9.2
       </li>
       <li>
        SAS Enterprise Guide 4.2
       </li>
       <li>
        SAS Information Map Studio 4.2
       </li>
       <li>
        SAS Web Report Studio 4.2
       </li>
       <li>
        SAS Dashboard 4.2
       </li>
       <li>
        SAS Information Delivery Portal 4.2
       </li>
       <li>
        SAS Web Olap Viewer for Java
       </li>
       <li>
        SAS Stored Process 4.2
       </li>
       <li>
        SAS Stored Process Web Application 4.2
       </li>
       <li>
        SAS Add In for Ms Office 4.2
       </li>
       <li>
        Introduction to DWH / BI Concepts
       </li>
      </ul>
      <h2>
       SAS DATA INTEGRATION STUDIO 4.2 INTRODUCTION TO SAS DIS STUDIO
      </h2>
      <ul>
       <li>
        Features of SAS DIS Studio
       </li>
       <li>
        Tasks performed by SAS DIS Studio
       </li>
       <li>
        Navigation to SAS DIS Studio
       </li>
       <li>
        Registering Source Tables Metadata
       </li>
      </ul>
      <h2>
       REGISTER METADATA FOR EXISTING SAS DATA SETS
      </h2>
      <ul>
       <li>
        Register metadata for a relational database accessed through an ODBC Driver
       </li>
       <li>
        Register metadata for an external flat file (.csv, .txt files)
       </li>
      </ul>
      <h2>
       REGISTERING TARGET TABLES METADATA
      </h2>
      <ul>
       <li>
        Register target metadata for dimensional and fact tables
       </li>
      </ul>
      <h2>
       CREATING A JOB TO LOAD TARGET TABLES
      </h2>
      <ul>
       <li>
        Dimensional Tables
       </li>
       <li>
        Fact Table
       </li>
       <li>
        Creating the Star Schema Design (Implementing Slowly Changing Dimension Concepts)
       </li>
       <li>
        Applying Transformations
       </li>
       <li>
        Slowly Changing Dimensions
       </li>
       <li>
        SCD Type2 Loader
       </li>
       <li>
        Fact Table Lookup
       </li>
       <li>
        Surrogate Key Generator
       </li>
       <li>
        Key Effective Date
       </li>
       <li>
        Data Transformations
       </li>
       <li>
        Data Transformations
       </li>
       <li>
        Proc SQL
       </li>
       <li>
        Lookup Table
       </li>
       <li>
        Append
       </li>
       <li>
        Extract
       </li>
       <li>
        Validate
       </li>
       <li>
        Return Code Check
       </li>
       <li>
        Data Transfer
       </li>
       <li>
        Sort
       </li>
       <li>
        Rank
       </li>
       <li>
        Control Table
       </li>
       <li>
        User Written Code
       </li>
       <li>
        Create Transformation Template
       </li>
       <li>
        Transpose
       </li>
       <li>
        Data Access Transformations
       </li>
       <li>
        File Reader
       </li>
       <li>
        File Writer
       </li>
       <li>
        Table Loader
       </li>
       <li>
        Analysis Transformations
       </li>
       <li>
        Frequency Report
       </li>
       <li>
        One-Way Frequency Report
       </li>
       <li>
        Summary Report
       </li>
       <li>
        Tabulate Report
       </li>
       <li>
        Print Report
       </li>
       <li>
        Standardize
       </li>
       <li>
        Converting a report job to a Stored Process
       </li>
       <li>
        Scheduling Jobs in DIS
       </li>
       <li>
        Metadata Sharing
       </li>
       <li>
        Metadata Synchronization
       </li>
       <li>
        Creating Project and Custom Repositories
       </li>
       <li>
        Setting up Change Management for Project Repository
       </li>
       <li>
        Creating a Job that contains Jobs
       </li>
       <li>
        Creating Status Handlers for the Jobs
       </li>
       <li>
        Impact and Reverse Impact Analysis
       </li>
      </ul>
      <h2>
       SAS OLAP CUBE STUDIO 4.2
      </h2>
      <ul>
       <li>
        OLAP Introduction
       </li>
       <li>
        OLAP Components / ArchitectureOLAP Models
       </li>
       <li>
        ROLAP
       </li>
       <li>
        MOLAP
       </li>
       <li>
        HOLAP
       </li>
       <li>
        Discuss Dimensions / Facts / Hierarchies / Levels / Measures / Ragged Hierarchies / Aggregation
       </li>
       <li>
        Introduction to SAS OLAP Cube Studio Tool
       </li>
       <li>
        Features of SAS OLAP Cube
       </li>
       <li>
        Tasks performed by SAS OLAP Cube
       </li>
       <li>
        SAS OLAP Server benefits
       </li>
       <li>
        Navigation to SAS OLAP Cube Studio
       </li>
       <li>
        Steps to create an OLAP Cube
       </li>
       <li>
        Creating an OLAP Cube using the cube designer:
       </li>
       <li>
        Create an OLAP Cube from Detail Table (ROLAP)
       </li>
       <li>
        Create an OLAP Cube from Star Schema (ROLAP, HOLAP)
       </li>
       <li>
        Create an OLAP Cube from Summarized Table
       </li>
       <li>
        Create an OLAP Cube with the following features:
       </li>
       <li>
        Dimensions
       </li>
       <li>
        Levels
       </li>
       <li>
        Hierarchies
       </li>
       <li>
        Measures
       </li>
       <li>
        Aggregates
       </li>
       <li>
        Review the Cube design and structure
       </li>
       <li>
        Exploring OLAP Cube with
       </li>
       <li>
        SAS Enterprise Guide
       </li>
       <li>
        SAS Information Map Studio
       </li>
       <li>
        Microsoft Office Excel
       </li>
       <li>
        SAS WRS
       </li>
       <li>
        Performing Cube Updates
       </li>
       <li>
        Incremental Update to OLAP Cube
       </li>
       <li>
        In-Place Cube Update
       </li>
       <li>
        Introduction to MDX queries
       </li>
      </ul>
      <h2>
       SAS MANAGEMENT CONSOLE 9.2
      </h2>
      <ul>
       <li>
        introduction to SAS Management Console
       </li>
       <li>
        Navigation in SAS Management Console
       </li>
       <li>
        SAS Open Metadata Architecture
       </li>
       <li>
        variable
       </li>
       <li>
        Introduction to Metadata
       </li>
       <li>
        Centralized Metadata Management
       </li>
       <li>
        SAS Metadata Server
       </li>
       <li>
        Connecting to the Metadata Server
       </li>
       <li>
        Introduction and Working With Metadata Repositories
       </li>
       <li>
        Client / Server Interactions
       </li>
       <li>
        Create a Custom and Project Repository
       </li>
       <li>
        Replication and Promotion of Metadata
       </li>
       <li>
        User Manager plug-in
       </li>
       <li>
        Introduction to SAS Application Servers and defining the SAS Application Servers and various other servers from SAS Management Console
       </li>
       <li>
        Security planning for the user and user groups
       </li>
       <li>
        Access control planning
       </li>
       <li>
        Data Library Manager plug-in
       </li>
       <li>
        Defining a data library
       </li>
       <li>
        Setting up Change Management
       </li>
       <li>
        Creating a new metadata profile from SAS ETL Studio
       </li>
       <li>
        Using Job Scheduler plug-in
       </li>
       <li>
        Define a SAS Batch Server
       </li>
       <li>
        Define a Scheduling Server
       </li>
       <li>
        Create and Schedule a job
       </li>
       <li>
        Introduction to Stored Process and creating Stored Process
       </li>
       <li>
        Additional Topics (Database Server, Database Schema, Data Library)
       </li>
      </ul>
      <h2>
       SAS ENTERPRISE GUIDE 4.2 INTRODUCTION TO SAS ENTERPRISE GUIDE
      </h2>
      <ul>
       <li>
        SAS Enterprise Guide Framework
       </li>
       <li>
        SAS EG Building Blocks
       </li>
       <li>
        SAS EG Navigation
       </li>
       <li>
        Create a Project
       </li>
       <li>
        Add data to the Project:
       </li>
       <li>
        Adding local SAS table
       </li>
       <li>
        Accessing remote data (Self Study)
       </li>
       <li>
        Adding spreadsheet to a project
       </li>
       <li>
        Adding text file to a project as a SAS dataset
       </li>
       <li>
        Creating various Report Tasks
       </li>
       <li>
        List Report
       </li>
       <li>
        Frequency Report
       </li>
       <li>
        One-Way Frequency Report
       </li>
       <li>
        Two-Way Frequency Report
       </li>
       <li>
        Generating Summary Statistics
       </li>
       <li>
        Create Tabular Summary Report
       </li>
       <li>
        Creating a Graph
       </li>
       <li>
        Create various Data Transformation
       </li>
       <li>
        Filter
       </li>
       <li>
        Sort
       </li>
       <li>
        Rank
       </li>
       <li>
        Transpose
       </li>
       <li>
        Append
       </li>
       <li>
        Introduction to Query Builder
       </li>
       <li>
        Setting a filter and selecting columns
       </li>
       <li>
        Creating new columns in a query
       </li>
       <li>
        Replacing values in a query
       </li>
       <li>
        Joining Tables
       </li>
       <li>
        Inner Joins
       </li>
       <li>
        Outer Joins
       </li>
       <li>
        Creating and applying custom formats
       </li>
       <li>
        Creating Advanced Queries
       </li>
       <li>
        Controlling Query Output
       </li>
       <li>
        Creating and applying parameterized queries
       </li>
       <li>
        Grouping and Filtering data in a query
       </li>
       <li>
        Additional Topics
       </li>
       <li>
        Automating projects and processes
       </li>
       <li>
        Creating customized Process Flow
       </li>
       <li>
        Modifying SAS Code
       </li>
       <li>
        Customizing Task Code
       </li>
       <li>
        Exporting SAS Code
       </li>
       <li>
        Customizing the Outputs
       </li>
       <li>
        Style Manager
       </li>
       <li>
        Document Builder
       </li>
       <li>
        Project View
       </li>
       <li>
        Viewing an OLAP Cube with SAS Enterprise Guide
       </li>
       <li>
        Viewing an Information Map with SAS Enterprise Guide
       </li>
       <li>
        Create and Register Stored Process from SAS Enterprise Guide
       </li>
      </ul>
      <h2>
       SAS INFORMATION MAP STUDIO 4.2
      </h2>
      <ul>
       <li>
        Introduction to SAS Information Map Studio
       </li>
       <li>
        Navigating SAS Information Map Studio
       </li>
       <li>
        Access and source data from SAS Information Map Studio
       </li>
       <li>
        Updating an Information Map with filters, prompts, folders and new data items
       </li>
       <li>
        Building an Information Map from:
       </li>
       <li>
        SAS Datasets (Relational SAS Information Map)
       </li>
       <li>
        OLAP Cube (Multi Dimensional Information Map)
       </li>
      </ul>
      <h2>
       SAS WEB REPORT STUDIO 4.2
      </h2>
      <ul>
       <li>
        Introduction and Navigation to SAS Web Report Studio Interface
       </li>
       <li>
        Understanding the Tasks performed within SAS Web Report Studio
       </li>
       <li>
        Creating Basic reports by using New Report and Report Wizard
       </li>
       <li>
        Create a Web Report from Detail table and OLAP cubes Information Maps
       </li>
       <li>
        Managing Existing Reports
       </li>
       <li>
        Searching a report
       </li>
       <li>
        Navigating report folders
       </li>
       <li>
        View report definitions
       </li>
       <li>
        Storage locations for reports
       </li>
       <li>
        Report actions available (Edit/Rename/Move/Copy/Delete/Rename/Schedule)
       </li>
       <li>
        Scheduling &amp; Distributing Report
       </li>
      </ul>
      <h2>
       SAS STORED PROCESS 4.2
      </h2>
      <ul>
       <li>
        Introduction to SAS Stored Process
       </li>
       <li>
        Building Registering and Testing Stored Process
       </li>
       <li>
        SAS Enterprise Guide 4.2 (Create, Register and Test)
       </li>
       <li>
        SAS Data Integration Studio (Create and Register)
       </li>
       <li>
        SAS Management Console (Registration)
       </li>
       <li>
        Access Stored Process from SAS Web Report Studio / SAS Information Map Studio / SAS Stored
       </li>
       <li>
        Process Web Application
       </li>
       <li>
        Create Stored Process with Prompts
       </li>
      </ul>
      <h2>
       SAS STORED PROCESS WEB APPLICATION 4.2
      </h2>
      <ul>
       <li>
        Introduction to SAS Stored Process Web Application
       </li>
       <li>
        Viewing SAS Stored Process from Web Application
       </li>
       <li>
        Managing Stored Process from Web Application
       </li>
      </ul>
      <h2>
       SAS ADD-IN FOR MICROSOFT OFFICE 4.2
      </h2>
      <ul>
       <li>
        Introduction and Navigation to SAS Add-In for Microsoft Office
       </li>
       <li>
        Connecting to the SAS Metadata Server from Microsoft Excel
       </li>
       <li>
        Inserting SAS Data into Microsoft Excel
       </li>
       <li>
        Accessing SAS data
       </li>
       <li>
        Filtering SAS data
       </li>
       <li>
        Sorting SAS data
       </li>
       <li>
        Selecting and Ordering columns
       </li>
       <li>
        Analyzing with SAS Tasks in Microsoft Office
       </li>
       <li>
        Access and Analyze SAS OLAP Cubes from Microsoft Office
       </li>
       <li>
        Running Stored Process from Microsoft Excel
       </li>
       <li>
        Customizing the Output Style
       </li>
      </ul>
      <h2>
       DATA WAREHOUSING AND BUSINESS INTELLIGENCE CONCEPTS DATA WAREHOUSING
      </h2>
      <ul>
       <li>
        Data Warehouse Introduction
       </li>
       <li>
        Steps to Design Data Warehouse
       </li>
       <li>
        Components of Data Warehouse
       </li>
       <li>
        Approaches to Data Warehouse Designing
       </li>
       <li>
        Data Modeling types:
       </li>
       <li>
        Normalized Data Model (Databases)
       </li>
       <li>
        Dimensional Data Model (Data Warehouses)
       </li>
      </ul>
      <h2>
       BUSINESS INTELLIGENCE
      </h2>
      <ul>
       <li>
        Business Intelligence Introduction
       </li>
       <li>
        SAS Business Intelligence Architecture
       </li>
       <li>
        Client Tier � Middle Tier � Server Tier
       </li>
       <li>
        Introduction to Metadata
       </li>
       <li>
        Types of Metadata
       </li>
       <li>
        SAS Centralized Metadata Repository
       </li>
       <li>
        Advantages of SAS Centralized Metadata Repository
       </li>
       <li>
        About Metadata Profile
       </li>
       <li>
        Data Model's
       </li>
       <li>
        Normalized Relational Data Model (Entity-Relationship)
       </li>
       <li>
        Dimensional Data Model
       </li>
       <li>
        Star Schema Design
       </li>
       <li>
        Snowflake Schema Design
       </li>
       <li>
        Dimension Tables
       </li>
       <li>
        Steps in designing Dimensional tables
       </li>
       <li>
        Conformed Dimensions
       </li>
       <li>
        Datatype
       </li>
       <li>
        Junk Dimensions
       </li>
       <li>
        Degenerate Dimension
       </li>
       <li>
        Slowly Changing Dimension (Type1, Type2 &amp; Type3)
       </li>
       <li>
        Fact Table
       </li>
       <li>
        Steps in designing Fact tables
       </li>
       <li>
        Datatype
       </li>
       <li>
        Types of Fact tables
       </li>
       <li>
        Additive * Semi-Additive * Non-Additive.
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="sasbitraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="11 + 12 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="23">
       <input name="url" type="hidden" value="/sasbitraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>