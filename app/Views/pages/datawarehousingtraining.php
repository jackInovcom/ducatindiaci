<?php echo view('includes/header.php'); ?>

<section id="breadCrumb">
 <div class="container">
  <div class="row">
   <div class="col-md-6">
    <h3>
     DATA WAREHOUSING
    </h3>
   </div>
   <!-- End Of Col MD 6 -->
   <div class="col-md-6 text-right">
    <a href="http://www.ducatindia.com">
     Home
    </a>
    /
    <a href="">
     DATA WAREHOUSING
    </a>
   </div>
   <!-- End Of Col MD 6 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>
<section id="mainArea">
 <div class="container">
  <div class="row">
   <div class="col-md-9">
    <div class="coursesArea">
     <p>
      DATAWARE HOUSING is a database which is used for data analysis.Data is collected from one or more disparate sources. DATAWARE HOUSING process allows historical as well as present data to be stored which is a helping hand for trending reports. The source of Data is scrutinized by transforming it and preparing Catalogues. It is provided for online analytical process or data mining. Dataware housing is also helpful in market research and decision support purposes. A DATAWARE HOUSING system manages to retrieve lost data, analyze data, extract then transform data and finally loading data to its repository. Data dictionary and Business intelligence is also a part of DATAWARE HOUSING SYSTEM. DATAWARE HOUSING system maintains data history and thus helpful in providing precise information about organization. Having single database made-up of multiple databases helps gather information in intact form. Customer relationship management becomes easier due to Data ware houses. Complex analysis of queries can be done easily with the help of DATAWARE HOUSING. Tactical reporting during sales purpose and validation of data in product marketing are the practical utilities of DATAWARE HOUSING. DUCAT students get a 3 months course in which all the concepts related to DATAWARE HOUSING is covered. Course is taken by expert data analysts and those trainers who have excellent work experience in IT industry. This helps students to win right direction for securing job. Training is placement oriented and placement assistance will help to secure better opportunities.
     </p>
     <div class="contentAcc">
      <h2>
       DATA WAREHOUSING INTRODUCTION TO DATA WAREHOUSING
      </h2>
      <ul>
       <li>
        Data warehouse Concept
       </li>
       <li>
        Difference between Transaction and warehose data
       </li>
       <li>
        Data warehouse Architecture
       </li>
       <li>
        Logical and physical Design
       </li>
       <li>
        Difference between Data warehouse and Data Mart
       </li>
       <li>
        Source Data , staging Data and target Data
       </li>
       <li>
        Operational data source (ODS )
       </li>
       <li>
        OLAP . ROLAP , MOLAP
       </li>
       <li>
        Star schema , star flake schema and snow flake schema
       </li>
       <li>
        Normalizatin and De-normailzation
       </li>
       <li>
        Meta data and mining
       </li>
      </ul>
      <h2>
       Entity � Relation Model (E-R Model for transactional database
      </h2>
      <ul>
       <li>
        Defining entities and their relationship
       </li>
       <li>
        Distribution of attributes under entities
       </li>
       <li>
        Normilization of entities
       </li>
       <li>
        Modeling logical E-R Model using E-R win tool
       </li>
       <li>
        Conversion of logical model into physical model
       </li>
       <li>
        Application of Business Rules
       </li>
       <li>
        Constraints , triggers , Domains , Back-end programs , etc
       </li>
       <li>
        Two case studies
       </li>
      </ul>
      <h2>
       DIMENSIONAL MODEL (STAR SCHEMA ) FOR DATA WAREHOUSE
      </h2>
      <ul>
       <li>
        Informatica Server setup
       </li>
       <li>
        Repository manager : Repository creation Deletion , backup &amp; restore
       </li>
       <li>
        Repository manager : Multiple Repository , Multiple Folders
       </li>
       <li>
        Repository manager : Folder Creation , Deletion and privileges Setup
       </li>
       <li>
        Repository manager : User , group &amp; Privileges Management
       </li>
       <li>
        Designer: Defining Source from RDBMS , Semi �RDBMS ,Flate File , XML File , Cobol File
       </li>
       <li>
        Designer : Defining Target for Staging Database &amp; for Data warehouse .
       </li>
       <li>
        Designer : Creating Multi �Dimensional Cubes
       </li>
       <li>
        Designer : Transformation Developer for Reusable User � Defined transformation
       </li>
       <li>
        Designer : Mappletes for reusable User �Defined Mappings
       </li>
       <li>
        Designer : Mapping to apply Transformation using 14 Transformation
       </li>
       <li>
        Designer: Global referencing of object across Folders * Repositories
       </li>
       <li>
        Designer : Data staging
       </li>
       <li>
        Server Manager : Setting up server for Repository
       </li>
       <li>
        Server Manager : Creating session and Batches
       </li>
       <li>
        Server Manager : Schudling session and Batches
       </li>
       <li>
        Server Manager : Monitoring Sessions and Batches for data loading
       </li>
       <li>
        Server Manager : Troubleshooting Data loading Erros
       </li>
      </ul>
      <h2>
       AB-INITIO
      </h2>
      <ul>
       <li>
        Introduction
       </li>
       <li>
        Hardware Platforms
       </li>
       <li>
        Products of Abinitio s/w Corporation
       </li>
       <li>
        Feature of Abinitio
       </li>
       <li>
        Building a Basic Abinitio Graphs
       </li>
       <li>
        Case � Study
       </li>
       <li>
        Multi-File System / Multifile Directory
       </li>
       <li>
        Types of parallelism
       </li>
       <li>
        Testing Abinito Graphs
       </li>
       <li>
        EME ( Enterprise meta &gt; Environment )
       </li>
       <li>
        Project
       </li>
       <li>
        Sandbox
       </li>
       <li>
        Version Control
       </li>
       <li>
        Performance Tuning of Abinitio Graphs
       </li>
      </ul>
      <h2>
       OLAP &amp; PRESENTATION USING COGNOS 8 BI
      </h2>
      <ul>
       <li>
        Introduction of Data Warehousing process
       </li>
       <li>
        Detailed explanation about O.L.T. P System
       </li>
       <li>
        Explanation of DWH Life Cycle
       </li>
       <li>
        Detailed explanation of OLAP Life cycle
       </li>
       <li>
        Detailed explanation of COGNOS 8 BI Architecture
       </li>
       <li>
        Types of OLAP's
       </li>
       <li>
        Detailed explanation of Dimensional Modeling
       </li>
       <li>
        Detailed explanation of STAR and SNOW FLAKE SCHEMA
       </li>
       <li>
        Detailed explanation Classes and variables
       </li>
       <li>
        Detailed explanation of D.I WEB.I Bussiness intelliegence and tools.
       </li>
       <li>
        Data mining concepts , model concepts
       </li>
      </ul>
      <h2>
       CREATING PACKAGES AND MODELS BY USING COGNOS 8 FRAME WORK MANAGER
      </h2>
      <ul>
       <li>
        Cognos 8 BI Framwork Manager : Designing Metadata Models
       </li>
       <li>
        Cognos 8 BI Transformer : Designing OLAP Models 4 day
       </li>
       <li>
        Cognos 8 BI Data manager : Building Data marts with Enterprise Data
       </li>
       <li>
        Publishing Packages
       </li>
       <li>
        Generating simple and ad �hoc reports using Cognos8 BI tools
       </li>
      </ul>
      <h2>
       GENERATING REPORTS WITH QUERY STUDIO
      </h2>
      <ul>
       <li>
        Cognos 8 BI Cognos Connection for Consumers
       </li>
       <li>
        Cognos 8 BI Query Studio : Building Ad � hoc Reports
       </li>
       <li>
        Cognos 8 BI Analysis Studio : Analyzing Data
       </li>
       <li>
        Cognos 8 BI Report Studio : Authoring statement style Reports in Express Authoring Mode
       </li>
       <li>
        Cognos 8 BI Analysis for Excel : Analyzing Data in Microsoft Excel
       </li>
      </ul>
      <h2>
       GENRATING REPORTS WITH REPORT STUDIO
      </h2>
      <ul>
       <li>
        Cognos 8 BI Report Studio : Authoring Proffessional Reports
       </li>
       <li>
        Cognos 8 BI Report Studio : Authoring Reports with Multidimensional Data
       </li>
       <li>
        Cognos 8 BI Report Studio : Introduction
       </li>
       <li>
        Cognos 8 BI Report Studio: Intermediate
       </li>
       <li>
        Cognos 8 BI Event Studio : Creating , Schedudling and Managing
       </li>
       <li>
        GENERATING REPORTS WITH EVENT STUDIO
       </li>
       <li>
        GENERATING REPORTS WITH ANALYSIS STUDIO
       </li>
       <li>
        GENERATING CHARTS AND MULTI DIMENSIONAL REPORTS USING COGNOS 8 BI SUITE
       </li>
       <li>
        PUBLISHING REPORTS ANALYSIS AND REVIEW
       </li>
       <li>
        SCHEDULING REPORTS USING BI SERVER
       </li>
       <li>
        COGNOS 8 BI ADMINISTRATION AND USER CREATION WITH PRIVILEGES
       </li>
       <li>
        OLAP &amp; PRESENTATION USING BUSINESS OBJECT
       </li>
       <li>
        Supervisor : Creating repository and Supervisor
       </li>
       <li>
        Supervisor : Database Conections.
       </li>
       <li>
        Supervisor : Defining Groups and Users
       </li>
       <li>
        Supervisor : Defining privileges and profiles for users
       </li>
       <li>
        Supervisor : Broadcast Agents
       </li>
       <li>
        Supervisor : Scheduling users Connections Sessions
       </li>
       <li>
        Designer : Creating and maintaining Universe
       </li>
       <li>
        Designer : Defining tables &amp; columns as Class and objects
       </li>
       <li>
        Designer : Creating and Maintaining measures
       </li>
       <li>
        Designer : Defining relations , formulas , etc
       </li>
       <li>
        Business objects: Defining Query , reports type
       </li>
       <li>
        Business objects:formatting reports
       </li>
       <li>
        Business objects:slicing &amp; dicing reports
       </li>
       <li>
        Business objects: drill- down , drill �up Reports
       </li>
       <li>
        Business objects: filtering records , Grouping Records , etc
       </li>
      </ul>
     </div>
     <div class="text-center">
      <a class="registrationButton" href="datawarehousingtraining.pdf">
       Download Brochure
      </a>
     </div>
    </div>
   </div>
   <!-- End Of Col MD 9 -->
   <div class="col-md-3">
    <div class="text-center">
     <a class="registrationButton" href="http://www.ducatindia.com/online-registration">
      Online Registration
     </a>
    </div>
    <div class="widgetArea">
     <h5>
      COMMENCING NEW BATCHES
     </h5>
     <ul class="listStyleCourses">
      <li>
       <h4>
        Noida
       </h4>
       <a href="../comming-soon-batches?center=noida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Greater Noida
       </h4>
       <a href="../comming-soon-batches?center=gnoida">
        Details
       </a>
      </li>
      <li>
       <h4>
        Ghaziabad
       </h4>
       <a href="../comming-soon-batches?center=ghaziabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Faridabad
       </h4>
       <a href="../comming-soon-batches?center=faridabad">
        Details
       </a>
      </li>
      <li>
       <h4>
        Gurgaon
       </h4>
       <a href="../comming-soon-batches?center=gurgaon">
        Details
       </a>
      </li>
     </ul>
    </div>
    <div class="widgetArea">
     <h5>
      ENQUIRY FORM
     </h5>
     <form action="../logics_database/course_enquiry.php" class="enquiryForm" method="post">
      <input name="name" pattern="[a-zA-Z ]{1,40}" placeholder="Full Name" required="" type="text"/>
      <input name="email" placeholder="Email" required="" type="text"/>
      <input name="city" placeholder="City" required="" type="text"/>
      <input name="contact" pattern="[0-9]{10,11}" placeholder="Contact Number" required="" type="text"/>
      <select name="branch">
       <option>
        Select Branch
       </option>
       <option value="Noida">
        Noida
       </option>
       <option value="Faridabad">
        Faridabad
       </option>
       <option value="Ghaziabad">
        Ghaziabad
       </option>
       <option value="Greater Noida">
        Greater Noida
       </option>
       <option value="Gurgaon">
        Gurgaon
       </option>
      </select>
      <select name="course">
       <option value="Select Course">
        Select Course
       </option>
       <option value="3D Studio Max">
        3D Studio Max
       </option>
       <option value="436_cluster">
        436 Cluster
       </option>
       <option value="microcontroller">
        8051-Microcontroller
       </option>
       <option value=".NET Adv MVC">
        .NET Adv MVC
       </option>
       <option value=".NET Three Months">
        .NET Three Months
       </option>
       <option value=".NET six Months">
        .NET six Months
       </option>
       <option value=".NET six weeks">
        .NET six weeks
       </option>
       <option value="Adobe Flex-3.0">
        Adobe Flex-3.0
       </option>
       <option value="Advance Digital marketing">
        Advance Digital marketing
       </option>
       <option value="Adv. Digital System Design">
        Adv. Digital System Design
       </option>
       <option value="AdvPython">
        Adv. Python
       </option>
       <option value="Advance QTP">
        Advance QTP
       </option>
       <option value="Amazon6weeks">
        Amazon 6 Weeks
       </option>
       <option value="Android">
        Android
       </option>
       <option value="Angular 4">
        Angular 4
       </option>
       <option value="Angularjs">
        Angularjs
       </option>
       <option value="Angularjs2">
        Angularjs 2
       </option>
       <option value="Api Testing">
        Api Testing
       </option>
       <option value="Appium">
        Appium
       </option>
       <option value="Apache Hadoop">
        Apache Hadoop
       </option>
       <option value="arm">
        Arm
       </option>
       <option value="arduino">
        Arduino
       </option>
       <option value="Autocad">
        Autocad
       </option>
       <option value="avr-microcontroller">
        Avr-Microcontroller
       </option>
       <option value="Azure">
        Azure
       </option>
       <option value="BI Cognos 8.4">
        BI Cognos 8.4
       </option>
       <option value="Big Commerce">
        Big Commerce
       </option>
       <option value="Big data">
        Big Data
       </option>
       <option value="C Language">
        C Language
       </option>
       <option value="C++ Language">
        C++ Language
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="CAD_Civil_SM">
        CAD_Civil_SM
       </option>
       <option value="CADcustomization">
        Cad Customization
       </option>
       <option value="CAD Mechanical Six Months">
        CAD Mechanical Six Months
       </option>
       <option value="Catia">
        Catia
       </option>
       <option value="CCNA">
        CCNA
       </option>
       <option value="Ccnasecurity">
        CCNA Security
       </option>
       <option value="CCNP">
        CCNP
       </option>
       <option value="Checkpoint">
        Checkpoint
       </option>
       <option value="Cloud Computing Full Course">
        Cloud Computing Full Course
       </option>
       <option value="Cloud Computing six Weeks">
        Cloud Computing six Weeks
       </option>
       <option value="Coreldraw">
        CorelDraw
       </option>
       <option value="Corepython">
        Core Python
       </option>
       <option value="Dataware Housing">
        Dataware Housing
       </option>
       <option value="Data Structures">
        Data Structures
       </option>
       <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">
        Data Science &amp; Machine Learning Using R Programming
       </option>
       <option value="Datasciencepython">
        DataSciencePython
       </option>
       <option value="DATA_SCIENCE_USING_R_PROGRAMMING">
        Data Science Using R Programming
       </option>
       <option value="deeplearning">
        Deep Learning
       </option>
       <option value="deeplearninginpython">
        Deep Learning in python
       </option>
       <option value="device-drivers">
        Device-Drivers
       </option>
       <option value="Devops">
        Devops
       </option>
       <option value="Digital Marketing">
        Digital Marketing
       </option>
       <option value="digitalmarketingsix">
        Digital Marketing 6 weeks
       </option>
       <option value="diipp">
        DIIPP
       </option>
       <option value="Diploma In Hardware Networking">
        Diploma In Hardware Networking
       </option>
       <option value="Django">
        Django
       </option>
       <option value="DO_407_Ansible">
        DO 407 Ansible
       </option>
       <option value="Drupal">
        Drupal
       </option>
       <option value="Embedded 3.6 Months">
        Embedded 3.6 Months
       </option>
       <option value="Embedded Six Months">
        Embedded Six Months
       </option>
       <option value="Embedded six Weeks">
        Embedded six Weeks
       </option>
       <option value="ERP Level-2">
        ERP Level-2
       </option>
       <option value="erpscm">
        ERP SCM
       </option>
       <option value="GD and T">
        GD &amp; T
       </option>
       <option value="hardware-and-electronics">
        Hardware-And-Electronics
       </option>
       <option value="HR GENERALIST">
        HR Generalist
       </option>
       <option value="IBM MAINFRAME">
        IBM Mainframe
       </option>
       <option value="IOT">
        IOT
       </option>
       <option value="I-Phone">
        I-Phone
       </option>
       <option value="Javaexpert">
        Java Expert
       </option>
       <option value="Javaexpert">
        Java Beginners
       </option>
       <option value="Java6w">
        Java6w
       </option>
       <option value="JAVA ANDROID KOTLIN">
        Java Android Kotlin
       </option>
       <option value="JAVA HADOOP">
        Java Hadoop
       </option>
       <option value="JAVA J2EE">
        Java J2ee
       </option>
       <option value="Java six Months">
        Java six Months
       </option>
       <option value="JAVA Spring &amp; Hibernate">
        Java Spring &amp; Amp; Hibernate
       </option>
       <option value="JAVA UI">
        Java UI
       </option>
       <option value="javawithangular4">
        Java With Angular 4
       </option>
       <option value="javasql">
        Java With SQL
       </option>
       <option value="Joomla">
        Joomla
       </option>
       <option value="Learn and Earn">
        Learn And Earn
       </option>
       <option value="MACHINELEARNING">
        Machine Learning
       </option>
       <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">
        Machine Learning Using R Programming
       </option>
       <option value="Magento PHP">
        Magento PHP
       </option>
       <option value="Mean">
        Mean
       </option>
       <option value="MCITP">
        MCITP
       </option>
       <option value="MCSA 2016">
        MCSA 2016
       </option>
       <option value="MCSE">
        MCSE
       </option>
       <option value="Mis">
        Mis
       </option>
       <option value="Msbi">
        MSBI
       </option>
       <option value="mssql">
        MS SQL
       </option>
       <option value="Multimedia Animation">
        Multimedia Animation
       </option>
       <option value="Networking Six Months">
        Networking Six Months
       </option>
       <option value="pcb-design">
        Pcb-Design
       </option>
       <option value="oracle 11G">
        Oracle 11G
       </option>
       <option value="oracle 11G DBA">
        Oracle 11G DBA
       </option>
       <option value="oracle 11G RAC">
        Oracle 11G RAC
       </option>
       <option value="oracle app's XII Financials">
        Oracle App's XII Financials
       </option>
       <option value="oracle app's XII MFG">
        Oracle App's XII MFG
       </option>
       <option value="oracle app's XII Tecnical">
        Oracle App's XII Tecnical
       </option>
       <option value="Performance_Tuning">
        Performance Tuning
       </option>
       <option value="Perl script">
        Perl Script
       </option>
       <option value="photoshop">
        Photoshop
       </option>
       <option value="PHP ++">
        PHP ++
       </option>
       <option value="PHP Six Months">
        PHP Six Months
       </option>
       <option value="PHP SIX WEEKS">
        PHP Six WEEKS
       </option>
       <option value="pic-microcontroller">
        Pic-Microcontroller
       </option>
       <option value="PLC SCADA 3 months">
        PLC Scada 3 months
       </option>
       <option value="PLC SCADA six weeks">
        PLC Scada Six Weeks
       </option>
       <option value="PLC SCADA FULL COURCE">
        PLC Scada Full Course
       </option>
       <option value="plsql">
        PLSQL
       </option>
       <option value="Primaverproject Planner">
        PrimaverProject Planner
       </option>
       <option value="pro-e">
        pro-e
       </option>
       <option value="powerbi">
        Power BI
       </option>
       <option value="Python">
        Python
       </option>
       <option value="PythonDjango">
        Python &amp; Django
       </option>
       <option value="pythonwithiot">
        Python with IOT
       </option>
       <option value="Pythonwithmachinelearning">
        Python With MachineLearning
       </option>
       <option value="pythonwithselenium">
        Python With Selenium
       </option>
       <option value="Randdataanalytics">
        R And Data Analytics
       </option>
       <option value="raspberry-pi">
        Raspberry-pi
       </option>
       <option value="Redhat Linux 7.0">
        Redhat Linux 7.0
       </option>
       <option value="Rprogramming">
        R Programming
       </option>
       <option value="Ravit Architecture">
        Ravit Architecture
       </option>
       <option value="RH_236">
        RH 236
       </option>
       <option value="RH_310_openstack">
        RH 310 Openstack
       </option>
       <option value="RH342">
        RH342
       </option>
       <option value="RHCVA">
        RHCVA
       </option>
       <option value="RHCE">
        RHCE
       </option>
       <option value="ROBOTICS COURSE">
        Robotics Course
       </option>
       <option value="RPA BLUE PRISM">
        RPA BLUE PRISM
       </option>
       <option value="RPA UI Path">
        RPA UI PATH
       </option>
       <option value="RPA WORK FUSION">
        RPA WORK FUSION
       </option>
       <option value="rtos">
        Rtos
       </option>
       <option value="SAS">
        SAS
       </option>
       <option value="SAS BI">
        SAS BI
       </option>
       <option value="SEO">
        SEO
       </option>
       <option value="Sharepoint Development">
        Sharepoint Development
       </option>
       <option value="Silverlight 4.0">
        Silverlight 4.0
       </option>
       <option value="SOAP-UI Testing">
        SOAP-UI Testing
       </option>
       <option value="Solidworks">
        Solidworks
       </option>
       <option value="Software Quality Testing">
        Software Quality Testing
       </option>
       <option value="SQT Six Months">
        SQT Six Months
       </option>
       <option value="SQT Six weeks">
        SQT Six weeks
       </option>
       <option value="SQL Server DBA">
        SQL Server DBA
       </option>
       <option value="Staad Pro">
        Staad Pro
       </option>
       <option value="tableau">
        Tableau
       </option>
       <option value="tally erp">
        Tally Erp 9
       </option>
       <option value="TIBCO">
        TIBCO
       </option>
       <option value="UI">
        UI
       </option>
       <option value="UIandangularjs">
        UI And Angular js
       </option>
       <option value="Unigraphics">
        Unigraphics
       </option>
       <option value="UNIX SHELL Scripting">
        Unix Shell Scripting
       </option>
       <option value="Upgrade mvc">
        Upgrade MVC 1 Month
       </option>
       <option value="VLSI Design Flow">
        VLSI Design Flow
       </option>
       <option value="Wordpress">
        Wordpress
       </option>
       <option value="ZEND PHP">
        ZEND PHP
       </option>
      </select>
      <textarea name="query" placeholder="Query Here" required=""></textarea>
      <input name="cp" placeholder="86 + 61 (Answer this)" required="" type="text"/>
      <input name="cpp" type="hidden" value="147">
       <input name="url" type="hidden" value="/datawarehousingtraining/">
        <input name="submit" type="submit" value="Submit Query"/>
       </input>
      </input>
     </form>
    </div>
   </div>
   <!-- End Of Col MD 3 -->
  </div>
  <!-- End Of Row -->
 </div>
 <!-- End OF Container -->
</section>


<?php echo view('includes/footer.php'); ?>