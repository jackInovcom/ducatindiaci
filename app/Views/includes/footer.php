<div id="inline" style="display: none;">
    <form method="post" action="../logics_database/first_page_query.php">
        <h3>Enquire For!!   </h3>
        <input type="text" placeholder="Enter your Name" name="nameR" pattern="[a-zA-Z ]{1,30}" required>
        <input type="text" placeholder="Enter Contact Number" name="mobileR" pattern="[0-9]{10,11}" required>

        <select name="techR">
            <option value="Select Course">Select Course</option>

            <option value="3D Studio Max">3D Studio Max</option>
            <option value="436_cluster">436 Cluster</option>

            <option value="microcontroller">8051-Microcontroller</option>
            <option value=".NET Adv MVC">.NET Adv MVC</option>

            <option value=".NET four Months">.NET Four Months</option>
            <option value=".NET six Months">.NET six Months</option>
            <option value=".NET six weeks">.NET six weeks</option>

            <option value="Adobe Flex-3.0">Adobe Flex-3.0</option>
            <option value="Advance Digital marketing">Advance Digital marketing</option>
            <option value="Adv. Digital System Design">Adv. Digital System Design</option>
            <option value="AdvPython">Adv. Python</option>
            <option value="Advance QTP">Advance QTP</option>

            <option value="Amazon6weeks">Amazon 6 Weeks</option>
            <option value="Android">Android</option>

            <option value="Angular 4">Angular 4</option>
            <option value="Angularjs">Angularjs</option>
            <option value="Angularjs2">Angularjs 2</option>
            <option value="Api Testing">Api Testing</option>
            <option value="Appium">Appium</option>
            <option value="Apache Hadoop">Apache Hadoop</option>
            <option value="arm">Arm</option>
            <option value="arduino">Arduino</option>
            <option value="Autocad">Autocad</option>
            <option value="Automation Anywhere">Automation Anywhere</option>
            <option value="avr-microcontroller">Avr-Microcontroller</option>
            <option value="Azure">Azure</option>
            <option value="BI Cognos 8.4">BI Cognos 8.4</option>
            <option value="Blockchain">Blockchain</option>

            <option value="Big Commerce">Big Commerce</option>
            <option value="Big data">Big Data</option>
            <option value="C Language">C Language</option>
            <option value="C++ Language">C++ Language</option>
            <option value="CAD Mechanical Six Months">CAD Mechanical Six Months</option>
            <option value="CAD_Civil_SM">CAD_Civil_SM</option>
            <option value="CADcustomization">Cad Customization</option>
            <option value="CAD Mechanical Six Months">CAD Mechanical Six Months</option>
            <option value="Catia">Catia</option>
            <option value="CCNA">CCNA</option>

            <option value="Ccnasecurity">CCNA Security</option>
            <option value="CCNP">CCNP</option>
            <option value="Checkpoint">Checkpoint</option>
            <option value="Cloud Computing Full Course">Cloud Computing Full Course</option>
            <option value="Cloud Computing six Weeks">Cloud Computing six Weeks</option>

            <option value="Coreldraw">CorelDraw</option>

            <option value="Dataware Housing">Dataware Housing</option>
            <option value="Data Structures"> Data Structures</option>

            <option value="DATA_SCIENCE_MACHINE_LEARNING_USING_R_PROGRAMMING">Data Science & Machine Learning Using R Programming</option>

            <option value="Datasciencepython">DataSciencePython</option>
            <option value="DATA_SCIENCE_USING_R_PROGRAMMING">Data Science Using R Programming</option>

            <option value="deeplearning">Deep Learning</option>
            <option value="deeplearninginpython">Deep Learning in python</option>
            <option value="device-drivers">Device-Drivers</option>
            <option value="Devops">Devops</option>
            <option value="Digital Marketing">Digital Marketing</option>
            <option value="digitalmarketingsix">Digital Marketing 6 weeks</option>
            <option value="diipp">DIIPP</option>
            <option value="Diploma In Hardware Networking">Diploma In Hardware Networking</option>
            <option value="Django">Django</option>

            <option value="DO_407_Ansible">DO 407 Ansible</option>
            <option value="Drupal">Drupal</option>
            <option value="Embedded 3.6 Months">Embedded 3.6 Months</option>
            <option value="Embedded Six Months">Embedded Six Months</option>
            <option value="Embedded six Weeks">Embedded six Weeks</option>
            <option value="ERP Level-2">ERP Level-2</option>
            <option value="erpscm">ERP SCM </option>
            <option value="GD and T">GD & T</option>

            <option value="hardware-and-electronics">Hardware-And-Electronics</option>
            <option value="HR GENERALIST">HR Generalist</option>
            <option value="IBM MAINFRAME">IBM Mainframe</option>
            <option value="IOT">IOT </option>
            <option value="I-Phone">I-Phone</option>
            <option value="Java6w">Java6w</option>
            <option value="Javaexperts">Java Experts</option>
            <option value="Javabeginners">Java Beginners</option>
            <option value="JAVA ANDROID KOTLIN">Java Android Kotlin</option>
            <option value="JAVA HADOOP">Java Hadoop</option>
            <option value="JAVA J2EE">Java J2ee</option>
            <option value="jmeter">jmeter</option>

            <option value="Java six Months">Java six Months</option>
            <option value="JAVA Spring &amp; Hibernate">Java Spring & Amp; Hibernate</option>
            <option value="Java Full Stack Developer training">Java Full Stack Developer training</option>
            <option value="JAVA UI">Java UI</option>
            <option value="javawithangular4">Java With Angular 4</option>
            <option value="javasql">Java With SQL</option>
            <option value="Joomla">Joomla</option>
            <option value="Learn and Earn">Learn And Earn</option>
            <option value="Load Runner">Load Runner</option>
            <option value="MACHINELEARNING">Machine Learning</option>
            <option value="MACHINE_LEARNING_USING_R_PROGRAMMING">Machine Learning Using R Programming</option>
            <option value="Magento PHP">Magento PHP</option>
            <option value="Manual-selenium">Manual + selenium</option>
            <option value="Mean">Mean</option>
            <option value="MCITP">MCITP</option>
            <option value="MCSA 2016">MCSA 2016</option>
            <option value="MCSE">MCSE</option>

            <option value="Mis">Mis</option>
            <option value="Msbi">MSBI</option>
            <option value="mssql">MS SQL</option>
            <option value="Multimedia Animation">Multimedia Animation</option>
            <option value="Networking Six Months">Networking Six Months</option>

            <option value="pcb-design">Pcb-Design</option>
            <option value="oracle 11G">Oracle 11G</option>
            <option value="oracle 11G DBA">Oracle 11G DBA</option>
            <option value="oracle 11G RAC">Oracle 11G RAC</option>
            <option value="oracle app's XII Financials">Oracle App's XII Financials</option>
            <option value="oracle app's XII MFG">Oracle App's XII MFG</option>
            <option value="oracle app's XII Tecnical">Oracle App's XII Tecnical</option>

            <option value="Powerbi">Power BI</option>
            <option value="Performance_Tuning">Performance Tuning</option>
            <option value="Perl script">Perl Script</option>
            <option value="Pega">Pega</option>
            <option value="photoshop">Photoshop</option>
            <option value="PHP ++">PHP ++</option>

            <option value="PHP Six Months">PHP Six Months</option>
            <option value="PHP SIX WEEKS">PHP Six WEEKS</option>
            <option value="pic-microcontroller">Pic-Microcontroller</option>
            <option value="PLC SCADA 3 months">PLC Scada 3 months</option>
            <option value="PLC SCADA six weeks">PLC Scada Six Weeks</option>
            <option value="PLC SCADA FULL COURCE">PLC Scada Full Course</option>
            <option value="plsql">PLSQL</option>
            <option value="Primaverproject Planner">PrimaverProject Planner</option>
            <option value="pro-e">pro-e</option>
            <option value="Python">Python</option>
            <option value="PythonDjango">Python & Django</option>
            <option value="pythonwithiot">Python with IOT</option>
            <option value="Pythonwithmachinelearning">Python With MachineLearning</option>

            <option value="pythonwithselenium">Python With Selenium </option>
            <option value="QTPUFT">QTPUFT </option>

            <option value="Randdataanalytics">R And Data Analytics</option>
            <option value="raspberry-pi">Raspberry-pi</option>
            <option value="Redhat Linux 7.0">Redhat Linux 7.0</option>
            <option value="Rprogramming">R Programming</option>
            <option value="React js">React js</option>
            <option value="Ravit Architecture">Ravit Architecture</option>
            <option value="RH_236">RH 236</option>
            <option value="RH_310_openstack">RH 310 Openstack</option>
            <option value="RH342">RH342</option>

            <option value="RHCVA">RHCVA</option>

            <option value="RHCE">RHCE</option>

            <option value="ROBOTICS COURSE">Robotics Course</option>
            <option value="RPA BLUE PRISM">RPA BLUE PRISM</option>
            <option value="RPA UI Path">RPA UI PATH</option>
            <option value="RPA WORK FUSION">RPA WORK FUSION</option>
            <option value="rtos">Rtos</option>
            <option value="SAS">SAS</option>
            <option value="SAS BI">SAS BI</option>
            <option value="Selenium with Python">Selenium with Python</option>
            <option value="SE-PD(Personality Dev.)">SE-PD(Personality Dev.)</option>
            <option value="SEO">SEO</option>
            <option value="Selenium"> Selenium</option>

            <option value="Sharepoint Development">Sharepoint Development</option>
            <option value="Silverlight 4.0">Silverlight 4.0</option>

            <option value="Solidworks">Solidworks</option>
            <option value="springboot">springboot</option>
            <option value="Software Quality Testing">Software Quality Testing</option>
            <option value="SQL Server DBA">SQL Server DBA</option>
            <option value="SQT Six weeks">SQT Six weeks</option>
            <option value="Staad Pro">Staad Pro</option>
            <option value="Success Mantra">Success Mantra</option>

            <option value="Tableau">Tableau</option>
            <option value="TIBCO">TIBCO</option>
            <option value="tally erp">Tally Erp 9</option>
            <option value="TIBCO">TIBCO</option>
            <option value="UI">UI</option>
            <option value="UIandangularjs">UI And Angular js</option>
            <option value="Unigraphics">Unigraphics</option>
            <option value="UNIX SHELL Scripting">Unix Shell Scripting</option>
            <option value="Upgrade mvc">Upgrade MVC 1 Month</option>
            <option value="VLSI Design Flow">VLSI Design Flow</option>
            <option value="Wordpress">Wordpress</option>
            <option value="ZEND PHP">ZEND PHP</option>

        </select>
        <select name="branchR">
            <!--<option value="Select Branch!">Select Branch!</option>-->
            <option value="Noida">Noida</option>
            <option value="Gr.Noida">Gr.Noida</option>
            <option value="Ghaziabad">Ghaziabad</option>
            <option value="Faridabad">Faridabad</option>
            <option value="Gurgaon">Gurgaon</option>

        </select>
        <input type="submit" value="Send" name="goDemoReq">
    </form>
</div>

<footer id="footer">
            
          <div class="container">
            
              <div class="row">
                
                  <div class="col-md-3">
                    
                      <h5>TOP COURSES</h5>
                      <div class="separator"><div class="separator_line"></div></div>
                      <ul class="footerLinks">
                          <li><a href="/javasixweekstraining">LANGUAGE</a></li>
                          <li><a href="/oracle11gdataguardtraining">DATABASE</a></li>
                          <li><a href="/embeddedtraining">ELECTRONICS</a></li>
                          <li><a href="/redhattraining">LINUX / ADMINISTRATION</a></li>
                          <li><a href="/ccnatraining">NETWORKING</a></li>
                          <li><a href="/autocadtraining">CAD / MODELING</a></li>
                          <li><a href="/sqtsixmonthstraining">SOFTWARE TESTING+</a></li>
                       </ul>

                  </div> <!-- End Of Col MD 3 -->
                
                  <div class="col-md-3">
                    
                      <h5>&nbsp;</h5>
                      <div class="separator"><div class="separator_line"></div></div>
                      <ul class="footerLinks">
                          <li><a href="/androidtraining">MOBILE APPLICATION</a></li>
                          <li><a href="/erp-abap">ERP</a></li>
                          <li><a href="/webdesigntraining">GRAPHICS/WEB DESIGN</a></li>
                          <li><a href="/hadooptraining">INDUSTRY TRENDS</a></li>
                          <li><a href="/cloudtraining">CLOUD COMPUTING</a></li>
                          <li><a href="/6-months-industrial-training">INDUSTRIAL TRAINING</a></li>
                          <li><a href="/autodeskcertification">AUTODESK PARTNER(GHAZIABAD)</a></li>
                       </ul>

                  </div> <!-- End Of Col MD 3 -->
                
                  <div class="col-md-3">
                    
                      <h5>OUR BRANCH OFFICES</h5>
                      <div class="separator"><div class="separator_line"></div></div>
                      <ul class="footerLinks">
                          <li><a href="/contact-us">DUCAT in Noida</a></li>
                          <li><a href="/contact-us">DUCAT in Ghaziabad</a></li>
                          <li><a href="/contact-us">DUCAT in Faridabad</a></li>
                          <li><a href="/contact-us">DUCAT in Gurgaon</a></li>
                          <li><a href="/contact-us">DUCAT in Gr. Noida</a></li>
                          
                      </ul>

                  </div> <!-- End Of Col MD 3 -->
                
                  <div class="col-md-3">
                    
                      <h5>STUDENT CORNER</h5>
                      <div class="separator"><div class="separator_line"></div></div>
                      <ul class="footerLinks">
                          <li><a href="/student-details">Student Placement Form</a></li>
                          <li><a href="/certificationrequest/">Certificate Request Form</a></li>
                          <li><a href="/ducat-gallery">Ducat Gallery</a></li>
                          <li><a href="/our-placement-cell">Job &amp; Placement</a></li>
                          <li><a href="https://www.facebook.com/ducatEducation" target="_blank">Social Communities</a></li>
                      </ul>
                      <a href="http://www.hitwebcounter.com/" target="_blank">
                      <img src="http://hitwebcounter.com/counter/counter.php?page=6019941&amp;style=0025&amp;nbdigits=6&amp;type=page&amp;initCount=0" title="reliable Counter" alt="reliable Counter"/>
                      </a><br>
                      <!-- hitwebcounter.com --><a href="http://www.hitwebcounter.com/" title="Web Hits Stat" target="_blank" style="font-family: Arial, Helvetica, sans-serif; 
                      font-size: 9px; color: #6D5B77; text-decoration: underline ;"><em>Web Hits Stat</em>
                      </a>   

                  </div> <!-- End Of Col MD 3 -->

              </div> <!-- End Of Row -->

          </div> <!-- End OF Container -->

      </footer> <!-- End Of Footer -->

      <section id="footerBottom">
            
          <div class="container">
            
              <div class="row">
                
                  <div class="col-md-3">
                    
                      <h5>FOLLOW US ON</h5>
                      <div class="foterSocial">
                        <a href="https://www.facebook.com/ducatEducation" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a>
                        <a href="https://www.linkedin.com/in/ducatindia" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                        <a href="https://plus.google.com/+ducat" target="_blank" title="Google Plus"><i class="fa fa-google-plus"></i></a>
                        <a href="https://www.twitter.com/@Ducat_education" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.youtube.com/c/DucatIndiatraining" target="_blank"><i class="fa fa-youtube-square"></i></a>
                      </div>

                  </div> <!-- End Of Col MD 3 -->
                
                  <div class="col-md-3">
                    
                      <h5>SUBSCRIBE TO OUR NEWSLETTER</h5>
                      <div class="subsForm">
		      		<form action="https://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('https://feedburner.google.com/fb/a/mailverify?uri=ducatindia/XZRs', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true"><p><input type="text" name="email" placeholder="Enter Email Id"/></p><input type="hidden" value="ducatindia/XZRs" name="uri"/><input type="hidden" name="loc" value="en_US"/><input type="submit" value="" /></form>
		      </div>

                  </div> <!-- End Of Col MD 3 -->
                
                  <div class="col-md-3">
                    
                      <h5>WE ACCEPT ONLINE PAYMENTS</h5>
                      <img src="/images/payment-icon.jpg"  alt="payment-icon"/>

                  </div> <!-- End Of Col MD 3 -->
                
                  <div class="col-md-3">
                    
                      <h5>PAY ONLINE</h5>
                      <a href="/online-registration"><img alt="pay-online" src="/images/pay-online.gif"/></a>

                  </div> <!-- End Of Col MD 3 -->

              </div> <!-- End Of Row -->

          </div> <!-- End OF Container -->

      </section>

      <section id="copyRight">
            
          <div class="container">
            
              <div class="row">
                
                  <div class="col-md-12">
                    
                      <p>Copyright 1999-2019 Ducat Creative, All Rights Reserved</p><h5 style="display: inline;">&nbsp;</h5>

                  </div> <!-- End Of Col MD 12 -->

              </div> <!-- End Of Row -->

          </div> <!-- End OF Container -->

      </section>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fingerprintjs2/1.5.1/fingerprint2.min.js"></script>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="/js/jquery.fancybox.js"></script>
     <script src="/js/jquery.magic-accordion.js"></script>
    <script src="/js/script.js"></script>
  </body>
</html>	 	