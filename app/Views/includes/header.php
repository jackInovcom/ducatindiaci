

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="images/favicon.png">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
   <title><?php echo $title;?></title>
<meta name="description" content="Industrial Training:DUCAT offers best IT training like Java .Net PHP Oracle program with live project by industry expert In Noida, Ghaziabad, Gurgaon, Faridabad, Greater Noida."/>
<meta name="robots" content="index, follow"/>
<meta name="coverage" content="Worldwide" />
<meta name="country" content="India" />

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/jquery.fancybox.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
  </head>
  <body>
 <header id="header">

    <div class="topHeader">

        <div class="container">

            <div class="row">

                <div class="col-md-6">

                    <ul class="topInfo">
                        <li><i class="fa fa-phone"></i> 0120-4646464</li>
                        <li><i class="fa fa-envelope"></i> info@ducatindia.com</li>
                    </ul>

                </div>
                <!-- End Of Col MD 6 -->

                <div class="col-md-6">

                    <ul class="topSocialIcons pull-right">
                        <li><a href="https://www.facebook.com/ducatEducation" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a href="https://www.twitter.com/@Ducat_education" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="https://plus.google.com/+ducat" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                        <li><a href="https://www.linkedin.com/in/ducatindia" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                        <li><a href="https://www.youtube.com/c/DucatIndiatraining" target="_blank"><i class="fa fa-youtube-square"></i></a></li>
                    </ul>

                </div>
                <!-- End Of Col MD 6 -->

            </div>
            <!-- End Of Row -->

        </div>
        <!-- End OF Container -->

    </div>
    <!-- End Of Top Header -->
    <div class="container">

        <div class="row">

            <div class="col-md-2">

                <a href="/" id="logo">
                    <img src="/images/logo.png" alt="Ducat">
                </a>

            </div>
            <!-- End Of Col MD 3 -->

            <div class="col-md-10">

                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="/">Home</a></li>

                                <li><a href="#">Courses</a>
                                    <ul>
                                        <li><a href="/6-months-industrial-training">6 Months Industrial Training</a></li>

                                        <li><a href="#">LANGUAGE</a>
                                            <ul>
                                                <li><a href="#">Java Technology+</a>
                                                    <ul>
                                                        <li><a href="/best-java-training-in-noida">JAVA EXPERT</a></li>
                                                        <li><a href="/springboottraining">Spring Boot Microservices & Security with hibernate</a></li>
                                                        <li><a href="/java-for-beginners">JAVA BEGINNERS</a></li>
                                                        <li><a href="/javaanduitraining">JAVA + UI</a></li>

                                                        <li><a href="/springhibernatetraining">JAVA Spring &amp; Hibernate</a></li>
                                                        <li><a href="/Javafullstackdeveloper">Java full stack developer</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">C , C++,Python & R Programming</a>
                                                    <ul>
                                                        <li><a href="/c-training">C WITH DATA STRUCTURE & ALGORITHMS</a></li>
                                                        <li><a href="/cpptraining">OBJECT ORIENTED DATA STRUCTURE & ALGORITHMS</a></li>
                                                        <li><a href="/pythontraining">Python</a></li>
                                                        <li><a href="/perlscriptingtraining">Perl Script</a></li>
                                                        <li><a href="/rprogrammingtraining">R Programming</a></li>
                                                        <li><a href="/data-science-using-r-programming">DATA SCIENCE USING R PROGRAMMING</a></li>
                                                        <li><a href="/data-science-ML-using-r-programming">DATA SCIENCE & MACHINE LEARNING USING R PROGRAMMING</a></li>
                                                        <li><a href="/machine-learning-using-r-programming">MACHINE LEARNING USING R PROGRAMMING</a></li>

                                                        <li><a href="/deeplearningtraining">Deep Learning(AI)</a></li>

                                                        <li><a href="/djangotraining">Django</a></li>
                                                        <li><a href="/pythonanddjangotraining">Python And Django</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="/big-commerce-training">Big Commerce</a></li>
                                                <li><a href="#">.Net & Microsoft</a>
                                                    <ul>

                                                        <li><a href="/dotnetraining">.Net 6 Months</a></li>
                                                        <li><a href="/dotnetcoretraining">.Net 4 Months</a></li>
                                                        <li><a href="/coredotnettraining">.Net Core 2.0</a></li>

                                                        <li><a href="/dotnettrainingnoida">Advanced .NET MVC</a></li>
                                                        <li><a href="/dotnetsixweekstraining">.Net 6 weeks</a></li>

                                                    </ul>
                                                </li>
                                                <li><a href="#">Php Technology+</a>
                                                    <ul>
                                                        <li><a href="/phpsixmonthstraining">PHP WITH ANGULAR</a></li>
                                                        <li><a href="/phpsixweekstraining">PHP SIX WEEKS</a></li>
                                                        <li><a href="/phptraining">CORE PHP</a></li>

                                                        <li><a href="/advancephptraining">ADVANCE PHP </a></li>
                                                        <li><a href="/magentotraining">Magento</a></li>
                                                        <li><a href="/laraveltraining">Laravel</a></li>
                                                        <li><a href="/uitraining">UI SPECIALIZATION</a></li>
                                                    </ul>
                                                </li>

                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">New Courses</a>
                                            <ul>
                                                <li><a href="/Blockchaintraining">Blockchain</a></li>
                                                <li><a href="/successmantra">Success Mantra</a></li>

                                                <li><a href="/pegatraininginnoida">PEGA</a></li>
                                                <li><a href="/reactjstraining">React JS</a></li>
                                                <li><a href="/datasciencemlusingpython">DATA SCIENCE & ML USING PYTHON</a></li>
                                                <li><a href="/DucatIntegratedIndustrialProfessionalProgram">DIIPP</a></li>
                                                <li><a href="/pythontraining">Python</a></li>
                                                <li><a href="/sql-server-dba-training-noida">SQL SERVER DBA</a></li>
                                                <li><a href="/besttallyerp9Training">Tally</a></li>
                                                <li><a href="/hrgeneralisttraining">HR Generalist</a></li>
                                                <li><a href="/machinelearningtraining">Machine Learning</a></li>
                                                <li><a href="/devopstraining/">Devops Training</a></li>
                                                <li><a href="/deeplearningtraining">Deep Learning(AI)</a></li>
                                                <li><a href="/deeplearninginpythontraining">Deep Learning Using Python</a></li>
                                                <li><a href="/azuretraining">AZURE</a></li>

                                                <li><a href="/bigdatatraining">Big Data</a></li>
                                                <li><a href="/erpscmtraining">ERP SCM</a></li>
                                                <li><a href="/mistraining">MIS</a></li>
                                                <li><a href="/Randdataanalyticstraining">R & Data Analytics</a></li>
                                                <li><a href="/xamarintraining">Xamarin</a></li>
                                                <li><a href="/Administrationessentials">ADMINISTRATION ESSENTIALS FOR NEW ADMINS</a></li>
                                                <li><a href="/awstraining">AWS Training</a></li>
                                                <li><a href="/Declarativedevelopment">Declarative Development for Platform App Builders </a></li>

                                                <li><a href="/programmaticdevelopers">PROGRAMMATIC DEVELOPMENT USING APEX AND </a></li>
                                                <li><a href="/LIGHTNINGCOMPONENTS">PROGRAMMING LIGHTNING COMPONENTS TRAINING </a></li>
                                                <li><a href="/datascienceusingpython">Data science using Python</a></li>
                                                <li><a href="/pythonwithmachinelearning">Machine learning With Python</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">Robotics process automation(RPA)</a>
                                            <ul>
                                                <li><a href="/roboticsprocessautomationcourses">Robotics process automation(RPA) BLUE PRISM</a></li>
                                                <li><a href="/automationanywheretraining">Automation Any Where</a></li>
                                                <li><a href="/rpauipathtraining">Robotics Process Automation UI Path</a></li>
                                                <li><a href="/rpaworkfusiontraining">Robotics Process Automation Work Fusion</a></li>

                                            </ul>
                                        </li>

                                        <li>
                                            <a href="#">Industrial Training</a>
                                            <ul>
                                                <li><a href="/javahadooptraining">Core java + Hadoop</a></li>

                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">Digital Marketing</a>
                                            <ul>
                                                <li><a href="/advance-digital-marketing-course">Advance Digital Marketing</a></li>
                                                <li><a href="/digital-marketing">Digital Marketing</a></li>
                                                <li><a href="/digitalmarketing6weeks">Digital Marketing 6 Weeks</a></li>
                                                <li><a href="/learnandearn">Learn and Earn</a></li>
                                            </ul>
                                        </li>

                                        <li><a href="#">Data science</a>
                                            <ul>
                                                <li><a href="/mistraining">MIS</a></li>
                                                <li><a href="/Randdataanalyticstraining">R & Data Analytics</a></li>
                                                <li><a href="/rprogrammingtraining">R Programming</a></li>
                                                <li><a href="/data-science-using-r-programming">DATA SCIENCE USING R PROGRAMMING</a></li>
                                                <li><a href="/data-science-ML-using-r-programming">DATA SCIENCE & MACHINE LEARNING USING R PROGRAMMING</a></li>
                                                <li><a href="/machine-learning-using-r-programming">MACHINE LEARNING USING R PROGRAMMING</a></li>
                                                <li><a href="/sastraining">SAS</a></li>
                                                <li><a href="/sasbitraining">SAS BI</a></li>
                                                <li><a href="/tableautraininginnoida">Tableau </a></li>
                                                <li><a href="/powerbitraininginnoida">Power BI</a></li>

                                            </ul>
                                        </li>
                                        <li><a href="#">Web Technology</a>
                                            <ul>
                                                <li><a href="/uitraining">UI </a></li>

                                                <li><a href="/angular2training">Angular </a></li>
                                                <li><a href="/uiandangulartraining">UI And Angular js</a></li>
                                                <li><a href="/meantraining">MEAN</a></li>

                                            </ul>
                                        </li>
                                        <li><a href="#">SOFTWARE TESTING+</a>
                                            <ul>

                                                <li><a href="/sqttraining">Manual Testing</a></li>
                                                <li><a href="/manul_selenium">Manual+Selenium</a></li>
                                                <li><a href="/jmetertraining">Jmeter</a></li>
                                                <li><a href="/Loadrunnertraining">Load Runner</a></li>
                                                <li><a href="/qtpufttraining">QTP/UFT</a></li>

                                            </ul>

                                        </li>
                                        <li><a href="#">Automation TESTING+</a>
                                            <ul>
                                                <li><a href="/apitestingtrainingnoida">API Testing</a></li>
                                                <li><a href="/appiumtraining">Appium Testing</a></li>

                                                <li><a href="/javaseleniumtraining">Java Selenium</a></li>
                                                <li><a href="/seleniumwithpythontraining">Python Selenium</a></li>

                                            </ul>

                                        </li>
                                        <li><a href="#">DATABASE</a>
                                            <ul>
                                                <li><a href="/oracle12cdbatraininginnoida">Oracle 12C DBA</a></li>
                                                <li><a href="/oracle11gdevtrainingindelhincr">Oracle 11G Dev</a></li>
                                                <li><a href="/oracle11gdbatraining">Oracle 6 Weeks</a></li>
                                                <li><a href="/oracle11gdataguardtraining">Oracle 11G DATA GUARD</a></li>
                                                <li><a href="/oracle11gractraininginnoida">Oracle 11G RAC</a></li>
                                                <li><a href="/plsqltraining">SQL+PL/SQL </a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">ECE / EEE / EI /CSE </a>
                                            <ul>
                                                <li><a href="/iottraining">IOT Training</a></li>
                                                <li><a href="#">Embedded 6 Weeks</a>
                                                    <ul>
                                                        <li><a href="/8051-microcontroller-training">8051 MICROCONTROLLER</a></li>
                                                        <li><a href="/pic-microcontroller-training">PIC MICROCONTROLLER</a></li>
                                                        <li><a href="/avr-microcontroller-training">AVR MICROCONTROLLER</a></li>

                                                        <li><a href="/armtraining">Embedded ARM 7</a></li>
                                                        <li><a href="/arduino-training">ARDUINO</a></li>
                                                        <li><a href="/raspberry-pi-training">RASPBERRY PI</a></li>
                                                        <li><a href="/hardware-and-electronics-training">BASIC ELECTRONICS</a></li>
                                                        <li><a href="/pcb-design-training">PCB DESIGN </a></li>
                                                        <li><a href="/device-drivers-training">DEVICE DRIVERS</a></li>
                                                        <li><a href="/rtos-training">RTOS </a></li>
                                                        <li><a href="/embedded-linux-internals-training">EMBEDDED LINUX</a></li>
                                                        <li><a href="/roboticstraining">Robotics Course</a></li>

                                                    </ul>
                                                </li>
                                                <li><a href="/embeddedtraining">Embedded 3 Months</a></li>
                                                <li><a href="/embeddedsystemtraining">Embedded 6 Months</a></li>
                                                <li><a href="/embeddedandroidtraining">Embedded with Android(6 Weeks)</a></li>
                                                <li><a href="#">More Related Electronics +</a>
                                                    <ul>
                                                        <li><a href="/matlabtraining">MATLAB</a></li>

                                                        <li><a href="/digitalsystemtraining">Adv. Digital System Design</a></li>

                                                        <li><a href="/cmosbasedtraining">Cmos Based Design</a></li>

                                                    </ul>
                                                </li>
                                                <li><a href="#">VLSI</a>
                                                    <ul>
                                                        <li><a href="/vlsivhdltraining">VLSI/VHDL 6 weeks</a></li>
                                                            <li><a href="/vlsiverilogtraining">VLSI VERILOG</a></li>
                                                            <li><a href="/vlsiverilogwithtcltraining">System VERILOG WITH TCL</a></li>
                                                            <li><a href="/vlsidesignflowtraining">VLSI Design </a></li>

                                                    </ul>
                                                    </li>
                                                    <li><a href="#">PLC SCADA+</a>
                                                        <ul>
                                                            <li><a href="/plcscada3monthstraining">PLC SCADA 3 months</a></li>
                                                            <li><a href="/plcscadasixweekstraining">PLC SCADA six weeks</a></li>
                                                            <li><a href="/plcscadatraining">PLC SCADA FULL COURCE</a></li>
                                                        </ul>
                                                    </li>
                                            </ul>
                                            </li>
                                            <li><a href="#">LINUX / ADMINISTRATION</a>
                                                <ul>
                                                    <li><a href="/redhattraining">RHCE</a></li>
                                                    <li><a href="/openstackadmintraining">Red Hat Openstack Cloud Administrator</a></li>
                                                    <li><a href="/rhcvatraining">RHCVA</a></li>
                                                    <li><a href="/unixshellscripttraining">UNIX / Linux SHELL Scripting</a></li>
                                                    <li><a href="/red-hat-server-hardening-rh413">RED HAT SERVER HARDENING (RH413)</a></li>
                                                    <li><a href="/436clustertraining">436 cluster </a></li>
                                                    <li><a href="/do407ansiblettraining">DO 407 Ansible</a></li>
                                                    <li><a href="/performancetuningtraining">Performance Tuning</a></li>
                                                    <li><a href="/rh236glustertraining">RH 236 Gluster</a></li>
                                                    <li><a href="/rh310openstacktraining">RH 310 openstack</a></li>
                                                    <li><a href="/rhtraining">RH 342</a></li>

                                                </ul>
                                            </li>
                                            <li><a href="#">NETWORKING</a>
                                                <ul>
                                                    <li><a href="/diplomainhardwarenetworking">Diploma In Hardware Networking</a></li>
                                                    <li><a href="/networkingtraining">Networking Six Months</a></li>
                                                    <li><a href="/networking3monthstraining">Networking 3 Months</a></li>
                                                    <li><a href="/networkingtraining6weeks">Networking 6 Weeks</a></li>
                                                    <li><a href="/ccnatraining">CCNA</a></li>
                                                    <li><a href="#">CCNA SECURITY +</a>
                                                        <ul>
                                                            <li><a href="/asafirewalltraining">ASA FIREWALL</a></li>
                                                            <li><a href="/ccnasecuritytraining">CCNA SECURITY</a></li>
                                                            <li><a href="/checkpointfirewalltraining">CHECK POINT FIREWALL</a></li>
                                                            <li><a href="/paloaltotraining">PALO ALTO</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="/ccnptraining">CCNP</a></li>
                                                    <li><a href="/mcsatraining">MCSA 2012</a></li>
                                                    <li><a href="/mcsaserver2016training">MCSA 2016</a></li>
                                                    <li><a href="/mcsetraining">MCSE</a></li>
                                                    <li><a href="#">Networking +</a>
                                                        <ul>
                                                            <li><a href="/mcse2012exchangetraining">MCSE 2012 EXCHANGE SERVER</a></li>
                                                            <li><a href="/mcitptraining">MCITP</a></li>
                                                            <li><a href="/ccna-linux">CCNA + LINUX</a></li>
                                                            <li><a href="/vmwaretraining">VMWare</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="#">CAD / MODELING</a>
                                                <ul>
                                                    <li><a href="/autocadtraining">Autocad</a></li>
                                                    <li><a href="/cadcustomizationtraining">CAD Customization Training </a></li>
                                                    <li><a href="/gdandttraining">GD&T Training</a></li>
                                                    <li><a href="/catiatraining">Catia</a></li>
                                                    <li><a href="/proetraining">Pro-e</a></li>
                                                    <li><a href="/cncprogrammingtraining">CNC Programming</a></li>
                                                    <li><a href="/staadprotraining">STAAD PRO</a></li>
                                                    <li><a href="/revittraining">Revit Architecture</a></li>
                                                    <li><a href="/3dmaxtraining">3D Studio Max</a></li>
                                                    <li><a href="/unigraphicstraining">Unigraphics</a></li>
                                                    <li><a href="/ansystraining">ANSYS TRAINING</a></li>
                                                    <li><a href="/solidworkstraining">Solidworks</a></li>
                                                    <li><a href="/primavertraining">Primavera Project Planner</a></li>
                                                    <li><a href="/cadciviltraining">CAD Civil Six Months</a></li>
                                                    <li><a href="/cadmechanicaltraining">CAD Mechanical Six Months</a></li>

                                                </ul>
                                            </li>

                                            <li><a href="#">MOBILE APPLICATIONS</a>
                                                <ul>
                                                    <li><a href="/androidtraining">Android</a></li>
                                                    <li><a href="/corejavaandroidandkotlintraining/">Corejava + Android + Kotlin</a></li>
                                                    <li><a href="/iphonetraining">I-Phone</a></li>
                                                </ul>

                                            </li>
                                            <li><a href="/Best-sap-erp-training/"> SAP ERP</a>
                                                <ul>

                                                    <li><a href="/Best-sap-erp-training/"> SAP ERP</a></li>
                                                    <li><a href="/erpbibwtraining">ERP-SAP-BI & BW</a></li>
                                                    <li><a href="/erp-abap">ERP-SAP-ABAP</a></li>
                                                    <li><a href="/erp-basis">ERP-SAP-BASIS</a></li>
                                                    <li><a href="/erp-crm">ERP-SAP-CRM</a></li>
                                                    <li><a href="/erp-fico">ERP-SAP-FICO</a></li>
                                                    <li><a href="/erpscmtraining">ERP-SAP- SCM</a></li>
                                                    <li><a href="/erp-hr">ERP-SAP-HR</a></li>
                                                    <li><a href="/erp-mm">ERP-SAP-MM</a></li>
                                                    <li><a href="/erp-pp">ERP-SAP-PP</a></li>
                                                    <li><a href="/erp-sd">ERP-SAP-SD</a></li>
                                                    <li><a href="/oracleappstraining">Oracle app’s R12 Financial</a></li>
                                                    <li><a href="/oracletechniacltraining">Oracle app’s R12 Technical</a></li>
                                                    <li><a href="/oracleappsdba">Oracle app’s DBA</a></li>
                                                    <li><a href="/oracleappsscm">Oracle app’s SCM</a></li>

                                                </ul>

                                            </li>
                                            <li><a href="#">GRAPHIC / WEB DESIGN</a>
                                                <ul>
                                                    <li><a href="/webdesigntraining">WEB DESIGNING</a></li>
                                                    <li><a href="/graphicsdesigntraining">GRAPHICS DESIGNING</a></li>
                                                    <li><a href="/html5training">HTML5</a></li>
                                                    <li><a href="/coreldrawtraining">CorelDraw</a></li>
                                                    <li><a href="/multimedia-animation">Multimedia &amp; Animation</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Big Data Hadoop</a>
                                                <ul>
                                                    <li><a href="/bigdatatraining">Big Data Hadoop</a></li>
                                                    <li><a href="/hadooptraining">Big Data Hadoop Admin</a></li>
                                                    <li><a href="/bigdatahadoopdevtraining">Big Data Hadoop Dev</a></li>
                                                    <li><a href="/hadoop6weeks">Hadoop 6 Weeks</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">INDUSTRY TRENDS</a>
                                                <ul>
                                                    <li><a href="/microsoft-sql-server-training">Microsoft sql server</a></li>
                                                    <li><a href="/msbi-training-in-noida">MSBI</a></li>
                                                    <li><a href="/ibmmainframetraining">IBM MAINFRAME</a></li>
                                                    <li><a href="/bicognostraining">Cognos 10 BI</a></li>
                                                    <li><a href="#">SAS+</a>
                                                        <ul>
                                                            <li><a href="/sastraining">SAS</a></li>
                                                            <li><a href="/sasbitraining">SAS BI</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="#">CLOUD COMPUTING</a>
                                                <ul>
                                                    <li><a href="/cloudtraining">Cloud Computing six Weeks</a></li>
                                                    <li><a href="/cloudcomputingtraining">Cloud Computing Full Course</a></li>
                                                    <li><a href="/cloudinfrastructuretraining">Cloud Infrastructure</a></li>
                                                    <li><a href="/salesforcedevtraining">Salesforce Development</a></li>
                                                    <li><a href="/salesforceadmintraining">Salesforce Administration</a></li>
                                                    <li><a href="/amazoncloudtraining">Amazon Cloud</a></li>
                                                    <li><a href="/azuretraining">Azure</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">SHAREPOINT</a>
                                                <ul>
                                                    <li><a href="/sharepointtraining">Sharepoint Development 2010 Training</a></li>
                                                    <li><a href="/sharepointdev2013training">Sharepoint Development 2013 Training</a></li>
                                                    <li><a href="/sharepointadmin2010training">SharePoint Admin 2010 Training</a></li>
                                                    <li><a href="/sharepointadmin2013training">Sharepoint Admin 2013 Training</a></li>

                                                </ul>
                                            </li>
                                            <li><a href="#">6 Weeks Summer Training</a>
                                                <ul>
                                                    <li><a href="/android-summer-training">Android 6 Weeks</a></li>
                                                    <li><a href="/amazon6weeks">Amazon 6 weeks</a></li>
                                                    <li><a href="/datascienceusingpython">Data science Python 6 Weeks</a></li>

                                                    <li><a href="/corepythontraining">Core Python  6 Weeks</a></li>
                                                    <li><a href="/digitalmarketing6weeks">Digital Marketing 6 Weeks</a></li>
                                                    <li><a href="/datasciencerprogramming">Data Science Using R Programming 6 weeks</a></li>
                                                    <li><a href="/dotnetsixweekstraining">Dot Net 6 Weeks</a></li>
                                                    <li><a href="/manulselenum6weeks">Software Testing 6 weeks</a></li>
                                                    <li><a href="/ethicalhackingtraining">Ethical Hacking 6 weeks</a></li>
                                                    <li><a href="#">java 6 Weeks +</a>
                                                        <ul>
                                                            <li><a href="/javasixweekstraining">java 6 Weeks</a></li>
                                                            <li><a href="/advancejavatraining">Adv java 6 Weeks</a></li>
                                                            <li><a href="/javawithandroidtraining">Java + Android 6 weeks</a></li>
                                                            <li><a href="/javahadooptraining">Java + Hadoop 6 weeks</a></li>
                                                            <li><a href="/javawithsqltraining">Java + SQL 6 weeks</a></li>
                                                            <li><a href="/javawithangular4training">java + Angular 6 weeks</a></li>
                                                        </ul>
                                                    </li>

                                                    <li><a href="/machinelearninginpython">Machine Learning In python</a></li>
                                                    <li><a href="/python6weekstraining">Python 6 weeks</a></li>
                                                    <li><a href="/phpsixweekstraining">PHP 6 weeks</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">OTHERS</a>
                                                <ul>
                                                    <li><a href="/datawarehousingtraining">Data Warehousing</a></li>
                                                    <li><a href="/seotraining">SEO</a></li>

                                                    <li><a href="/sepdtraining">SE-PD(Personality Dev.)</a></li>
                                                    <li><a href="/advancedmicrosoftexceltraining">Advanced Microsoft Excel</a></li>

                                                    <li><a href="/6weekssummertraining">Summer Training</a></li>
                                                </ul>
                                            </li>
                                    </ul>

                                    <li><a href="/aboutus">About Us</a>
                                    </li>

                                    <li><a href="#">Services</a>
                                        <ul>
                                            <li><a href="/campus-training">Campus Training</a></li>
                                            <li><a href="/corporate-training">Corporate Training</a></li>
                                            <li><a href="/classroom-training">Classroom Training</a></li>
                                            <li><a href="/industrial-training">Industrial Training</a></li>
                                        </ul>
                                    </li>

                                    <!--<li><a href="/events">Events</a></li>-->
                                    <li><a href="/online-registration">Online Registration</a></li>
                                    <!--<li><a href="/jobfair-registration">Jobs Fair</a></li>-->
                                    <!--<li><a href="/blog/">Blog</a></li>-->
                                    <li><a href="/contact-us">Contact us</a></li>
                                    <li class="search-link">
                                        <a href="javascript:void(0);"><i class="fa fa-search"></i></a>
                                        <div class="search-area">
                                            <form action="#" id="header-searchform" method="get">
                                                <input type="text" id="header-s" name="s" value="" autocomplete="off">
                                                <button type="submit" id="header-searchsubmit" class="button default medium"><i class="fa fa-search"></i></button>
                                            </form>
                                        </div>
                                    </li>
                            </ul>

                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container-fluid -->
                </nav>

            </div>
            <!-- End Of Col MD 9 -->

        </div>
        <!-- End Of Row -->

    </div>
    <!-- End OF Container -->

</header>
<!-- End Of Header -->