<?php namespace Config;

/**
 * --------------------------------------------------------------------
 * URI Routing
 * --------------------------------------------------------------------
 * This file lets you re-map URI requests to specific controller functions.
 *
 * Typically there is a one-to-one relationship between a URL string
 * and its corresponding controller class/method. The segments in a
 * URL normally follow this pattern:
 *
 *    example.com/class/method/id
 *
 * In some instances, however, you may want to remap this relationship
 * so that a different class/function is called than the one
 * corresponding to the URL.
 */

// Create a new instance of our RouteCollection class.
$routes = Services::routes(true);

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 * The RouteCollection object allows you to modify the way that the
 * Router works, by acting as a holder for it's configuration settings.
 * The following methods can be called on the object to modify
 * the default operations.
 *
 *    $routes->defaultNamespace()
 *
 * Modifies the namespace that is added to a controller if it doesn't
 * already have one. By default this is the global namespace (\).
 *
 *    $routes->defaultController()
 *
 * Changes the name of the class used as a controller when the route
 * points to a folder instead of a class.
 *
 *    $routes->defaultMethod()
 *
 * Assigns the method inside the controller that is ran when the
 * Router is unable to determine the appropriate method to run.
 *
 *    $routes->setAutoRoute()
 *
 * Determines whether the Router will attempt to match URIs to
 * Controllers when no specific route has been defined. If false,
 * only routes that have been defined here will be available.
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

$routes->get('/comming-soon-batches/', 'Home::commingsoonbatches');

$routes->get('/3dmaxtraining', 'Home::DUC3dmaxtraining');
$routes->get('/6-months-industrial-training', 'Home::DUC6monthsindustrialtraining');
$routes->get('/6weekssummertraining', 'Home::DUC6weekssummertraining');
$routes->get('/436clustertraining', 'Home::DUC436clustertraining');
$routes->get('/8051-microcontroller-training', 'Home::DUC8051microcontrollertraining');
$routes->get('/aboutus', 'Home::DUCaboutus');
$routes->get('/Administrationessentials', 'Home::DUCAdministrationessentials');
$routes->get('/advance-digital-marketing-course', 'Home::DUCadvancedigitalmarketingcourse');
$routes->get('/advancedmicrosoftexceltraining', 'Home::DUCadvancedmicrosoftexceltraining');
$routes->get('/advancejavatraining', 'Home::DUCadvancejavatraining');
$routes->get('/advancephptraining', 'Home::DUCadvancephptraining');
$routes->get('/amazon6weeks', 'Home::DUCamazon6weeks');
$routes->get('/amazoncloudtraining', 'Home::DUCamazoncloudtraining');
$routes->get('/android-summer-training', 'Home::DUCandroidsummertraining');
$routes->get('/androidtraining', 'Home::DUCandroidtraining');
$routes->get('/angular2training', 'Home::DUCangular2training');
$routes->get('/ansystraining', 'Home::DUCansystraining');
$routes->get('/apitestingtrainingnoida', 'Home::DUCapitestingtrainingnoida');
$routes->get('/appiumtraining', 'Home::DUCappiumtraining');
$routes->get('/arduino-training', 'Home::DUCarduinotraining');
$routes->get('/armtraining', 'Home::DUCarmtraining');
$routes->get('/asafirewalltraining', 'Home::DUCasafirewalltraining');
$routes->get('/autocadtraining', 'Home::DUCautocadtraining');
$routes->get('/autodeskcertification', 'Home::DUCautodeskcertification');
$routes->get('/automationanywheretraining', 'Home::DUCautomationanywheretraining');
$routes->get('/avr-microcontroller-training', 'Home::DUCavrmicrocontrollertraining');
$routes->get('/awstraining', 'Home::DUCawstraining');
$routes->get('/azuretraining', 'Home::DUCazuretraining');
$routes->get('/become-an-instructor', 'Home::DUCbecomeaninstructor');
$routes->get('/best-java-training-in-noida', 'Home::DUCbestjavatraininginnoida');
$routes->get('/Best-sap-erp-training', 'Home::DUCBestsaperptraining');
$routes->get('/besttallyerp9Training', 'Home::DUCbesttallyerp9Training');
$routes->get('/bicognostraining', 'Home::DUCbicognostraining');
$routes->get('/big-commerce-training', 'Home::DUCbigcommercetraining');
$routes->get('/bigdatahadoopdevtraining', 'Home::DUCbigdatahadoopdevtraining');
$routes->get('/bigdatatraining', 'Home::DUCbigdatatraining');
$routes->get('/Blockchaintraining', 'Home::DUCBlockchaintraining');
$routes->get('/c-training', 'Home::DUCctraining');
$routes->get('/cadciviltraining', 'Home::DUCcadciviltraining');
$routes->get('/cadcustomizationtraining', 'Home::DUCcadcustomizationtraining');
$routes->get('/cadmechanicaltraining', 'Home::DUCcadmechanicaltraining');
$routes->get('/campus-training', 'Home::DUCcampustraining');
$routes->get('/catiatraining', 'Home::DUCcatiatraining');
$routes->get('/ccna-linux', 'Home::DUCccnalinux');
$routes->get('/ccnasecuritytraining', 'Home::DUCccnasecuritytraining');
$routes->get('/ccnatraining', 'Home::DUCccnatraining');
$routes->get('/ccnptraining', 'Home::DUCccnptraining');
$routes->get('/certificationrequest', 'Home::DUCcertificationrequest');
$routes->get('/checkpointfirewalltraining', 'Home::DUCcheckpointfirewalltraining');
$routes->get('/classroom-training', 'Home::DUCclassroomtraining');
$routes->get('/cloudcomputingtraining', 'Home::DUCcloudcomputingtraining');
$routes->get('/cloudinfrastructuretraining', 'Home::DUCcloudinfrastructuretraining');
$routes->get('/cloudtraining', 'Home::DUCcloudtraining');
$routes->get('/cmosbasedtraining', 'Home::DUCcmosbasedtraining');
$routes->get('/cncprogrammingtraining', 'Home::DUCcncprogrammingtraining');
$routes->get('/contact-us', 'Home::DUCcontactus');
$routes->get('/coredotnettraining', 'Home::DUCcoredotnettraining');
$routes->get('/corejavaandroidandkotlintraining', 'Home::DUCcorejavaandroidandkotlintraining');
$routes->get('/coreldrawtraining', 'Home::DUCcoreldrawtraining');
$routes->get('/corepythontraining', 'Home::DUCcorepythontraining');
$routes->get('/corporate-training', 'Home::DUCcorporatetraining');
$routes->get('/cpptraining', 'Home::DUCcpptraining');
$routes->get('/data-science-ML-using-r-programming', 'Home::DUCdatascienceMLusingrprogramming');
$routes->get('/data-science-using-r-programming', 'Home::DUCdatascienceusingrprogramming');
$routes->get('/datasciencemlusingpython', 'Home::DUCdatasciencemlusingpython');
$routes->get('/datasciencerprogramming', 'Home::DUCdatasciencerprogramming');
$routes->get('/datascienceusingpython', 'Home::DUCdatascienceusingpython');
$routes->get('/datawarehousingtraining', 'Home::DUCdatawarehousingtraining');
$routes->get('/Declarativedevelopment', 'Home::DUCDeclarativedevelopment');
$routes->get('/deeplearninginpythontraining', 'Home::DUCdeeplearninginpythontraining');
$routes->get('/deeplearningtraining', 'Home::DUCdeeplearningtraining');
$routes->get('/device-drivers-training', 'Home::DUCdevicedriverstraining');
$routes->get('/devopstraining', 'Home::DUCdevopstraining');
$routes->get('/digital-marketing', 'Home::DUCdigitalmarketing');
$routes->get('/digitalmarketing6weeks', 'Home::DUCdigitalmarketing6weeks');
$routes->get('/digitalsystemtraining', 'Home::DUCdigitalsystemtraining');
$routes->get('/diplomainhardwarenetworking', 'Home::DUCdiplomainhardwarenetworking');
$routes->get('/djangotraining', 'Home::DUCdjangotraining');
$routes->get('/do407ansiblettraining', 'Home::DUCdo407ansiblettraining');
$routes->get('/dotnetcoretraining', 'Home::DUCdotnetcoretraining');
$routes->get('/dotnetraining', 'Home::DUCdotnetraining');
$routes->get('/dotnetsixweekstraining', 'Home::DUCdotnetsixweekstraining');
$routes->get('/dotnettrainingnoida', 'Home::DUCdotnettrainingnoida');
$routes->get('/ducat-gallery', 'Home::DUCducatgallery');
$routes->get('/DucatIntegratedIndustrialProfessionalProgram', 'Home::DUCDucatIntegratedIndustrialProfessionalProgram');
$routes->get('/embedded-linux-internals-training', 'Home::DUCembeddedlinuxinternalstraining');
$routes->get('/embeddedandroidtraining', 'Home::DUCembeddedandroidtraining');
$routes->get('/embeddedsystemtraining', 'Home::DUCembeddedsystemtraining');
$routes->get('/embeddedtraining', 'Home::DUCembeddedtraining');
$routes->get('/erp-abap', 'Home::DUCerpabap');
$routes->get('/erp-basis', 'Home::DUCerpbasis');
$routes->get('/erp-crm', 'Home::DUCerpcrm');
$routes->get('/erp-fico', 'Home::DUCerpfico');
$routes->get('/erp-hr', 'Home::DUCerphr');
$routes->get('/erp-mm', 'Home::DUCerpmm');
$routes->get('/erp-pp', 'Home::DUCerppp');
$routes->get('/erp-sd', 'Home::DUCerpsd');
$routes->get('/erpbibwtraining', 'Home::DUCerpbibwtraining');
$routes->get('/erpscmtraining', 'Home::DUCerpscmtraining');
$routes->get('/ethicalhackingtraining', 'Home::DUCethicalhackingtraining');
$routes->get('/gdandttraining', 'Home::DUCgdandttraining');
$routes->get('/global-alliances', 'Home::DUCglobalalliances');
$routes->get('/hadoop6weeks', 'Home::DUChadoop6weeks');
$routes->get('/hadooptraining', 'Home::DUChadooptraining');
$routes->get('/hardware-and-electronics-training', 'Home::DUChardwareandelectronicstraining');
$routes->get('/hrgeneralisttraining', 'Home::DUChrgeneralisttraining');
$routes->get('/html5training', 'Home::DUChtml5training');
$routes->get('/ibmmainframetraining', 'Home::DUCibmmainframetraining');
$routes->get('/industrial-training', 'Home::DUCindustrialtraining');
$routes->get('/iottraining', 'Home::DUCiottraining');
$routes->get('/iphonetraining', 'Home::DUCiphonetraining');
$routes->get('/java-for-beginners', 'Home::DUCjavaforbeginners');
$routes->get('/javaanduitraining', 'Home::DUCjavaanduitraining');
$routes->get('/Javafullstackdeveloper', 'Home::DUCJavafullstackdeveloper');
$routes->get('/javahadooptraining', 'Home::DUCjavahadooptraining');
$routes->get('/javaseleniumtraining', 'Home::DUCjavaseleniumtraining');
$routes->get('/javasixweekstraining', 'Home::DUCjavasixweekstraining');
$routes->get('/javawithandroidtraining', 'Home::DUCjavawithandroidtraining');
$routes->get('/javawithangular4training', 'Home::DUCjavawithangular4training');
$routes->get('/javawithsqltraining', 'Home::DUCjavawithsqltraining');
$routes->get('/jmetertraining', 'Home::DUCjmetertraining');
$routes->get('/jobfair-registration', 'Home::DUCjobfairregistration');
$routes->get('/laraveltraining', 'Home::DUClaraveltraining');
$routes->get('/learnandearn', 'Home::DUClearnandearn');
$routes->get('/LIGHTNINGCOMPONENTS', 'Home::DUCLIGHTNINGCOMPONENTS');
$routes->get('/Loadrunnertraining', 'Home::DUCLoadrunnertraining');
$routes->get('/machine-learning-using-r-programming', 'Home::DUCmachinelearningusingrprogramming');
$routes->get('/machinelearninginpython', 'Home::DUCmachinelearninginpython');
$routes->get('/machinelearningtraining', 'Home::DUCmachinelearningtraining');
$routes->get('/magentotraining', 'Home::DUCmagentotraining');
$routes->get('/manulselenum6weeks', 'Home::DUCmanulselenum6weeks');
$routes->get('/manul_selenium', 'Home::DUCmanul_selenium');
$routes->get('/matlabtraining', 'Home::DUCmatlabtraining');
$routes->get('/mcitptraining', 'Home::DUCmcitptraining');
$routes->get('/mcsaserver2016training', 'Home::DUCmcsaserver2016training');
$routes->get('/mcsatraining', 'Home::DUCmcsatraining');
$routes->get('/mcse2012exchangetraining', 'Home::DUCmcse2012exchangetraining');
$routes->get('/mcsetraining', 'Home::DUCmcsetraining');
$routes->get('/meantraining', 'Home::DUCmeantraining');
$routes->get('/microsoft-sql-server-training', 'Home::DUCmicrosoftsqlservertraining');
$routes->get('/mistraining', 'Home::DUCmistraining');
$routes->get('/msbi-training-in-noida', 'Home::DUCmsbitraininginnoida');
$routes->get('/multimedia-animation', 'Home::DUCmultimediaanimation');
$routes->get('/networking3monthstraining', 'Home::DUCnetworking3monthstraining');
$routes->get('/networkingtraining', 'Home::DUCnetworkingtraining');
$routes->get('/networkingtraining6weeks', 'Home::DUCnetworkingtraining6weeks');
$routes->get('/online-registration', 'Home::DUConlineregistration');
$routes->get('/openstackadmintraining', 'Home::DUCopenstackadmintraining');
$routes->get('/oracle11gdataguardtraining', 'Home::DUCoracle11gdataguardtraining');
$routes->get('/oracle11gdbatraining', 'Home::DUCoracle11gdbatraining');
$routes->get('/oracle11gdevtrainingindelhincr', 'Home::DUCoracle11gdevtrainingindelhincr');
$routes->get('/oracle11gractraininginnoida', 'Home::DUCoracle11gractraininginnoida');
$routes->get('/oracle12cdbatraininginnoida', 'Home::DUCoracle12cdbatraininginnoida');
$routes->get('/oracleappsdba', 'Home::DUCoracleappsdba');
$routes->get('/oracleappsscm', 'Home::DUCoracleappsscm');
$routes->get('/oracleappstraining', 'Home::DUCoracleappstraining');
$routes->get('/oracletechniacltraining', 'Home::DUCoracletechniacltraining');
$routes->get('/paloaltotraining', 'Home::DUCpaloaltotraining');
$routes->get('/pcb-design-training', 'Home::DUCpcbdesigntraining');
$routes->get('/pegatraininginnoida', 'Home::DUCpegatraininginnoida');
$routes->get('/performancetuningtraining', 'Home::DUCperformancetuningtraining');
$routes->get('/perlscriptingtraining', 'Home::DUCperlscriptingtraining');
$routes->get('/phpsixmonthstraining', 'Home::DUCphpsixmonthstraining');
$routes->get('/phpsixweekstraining', 'Home::DUCphpsixweekstraining');
$routes->get('/phptraining', 'Home::DUCphptraining');
$routes->get('/pic-microcontroller-training', 'Home::DUCpicmicrocontrollertraining');
$routes->get('/plcscadasixweekstraining', 'Home::DUCplcscadasixweekstraining');
$routes->get('/plcscadatraining', 'Home::DUCplcscadatraining');
$routes->get('/plsqltraining', 'Home::DUCplsqltraining');
$routes->get('/powerbitraininginnoida', 'Home::DUCpowerbitraininginnoida');
$routes->get('/primavertraining', 'Home::DUCprimavertraining');
$routes->get('/proetraining', 'Home::DUCproetraining');
$routes->get('/programmaticdevelopers', 'Home::DUCprogrammaticdevelopers');
$routes->get('/python6weekstraining', 'Home::DUCpython6weekstraining');
$routes->get('/pythonanddjangotraining', 'Home::DUCpythonanddjangotraining');
$routes->get('/pythontraining', 'Home::DUCpythontraining');
$routes->get('/pythonwithmachinelearning', 'Home::DUCpythonwithmachinelearning');
$routes->get('/qtpufttraining', 'Home::DUCqtpufttraining');
$routes->get('/Randdataanalyticstraining', 'Home::DUCRanddataanalyticstraining');
$routes->get('/raspberry-pi-training', 'Home::DUCraspberrypitraining');
$routes->get('/reactjstraining', 'Home::DUCreactjstraining');
$routes->get('/red-hat-server-hardening-rh413', 'Home::DUCredhatserverhardeningrh413');
$routes->get('/redhattraining', 'Home::DUCredhattraining');
$routes->get('/revittraining', 'Home::DUCrevittraining');
$routes->get('/rh236glustertraining', 'Home::DUCrh236glustertraining');
$routes->get('/rh310openstacktraining', 'Home::DUCrh310openstacktraining');
$routes->get('/rhcvatraining', 'Home::DUCrhcvatraining');
$routes->get('/rhtraining', 'Home::DUCrhtraining');
$routes->get('/roboticsprocessautomationcourses', 'Home::DUCroboticsprocessautomationcourses');
$routes->get('/roboticstraining', 'Home::DUCroboticstraining');
$routes->get('/rpauipathtraining', 'Home::DUCrpauipathtraining');
$routes->get('/rpaworkfusiontraining', 'Home::DUCrpaworkfusiontraining');
$routes->get('/rprogrammingtraining', 'Home::DUCrprogrammingtraining');
$routes->get('/rtos-training', 'Home::DUCrtostraining');
$routes->get('/salesforceadmintraining', 'Home::DUCsalesforceadmintraining');
$routes->get('/salesforcedevtraining', 'Home::DUCsalesforcedevtraining');
$routes->get('/sasbitraining', 'Home::DUCsasbitraining');
$routes->get('/sastraining', 'Home::DUCsastraining');
$routes->get('/schedule-a-free-demo', 'Home::DUCscheduleafreedemo');
$routes->get('/seleniumwithpythontraining', 'Home::DUCseleniumwithpythontraining');
$routes->get('/seotraining', 'Home::DUCseotraining');
$routes->get('/sepdtraining', 'Home::DUCsepdtraining');
$routes->get('/sharepointadmin2010training', 'Home::DUCsharepointadmin2010training');
$routes->get('/sharepointadmin2013training', 'Home::DUCsharepointadmin2013training');
$routes->get('/sharepointdev2013training', 'Home::DUCsharepointdev2013training');
$routes->get('/sharepointtraining', 'Home::DUCsharepointtraining');
$routes->get('/solidworkstraining', 'Home::DUCsolidworkstraining');
$routes->get('/springboottraining', 'Home::DUCspringboottraining');
$routes->get('/springhibernatetraining', 'Home::DUCspringhibernatetraining');
$routes->get('/sql-server-dba-training-noida', 'Home::DUCsqlserverdbatrainingnoida');
$routes->get('/sqtsixmonthstraining', 'Home::DUCsqtsixmonthstraining');
$routes->get('/sqttraining', 'Home::DUCsqttraining');
$routes->get('/staadprotraining', 'Home::DUCstaadprotraining');
$routes->get('/student-details', 'Home::DUCstudentdetails');
$routes->get('/successmantra', 'Home::DUCsuccessmantra');
$routes->get('/tableautraininginnoida', 'Home::DUCtableautraininginnoida');
$routes->get('/uiandangulartraining', 'Home::DUCuiandangulartraining');
$routes->get('/uitraining', 'Home::DUCuitraining');
$routes->get('/unigraphicstraining', 'Home::DUCunigraphicstraining');
$routes->get('/unixshellscripttraining', 'Home::DUCunixshellscripttraining');
$routes->get('/upcoming-batches', 'Home::DUCupcomingbatches');
$routes->get('/vlsidesignflowtraining', 'Home::DUCvlsidesignflowtraining');
$routes->get('/vlsiverilogtraining', 'Home::DUCvlsiverilogtraining');
$routes->get('/vlsiverilogwithtcltraining', 'Home::DUCvlsiverilogwithtcltraining');
$routes->get('/vlsivhdltraining', 'Home::DUCvlsivhdltraining');
$routes->get('/vmwaretraining', 'Home::DUCvmwaretraining');
$routes->get('/webdesigntraining', 'Home::DUCwebdesigntraining');
$routes->get('/workshop-at-colleges', 'Home::DUCworkshopatcolleges');
$routes->get('/xamarintraining', 'Home::DUCxamarintraining');
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}