$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});

jQuery(document).click(function() {
    jQuery(".search-link .search-area").fadeOut('fast');
    jQuery('li.cart, li.cart-main').find('> div').stop().fadeOut('fast');
});
jQuery(".search-link .search-area, .cart-contents, .cart-empty").click(function (e) {
    e.stopPropagation();
    //do redirect or any other action on the content
});    
jQuery(".search-link a").click(function(e) {
    e.stopPropagation();
    jQuery('li.cart, li.cart-main').find('> div').stop().fadeOut('fast');
    jQuery(this).next().stop().fadeIn('fast');
});
jQuery("li.cart, li.cart-main").click(function(e) {
    e.stopPropagation();
    jQuery(".search-link .search-area").fadeOut('fast');
    jQuery(this).find('> div').stop().fadeIn('fast');
});

$("#myCarouselClient").carousel();

$("#myCarouselSpeak").carousel();

//$('.fancybox').fancybox();

$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});

$( ".panel-heading" ).click(function() {
  $( this ).toggleClass( "plus" );
});

$('.contentAcc').magicAccordion({
    headingTag  : 'h2',
    bodyClass   : 'body',
    headClass   : 'head',
    activeClass : 'active',
    speed       : 200,
    easing      : 'swing'
}).on( 'opened.magic', function(e){
    console.log(e.head);
}).on( 'closed.magic', function(e){
    console.log(e.body);
});

var app = $('.contentAcc').data( 'magic-accordion' );
$('.unbind').click( function(e) {
    e.preventDefault();
    app.unbind();
});
$('.rebind').click( function(e) {
    e.preventDefault();
    app.rebind();
});
